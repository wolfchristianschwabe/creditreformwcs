#!/bin/sh
echo Start! $(date)
echo Lade Minigui aus Docker-Mandanten..
scp -r -P 2205 s3@localhost:/transfer .
cd transfer/
echo Entpacke Minigui...
tar xzvf abasgui-mini.tgz
rm abasgui-mini.tgz
echo Übernehme Konfiguration...
cp ../wineks.ini .
echo Fertig! $(date)