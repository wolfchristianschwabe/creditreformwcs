# Creditreform-Schnittstelle als ESDK-Projekt 

![ESDK AppID](https://img.shields.io/badge/esdk-ckcre-success)
[![Start Jenkins build](https://img.shields.io/badge/jenkins-start%20build-yellow)](http://pfoapp029.computerkomplett.de:8080/job/ckcre/build?token=ckcre)
[![Build Status](http://pfoapp029.computerkomplett.de:8080/buildStatus/icon?job=ckcre)](http://pfoapp029.computerkomplett.de:8080/job/ckcre/)
[![Quality Gate Status](http://pfoapp029:9000/api/project_badges/measure?project=de.ck.ckcre:ckcre&metric=alert_status)](http://pfoapp029:9000/dashboard?id=de.ck.ckcre:ckcre)
[![Maintainability Rating](http://pfoapp029:9000/api/project_badges/measure?project=de.ck.ckcre:ckcre&metric=sqale_rating)](http://pfoapp029:9000/dashboard?id=de.ck.ckcre:ckcre)
[![Reliability Rating](http://pfoapp029:9000/api/project_badges/measure?project=de.ck.ckcre:ckcre&metric=reliability_rating)](http://pfoapp029:9000/dashboard?id=de.ck.ckcre:ckcre)
[![Security Rating](http://pfoapp029:9000/api/project_badges/measure?project=de.ck.ckcre:ckcre&metric=security_rating)](http://pfoapp029:9000/dashboard?id=de.ck.ckcre:ckcre)
[![Lines of Code](http://pfoapp029:9000/api/project_badges/measure?project=de.ck.ckcre:ckcre&metric=ncloc)](http://pfoapp029:9000/dashboard?id=de.ck.ckcre:ckcre)
[![Coverage](http://pfoapp029:9000/api/project_badges/measure?project=de.ck.ckcre:ckcre&metric=coverage)](http://pfoapp029:9000/dashboard?id=de.ck.ckcre:ckcre)
[![Bugs](http://pfoapp029:9000/api/project_badges/measure?project=de.ck.ckcre:ckcre&metric=bugs)](http://pfoapp029:9000/dashboard?id=de.ck.ckcre:ckcre)

- - -

## Technische Voraussetzungen

Der Technische Stand der Entwicklung der Creditreform Schnittstelle ist das sie ab abas Version ***2016r4n16p23*** einsetzbar ist.
Sie kann auf niedrigeren Systemen nur unter bestimmten Voraussetzungen installiert werden.

### Installation bei älteren Versionen

1.	Größerer Aufwand für Kompilierung der Programme muss eingerechnet werden
2.	Alle Versionen die kein AJO unterstützen fallen heraus.
3.	Die Version muss bereits JaxB in einem gesonderten lib Verzeichnis beherbergen. (muss abas in der Regel machen) 
4.	Es müssen alle Datenbanken angepasst werden und die FOP.txt benötigt einen Eintrag. 
5.	Masken Anpassungen und IS können nur über den Standard weg importiert werden. 

### Generell gilt:

- Das abas System muss einen Zugriff auf das Internet haben hierbei wird der Port 443 sowie der SOAP Port benötigt. 
- Das abas System muss zwingend genügend Ressourcen haben um mindestens 500MB Daten zu speichern. Jede Anfrage an die Creditreform wird hier gespeichert. 
- Das abas System sollte wenn möglich auf der Neusten Java Version sein, ansonsten ist diese nach zu installieren.
- Das abas System sollte immer die folgenden Ordner im Backup beherbergen: „mandantenverzeichnis/java/log„ und „mandantenverzeichnis/owcrefo“ 
- Alle Nutzer der Creditreform Schnittstelle benötigen das recht sich an der Schnittstelle anmelden zu dürfen. Spezielle Nutzer benötigen das Administrations recht, diese Benutzer dürfen ausschließlich anderen Benutzern das Recht erteilen sich an der Schnittstelle anzumelden. 

- - -

## Datenverwaltung / Datenhaltung

Alle Daten welche die Creditreform oder die DRD Schnittstelle betreffen werden im abas System gespeichert.
Hierbei werden DRD Informationen der Schnittstelle in einer Zusatz Datenbank gesichert.
Creditreform Informationen werden gesondert im Filesystem abgelegt. Wir haben eine 3 Monatige Aufbewahrungszeit von der Creditreform vorgegeben bekommen.
Diese halten wir strikt ein. Die Speicherung der Daten der Creditreform erfolgt durch die Erfassung der Ein-/Ausgehenden XML Daten.
Die XML Daten werden in den Ordner OWCREFO geschrieben und lokal auf der Festplatte gesichert.
Die XML Daten sind im Zip Format unter „mandantenverzeichnis ***/owcrefo/xml*** gespeichert.
 
- - -
 
## Projektumfang

### Generelle Klassen

1.	abas.connection.SOAPException
2.	abas.connection.AbstractService
3.	abas.connection.XMLConverterforLogging(XML Convertierung)
4.	abas.connection.Services(Enum für Service Abfrage)

### Testklassen

1.	abas.connection.TempMainServiceMethod

- - -

### Abläufe

#### Login

**Der Technische Ablauf des Logins ist folgender:**
1.	Prüfung der Login Berechtigung des Nutzers
2.	Plausibilitätsprüfung der eigegebenen Daten für alle Textfelder im abas
3.	Sende Vorgang der Validierten Daten einleiten
4.	Daten an die Creditreform senden per SOAP 
5.	Warten auf Antwort der Creditreform währenddessen XML schreiben
6.	Empfangen der Daten der Creditreform währenddessen XML schreiben
7.	Validieren der Daten zu den entsprechenden Feldern im abas
8.	Daten eintragen im Fehlerfall Fehler ausgeben und Abbrechen
9.	Daten im abas Passwortdatensatz Speichern falls Boolfeld gesetzt
10.	Anzeige der Daten und Herunterladen der Keylist
11.	Keylist speichern als XML und in IS schreiben
12.	ENDE der Anfragen

##### Klassen

***Für Das Login werden Folgende Klassen in Java heran gezogen:*** 
1.	de.ck.app.ckcre.cws.infosystem.event.CKCRELOGINEventHandler(IS Eventhandler) 
2.	de.ck.app.ckcre.cws.infosystem.event.Ckcrelogin
3.	de.ck.app.ckcre.cws.infosystem.event.Ckcreloginutil
4.	de.ck.app.ckcre.cws.infosystem.event.ScreensetLogin
5.	de.ck.app.ckcre.cws.infosystem.event.SetLoginData
6.	abas.connection.Login
7.	abas.connection.Logonrequest

#### Passwortänderung

***Das Passwort muss eine Mindestlänge von 6 Zeichen haben. Das Passwort darf maximal 8 Zeichen beinhalten. Der Ablauf ist wie folgt:***
1.	Prüfung der Login Berechtigung des Nutzers
2.	Plausibilitätsprüfung der eigegebenen Daten für alle Textfelder im abas
3.	Sende Vorgang der Validierten Daten einleiten
4.	Daten an die Creditreform senden per SOAP 
5.	Warten auf Antwort der Creditreform währenddessen XML schreiben
6.	Empfangen der Daten der Creditreform währenddessen XML schreiben
7.	Validieren der Daten zu den entsprechenden Feldern im abas
8.	Daten eintragen im Fehlerfall Fehler ausgeben und Abbrechen
9.	Daten im abas Passwortdatensatz Speichern falls Boolfeld gesetzt

##### Klassen

***Diese Klassen werden beim Passwort ändern ausgeführt:***
1.	de.ck.app.ckcre.cws.infosystem.event.CKCRELOGINEventHandler(IS Eventhandler) 
2.	de.ck.app.ckcre.cws.infosystem.event.Ckcrepasswd
3.	de.ck.app.ckcre.cws.infosystem.event.Ckcreloginutil
4.	de.ck.app.ckcre.cws.infosystem.event.ScreensetLogin
5.	de.ck.app.ckcre.cws.infosystem.event.SetLoginData
6.	abas.connection.ChangePassword
7.	abas.connection.Logonrequest

#### Keylist

***Die Keylist kann nur abgerufen werden wenn der Nutzer die Berechtigungen besitzt also Administrator der Anwendung ist. Die Keylist wird nur dazu benötigt Schlüssel einer Version zuzuordnen. Der Ablauf ist wie folgt:***
1.	Prüfung der Login Berechtigung des Nutzers
2.	Plausibilitätsprüfung der eingegebenen Daten für alle Textfelder im abas
3.	Sende Vorgang der Validierten Daten einleiten
4.	Daten an die Creditreform senden per SOAP 
5.	Warten auf Antwort der Creditreform währenddessen XML schreiben
6.	Empfangen der Daten der Creditreform währenddessen XML schreiben
7.	Keylist erstellen und in Tabelle im Abas schreiben
8.	Daten eintragen im Fehlerfall Fehler ausgeben und Abbrechen

##### Klassen

1.	de.ck.app.ckcre.cws.infosystem.event.CKCRELOGINEventHandler(IS Eventhandler) 
2.	de.ck.app.ckcre.cws.infosystem.event.CkcreKeylist
3.	de.ck.app.ckcre.cws.infosystem.event.CkcreKeylistCall
4.	de.ck.app.ckcre.cws.infosystem.event.Ckcreloginutil
5.	de.ck.app.ckcre.cws.infosystem.event.ScreensetLogin
6.	de.ck.app.ckcre.cws.infosystem.event.SetLoginData
7.	abas.connection.Keylist
8.	abas.connection.Logonrequest

#### Mailchange

***Die E-Mail Adresse muss Valide sein ansonsten wird sie von Abas Seite aus bereits nicht zu gelassen.
Der Ablauf ist wie folgt:***
1.	Prüfung der Login Berechtigung des Nutzers
2.	Plausibilitätsprüfung der eigegebenen Daten für alle Textfelder im abas
(Mail Adressen benötigen ein @)
3.	Sende Vorgang der Validierten Daten einleiten
4.	Daten an die Creditreform senden per SOAP 
5.	Warten auf Antwort der Creditreform währenddessen XML schreiben
6.	Empfangen der Daten der Creditreform währenddessen XML schreiben
7.	Validieren der Daten zu den entsprechenden Feldern im abas
8.	Daten eintragen im Fehlerfall Fehler ausgeben und Abbrechen

##### Klassen

1.	de.ck.app.ckcre.cws.infosystem.event.CKCRELOGINEventHandler(IS Eventhandler) 
2.	de.ck.app.ckcre.cws.infosystem.event.Mailchange
3.	de.ck.app.ckcre.cws.infosystem.event.Ckcreloginutil
4.	de.ck.app.ckcre.cws.infosystem.event.ScreensetLogin
5.	de.ck.app.ckcre.cws.infosystem.event.SetLoginData
6.	abas.connection.Mailchange
7.	abas.connection.Logonrequest

#### Suche

***Die Suchfunktion wird bereits von abas Seite aus mit Validen Daten ausgeführt hierzu wird der Kunde benötigt.
Der Ablauf ist wie folgt:***
1.	Prüfung der Login Berechtigung des Nutzers
2.	Plausibilitätsprüfung der eigegebenen Daten für alle Textfelder im abas
3.	Sende Vorgang der Validierten Daten einleiten
4.	Daten an die Creditreform senden per SOAP 
5.	Warten auf Antwort der Creditreform währenddessen XML schreiben
6.	Empfangen der Daten der Creditreform währenddessen XML schreiben
7.	Validieren der Daten zu den entsprechenden Feldern im abas
8.	Daten eintragen im Fehlerfall Fehler ausgeben und Abbrechen
9.	Daten in Tabelle schreiben

##### Klassen

1.	de.ck.app.ckcre.cws.infosystem.event.CKCRESEARCHEventHandler(IS Eventhandler) 
2.	de.ck.app.ckcre.cws.infosystem.event.Search
3.	de.ck.app.ckcre.cws.infosystem.event.CkcreSearchUtil
4.	de.ck.app.ckcre.cws.infosystem.event.CkcreSearchKeylist
5.	de.ck.app.ckcre.cws.infosystem.event.SetCustomer oder SetVendor oder SetProspect
6.	abas.connection.Search
7.	abas.connection.Logonrequest

#### Report

***Über das Infosystem CKCREREPORTS können von mit Informationen der Creditreform automatisch generierte Auskünfte abgerufen werden.
Der Abauf ist wie folgt:***

1.  Prüfung der Loginberechtigung des Nutzers
2.  Eintragen des Kunden (mit hinterlegter Crefoidentnummer) oder alternativ Eintragen der Identnummer direkt
3.  Im Hintergrund werden sowohl Login, wie auch die Productavailability ausgeführt
4.  Auswählen von Berechtigtem Interesse, Sprache und Produkttypen
5.  Abrufen des Reports über den Startknopf
6.  Über den Knopf 'Report zeigen' kann der Report aufgerufen werden
7.  Im Hintergrund wurde der Kunde mit den gelieferten Daten der Creditreform aktualisiert

##### Klassen

1. de.ck.app.ckcre.cws.infosystem.event.head.Bugrund
2. de.ck.app.ckcre.cws.infosystem.event.head.Bulang
3. de.ck.app.ckcre.cws.infosystem.event.head.Buproducttype
4. de.ck.app.ckcre.cws.infosystem.event.head.Start
5. de.ck.app.ckcre.cws.util.AsynchronousKeylist
6. de.ck.app.ckcre.cws.util.AsynchronousLogin
7. de.ck.app.ckcre.cws.util.AsynchronousProductAvailability
8. de.ck.app.ckcre.cws.util.Keylist.kt
9. de.ck.app.ckcre.cws.util.Login.kt
10. de.ck.app.ckcre.cws.util.ProductAvailability
11. de.ck.app.ckcre.cws.infosystem.event.CrefoReportEventHandler
12. de.ck.app.ckcre.cws.infosystem.event.CrefoReportViewEventHandler

#### Rechercheauftrag

***Die E-Mail Adresse muss Valide sein ansonsten wird sie von Abas Seite aus bereits nicht zu gelassen.
Der Ablauf ist wie folgt:***
1.	Prüfung der Login Berechtigung des Nutzers
2.	Plausibilitätsprüfung der eigegebenen Daten für alle Textfelder im abas
3.	Sende Vorgang der Validierten Daten einleiten
4.	Daten an die Creditreform senden per SOAP 
5.	Warten auf Antwort der Creditreform währenddessen XML schreiben
6.	Empfangen der Daten der Creditreform währenddessen XML schreiben
7.	Validieren der Daten zu den entsprechenden Feldern im abas
8.	Daten eintragen im Fehlerfall Fehler ausgeben und Abbrechen
9.	Rückgabe der Referenznummer an den User(Speicherung im Abas

##### Klassen

1.	de.ck.app.ckcre.cws.infosystem.event.CKCREORDERSEventHandler(IS Eventhandler) 
2.	de.ck.app.ckcre.cws.infosystem.event.CheckOrder
3.	de.ck.app.ckcre.cws.infosystem.event.Order
4.	de.ck.app.ckcre.cws.infosystem.event.LogonabasCWS
5.	de.ck.app.ckcre.cws.infosystem.event.SetVorbelegung
6.	abas.connection.MailboxDirectory
7.	abas.connection.Logonrequest

#### Rechercheauftrag abfragen

***Die E-Mail Adresse muss Valide sein ansonsten wird sie von Abas Seite aus bereits nicht zu gelassen.
Der Ablauf ist wie folgt:***
1.	Prüfung der Login Berechtigung des Nutzers
2.	Plausibilitätsprüfung der eigegebenen Daten für alle Textfelder im abas
3.	Sende Vorgang der Validierten Daten einleiten
4.	Daten an die Creditreform senden per SOAP 
5.	Warten auf Antwort der Creditreform währenddessen XML schreiben
6.	Empfangen der Daten der Creditreform währenddessen XML schreiben
7.	Validieren der Daten zu den entsprechenden Feldern im abas
8.	Daten eintragen im Fehlerfall Fehler ausgeben und Abbrechen
9.	Daten in Tabelle schreiben

##### Klassen

1.	de.ck.app.ckcre.cws.infosystem.event.CKCREORDERSEventHandler(IS Eventhandler) 
2.	de.ck.app.ckcre.cws.infosystem.event.CheckOrder
3.	de.ck.app.ckcre.cws.infosystem.event.Mailboxcall
4.	de.ck.app.ckcre.cws.infosystem.event.LogonabasCWS
5.	de.ck.app.ckcre.cws.infosystem.event.SetVorbelegung
6.	abas.connection.MailboxDirectory
7.	abas.connection.Logonrequest

#### Rechercheauftrag aufgeben

***Die E-Mail Adresse muss Valide sein ansonsten wird sie von Abas Seite aus bereits nicht zu gelassen.
Der Ablauf ist wie folgt:***
1.	Prüfung der Login Berechtigung des Nutzers
2.	Plausibilitätsprüfung der eigegebenen Daten für alle Textfelder im abas
(Mail Adressen benötigen ein @)
3.	Sende Vorgang der Validierten Daten einleiten
4.	Daten an die Creditreform senden per SOAP 
5.	Warten auf Antwort der Creditreform währenddessen XML schreiben
6.	Empfangen der Daten der Creditreform währenddessen XML schreiben
7.	Validieren der Daten zu den entsprechenden Feldern im abas
8.	Daten eintragen im Fehlerfall Fehler ausgeben und Abbrechen
9.	Daten vom PDF Ausgeben

##### Klassen

1.	de.ck.app.ckcre.cws.infosystem.event.CKCREORDERSEventHandler(IS Eventhandler) 
2.	de.ck.app.ckcre.cws.infosystem.event.Mailboxentry
3.	de.ck.app.ckcre.cws.infosystem.event.Ckcreloginutil
4.	de.ck.app.ckcre.cws.infosystem.event.ScreensetLogin
5.	de.ck.app.ckcre.cws.infosystem.event.SetLoginData
6.	abas.connection.Mailboxentry
7.	abas.connection.Logonrequest
