/**
 * 
 */
package src.main.java.webservice.de.ck.abas.connection;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

/**
 * @author Christian Schwabe Computerkomplett 
 * 15.04.2019
 * Webservice
 *
 */
public class XMLConverterforLogging {
	public void XMLConvert(Object obj) {
	 try {
		 File file = new File("test.xml");
	        JAXBContext ctx = JAXBContext.newInstance("src.main.java.webservice.https.onlineservice_creditreform_de.webservice._0600_0021");
	        Marshaller marshaller = ctx.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	        marshaller.marshal(obj, file);
	    }
	    catch (Exception e) {
	    	e.printStackTrace();
	     // System.out.println(e.getMessage());
	    }
	}
	
}
