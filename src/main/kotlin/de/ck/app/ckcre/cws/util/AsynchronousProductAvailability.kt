package de.ck.app.ckcre.cws.util

import de.abas.erp.api.session.OperatorInformation
import de.abas.erp.db.DbContext
import de.abas.erp.db.EditorAction
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReport
import de.abas.erp.db.schema.company.PasswordEditor
import de.ck.abas.connection.Logonrequest
import de.ck.abas.connection.Services
import de.ck.app.ckcre.cws.infosystem.event.CrefoReportEventHandler
import de.ck.app.ckcre.cws.infosystem.event.head.Start
import https.onlineservice_creditreform_de.webservice._0600_0021.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * TODO: Insert Description!
 * Project: Creditreform
 * Package: de.ck.app.ckcre.cws.util
 * Created: 16.04.2019 15:40
 * @author = manuel.ott
 * @since = 16. April 2019
 */
object AsynchronousProductAvailability {

    @JvmStatic
    fun doAsynchronousProdcutAvailability(ctx: DbContext, head: CrefoReport, force: Boolean = false){
        Constants.ctx = ctx
        LoggerUtil.initialize(ctx)

        val l = LoggerUtil.LOG

        if(CrefoReportEventHandler.productAvailability == null || force) {

            var edi: PasswordEditor? = null
            var pwlist: List<String>? = null
            try{
                l.info("Öffne Editor..")
                edi = OperatorInformation(ctx).pwdRecord.createEditor()
                edi.open(EditorAction.VIEW)
                l.info("Passwortdefinition: " + edi.id())

                // Editoren auslesen
                pwlist = listOf(edi.yckcregenpwd, edi.yckcrepw, head.language.toUpperCase(), edi.yckcreuser, edi.yckcrekeylist)
                l.info("Schließe Editor..")
                edi.abort()
            }catch(e: Exception){
                l.error("Fehler beim Auslesen von Passwortdefinition!", e)
            } finally {
                if (edi?.active()?:false) edi!!.abort()
            }

            val crefoident = head.crefonum

            GlobalScope.launch {
                try {
                    l.debug("Creating LogonHeader..")
                    pwlist!!
                    val logon = Logonrequest().buildupLogin(pwlist[0], pwlist[1], pwlist[2], pwlist[3], pwlist[4].toIntOrNull() ?: 21)

                    l.info("Fetching ProductAvailability..")
//                    CrefoReportEventHandler.logon = Services.Logon.start(null, logon) as LogonResponse
                    CrefoReportEventHandler.productAvailability = Services.ProductAvailability.start(Tproductavailabilityrequestbody().apply {
                        this.identificationnumber = crefoident
                    }, logon) as Tproductavailabilityresponsebody
                    l.info("Suchessfully fetched product availability..")
                } catch (e: Exception) {
                    l.error("Fehler bei Produktverfügbarkeit!", e)
                }
            }
        }
    }

}