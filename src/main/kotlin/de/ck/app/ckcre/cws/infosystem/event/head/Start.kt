package de.ck.app.ckcre.cws.infosystem.event.head

import de.abas.eks.jfop.remote.FO
import de.abas.erp.api.session.OperatorInformation
import de.abas.erp.axi2.event.ButtonEvent
import de.abas.erp.axi.screen.ScreenControl
import de.abas.erp.common.type.AbasDate
import de.abas.erp.db.DbContext
import de.abas.erp.db.EditorAction
import de.abas.erp.db.field.Field
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReport
import de.abas.erp.db.schema.company.PasswordEditor
import de.abas.erp.db.schema.customer.Customer
import de.abas.erp.db.schema.customer.CustomerEditor
import de.abas.erp.db.selection.Conditions
import de.abas.erp.db.selection.SelectionBuilder
import de.abas.erp.db.util.QueryUtil
import de.ck.abas.connection.Logonrequest
import de.ck.abas.connection.Services
import de.ck.app.ckcre.cws.util.Constants
import de.ck.app.ckcre.cws.util.Keylist
import de.ck.app.ckcre.cws.util.LoggerUtil
import de.ck.app.ckcre.cws.util.Login
import https.onlineservice_creditreform_de.webservice._0600_0021.*
import org.apache.commons.io.FileUtils
import java.io.File
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import javax.xml.datatype.DatatypeFactory
import javax.xml.datatype.XMLGregorianCalendar

class Start {

    companion object {
        val l = LoggerUtil.LOG

        private fun printFields(clazz: Class<*>, instance: Any, depth: Int){
            clazz.declaredFields.forEach {
                try{
                    var x: Any? = null
                    var cont = true
                    try{
                        x = clazz.methods.first {method -> method.name.contains(it.name, ignoreCase = true) }.invoke(instance)
                    }catch(e: Exception){
                        try{
                            x = it.get(instance)
                        }catch(e: Exception){
//                            l.warn("Keine Methode/AccessibleField gefunden für Feld ${it.name}")
                            cont = false
//                            return
                        }
                    }

                    var s = ""
                    repeat(depth, {s += '\t'})
                    if(x != null){
                        l.debug("$s Field: ${it.type.simpleName} ${it.name} = $x")
                        if(!it.type.isPrimitive && !it.type.isArray && !it.type.isEnum && cont) printFields(it.type, x, depth+1)
                    }
                }catch(e: Exception){l.warn("WARN DEBUG", e)}
            }
        }

        @JvmStatic
        fun startAfter(event: ButtonEvent, screenControl: ScreenControl, ctx: DbContext, head: CrefoReport) {
            l.info(event.toString())
            l.info("Requesting report..")

            var edi: PasswordEditor? = null

            try{
                l.info("Öffne Editor..")
                edi = OperatorInformation(ctx).pwdRecord.createEditor()
                edi.open(EditorAction.VIEW)
                l.info("Passwortdefinition: " + edi.id())

                // Editoren auslesen
                val pwlist = listOf(edi.yckcregenpwd, edi.yckcrepw, head.language.toUpperCase(), edi.yckcreuser, edi.yckcrekeylist )

                edi.abort()

                val logon = Logonrequest().buildupLogin(pwlist[0], pwlist[1], pwlist[2], pwlist[3],pwlist[4].toIntOrNull()?:21)

                val leinkey = Keylist.findKeyByDesignation(Constants.keylist, head.legitimateinterest, head.language)
                val prtykey = Keylist.findKeyByDesignation(Constants.keylist, head.producttype, head.language)

                Services.Report.start( Treportrequestbody().apply{
                    this.identificationnumber = head.crefonum
                    if(head.kunde != null) this.customerreference = head.kunde?.idno
                    this.reportlanguage = Tlanguagerequest.fromValue(head.language)

                    if(head.extendedmonitoring){
                        extendedmonitoring = Treportrequestbody.Extendedmonitoring().apply {
                            isExtendedmonitoringplus = head.isMonitoringplus
                            if(head.dmonitoring != null)
                                endofextendedmonitoring = DatatypeFactory.newInstance().newXMLGregorianCalendar( GregorianCalendar().apply{time = head.dmonitoring.toDate()} )
                        }
                    }

                    this.legitimateinterest = leinkey
//                    head.legitimateinterest
                    this.producttype = prtykey
//                    this.producttype = head.producttype
                }, logon).apply {
                    l.info("Loading Response..")
                    with((this as ReportResponse).body){
                        val pdf = File("""owcrefo/pdf/${head.crefonum}_${prtykey}_${LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))}.pdf""")
                        l.info("Writing PDF-File: ${pdf.absolutePath}")
                        FileUtils.writeByteArrayToFile(pdf, reportdata.textreport.inputStream.readBytes())
                        head.pdfpath = pdf.absolutePath

                        FO.box("Erfolg!", "Report wurde erfolgreich generiert!\nPDF: ${head.pdfpath}")

                        //TODO: Kundendaten aktualisieren!
                        l.info("saving customerdata..")
                        try{
                            this.let { resp ->

                                //Prüfen, ob Report vorhanden!
                                if(resp.isNegativereport){
                                    resp.reportdata.negativereport.apply {
                                        l.warn("Negativreport!")
                                        l.warn(this.solvencyindexmeaning)
                                        this.note.paragraph.forEach(l::warn)
                                        this.recommendation.paragraph.forEach(l::warn)
                                        return
                                    }
                                }

                                QueryUtil.getFirst(ctx, SelectionBuilder.create(Customer::class.java).add(Conditions.eq(Customer.META.yckcreident, head.crefonum)).build()).createEditor().apply {
                                   try{
                                       this.open(EditorAction.MODIFY)
                                       l.info("Aktualisiere Kunden ${this.id}: ${this.descr}")

                                       updateField(this, Customer.META.yckcrecrefonum, resp.identificationnumber)

                                       var tmp: Any? = if(resp.reportdata?.solvencyindex?.isSolvencyindextwozerosupported?:false)
                                           resp.reportdata?.solvencyindex?.solvencyindextwozero
                                       else if (resp.reportdata?.trafficlightsolvency?.solvencyindexrange?.isNotBlank()?:false)
                                           resp.reportdata?.trafficlightsolvency?.solvencyindexrange
                                       else resp.reportdata.solvencyindex?.solvencyindexone
                                       if(updateField(this, Customer.META.yckcreboniidx, tmp))
                                           yckcredboniidx = AbasDate()

                                       if(updateField(this, Customer.META.yckcrebilboni, resp.reportdata?.balancesolvency?.clazz))
                                            yckcredbilboni = AbasDate()
                                       if(updateField(this, Customer.META.yckcreboniklasse, resp.reportdata?.solvencyclass?.solvencyclassmeaning))
                                           yckcredboniklasse = AbasDate()

                                       updateField(this, Customer.META.yckcrerisikobewkey, resp.reportdata?.riskjudgement?.colour?.key)
                                       updateField(this, Customer.META.yckcrerisikobewtxt, resp.reportdata?.riskjudgement?.meaning)

                                       l.debug("Trafficlightsolvency: ${resp.reportdata?.trafficlightsolvency?.colour?.grade}")
                                       l.debug("Mögliche Werte: (10, 20, 30)")
                                       tmp = if(resp.reportdata.trafficlightsolvency?.colour?.grade == 30L) "icon:ball_red"
                                       else if(resp.reportdata.trafficlightsolvency?.colour?.grade == 20L) "icon:ball_yellow"
                                       else if(resp.reportdata.trafficlightsolvency?.colour?.grade == 10L) "icon:ball_green"
                                       else null
                                       updateField(this, Customer.META.yckcreboniampel1, tmp)

                                       l.debug("Solvencyindex.trafficlight: ${resp.reportdata?.solvencyindex?.trafficlight?.grade}")
                                       l.debug("Mögliche Werte: (10, 20, 30)")
                                       tmp = if(resp.reportdata?.solvencyindex?.trafficlight?.grade == 30L) "icon:ball_red"
                                       else if(resp.reportdata?.solvencyindex?.trafficlight?.grade == 20L) "icon:ball_yellow"
                                       else if(resp.reportdata?.solvencyindex?.trafficlight?.grade == 10L) "icon:ball_green"
                                       else null
                                       updateField(this, Customer.META.yckcreboniampel1, tmp)
//
//                                       l.debug("Paymentbehaviour.trafficlight: ${resp.reportdata?.paymentbehaviour?.trafficlight?.grade}")
//                                       l.debug("Mögliche Werte: (10, 20, 30)")
//                                       tmp = if(resp.reportdata?.paymentbehaviour?.trafficlight?.grade == 30L) "icon:ball_red"
//                                       else if(resp.reportdata?.paymentbehaviour?.trafficlight?.grade == 20L) "icon:ball_yellow"
//                                       else if(resp.reportdata?.paymentbehaviour?.trafficlight?.grade == 10L) "icon:ball_green"
//                                       else null
//                                       updateField(this, Customer.META.yckcreboniampel1, tmp)

                                       l.debug("Balancesolvency: ${resp.reportdata?.balancesolvency?.trafficlight?.grade}")
                                       l.debug("Mögliche Werte: (10, 20, 30)")
                                       tmp = if(resp.reportdata.balancesolvency?.trafficlight?.grade == 30L) "icon:ball_red"
                                       else if(resp.reportdata.balancesolvency?.trafficlight?.grade == 20L) "icon:ball_yellow"
                                       else if(resp.reportdata.balancesolvency?.trafficlight?.grade == 10L) "icon:ball_green"
                                       else null
                                       updateField(this, Customer.META.yckcreboniampel2, tmp)

                                       updateField(this, Customer.META.yckcrekreditlimit, resp.reportdata?.creditopinion?.creditlimit?.value)

                                       /*
                                            TODO:
                                                Brancheninformationen aktualisieren
                                        */
                                        try{
                                            resp.reportdata?.branch?.typeofbranch?.forEach {
                                                it?.branch?.forEach {
                                                    updateField(this, Customer.META.yckcrebranchcode, it?.code)
                                                    updateField(this, Customer.META.yckcrebranchname, it?.description)
                                                }
                                            }
                                        }catch(e: Exception){
                                            l.error("Fehler beim Aktualisieren der Brancheninformationen", e)
                                        }



//                                       //Datum von Report übernehmen
//                                       l.debug("Setze Datum des letzten Reports..")
//                                       yckcredatumreport = AbasDate.valueOf( LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")) )

//                                       printFields(resp::class.java, resp, 0)

                                       this.commit()
                                   }catch(e: Exception){
                                       l.error(e); throw e
                                   }finally{
                                       if(this.active()) abort()
                                    }
                                }
                            }
                        }catch(e: Exception){
                            l.error("Fehler beim Aktualisieren der Kundenstammdaten!", e)
                        }
                    }
                }
            }catch(e: Exception) {
                l.error("Fehler beim Laden des Reports!", e)
                try {
                    val fi = e::class.java.getMethod("getFaultInfo").invoke(e) as Tservicefault
                    l.error("${fi.body.errorkey.key} - ${fi.body.errorkey.designation}")
                    fi.body.fault.forEach { l.error(it.errorkey.key + " - " + it.errorkey.designation) }
                    FO.box("Fehler!", fi.body.fault.joinToString { it.errorkey.key + " - " + it.errorkey.designation + "\n" })
                } catch (ex2: Exception) {
                    l.error("Fehler beim Laden der Fehlernachricht des Webervices!", ex2)
                    l.error("Fehler beim Laden der Fehlernachricht des Webervices!", e)
                    FO.box("Fehler!", "$e: ${e.message}")
                }
            }
        }

        @JvmStatic
        fun updateField(dataset: CustomerEditor, field: Field, value: Any?): Boolean{
            try{
                l.debug("""Prüfe Wert "$value" für Feld ${field.name}""")
                if(value?.toString()?.isEmpty()?:return false) return false
                l.debug("Setze Feld: ${field.name} = $value")
                dataset.setString(field, value.toString())
                return true
            }catch(e: Exception){
                l.error("Fehler beim Updaten von ${field.name}!", e)
                return false
            }
        }

    }

}
