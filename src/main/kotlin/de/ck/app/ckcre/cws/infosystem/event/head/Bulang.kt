package de.ck.app.ckcre.cws.infosystem.event.head

import de.abas.eks.jfop.remote.FO
import de.abas.erp.axi.screen.ScreenControl
import de.abas.erp.axi2.event.ButtonEvent
import de.abas.erp.db.DbContext
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReport
import de.ck.app.ckcre.cws.util.AsynchronousLogin
import de.ck.app.ckcre.cws.util.LoggerUtil
import https.onlineservice_creditreform_de.webservice._0600_0021.Tlanguagerequest

/**
 * TODO: Insert Description!
 * Project: creditreformwcs
 * Package: de.ck.owcrefo.infosystem.event.head
 * Created: 08.02.2019 16:14
 * @author = manuel.ott
 * @since = 08. Februar 2019
 */
object Bulang {

    @JvmStatic
    fun buttonAfter(event: ButtonEvent, screenControl: ScreenControl, ctx: DbContext, head: CrefoReport){
        val menue = Array<String>(https.onlineservice_creditreform_de.webservice._0600_0021.Tlanguagerequest.values().size + 1, {"Sprache wählen"})
        for(i in 0 until https.onlineservice_creditreform_de.webservice._0600_0021.Tlanguagerequest.values().size) menue[i + 1] = https.onlineservice_creditreform_de.webservice._0600_0021.Tlanguagerequest.values()[i].value()
        head.language = menue[FO.menue(menue)]

        //Login aktualisieren
        LoggerUtil.LOG.info("forcing login update..")
        AsynchronousLogin.doAsynchronousLogin(ctx, head, true)

    }

}