package de.ck.app.ckcre.cws.util

import de.abas.erp.api.session.OperatorInformation
import de.abas.erp.db.DbContext
import de.abas.erp.db.EditorAction
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReport
import de.abas.erp.db.schema.company.PasswordEditor
import de.ck.abas.connection.Logonrequest
import de.ck.abas.connection.Services
import de.ck.app.ckcre.cws.infosystem.event.CrefoReportEventHandler
import de.ck.app.ckcre.cws.infosystem.event.head.Start
import https.onlineservice_creditreform_de.webservice._0600_0021.LogonResponse
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * TODO: Insert Description!
 * Project: creditreformwcs
 * Package: de.ck.app.ckcre.cws.util
 * Created: 26.03.2019 18:02
 * @author = manuel.ott
 * @since = 26. März 2019
 */
object AsynchronousLogin {

    @JvmStatic
    fun doAsynchronousLogin(ctx: DbContext, head: CrefoReport, force: Boolean = false){
        Constants.ctx = ctx
        LoggerUtil.initialize(ctx)

        val l = LoggerUtil.LOG

        if(CrefoReportEventHandler.logon == null || force) {

            var edi: PasswordEditor? = null
            var pwlist: List<String>? = null
            try{
                Start.l.info("Öffne Editor..")
                edi = OperatorInformation(ctx).pwdRecord.createEditor()
                edi.open(EditorAction.VIEW)
                Start.l.info("Passwortdefinition: " + edi.id())

                // Editoren auslesen
                pwlist = listOf(edi.yckcregenpwd, edi.yckcrepw, head.language.toUpperCase(), edi.yckcreuser, edi.yckcrekeylist)
                edi.abort()
            }catch(e: Exception){
                l.error("Fehler beim Auslesen von Passwortdefinition!", e)
            } finally {
                if (edi?.active()?:false) edi!!.abort()
            }

            GlobalScope.launch {
                try {
                    l.debug("Creating LogonHeader..")
                    pwlist!!
                    val logon = Logonrequest().buildupLogin(pwlist[0], pwlist[1], pwlist[2], pwlist[3], pwlist[4].toIntOrNull() ?: 21)

                    l.info("Logging in..")
                    CrefoReportEventHandler.logon = Services.Logon.start(null, logon) as https.onlineservice_creditreform_de.webservice._0600_0021.LogonResponse
                    l.info("Suchessfully logged in..")
                } catch (e: Exception) {
                    l.error("Fehler beim Login!", e)
                }
            }
        }
    }
}