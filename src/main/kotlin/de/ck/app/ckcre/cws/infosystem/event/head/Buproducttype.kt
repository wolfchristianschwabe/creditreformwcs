package de.ck.app.ckcre.cws.infosystem.event.head

import de.abas.eks.jfop.remote.FO
import de.abas.erp.api.gui.MenuBuilder
import de.abas.erp.axi.screen.ScreenControl
import de.abas.erp.axi2.event.ButtonEvent
import de.abas.erp.db.DbContext
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReport
import de.ck.abas.connection.Logonrequest
import de.ck.app.ckcre.cws.infosystem.event.CrefoReportEventHandler
import de.ck.app.ckcre.cws.util.*
import java.util.*

/**
 * TODO: Insert Description!
 * Project: creditreformwcs
 * Package: de.ck.owcrefo.infosystem.event.head
 * Created: 08.02.2019 15:33
 * @author = manuel.ott
 * @since = 08. Februar 2019
 */
object Buproducttype {

    @JvmStatic
    fun buttonAfter(event: ButtonEvent, screenControl: ScreenControl, ctx: DbContext, head: CrefoReport){

        val l = LoggerUtil.LOG

        try{
            if(head.language.isEmpty()){
                FO.box("Warnung", "Bitte Sprache wählen!")
                return
            }
            val menu = MenuBuilder<String>(ctx, "Bitte wählen")
            ProductAvailability.getAvailableProductsForService(CrefoReportEventHandler.productAvailability, "report").forEach { k, v -> menu.addItem(k, v) }
//            Login.findValidKeysByKeygroup(CrefoReportEventHandler.logon, "report", "PRTY", head.language).forEach{ (k,v) -> menu.addItem(k, v) }
//            Keylist.findByKeyGroup(Constants.keylist, "PRTY", head.language).filterNot { it.key.matches(Regex(""".*-\d{7,10}"""))}.forEach{ (k,v) -> menu.addItem(k, v) }
            head.producttype = menu.show()
            head.producttype = Keylist.findDesignationByKey(Constants.keylist, head.producttype, head.language)

        }catch(e: Exception){
            l.error("Error displaying PRTY from Keylist!", e)
            FO.box("Warnung!", "ProductAvailability noch nicht geladen!\nLog: ${LoggerUtil.LOGFILE_DEBUG.absolutePath}")
        }


    }
}