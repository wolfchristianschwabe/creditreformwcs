package de.ck.app.ckcre.cws.util

import https.onlineservice_creditreform_de.webservice._0600_0021.ProductavailabilityResponse
import https.onlineservice_creditreform_de.webservice._0600_0021.Tproductavailabilityresponsebody

/**
 * TODO: Insert Description!
 * Project: Creditreform
 * Package: de.ck.app.ckcre.cws.util
 * Created: 16.04.2019 15:49
 * @author = manuel.ott
 * @since = 16. April 2019
 */

object ProductAvailability {

    @JvmStatic
    fun getAvailableProductsForService (productavailability: Tproductavailabilityresponsebody, service: String): Map<String, String>{

        val res = HashMap<String, String>()

        productavailability.service.firstOrNull {
            it.operation.compareTo(service, true) == 0
        }?.product?.forEach {
            res.put(it.producttype.key, it.producttype.designation)
        }?:throw NoSuchServiceException("Service $service hat keine Produkte für #${productavailability.identificationnumber}!")

        return res
    }

}