package de.ck.app.ckcre.cws.util

import https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody
import java.util.*

/**
 * TODO: Insert Description!
 * Project: creditreformwcs
 * Package: de.ck.app.ckcre.cws.util
 * Created: 19.02.2019 16:57
 * @author = manuel.ott
 * @since = 19. Februar 2019
 */
object Keylist {

    @JvmStatic
    fun findByKeyGroup(keylist: https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody.Keylist?, keyGroupName: String, language: String): SortedMap<String, String> {

        keylist?:throw NoKeylistGivenException("""Bitte die Variable 'KEYLIST' füllen!
            |Klasse: ${this::class.java.canonicalName}""".trimMargin())

        val res: HashMap<String, String> = HashMap()

        keylist.keygroup.first {
            it.keygroupname.compareTo(keyGroupName, true) == 0
        }.keyentry.forEach {
            res.put(
                    it.keyattribute.first { it.keyattributename.compareTo("key", true) == 0 }.keyattributevalue.first().value,
                    it.keyattribute.first { it.keyattributename.compareTo("designation", true) == 0 }.keyattributevalue.first { it.language.compareTo(language, true) == 0 }.value)
        }
        return res.toSortedMap(Comparator{ curr, next -> res[curr]!!.compareTo(res[next]!!, true)})
    }

    @JvmStatic
    fun findDesignationByKey(keylist: https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody.Keylist?, key: String, language: String): String {
        keylist?:throw NoKeylistGivenException("""Bitte die Variable 'KEYLIST' füllen!
            |Klasse: ${this::class.java.canonicalName}""".trimMargin())

        keylist.keygroup.forEach {
            it.keyentry.forEach {
                val x = it.keyattribute.firstOrNull { it.keyattributename.compareTo("key") == 0 && it.keyattributevalue.first().value.compareTo(key, true) == 0 }
                if(x != null) return it.keyattribute.firstOrNull {  it.keyattributename.compareTo("designation", true) == 0 }!!.keyattributevalue.first{ it.language.compareTo(language, true) == 0 }.value
            }
        }
        throw NoSuchKeyException("designation zu key $key wurde nicht gefunden!")
    }

    @JvmStatic
    fun findKeyByDesignation(keylist: Tkeylistresponsebody.Keylist?, designation: String, language: String): String {
        keylist ?: throw NoKeylistGivenException("""Bitte die Variable 'KEYLIST' füllen!
            |Klasse: ${this::class.java.canonicalName}""".trimMargin())

        keylist.keygroup.forEach {
            it.keyentry.forEach {
                val x = it.keyattribute.firstOrNull { it.keyattributename.compareTo("designation") == 0 && it.keyattributevalue.any { it.value.compareTo(designation, true) == 0 && it.language.compareTo(language, true) == 0} }
                if (x != null) return it.keyattribute.firstOrNull { it.keyattributename.compareTo("key", true) == 0 }!!.keyattributevalue.first {it.language == null}.value
            }
        }
        throw NoSuchKeyException("key zu designation $designation wurde nicht gefunden!")
    }
}

data class NoKeylistGivenException(val msg: String) : Exception(msg)
data class NoSuchKeyException(val msg: String) : Exception(msg)