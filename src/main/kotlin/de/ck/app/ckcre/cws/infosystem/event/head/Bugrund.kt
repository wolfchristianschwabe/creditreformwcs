package de.ck.app.ckcre.cws.infosystem.event.head

import de.abas.eks.jfop.remote.FO
import de.abas.erp.api.gui.MenuBuilder
import de.abas.erp.axi.screen.ScreenControl
import de.abas.erp.axi2.event.ButtonEvent
import de.abas.erp.db.DbContext
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReport
import de.ck.app.ckcre.cws.infosystem.event.CrefoReportEventHandler
import de.ck.app.ckcre.cws.util.LoggerUtil
import de.ck.app.ckcre.cws.util.Login

/**
 * TODO: Insert Description!
 * Project: creditreformwcs
 * Package: de.ck.owcrefo.infosystem.event.head
 * Created: 08.02.2019 17:02
 * @author = manuel.ott
 * @since = 08. Februar 2019
 */
object Bugrund {

    @JvmStatic
    fun buttonAfter(event: ButtonEvent, screenControl: ScreenControl, ctx: DbContext, head: CrefoReport){

        val l = LoggerUtil.LOG

        try{
            if(head.language.isEmpty()){
                FO.box("Warnung", "Bitte Sprache wählen!")
                return
            }
            val menu = MenuBuilder<String>(ctx, "Bitte wählen")
            Login.findValidKeysByKeygroup(CrefoReportEventHandler.logon, "report", "LEIN", head.language).forEach { t, u -> menu.addItem(t, u) }
            head.legitimateinterest = menu.show()
            head.legitimateinterest = Keylist.findDesignationByKey(Constants.keylist, head.legitimateinterest, head.language)

        }catch(e: Exception){
            l.error("Error displaying LEIN from Keylist!", e)
            FO.box("Warnung!", "LoginResponse noch nicht geladen!\nLog: ${LoggerUtil.LOGFILE_DEBUG.absolutePath}")
        }

    }

}