package de.ck.app.ckcre.cws.util

import de.abas.erp.api.session.OperatorInformation
import de.abas.erp.db.DbContext
import de.abas.erp.db.EditorAction
import de.abas.erp.db.schema.company.PasswordEditor
import de.ck.abas.connection.Logonrequest
import de.ck.abas.connection.Services
import de.ck.app.ckcre.cws.infosystem.event.head.Start
import https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * TODO: Insert Description!
 * Project: creditreformwcs
 * Package: de.ck.app.ckcre.cws.util
 * Created: 12.03.2019 11:21
 * @author = manuel.ott
 * @since = 12. März 2019
 */
object AsynchronousKeylist {

    @JvmStatic
    fun getAsynchronnousKeylist(ctx: DbContext) {

        Constants.ctx = ctx
        LoggerUtil.initialize(ctx)

        val l = LoggerUtil.LOG

        if(Constants.keylist == null){

            var edi: PasswordEditor? = null

            Start.l.info("Öffne Editor..")
            edi = OperatorInformation(ctx).pwdRecord.createEditor()
            edi.open(EditorAction.VIEW)
            Start.l.info("Passwortdefinition: " + edi.id())

            // Editoren auslesen
            val pwlist = listOf(edi.yckcregenpwd, edi.yckcrepw, edi.yckcrelang, edi.yckcreuser, edi.yckcrekeylist )
            edi.abort()

            // Nur Keylist laden, wenn Passwort fuer Benutzer auch hinterlegt!!
            if(pwlist.any { it.isNullOrBlank() }) return

            val logon = Logonrequest().buildupLogin(pwlist[0], pwlist[1], pwlist[2], pwlist[3],pwlist[4].toIntOrNull()?:21)

            GlobalScope.launch {
                try{
                    //TODO: Nicht statisch verwenden!! - Passwotdefinition verwenden!
                    l.info("Fetching Keylist.. (${this.coroutineContext})")
                    with((Services.Keylist.start(null, logon) as https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody).keylist){
                        //                with((Services.Keylist.start(null, Logonrequest().buildupLogin("AgPw01!", "CkStS251", operatorCode.yckcrelang, "345128000001", 21)) as Tkeylistresponsebody).keylist){
                        Constants.keylist = this

                        l.info("Loaded Keylist ($version)")
//                        this.keygroup.forEach {
//                            l.info("""Keygroup: ${it.keygroupname}""")
//                            l.info("""Keygrouptype: ${it.keygrouptype}""")
//                            it.keyentry.forEach{
//                                it.keyattribute.forEach { attr ->
//                                    attr.keyattributevalue.forEach {
//                                        l.info("""${attr.keyattributename} = ${it?.value ?: "(null)"}""")
//                                    }
//                                }
//                            }
//                        }
                    }
                }catch(e: Exception){
                    l.error("Fehler beim Laden der Keylist!", e)
                }finally {
                    edi!!.commit()
                    if(edi!!.active()) edi!!.abort()
                    edi = null
                }
            }
        }

    }

}