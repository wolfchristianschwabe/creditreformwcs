package de.ck.app.ckcre.cws.util

import https.onlineservice_creditreform_de.webservice._0600_0021.LogonResponse
import java.util.*
import kotlin.collections.HashMap

/**
 * TODO: Insert Description!
 * Project: creditreformwcs
 * Package: de.ck.app.ckcre.cws.util
 * Created: 26.03.2019 10:20
 * @author = manuel.ott
 * @since = 26. März 2019
 */

object Login{

    @JvmStatic
    fun findValidKeysByKeygroup(logonresponse: https.onlineservice_creditreform_de.webservice._0600_0021.LogonResponse?, service: String = "N/A", keygroup: String = "", language: String): SortedMap<String, String> {

        val l = LoggerUtil.LOG

        logonresponse?:throw UnsucessfulLogonException("Invalid value passed for parameter logonresponse!")

        val res = HashMap<String, String>()

        l.debug("Services:")
        l.debug(logonresponse.body.service.joinToString(" "))

        val serv = logonresponse.body.service.firstOrNull { it.operation.compareTo(service, true) == 0 }

        logonresponse.body.service.firstOrNull { it.operation.compareTo(service, true) == 0 }?.allowedkeys?.forEach {
            it.keyconstraint.filter {
                it.keycontent.key.matches(Regex(""".*$keygroup.*"""))
            }.forEach {
                res.put(it.keycontent.key, it.keycontent.designation)
            }
        }?:throw NoSuchServiceException("Invalid value passed for parameter service!")

        return res.toSortedMap(Comparator{ curr, next -> res[curr]!!.compareTo(res[next]!!, true)})
    }

}

data class UnsucessfulLogonException(val msg: String) : Exception(msg)
data class NoSuchServiceException(val msg: String) : Exception(msg)