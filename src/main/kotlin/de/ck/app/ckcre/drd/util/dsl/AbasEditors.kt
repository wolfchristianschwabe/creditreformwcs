package de.ck.app.ckcre.drd.util.dsl

import de.abas.erp.db.*
import de.ck.app.ckcre.drd.util.Constants
import de.ck.app.ckcre.drd.util.LoggerUtil
import java.security.InvalidParameterException

fun <T : EditorObject> edit(id: EditorFactory<T>? = null, ctx: DbContext = Constants.ctx!!, clazz: Class<T>? = null, f: T.() -> Unit): SelectableObject {
    id ?: clazz ?: throw InvalidParameterException("invalid parameters given!")

    val e: T = if (id == null) ctx.newObject(clazz) else id.createEditor() as T
    try{
        LoggerUtil.getLogger().debug("Editor aktiv?: ${e.active()}")
        if (!e.active())
            e.open(EditorAction.MODIFY)
        e.apply(f).commit()

    }catch (ex: Exception) {
        e.abort()
        throw ex
    }
    return e.objectId()
}

fun <T : SelectableObject> view(id: T, f: T.() -> Unit) = id.apply(f)