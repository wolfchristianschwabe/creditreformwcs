package de.ck.app.ckcre.drd.util.dsl

import de.ck.app.ckcre.drd.template.req.*

//Dynamische Klassen
fun <T: Any> type(clazz: Class<T>, f: T.() -> Unit) = clazz.newInstance().apply(f)

//Siehe: package template.req (*Type)
//KOPF
fun requestdaten(f: DrdDaten.() -> Unit) = DrdDaten().apply(f)

fun extraktiontype(f: ExtraktionType.() -> Unit) = ExtraktionType().apply(f)

fun adressetype(f: AdresseType.() -> Unit) = AdresseType().apply(f)

fun konvertierungsstatistik(f: DrdDaten.Konvertierungsstatistik.() -> Unit) = DrdDaten.Konvertierungsstatistik().apply(f)

fun belege(f: DrdDaten.Belege.() -> Unit) = DrdDaten.Belege().apply(f)
fun belegtype(f: BelegType.() -> Unit) = BelegType().apply(f)

fun debitoren(f:DrdDaten.Debitoren.() -> Unit) = DrdDaten.Debitoren().apply(f)
fun debitortype(f: DebitorType.() -> Unit) = DebitorType().apply(f)