package de.ck.app.ckcre.drd.util

import org.apache.log4j.DailyRollingFileAppender
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.log4j.PatternLayout
import java.io.File

object LoggerUtil {

    @JvmStatic
    val LOGFILE_ERROR by lazy {
        File("java/log/ckcre-drd.err")
//        File("java/log/drd-crefo.${LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))}.log-err")
    }
    @JvmStatic
    val LOGFILE_DEBUG by lazy {
        File("java/log/ckcre-drd.debug")
//        File("java/log/drd-crefo.${LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"))}.log-debug")
    }
    @JvmStatic
    val LOGGER_NAME = "ckcre-drd"

    @JvmStatic
    val out = Constants.ctx!!.out()

    @JvmStatic
    fun getLogger(): Logger = customLogger

    private val customLogger by lazy{
        val res = Logger.getLogger(LOGGER_NAME)
        res.removeAllAppenders()

        //Appenders aufbauen!
        with(res){
            level = Level.ALL
            //Konsole
//            val console = WriterAppender(PatternLayout("%-5p: %m"), Constants.ctx?.out()?.buffered(1024 * 1024)?:System.out.writer())
//            console.threshold = Level.TRACE
//            console.immediateFlush = true
//            console.encoding = "UTF-8"
//            addAppender(console)
            //Debug
            val debug = DailyRollingFileAppender(PatternLayout("%d{yyyy-MM-dd HH:mm:ss} %c{3}.%C{1}:%M %-5p - %m%n"), LOGFILE_DEBUG.path, """'.'yyyy-MM-dd""")
            debug.threshold = Level.DEBUG
            debug.encoding = "UTF-8"
            addAppender(debug)
            //Error
            val error = DailyRollingFileAppender(PatternLayout("""[%-5p] %d{yyyy-MM-dd HH:mm:ss} (%F:%L) %C:%M [Thread: %t, %X] - %m%n"""), LOGFILE_ERROR.path, "'.'yyyy-MM-dd")
            error.threshold = Level.WARN
            error.encoding = "UTF-8"
            addAppender(error)
        }
        return@lazy res
    }
}