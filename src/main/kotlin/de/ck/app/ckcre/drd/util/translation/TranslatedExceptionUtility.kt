package de.ck.app.ckcre.drd.util.translation

import de.abas.jfop.base.buffer.BufferFactory
import de.ck.app.ckcre.drd.util.Constants
import de.ck.app.ckcre.drd.util.LoggerUtil

object TranslatedExceptionUtility {

    val isGerman by lazy{
        try {
            BufferFactory.newInstance(false).globalTextBuffer.getStringValue("bsprache").matches("D".toRegex())
        }catch (e: Exception){
            false
        }}

    @JvmStatic
    fun <T : Exception> createTranslatedException(errorcode: Int, exceptionclazz: Class<T>, extramsg: String?=null): T {
        val ex = TranslatedException.byErrorcode(errorcode)

        //Übersetzen
        val trans = if (isGerman) ex.TEXT_DE else ex.TEXT_EN

        //Return
        try {
            return exceptionclazz.getDeclaredConstructor(String::class.java).newInstance(
                    """$trans
                        | Message: ${extramsg?:"none"}
                        |   Logfile:    ${LoggerUtil.LOGFILE_ERROR}
                        |   Type:       ${ex.name}
                        |   Errorcode:  ${ex.ERRORCODE}
                    """.trimMargin())
        } catch (e: Exception) {
            e.printStackTrace(Constants.ctx!!.out())
            Constants.ctx!!.out().println(e.toString())
            return ExceptionTranslationException("Error while translating Exception!") as T
        }
    }
}