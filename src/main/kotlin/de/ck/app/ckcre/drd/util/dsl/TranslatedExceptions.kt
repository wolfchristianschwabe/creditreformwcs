package de.ck.app.ckcre.drd.util.dsl

import de.ck.app.ckcre.drd.util.translation.TranslatedExceptionUtility
import kotlin.reflect.KClass

infix fun <T: Exception> Class<T>.withErrorCode(errorcode: Int): T{
    return TranslatedExceptionUtility.createTranslatedException(errorcode, this)
}

infix fun <T: Exception> T.withErrorCode(errorcode: Int): T {
    return TranslatedExceptionUtility.createTranslatedException(errorcode, this::class.java, this.localizedMessage)
}

infix fun <T: Exception> KClass<T>.withErrorCode(errorcode: Int): T{
    return TranslatedExceptionUtility.createTranslatedException(errorcode, this.java)
}