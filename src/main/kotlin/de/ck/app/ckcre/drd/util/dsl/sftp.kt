package de.ck.app.ckcre.drd.util.dsl

import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import com.jcraft.jsch.Session

data class SFTPCredentials(val user: String, val password: String, val host: String, val port: Int = 22)

fun sftp(cred: SFTPCredentials, f: ChannelSftp.() -> Unit) {
    var sess: Session? = null
    try {
        sess = JSch().getSession(cred.user, cred.host, cred.port)
        sess.let {
            it.setPassword(cred.password)
            it.setConfig("StrictHostKeyChecking", "no")
            it.connect()
            val sftp = (it.openChannel("sftp") as ChannelSftp)
            try {
                sftp.connect()
            sftp.apply(f)
            }catch(e: Exception){
                sftp.disconnect()
                throw e
            }
        }
    } catch (e: Exception){
        sess?.disconnect()
        throw e
    }
}