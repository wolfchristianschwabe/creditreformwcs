package de.ck.app.ckcre.drd.request

import de.abas.erp.common.type.AbasDate
import de.abas.erp.db.infosystem.standard.op.OITransactions
import de.abas.erp.db.schema.custom.debitorregister.RequestEditor
import de.abas.erp.db.schema.customer.Customer
import de.abas.jfop.base.buffer.BufferFactory
import de.ck.app.ckcre.drd.template.req.*
import de.ck.app.ckcre.drd.util.Constants
import de.ck.app.ckcre.drd.util.Constants.ctx
import de.ck.app.ckcre.drd.util.LoggerUtil
import de.ck.app.ckcre.drd.util.MarshalUtility
import de.ck.app.ckcre.drd.util.dsl.*
import de.ck.app.ckcre.drd.util.translation.CantUseInfosystemException
import de.ck.app.ckcre.drd.util.translation.MarshalFileIOException
import java.io.File
import java.math.BigDecimal
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

object RequestExport{
    val log = LoggerUtil.getLogger()

    private var counter = 1

    fun extractInformation(): DrdDaten? {
        log.info("""Opening Infosystem "Outstanding Items"..""")
        try{
            /*
            -- Zum ausbuchen von OPs:
            Zahlungsobligo / Offene Posten ausbuchen
            18100 Gegenkonto
            i LZ
             */
            ctx!!.openInfosystem(OITransactions::class.java).let { IS ->
                IS.setString(OITransactions.META.von, "-${Constants.CONFIG.get("INTERVAL")}")
                IS.bis = AbasDate(Date())
                IS.optyp85 = false
                log.debug("Zeitraum: ${IS.von} - ${IS.bis}")
                log.info("Invoking Start from IS ${IS.swd}, ${IS.workspaceDir}..")
                IS.invokeStart()

                try {

                    log.debug("Found ${IS.rowCount} lines!")

                    return requestdaten {
                        log.info("adding general data..")
                        extraktion = extraktiontype {
                            log.trace("buchhaltungssytem..")
                            buchhaltungssystem = type(BuchhaltungssystemType::class.java) {
                                version = BufferFactory.newInstance(true).globalTextBuffer.getStringValue("programVersion") //V-13-00
                                value = "abas-ERP"
                            }
                            buchungskreis = "HGB"
                            log.trace("extraktionssoftware..")
                            extraktionssoftware = type(ExtraktionssoftwareType::class.java) {
                                version = BufferFactory.newInstance(true).globalTextBuffer.getStringValue("programVersion") //V-13-00
                                value = "abas-ERP"
                            }
                            extraktionszeitpunkt = calendar {
                                LocalDateTime.now().let{ D ->
                                    day = D.dayOfMonth; month = D.monthValue; year = D.year
                                    hour = D.hour; minute = D.minute; second = D.second
                                }
                            }
                            log.debug("extraktionszeitpunkt: $extraktionszeitpunkt")
                            mandant = BufferFactory.newInstance(true).globalTextBuffer.getStringValue("client")
                            log.debug("mandant: $mandant")
                            mitgliedsnummer = BigDecimal(Constants.CONFIG["MITGLIEDSNUMMER"])
                            sendezeitpunkt = calendar {
                                LocalDateTime.now().let{ D ->
                                    day = D.dayOfMonth; month = D.monthValue; year = D.year
                                    hour = D.hour; minute = D.minute; second = D.second
                                }
                            }
                            log.debug("sendezeitpunkt: $sendezeitpunkt")
                            stichtagOffenePosten = calendar {
                                LocalDateTime.now().let{ D ->
                                    day = D.dayOfMonth; month = D.monthValue; year = D.year
                                    hour = D.hour; minute = 0; second = 0
                                }
                            }
                            log.debug("stichtagOffenePosten: $stichtagOffenePosten")
                        }

                        debitoren = debitoren {
                            log.info("adding debtor data from infosystem..")
                            IS.table().rows.forEach { row ->
                                log.debug("current row: ${row.rowNo} - ${row.top.idno}")
                                //Debitoren nur einmal anhängen, auch bei mehreren Belegen von gleichem Debitor!
                                if(this.debitor.any { it.debitornummer == (row.tobjekt as Customer).idno })
                                    log.debug("skipping debtor.. (already exists!)")
                                else
                                debitor.add(
                                        debitortype {
                                            val deb = row.tobjekt as Customer
                                            debitornummer = deb.idno
                                            amtsgerichtPlz = ""
                                            crefonummer = if(deb.yckcrecrefonum.isNotEmpty()) BigDecimal(deb.yckcrecrefonum) else null
                                            email = deb.emailAddr
                                            fax = type(KommunikationsnummerType::class.java) {
//                                                vorwahl = ""
                                                rufnummer = deb.faxNo
                                            }
                                            adresse = adressetype {
                                                grosskundenadresse = type(GrosskundenadresseType::class.java) {
                                                    ort = deb.town
                                                    land = deb.stateOfTaxOffice?.ctryCode2Char
                                                    plz = deb.zipCode
                                                }
                                                zustelladresse = type(ZustelladresseType::class.java) {
//                                                    hausnummer = ""
                                                    land = deb.stateOfTaxOfficeDescr2?.ctryCode2Char
                                                    ort = deb.town
                                                    plz = deb.zipCode
                                                    strasse = deb.street
                                                }
                                                postfachadresse = type(PostfachadresseType::class.java) {
                                                    land = deb.stateOfTaxOfficeDescr2?.ctryCode2Char
                                                    ort = deb.town
                                                    plz = deb.zipCode
                                                    postfach = ""
                                                }
                                            }
                                            name = type(NameType::class.java) {
                                                deb.addr.split(Regex("""[;|\n]""")).let { name ->
                                                    try{
                                                        name1 = name[0]
                                                        name2 = name[1]
                                                        name3 = name[2]
                                                        name4 = name[3]
                                                    }catch(e: Exception){}
                                                }
                                            }
                                            handelsregisternummer = ""
                                            rechtsform = ""
                                            registerart = ""
                                            mobil = deb.cellPhoneNo
                                            telefon = type(KommunikationsnummerType::class.java) {
                                                rufnummer = deb.phoneNo
//                                                vorwahl = ""
                                            }
                                            saldoOffenePosten = type(BetragType::class.java) {
                                                value = BigDecimal(deb.getRawString("bal")) //saldo //saldo ist skipfeld, deshalb getRawString()
                                                waehrung = BetragWaehrungType.EUR
                                            }
                                            umsatzsteuerID = row.topz.vatIdno
                                            web = deb.webSiteURL
                                        })
                            }
                        }

                        belege = de.ck.app.ckcre.drd.util.dsl.belege {
                            log.info("adding vacant data from infosystem..")

                            IS.table().rows.forEach { row ->
                                log.debug("current row: ${row.rowNo} - ${row.top.idno} - ${row.topztyp.displayString}")

                                //Nur anhängen wenn noch nicht angehängt!
                                if(beleg.any{it.belegnummer == row.tzbeleg && it.debitornummer == (row.tobjekt as Customer).idno})
                                    log.debug("skipping vacant data (already exists!)")
                                else
                                beleg.add(type(BelegType::class.java) {
                                    debitornummer = (row.tobjekt as Customer).idno
                                    belegnummer = row.tzbeleg
                                    buchungsvorgang = row.topztyp.name //TODO: Prüfen!
                                    mahnfreitext = row.tmahntext
                                    mahnschluessel = row.topz.reminder?.swd?:""
                                    mahnsperre = row.topz.reminderLock.toString()
                                    mahnstufe = row.topz.remLev.toString()
                                    zahlungskondition = row.topz.payTerm.descr.let {  d -> if(d.length > 30) d.substring(0, 29) else d }
                                    zahlungsart = row.topz.methodOfPaymt?.name?:""

                                    row.top.openItems.let { op ->
                                        //gem. EMail von DRD!
                                        op.outstInvAmtDomCurr.let { curr ->
                                            op.domCurrOrigInvAmt.let { erfass ->
                                                log.debug("Derzeitiger Restbetrag: $curr")
                                                log.debug("Erfassungsbetrag: ${op.domCurrOrigInvAmt}")

                                                rechnungsbetrag = type(BetragType::class.java) {
                                                    value = if (curr.compareTo(BigDecimal.ZERO) == 0) BigDecimal.ZERO else op.domCurrOrigInvAmt // OP-Ursprungsbetrag
                                                    waehrung = BetragWaehrungType.EUR
                                                    log.debug("rechnungsbetrag  -  $value $waehrung")
                                                }
                                                rechnungsrestbetrag = type(BetragType::class.java) {
                                                    value = if (curr.compareTo(BigDecimal.ZERO) != 0 && curr.compareTo(op.openItems.domCurrOrigInvAmt) != 0) curr else BigDecimal.ZERO
                                                    waehrung = BetragWaehrungType.EUR
                                                    log.debug("rechnugsrestbetrag  -  $value $waehrung")
                                                }

                                            if(curr.compareTo(erfass) != 0) // Nur bei vorhandenen Zahlungen!!
                                            ausgleich = type(AusgleichType::class.java) {
                                                zahlungsbetrag = type(BetragType::class.java) {
                                                    value = rechnungsbetrag.value.minus(rechnungsrestbetrag.value)
                                                    waehrung = BetragWaehrungType.EUR
                                                    log.debug("zahlungsbetrag  -  $value $waehrung")
                                                }
                                                belegnummer = op.ioStateLast.payDoc
                                                datum = calendar {
                                                    op.entDateLast.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().let { D ->
                                                        day = D.dayOfMonth; month = D.monthValue; year = D.year
                                                        hour = D.hour; minute = D.minute; second = D.second
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    buchungsdatum = calendar {
                                        row.tbudat.toDate().toInstant()
                                                .atZone(ZoneId.systemDefault())
                                                .toLocalDateTime().let { D ->
                                                    day = D.dayOfMonth; month = D.monthValue; year = D.year
                                                    hour = D.hour; minute = D.minute; second = D.second
                                                }
                                    }
                                    log.debug("buchungsdatum: $buchungsdatum")
                                    log.debug("adding zahlungsfrist..")
                                    zahlungsfrist = type(ZahlungsfristType::class.java) {
                                        basisdatum = calendar {
                                            row.topz.valDate.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().let { D ->
                                                day = D.dayOfMonth; month = D.monthValue; year = D.year
                                                hour = D.hour; minute = D.minute; second = D.second
                                            }
                                        }
                                        nettofaelligkeitsdatum = calendar {
                                            row.topz.deadlineDay.toDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime().let { D ->
                                                day = D.dayOfMonth; month = D.monthValue; year = D.year
                                                hour = D.hour; minute = D.minute; second = D.second
                                            }
                                        }

                                        log.debug("adding skonto..")
                                        row.topz.payTerm.table().rows.forEach { sk ->
                                            //Bei Skonto entweder Schlüsel oder Tage *und* Prozent!
                                            skonto.add(type(SkontoType::class.java) {
                                                tage = BigDecimal(sk.payCalDays)
                                                prozent = sk.discount
                                            })
                                        }
                                    }
                                    }
                                })
                            }
                        }
//                        IS.close()
                        //TODO: Bugfix? abort() gibt es nicht bei 2016!?
                        IS.abort()
                    }
                }catch(e: Exception){
                    log.error(e)
                    if(IS.active()) IS.close() // IS.abort()
                    throw e
                }
            }
        }catch(e: Exception){
            log.error(CantUseInfosystemException(e.toString()) withErrorCode 601, e)
            return null
        }
    }

    @JvmStatic
    fun createXML(daten: DrdDaten) {
        val tmpF = File("tmp/${Constants.CONFIG.get("MITGLIEDSNUMMER")}_${daten.extraktion.stichtagOffenePosten.toGregorianCalendar().toZonedDateTime().format(DateTimeFormatter.ofPattern("yyyyMMdd"))}_.xml")
        log.info("marshalling to file: ${tmpF.absolutePath}")
        try{
            MarshalUtility.marshal(Constants.XSDRequest, tmpF, daten)
            log.info("saving xml to request-database..")

//            ctx!!.newObject(RequestEditor::class.java).open(EditorAction.NEW).apply {
//                setYckcrefxml(tmpF.bufferedReader())
//                swd = "REQ_${LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))}-$counter"
//                descr = "Response: ${LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"))}\n${tmpF.absolutePath}"
//                yckcredatcreate = AbasDate.valueOf(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))
//                yckcredatend = AbasDate()
//                yckcredatstart = AbasDate.valueOf(LocalDateTime.now().minusDays(Constants.CONFIG.get("INTERVAL")?.toLongOrNull()?:-1L).format(DateTimeFormatter.ofPattern("yyyyMMdd")))
//                yckcredatxml = AbasDate(Date(daten.extraktion.extraktionszeitpunkt.toGregorianCalendar().toInstant().toEpochMilli()))
//                yckcrexmlname = tmpF.absolutePath
//                setFreeText2(LoggerUtil.LOGFILE_DEBUG.bufferedReader())
//                setFreeText(LoggerUtil.LOGFILE_ERROR.bufferedReader())
//                daten.debitoren.debitor.forEach {
//                    table().appendRow().apply {
//                        setString("yckcrecustno", it.debitornummer)
//                        setString("yckcrecustname", it.debitornummer)
//                    }
//                }
//            }.commit()

            val req = edit(clazz = RequestEditor::class.java){
                setYckcrefxml(tmpF.bufferedReader())
                swd = "REQ_${LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))}-$counter"
                descr = "Response: ${LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"))}\n${tmpF.absolutePath}"
                yckcredatcreate = AbasDate.valueOf(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))
                yckcredatend = AbasDate()
                yckcredatstart = AbasDate.valueOf(LocalDateTime.now().minusDays(Constants.CONFIG.get("INTERVAL")?.toLongOrNull()?:-1L).format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                yckcredatxml = AbasDate(Date(daten.extraktion.extraktionszeitpunkt.toGregorianCalendar().toInstant().toEpochMilli()))
                yckcrexmlname = tmpF.absolutePath
                setFreeText2(LoggerUtil.LOGFILE_DEBUG.bufferedReader())
                setFreeText(LoggerUtil.LOGFILE_ERROR.bufferedReader())
                daten.debitoren.debitor.forEach {
                    table().appendRow().apply {
                        setString("yckcrecustno", it.debitornummer)
                        setString("yckcrecustname", it.debitornummer)
                    }
                }
            }
            log.info("created request: ${req.id()}")

            //Auf SFTPServer laden!
            log.info("Lade Datei ${tmpF.absolutePath} auf SFTP-Server (${Constants.SFTPPathRequest}/${tmpF.name})..")
            sftp(cred = Constants.SFTPCredentials){
                this.put(tmpF.inputStream(), Constants.SFTPPathRequest + "/" + tmpF.name)
            }

        }catch(e: Exception){
            log.error(MarshalFileIOException(e.toString()) withErrorCode 2)
        }
    }
}