package de.ck.app.ckcre.drd.events.head

import com.jcraft.jsch.ChannelSftp
import de.abas.eks.jfop.remote.FO
import de.abas.erp.db.schema.custom.debitorregister.Response
import de.ck.app.ckcre.drd.response.ResponseImport
import de.ck.app.ckcre.drd.util.Constants
import de.ck.app.ckcre.drd.util.LoggerUtil
import de.ck.app.ckcre.drd.util.dsl.sftp
import de.ck.app.ckcre.drd.util.dsl.withErrorCode
import de.ck.app.ckcre.drd.util.translation.SFTPResponseException
import java.io.File
import java.util.*

object Buresponse{
    private val log = LoggerUtil.getLogger()

    @JvmStatic
    fun buttonAfter(){
        log.debug("getting server-response (xml)..")
        log.debug("configuration-values:")
        Constants.CONFIG.forEach{
            if(it.key.matches(Regex(".*PASS.*"))) log.trace("${it.key} = ${it.value}")
            else log.debug("${it.key} = ${it.value}")
        }
        log.info("downloading new xml-files from host ${Constants.CONFIG.get("HOST")}")
        try{
            sftp(Constants.SFTPCredentials){
                ls(Constants.SFTPPathResponse).let{
                    log.debug("ls..\n${it.joinToString("\n")}")
                    //TODO: XMLs nach download umbenennen?
            it.filter { !(it as ChannelSftp.LsEntry).attrs.isDir && it.filename.endsWith(".xml", true)}.forEach {
                    //&& Date(it.attrs.aTime.toLong() * 1000).after(Date(Date().time - (1000 * 60 * 60 * 24 * (Constants.CONFIG.get("INTERVAL")?.toLongOrNull()?:7) )))}.forEach {
                        log.info("downloading file ${(it as ChannelSftp.LsEntry).let { it.filename }}\n$it")
                        get("${Constants.SFTPPathResponse}/${it.filename}", File("${Constants.XMLResponseFolder}/${it.filename}").outputStream())
                        this.rename("${Constants.SFTPPathResponse}/${it.filename}", "${Constants.SFTPPathResponse}/${it.filename}.processed")
                    }
                }
            }
        }catch(e: Exception){
            log.error(SFTPResponseException(e.toString()) withErrorCode 301, e)
        }
        log.info("processing files..")
        var response: Response? = null
        val responses = LinkedList<Response>()
        Constants.XMLResponseFolder.let {
            log.debug("files found in ${Constants.XMLResponseFolder}:\n${it.list().joinToString("\n")}")
            it.listFiles().filter { it.isFile && it.extension.matches(Regex("""(?i)xml""")) }.forEach{cF ->
                log.debug("processing file ${cF.absolutePath}..")
                response = ResponseImport.doImport(cF)
                responses.add(response!!)
            }
        }
        if(!responses.isNullOrEmpty())
            FO.box("Fertig!", "Import beendet!\n${responses.joinToString(separator = "\n")}")
    }
}