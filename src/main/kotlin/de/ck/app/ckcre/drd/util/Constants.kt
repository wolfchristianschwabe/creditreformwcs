package de.ck.app.ckcre.drd.util

import de.abas.erp.db.DbContext
import de.abas.erp.db.schema.valueset.Value
import de.abas.erp.db.selection.Conditions
import de.abas.erp.db.selection.SelectionBuilder
import de.ck.app.ckcre.drd.util.dsl.SFTPCredentials
import de.ck.app.ckcre.drd.util.dsl.withErrorCode
import de.ck.app.ckcre.drd.util.translation.CantFindConfigurationException
import java.io.File

object Constants{
    @JvmStatic
    val XMLResponseFolder = File("owcrefo/xml")
    @JvmStatic
    val XMLRequest = File("tmp/XCREFO.DRD.REQUEST.XML")
    @JvmStatic
    val XSDResponse = File("owcrefo/xsd/DRDRuecklieferungSchemaErw-v1.1.xsd")
    @JvmStatic
    val XSDRequest = File("owcrefo/xsd/DRDSchema-v1.0.xsd")

    @JvmStatic
    val SFTPPathRequest = "drd-einlieferung"
    @JvmStatic
    val SFTPPathResponse = "drd-ruecklieferung"

    @JvmStatic
    val CONFIG by lazy{
        val log = LoggerUtil.getLogger()
        log.info("processing configuration..")
        try{
            val res = HashMap<String, String>()
            ctx!!.createQuery(SelectionBuilder.create(Value::class.java).add(Conditions.eq(Value.META.valSetId.name, "CKCRECONFIG")).build()).execute().forEach {
                res.put(it.ident.descr, it.`val`)
            }
            return@lazy res
        }catch(e: Exception){
            log.error(CantFindConfigurationException("@gruppe=(ValueSetIdentifier);swd==CKCRECONFIG") withErrorCode 401)
            error("Error while reading configuration from client(abas)! Logfile: ${LoggerUtil.LOGFILE_ERROR.absolutePath}")
        }
    }

    val SFTPCredentials by lazy{
        listOf("HOST", "USERNAME", "PASSWORD", "PORT").forEach { if(CONFIG.get((it)).isNullOrEmpty()) LoggerUtil.getLogger().warn("Value '$it' was not found!!")}
        SFTPCredentials(host=CONFIG.get("HOST")!!, user=CONFIG.get("USERNAME")!!, password=CONFIG.get("PASSWORD")!!, port=CONFIG.get("PORT")?.toIntOrNull()?:22)
    }

    @JvmStatic
    var ctx: DbContext? = null
}