package de.ck.app.ckcre.drd.events.head

import de.abas.eks.jfop.remote.FO
import de.ck.app.ckcre.drd.request.RequestExport
import de.ck.app.ckcre.drd.util.LoggerUtil

object Burequest{
    private val log = LoggerUtil.getLogger()

    @JvmStatic
    fun buttonAfter(){
        try{
            RequestExport.createXML(RequestExport.extractInformation()!!)
            FO.box("Fertig", "Export beendet!")
        }catch(e: Exception){
            log.error("Fehler beim Exportieren der offenen Posten!", e)
            FO.box("Fehler", "Fehler beim Export!")
        }
    }
}