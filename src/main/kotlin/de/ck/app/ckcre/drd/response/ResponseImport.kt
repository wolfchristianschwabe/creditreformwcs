package de.ck.app.ckcre.drd.response

import de.abas.erp.common.type.AbasDate
import de.abas.erp.common.type.AbasDateTime
import de.abas.erp.db.SelectableObject
import de.abas.erp.db.schema.custom.debitorregister.Response
import de.abas.erp.db.schema.custom.debitorregister.ResponseEditor
import de.abas.erp.db.schema.customer.Customer
import de.abas.erp.db.selection.Conditions
import de.abas.erp.db.selection.SelectionBuilder
import de.abas.erp.db.util.QueryUtil
import de.ck.app.ckcre.drd.template.resp.DrdDebitorTypeErw
import de.ck.app.ckcre.drd.template.resp.DrdRuecklieferungErw
import de.ck.app.ckcre.drd.util.Constants
import de.ck.app.ckcre.drd.util.LoggerUtil
import de.ck.app.ckcre.drd.util.UnmarshalUtility
import de.ck.app.ckcre.drd.util.dsl.edit
import de.ck.app.ckcre.drd.util.dsl.withErrorCode
import de.ck.app.ckcre.drd.util.translation.CantCreateResponseException
import de.ck.app.ckcre.drd.util.translation.CantEditCustomerException
import de.ck.app.ckcre.drd.util.translation.UnmarshalException
import java.io.File
import java.io.FileReader
import java.io.Reader
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.format.DateTimeFormatter

object ResponseImport {

    private var counter = 1
    private var parsedResponse: DrdRuecklieferungErw? = null

    @JvmStatic
    fun doImport(xml: File): Response? {
        val log = LoggerUtil.getLogger()
        try{
            log.debug("creating (Response) from ${xml.absolutePath}..")
            log.debug("checking, if response is already created..")
            var nSwd = "RES_${LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))}-$counter"
            Constants.ctx!!.createQuery(SelectionBuilder.create(Response::class.java).add(Conditions.eq("swd", nSwd)).build()).execute().forEach {
                nSwd = "RES_${LocalDate.now().format(DateTimeFormatter.ofPattern("yyyyMMdd"))}-X"
                log.warn("found existing response: $it ${it.swd}")
                log.warn("swd for new response will be set to $nSwd!")
            }
            parsedResponse =
            try{
                log.debug("trying to unmarshal ${xml.absolutePath}..")
                UnmarshalUtility.unmarshal(Constants.XSDResponse, xml, DrdRuecklieferungErw::class.java)
            }catch(e: Exception){
                log.error(UnmarshalException("File: ${xml.absolutePath}") withErrorCode 1)
                throw e
            }

            val debtors = HashMap<Customer, DrdDebitorTypeErw>()

            val id: SelectableObject =
            edit(clazz = ResponseEditor::class.java){
                swd = nSwd
                descr = "Response: ${LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss"))}\n${xml.name}"
                yckcredatcreate = AbasDate.valueOf(LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss")))
                yckcredatend = AbasDate()
                yckcredatstart = AbasDate.valueOf(LocalDateTime.now().minusDays(Constants.CONFIG.get("INTERVAL")?.toLongOrNull()?:-1L).format(DateTimeFormatter.ofPattern("yyyyMMdd")))
                parsedResponse!!.datum.let {
                    yckcredatxml = AbasDateTime.valueOf("${it.year}${if(it.month < 10) "0" else ""}${it.month}${if(it.day < 10) "0" else ""}${it.day}${if(it.hour < 10)"0" else ""}${it.hour}${if(it.minute < 10)"0" else ""}${it.minute}${if (it.second < 10)"0" else ""}${it.second}")
                }
                yckcrexmlname = xml.name
                setYckcrefxml(FileReader(xml) as Reader)

                log.debug("processing debtors/customers..")
                parsedResponse!!.drdDebitoren.drdDebitor.forEach {deb ->
                    try{
                        log.trace("adding debtor ${deb.debitornummer}")
                        table().appendRow().let {
                            it.setString(Response.Row.META.yckcrecustno, deb.debitornummer)
                            it.setString(Response.Row.META.yckcrecustname, deb.debitornummer)
                        }
                        debtors.put(QueryUtil.getFirst(Constants.ctx!!, SelectionBuilder.create(Customer::class.java).add(Conditions.eq(Customer.META.idno, deb.debitornummer)).build()), deb)
                    }
                    catch(e: Exception){
                        log.warn("failed to append debtor ${deb.debitornummer}")
                    }
                }
            }

            //DEBITOREN aktualisieren
            debtors.entries.forEach { (cust, deb) ->
                try{
                    edit(cust){
                        yckcreinso = deb.inso
                        yckcrebranchcode = deb.branchenCode
                        yckcrebranchname = deb.branchenBezeichnung
                        yckcrebranchtype = deb.branchenArt
                        yckcrebranchcount = deb.branchenLand
                        yckcredrdindex = deb.drdIndex?.toString()?:""
                        yckcredebnum = deb.debitornummer
                        yckcrememberno = deb.mitgliedsnummer
                        yckcrecrefonum = deb.crefonummer?.toString()?:""
                        yckcrelastresp = id as Response
                    }
                }catch (e: Exception){
                    log.error(CantEditCustomerException(e.toString()) withErrorCode 505, e)
                }
            }

            counter++ //Counter erhöhen f+r nächsten Import!
            log.info("created (Response): ${id.swd} $id")
            File("${xml.parentFile.absolutePath}/${xml.nameWithoutExtension}.processed").let {
                log.debug("moving file ${xml.absolutePath} to ${it.absolutePath}..")
                xml.renameTo(it)
            }
            log.debug("transferring logfiles to response..")
            log.debug("casting class ${id::class.java} to ${Response::class.java}..")
            log.debug(id as Response)

            edit(id as Response){
                setFreeText(FileReader(LoggerUtil.LOGFILE_ERROR))
                setFreeText2(FileReader(LoggerUtil.LOGFILE_DEBUG))
            }
            log.info("finished response! - no errors!")
            return id
        }catch(e: Exception){
            log.error(CantCreateResponseException("File: ${xml.absolutePath}") withErrorCode 501, e)
        }
        return null
    }
}