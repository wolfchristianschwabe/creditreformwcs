package de.ck.app.ckcre.drd.events.head

import de.abas.erp.axi.screen.ScreenControl
import de.abas.erp.db.DbContext
import de.abas.erp.db.field.Field
import de.abas.erp.db.infosystem.custom.owcrefo.DRDCrefo
import de.abas.erp.db.schema.custom.debitorregister.Response
import de.abas.erp.db.selection.ExpertSelection
import de.abas.jfop.base.buffer.BufferFactory
import de.ck.app.ckcre.drd.response.ResponseParser
import de.ck.app.ckcre.drd.template.resp.DrdRuecklieferungErw
import de.ck.app.ckcre.drd.util.Constants
import de.ck.app.ckcre.drd.util.LoggerUtil
import de.ck.app.ckcre.drd.util.dsl.withErrorCode
import de.ck.app.ckcre.drd.util.translation.*
import java.io.File

/**
 * Klasse zum handlen von Kopfvariable Start (Button)
 */
object Start{
    private val log = LoggerUtil.getLogger()

    /**
     * Event: FBN
     * buttonnach
     */
    @JvmStatic
    fun buttonAfter(screenControl: ScreenControl, ctx: DbContext, head: DRDCrefo) {

        log.debug("clearing table...")
        head.table().clear()

        // ScreenScopes setzen! - siehe Reiter Optionen in Infosystem!
        log.debug("creating maskcontext..")
        with(DRDCrefo.META){
            BufferFactory.newInstance(false).printBuffer.setValue("maskkontextfop",
                    createScreenContext(head,
                            listOf(showlief, showpool, showbranch, showzusinfo,
                                    showm2x, showm4x, showm8x, showm12x),
                            listOf("LIEF", "POOL", "BRA", "ZUS",
                                    "M2", "M4", "M8", "M12")))
        }

        log.debug("maskkontextfop: " + BufferFactory.newInstance(false).printBuffer.getStringValue("maskkontextfop"))

        //TODO: Selektion einbauen!

        val sel = ExpertSelection.create(Response::class.java, "@zeilen=ja;yckcrecustno==${head.kunde?.idno?:""};yckcredatxml=${head.datvom?:""}!${head.datbis?:"."}")
        log.debug("Selektion: ${sel.type}:${sel.tableDesc}:${sel.criteria}")

        ctx.createQuery(sel).sortedBy { it.yckcredatxml }.forEach{ resp ->
            val currTmpFile = File.createTempFile("tmp/X.CKCRE.XML", ".TMP")
            log.info("Parsing - XML: ${currTmpFile.absolutePath}, XSD: ${Constants.XSDResponse}")
            log.debug("reading xml from freetext..")
            val tmp = currTmpFile.printWriter().buffered()
            resp.getYckcrefxml(tmp)
            tmp.flush()
            currTmpFile.forEachLine { log.debug(it) }
            var response: DrdRuecklieferungErw?=null
            try {
                response = ResponseParser.parse(Constants.XSDResponse, currTmpFile)
                log.debug(
                        """Parsed Response:
                |${response.datum}
                |${response.drdDebitoren.drdDebitor.joinToString { "Debitornummer: ${it.debitornummer}: ${it.name.name1}\n" }}
            """.trimMargin())
            } catch (e: Exception) {
                log.error(MarshalFileIOException(e.toString()) withErrorCode 1, e)
            }

            response?: error("""Error! See logfile "${LoggerUtil.LOGFILE_ERROR}" for further information!""")

            log.debug("appending infosystem rows..")
            try{
                //Tabelle füllen
                response.drdDebitoren.drdDebitor.filter { it.debitornummer.compareTo(head.kunde?.idno?:it.debitornummer,true) == 0 }.forEach {
                    with(DRDCrefo.Row.META){
                        fillRow(head.table().appendRow(),
                                listOf(debnum, debsuch, datum,
                                        mitgliedsnummer, mandant, buchungskreis,
                                        crefonummer, debitornummer, betragsvolumen,
                                        anzcrefosbranch, tagediffbranche, tageisbranche,
                                        tagesollbranche, betragbranche, anzbelegebranch,
                                        m12anzbelege, m12anzbelegepool, m12betraglieferant,
                                        m12betragpool, m12tagesoll, m12tagesollpool,
                                        m12tageist, m12tagediff, m12tagediffpool,
                                        m8anzbelege, m8anzbelegepool, m8betraglieferant,
                                        m8betragpool, m8tagesoll, m8tagesollpool,
                                        m8tageist, m8tagediff, m8tagediffpool,
                                        m4anzbelege, m4anzbelegepool, m4betraglieferant,
                                        m4betragpool, m4tagesoll, m4tagesollpool,
                                        m4tageist, m4tagediff, m4tagediffpool,
                                        m2anzbelege, m2anzbelegepool, m2betraglieferant,
                                        m2betragpool, m2tagesoll, m2tagesollpool,
                                        m2tageist, m2tagediff, m2tagediffpool),
                                listOf(it.debitornummer, it.debitornummer, with(response.datum){"$day.$month.$year"},
                                        it.mitgliedsnummer, it.mandant, it.buchungskreis,
                                        it.crefonummer, it.debitornummer, it.betragsvolumenOP?.value,
                                        it.anzahlCrefosBranche, it.tageDiffBranche, it.tageIstBranche,
                                        it.tageSollBranche, it.betragBranche?.value, it.anzahlBelegeBranche,
                                        it.anzahlBelegeLieferant12, it.anzahlBelegePoolOhneLieferant12, it.betragLieferant12?.value,
                                        it.betragPoolOhneLieferant12?.value, it.tageSollLieferant12, it.tageSollPoolOhneLieferant12,
                                        it.tageIstLieferant12, it.tageDiffLieferant12, it.tageDiffLieferant12,
                                        it.anzahlBelegeLieferant8, it.anzahlBelegePoolOhneLieferant8, it.betragLieferant8?.value,
                                        it.betragPoolOhneLieferant8?.value, it.tageSollLieferant8, it.tageSollPoolOhneLieferant8,
                                        it.tageIstLieferant8, it.tageDiffLieferant8, it.tageDiffLieferant8,
                                        it.anzahlBelegeLieferant4, it.anzahlBelegePoolOhneLieferant4, it.betragLieferant4?.value,
                                        it.betragPoolOhneLieferant4?.value, it.tageSollLieferant4, it.tageSollPoolOhneLieferant4,
                                        it.tageIstLieferant4, it.tageDiffLieferant4, it.tageDiffLieferant4,
                                        it.anzahlBelegeLieferant2, it.anzahlBelegePoolOhneLieferant2, it.betragLieferant2?.value,
                                        it.betragPoolOhneLieferant2?.value, it.tageSollLieferant2, it.tageSollPoolOhneLieferant2,
                                        it.tageIstLieferant2, it.tageDiffLieferant2, it.tageDiffLieferant2)
                        )
                    }
                }
            }catch(e: Exception){
                log.error(InfosystemException(e.toString()) withErrorCode 101, e)
            }
        }
    }

    private fun fillRow(row: DRDCrefo.Row, tabfields: List<Field>, values: List<Any?>) {
        val x = tabfields.zip(values)
        x.forEach{try{row.setString(it.first, it.second?.toString()?: "")}catch(e: Exception){log.warn( CantFillISFieldException(e.toString()) withErrorCode 102, e)}}
    }

    private fun createScreenContext(head: DRDCrefo, boolfields: List<Field>, contexts: List<String>): String{
        var res = ""
        try{
            boolfields.zip(contexts).filter { head.getBoolean(it.first.name) }.forEach{res += " ${it.second}"}
            boolfields.zip(contexts).filter { it.second.matches(Regex("""M[0-9]*""")) && head.getBoolean(it.first.name) }.forEach{
                val cContext = it.second
                boolfields.zip(contexts).filter { it.second.matches(Regex("""[A-Z]*""")) && head.getBoolean(it.first.name) }.forEach{
                    res += """ $cContext${it.second}"""
                }
            }
        }catch(e: Exception){
            log.warn(TranslatedExceptionUtility.createTranslatedException(107, CantCreateFopContextException::class.java), e)
        }
        return res
    }
}