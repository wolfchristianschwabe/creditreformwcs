package de.ck.app.ckcre.drd.util.translation

import de.ck.app.ckcre.drd.util.Constants

enum class TranslatedException(var ERRORCODE: Int, var TEXT_EN: String, var TEXT_DE: String? = TEXT_EN) {

    NOT_TRANSLATED(-1,
            """Errorcode does not exist!""",
            """Errorcode existiert nicht!"""),

    CANNOT_UNMARSHAL_XML(1,
            """Could not parse (unmarshal) XML-File
                |XML: "${Constants.XMLResponseFolder.absolutePath}"
                |XSD: "${Constants.XSDResponse.absolutePath}"
                |Please generate Classes under "template/" in case of change: JAXB, xjc.exe under "java/"
            """.trimMargin(),
            """Fehler beim Unmarshalling von XML!
                |XML: "${Constants.XMLResponseFolder.absolutePath}"
                |XSD: "${Constants.XSDResponse.absolutePath}"
                |Bitte bei Änderungen die Klassen unter "template/" neu generieren: JAXB, xjc.exe unter "java/"
            """.trimMargin()),

    CANNOT_MARSHAL_XML(2,
            """Error in Marshalling!
                |Could not create XML-File!
                |XSD: "${Constants.XSDRequest.absolutePath}"
                |XML: "${Constants.XMLRequest.absolutePath}"
                |Please generate Classes under "template/" in case of change: JAXB, xjc.exe under "java/"
            """.trimMargin(),
            """Fehler beim Marshalling!
                |Konnte XML-Datei nicht generieren!
                |XSD: "${Constants.XSDRequest.absolutePath}
                |XML: "${Constants.XMLRequest.absolutePath}"
                |Bitte bei Änderungen die Klassen unter "template/" neu generieren: JAXB, xjc.exe unter "java/"
            """.trimMargin()),

    CANNOT_FILL_IS(101,
            """Error while filling Infosystem!""",
            """Fehler beim Füllen des Infosystems!"""),

    CANNOT_FILL_IS_FIELD(102,
            """Cannot fill Field!
                |Please check if the value has the right format!
            """.trimMargin(),
            """Kann Feld nicht beschreiben!
                |Bitte auf korrektes Werteformat prüfen!
            """.trimMargin()),

    CANNOT_CREATE_FOPCONTEXT(107,
            """Can't create FOPContext in printBuffer!
                |Please check screen to verify Visibility-Scopes!
            """.trimMargin(),
            """Fehler beim Setzen des FOPKontexts!
                |Bitte Infosystemmaske auf Geltungsbereich prüfen!
            """.trimMargin()),

    CANNOT_USE_INFOSYSTEM(201,
            """Can't read outstanding items through Infosystem "OutstandingItems"!
                |Please check if anything blocks the infosystem-editor!
            """.trimMargin(),
            """Kann offene Posten nicht Laden!
                |Bitte Infosystem "LOP" prüfen!
            """.trimMargin()),

    CANNOT_DOWNNLOAD_FILE_SFTP(301,
            """Can't download file!
                |Please check connection to sftp-host and permissions!
            """.trimMargin(),
            """Kann Datei nicht downloaden!
                |Bitte SFTP-Server-Verbindung und Rechte prüfen!
            """.trimMargin()),

    CONFIGURATION_NOT_FOUND(401,
            """Configuration wasn't found!
                |Please check Configuration in Client (abas)!
            """.trimMargin(),
            """Konfiguration wurde nicht gefunden!
                |Bitte prüfen, ob im abas angelegt!
            """.trimMargin()),

    CANNOT_CREATE_RESPONSE(501,
            """Unable to create dataset in group (Response)!""",
            """Fehler beim Erstellen von Datensatz in Gruppe (Response)!"""),

    CANNOT_EDIT_CUSTOMER(505,
            """Unable to edit Customer/debtor!""",
            """Fehler beim bearbeiten des Debitors/Kunden!"""),

    CANNOT_READ_OP(601,
            """Unable to read data from IS LOP!""",
            """Fehler beim Lesen von Daten des IS LOP!""");

    companion object {
        fun byErrorcode(errorcode: Int): TranslatedException = try {
            TranslatedException.values().filter { it.ERRORCODE == errorcode }.first()
        } catch (e: Exception) {
            NOT_TRANSLATED
        }
    }
}

//Marshalling Exceptions
open class MarshalException(msg: String?=null) : Exception(msg)

class MarshalFileIOException(msg: String?=null) : MarshalException(msg)
class MalformedMarshalObjectException(msg: String?=null) : MarshalException(msg)

//Unmarshalling Exceptions
open class UnmarshalException(msg: String?=null) : Exception(msg)

class UnmarshalFileIOException(msg: String?=null) : UnmarshalException(msg)
class MalformedUnmarshalObjectException(msg: String?=null) : UnmarshalException(msg)

//FTP
open class SFTPException(msg: String?=null) : Exception(msg)

open class SFTPRequestException(msg: String?=null) : SFTPException(msg)
class SFTPDestinationException(msg: String?=null) : SFTPRequestException(msg)

open class SFTPResponseException(msg: String?=null) : SFTPException(msg)
class SFTPTargetException(msg: String?=null) : SFTPResponseException(msg)

//Translations
open class ExceptionTranslationException(msg: String?=null) : Exception(msg)

//Infosystem
open class InfosystemException(msg: String?=null) : Exception(msg)
class CantFillISFieldException(msg: String?=null) : InfosystemException(msg)
class CantCreateFopContextException(msg: String?=null) : InfosystemException(msg)
class CantUseInfosystemException(msg: String?=null) : InfosystemException(msg)

//Konfiguration
open class ConfigurationException(msg: String?=null) : Exception(msg)
class CantFindConfigurationException(msg: String? = null) : Exception(msg)

//Zusatzdatenbank-Response
open class CantCreateResponseException(msg: String?=null) : Exception(msg)

//Kunde/Customer
open class CantEditCustomerException(msg: String? = null) : Exception(msg)

//Tests
open class TestException(msg: String?=null) : Exception(msg)