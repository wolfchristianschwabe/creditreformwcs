package de.ck.app.ckcre.drd.util.dsl

import javax.xml.datatype.DatatypeFactory
import javax.xml.datatype.XMLGregorianCalendar

fun calendar(f: XMLGregorianCalendar.() -> Unit) = DatatypeFactory.newInstance().newXMLGregorianCalendar().apply(f)