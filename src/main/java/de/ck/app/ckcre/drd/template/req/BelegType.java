//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.08.21 um 03:15:13 PM CEST 
//


package de.ck.app.ckcre.drd.template.req;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r BelegType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="BelegType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="debitornummer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="belegnummer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="buchungsvorgang" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="buchungsdatum" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="rechnungsbetrag" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}BetragType"/>
 *         &lt;element name="rechnungsrestbetrag" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}BetragType" minOccurs="0"/>
 *         &lt;element name="zahlungsfrist" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}ZahlungsfristType"/>
 *         &lt;element name="ausgleich" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}AusgleichType" minOccurs="0"/>
 *         &lt;element name="zahlungsart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="zahlungskondition" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mahnschluessel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mahnsperre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mahnstufe" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="mahnfreitext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BelegType", propOrder = {
    "debitornummer",
    "belegnummer",
    "buchungsvorgang",
    "buchungsdatum",
    "rechnungsbetrag",
    "rechnungsrestbetrag",
    "zahlungsfrist",
    "ausgleich",
    "zahlungsart",
    "zahlungskondition",
    "mahnschluessel",
    "mahnsperre",
    "mahnstufe",
    "mahnfreitext"
})
public class BelegType {

    @XmlElement(required = true)
    protected String debitornummer;
    @XmlElement(required = true)
    protected String belegnummer;
    @XmlElement(required = true)
    protected String buchungsvorgang;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar buchungsdatum;
    @XmlElement(required = true)
    protected BetragType rechnungsbetrag;
    protected BetragType rechnungsrestbetrag;
    @XmlElement(required = true)
    protected ZahlungsfristType zahlungsfrist;
    protected AusgleichType ausgleich;
    protected String zahlungsart;
    protected String zahlungskondition;
    protected String mahnschluessel;
    protected String mahnsperre;
    protected String mahnstufe;
    protected String mahnfreitext;

    /**
     * Ruft den Wert der debitornummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitornummer() {
        return debitornummer;
    }

    /**
     * Legt den Wert der debitornummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitornummer(String value) {
        this.debitornummer = value;
    }

    /**
     * Ruft den Wert der belegnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBelegnummer() {
        return belegnummer;
    }

    /**
     * Legt den Wert der belegnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBelegnummer(String value) {
        this.belegnummer = value;
    }

    /**
     * Ruft den Wert der buchungsvorgang-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuchungsvorgang() {
        return buchungsvorgang;
    }

    /**
     * Legt den Wert der buchungsvorgang-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuchungsvorgang(String value) {
        this.buchungsvorgang = value;
    }

    /**
     * Ruft den Wert der buchungsdatum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBuchungsdatum() {
        return buchungsdatum;
    }

    /**
     * Legt den Wert der buchungsdatum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBuchungsdatum(XMLGregorianCalendar value) {
        this.buchungsdatum = value;
    }

    /**
     * Ruft den Wert der rechnungsbetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BetragType }
     *     
     */
    public BetragType getRechnungsbetrag() {
        return rechnungsbetrag;
    }

    /**
     * Legt den Wert der rechnungsbetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BetragType }
     *     
     */
    public void setRechnungsbetrag(BetragType value) {
        this.rechnungsbetrag = value;
    }

    /**
     * Ruft den Wert der rechnungsrestbetrag-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BetragType }
     *     
     */
    public BetragType getRechnungsrestbetrag() {
        return rechnungsrestbetrag;
    }

    /**
     * Legt den Wert der rechnungsrestbetrag-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BetragType }
     *     
     */
    public void setRechnungsrestbetrag(BetragType value) {
        this.rechnungsrestbetrag = value;
    }

    /**
     * Ruft den Wert der zahlungsfrist-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZahlungsfristType }
     *     
     */
    public ZahlungsfristType getZahlungsfrist() {
        return zahlungsfrist;
    }

    /**
     * Legt den Wert der zahlungsfrist-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZahlungsfristType }
     *     
     */
    public void setZahlungsfrist(ZahlungsfristType value) {
        this.zahlungsfrist = value;
    }

    /**
     * Ruft den Wert der ausgleich-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AusgleichType }
     *     
     */
    public AusgleichType getAusgleich() {
        return ausgleich;
    }

    /**
     * Legt den Wert der ausgleich-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AusgleichType }
     *     
     */
    public void setAusgleich(AusgleichType value) {
        this.ausgleich = value;
    }

    /**
     * Ruft den Wert der zahlungsart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlungsart() {
        return zahlungsart;
    }

    /**
     * Legt den Wert der zahlungsart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlungsart(String value) {
        this.zahlungsart = value;
    }

    /**
     * Ruft den Wert der zahlungskondition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZahlungskondition() {
        return zahlungskondition;
    }

    /**
     * Legt den Wert der zahlungskondition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZahlungskondition(String value) {
        this.zahlungskondition = value;
    }

    /**
     * Ruft den Wert der mahnschluessel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnschluessel() {
        return mahnschluessel;
    }

    /**
     * Legt den Wert der mahnschluessel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnschluessel(String value) {
        this.mahnschluessel = value;
    }

    /**
     * Ruft den Wert der mahnsperre-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnsperre() {
        return mahnsperre;
    }

    /**
     * Legt den Wert der mahnsperre-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnsperre(String value) {
        this.mahnsperre = value;
    }

    /**
     * Ruft den Wert der mahnstufe-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnstufe() {
        return mahnstufe;
    }

    /**
     * Legt den Wert der mahnstufe-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnstufe(String value) {
        this.mahnstufe = value;
    }

    /**
     * Ruft den Wert der mahnfreitext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMahnfreitext() {
        return mahnfreitext;
    }

    /**
     * Legt den Wert der mahnfreitext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMahnfreitext(String value) {
        this.mahnfreitext = value;
    }

}
