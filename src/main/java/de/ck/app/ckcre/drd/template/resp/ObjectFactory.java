//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.08.21 um 03:15:31 PM CEST 
//


package de.ck.app.ckcre.drd.template.resp;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.creditreform.xsd.drd.drdruecklieferungschemaerw_v1_1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.creditreform.xsd.drd.drdruecklieferungschemaerw_v1_1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DrdRuecklieferungErw }
     * 
     */
    public DrdRuecklieferungErw createDrdRuecklieferungErw() {
        return new DrdRuecklieferungErw();
    }

    /**
     * Create an instance of {@link DrdRuecklieferungErw.DrdDebitoren }
     * 
     */
    public DrdRuecklieferungErw.DrdDebitoren createDrdRuecklieferungErwDrdDebitoren() {
        return new DrdRuecklieferungErw.DrdDebitoren();
    }

    /**
     * Create an instance of {@link NameType }
     * 
     */
    public NameType createNameType() {
        return new NameType();
    }

    /**
     * Create an instance of {@link BetragType }
     * 
     */
    public BetragType createBetragType() {
        return new BetragType();
    }

    /**
     * Create an instance of {@link DrdDebitorTypeErw }
     * 
     */
    public DrdDebitorTypeErw createDrdDebitorTypeErw() {
        return new DrdDebitorTypeErw();
    }

}
