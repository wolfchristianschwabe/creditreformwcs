//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.08.21 um 03:15:13 PM CEST 
//


package de.ck.app.ckcre.drd.template.req;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r AdresseType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="AdresseType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="zustelladresse" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}ZustelladresseType" minOccurs="0"/>
 *         &lt;element name="postfachadresse" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}PostfachadresseType" minOccurs="0"/>
 *         &lt;element name="grosskundenadresse" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}GrosskundenadresseType" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AdresseType", propOrder = {
    "zustelladresse",
    "postfachadresse",
    "grosskundenadresse"
})
public class AdresseType {

    protected ZustelladresseType zustelladresse;
    protected PostfachadresseType postfachadresse;
    protected GrosskundenadresseType grosskundenadresse;

    /**
     * Ruft den Wert der zustelladresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ZustelladresseType }
     *     
     */
    public ZustelladresseType getZustelladresse() {
        return zustelladresse;
    }

    /**
     * Legt den Wert der zustelladresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ZustelladresseType }
     *     
     */
    public void setZustelladresse(ZustelladresseType value) {
        this.zustelladresse = value;
    }

    /**
     * Ruft den Wert der postfachadresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link PostfachadresseType }
     *     
     */
    public PostfachadresseType getPostfachadresse() {
        return postfachadresse;
    }

    /**
     * Legt den Wert der postfachadresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link PostfachadresseType }
     *     
     */
    public void setPostfachadresse(PostfachadresseType value) {
        this.postfachadresse = value;
    }

    /**
     * Ruft den Wert der grosskundenadresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link GrosskundenadresseType }
     *     
     */
    public GrosskundenadresseType getGrosskundenadresse() {
        return grosskundenadresse;
    }

    /**
     * Legt den Wert der grosskundenadresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link GrosskundenadresseType }
     *     
     */
    public void setGrosskundenadresse(GrosskundenadresseType value) {
        this.grosskundenadresse = value;
    }

}
