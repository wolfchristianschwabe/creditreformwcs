package de.ck.app.ckcre.drd.util;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

/**
 * UNMARSHAL:
 *    XML über XSD in JAVA-Objeckt-Struktur
 */
public class UnmarshalUtility {

    public static <T> T unmarshal( File xsd, File xml, Class<T> clazz ) throws Exception {
        // Schema und JAXBContext sind multithreadingsicher ("thread safe"):
        SchemaFactory schemaFactory = SchemaFactory.newInstance( XMLConstants.W3C_XML_SCHEMA_NS_URI);
        Schema        schema        = schemaFactory.newSchema(xsd);
        JAXBContext   jaxbContext   = JAXBContext.newInstance(clazz.getPackage().getName());
        return unmarshal(jaxbContext, schema, xml, clazz);
    }

    public static <T> T unmarshal( JAXBContext jaxbContext, Schema schema, File xml, Class<T> clazz ) throws Exception {
        // Unmarshaller ist nicht multithreadingsicher:
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setSchema(schema);
        return clazz.cast( unmarshaller.unmarshal(xml));
    }
}
