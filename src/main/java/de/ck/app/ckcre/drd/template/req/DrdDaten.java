//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.08.21 um 03:15:13 PM CEST 
//


package de.ck.app.ckcre.drd.template.req;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="extraktion" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}ExtraktionType"/>
 *         &lt;element name="debitoren">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="debitor" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}DebitorType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="belege">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="beleg" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}BelegType" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="konvertierungsstatistik" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="info" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}KonvertierungsInfoType" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "extraktion",
    "debitoren",
    "belege",
    "konvertierungsstatistik"
})
@XmlRootElement(name = "drdDaten")
public class DrdDaten {

    @XmlElement(required = true)
    protected ExtraktionType extraktion;
    @XmlElement(required = true)
    protected DrdDaten.Debitoren debitoren;
    @XmlElement(required = true)
    protected DrdDaten.Belege belege;
    protected DrdDaten.Konvertierungsstatistik konvertierungsstatistik;

    /**
     * Ruft den Wert der extraktion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtraktionType }
     *     
     */
    public ExtraktionType getExtraktion() {
        return extraktion;
    }

    /**
     * Legt den Wert der extraktion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtraktionType }
     *     
     */
    public void setExtraktion(ExtraktionType value) {
        this.extraktion = value;
    }

    /**
     * Ruft den Wert der debitoren-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DrdDaten.Debitoren }
     *     
     */
    public DrdDaten.Debitoren getDebitoren() {
        return debitoren;
    }

    /**
     * Legt den Wert der debitoren-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DrdDaten.Debitoren }
     *     
     */
    public void setDebitoren(DrdDaten.Debitoren value) {
        this.debitoren = value;
    }

    /**
     * Ruft den Wert der belege-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DrdDaten.Belege }
     *     
     */
    public DrdDaten.Belege getBelege() {
        return belege;
    }

    /**
     * Legt den Wert der belege-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DrdDaten.Belege }
     *     
     */
    public void setBelege(DrdDaten.Belege value) {
        this.belege = value;
    }

    /**
     * Ruft den Wert der konvertierungsstatistik-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DrdDaten.Konvertierungsstatistik }
     *     
     */
    public DrdDaten.Konvertierungsstatistik getKonvertierungsstatistik() {
        return konvertierungsstatistik;
    }

    /**
     * Legt den Wert der konvertierungsstatistik-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DrdDaten.Konvertierungsstatistik }
     *     
     */
    public void setKonvertierungsstatistik(DrdDaten.Konvertierungsstatistik value) {
        this.konvertierungsstatistik = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="beleg" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}BelegType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "beleg"
    })
    public static class Belege {

        @XmlElement(required = true)
        protected List<BelegType> beleg;

        /**
         * Gets the value of the beleg property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the beleg property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getBeleg().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BelegType }
         * 
         * 
         */
        public List<BelegType> getBeleg() {
            if (beleg == null) {
                beleg = new ArrayList<BelegType>();
            }
            return this.beleg;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="debitor" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}DebitorType" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "debitor"
    })
    public static class Debitoren {

        @XmlElement(required = true)
        protected List<DebitorType> debitor;

        /**
         * Gets the value of the debitor property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the debitor property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDebitor().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DebitorType }
         * 
         * 
         */
        public List<DebitorType> getDebitor() {
            if (debitor == null) {
                debitor = new ArrayList<DebitorType>();
            }
            return this.debitor;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="info" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}KonvertierungsInfoType" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "info"
    })
    public static class Konvertierungsstatistik {

        protected List<KonvertierungsInfoType> info;

        /**
         * Gets the value of the info property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the info property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInfo().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link KonvertierungsInfoType }
         * 
         * 
         */
        public List<KonvertierungsInfoType> getInfo() {
            if (info == null) {
                info = new ArrayList<KonvertierungsInfoType>();
            }
            return this.info;
        }

    }

}
