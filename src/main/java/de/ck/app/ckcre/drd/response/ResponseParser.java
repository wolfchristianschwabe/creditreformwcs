package de.ck.app.ckcre.drd.response;

import de.ck.app.ckcre.drd.template.resp.DrdRuecklieferungErw;
import de.ck.app.ckcre.drd.template.resp.NameType;
import de.ck.app.ckcre.drd.util.UnmarshalUtility;

import java.io.File;

/**
 * unmarshals
 */
public class ResponseParser {

    public static DrdRuecklieferungErw parse(File xsd, File xml) throws Exception {
       return UnmarshalUtility.unmarshal(xsd, xml, DrdRuecklieferungErw.class);
    }

    /**
     * TESTMETHODE
     * @deprecated
     * @param args
     */
    public static void main(String... args){
        try{
            File xsd = new File("src/main/resources/crefo/DRDRuecklieferungSchemaErw-v1.1.xsd");
            File xml = new File("src/test/resources/xml/123456789_100_1100_150929.xml");

            System.out.println(xsd.getAbsolutePath() + " : " + xsd.canRead());
            System.out.println(xml.getAbsolutePath() + " : " + xml.canRead());

            DrdRuecklieferungErw res = ResponseParser.parse(xsd, xml);

            System.out.println("\n" + res + "\n" + res.getDatum() + "\n");
            System.out.println("\n--DEBITOREN--\n\n");
            res.getDrdDebitoren().getDrdDebitor().forEach(deb -> {
                NameType cName = deb.getName();
                System.out.println(cName.getName1() + " " + cName.getName2() + " " + cName.getName3() + " " + cName.getName4());
                System.out.println(deb.getPlz() + " " + deb.getOrt());
                System.out.println(deb.getLand());
                System.out.println(deb.getBranchenArt() + " - "  +deb.getBranchenCode() + " - " + deb.getBranchenBezeichnung() + " - " + deb.getBranchenLand() );
            });

        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
