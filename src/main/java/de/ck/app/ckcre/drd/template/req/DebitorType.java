//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.08.21 um 03:15:13 PM CEST 
//


package de.ck.app.ckcre.drd.template.req;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Java-Klasse f�r DebitorType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="DebitorType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="debitornummer" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="name" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}NameType"/>
 *         &lt;element name="adresse" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}AdresseType"/>
 *         &lt;element name="telefon" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}KommunikationsnummerType" minOccurs="0"/>
 *         &lt;element name="fax" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}KommunikationsnummerType" minOccurs="0"/>
 *         &lt;element name="mobil" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="web" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="saldoOffenePosten" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}BetragType"/>
 *         &lt;element name="amtsgerichtPlz" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="handelsregisternummer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="registerart" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="rechtsform" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="umsatzsteuerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="crefonummer" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DebitorType", propOrder = {
    "debitornummer",
    "name",
    "adresse",
    "telefon",
    "fax",
    "mobil",
    "email",
    "web",
    "saldoOffenePosten",
    "amtsgerichtPlz",
    "handelsregisternummer",
    "registerart",
    "rechtsform",
    "umsatzsteuerID",
    "crefonummer"
})
public class DebitorType {

    @XmlElement(required = true)
    protected String debitornummer;
    @XmlElement(required = true)
    protected NameType name;
    @XmlElement(required = true)
    protected AdresseType adresse;
    protected KommunikationsnummerType telefon;
    protected KommunikationsnummerType fax;
    protected String mobil;
    protected String email;
    protected String web;
    @XmlElement(required = true)
    protected BetragType saldoOffenePosten;
    protected String amtsgerichtPlz;
    protected String handelsregisternummer;
    protected String registerart;
    protected String rechtsform;
    protected String umsatzsteuerID;
    protected BigDecimal crefonummer;

    /**
     * Ruft den Wert der debitornummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebitornummer() {
        return debitornummer;
    }

    /**
     * Legt den Wert der debitornummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebitornummer(String value) {
        this.debitornummer = value;
    }

    /**
     * Ruft den Wert der name-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link NameType }
     *     
     */
    public NameType getName() {
        return name;
    }

    /**
     * Legt den Wert der name-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link NameType }
     *     
     */
    public void setName(NameType value) {
        this.name = value;
    }

    /**
     * Ruft den Wert der adresse-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link AdresseType }
     *     
     */
    public AdresseType getAdresse() {
        return adresse;
    }

    /**
     * Legt den Wert der adresse-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link AdresseType }
     *     
     */
    public void setAdresse(AdresseType value) {
        this.adresse = value;
    }

    /**
     * Ruft den Wert der telefon-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KommunikationsnummerType }
     *     
     */
    public KommunikationsnummerType getTelefon() {
        return telefon;
    }

    /**
     * Legt den Wert der telefon-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KommunikationsnummerType }
     *     
     */
    public void setTelefon(KommunikationsnummerType value) {
        this.telefon = value;
    }

    /**
     * Ruft den Wert der fax-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link KommunikationsnummerType }
     *     
     */
    public KommunikationsnummerType getFax() {
        return fax;
    }

    /**
     * Legt den Wert der fax-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link KommunikationsnummerType }
     *     
     */
    public void setFax(KommunikationsnummerType value) {
        this.fax = value;
    }

    /**
     * Ruft den Wert der mobil-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobil() {
        return mobil;
    }

    /**
     * Legt den Wert der mobil-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobil(String value) {
        this.mobil = value;
    }

    /**
     * Ruft den Wert der email-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Legt den Wert der email-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Ruft den Wert der web-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWeb() {
        return web;
    }

    /**
     * Legt den Wert der web-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWeb(String value) {
        this.web = value;
    }

    /**
     * Ruft den Wert der saldoOffenePosten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BetragType }
     *     
     */
    public BetragType getSaldoOffenePosten() {
        return saldoOffenePosten;
    }

    /**
     * Legt den Wert der saldoOffenePosten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BetragType }
     *     
     */
    public void setSaldoOffenePosten(BetragType value) {
        this.saldoOffenePosten = value;
    }

    /**
     * Ruft den Wert der amtsgerichtPlz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmtsgerichtPlz() {
        return amtsgerichtPlz;
    }

    /**
     * Legt den Wert der amtsgerichtPlz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmtsgerichtPlz(String value) {
        this.amtsgerichtPlz = value;
    }

    /**
     * Ruft den Wert der handelsregisternummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHandelsregisternummer() {
        return handelsregisternummer;
    }

    /**
     * Legt den Wert der handelsregisternummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHandelsregisternummer(String value) {
        this.handelsregisternummer = value;
    }

    /**
     * Ruft den Wert der registerart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterart() {
        return registerart;
    }

    /**
     * Legt den Wert der registerart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterart(String value) {
        this.registerart = value;
    }

    /**
     * Ruft den Wert der rechtsform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRechtsform() {
        return rechtsform;
    }

    /**
     * Legt den Wert der rechtsform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRechtsform(String value) {
        this.rechtsform = value;
    }

    /**
     * Ruft den Wert der umsatzsteuerID-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUmsatzsteuerID() {
        return umsatzsteuerID;
    }

    /**
     * Legt den Wert der umsatzsteuerID-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUmsatzsteuerID(String value) {
        this.umsatzsteuerID = value;
    }

    /**
     * Ruft den Wert der crefonummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCrefonummer() {
        return crefonummer;
    }

    /**
     * Legt den Wert der crefonummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCrefonummer(BigDecimal value) {
        this.crefonummer = value;
    }

}
