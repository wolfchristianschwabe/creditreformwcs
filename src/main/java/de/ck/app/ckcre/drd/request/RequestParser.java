package de.ck.app.ckcre.drd.request;

import de.ck.app.ckcre.drd.template.req.DrdDaten;
import de.ck.app.ckcre.drd.util.MarshalUtility;
import de.ck.app.ckcre.drd.util.translation.MarshalFileIOException;

import java.io.File;

public class RequestParser {

    private DrdDaten daten;

    public RequestParser(DrdDaten Daten){
        daten = Daten;
    }

    public File parse(File xsd, File output) throws Exception{
        MarshalUtility.marshal(xsd, output, daten);
        if (output.exists()) return output;
        throw new MarshalFileIOException("Exception in marshalling!");
    }
}
