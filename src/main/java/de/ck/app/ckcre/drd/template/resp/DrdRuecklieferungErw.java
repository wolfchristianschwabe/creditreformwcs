//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.08.21 um 03:15:31 PM CEST 
//


package de.ck.app.ckcre.drd.template.resp;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="datum" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="drdDebitoren" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="drdDebitor" type="{http://xsd.creditreform.de/DRD/DRDRuecklieferungSchemaErw-v1.1.xsd}DrdDebitorTypeErw" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "datum",
    "drdDebitoren"
})
@XmlRootElement(name = "drdRuecklieferungErw")
public class DrdRuecklieferungErw {

    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar datum;
    protected DrdRuecklieferungErw.DrdDebitoren drdDebitoren;

    /**
     * Ruft den Wert der datum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatum() {
        return datum;
    }

    /**
     * Legt den Wert der datum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatum(XMLGregorianCalendar value) {
        this.datum = value;
    }

    /**
     * Ruft den Wert der drdDebitoren-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DrdRuecklieferungErw.DrdDebitoren }
     *     
     */
    public DrdRuecklieferungErw.DrdDebitoren getDrdDebitoren() {
        return drdDebitoren;
    }

    /**
     * Legt den Wert der drdDebitoren-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DrdRuecklieferungErw.DrdDebitoren }
     *     
     */
    public void setDrdDebitoren(DrdRuecklieferungErw.DrdDebitoren value) {
        this.drdDebitoren = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="drdDebitor" type="{http://xsd.creditreform.de/DRD/DRDRuecklieferungSchemaErw-v1.1.xsd}DrdDebitorTypeErw" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "drdDebitor"
    })
    public static class DrdDebitoren {

        protected List<DrdDebitorTypeErw> drdDebitor;

        /**
         * Gets the value of the drdDebitor property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the drdDebitor property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDrdDebitor().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link DrdDebitorTypeErw }
         * 
         * 
         */
        public List<DrdDebitorTypeErw> getDrdDebitor() {
            if (drdDebitor == null) {
                drdDebitor = new ArrayList<DrdDebitorTypeErw>();
            }
            return this.drdDebitor;
        }

    }

}
