package de.ck.app.ckcre.drd.util;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.File;

/**
 *  MARSHAL:
 *     Java-Objekt --> XML über XSD
 */
public class MarshalUtility {

    public static void marshal( File xsd, File xml, Object jaxbElement ) throws Exception {
        SchemaFactory schemaFactory = SchemaFactory.newInstance( XMLConstants.W3C_XML_SCHEMA_NS_URI );
        Schema schema               =  schemaFactory.newSchema(xsd);
        JAXBContext jaxbContext     = JAXBContext.newInstance(jaxbElement.getClass().getPackage().getName());
        marshal(jaxbContext, schema, xml , jaxbElement);
    }

    public static void marshal( JAXBContext jaxbContext, Schema schema, File xml, Object jaxbElement ) throws Exception {
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setSchema(schema);
        marshaller.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        marshaller.marshal(jaxbElement, xml);
    }
}
