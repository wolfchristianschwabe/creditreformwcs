//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.08.21 um 03:15:13 PM CEST 
//


package de.ck.app.ckcre.drd.template.req;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Java-Klasse f�r ZahlungsfristType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ZahlungsfristType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="basisdatum" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="nettofaelligkeitsdatum" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *           &lt;element name="skonto" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}SkontoType"/>
 *         &lt;/sequence>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ZahlungsfristType", propOrder = {
    "basisdatum",
    "nettofaelligkeitsdatum",
    "skonto"
})
public class ZahlungsfristType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar basisdatum;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar nettofaelligkeitsdatum;
    protected List<SkontoType> skonto;

    /**
     * Ruft den Wert der basisdatum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getBasisdatum() {
        return basisdatum;
    }

    /**
     * Legt den Wert der basisdatum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setBasisdatum(XMLGregorianCalendar value) {
        this.basisdatum = value;
    }

    /**
     * Ruft den Wert der nettofaelligkeitsdatum-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getNettofaelligkeitsdatum() {
        return nettofaelligkeitsdatum;
    }

    /**
     * Legt den Wert der nettofaelligkeitsdatum-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setNettofaelligkeitsdatum(XMLGregorianCalendar value) {
        this.nettofaelligkeitsdatum = value;
    }

    /**
     * Gets the value of the skonto property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the skonto property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSkonto().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SkontoType }
     * 
     * 
     */
    public List<SkontoType> getSkonto() {
        if (skonto == null) {
            skonto = new ArrayList<SkontoType>();
        }
        return this.skonto;
    }

}
