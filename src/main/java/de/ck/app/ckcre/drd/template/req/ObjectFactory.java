//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.08.21 um 03:15:13 PM CEST 
//


package de.ck.app.ckcre.drd.template.req;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.creditreform.xsd.drd.drdschema_v1_0 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.creditreform.xsd.drd.drdschema_v1_0
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DrdDaten }
     * 
     */
    public DrdDaten createDrdDaten() {
        return new DrdDaten();
    }

    /**
     * Create an instance of {@link ExtraktionType }
     * 
     */
    public ExtraktionType createExtraktionType() {
        return new ExtraktionType();
    }

    /**
     * Create an instance of {@link DrdDaten.Debitoren }
     * 
     */
    public DrdDaten.Debitoren createDrdDatenDebitoren() {
        return new DrdDaten.Debitoren();
    }

    /**
     * Create an instance of {@link DrdDaten.Belege }
     * 
     */
    public DrdDaten.Belege createDrdDatenBelege() {
        return new DrdDaten.Belege();
    }

    /**
     * Create an instance of {@link DrdDaten.Konvertierungsstatistik }
     * 
     */
    public DrdDaten.Konvertierungsstatistik createDrdDatenKonvertierungsstatistik() {
        return new DrdDaten.Konvertierungsstatistik();
    }

    /**
     * Create an instance of {@link ZustelladresseType }
     * 
     */
    public ZustelladresseType createZustelladresseType() {
        return new ZustelladresseType();
    }

    /**
     * Create an instance of {@link ZahlungsfristType }
     * 
     */
    public ZahlungsfristType createZahlungsfristType() {
        return new ZahlungsfristType();
    }

    /**
     * Create an instance of {@link KonvertierungsInfoType }
     * 
     */
    public KonvertierungsInfoType createKonvertierungsInfoType() {
        return new KonvertierungsInfoType();
    }

    /**
     * Create an instance of {@link NameType }
     * 
     */
    public NameType createNameType() {
        return new NameType();
    }

    /**
     * Create an instance of {@link GrosskundenadresseType }
     * 
     */
    public GrosskundenadresseType createGrosskundenadresseType() {
        return new GrosskundenadresseType();
    }

    /**
     * Create an instance of {@link BelegType }
     * 
     */
    public BelegType createBelegType() {
        return new BelegType();
    }

    /**
     * Create an instance of {@link KommunikationsnummerType }
     * 
     */
    public KommunikationsnummerType createKommunikationsnummerType() {
        return new KommunikationsnummerType();
    }

    /**
     * Create an instance of {@link AusgleichType }
     * 
     */
    public AusgleichType createAusgleichType() {
        return new AusgleichType();
    }

    /**
     * Create an instance of {@link PostfachadresseType }
     * 
     */
    public PostfachadresseType createPostfachadresseType() {
        return new PostfachadresseType();
    }

    /**
     * Create an instance of {@link ExtraktionssoftwareType }
     * 
     */
    public ExtraktionssoftwareType createExtraktionssoftwareType() {
        return new ExtraktionssoftwareType();
    }

    /**
     * Create an instance of {@link BetragType }
     * 
     */
    public BetragType createBetragType() {
        return new BetragType();
    }

    /**
     * Create an instance of {@link DebitorType }
     * 
     */
    public DebitorType createDebitorType() {
        return new DebitorType();
    }

    /**
     * Create an instance of {@link AdresseType }
     * 
     */
    public AdresseType createAdresseType() {
        return new AdresseType();
    }

    /**
     * Create an instance of {@link SkontoType }
     * 
     */
    public SkontoType createSkontoType() {
        return new SkontoType();
    }

    /**
     * Create an instance of {@link BuchhaltungssystemType }
     * 
     */
    public BuchhaltungssystemType createBuchhaltungssystemType() {
        return new BuchhaltungssystemType();
    }

}
