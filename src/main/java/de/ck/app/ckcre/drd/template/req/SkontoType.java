//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.08.21 um 03:15:13 PM CEST 
//


package de.ck.app.ckcre.drd.template.req;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import java.math.BigDecimal;


/**
 * <p>Java-Klasse f�r SkontoType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="SkontoType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;sequence>
 *           &lt;element name="schluessel" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;/sequence>
 *         &lt;sequence>
 *           &lt;element name="tage" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *           &lt;element name="prozent" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *         &lt;/sequence>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SkontoType", propOrder = {
    "schluessel",
    "tage",
    "prozent"
})
public class SkontoType {

    protected String schluessel;
    protected BigDecimal tage;
    protected BigDecimal prozent;

    /**
     * Ruft den Wert der schluessel-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchluessel() {
        return schluessel;
    }

    /**
     * Legt den Wert der schluessel-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchluessel(String value) {
        this.schluessel = value;
    }

    /**
     * Ruft den Wert der tage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTage() {
        return tage;
    }

    /**
     * Legt den Wert der tage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTage(BigDecimal value) {
        this.tage = value;
    }

    /**
     * Ruft den Wert der prozent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProzent() {
        return prozent;
    }

    /**
     * Legt den Wert der prozent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProzent(BigDecimal value) {
        this.prozent = value;
    }

}
