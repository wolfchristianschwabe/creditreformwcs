//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.2.8-b130911.1802 generiert 
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// �nderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren. 
// Generiert: 2018.08.21 um 03:15:13 PM CEST 
//


package de.ck.app.ckcre.drd.template.req;

import javax.xml.bind.annotation.*;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;


/**
 * <p>Java-Klasse f�r ExtraktionType complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="ExtraktionType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="mitgliedsnummer" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}MitgliedsnummerType"/>
 *         &lt;element name="mandant" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}MandantType"/>
 *         &lt;element name="buchungskreis" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}BuchungskreisType"/>
 *         &lt;element name="stichtagOffenePosten" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="extraktionszeitpunkt" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="sendezeitpunkt" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="buchhaltungssystem" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}BuchhaltungssystemType"/>
 *         &lt;element name="extraktionssoftware" type="{http://xsd.creditreform.de/DRD/DRDSchema-v1.0.xsd}ExtraktionssoftwareType"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ExtraktionType", propOrder = {
    "mitgliedsnummer",
    "mandant",
    "buchungskreis",
    "stichtagOffenePosten",
    "extraktionszeitpunkt",
    "sendezeitpunkt",
    "buchhaltungssystem",
    "extraktionssoftware"
})
public class ExtraktionType {

    @XmlElement(required = true)
    protected BigDecimal mitgliedsnummer;
    @XmlElement(required = true)
    protected String mandant;
    @XmlElement(required = true)
    protected String buchungskreis;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar stichtagOffenePosten;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar extraktionszeitpunkt;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar sendezeitpunkt;
    @XmlElement(required = true)
    protected BuchhaltungssystemType buchhaltungssystem;
    @XmlElement(required = true)
    protected ExtraktionssoftwareType extraktionssoftware;

    /**
     * Ruft den Wert der mitgliedsnummer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getMitgliedsnummer() {
        return mitgliedsnummer;
    }

    /**
     * Legt den Wert der mitgliedsnummer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setMitgliedsnummer(BigDecimal value) {
        this.mitgliedsnummer = value;
    }

    /**
     * Ruft den Wert der mandant-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMandant() {
        return mandant;
    }

    /**
     * Legt den Wert der mandant-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMandant(String value) {
        this.mandant = value;
    }

    /**
     * Ruft den Wert der buchungskreis-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuchungskreis() {
        return buchungskreis;
    }

    /**
     * Legt den Wert der buchungskreis-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuchungskreis(String value) {
        this.buchungskreis = value;
    }

    /**
     * Ruft den Wert der stichtagOffenePosten-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getStichtagOffenePosten() {
        return stichtagOffenePosten;
    }

    /**
     * Legt den Wert der stichtagOffenePosten-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setStichtagOffenePosten(XMLGregorianCalendar value) {
        this.stichtagOffenePosten = value;
    }

    /**
     * Ruft den Wert der extraktionszeitpunkt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getExtraktionszeitpunkt() {
        return extraktionszeitpunkt;
    }

    /**
     * Legt den Wert der extraktionszeitpunkt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setExtraktionszeitpunkt(XMLGregorianCalendar value) {
        this.extraktionszeitpunkt = value;
    }

    /**
     * Ruft den Wert der sendezeitpunkt-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSendezeitpunkt() {
        return sendezeitpunkt;
    }

    /**
     * Legt den Wert der sendezeitpunkt-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSendezeitpunkt(XMLGregorianCalendar value) {
        this.sendezeitpunkt = value;
    }

    /**
     * Ruft den Wert der buchhaltungssystem-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BuchhaltungssystemType }
     *     
     */
    public BuchhaltungssystemType getBuchhaltungssystem() {
        return buchhaltungssystem;
    }

    /**
     * Legt den Wert der buchhaltungssystem-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BuchhaltungssystemType }
     *     
     */
    public void setBuchhaltungssystem(BuchhaltungssystemType value) {
        this.buchhaltungssystem = value;
    }

    /**
     * Ruft den Wert der extraktionssoftware-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link ExtraktionssoftwareType }
     *     
     */
    public ExtraktionssoftwareType getExtraktionssoftware() {
        return extraktionssoftware;
    }

    /**
     * Legt den Wert der extraktionssoftware-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtraktionssoftwareType }
     *     
     */
    public void setExtraktionssoftware(ExtraktionssoftwareType value) {
        this.extraktionssoftware = value;
    }

}
