package de.ck.app.ckcre.drd.events;

import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.ButtonEventHandler;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ButtonEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.DRDCrefo;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.ck.app.ckcre.drd.events.head.Burequest;
import de.ck.app.ckcre.drd.events.head.Buresponse;
import de.ck.app.ckcre.drd.events.head.Start;
import de.ck.app.ckcre.drd.util.Constants;

@EventHandler(head = DRDCrefo.class, row = DRDCrefo.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class DRDCrefoEventHandler {

    @ButtonEventHandler(field = "start", type = ButtonEventType.AFTER)
    public void startAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, DRDCrefo head) {
        Start.buttonAfter(screenControl, ctx, head);
    }

    @ButtonEventHandler(field = "burequest", type = ButtonEventType.AFTER)
    public void burequestAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, DRDCrefo head) {
        Burequest.buttonAfter();
    }

    @ButtonEventHandler(field = "buresponse", type = ButtonEventType.AFTER)
    public void buresponseAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, DRDCrefo head) {
        Buresponse.buttonAfter();
    }

    @ButtonEventHandler(field = "bushowdata", type = ButtonEventType.AFTER, table = true)
    public void bushowdataAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, DRDCrefo head, DRDCrefo.Row currentRow) {

    }

   @ScreenEventHandler(type = ScreenEventType.ENTER)
   public void screenEnter(ScreenEvent event, ScreenControl screenControl, DbContext ctx, DRDCrefo head) {
        Constants.setCtx(ctx);
        head.setShowbranch(true);
        head.setShowlief(true);
        head.setShowpool(true);
        head.setShowm2x(true);
    }
}
