package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.exception.CommandException;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.app.ckcre.cws.util.LoggerUtil;

public class SetVorbelegung {
    public void order(DbContext ctx, CKCREORDERS head){
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        PasswordEditor editor = operatorCode.createEditor();
        String vorbelegung;
        try {
            editor.open(EditorAction.UPDATE);
            vorbelegung = head.getCkcreprodtype();
            vorbelegung += ":"+head.getCkcreprodtypeklar();
            vorbelegung += ":"+head.getCkcreordertype();
            vorbelegung += ":"+head.getCkcreordertypeklar();
            vorbelegung += ":"+head.getCkcrelegitimateint();
            vorbelegung += ":"+head.getCkcrelegitimatklar();
            editor.setString("Yckcreordervor",vorbelegung);
            editor.commit();

        } catch (CommandException e) {
            FO.box("Fehler", "Vorbelegung gescheitert!"+e.getMessage());
            LoggerUtil.LOG.error("Fehler bei Vorbelegung", e);
        }finally {
            if(editor!=null&&editor.active()){
                editor.abort();
                editor = null;
            }
        }


    }
    public void search(DbContext ctx, CKCREORDERS head){
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        PasswordEditor editor = operatorCode.createEditor();
        String vorbelegung;
        try {
            editor.open(EditorAction.UPDATE);
            vorbelegung = head.getCkcreprodtype();
            vorbelegung += ":"+head.getCkcreprodtypeklar();
            vorbelegung += ":"+head.getCkcreordertype();
            vorbelegung += ":"+head.getCkcreordertypeklar();
            vorbelegung += ":"+head.getCkcrelegitimateint();
            vorbelegung += ":"+head.getCkcrelegitimatklar();
            editor.setString("Yckcresearch",vorbelegung);

        } catch (CommandException e) {
            FO.box("Fehler", "Vorbelegung gescheitert!");
            LoggerUtil.LOG.error("Fehler bei Vorbelegung", e);
        }


    }
}

