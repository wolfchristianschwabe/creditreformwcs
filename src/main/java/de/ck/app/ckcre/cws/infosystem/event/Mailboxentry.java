/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */
package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.MailboxentryResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxentryrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.InputStream;

/**
 * @author christian.schwabe
 * @param: ctx is the Database Context witch the IS is loaded in
 * @do This Method reads a Mailboxentry of the Webservices Mailbox
 */
public class Mailboxentry {
	public void Entryread(CKCREORDERS head,CKCREORDERS.Row row,  DbContext ctx){
		final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
		PasswordEditor editor=null;
		Logonrequest login = new Logonrequest();
		Trequestheader logonrequest = null;
		MailboxentryResponse response = null;
		String IP = "";
		Tmailboxentryrequestbody probs = new Tmailboxentryrequestbody();
		probs.setMailboxentrynumber(row.getCremailentry());
		try {
			editor = operatorCode.createEditor();
			editor.open(EditorAction.VIEW);
		}catch(Exception e){
			FO.box("Fehler","Fehler beim Abruf der Daten f\u00fcr Nutzer: "+operatorCode.getIdno());
			LoggerUtil.LOG.error("Fehler beim Abruf der Nutzerdaten durch den Editor",e);
		}

		try{
			logonrequest = new Logonrequest().buildupLogin(editor.getYckcregenpwd(), editor.getYckcrepw(), editor.getYckcrelang(), editor.getYckcreuser(), Integer.parseInt(editor.getYckcrekeylist()));
			response = (MailboxentryResponse) Services.Mailboxentry.start(probs, logonrequest);


			///ctx.out().println(response.getBody().getIdentificationnumber());
			try {
				//--------------------------------------------------------------------------------------------------------------------------------------
				//Getting the InputStream from the Stream of Data that is represented in Textreport. The DataSource is with MTOM Mechanisum convertabel.
				//--------------------------------------------------------------------------------------------------------------------------------------
				InputStream inputStream = response.getBody().getReport().getReportdata().getTextreport().getInputStream();
				//ctx.out().println(inputStream.toString());
				/**
				 * Delete if File exists  
				 * This Code generate a File from the inputStream.
				 * The Datareader is for the XML Body. 
				 */

//				File file2 = new File("owcrefo");
				File file = new File("win/owcrefo/pdf/"+response.getBody().getIdentificationnumber()+response.getBody().getProducttype().getKey()+response.getBody().getCalltime() +".pdf");
				if (file.exists()) {
					if (file.delete()) {
					}
				}
//				FileUtils.forceMkdir(file2);
/*				Enumeration<NetworkInterface> netInter = NetworkInterface.getNetworkInterfaces();
				while(netInter.hasMoreElements()) {
					NetworkInterface net = NetworkInterface.getNetworkInterfaces().nextElement();
					for (InetAddress adress : Collections.list(net.getInetAddresses())) {
						if(!adress.isLoopbackAddress()){
							if(!adress.getHostAddress().equals("127.0.0.1")){
								IP = adress.getHostAddress();
								break;
							}
						}
					}
				}*/
				FileUtils.copyInputStreamToFile(inputStream, file);
				if(file.exists()){
					//ctx.out().println(file.getPath());
					head.setTab("PDF Anhang");
					head.setString("Ckcrereportpdf","file://abas/erp/win/owcrefo/"+response.getBody().getIdentificationnumber()+response.getBody().getProducttype().getKey()+response.getBody().getCalltime()+".pdf");
					head.setString("Ckcredatv","file://abas/erp/win/owcrefo/"+response.getBody().getIdentificationnumber()+response.getBody().getProducttype().getKey()+response.getBody().getCalltime()+".pdf");

				}
			} catch (Exception e) {
				FO.box("Fehler",e.toString());
			}
		}catch(Exception e){
			e.getMessage();
		}
		finally{
			if(editor.active()||editor!=null){
				editor.abort();
				editor = null;
			}
		}
		
	}
}
