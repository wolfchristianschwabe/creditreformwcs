package de.ck.app.ckcre.cws.infosystem.event;
/**
 * @author christian.schwabe
 * @version 0.0.0.1
 */

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRESEARCH;
import de.abas.erp.db.schema.customer.Customer;

/**
 * @author christian.schwabe
 * @param: The Parameter that are Used Here are the Head of the IS.
 * @do The Method sets the Customer Fieldvalues in the Fields of the IS CKCRESEARCH
 */
public class SetCustomer {
    public void Customerset(Customer cust, Object head, DbContext ctx){
        String affix = "";
        String number ="";
        if(head instanceof CKCREORDERS){
            new CkcreordersUtil().clear((CKCREORDERS)head);
            if(cust.getAddr()!=null&&!cust.getAddr().matches("")) {
                if(cust.getStreet()!=null&&!cust.getStreet().matches("")) {
                     if (cust.getStreet().split("[\\d]").length>1)
                        affix = cust.getStreet().split("[\\d]")[1];
                  number = cust.getStreet().replaceAll("[\\D]","");
                }
                ((CKCREORDERS) head).setCkcrecompany(cust.getAddr());
                ((CKCREORDERS) head).setCkcrecrefoid(cust.getString("Yckcreident"));
                if(cust.getTown()!=null&&!cust.getTown().matches(""))
                    ((CKCREORDERS) head).setCkcrekcity(cust.getTown());
                // if(cust.getStateOfTaxOffice().getCtryCode2Char()!=null&&!cust.getStateOfTaxOffice().getCtryCode2Char().matches(""))
                //    ((CKCREORDERS) head).setCkcrekcountry(cust.getStateOfTaxOffice().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                    ((CKCREORDERS) head).setCkcrekhousnumber(number);
                if(affix!=null&&!affix.matches(""))
                    ((CKCREORDERS) head).setCkcrehousnumberaff(affix);
                if(cust.getZipCode()!=null&&!cust.getZipCode().matches(""))
                    ((CKCREORDERS) head).setCkcrekpostcode(cust.getZipCode());
                if(cust.getRegion()!=null&&!cust.getRegion().toString().matches(""))
                    ((CKCREORDERS) head).setCkcrekregion(cust.getRegion().getCode());
                if(cust.getStreet()!=null&&!cust.getStreet().matches(""))
                    ((CKCREORDERS) head).setCkcrekstreet(cust.getStreet().split("[\\d]")[0]);
                return;
            }if(cust.getAddr2()!=null&&!cust.getAddr2().matches("")) {
                if (cust.getStreet2().split("[\\d]").length>1)
                    affix = cust.getStreet2().split("[\\d]")[1];
                number = cust.getStreet2().replaceAll("[\\D]","");
                ((CKCREORDERS) head).setCkcrecompany(cust.getAddr2());
                if(cust.getTown2()!=null&&!cust.getTown2().matches(""))
                    ((CKCREORDERS) head).setCkcrekcity(cust.getTown2());
                ((CKCREORDERS) head).setCkcrecrefoid(cust.getString("Yckcreident"));
                // if(cust.getStateOfTaxOffice2().getCtryCode2Char()!=null&&!cust.getStateOfTaxOffice2().getCtryCode2Char().matches(""))
                //      ((CKCREORDERS) head).setCkcrekcountry(cust.getStateOfTaxOffice2().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                ((CKCREORDERS) head).setCkcrekhousnumber(number);
                if(affix!=null&&!affix.matches(""))
                ((CKCREORDERS) head).setCkcrehousnumberaff(affix);
                if(cust.getZipCode2()!=null&&!cust.getZipCode2().matches(""))
                ((CKCREORDERS) head).setCkcrekpostcode(cust.getZipCode2());
                if(cust.getRegion2().getCode()!=null&&!cust.getRegion2().getCode().matches(""))
                    ((CKCREORDERS) head).setCkcrekregion(cust.getRegion2().getCode());
                if(cust.getStreet2()!=null&&!cust.getStreet2().matches(""))
                ((CKCREORDERS) head).setCkcrekstreet(cust.getStreet2().split("[\\d]")[0]);

                return;
            }else {
                FO.box("Fehler","Der Kunde ist nicht korrekt angelegt.");
            }

        }
        if(head instanceof CKCRESEARCH){
           // new CkcreSearchUtil().Clear((CKCRESEARCH)head);
            if(cust.getAddr()!=null&&!cust.getAddr().matches("")) {
                if(cust.getStreet()!=null&&!cust.getStreet().matches("")) {
                    if (cust.getStreet().split("[\\d]").length>1)
                        affix = cust.getStreet().split("[\\d]")[1];
                    number = cust.getStreet().replaceAll("[\\D]","");
                }
                ((CKCRESEARCH) head).setCkcrecompany(cust.getAddr());
                ((CKCRESEARCH) head).setCkcresearchid(cust.getString("Yckcreident"));
                if(cust.getTown()!=null&&!cust.getTown().matches(""))
                    ((CKCRESEARCH) head).setCkcrecity(cust.getTown());
                // if(cust.getStateOfTaxOffice().getCtryCode2Char()!=null&&!cust.getStateOfTaxOffice().getCtryCode2Char().matches(""))
                //    ((CKCRESEARCH) head).setCkcrecountry(cust.getStateOfTaxOffice().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                    ((CKCRESEARCH) head).setCkcrehousenumber(number);
                if(affix!=null&&!affix.matches(""))
                    ((CKCRESEARCH) head).setCkcrehousenumberaf(affix);
                if(cust.getZipCode()!=null&&!cust.getZipCode().matches(""))
                    ((CKCRESEARCH) head).setCkcrepostcode(cust.getZipCode());
                if(cust.getRegion()!=null&&!cust.getRegion().toString().matches(""))
                    ((CKCRESEARCH) head).setCkcreregion(cust.getRegion().getCode());
                if(cust.getStreet()!=null&&!cust.getStreet().matches(""))
                    ((CKCRESEARCH) head).setCkcrestreet(cust.getStreet().split("[\\d]")[0]);
                return;
            }if(cust.getAddr2()!=null&&!cust.getAddr2().matches("")) {
                if (cust.getStreet2().split("[\\d]").length>1)
                    affix = cust.getStreet2().split("[\\d]")[1];
                number = cust.getStreet2().replaceAll("[\\D]","");
                ((CKCRESEARCH) head).setCkcrecompany(cust.getAddr2());
                ((CKCRESEARCH) head).setCkcresearchid(cust.getString("Yckcreident"));
                if(cust.getTown2()!=null&&!cust.getTown2().matches(""))
                    ((CKCRESEARCH) head).setCkcrecity(cust.getTown2());
                // if(cust.getStateOfTaxOffice2().getCtryCode2Char()!=null&&!cust.getStateOfTaxOffice2().getCtryCode2Char().matches(""))
                //     ((CKCRESEARCH) head).setCkcrecountry(cust.getStateOfTaxOffice2().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                    ((CKCRESEARCH) head).setCkcrehousenumber(number);
                if(affix!=null&&!affix.matches(""))
                    ((CKCRESEARCH) head).setCkcrehousenumberaf(affix);
                if(cust.getZipCode2()!=null&&!cust.getZipCode2().matches(""))
                    ((CKCRESEARCH) head).setCkcrepostcode(cust.getZipCode2());
                if(cust.getRegion2().getCode()!=null&&!cust.getRegion2().getCode().matches(""))
                    ((CKCRESEARCH) head).setCkcreregion(cust.getRegion2().getCode());
                if(cust.getStreet2()!=null&&!cust.getStreet2().matches(""))
                    ((CKCRESEARCH) head).setCkcrestreet(cust.getStreet2().split("[\\d]")[0]);
                return;
            }else {
                FO.box("Fehler","Der Kunde ist nicht korrekt angelegt.");
            }
        }
    }

}
