package de.ck.app.ckcre.cws.infosystem.event;

/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRESEARCH;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.*;

/**
 * @author christian.schwabe
 * @param: head is from the IS CKCRESEARCH the ctx is the DatabaseContext in withc the IS is loaded.
 * @do This Method is for the Searchrequest to the Creditreform webservice.
 */

public class Search {
    public void Search(CKCRESEARCH head, DbContext ctx){
        Trequestheader header = new Trequestheader();
        Tsearchrequestbody body = new Tsearchrequestbody();
        Tphonerequest phone= new Tphonerequest();
        SearchResponse search = null ;
        CKCRESEARCH.Row row = null;
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        PasswordEditor editor=null;
        try {
            editor = operatorCode.createEditor();
            editor.open(EditorAction.VIEW);
        }catch(Exception e){
            FO.box("Fehler","Fehler beim Abruf der Daten f\u00fcr Nutzer: "+operatorCode.getIdno());
            LoggerUtil.LOG.error("Fehler beim Abruf der Nutzerdaten durch den Editor",e);
        }

        try {
            header =  new Logonrequest().buildupLogin(editor.getYckcregenpwd(), editor.getYckcrepw(), editor.getYckcrelang(), editor.getYckcreuser(), Integer.parseInt(editor.getYckcrekeylist()));

        }catch(Exception e){
            FO.box("Fehler",e.getMessage());
            LoggerUtil.LOG.error("Fehler beim Login in den Webservice",e);
            return;
        }finally{

            if(editor.active()||editor!=null){
                editor.abort();
                editor = null;
            }
        }
        try {
            if (head.getCkcrecity() != null && !head.getCkcrecity().equals(""))
                body.setCity(head.getCkcrecity());
            if (head.getCkcrecompany() != null && !head.getCkcrecompany().equals(""))
                body.setCompanyname(head.getCkcrecompany());
            if (head.getCkcrecountry() != null && !head.getCkcrecountry().equals("")) {
                body.setCountry(head.getCkcrecountry());
               }
            if (head.getCkcreemail() != null && !head.getCkcreemail().equals(""))
                body.setEmail(head.getCkcreemail());
            if (head.getCkcrehousenumber() != null && !head.getCkcrehousenumber().equals(""))
                body.setHousenumber(Integer.parseInt(head.getCkcrehousenumber()));
            if (head.getCkcrehousenumberaf() != null && !head.getCkcrehousenumberaf().equals(""))
                body.setHousenumberaffix(head.getCkcrehousenumberaf());
            if (head.getCkcresearchid() != null && !head.getCkcresearchid().equals("") && head.getCkcresearchid().matches("^\\d+"))
                body.setIdentificationnumber(head.getCkcresearchid());
            if (head.getCkcrelegalform() != null && !head.getCkcrelegalform().equals(""))
                body.setLegalform(head.getCkcrelegalform());
            if (head.getCkcrephonedial() != null && head.getCkcrephonedial().equals(""))
                       phone.setDiallingcode(head.getCkcrephonedial());
            if (head.getCkcrephonenum() != null && head.getCkcrephonenum().equals(""))
                       phone.setPhonenumber(head.getCkcrephonenum());
            if (phone != null && phone.equals(null))
                       body.setPhone(phone);
            if (head.getCkcrepostcode() != null && !head.getCkcrepostcode().equals(""))
                body.setPostcode(head.getCkcrepostcode());
            if (head.getCkcreregion() != null && !head.getCkcreregion().equals(""))
                body.setRegion(head.getCkcreregion());
            if (head.getCkcreregisterid() != null && !head.getCkcreregisterid().equals(""))
                body.setRegisterid(head.getCkcreregisterid());
            if (head.getCkcreregistertype() != null && !head.getCkcreregistertype().equals(""))
                body.setRegistertype(head.getCkcreregistertype());
            if (head.getCkcrevatid() != null && !head.getCkcrevatid().equals(""))
                body.setVatid(head.getCkcrevatid());
            if (head.getCkcresearchtype() != null && !head.getCkcresearchtype().equals(""))
                body.setSearchtype(head.getCkcresearchtype());
            if (head.getCkcrestreet() != null && !head.getCkcrestreet().equals(""))
                body.setStreet(head.getCkcrestreet());
            search = (SearchResponse)Services.Search.start(body, header);
        }catch (Exception e){
            FO.box("Fehler", ""+e.getMessage());
            LoggerUtil.LOG.error("Fehler bei Search Anfrage an Webservice",e);
        }
        if(search.getBody().isMorehits()){
            FO.box("Meldung","Es gibt mehr Treffer als angezeigt werden, bitte pr\u00e4zisieren sie ihre Angaben.");
        }
        head.table().clear();
        for(Tsearchhit hit : search.getBody().getHit()){
            if(hit!=null) {
                row = head.table().appendRow();
                if (hit.isAdditionalinformation() != null)
                    row.setCkcradditionalinfo(hit.isAdditionalinformation());
                if (hit.getAddressrange() != null)
                    row.setCkcreaddressrange(hit.getAddressrange().getDesignation());
                if (hit.getIdentificationnumber() != null)
                    row.setCkcreid(hit.getIdentificationnumber());
                if (hit.getEasynumber() != null)
                    row.setCkcreeasynumber(hit.getEasynumber());
                if (hit.getCompanyname() != null)
                    row.setCkcrename(hit.getCompanyname());
                if (hit.isReportavailable() != null)
                    row.setCkcreportavailab(hit.isReportavailable());
                if (hit.isReportaddress() != null)
                    row.setCkcrereportaddress(hit.isReportaddress());
                if (hit.isReportcompanyname() != null)
                    row.setCkcrereportcompany(hit.isReportcompanyname());
                if (hit.getLegalform() != null)
                    row.setCkcretlegalform(hit.getLegalform().getDesignation());
                if (hit.getOperationalstatus() != null)
                    row.setCkcreoperationalst(hit.getOperationalstatus().getDesignation());
                if (hit.getCountry() != null) {
                    row.setCkcretland(hit.getCountry().getDesignation());
                    row.setString("Ckcretlandk",hit.getCountry().getKey());
                }
                if (hit.getCity() != null)
                    row.setCkcretort(hit.getCity());
                if (hit.getStatus() != null)
                    row.setCkcretstatus(hit.getStatus().getDescription());
                if (hit.getStreet() != null)
                    row.setCkcretstreet(hit.getStreet());
                if(hit.getHousenumber()!=null){
                    row.setString("ckcrehousnumber",hit.getHousenumber().toString());
                }
                if(hit.getHousenumberaffix()!=null){
                    row.setString("ckcrehousnumberaf",hit.getHousenumberaffix());
                }
                if(hit.getPostcode()!=null){
                    row.setString("ckcretpostcode",hit.getPostcode());
                }

            }
        }
    }
}
