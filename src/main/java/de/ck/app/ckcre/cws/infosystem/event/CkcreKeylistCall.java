package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody;
import org.apache.log4j.Logger;

public class CkcreKeylistCall {

    Logger l = LoggerUtil.LOG;

    public Tkeylistresponsebody Call(DbContext ctx) {
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        PasswordEditor editor=null;
        Tkeylistresponsebody response =null;
        Logonrequest logon=new Logonrequest();
        try {
            editor = operatorCode.createEditor();
            editor.open(EditorAction.VIEW);
            response = (Tkeylistresponsebody) Services.Keylist.start(null, logon.buildupLogin(editor.getYckcregenpwd(), editor.getYckcrepw(), editor.getYckcrelang(), editor.getYckcreuser(), Integer.parseInt(editor.getYckcrekeylist())));
            return response;
        }catch (Exception e){
            FO.box("Fehler",e.getMessage());
            l.error("Fehler bei Keylist auslesen",e);
        }finally {
            if(editor!=null||editor.active()){
                editor.abort();
                editor=null;
              }
        }
        return response;
    }
}
