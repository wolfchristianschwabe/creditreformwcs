package de.ck.app.ckcre.cws.infosystem.event.impl;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.common.type.enums.EnumTreeViewStatus;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReportView;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.customer.SelectableCustomer;
import de.abas.erp.db.selection.Conditions;
import de.abas.erp.db.selection.SelectionBuilder;
import de.abas.erp.db.util.QueryUtil;
import de.abas.jfop.base.buffer.BufferFactory;
import de.ck.app.ckcre.cws.util.Constants;
import de.ck.app.ckcre.cws.util.Keylist;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import org.apache.log4j.Logger;

import java.io.File;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * TODO: Insert Description!
 * Project: Creditreform
 * Package: de.ck.app.ckcre.cws.infosystem.event.impl
 * Created: 15.04.2019 11:52
 *
 * @author = manuel.ott
 * @since = 15. April 2019
 */
public class TreeViewBuilder {

    CrefoReportView head;
    Logger l = LoggerUtil.LOG;
    DbContext ctx;

    private final String NO_CUSTOMER_HEADER_KEY = "NOCUSTOMERHEADERS";

    /**
     * Aufbau:
     *     (Identnummer)
     *          DATUM (MM/yyyy = 04/2019)
     *              PRTY
     *                  DATEI001
     *                  DATEI002
     *                  .
     *                  .
     *                  DATEI_N
     */
    public final HashMap<String, HashMap<String, HashMap<String, LinkedList<File>>>> tree = new HashMap<>();

    public TreeViewBuilder(DbContext ctx, CrefoReportView crefoReportView){
        head = crefoReportView;
        this.ctx = ctx;
    }

    public void buildTree(){
        buildTree(head.getKunde() == null);
    }

    /**
     * Baut Baumstruktur auf.
     * PDF-Namensaufbau/Beispiel: 1033123456_PRTY-5_20190415113816.pdf
     * @param addCustomerHeaders Gruppierung nach Kundennummer vornehmen
     */
    private void buildTree(boolean addCustomerHeaders){
        head.table().clear();

        File pdfs = new File("owcrefo/pdf/");
        if(pdfs.listFiles() == null) return;

        // HashMaps aufbauen im Hintergrund (RAM):
//        Arrays.stream(pdfs.listFiles()).forEach( file -> {
        final String crefoident = head.getKunde() != null ? head.getKunde().getString("yckcreident") : "";

        Arrays.stream(pdfs.listFiles( (x) -> x != null && x.getName().matches(".*" + crefoident + "_.*.[pPdDfF]{3}"))).forEach( file -> {
            try{
                l.debug("Found File: " + file.getAbsolutePath());

                String ident = file.getName().split("_")[0];
                String PRTY = file.getName().split("_")[1];
                String datetime = file.getName().split("_")[2].split("\\.")[0];

                try{
                    l.debug("Passe Werte für TreeView an..");
                    datetime = LocalDateTime.parse(datetime, DateTimeFormatter.ofPattern("yyyyMMddHHmmss")).format(DateTimeFormatter.ofPattern("MM/yyyy"));
                    SelectableCustomer cust = QueryUtil.getFirst(ctx, SelectionBuilder.create(SelectableCustomer.class).add(Conditions.eq(Customer.META.yckcreident.getName(), ident)).build());
                    ident = cust.getIdno() + " - " + cust.getString(Customer.META.descr.getName());

                    //G|ispri
                    String lang = BufferFactory.newInstance().getGlobalTextBuffer().getStringValue("ispri");

                    PRTY = Keylist.findDesignationByKey(Constants.keylist, PRTY, lang);
                }catch(Exception e){
                    l.error("Fehler beim Anpassen der TreeDescriptions!", e);
                }

                if(!addCustomerHeaders) ident = NO_CUSTOMER_HEADER_KEY;

                tree.computeIfAbsent(ident, k -> new HashMap<>());
                HashMap<String, HashMap<String, LinkedList<File>>> custHeaders = tree.get(ident);

                custHeaders.computeIfAbsent(datetime, k -> new HashMap<>());
                HashMap<String, LinkedList<File>> prtyHeaders = custHeaders.get(datetime);

                prtyHeaders.computeIfAbsent(PRTY, k -> new LinkedList<>());
                LinkedList<File> datetimeHeaders = prtyHeaders.get(PRTY);

                datetimeHeaders.add(file);
            }catch(Exception e){
                l.error("Fehler beim Aufbauen des TreeView'!", e);
            }
        });

        _buildTree(tree, 0);
    }

    private void _buildTree(Object tree, int level){
//        CrefoReportView.Row cR = head.table().appendRow();

        if(tree instanceof HashMap){
            ((HashMap<String, Object>)tree).keySet().stream().sorted( (curr, next) -> {
                //Sortierung nach Monat/Jahr!
                try{
                    l.debug("curr: " + curr + ", next: " + next);
                    LocalDate ld1 = LocalDate.parse("01/" + curr, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                    LocalDate ld2 = LocalDate.parse("01/" + next, DateTimeFormatter.ofPattern("dd/MM/yyyy"));
                    l.debug("Comparing Dates.. " + ld1 + " mit " + ld2);
                    return -ld1.compareTo(ld2);
                }catch(Exception ignored){}
                return curr.compareTo(next);
            }).forEachOrdered( (key) -> {
                if(key.compareTo(NO_CUSTOMER_HEADER_KEY) != 0){
                    CrefoReportView.Row cR = head.table().appendRow();
                    cR.setTreeLevel(level);
                    cR.setTreeElemDescr(key);

                    //Icon setzen
                    if(key.matches( "[0-9]{2}/[0-9]{2,4}"))
                        cR.setTreeElemIcon("icon:date");
                    else if(key.matches("[0-9]{3,} - [A-Za-z0-9]{2,}.*")){
                        cR.setTreeElemIcon("icon:user");
                    }else{
                        cR.setTreeElemIcon("icon:hierarchy");
                    }
                }
                _buildTree( ((HashMap<String, Object>) tree).get(key), level + 1 );
                //BACKTRACING: Rückwärts zuklappen!
                //TODO ?
//                FO.println(cR.getTreeElemDescr() + ": " + cR.getTreeElemStatus().name());
//                if(cR.getTreeElemStatus() == EnumTreeViewStatus.Minus){
//                    cR.setTreeElemStatus(EnumTreeViewStatus.Plus);
////                    cR.invokeTreeElemExpandCollapse();
//                }
            });
        }else if(tree instanceof LinkedList){
            ((LinkedList<File>)tree).forEach( file -> {
                CrefoReportView.Row cR = head.table().appendRow();
                cR.setTreeLevel(level);
                cR.setTreeElemDescr(file.getAbsolutePath());

                //Icon für die Reports:
                cR.setTreeElemIcon("icon:text_empty");
            });
        }

    }



}
