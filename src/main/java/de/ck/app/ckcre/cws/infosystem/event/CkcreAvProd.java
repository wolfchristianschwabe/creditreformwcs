package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREPRODP;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRESEARCH;
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReport;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.customer.CustomerEditor;
import de.abas.erp.db.schema.customer.Prospect;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.db.schema.vendor.VendorEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tproductavailabilityrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tproductavailabilityresponsebody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

public class CkcreAvProd {
    /**
     * @author christian.schwabe
     * @param: ctx is the Database Context witch the IS is loaded in
     * @do this Method provides the Webservice Service that is needed to check Productavailability and to import this in Abas Customerdatabase
     */
        public Tproductavailabilityresponsebody Prodpruef(Object head, DbContext ctx){
            Trequestheader header= new Trequestheader();
            Tproductavailabilityrequestbody request = new Tproductavailabilityrequestbody();
            Tproductavailabilityresponsebody prod = null;
            CKCREPRODP.Row row ;
            CustomerEditor custeditor = null;
            VendorEditor vendorEditor = null;
            Customer cust = null;
            Vendor vendor = null;
            Prospect pros = null;
            String products="Produktname : Produkttyp\n";
            if(head instanceof CKCREORDERS) {
                request.setIdentificationnumber(((CKCREORDERS) head).getCkcrecrefoid());
            } if(head instanceof CKCRESEARCH) {
                request.setIdentificationnumber(((CKCRESEARCH) head).getCkcresearchid());
            }if(head instanceof CrefoReport) {
                request.setIdentificationnumber(((CrefoReport) head).getCrefonum());
            }else {
             return null;
            }
            final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
            PasswordEditor editor=null;
            try {
                editor = operatorCode.createEditor();
                editor.open(EditorAction.VIEW);
            }catch(Exception e){
                FO.box("Fehler","Fehler beim Abruf der Daten f\u00fcr Nutzer: "+operatorCode.getIdno());
                LoggerUtil.LOG.error("Fehler beim Abruf der Nutzerdaten durch den Editor",e);
            }

            try {
                header =  new Logonrequest().buildupLogin(editor.getYckcregenpwd(), editor.getYckcrepw(), editor.getYckcrelang(), editor.getYckcreuser(), Integer.parseInt(editor.getYckcrekeylist()));
            }catch(Exception e){
                LoggerUtil.LOG.error("Kann editor nicht laden",e);
            }finally{
                if(editor.active()||editor!=null){
                    editor.abort();
                    editor = null;
                }
            }
            try{
                prod = (Tproductavailabilityresponsebody) Services.ProductAvailability.start(request,header);
            }catch(Exception e){
                LoggerUtil.LOG.error("Kann editor nicht laden",e);
            }
            return prod;
        }
    }

