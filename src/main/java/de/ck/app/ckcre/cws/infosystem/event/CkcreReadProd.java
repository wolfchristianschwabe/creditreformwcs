package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tlogonresponsebody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tproductavailabilityresponsebody;

public class CkcreReadProd {
    public void read(CKCREORDERS head, DbContext ctx){
        Tlogonresponsebody response = new LogonabasCWS().LogonabasCWS(ctx);
        Tproductavailabilityresponsebody prod = new CkcreAvProd().Prodpruef(head,ctx);
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        PasswordEditor editor=null;
        try {
            editor = operatorCode.createEditor();
            editor.open(EditorAction.VIEW);
            CKCREORDERSEventHandler.constrainslang = new GetConstrain().GetServiceConstrain(editor.getYckcrelang(), "order", ctx, response, prod);
            if(head.getCkcreprodtype()!=null&&!head.getCkcreprodtype().matches(""))
            CKCREORDERSEventHandler.constrains = new GetConstrain().GetConstrains(head.getCkcreprodtype(), "order", ctx, response,prod);
            CKCREORDERSEventHandler.constrainstate = new GetConstrain().GetState(editor.getYckcrelang(), "order", ctx, response);
            editor.commit();
        }catch (Exception e){
            FO.box("Fehler", "Fehler beim auslesen der Produktdaten.");
            LoggerUtil.LOG.error("FehlerID", e);
        }finally{
            editor.commit();
            if(editor!=null&&editor.active()){
                editor.abort();
                editor=null;
            }
        }
        if(head.getCkcreprodtype()==null||head.getCkcreprodtype().matches("")){
            head.setCkcrelegitimateint("");
            head.setCkcrelegitimatklar("");
        }
    }
}
