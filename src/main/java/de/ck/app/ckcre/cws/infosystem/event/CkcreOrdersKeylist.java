package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.gui.MenuBuilder;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.LogonResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.util.HashMap;
/**
 * @author christian.schwabe
 * @do This Class reads the Hashmap of informations generated in GetConstrain and give back a Menue for better Use.
 */
public class CkcreOrdersKeylist {
    public void Keylistreturn(CKCREORDERS head, DbContext ctx, ButtonEvent event, HashMap<String,Object> keylist,HashMap<String,String> keylist2) {
        Logonrequest logon = new Logonrequest();
        Trequestheader login = null;
        String temp;
        int count=0;
        LogonResponse response = null;
        PasswordEditor editor = null;
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        MenuBuilder<String> menue = new MenuBuilder<String>(ctx, "Ausw\u00e4hlen");
        try {
           editor = operatorCode.createEditor();
           editor.open(EditorAction.VIEW);
        }catch(Exception e){
            ctx.out().println(e.getMessage());
        }
        if (event.getFieldName().matches("ckcrelang")) {
            try {
                menue = new MenuBuilder<String>(ctx, "Ausw\u00e4hlen");
                menue.addItem("","");
                for (String key : keylist.keySet()) {
                    menue.addItem(key, keylist2.get(key));
                }
                temp = menue.show();
                if (temp == null)
                    return;
                head.setCkcrekcountry(temp.toUpperCase());
            } catch (Exception e) {
                FO.box("Fehler", e.getMessage());
                LoggerUtil.LOG.error("Fehler Keylist kann nicht gelesen werden", e);
            } finally {
                if (editor != null || editor.active()) {
                    editor.abort();
                    editor = null;
                }
            }
        }
            if (event.getFieldName().matches("ckcrelanguagew")) {
                    try {
                        menue = new MenuBuilder<String>(ctx, "Ausw\u00e4hlen");
                        menue.addItem("","");
                               menue.addItem("DE", "DE");
                                menue.addItem("EN", "EN");
                                menue.addItem("FR", "FR");
                                menue.addItem("IT", "IT");
                                menue.setTitle("Language ausw\u00e4hlen:");
                         temp = menue.show();
                              if(temp == null)
                                  return;
                    head.setCkcrereportlang(temp.toUpperCase());
                    } catch (Exception e) {
                        FO.box("Fehler",e.getMessage());
                        LoggerUtil.LOG.error("Fehler Keylist kann nicht gelesen werden",e);
                    } finally {
                        if (editor != null || editor.active()) {
                            editor.abort();
                            editor = null;
                        }
                    }

                }
        if (event.getFieldName().matches("ckcreordertypew")) {

            try {
                if (editor != null) {
                    menue.addItem("","");
                    for (String key : ((HashMap<String,String>)keylist.get(head.getCkcrereportlang())).keySet()) {
                        if (key.contains("ORTY"))
                            menue.addItem(key,((HashMap<String,String>)keylist.get(head.getCkcrereportlang())).get(key));
                    }
                    temp = menue.show();
                    if(temp==null){
                        return;
                    } else {
                          head.setString("Ckcreordertypeklar",((HashMap<String, String>) keylist.get(head.getCkcrereportlang())).get(temp));
                          head.setString("Ckcreordertype",temp);

                    }
                }else{ FO.box("Kann Ordertyp nicht auslesen");}
            } catch(Exception e){
                FO.box("Fehler","Fehler beim abruf des Ordertyps"+e.getMessage());
                LoggerUtil.LOG.error("Fehler Keylist kann nicht gelesen werden",e);
                } finally{
                    if (editor != null || editor.active()) {
                        editor.abort();
                        editor = null;
                    }
                }

        }

                if (event.getFieldName().matches("ckcreprodtypwbn")) {

                    try {
                        menue.addItem("","");
                        for (String key : ((HashMap<String,String>)keylist.get(head.getCkcrereportlang())).keySet()) {
                            if (key.contains("PRTY"))
                                menue.addItem(key,((HashMap<String,String>)keylist.get(head.getCkcrereportlang())).get(key));
                        }
                        temp = menue.show();
                        if(temp==null){
                            return;
                        }else {
                            head.setString("Ckcreprodtypeklar",((HashMap<String, String>) keylist.get(head.getCkcrereportlang())).get(temp));
                                head.setString("Ckcreprodtype",temp);
                            }
                    } catch (Exception e) {
                        FO.box("Fehler","Fehler beim abruf des Produkttyps"+e.getMessage());
                        LoggerUtil.LOG.error("Fehler Keylist kann nicht gelesen werden",e);
                    } finally {
                        if (editor != null || editor.active()) {
                            editor.abort();
                            editor = null;
                        }
                    }

                }
                if (event.getFieldName().matches("ckcrelegitimintw")) {
                    try {
                        menue.addItem("","");
                        for (String key : keylist2.keySet()){
                            if (key.contains("LEIN"))
                                menue.addItem(key,keylist2.get(key));
                        }
                        temp = menue.show();
                        if(temp==null){
                            return;
                        }else {
                            head.setString("ckcrelegitimatklar", keylist2.get(temp));
                            head.setString("Ckcrelegitimateint",(temp));
                        }
                    } catch (Exception e) {
                        FO.box("Fehler","Fehler beim abruf des Legitimierungstyps"+e.getMessage());
                        LoggerUtil.LOG.error("Fehler Keylist kann nicht gelesen werden",e);
                    } finally {
                        if (editor != null || editor.active()) {
                            editor.abort();
                            editor = null;
                        }
                    }
                }
        if (event.getFieldName().matches("ckcrelegalformbn")) {
            try {
                menue.addItem("","");
                for (String key : ((HashMap<String,String>)keylist.get(head.getCkcrereportlang())).keySet()) {
                    if (key.contains("LEFO"))
                        menue.addItem(key,((HashMap<String,String>)keylist.get(head.getCkcrereportlang())).get(key));
                }
                temp = menue.show();
                if(temp==null){
                    return;
                }else {
                    head.setString("Ckcrelegalformklar",((HashMap<String, String>) keylist.get(head.getCkcrereportlang())).get(temp));
                    head.setString("Ckcrelegalform",temp);
                }
            } catch (Exception e) {
                FO.box("Fehler","Fehler beim abruf des Ordertyps"+e.getMessage());
                LoggerUtil.LOG.error("Fehler Keylist kann nicht gelesen werden",e);
            } finally {
                if (editor != null || editor.active()) {
                    editor.abort();
                    editor = null;
                }
            }
        }

            }
}
