package de.ck.app.ckcre.cws.infosystem.event;
/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN.Row;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.*;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody.Keylist.Keygroup;

/**
 * @author christian.schwabe
 * @param: head is the IS Context and the ctx is the DatabaseContext in withc the Is is loaded
 * @do This Method reads the Keylist of the Webservice and Shows it on the Table of the IS
 */
public class CkcreKeylist {
    public void Keylistakt(CKCRELOGIN head, DbContext ctx) {
        Logonrequest logon = new Logonrequest();
        Trequestheader login = null;
        LogonResponse response = null;
        try {
            //genpw ="AgPw01!"
            login = logon.buildupLogin(head.getCkcregenpw(), head.getCkcrepasswd(), head.getCkcrelangchosen(), head.getCkcrelogin(), Integer.parseInt(head.getCkcrekeylist()));
            response = (LogonResponse) Services.Logon.start(login, login);
            if(response!=null){
               head.setCkcrekeylist(response.getBody().getCurrentkeylistversion().toString());
                Tkeylistresponsebody responsekey = (Tkeylistresponsebody) Services.Keylist.start(logon, logon);
                responsekey.getKeylist().getKeygroup().stream().count();
                responsekey.getKeylist().getKeygroup().get(0).getKeygroupname();
            }
        } catch (Exception e) {
           FO.box(e.getMessage() + "\n Login gescheitert bitte geben sie alle Daten ein.");
            LoggerUtil.LOG.error("Fehler beim lesen der Nutzerdaten durch den Editor",e);
        }

    }

    public void Anz(CKCRELOGIN head,DbContext ctx) {
        Logonrequest logon = new Logonrequest();
        Trequestheader login = null;
        LogonResponse response = null;
        head.table().clear();
        Row row = head.table().appendRow();
        try{
            //Integer.parseInt(head.getCkcrekeylist()
            login = logon.buildupLogin(head.getCkcregenpw(), head.getCkcrepasswd(), head.getCkcrelangchosen(), head.getCkcrelogin(), 27);
       // response = (LogonResponse) Services.Logon.start(login, login);
            //head.setCkcrekeylist(response.getBody().getCurrentkeylistversion().toString());
            Tkeylistresponsebody responsekey = (Tkeylistresponsebody) Services.Keylist.start(login, login);
            //FO.box(responsekey.getKeylist().getVersion().toString());
            for(Keygroup key : responsekey.getKeylist().getKeygroup()){
                for(Keygroup.Keyentry key2: key.getKeyentry()){
                    for(Tkeyattribute key3 : key2.getKeyattribute()) {
                        for(Tkeyattributevalue key4 : key3.getKeyattributevalue()) {
                      //      ctx.out().println(key.getKeygroupname()+key.getKeygrouptype()+key3.getKeyattributename());
                            row.setCkcrekeygroupname(key.getKeygroupname());
                            row.setCkcrekeygrouptype(key.getKeygrouptype());
                            row.setCkcrekeyattributen(key3.getKeyattributename());
                            row.setCkcrelanguage(key4.getLanguage());
                            row.setCkcreeyattributeva(key4.getValue());
                            row = head.table().appendRow();
                        }
                    }
                }
            }
         } catch (Exception e) {
               FO.box(e.getMessage() + "\n Keylistabruf gescheitert bitte Daten pr\u00fcfen.");
            LoggerUtil.LOG.error("Fehler beim Keylistabruf",e);
        }

    }
}
