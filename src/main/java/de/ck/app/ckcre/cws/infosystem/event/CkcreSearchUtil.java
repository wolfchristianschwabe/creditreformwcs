package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.erp.db.infosystem.custom.owcrefo.CKCRESEARCH;
/**
 * @author christian.schwabe
 * @do This Class clears the Search IS.
 */
public class CkcreSearchUtil {

    public void Clear(CKCRESEARCH head) {
        head.setCkcreregistertype("");
        head.setCkcreregisterid("");
        head.setCkcrestreet("");
        head.setCkcreregion("");
        head.setCkcrepostcode("");
        head.setCkcrehousenumberaf("");
        head.setCkcrehousenumber("");
        head.setCkcrecountry("");
        head.setCkcrecity("");
        head.setCkcrecompany("");
        head.setCkcreemail("");
        head.setCkcrelegalform("");
        head.setCkcrephonedial("");
        head.setCkcrephonenum("");
        head.setCkcresearchid("");
        head.setCkcresearchtype("");
        head.setCkcrevatid("");
        head.setCkcrewebsite("");
        if(head.getString("Ckcrelief").matches("")) {
            head.setString("Ckcrelief", "");
        }
        if(head.getString("Ckcrekundea").matches("")) {
            head.setString("Ckcrekundea", "");
        }
    }
}
