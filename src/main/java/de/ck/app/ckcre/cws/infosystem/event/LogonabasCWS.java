package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.LogonResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tlogonresponsebody;

/**
 * @author christian.schwabe
 * @do This Class is for Login in the Webservice it Returns a Header with Information about the Services.
 */
public class LogonabasCWS {
    public Tlogonresponsebody LogonabasCWS(DbContext ctx){
        Tlogonresponsebody header =null;
        LogonResponse headerresp =null;
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        PasswordEditor editor=null;
        try {
            editor = operatorCode.createEditor();
            editor.open(EditorAction.VIEW);
            try {
                headerresp = (LogonResponse) Services.Logon.start(null,new Logonrequest().buildupLogin(editor.getYckcregenpwd(), editor.getYckcrepw(), editor.getYckcrelang(), editor.getYckcreuser(), Integer.parseInt(editor.getYckcrekeylist())));
                header = headerresp.getBody();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }catch(Exception e){
            FO.box("Fehler","Fehler beim Abruf der Daten f\u00fcr Nutzer: "+operatorCode.getIdno());
            LoggerUtil.LOG.error("Fehler beim Abruf der Nutzerdaten durch den Editor",e);
        }finally {
            editor.commit();
            if(editor!=null&&editor.active()){
                editor.abort();
                editor=null;
            }
            return header;
        }
    }

}
