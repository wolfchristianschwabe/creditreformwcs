package de.ck.app.ckcre.cws.infosystem.event;
/**
 * @author christian.schwabe
 * @version 0.0.0.1
 */

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRESEARCH;
import de.abas.erp.db.schema.vendor.Vendor;

/**
 * @author christian.schwabe
 * @param: The Parameter that are Used Here are the Head of the IS.
 * @do The Method sets the Customer Fieldvalues in the Fields of the IS CKCRESEARCH
 */
public class SetVendor {
    public void Vendorset(Vendor vendor, Object head, DbContext ctx){
        String affix = "";
        String number ="";
        if(head instanceof CKCREORDERS){
            new CkcreordersUtil().clear((CKCREORDERS)head);
            if(vendor.getAddr()!=null&&!vendor.getAddr().matches("")) {
                if(vendor.getStreet()!=null&&!vendor.getStreet().matches("")) {
                     if (vendor.getStreet().split("[\\d]").length>1)
                        affix = vendor.getStreet().split("[\\d]")[1];
                  number = vendor.getStreet().replaceAll("[\\D]","");
                }
                ((CKCREORDERS) head).setCkcrecompany(vendor.getAddr());
                ((CKCREORDERS) head).setCkcrecrefoid(vendor.getString("Yckcreident"));
                if(vendor.getTown()!=null&&!vendor.getTown().matches(""))
                    ((CKCREORDERS) head).setCkcrekcity(vendor.getTown());
                if(vendor.getStateOfTaxOffice().getCtryCode2Char()!=null&&!vendor.getStateOfTaxOffice().getCtryCode2Char().matches(""))
                    ((CKCREORDERS) head).setCkcrekcountry(vendor.getStateOfTaxOffice().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                    ((CKCREORDERS) head).setCkcrekhousnumber(number);
                if(affix!=null&&!affix.matches(""))
                    ((CKCREORDERS) head).setCkcrehousnumberaff(affix);
                if(vendor.getZipCode()!=null&&!vendor.getZipCode().matches(""))
                    ((CKCREORDERS) head).setCkcrekpostcode(vendor.getZipCode());
                if(vendor.getRegion()!=null&&!vendor.getRegion().toString().matches(""))
                    ((CKCREORDERS) head).setCkcrekregion(vendor.getRegion().getCode());
                if(vendor.getStreet()!=null&&!vendor.getStreet().matches(""))
                    ((CKCREORDERS) head).setCkcrekstreet(vendor.getStreet().split("[\\d]")[0]);
                return;
            }if(vendor.getAddr2()!=null&&!vendor.getAddr2().matches("")) {
                if (vendor.getStreet2().split("[\\d]").length>1)
                    affix = vendor.getStreet2().split("[\\d]")[1];
                number = vendor.getStreet2().replaceAll("[\\D]","");
                ((CKCREORDERS) head).setCkcrecompany(vendor.getAddr2());
                if(vendor.getTown2()!=null&&!vendor.getTown2().matches(""))
                    ((CKCREORDERS) head).setCkcrekcity(vendor.getTown2());
                if(vendor.getStateOfTaxOffice2().getCtryCode2Char()!=null&&!vendor.getStateOfTaxOffice2().getCtryCode2Char().matches(""))
                    ((CKCREORDERS) head).setCkcrekcountry(vendor.getStateOfTaxOffice2().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                ((CKCREORDERS) head).setCkcrekhousnumber(number);
                if(affix!=null&&!affix.matches(""))
                ((CKCREORDERS) head).setCkcrehousnumberaff(affix);
                if(vendor.getZipCode2()!=null&&!vendor.getZipCode2().matches(""))
                ((CKCREORDERS) head).setCkcrekpostcode(vendor.getZipCode2());
                if(vendor.getRegion2().getCode()!=null&&!vendor.getRegion2().getCode().matches(""))
                    ((CKCREORDERS) head).setCkcrekregion(vendor.getRegion2().getCode());
                if(vendor.getStreet2()!=null&&!vendor.getStreet2().matches(""))
                ((CKCREORDERS) head).setCkcrekstreet(vendor.getStreet2().split("[\\d]")[0]);
                return;
            }else {
                FO.box("Fehler","Der Lieferant ist nicht korrekt angelegt.");
            }

        }
        if(head instanceof CKCRESEARCH){
            //new CkcreSearchUtil().Clear((CKCRESEARCH)head);
            if(vendor.getAddr()!=null&&!vendor.getAddr().matches("")) {
                if(vendor.getStreet()!=null&&!vendor.getStreet().matches("")) {
                    if (vendor.getStreet().split("[\\d]").length>1)
                        affix = vendor.getStreet().split("[\\d]")[1];
                    number = vendor.getStreet().replaceAll("[\\D]","");
                }
                ((CKCRESEARCH) head).setCkcrecompany(vendor.getAddr());
                ((CKCRESEARCH) head).setCkcresearchid(vendor.getString("Yckcreident"));
                if(vendor.getTown()!=null&&!vendor.getTown().matches(""))
                    ((CKCRESEARCH) head).setCkcrecity(vendor.getTown());
                //if(vendor.getStateOfTaxOffice().getCtryCode2Char()!=null&&!vendor.getStateOfTaxOffice().getCtryCode2Char().matches(""))
                    //   ((CKCRESEARCH) head).setCkcrecountry(vendor.getStateOfTaxOffice().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                    ((CKCRESEARCH) head).setCkcrehousenumber(number);
                if(affix!=null&&!affix.matches(""))
                    ((CKCRESEARCH) head).setCkcrehousenumberaf(affix);
                if(vendor.getZipCode()!=null&&!vendor.getZipCode().matches(""))
                    ((CKCRESEARCH) head).setCkcrepostcode(vendor.getZipCode());
                if(vendor.getRegion()!=null&&!vendor.getRegion().toString().matches(""))
                    ((CKCRESEARCH) head).setCkcreregion(vendor.getRegion().getCode());
                if(vendor.getStreet()!=null&&!vendor.getStreet().matches(""))
                    ((CKCRESEARCH) head).setCkcrestreet(vendor.getStreet().split("[\\d]")[0]);
                return;
            }if(vendor.getAddr2()!=null&&!vendor.getAddr2().matches("")) {
                if (vendor.getStreet2().split("[\\d]").length>1)
                    affix = vendor.getStreet2().split("[\\d]")[1];
                number = vendor.getStreet2().replaceAll("[\\D]","");
                ((CKCRESEARCH) head).setCkcrecompany(vendor.getAddr2());
                ((CKCRESEARCH) head).setCkcresearchid(vendor.getString("Yckcreident"));
                if(vendor.getTown2()!=null&&!vendor.getTown2().matches(""))
                    ((CKCRESEARCH) head).setCkcrecity(vendor.getTown2());
                //if(vendor.getStateOfTaxOffice2().getCtryCode2Char()!=null&&!vendor.getStateOfTaxOffice2().getCtryCode2Char().matches(""))
                //   ((CKCRESEARCH) head).setCkcrecountry(vendor.getStateOfTaxOffice2().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                    ((CKCRESEARCH) head).setCkcrehousenumber(number);
                if(affix!=null&&!affix.matches(""))
                    ((CKCRESEARCH) head).setCkcrehousenumberaf(affix);
                if(vendor.getZipCode2()!=null&&!vendor.getZipCode2().matches(""))
                    ((CKCRESEARCH) head).setCkcrepostcode(vendor.getZipCode2());
                if(vendor.getRegion2().getCode()!=null&&!vendor.getRegion2().getCode().matches(""))
                    ((CKCRESEARCH) head).setCkcreregion(vendor.getRegion2().getCode());
                if(vendor.getStreet2()!=null&&!vendor.getStreet2().matches(""))
                    ((CKCRESEARCH) head).setCkcrestreet(vendor.getStreet2().split("[\\d]")[0]);
                return;
            }else {
                FO.box("Fehler","Der Lieferant ist nicht korrekt angelegt.");
            }
        }
    }

}
