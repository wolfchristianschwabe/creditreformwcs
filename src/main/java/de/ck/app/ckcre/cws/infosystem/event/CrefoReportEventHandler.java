package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.AppContext;
import de.abas.erp.api.commands.CommandFactory;
import de.abas.erp.api.commands.FieldManipulator;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.ButtonEventHandler;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.*;
import de.abas.erp.axi2.event.internal.FieldFillEventImpl;
import de.abas.erp.axi2.type.ButtonEventType;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReport;
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReportView;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.abas.jfop.base.buffer.BufferFactory;
import de.abas.jfop.base.buffer.PrintBuffer;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.infosystem.event.head.Bugrund;
import de.ck.app.ckcre.cws.infosystem.event.head.Bulang;
import de.ck.app.ckcre.cws.infosystem.event.head.Buproducttype;
import de.ck.app.ckcre.cws.infosystem.event.head.Start;
import de.ck.app.ckcre.cws.util.*;
import https.onlineservice_creditreform_de.webservice._0600_0021.LogonResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.ProductavailabilityResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tproductavailabilityresponsebody;

@EventHandler(head = CrefoReport.class, row = CrefoReport.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class CrefoReportEventHandler {

	private final static String VISIBILITY_SCOPE = "MONITORING";

	public static LogonResponse logon = null;
	public static Tproductavailabilityresponsebody productAvailability = null;

	@ButtonEventHandler(field="bugrund", type = ButtonEventType.AFTER)
	public void bugrundAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
        Bugrund.buttonAfter(event, screenControl, ctx, head);
	}
	
	@ButtonEventHandler(field="bulang", type = ButtonEventType.AFTER)
	public void bulangAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		Bulang.buttonAfter(event, screenControl, ctx, head);
	}

	@ButtonEventHandler(field="buproducttype", type = ButtonEventType.AFTER)
	public void buproducttypeAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		Buproducttype.buttonAfter(event, screenControl, ctx, head);
	}
	
	@FieldEventHandler(field="kunde", type = FieldEventType.EXIT)
	public void kundeExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		try{
			head.setCrefonum(head.getKunde().getYckcreident());
			crefonumExit(event, screenControl, ctx, head);
		}catch(Exception ignored){}
    }

    /*
    	Events werden per AJO nicht ausgeführt!?
     */
	@FieldEventHandler(field="producttype", type = FieldEventType.EXIT)
	public void producttypeExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		try{
			head.setProducttype(Keylist.findDesignationByKey(Constants.keylist, head.getProducttype(), head.getLanguage()));
		}catch(Exception ignored){}
	}
	@FieldEventHandler(field="legitimateinterest", type = FieldEventType.EXIT)
	public void legitimateinterestExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		try{
			head.setLegitimateinterest(Keylist.findDesignationByKey(Constants.keylist, head.getLegitimateinterest(), head.getLanguage()));
		}catch(Exception ignored){}
	}
	@FieldEventHandler(field="crefonum", type = FieldEventType.EXIT)
	public void crefonumExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		//Produktverfügbarkeit triggern!
		AsynchronousProductAvailability.doAsynchronousProdcutAvailability(ctx, head, true);
	}

	@FieldEventHandler(field="extendedmonitoring", type = FieldEventType.EXIT)
	public void extendedmonitoringExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		final PrintBuffer P = BufferFactory.newInstance().getPrintBuffer();
		if(head.getExtendedmonitoring()){
			//maskkontextfop setzen
			P.assign("maskkontextfop", P.getStringValue("maskkontextfop") + " " + VISIBILITY_SCOPE);
		}else{
			//maskkontextfop entfernen
			P.assign("maskkontextfop", P.getStringValue("maskkontextfop").replaceAll( "[ ]*" + VISIBILITY_SCOPE, "" ));
		}
	}

	@ButtonEventHandler(field="pdfzeigen", type = ButtonEventType.AFTER)
	public void pdfzeigenAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		//TODO: Infosystem CrefoReportView aufrufen und Kunden vorbelegen!
		CommandFactory commandFactory = AppContext.createFor(ctx).getCommandFactory();
		FieldManipulator params = commandFactory.getScrParamBuilder(CrefoReportView.class);
		params.setReference(CrefoReportView.META.kunde, head.getKunde());
		commandFactory.startInfosystem(CrefoReportView.class, params);
	}
	
	@ButtonEventHandler(field="start", type = ButtonEventType.AFTER)
	public void startAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		if(head.getCrefonum().isEmpty()){
			FO.box("Hinweis", "Bitte Crefonummer/Kunden angeben!");
			screenControl.moveCursor(null, CrefoReport.META.kunde);
			return;
		}
		Start.startAfter(event, screenControl, ctx, head);
	}
	
	@ScreenEventHandler(type = ScreenEventType.ENTER)
	public void screenEnter(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		AsynchronousKeylist.getAsynchronnousKeylist(ctx);

        CheckLogondata logincheck = new CheckLogondata();
        if(!logincheck.LoginParamCheck(ctx,head)) {
            AppContext appContext = AppContext.createFor(ctx);
            CommandFactory commandFactory = appContext.getCommandFactory();
            FieldManipulator<CKCRELOGIN> scrParamBuilder = commandFactory.getScrParamBuilder(CKCRELOGIN.class);
            scrParamBuilder.pressButton(CKCRELOGIN.META.start);
            commandFactory.startInfosystem(CKCRELOGIN.class, scrParamBuilder);
            throw new EventException("Bitte f\u00fchren sie erst das Login aus.");
        }if(!logincheck.LoginPasswdCheck(ctx)) {
            AppContext appContext = AppContext.createFor(ctx);
            CommandFactory commandFactory = appContext.getCommandFactory();
            FieldManipulator<CKCRELOGIN> scrParamBuilder = commandFactory.getScrParamBuilder(CKCRELOGIN.class);
            scrParamBuilder.pressButton(CKCRELOGIN.META.start);
            commandFactory.startInfosystem(CKCRELOGIN.class, scrParamBuilder);
            throw new EventException("Bitte \u00e4ndern sie zuerst ihr Pers\u00F6nliches Passwort.");
        }

		//Login durchführen
		head.setLanguage("de");
		AsynchronousLogin.doAsynchronousLogin(ctx, head, false);
	}
	
	@ScreenEventHandler(type = ScreenEventType.VALIDATION)
	public void screenValidation(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		// TODO Auto-generated method stub
	}
	
	@ScreenEventHandler(type = ScreenEventType.EXIT)
	public void screenExit(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		// TODO Auto-generated method stub
	}
	
	@ScreenEventHandler(type = ScreenEventType.CANCEL)
	public void screenCancel(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		// TODO Auto-generated method stub
	}
	
	@ScreenEventHandler(type = ScreenEventType.END)
	public void screenEnd(ScreenEndEvent event, ScreenControl screenControl, DbContext ctx, CrefoReport head) throws EventException {
		// TODO Auto-generated method stub
	}

}