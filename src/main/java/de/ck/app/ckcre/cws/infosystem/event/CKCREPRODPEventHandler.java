package de.ck.app.ckcre.cws.infosystem.event;
import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.AppContext;
import de.abas.erp.api.commands.CommandFactory;
import de.abas.erp.api.commands.FieldManipulator;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.ButtonEventHandler;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ButtonEventType;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.SelectableObject;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREPRODP;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.customer.Prospect;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.db.selection.Conditions;
import de.abas.erp.db.selection.SelectionBuilder;
import de.abas.erp.db.util.QueryUtil;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.ck.app.ckcre.cws.util.AsynchronousKeylist;


@EventHandler(head = CKCREPRODP.class, row = CKCREPRODP.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class CKCREPRODPEventHandler {
     @ButtonEventHandler(field = "ckcrecancel", type = ButtonEventType.AFTER)
    public void ckcreabbruchAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREPRODP head) throws EventException {
        head.table().clear();
        head.setCkcreid("");
    }


	@FieldEventHandler(field="ckcrelief", type = FieldEventType.EXIT)
	public void ckcreliefExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCREPRODP head) throws EventException {
        if(head.getCkcrelief()==null||head.getCkcrelief().id().isNullRef()){
                return;
        }
        head.setString("Ckcrekunde","");
        head.setString("Ckcreid","");
        SelectableObject object = head.getCkcrelief().getId();
        SelectionBuilder<Vendor> selectionBuilder = SelectionBuilder.create(Vendor.class);
        selectionBuilder.add(Conditions.eq(Vendor.META.id, object.id()));
        Vendor vendor = QueryUtil.getFirst(ctx, selectionBuilder.build());
        head.setCkcreid(vendor.getYckcreident());

	}

	@FieldEventHandler(field="ckcrekunde", type = FieldEventType.EXIT)
	public void ckcrekundeExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCREPRODP head) throws EventException {
        if(head.getCkcrekunde()==null||head.getCkcrekunde().id().isNullRef()){
            //new CkcreSearchUtil().Clear((CKCRESEARCH)head);
            return;
        }
        SelectableObject object = head.getCkcrekunde().getId();
        head.setString("Ckcrelief","");
        head.setString("Ckcreid","");
        if(head.getCkcrekunde() instanceof Customer){
        SelectionBuilder<Customer> selectionBuilder = SelectionBuilder.create(Customer.class);
        selectionBuilder.add(Conditions.eq(Customer.META.id, object.id()));
        Customer cust = QueryUtil.getFirst(ctx, selectionBuilder.build());
        head.setCkcreid(cust.getYckcreident());

        }
        if(head.getCkcrekunde() instanceof Prospect){
            head.setString("Ckcrelief","");
            head.setString("Ckcreid","");
            SelectionBuilder<Prospect> selectionBuilder = SelectionBuilder.create(Prospect.class);
            selectionBuilder.add(Conditions.eq(Prospect.META.id, object.id()));
            Prospect cust = QueryUtil.getFirst(ctx, selectionBuilder.build());
            head.setCkcreid(cust.getYckcreident());

        }
	}

	@ButtonEventHandler(field="start", type = ButtonEventType.AFTER)
	public void startAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREPRODP head) throws EventException {
        if(head.getCkcreid()!=null&&!head.getCkcreid().matches("")) {
            new CkcreProdV().Prodpruef(head, ctx);

        }else{
            FO.box("Bitte Creditreform ID eingeben!");
        }
	}


	@ScreenEventHandler(type = ScreenEventType.ENTER)
	public void screenEnter(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CKCREPRODP head) throws EventException {
        CheckLogondata logincheck = new CheckLogondata();
        if(!logincheck.LoginParamCheck(ctx,head)) {
            AppContext appContext = AppContext.createFor(ctx);
            CommandFactory commandFactory = appContext.getCommandFactory();
            FieldManipulator<CKCRELOGIN> scrParamBuilder = commandFactory.getScrParamBuilder(CKCRELOGIN.class);
            scrParamBuilder.pressButton(CKCRELOGIN.META.start);
            commandFactory.startInfosystem(CKCRELOGIN.class, scrParamBuilder);
            throw new EventException("Bitte f\u00fchren sie erst das Login aus.");
        }if(!logincheck.LoginPasswdCheck(ctx)) {
            AppContext appContext = AppContext.createFor(ctx);
            CommandFactory commandFactory = appContext.getCommandFactory();
            FieldManipulator<CKCRELOGIN> scrParamBuilder = commandFactory.getScrParamBuilder(CKCRELOGIN.class);
            scrParamBuilder.pressButton(CKCRELOGIN.META.start);
            commandFactory.startInfosystem(CKCRELOGIN.class, scrParamBuilder);
            throw new EventException("Bitte \u00e4ndern sie zuerst ihr Pers\u00F6nliches Passwort.");
        }
        AsynchronousKeylist.getAsynchronnousKeylist(ctx);
	}
}
