package de.ck.app.ckcre.cws.util;

import de.abas.erp.db.DbContext;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import java.io.File;
import java.io.IOException;

/**
 * TODO: Insert Description!
 * Project: creditreformwcs
 * Package: de.ck.owcrefo.util
 * Created: 18.01.2019 16:54
 *
 * @author = manuel.ott
 * @since = 18. Januar 2019
 */
public class LoggerUtil {

    public final static File LOGFILE_ERROR = new File("java/log/ckcre-cws.err");
    public final static File LOGFILE_DEBUG = new File("java/log/ckcre-cws.debug");

    public final static String LOGGER_NAME = "ckcre-cws";

    public static Logger LOG = null;

    public static Logger initialize(DbContext ctx){
        try {
            LOG = getLogger(ctx);
            return LOG;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static Logger getLogger(DbContext ctx) throws IOException {

        Logger res = Logger.getLogger(LOGGER_NAME);
        res.removeAllAppenders();

        //Appenders aufbauen!
        res.setLevel(Level.ALL);

//        // KONSOLE
//        WriterAppender console = new WriterAppender(new PatternLayout("%-5p: %m"), ctx.out());
//        console.setThreshold(Level.TRACE);
//        console.setImmediateFlush(true);
//        console.setEncoding("UTF-8");
//        res.addAppender(console);

        // DEBUG-LOG
        DailyRollingFileAppender debug = new DailyRollingFileAppender(new PatternLayout("[%-5p] %d{yyyy-MM-dd HH:mm:ss} %c{3}.%C{1}:%M - %m%n"), LOGFILE_DEBUG.getPath(), "'.'yyyy-MM-dd");
        debug.setThreshold(Level.DEBUG);
        debug.setEncoding("UTF-8");
        res.addAppender(debug);

        //ERROR-LOG
        DailyRollingFileAppender error = new DailyRollingFileAppender(new PatternLayout("[%-5p] %d{yyyy-MM-dd HH:mm:ss} (%F:%L) %C:%M [Thread: %t, %X] - %m%n"), LOGFILE_ERROR.getPath(), "'.'yyyy-MM-dd");
        error.setThreshold(Level.WARN);
        error.setEncoding("UTF-8");
        res.addAppender(error);

        return res;
    }

}
