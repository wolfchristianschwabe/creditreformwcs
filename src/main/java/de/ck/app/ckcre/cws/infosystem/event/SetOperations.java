package de.ck.app.ckcre.cws.infosystem.event;
/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */

import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;

/**
 * @author christian.schwabe
 * @param: head is the Infosystem Context from Abas, the String is picked from the List of Services
 * @do The Method sets the Boolfields in the IS that shows the User witch Service is activ
 */
public class SetOperations {
    public void SetOpt(CKCRELOGIN head,String opt){
        switch(opt){
            case "logon":
                head.setCkcrelogon(true);
                break;
            case "mailboxdirectory":
                head.setCkcremailboxd(true);
                break;
            case "mailboxentry":
                head.setCkcremailboxe(true);
                break;
            case "order":
                head.setCkcreorder(true);
                break;
            case "report":
                head.setCkcrereport(true);
                break;
            case "search":
                head.setCkcresearch(true);
                break;
            case "participationdatastart":
                head.setCkcreparticidat(true);
                break;
            case "participationdata":
                head.setCkcreparticipatio(true);
                break;
            case "balanceanalysissinglecompany":
                head.setCkcreblanceans(true);
                break;
            case "balanceanalysismultiplecompanies":
                head.setCkcreblanceanm(true);
                break;
            case "cancelstandardmonitoring":
                head.setCkcrecancelstmon(true);
                break;
            case "changeextendedmonitoring":
                head.setCkcrechangemon(true);
                break;
            case "monitoringstatus":
                head.setCkcremonstatus(true);
                break;
            case "changepassword":
                head.setCkcrechangepw(true);
                break;
            case "changeemail":
                head.setCkcrechangem(true);
                break;
            case "identificationnumbermap":
                head.setCkcreidentify(true);
                break;
            case "upgradelist":
                head.setCkcreupgradel(true);
                break;
            case "collectionaccount":
                head.setCkcrecollecta(true);
                break;
            case "collectionagencydocument":
                head.setCkcrecollectd(true);
                break;
            case "collectionagencyfreetextmessage":
                head.setCkcrecollectf(true);
                break;
            case "collectionorder":
                head.setCkcrecollecto(true);
                break;
            case "creditorfreetextmessage":
                head.setCkcrecreditorf(true);
                break;
            case "creditorfreetextmessagequery":
                head.setCkcrecreditorfq(true);
               break;
            case "forwardreceivable":
                head.setCkcreforwardre(true);
                break;
            case "generalinformation":
                head.setCkcregeninfo(true);
                break;
            case "payments":
                head.setCkcrepayment(true);
                break;
            case "process":
                head.setCkcreprocess(true);
            break;
            case "receivable":
                head.setCkcrerecivable(true);
                break;
            case "statementreceivables":
                head.setCkcresrecivable(true);
                break;
            case "titledreceivable":
                head.setCkcretrecivable(true);
                break;
            case "collectionfilesearch":
                head.setCkcrecollectfs(true);
                break;
            case "consumerreport":
                head.setCkcreconsumerr(true);
                break;
            case "productavailability":
                head.setCkcreproductav(true);
                break;
            case "bonimareport":
                head.setCkcrebonimar(true);
                break;
            }

    }
}
