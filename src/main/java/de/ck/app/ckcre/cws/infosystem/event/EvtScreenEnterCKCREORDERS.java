package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.AppContext;
import de.abas.erp.api.commands.CommandFactory;
import de.abas.erp.api.commands.FieldManipulator;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.app.ckcre.cws.util.LoggerUtil;

public class EvtScreenEnterCKCREORDERS {
    public void LogonCheckScreenEnter(CKCREORDERS head, DbContext ctx) throws EventException{
    CheckLogondata logincheck = new CheckLogondata();
		if(!logincheck.LoginParamCheck(ctx,head))
    {
        AppContext appContext = AppContext.createFor(ctx);
        CommandFactory commandFactory = appContext.getCommandFactory();
        FieldManipulator<CKCRELOGIN> scrParamBuilder = commandFactory.getScrParamBuilder(CKCRELOGIN.class);
        scrParamBuilder.pressButton(CKCRELOGIN.META.start);
        commandFactory.startInfosystem(CKCRELOGIN.class, scrParamBuilder);
        throw new EventException("Bitte f\u00fchren sie erst das Login aus.");
    }if(!logincheck.LoginPasswdCheck(ctx))

    {
        AppContext appContext = AppContext.createFor(ctx);
        CommandFactory commandFactory = appContext.getCommandFactory();
        FieldManipulator<CKCRELOGIN> scrParamBuilder = commandFactory.getScrParamBuilder(CKCRELOGIN.class);
        scrParamBuilder.pressButton(CKCRELOGIN.META.start);
        commandFactory.startInfosystem(CKCRELOGIN.class, scrParamBuilder);
        throw new EventException("Bitte \u00e4ndern sie zuerst ihr Pers\u00F6nliches Passwort.");
    }

    final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
		operatorCode.createEditor();
    PasswordEditor editor = null;
		try
    {
        editor = operatorCode.createEditor();
        editor.open(EditorAction.VIEW);
        head.setCkcrereportlang(editor.getYckcrelang());
        head.setCkcrekundenref(editor.getYckcregeschaeft());
        if (editor.getYckcreordervor() != null && !editor.getYckcreordervor().matches("")) {
            String[] order = editor.getYckcreordervor().split(":");
            head.setCkcreordertype(order[0]);
            head.setCkcreordertypeklar(order[1]);
            head.setCkcreprodtype(order[2]);
            head.setCkcreprodtypeklar(order[3]);
            head.setCkcrelegitimateint(order[4]);
            head.setCkcrelegitimatklar(order[5]);
        }
        editor.commit();
    }catch(Exception e)
    {
        LoggerUtil.LOG.error("Fehler beim Auslesen der Daten", e);
        FO.box("Fehler", "Fehler beim lesen der Daten.");
    }finally {
        if (editor != null || editor.active()) {
            editor.abort();
            editor = null;
        }
    }
    }
}
