package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.SelectableObject;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREPRODP;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.customer.CustomerEditor;
import de.abas.erp.db.schema.customer.Prospect;
import de.abas.erp.db.schema.customer.ProspectEditor;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.db.schema.vendor.VendorEditor;
import de.abas.erp.db.selection.Conditions;
import de.abas.erp.db.selection.SelectionBuilder;
import de.abas.erp.db.util.QueryUtil;
import de.ck.app.ckcre.cws.util.LoggerUtil;

/**
 * @author christian.schwabe
 * @do This Class read the Customer or Vendor Dataset out of the Database of Abas and opens an Editor for the Database that is needed.
 */

public class CreateEditorProd {
    /**
     *
     * @param products The text that is created by the Webservice loop in the Programm above
     * @param ctx The context of Abas in witch the IS is called to return Errors back to the User
     */
    public void EditorCreateStart(String products,CKCREPRODP head, DbContext ctx){
        VendorEditor vendorEditor =null;
        ProspectEditor prosEditor =null;
        CustomerEditor custeditor = null;
        Object id =null;
        if(head.getCkcrelief() instanceof Vendor) {
            SelectableObject object = head.getCkcrelief().getId();
            SelectionBuilder<Vendor> selectionBuilder = SelectionBuilder.create(Vendor.class);
            selectionBuilder.add(Conditions.eq(Vendor.META.id, object.id()));
            //ctx.out().println(object);
            id = QueryUtil.getFirst(ctx,selectionBuilder.build());
        }else if (head.getCkcrekunde() instanceof Prospect) {
            SelectableObject objectc = head.getCkcrekunde().getId();
            SelectionBuilder<Prospect> selectionBuilder = SelectionBuilder.create(Prospect.class);
            selectionBuilder.add(Conditions.eq(Prospect.META.id, objectc.id()));
            id = QueryUtil.getFirst(ctx,selectionBuilder.build());
          }
        else if(head.getCkcrekunde() instanceof Customer){
            SelectableObject objectc = head.getCkcrekunde().getId();
            //ctx.out().println(objectc);
            SelectionBuilder<Customer>  selectionBuilder = SelectionBuilder.create(Customer.class);
            selectionBuilder.add(Conditions.eq(Customer.META.id, objectc.id()));
            id = QueryUtil.getFirst(ctx,selectionBuilder.build());
        }
        if(id != null){
            if(id instanceof Vendor) {
                try {
                    vendorEditor =  ((Vendor)id).createEditor();
                    vendorEditor.open(EditorAction.UPDATE);
                    vendorEditor.setString("Yckcreproducts", products);
                    vendorEditor.commit();
                } catch (Exception e) {
                    FO.box("Fehler",e.getMessage());
                    LoggerUtil.LOG.error("Editor kann Daten nicht laden",e);
                }finally{
                    if (vendorEditor !=null && vendorEditor.active() ){
                        vendorEditor.abort();
                        vendorEditor = null;
                    }
                }
            }else if(id instanceof Customer) {
                try {
                    custeditor =  ((Customer)id).createEditor();
                    custeditor.open(EditorAction.UPDATE);
                    custeditor.setString("Yckcreproducts", products);
                    custeditor.commit();
                } catch (Exception e) {
                    FO.box("Fehler",e.getMessage());
                    LoggerUtil.LOG.error("Editor kann Daten nicht laden",e);
                }finally{
                    if (custeditor !=null && custeditor.active() ){
                        custeditor.abort();
                        custeditor = null;
                    }
                }
            }else if(id instanceof Prospect) {
                try {
                    prosEditor =  ((Prospect)id).createEditor();
                    prosEditor.open(EditorAction.UPDATE);
                    prosEditor.setString("Yckcreproducts", products);
                    prosEditor.commit();
                } catch (Exception e) {
                    FO.box("Fehler",e.getMessage());
                    LoggerUtil.LOG.error("Editor kann Daten nicht laden",e);
                }finally{
                    if (prosEditor !=null && prosEditor.active() ){
                        prosEditor.abort();
                        prosEditor = null;
                    }
                }
            }

        }
    }
}
