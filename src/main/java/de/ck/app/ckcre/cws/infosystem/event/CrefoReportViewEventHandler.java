package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.*;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ButtonEventType;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReportView;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.abas.jfop.base.buffer.BufferFactory;
import de.abas.jfop.base.buffer.GlobalTextBuffer;
import de.abas.jfop.base.buffer.PrintBuffer;
import de.ck.app.ckcre.cws.infosystem.event.impl.TreeViewBuilder;
import de.ck.app.ckcre.cws.util.AsynchronousKeylist;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import org.apache.log4j.Logger;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedList;

@EventHandler(head = CrefoReportView.class, row = CrefoReportView.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class CrefoReportViewEventHandler {

	private static TreeViewBuilder treeViewBuilder;

	@FieldEventHandler(field="kunde", type = FieldEventType.EXIT)
	public void kundeExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CrefoReportView head) throws EventException {
		if(head.getKunde() == null) return;
		try{
			String yckcreident = head.getKunde().getRawString(Customer.META.yckcreident.getName());
			if(yckcreident.isEmpty()) throw new Exception("Keine Identnummer (Creditreform) für Kunden hinterlegt!");

			// Baum autom. aufbauen
//			head.invokeStart();
			startAfter(null, screenControl, ctx, head);
		}catch(Exception e){
			FO.box("Fehler", e.getMessage());
			head.setString(CrefoReportView.META.kunde, "");
		}
	}

	@ButtonEventHandler(field="start", type = ButtonEventType.AFTER)
	public void startAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CrefoReportView head) throws EventException {
		treeViewBuilder = new TreeViewBuilder(ctx, head);
		treeViewBuilder.buildTree();
	}

	@ButtonEventHandler(field="treeElemExpandCollapse", type = ButtonEventType.AFTER)
	public void treeElemExpandCollapseAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CrefoReportView head) throws  EventException{
		//TODO: Das Event funktioniert nicht ?!
	}
	
	@ScreenEventHandler(type = ScreenEventType.ENTER)
	public void screenEnter(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CrefoReportView head) throws EventException {
		AsynchronousKeylist.getAsynchronnousKeylist(ctx);

//		//Login durchführen
//		head.setLanguage("de");
//		AsynchronousLogin.doAsynchronousLogin(ctx, head, false);
	}

	@ButtonEventHandler(field="buzeigen", type = ButtonEventType.AFTER, table=true)
	public void isbuzeigenAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CrefoReportView head) throws EventException {
		try{
			//TODO: .pc.copy ausführen, dann Lokalpfad assignen!
			Logger log = LoggerUtil.LOG;
			GlobalTextBuffer G = BufferFactory.newInstance().getGlobalTextBuffer();

			// TempDir unter GUI in Windows
			String destWin = G.getRawStringValue("cltempdir") + "\\abas_report" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmssSS")) + ".pdf";
			destWin = destWin.replace('\\', '/').replace("//", "/");
			String cmd = "-PC -BIN \"" + head.table().getRow(event.getRowNumber()).getTreeElemDescr() + "\" \"" + destWin + "\"";

			log.debug("Führe FO-Kommando aus: .pc.copy " + cmd );
			FO.pc_copy(cmd);

			log.debug("Setzte URI: file://" + destWin);
			head.setPdf("file://" + destWin);
			log.debug("PDF: " + head.getPdf());
		}catch(Exception e){
			FO.box("Fehler", e.toString() + ": " + e.getMessage());
		}
	}

}