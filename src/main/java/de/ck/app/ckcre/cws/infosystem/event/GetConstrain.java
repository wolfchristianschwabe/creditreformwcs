package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.erp.db.DbContext;
import https.onlineservice_creditreform_de.webservice._0600_0021.*;

import java.util.HashMap;
/*
logon
mailboxdirectory
mailboxentry
order
report
search
participationdatastart
participationdata
balanceanalysissinglecompany
balanceanalysismultiplecompanies
cancelstandardmonitoring
changeextendedmonitoring
monitoringstatus
changeemail
changepassword
identificationnumbermap
upgradelist
identificationreport
collectionaccount
collectionagencydocument
collectionagencyfreetextmessage
collectionorder
creditorfreetextmessage
creditorfreetextmessagequery
forwardreceivable
generalinformation
payments
process
receivable
statementreceivables
titledreceivable
collectionfilesearch
consumerreport
productavailability
bonimareport
*/

/**
 * @author christian.schwabe
 * @do This Class Returnes 3 Hashmaps each is needed to select the right output for the picked Service.
 */
public class GetConstrain{
    public HashMap<String, Object> GetServiceConstrain(String Sprache, String Service, DbContext ctx, Tlogonresponsebody tlogonresponsebody, Tproductavailabilityresponsebody prod) {
        HashMap<String,Object> keys = new HashMap<>();
        HashMap<String,Object> count = new HashMap<>();
        HashMap<String,Object> cont = new HashMap<>();
        if(prod!=null) {
            for (Tserviceavailability ser : prod.getService()) {
                if (ser.getOperation().matches(Service))
                    for (Tserviceavailability.Product prodav : ser.getProduct()) {
                        count.put(prodav.getProducttype().getKey(), prodav.getProducttype().getDesignation());
                    }
            }
        }
         else {
            for (Tservice serv : tlogonresponsebody.getService()) {
                for (Tcountryconstraint country : serv.getCountryconstraint()) {
                    for (Tallowedkeys allow : country.getAllowedkeys()) {
                        for (Tallowedkeys.Keyconstraint key : allow.getKeyconstraint()) {
                            if (serv.getOperation().matches(Service) && country.getCountry().getKey().matches(Sprache)) {
                                cont.put(key.getKeycontent().getKey(), key.getKeycontent().getDesignation());
                                //ctx.out().println("Moment: " + key.getKeycontent().getKey());
                            }
                        }
                    }
                    count.put(country.getCountry().getKey(), cont);
                }
                //ctx.out().println(((HashMap<String,String>) count.get("DE")).containsKey("PRTY-2"));
            }
        }
              return count;
    }

    public HashMap<String, String> GetConstrains(String productinput,String Service,DbContext ctx, Tlogonresponsebody tlogonresponsebody, Tproductavailabilityresponsebody prod) {
        HashMap<String,String> keyscont = new HashMap<>();
        HashMap<String,Object> keys = new HashMap<>();
        if(prod!=null) {
            for (Tserviceavailability ser : prod.getService()) {
                    for (Tserviceavailability.Product prodav : ser.getProduct()) {
                        if (prodav.getProducttype().getKey().matches(productinput)) {
                            for (Tparameterlist param : prodav.getParameters())
                                for (Tparameter param2 : param.getParameter()) {
                                    for (Tkey param3 : param2.getKey()) {
                                        keyscont.put(prodav.getProducttype().getKey(), prodav.getProducttype().getDesignation());
                                    }
                                }
                        }
                    }
            }
        }
        else {
        for(Tservice serv:tlogonresponsebody.getService()) {
            for (Tallowedkeys keyser : serv.getAllowedkeys()) {
                for (Tallowedkeys.Keyconstraint constrain : keyser.getKeyconstraint()) {
                        keyscont.put(constrain.getKeycontent().getKey(), constrain.getKeycontent().getDesignation());
                        //ctx.out().println("Moment: " + constrain.getKeycontent().getKey() + "," + constrain.getKeycontent().getDesignation());
                    }
                }
            }

        }
        return keyscont;
    }
    public HashMap<String, String> GetState(String Sprache,String Service,DbContext ctx, Tlogonresponsebody tlogonresponsebody) {
        HashMap<String,String> count = new HashMap<>();
        for(Tservice serv:tlogonresponsebody.getService()) {
            for(Tcountryconstraint country:serv.getCountryconstraint()){
                count.put(country.getCountry().getKey(),country.getCountry().getDesignation());
            }
        }
           return count;

    }

}
