package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.gui.MenuBuilder;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tkeyattributevalue;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody;


/**
 * @author christian.schwabe
 * @param: head resamble the IS Context and ctx is the Context in witch the IS is loaded
 * @do This Method sets the Default Language that could bechosen from the Login IS.
 */
public class ChooseLanguage {
    public void ChooseLang(CKCRELOGIN head, DbContext ctx, Tkeylistresponsebody.Keylist keylist) {
            MenuBuilder<String> menuBuilder = new MenuBuilder<String>(ctx, "Language");
            createMenu(menuBuilder, keylist,ctx);
            String temp = menuBuilder.show();
            if(temp==null)
                return;
            head.setCkcrelangchosen(temp);
            final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
            PasswordEditor editor = null;
            try {
                   editor = operatorCode.createEditor();
            editor.open(EditorAction.UPDATE);
                if(!head.getCkcrelangchosen().matches(editor.getYckcrelang())
                        ||editor.getYckcrelang().isEmpty())
                    editor.setYckcrelang(head.getCkcrelangchosen());
                editor.commit();
            } catch (Exception e) {
               FO.box("Fehler",e.getMessage());
               LoggerUtil.LOG.error("Fehler beim schreiben der Nutzerdaten durch den Editor",e);
            } finally {
                if (editor != null && editor.active()) {
                    editor.abort();
                    editor = null;
                }
            }
    }

    public void createMenu(MenuBuilder menue, Tkeylistresponsebody.Keylist keylist, DbContext ctx) {
        if (keylist == null||keylist.equals(null)) {
            menue.addItem("DE","DE");
            menue.addItem("EN","EN");
            menue.addItem("FR","FR");
            menue.addItem("IT","IT");
            menue.setTitle("Language ausw\u00e4hlen:");
        } else {
            for (Tkeyattributevalue keyvalue : keylist.getKeygroup().get(1).getKeyentry().get(1).getKeyattribute().get(1).getKeyattributevalue()) {
                menue.addItem(keyvalue.getLanguage(),keyvalue.getLanguage());
            }
        }
    }
}
