package de.ck.app.ckcre.cws.infosystem.event;
/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 * @Name Eventhandler for Infosystem CKCRELOGIN
 */

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.ButtonEventHandler;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ButtonEventType;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.ck.app.ckcre.cws.util.AsynchronousKeylist;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody;

import java.util.List;


@EventHandler(head = CKCRELOGIN.class, row = CKCRELOGIN.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class CKCRELOGINEventHandler {
    public static Tkeylistresponsebody.Keylist key;
    @ButtonEventHandler(field = "ckcreabbruch", type = ButtonEventType.AFTER)
    public void ckcreabbruchAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRELOGIN head) throws EventException {
    new Ckcreloginutil().Clear(head);
    }

    @ButtonEventHandler(field = "ckcrekeylistakt", type = ButtonEventType.AFTER)
    public void ckcrekeylistaktAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRELOGIN head) throws EventException {
        new CkcreKeylist().Keylistakt(head,ctx);
    }

    @ButtonEventHandler(field = "ckcrekeylistanz", type = ButtonEventType.AFTER)
    public void ckcrekeylistanzAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRELOGIN head) throws EventException {
        if(head.getCkcrepasswd()!=null &&head.getCkcregenpw()!=null &&head.getCkcrelogin()!=null
        &&!head.getCkcrepasswd().matches("")&&!head.getCkcregenpw().matches("")&&!head.getCkcrelogin().matches("") )
        new CkcreKeylist().Anz(head,ctx);
        else{
            FO.box("Fehler","Anmeldedaten fehlen bitte nachtragen bevor sie die Keylist abrufen.");
        }
    }

    @ButtonEventHandler(field = "ckcreloginbn", type = ButtonEventType.AFTER)
    public void ckcreloginbnAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRELOGIN head) throws EventException {
        if(head.getCkcrelogin()!=null&&head.getCkcrelogin().matches("^\\d+")&&!head.getCkcrelogin().matches("")) {
            if((head.getCkcrepasswd()!=null&&!head.getCkcrepasswd().matches(""))||(head.getCkcregenpw()!=null&&!head.getCkcregenpw().matches(""))){
                Ckcrelogin login = new Ckcrelogin();
            Tkeylistresponsebody body=(Tkeylistresponsebody)login.Login(ctx, head);
            if(body!= null)
            key = body.getKeylist();
            AsynchronousKeylist.getAsynchronnousKeylist(ctx);
            }else{
                FO.box("Fehler", "Passwortfelder d\u00fcrfen nicht leer sein!");
            }
        }else{

            FO.box("Fehler", "Die Mitgliedsnummer darf keine Buchstaben enthalten und nicht leer sein.");
        }
    }

    @ButtonEventHandler(field = "ckcrepasswdaendern", type = ButtonEventType.AFTER)
    public void ckcrepasswdaendernAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRELOGIN head) throws EventException {
        try {
            if(head.getCkcrepasswdneu().length()>=6&&head.getCkcrepasswdwh().length()>=6) {
                new Ckcrepasswd().Passwd(head, ctx);
            }else{
                FO.box("Fehler","Passwort zu klein 6-8 Zeichen.");
            }
        } catch (Exception e) {
            FO.box("Gescheitert: "+e.getMessage());
            LoggerUtil.LOG.error("Fehler Passwort \u00e4ndern gescheitert",e);
        }
    }

	@ScreenEventHandler(type = ScreenEventType.ENTER)
	public void screenEnter(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CKCRELOGIN head) throws EventException {

        new ScreensetLogin().Screenset(head,screenControl);
        new SetLoginData().loginData(head,ctx,screenControl);
        screenControl.setProtection(head,head.META.ckcrekeylistanz,true);
        LoggerUtil.initialize(ctx);
        List<Boolean>logingrantlist = (List<Boolean>)new CheckLogondata().LogonGranted(ctx,head);
        boolean grantlogon = logingrantlist.get(0);
        boolean grantadmin = logingrantlist.get(1);
        //ctx.out().println(checkup);
        if(grantlogon){
            if(grantadmin){
                screenControl.setProtection(head,head.META.ckcrekeylistanz,false);
            }
        }else{
            throw new EventException("Sie haben nicht die Berechtigung dieses IS auf zurufen. Bitte Wenden sie sich an Ihren Administrator.");
        }

    }

	@FieldEventHandler(field="ckcrekeylist", type = FieldEventType.EXIT)
	public void ckcrekeylistExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCRELOGIN head) throws EventException {
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        PasswordEditor editor=null;
        int keyl=21;
        try {
            editor = operatorCode.createEditor();
            editor.open(EditorAction.VIEW);
            keyl=Integer.parseInt(editor.getYckcrekeylist());
        }catch(Exception e){
            FO.box("Fehler","Fehler beim Abruf der Daten"+operatorCode.getIdno());
            LoggerUtil.LOG.error("Fehler beim schreiben der Nutzerdaten durch den Editor",e);
        }
		if(!head.getCkcrekeylist().matches("21")&&keyl > 21)
            head.setCkcredefkeylist(false);
		if(head.getCkcrekeylist().matches("21")&&keyl == 21)
		    head.setCkcredefkeylist(true);
	}

	@ButtonEventHandler(field="ckcrelangchose", type = ButtonEventType.AFTER)
	public void ckcrelangchoseAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRELOGIN head) throws EventException {
		new ChooseLanguage().ChooseLang(head,ctx,key);
	}

	@ButtonEventHandler(field="ckcreemailbu", type = ButtonEventType.AFTER)
	public void ckcreemailbuAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRELOGIN head) throws EventException {
        if(!head.getCkcreemail().matches("")) {
            if(head.getCkcreemail().contains("@")) {
                new Mailchange().Mailchanger(ctx, head);
            }else{
                FO.box("Fehler", "Dies ist keine g\u00fcltige Mailadresse.");
            }
        }else FO.box("Fehler","Das Feld Emailadresse darf nicht leer sein!");
	}

}
