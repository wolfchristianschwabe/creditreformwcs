package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.SelectableObject;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRESEARCH;
import de.abas.erp.db.schema.customer.Prospect;
import de.abas.erp.db.schema.customer.ProspectEditor;
import de.abas.erp.db.selection.Conditions;
import de.abas.erp.db.selection.SelectionBuilder;
import de.abas.erp.db.util.QueryUtil;
import de.ck.app.ckcre.cws.util.LoggerUtil;

/**
 * @author christian.schwabe
 * @do This Class updates the Prospect Data in the Abas Object Prospect.
 */
public class CkcreSetAbasProspect {
    public void SetCustomer(CKCRESEARCH head, CKCRESEARCH.Row row, DbContext ctx) {
        SelectableObject object =  head.getCkcrekundea().getId();
        SelectionBuilder<Prospect> selectionBuilder = SelectionBuilder.create(Prospect.class);
        selectionBuilder.add(Conditions.eq(Prospect.META.id, object.id()));
        Prospect cust = QueryUtil.getFirst(ctx,selectionBuilder.build());
        //ctx.out().println("ID:" + cust.getId().toString());
        ProspectEditor editor = null;
        if( head.getCkcrekundea()!=null &&  !head.getCkcrekundea().toString().matches("")) {
            //editor = null;
        }
        try {
            editor = cust.createEditor();
            editor.open(EditorAction.UPDATE);
            editor.setYckcrecrefonum(row.getCkcreid());
            editor.setStreet(row.getCkcretstreet() + " " + row.getString("Ckcrehousnumber") + " " + row.getString("Ckcrehousnumberaf"));
            if(row.getCkcrename()!=null&&!row.getCkcrename().matches("")) {
                editor.setAddr(row.getCkcrename());
            }
            if(row.getString("Ckcretpostcode")!=null&&!row.getString("Ckcretpostcode").matches(""))
            editor.setZipCode(row.getString("Ckcretpostcode"));
            if(row.getString("Ckcretort")!=null&&!row.getString("Ckcretort").matches(""))
            editor.setTown(row.getString("Ckcretort"));
          //  editor.setString("stateOfTaxOffice",row.getCkcretland());
            editor.commit();
            FO.box("Erfolg","Interessentendaten Aktualisiert.");
        }catch (Exception e){
            FO.box("Fehler",e.getMessage());
            LoggerUtil.LOG.error("Editor konnte Daten nicht laden",e);
        }finally{
            if(editor!=null&&editor.active()){
                editor.abort();
                editor = null;
            }else{
                editor =null;
            }
        }

    }
}
