/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */
package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.common.type.AbasDate;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.OrderResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tlanguagerequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.Torderrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.math.BigDecimal;
import java.util.GregorianCalendar;

/**
 * @author christian.schwabe
 * @param: ctx is the DatabaseContext in witch the IS is loaded, head is the IS that is called by the User
 * @do This Method sends the Orderrequest to the Webservice
 */
public class MailboxSendOrder {
	public void mailboxcallup(DbContext ctx, CKCREORDERS head) {
	    final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
		PasswordEditor editor=null;
		OrderResponse orderr =null;
		try {
			editor = operatorCode.createEditor();
			editor.open(EditorAction.VIEW);
		}catch(Exception e){
			FO.box("Fehler","Fehler beim Abruf der Daten f\u00fcr Nutzer: "+operatorCode.getIdno());
			LoggerUtil.LOG.error("Fehler beim Abruf der Nutzerdaten durch den Editor",e);
		}
		Treportrequestbody.Extendedmonitoring ext = new	Treportrequestbody.Extendedmonitoring();
		OrderResponse order = null;
		Torderrequestbody trequest = new Torderrequestbody();
		Torderrequestbody.Company company = new Torderrequestbody.Company();
		Torderrequestbody.Privateperson privatp = new Torderrequestbody.Privateperson();
		if(head.getCkcrekundekon()) {
			if (head.getCkcredateofbirth() != null && !head.getCkcredateofbirth().matches(""))
				if(head.getCkcredateofbirth().startsWith("^\\d+.")){
					String dateofbirth = "0"+head.getCkcredateofbirth();
					privatp.setDateofbirth(dateofbirth);
				}

			if (head.getCkcrefirstname() != null && !head.getCkcrefirstname().matches(""))
				privatp.setFirstname(head.getCkcrefirstname());
			if (head.getCkcresurname() != null && !head.getCkcresurname().matches(""))
				privatp.setSurname(head.getCkcresurname());
			if(privatp.getSurname()!=null&&!privatp.getSurname().matches(""))
				trequest.setPrivateperson(privatp);

		}else {
			if (head.getCkcrecompany() != null && !head.getCkcrecompany().matches(""))
				company.setCompanyname(head.getCkcrecompany());
			if (head.getCkcrelegalform() != null && !head.getCkcrelegalform().matches(""))
				company.setLegalform(head.getCkcrelegalform());
			if(company.getCompanyname()!=null&&!company.getCompanyname().matches(""))
				trequest.setCompany(company);
		}
		if(head.getCkcrekcity()!=null&&!head.getCkcrekcity().matches(""))
		trequest.setCity(head.getCkcrekcity());
		if(head.getCkcrekcountry()!=null&&!head.getCkcrekcountry().matches(""))
		trequest.setCountry(head.getCkcrekcountry());
		if(head.getCkcrecreditamount()!=null&&!head.getCkcrecreditamount().matches(""))
			trequest.setCreditenquiryamount((BigDecimal.valueOf(Long.parseLong(head.getCkcrecreditamount()))));
		if(head.getCkcrecreditcurren()!=null&&!head.getCkcrecreditcurren().matches(""))
			trequest.setCreditenquirycurrency(head.getCkcrecreditcurren());
		if(head.getCkcreextmon())
			ext.setExtendedmonitoringplus(head.getCkcreextmon());
		if(head.getCkcreextmon()&&head.getCkcreextmondate()!=null&&head.getCkcreextmondate().toString().matches("")) {
			GregorianCalendar calendar = new GregorianCalendar();
			calendar.setTime(head.getCkcreextmondate().toDate());
			try {
				ext.setEndofextendedmonitoring(DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar));
				trequest.setExtendedmonitoring(ext);
			} catch (DatatypeConfigurationException e) {
				LoggerUtil.LOG.error("Fehler beim Covertieren des Datums",e);
				FO.box("Fehler",e.toString());
			}
		}
		if(head.getCkcrekhousnumber()!=null&&!head.getCkcrekhousnumber().matches(""))
			trequest.setHousenumber(Integer.valueOf(head.getCkcrekhousnumber()));
		if(head.getCkcrehousnumberaff()!=null&&!head.getCkcrehousnumberaff().matches(""))
		trequest.setHousenumberaffix(head.getCkcrehousnumberaff());
		if(head.getCkcrecrefoid()!=null&&!head.getCkcrecrefoid().matches(""))
		trequest.setIdentificationnumber(head.getCkcrecrefoid());
		if(head.getCkcrelegitimateint()!=null&&!head.getCkcrelegitimateint().matches(""))
		trequest.setLegitimateinterest(head.getCkcrelegitimateint());
		if(head.getCkcreorderspecific()!=null&&!head.getCkcreorderspecific().matches(""))
		trequest.setOrderspecifyingtext(head.getCkcreorderspecific());
		if(head.getCkcreordertype()!=null&&!head.getCkcreordertype().matches(""))
		trequest.setOrdertype(head.getCkcreordertype());
		if(head.getCkcrekpostcode()!=null&&!head.getCkcrekpostcode().matches(""))
		trequest.setPostcode(head.getCkcrekpostcode());
		if(head.getCkcreprodtype()!=null&&!head.getCkcreprodtype().matches(""))
		trequest.setProducttype(head.getCkcreprodtype());
		if(head.getCkcrekregion()!=null&&!head.getCkcrekregion().matches(""))
		trequest.setRegion(head.getCkcrekregion());
		if(head.getCkcrereportlang()!=null&&!head.getCkcrereportlang().matches(""))
		trequest.setReportlanguage(Tlanguagerequest.valueOf(head.getCkcrereportlang()));
		if(head.getCkcrekstreet()!=null&&!head.getCkcrekstreet().matches(""))
		trequest.setStreet(head.getCkcrekstreet());
		try {
			orderr = (OrderResponse)Services.Order.start(trequest, new Logonrequest().buildupLogin(editor.getYckcregenpwd(), editor.getYckcrepw(), editor.getYckcrelang(), editor.getYckcreuser(), Integer.parseInt(editor.getYckcrekeylist())));
		}catch (Exception e){
			LoggerUtil.LOG.error("Fehler beim Aufruf des Recherche Services",e);
			FO.box("Fehler",e.toString());
		}finally{
				if(editor!= null && editor.active()) {
				editor.abort();
				editor = null;
			}

		}
		if(orderr != null ) {
			FO.box("", "Recherche Auftrag eingereicht Referenz: "+orderr.getBody().getReferencenumber());
			if ((head.getCkcrelief() != null || head.getCkcrekunde() != null)) {
				new CreateEditorRefOrder().EditorCreateStart("Referenz vom:" + new AbasDate().toString().substring(6) + "." + new AbasDate().toString().substring(4, 6) + "." + new AbasDate().toString().substring(0, 4) + " Typ: " + head.getString("Ckcreprodtypeklar") + ", Ordertyp: " + head.getString("Ckcreordertypeklar") + " Referenz:" + orderr.getBody().getReferencenumber(), head, ctx);
			}
		}

	}
}
