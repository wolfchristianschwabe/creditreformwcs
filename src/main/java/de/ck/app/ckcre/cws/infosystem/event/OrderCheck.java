package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.jfop.base.Color;

public class OrderCheck {
    public boolean orderCheckinput(CKCREORDERS head, DbContext ctx, ScreenControl screenControl) {

        if ((head.getCkcrekcountry() != null && head.getCkcrelegitimateint() != null && head.getCkcreordertype() != null && head.getCkcreprodtype() != null && head.getCkcrereportlang() != null)
                && (!head.getCkcrekcountry().matches("") && !head.getCkcrekcountry().matches("") && !head.getCkcrelegitimateint().matches("") && !head.getCkcreordertype().matches("") && !head.getCkcreprodtype().matches("") && !head.getCkcrereportlang().matches(""))) {
            screenControl.setColor(head, head.META.ckcreordertype, Color.BLACK, Color.WHITE);
            screenControl.setColor(head, head.META.ckcrelegitimateint, Color.BLACK, Color.WHITE);
            screenControl.setColor(head, head.META.ckcreprodtype, Color.BLACK, Color.WHITE);
            screenControl.setColor(head, head.META.ckcrelanguagew, Color.BLACK, Color.WHITE);
            screenControl.setColor(head, head.META.ckcrekcountry, Color.BLACK, Color.WHITE);
            //Companyname Filled
            if (!head.getCkcrekundekon()) {
                if ((head.getCkcrecrefoid() == null && head.getCkcrecrefoid().matches("")) && (head.getCkcrecompany() == null && head.getCkcrecompany().matches(""))) {
                    screenControl.setColor(head, head.META.ckcrecompany, Color.WHITE, Color.RED);
                    screenControl.setColor(head, head.META.ckcrecrefoid, Color.WHITE, Color.RED);
                    FO.box("Fehler", "Pflichtfelder sind nicht gefuellt!");
                    return false;
                } else {
                    screenControl.setColor(head, head.META.ckcrecrefoid, Color.BLACK, Color.WHITE);
                    screenControl.setColor(head, head.META.ckcrecompany, Color.BLACK, Color.WHITE);
                    if ((head.getCkcrekstreet() == null && head.getCkcrekstreet().toString().matches("")) && (head.getCkcrekcity() == null && head.getCkcrekcity().toString().matches(""))
                            && (head.getCkcrekpostcode() == null && head.getCkcrekpostcode().toString().matches("")) ||
                            (head.getCkcrekstreet() != null && !head.getCkcrekstreet().toString().matches("")) && (head.getCkcrekcity() != null && !head.getCkcrekcity().toString().matches(""))
                                    && (head.getCkcrekpostcode() != null && !head.getCkcrekpostcode().toString().matches(""))) {
                        screenControl.setColor(head, head.META.ckcrekcity, Color.BLACK, Color.WHITE);
                        screenControl.setColor(head, head.META.ckcrekpostcode, Color.BLACK, Color.WHITE);
                        screenControl.setColor(head, head.META.ckcrekstreet, Color.BLACK, Color.WHITE);
                        if (head.getCkcrecrefoid().matches("^\\d+")) {
                            return true;
                        } else {
                            FO.box("Fehler", "Bitte geben sie die Identnummer nur mit Zahlen an.");
                        }
                    } else {
                        screenControl.setColor(head, head.META.ckcrekcity, Color.WHITE, Color.RED);
                        screenControl.setColor(head, head.META.ckcrekpostcode, Color.WHITE, Color.RED);
                        screenControl.setColor(head, head.META.ckcrekstreet, Color.WHITE, Color.RED);
                        FO.box("Fehler", "Bitte die markierten Felder ausf\u00fcllen!");
                        return false;
                    }
                    screenControl.setColor(head, head.META.ckcrecompany, Color.BLACK, Color.WHITE);
                    if ((head.getCkcrekstreet() == null && head.getCkcrekstreet().toString().matches("")) && (head.getCkcrekcity() == null && head.getCkcrekcity().toString().matches(""))
                            && (head.getCkcrekpostcode() == null && head.getCkcrekpostcode().toString().matches("")) ||
                            (head.getCkcrekstreet() != null && !head.getCkcrekstreet().toString().matches("")) && (head.getCkcrekcity() != null && !head.getCkcrekcity().toString().matches(""))
                                    && (head.getCkcrekpostcode() != null && !head.getCkcrekpostcode().toString().matches(""))) {
                        screenControl.setColor(head, head.META.ckcrekcity, Color.BLACK, Color.WHITE);
                        screenControl.setColor(head, head.META.ckcrekpostcode, Color.BLACK, Color.WHITE);
                        screenControl.setColor(head, head.META.ckcrekstreet, Color.BLACK, Color.WHITE);
                        return true;
                    } else {
                        screenControl.setColor(head, head.META.ckcrekcity, Color.WHITE, Color.RED);
                        screenControl.setColor(head, head.META.ckcrekpostcode, Color.WHITE, Color.RED);
                        screenControl.setColor(head, head.META.ckcrekstreet, Color.WHITE, Color.RED);
                        FO.box("Fehler", "Bitte die markierten Felder ausf\u00fcllen!");
                        return false;
                    }
                }
            }
                    //surname Filled
             else {
                if ((head.getCkcrecrefoid() == null && head.getCkcrecrefoid().matches("")) && (head.getCkcresurname() == null && head.getCkcresurname().matches(""))) {
                    screenControl.setColor(head, head.META.ckcresurname, Color.WHITE, Color.RED);
                    screenControl.setColor(head, head.META.ckcrecrefoid, Color.WHITE, Color.RED);
                    FO.box("Fehler", "Pflichtfelder sind nicht gefuellt, bitte eine Person angeben!");
                    return false;
                } else if ((head.getCkcrecrefoid() != null && !head.getCkcrecrefoid().matches("")) && (head.getCkcresurname() == null && head.getCkcresurname().matches(""))) {
                    screenControl.setColor(head, head.META.ckcresurname, Color.BLACK, Color.WHITE);
                    if ((head.getCkcrekstreet() == null && head.getCkcrekstreet().toString().matches("")) && (head.getCkcrekcity() == null && head.getCkcrekcity().toString().matches(""))
                            && (head.getCkcrekpostcode() == null && head.getCkcrekpostcode().toString().matches("")) ||
                            (head.getCkcrekstreet() != null && !head.getCkcrekstreet().toString().matches("")) && (head.getCkcrekcity() != null && !head.getCkcrekcity().toString().matches(""))
                                    && (head.getCkcrekpostcode() != null && !head.getCkcrekpostcode().toString().matches(""))) {
                        screenControl.setColor(head, head.META.ckcrekcity, Color.BLACK, Color.WHITE);
                        screenControl.setColor(head, head.META.ckcrekpostcode, Color.BLACK, Color.WHITE);
                        screenControl.setColor(head, head.META.ckcrekstreet, Color.BLACK, Color.WHITE);
                        if (head.getCkcrecrefoid().matches("^\\d+")) {
                            return true;
                        } else {
                            FO.box("Fehler", "Bitte geben sie die Identnummer nur mit Zahlen an.");
                        }
                    } else {
                        screenControl.setColor(head, head.META.ckcrekcity, Color.WHITE, Color.RED);
                        screenControl.setColor(head, head.META.ckcrekpostcode, Color.WHITE, Color.RED);
                        screenControl.setColor(head, head.META.ckcrekstreet, Color.WHITE, Color.RED);
                        FO.box("Fehler", "Bitte die markierten Felder ausf\u00fcllen!");
                        return false;
                    }

                } else {
                    screenControl.setColor(head, head.META.ckcresurname, Color.BLACK, Color.WHITE);
                    if ((head.getCkcrekstreet() == null && head.getCkcrekstreet().toString().matches("")) && (head.getCkcrekcity() == null && head.getCkcrekcity().toString().matches(""))
                            && (head.getCkcrekpostcode() == null && head.getCkcrekpostcode().toString().matches("")) ||
                            (head.getCkcrekstreet() != null && !head.getCkcrekstreet().toString().matches("")) && (head.getCkcrekcity() != null && !head.getCkcrekcity().toString().matches(""))
                                    && (head.getCkcrekpostcode() != null && !head.getCkcrekpostcode().toString().matches(""))) {
                        screenControl.setColor(head, head.META.ckcrekcity, Color.BLACK, Color.WHITE);
                        screenControl.setColor(head, head.META.ckcrekpostcode, Color.BLACK, Color.WHITE);
                        screenControl.setColor(head, head.META.ckcrekstreet, Color.BLACK, Color.WHITE);
                        return true;
                    } else {
                        screenControl.setColor(head, head.META.ckcrekcity, Color.WHITE, Color.RED);
                        screenControl.setColor(head, head.META.ckcrekpostcode, Color.WHITE, Color.RED);
                        screenControl.setColor(head, head.META.ckcrekstreet, Color.WHITE, Color.RED);
                        FO.box("Fehler", "Bitte die markierten Felder ausf\u00fcllen!");
                        return false;
                    }

                }
            }
        } else {
            screenControl.setColor(head, head.META.ckcreordertype, Color.WHITE, Color.RED);
            screenControl.setColor(head, head.META.ckcrelegitimateint, Color.WHITE, Color.RED);
            screenControl.setColor(head, head.META.ckcreprodtype, Color.WHITE, Color.RED);
            screenControl.setColor(head, head.META.ckcrelanguagew, Color.WHITE, Color.RED);
            screenControl.setColor(head, head.META.ckcrekcountry, Color.WHITE, Color.RED);
            return false;
        }
        return false;
    }

}
