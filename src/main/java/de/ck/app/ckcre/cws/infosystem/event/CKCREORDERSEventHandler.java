package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.ButtonEventHandler;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ButtonEventType;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.SelectableObject;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.customer.Prospect;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.db.selection.Conditions;
import de.abas.erp.db.selection.SelectionBuilder;
import de.abas.erp.db.util.QueryUtil;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.ck.app.ckcre.cws.util.AsynchronousKeylist;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tlogonresponsebody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tproductavailabilityresponsebody;

import java.util.HashMap;

@EventHandler(head = CKCREORDERS.class, row = CKCREORDERS.Row.class)

@RunFopWith(EventHandlerRunner.class)
//xxyckcrebrecht
public class CKCREORDERSEventHandler {
	static HashMap<String,Object> constrainslang = null;
	static HashMap<String,String> constrains = null;
	static HashMap<String,String> constrainstate = null;
	@ButtonEventHandler(field = "ckcreabbruch", type = ButtonEventType.AFTER)
	public void ckcreabbruchAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head)
			throws EventException {
		new CkcreordersUtil().clear(head);
	}

	@ButtonEventHandler(field = "ckcreabruf", type = ButtonEventType.AFTER)
	public void ckcreabrufAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head)
			throws EventException {
		if(head.getCkcdeliverytypedel()||head.getCkcredeliverytype()||head.getCkcredeliverytyper()||head.getCkcredeliverytypes()||head.getCkcredeliverytypeu()
				||head.getCkcreentriesread()||head.getCkcreentriesunread()||head.getCkcreopenorders()) {
			new Mailboxcall().mailboxcallup(ctx, head);
		}else{
			FO.box("Fehler","Bitte f\u00fcllen sie mindestens ein Pflichtfeld aus.");
		}
	}

	@ButtonEventHandler(field = "ckcrerechsend", type = ButtonEventType.AFTER)
	public void ckcrerechsendAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head)
			throws EventException {
		if(head.getCkcrecrefoid()==null||head.getCkcrecrefoid().matches("")) {
			FO.box("Fehler","Bitte geben sie eine Identnummer ein.");
			return;
		}
		if(head.getCkcrekstreet()!=null && head.getCkcrekstreet().contains(".")){
			FO.box("Fehler", "Bitte schreiben sie Stra\u00dfennamen aus Str. wird nicht akzeptiert.");
			return;
		}
		if(new OrderCheck().orderCheckinput(head,ctx,screenControl))
		new MailboxSendOrder().mailboxcallup(ctx, head);

	}

	@ButtonEventHandler(field = "ckcreauswahl", type = ButtonEventType.AFTER, table = true)
	public void ckcreauswahlAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head,
			CKCREORDERS.Row currentRow) throws EventException {
		new Mailboxentry().Entryread(head, currentRow, ctx);
	}

	@ButtonEventHandler(field="ckcrelanguagew", type = ButtonEventType.AFTER)
	public void ckcrelanguagewAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		if(head.getCkcrecrefoid()==null||head.getCkcrecrefoid().matches("")) {
			FO.box("Fehler","Bitte geben sie eine Identnummer ein.");
			return;
		}
		new CkcreOrdersKeylist().Keylistreturn(head,ctx, event, constrainslang,constrains);
	}

	@ButtonEventHandler(field="start", type = ButtonEventType.AFTER)
	public void startAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		if(head.getCkcrecrefoid()==null||head.getCkcrecrefoid().matches("")) {
			FO.box("Fehler","Bitte geben sie eine Identnummer ein.");
			return;
		}
		Tlogonresponsebody response = new LogonabasCWS().LogonabasCWS(ctx);
		Tproductavailabilityresponsebody prod = new CkcreAvProd().Prodpruef(head,ctx);
		final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
		PasswordEditor editor=null;
		try {
			editor = operatorCode.createEditor();
			editor.open(EditorAction.VIEW);
			constrainslang = new GetConstrain().GetServiceConstrain(editor.getYckcrelang(), "order", ctx, response, prod);
			constrains = new GetConstrain().GetConstrains(head.getCkcreprodtype(), "order", ctx, response,prod);
			constrainstate = new GetConstrain().GetState(editor.getYckcrelang(), "order", ctx, response);
		}catch (Exception e){
			FO.box("Fehler", "Fehler beim auslesen der Produktdaten.");
			LoggerUtil.LOG.error("FehlerID", e);
		}finally{
			editor.commit();
			if(editor!=null&&editor.active()){
				editor.abort();
				editor=null;
			}
		}
		//ctx.out().println("Startbutton"+constrains.toString()+"\\r"+constrainstate.toString()+"\\r"+constrainslang.toString());


	}

	@ButtonEventHandler(field="ckcrelang", type = ButtonEventType.AFTER)
	public void ckckcrelangAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		if(head.getCkcrecrefoid()==null&&head.getCkcrecrefoid().matches("")) {
			FO.box("Fehler","Bitte geben sie eine Identnummer ein.");
			return;
		}
			if(constrainstate!=null){
				new CkcreOrdersKeylist().Keylistreturn(head,ctx, event,constrainslang,constrainstate);
			}
	}

	@ButtonEventHandler(field="ckcrelegitimintw", type = ButtonEventType.AFTER)
	public void ckcrelegitimintwAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		if((head.getCkcrecrefoid()==null||head.getCkcrecrefoid().matches(""))||(head.getCkcreprodtype()==null||head.getCkcreprodtype().matches(""))) {
			FO.box("Fehler","Bitte geben sie eine Identnummer ein und w\u00e4hlen sie ein Produkttyp aus.");
			return;
		}
			new CkcreOrdersKeylist().Keylistreturn(head, ctx, event, constrainslang,constrains);
	}
	
	@ButtonEventHandler(field="ckcreordertypew", type = ButtonEventType.AFTER)
	public void ckcreordertypewAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		if(head.getCkcrereportlang()!=null&&!head.getCkcrereportlang().matches("")) {
			if (constrainslang != null)
				new CkcreOrdersKeylist().Keylistreturn(head, ctx, event, constrainslang, constrains);
			else
				FO.box("Meldung", "Keylist noch nicht geladen, bitte warten sie einen Moment.");
		}else{
			FO.box("Fehler","Erst die Sprache w\u00e4hlen");
		}
	}
	
	@ButtonEventHandler(field="ckcreprodtypwbn", type = ButtonEventType.AFTER)
	public void ckcreprodtypwbnAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		if(head.getCkcrecrefoid()==null||head.getCkcrecrefoid().matches("")) {
			FO.box("Fehler","Bitte geben sie eine Identnummer ein.");
			return;
		}
		if(constrains!=null&&constrainslang!=null)
		new CkcreOrdersKeylist().Keylistreturn(head,ctx, event, constrainslang,constrains);

		new CkcreOrdersKeylist().Keylistreturn(head,ctx, event, constrainslang,constrains);
	}

	@ScreenEventHandler(type = ScreenEventType.ENTER)
	public void screenEnter(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		try{
			LoggerUtil.initialize(ctx);
			new EvtScreenEnterCKCREORDERS().LogonCheckScreenEnter(head,ctx);
		}catch(EventException e){
			throw new EventException(e.getMessage());
		}
		AsynchronousKeylist.getAsynchronnousKeylist(ctx);
	}

	@ButtonEventHandler(field="ckcrelegalformbn", type = ButtonEventType.AFTER)
	public void ckcrelegalformbnAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		if(head.getCkcrecrefoid()==null||head.getCkcrecrefoid().matches("")) {
			FO.box("Fehler","Bitte geben sie eine Identnummer ein.");
			return;
		}
		new CkcreOrdersKeylist().Keylistreturn(head,ctx, event, constrainslang,constrains);
	}

	@FieldEventHandler(field="ckcrekunde", type = FieldEventType.EXIT)
	public void ckcrekundeExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		if (head.getCkcrekunde() != null && !head.getCkcrekunde().toString().matches("")) {
			head.setString("Ckcrelief", "");
			SelectableObject object = head.getCkcrekunde().getId();
			if (head.getCkcrekunde() instanceof Customer) {
				head.setString("Ckcrelief", "");
				SelectionBuilder<Customer> selectionBuilder = SelectionBuilder.create(Customer.class);
				selectionBuilder.add(Conditions.eq(Customer.META.id, object.id()));
				Customer cust = QueryUtil.getFirst(ctx, selectionBuilder.build());
				if (!head.getCkcrekundekon()) {
					new SetCustomer().Customerset(cust, head, ctx);
				} else {
					new SetPrivatPersonCustomer().PrivatPersonsetCustomer(cust, head, ctx);
				}
			}
				if (head.getCkcrekunde() instanceof Prospect) {

					SelectionBuilder<Prospect> selectionBuilder = SelectionBuilder.create(Prospect.class);
					selectionBuilder.add(Conditions.eq(Prospect.META.id, object.id()));
					Prospect cust = QueryUtil.getFirst(ctx, selectionBuilder.build());
					if (!head.getCkcrekundekon()) {
						new SetProspect().Prospectset(cust, head, ctx);
					} else {
						new SetPrivatPersonProspect().PrivatPersonsetProspect(cust, head, ctx);
					}
				}
			head.invokeButton("start");
		}
	}

	@FieldEventHandler(field="ckcrelief", type = FieldEventType.EXIT)
	public void ckcreliefExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		if(head.getCkcrelief()!=null&&!head.getCkcrelief().toString().matches("")) {
			head.setString("Ckcrekunde", "");
			SelectableObject object = head.getCkcrelief().getId();
			if(head.getCkcrelief() instanceof Vendor) {
				SelectionBuilder<Vendor> selectionBuilder = SelectionBuilder.create(Vendor.class);
				selectionBuilder.add(Conditions.eq(Vendor.META.id, object.id()));
				Vendor vendor = QueryUtil.getFirst(ctx, selectionBuilder.build());
				if (!head.getCkcrekundekon()) {
					new SetVendor().Vendorset(vendor, head, ctx);
				} else {
					new SetPrivatPersonVendor().PrivatPersonsetVendor(vendor, head, ctx);
				}
			}
		}

	}

	@ButtonEventHandler(field="ckcredatvbn", type = ButtonEventType.AFTER)
	public void ckcredatvbnAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
			head.setCkcrereportpdf("file:\\"+head.getCkcredatv());
	}
	@FieldEventHandler(field="ckcrecrefoid", type = FieldEventType.EXIT)
	public void ckcrecrefoidExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		if(head.getCkcrecrefoid()!=null||!head.getCkcrecrefoid().matches("")){
		Tlogonresponsebody response = new LogonabasCWS().LogonabasCWS(ctx);
		Tproductavailabilityresponsebody prod = new CkcreAvProd().Prodpruef(head,ctx);
		final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
		PasswordEditor editor=null;
		try {
			editor = operatorCode.createEditor();
			editor.open(EditorAction.VIEW);
			constrainslang = new GetConstrain().GetServiceConstrain(editor.getYckcrelang(), "order", ctx, response, prod);
			if(head.getCkcreprodtype()!=null&&!head.getCkcreprodtype().matches(""))
			constrains = new GetConstrain().GetConstrains(head.getCkcreprodtype(), "order", ctx, response,prod);
			constrainstate = new GetConstrain().GetState(editor.getYckcrelang(), "order", ctx, response);
		}catch (Exception e){
			FO.box("Fehler", "Fehler beim auslesen der Produktdaten.");
			LoggerUtil.LOG.error("FehlerID", e);
		}finally{
			editor.commit();
			if(editor!=null&&editor.active()){
				editor.abort();
				editor=null;
			}
		}
		}
		//ctx.out().println("ID"+constrains.toString()+"\\r"+constrainstate.toString()+"\\r"+constrainslang.toString());

	}
	@FieldEventHandler(field="ckcreordervor", type = FieldEventType.EXIT)
	public void ckcreordervorExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCREORDERS head) throws EventException {
		new SetVorbelegung().order(ctx,head);

	}

}
