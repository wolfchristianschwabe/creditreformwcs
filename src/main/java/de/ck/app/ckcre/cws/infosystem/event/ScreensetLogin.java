package de.ck.app.ckcre.cws.infosystem.event;
/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */

import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;

/**
 * @author christian.schwabe
 * @param: head is the Context of the IS and screenControl is the Screencontrol object of the Current loaded IS
 * @do This Method sets the Protection of the Boolfields in the IS CKCRELOGIN
 */
public class ScreensetLogin {
    public void Screenset(CKCRELOGIN head, ScreenControl screenControl){
        screenControl.setProtection(head, head.META.ckcreblanceans,true);
        screenControl.setProtection(head, head.META.ckcrecollectfs,true);
        screenControl.setProtection(head, head.META.ckcrekeylists,true);
        screenControl.setProtection(head, head.META.ckcrecollectf,true);
        screenControl.setProtection(head, head.META.ckcremails,true);
        screenControl.setProtection(head, head.META.ckcremonstatus,true);
        screenControl.setProtection(head, head.META.ckcreprocess,true);
        screenControl.setProtection(head, head.META.ckcreblanceanm,true);
        screenControl.setProtection(head, head.META.ckcrebonimar,true);
        screenControl.setProtection(head, head.META.ckcrecancelstmon,true);
        screenControl.setProtection(head, head.META.ckcrechangem,true);
        screenControl.setProtection(head, head.META.ckcrechangepw,true);
        screenControl.setProtection(head, head.META.ckcrecollecta,true);
        screenControl.setProtection(head, head.META.ckcrecollectd,true);
        screenControl.setProtection(head, head.META.ckcrecollecto,true);
        screenControl.setProtection(head, head.META.ckcreconsumerr,true);
        screenControl.setProtection(head, head.META.ckcreforwardre,true);
        screenControl.setProtection(head, head.META.ckcregeninfo,true);
        screenControl.setProtection(head, head.META.ckcreidentify,true);
        screenControl.setProtection(head, head.META.ckcreindentifyr,true);
        screenControl.setProtection(head, head.META.ckcrecreditorfq,true);
        screenControl.setProtection(head, head.META.ckcremailboxd,true);
        screenControl.setProtection(head, head.META.ckcremailboxe,true);
        screenControl.setProtection(head, head.META.ckcreorder,true);
        screenControl.setProtection(head, head.META.ckcrechangemon,true);
        screenControl.setProtection(head, head.META.ckcreupgradel,true);
        screenControl.setProtection(head, head.META.ckcresearch,true);
        screenControl.setProtection(head, head.META.ckcreproductav,true);
        screenControl.setProtection(head, head.META.ckcrereport,true);
        screenControl.setProtection(head, head.META.ckcrecreditorf,true);
        screenControl.setProtection(head, head.META.ckcrepayment,true);
        screenControl.setProtection(head, head.META.ckcreparticipatio,true);
        screenControl.setProtection(head, head.META.ckcreparticidat,true);
        screenControl.setProtection(head, head.META.ckcrerecivable,true);
        screenControl.setProtection(head, head.META.ckcresrecivable,true);
        screenControl.setProtection(head, head.META.ckcretrecivable,true);
        screenControl.setProtection(head, head.META.ckcrelogon,true);
        screenControl.setProtection(head, head.META.ckcredefkeylist,true);
    }
}
