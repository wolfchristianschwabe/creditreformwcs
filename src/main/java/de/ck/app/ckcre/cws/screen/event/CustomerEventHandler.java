package de.ck.app.ckcre.cws.screen.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.AppContext;
import de.abas.erp.api.commands.CommandFactory;
import de.abas.erp.api.commands.FieldManipulator;
import de.abas.erp.api.gui.MenuBuilder;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.ButtonEventHandler;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ButtonEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRESEARCH;
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReport;
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReportView;
import de.abas.erp.db.schema.customer.CustomerEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.ck.app.ckcre.cws.util.AsynchronousKeylist;
import de.ck.app.ckcre.cws.util.Constants;
import de.ck.app.ckcre.cws.util.Keylist;
import de.ck.app.ckcre.cws.util.LoggerUtil;

import java.io.File;
import java.io.FilenameFilter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Objects;

@EventHandler(head = CustomerEditor.class)

@RunFopWith(EventHandlerRunner.class)

public class CustomerEventHandler {

	@ButtonEventHandler(field="yckcrebuadressprue", type = ButtonEventType.AFTER)
	public void yckcrebuadressprueAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CustomerEditor head) throws EventException {
		CommandFactory commandFactory = AppContext.createFor(ctx).getCommandFactory();
		FieldManipulator params = commandFactory.getScrParamBuilder(CKCRESEARCH.class);
		params.setReference(CKCRESEARCH.META.ckcrekundea, head);
		commandFactory.startInfosystem(CKCRESEARCH.class, params);
	}

	@ButtonEventHandler(field="yckcrebukreditlim", type = ButtonEventType.AFTER)
	public void yckcrebukreditlimAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CustomerEditor head) throws EventException {
		head.setCredLim(head.getYckcrekreditlimit());
	}
	@ButtonEventHandler(field="yckcreorder", type = ButtonEventType.AFTER)
	public void yckcreorderAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CustomerEditor head) throws EventException {
		AppContext appContext = AppContext.createFor(ctx);
		CommandFactory commandFactory = appContext.getCommandFactory();
		FieldManipulator<CKCREORDERS> scrParamBuilder = commandFactory.getScrParamBuilder(CKCREORDERS.class);
		scrParamBuilder.setReference(CKCREORDERS.META.ckcrekunde,head.getId());
		scrParamBuilder.setField(CKCREORDERS.META.ckcrecrefoid,head.getYckcreident());
		scrParamBuilder.setField(CKCREORDERS.META.ckcrekcountry,head.getStateOfTaxOffice().getCtryCode2Char().toUpperCase());
		commandFactory.startInfosystem(CKCREORDERS.class, scrParamBuilder);
	}
	@ButtonEventHandler(field="yckcrereport", type = ButtonEventType.AFTER)
	public void yckcrereportAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CustomerEditor head) throws EventException {
		AppContext appContext = AppContext.createFor(ctx);
		CommandFactory commandFactory = appContext.getCommandFactory();
		FieldManipulator<CrefoReport> scrParamBuilder = commandFactory.getScrParamBuilder(CrefoReport.class);
		scrParamBuilder.setReference(CrefoReport.META.kunde,head);
		scrParamBuilder.setField(CrefoReport.META.crefonum,head.getYckcreident());
		commandFactory.startInfosystem(CrefoReport.class, scrParamBuilder);
	}
	@ButtonEventHandler(field="yckcredetails", type = ButtonEventType.AFTER)
	public void yckcredetailsAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CustomerEditor head) throws EventException {
		//TODO: auslagern auf Infosystem --> Infosystem aufrufen!

		CommandFactory commandFactory = AppContext.createFor(ctx).getCommandFactory();
		FieldManipulator params = commandFactory.getScrParamBuilder(CrefoReportView.class);
		params.setReference(CrefoReportView.META.kunde, head);
		commandFactory.startInfosystem(CrefoReportView.class, params);

//		try{
//			File pdfs = new File("owcrefo/pdf/");
//
//			FilenameFilter filter = new FilenameFilter() {
//				@Override
//				public boolean accept(File dir, String name) {
//					return name.split("_")[0].compareTo(head.getYckcrecrefonum()) == 0;
//				}
//			};
//
//			MenuBuilder<File> menu = new MenuBuilder<File>(ctx, "Report wählen");
//			for(Object currFile : Arrays.stream(Objects.requireNonNull(pdfs.listFiles(filter))).sorted( Comparator.comparing( it -> it.getName().split("_")[2]) ).toArray()) {
//				try{
//					File cF = (File)currFile;
//					LoggerUtil.LOG.debug("gefundene Datei: " + cF.getAbsolutePath());
//					LocalDateTime date = LocalDateTime.parse(cF.getName().split("_")[2].split(".pdf")[0], DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
//					String key = cF.getName().split("_")[1];
//					try{
//						key = Keylist.findDesignationByKey(Constants.keylist, key, FO.getOperatingLanguage().compareTo("D") == 0 ? "de" : "en");
//					}catch(Exception e){LoggerUtil.LOG.error("Fehler beim Auslesen von Key " + key, e);}
//					menu.addItem(cF, date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm")), key);
//				}catch(Exception e) {
//					LoggerUtil.LOG.warn("Fehler beim Auslesen von PDF-Typen (Filename)", e);
//				}
//			}
//			head.setYckcrelastreport(menu.show().getAbsolutePath());
//		}
//		catch(Exception e){
//			FO.box("Warnung", "Keine PDF gewählt!");
//		}
	}

	@ScreenEventHandler(type = ScreenEventType.ENTER)
	public void screenEnter(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CustomerEditor head) throws EventException {
		AsynchronousKeylist.getAsynchronnousKeylist(ctx);

		//TODO: Auslagern auf Infosystem!
//		//Letzten Report setzen
//		try{
//			File pdfs = new File("owcrefo/pdf/");
//
//			FilenameFilter filter = new FilenameFilter() {
//				@Override
//				public boolean accept(File dir, String name) {
//					return name.split("_")[0].compareTo(head.getYckcrecrefonum()) == 0;
//				}
//			};
//			head.setYckcrelastreport( Arrays.stream(Objects.requireNonNull(pdfs.listFiles(filter))).max(Comparator.comparing(it -> it.getName().split("_")[2])).get().getAbsolutePath());
//		}catch(Exception e){
//			LoggerUtil.LOG.error("Fehler beim Vorbelegen des letzten Reports!", e);
//		}
	}

}