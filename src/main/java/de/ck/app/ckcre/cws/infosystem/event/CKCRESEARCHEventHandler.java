package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.AppContext;
import de.abas.erp.api.commands.CommandFactory;
import de.abas.erp.api.commands.FieldManipulator;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.ButtonEventHandler;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.annotation.ScreenEventHandler;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.event.ScreenEvent;
import de.abas.erp.axi2.type.ButtonEventType;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.axi2.type.ScreenEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.SelectableObject;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRESEARCH;
import de.abas.erp.db.infosystem.custom.owcrefo.CrefoReport;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.abas.erp.db.schema.customer.Customer;
import de.abas.erp.db.schema.customer.Prospect;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.db.selection.Conditions;
import de.abas.erp.db.selection.SelectionBuilder;
import de.abas.erp.db.util.QueryUtil;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;
import de.abas.jfop.base.Color;
import de.ck.app.ckcre.cws.util.AsynchronousKeylist;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tlogonresponsebody;

import java.util.HashMap;


@EventHandler(head = CKCRESEARCH.class, row = CKCRESEARCH.Row.class)

@RunFopWith(EventHandlerRunner.class)

public class CKCRESEARCHEventHandler {
    static HashMap<String,Object> constrainslang = null;
    static HashMap<String,String> constrains = null;
    static HashMap<String,String> constrainstate = null;
    @ButtonEventHandler(field = "ckcreuebernehmen", type = ButtonEventType.AFTER)
    public void ckcreuebernehmenAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        if (head.table().getRowCount() > 0) {
            for (CKCRESEARCH.Row row : head.getTableRows()) {
                if (row.getBoolean("ckcreauswahl")) {
                    if (row.getCkcreid()!=null&&!row.getCkcreid().matches("")&&row.getCkcreid().matches("^\\d+")) {
                        FO.box("Auftrag", "\u00D6ffne Rechercheauftrag...");
                        AppContext appContext = AppContext.createFor(ctx);
                        CommandFactory commandFactory = appContext.getCommandFactory();
                        FieldManipulator<CKCREORDERS> scrParamBuilder = commandFactory.getScrParamBuilder(CKCREORDERS.class);
                        scrParamBuilder.setField(CKCREORDERS.META.ckcrecompany, row.getCkcrename());
                        scrParamBuilder.setField(CKCREORDERS.META.ckcrecrefoid, row.getCkcreid());
                        scrParamBuilder.setField(CKCREORDERS.META.ckcrekstreet, row.getCkcretstreet());
                        scrParamBuilder.setField(CKCREORDERS.META.ckcrekhousnumber, row.getCkcrehousnumber());
                        scrParamBuilder.setField(CKCREORDERS.META.ckcrehousnumberaff, row.getCkcrehousnumberaf());
                        scrParamBuilder.setField(CKCREORDERS.META.ckcrekpostcode, row.getCkcretpostcode());
                        scrParamBuilder.setField(CKCREORDERS.META.ckcrekcity, row.getCkcretort());
                        scrParamBuilder.setField(CKCREORDERS.META.ckcrekcountry, row.getString("Ckcretlandk").toUpperCase());
                        scrParamBuilder.setField(CKCREORDERS.META.ckcrelegalform, row.getString("Ckcretlegalformc"));
                        scrParamBuilder.setField(CKCREORDERS.META.ckcrelegalformklar, row.getString("Ckcretlegalform"));
                        scrParamBuilder.pressButton(CKCREORDERS.META.start);
                        commandFactory.startInfosystem(CKCREORDERS.class, scrParamBuilder);
                        return;
                    } else {
                        FO.box("Fehler", "Diese Zeile ist leer.");
                        return;
                    }
                }

            }
            FO.box("Fehler", "Es wurde keine Zeile gew\u00e4hlt.");
            return;

        }
    }
    @ButtonEventHandler(field = "ckcrereport", type = ButtonEventType.AFTER)
    public void ckcrereportAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        if(head.table().getRowCount()>0) {
            for (CKCRESEARCH.Row row : head.getTableRows()) {
                if (row.getBoolean("ckcreauswahl")) {
                    if (row.getCkcreid() != null && !row.getCkcreid().matches("") && row.getCkcreid().matches("^\\d+")) {
                        AppContext appContext = AppContext.createFor(ctx);
                        CommandFactory commandFactory = appContext.getCommandFactory();
                        FieldManipulator<CrefoReport> scrParamBuilder = commandFactory.getScrParamBuilder(CrefoReport.class);
                        scrParamBuilder.setField(CrefoReport.META.crefonum, row.getCkcreid());
                        commandFactory.startInfosystem(CrefoReport.class, scrParamBuilder);
                        return;
                    }
                    else {
                        FO.box("Fehler", "Diese Zeile ist leer.");
                        return;
                    }
                }
            }
            FO.box("Fehler", "Es wurde keine Zeile gew\u00e4hlt.");
            return;

        }
    }
    @ButtonEventHandler(field = "ckcrecancel", type = ButtonEventType.AFTER)
    public void ckcrecancelAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        new CkcreSearchUtil().Clear(head);
        head.table().clear();
    }
    @ButtonEventHandler(field = "ckcrerequest", type = ButtonEventType.AFTER)
    public void ckcrerequestAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {

        if (head.getCkcrecountry() == null || head.getCkcresearchtype() == null || head.getCkcrecountry().equals("") || head.getCkcresearchtype().equals("")){
            screenControl.setColor(head, head.META.ckcresearchtype, Color.WHITE, Color.RED);
            screenControl.setColor(head, head.META.ckcrecountry, Color.WHITE, Color.RED);
            FO.box("Fehler","Pflichtfelder sind nicht gef\u00fcllt!");
    }else {
            screenControl.setColor(head, head.META.ckcresearchtype, Color.BLACK, Color.WHITE);
            screenControl.setColor(head, head.META.ckcrecountry, Color.BLACK, Color.WHITE);
            if(head.getCkcresearchid().matches("^\\d+")||head.getCkcresearchid().isEmpty()) {
                new Search().Search(head, ctx);
            }else {
                FO.box("Fehler", "Bitte geben sie die Identnummer nur mit Zahlen an.");
            }
           }

    }
	@FieldEventHandler(field="ckcrekundea", type = FieldEventType.EXIT)
	public void ckcrekundeaExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        if(head.getCkcrekundea()==null||head.getCkcrekundea().id().isNullRef()){
            new CkcreSearchUtil().Clear((CKCRESEARCH)head);
            return;
        }
       if(head.getCkcrekundea()!=null&&!head.getCkcrekundea().toString().matches("")) {
            SelectableObject object = head.getCkcrekundea().getId();
            if (head.getCkcrelief() != null && !head.getCkcrelief().getIdno().matches("")) {
                head.setString("Ckcrelief", "");
            }
            if (head.getCkcrekundea() instanceof Customer) {
                SelectionBuilder<Customer> selectionBuilder = SelectionBuilder.create(Customer.class);
                selectionBuilder.add(Conditions.eq(Customer.META.id, object.id()));
                Customer cust = QueryUtil.getFirst(ctx, selectionBuilder.build());
                new SetCustomer().Customerset(cust, head, ctx);
            }
            if (head.getCkcrekundea() instanceof Prospect) {
                SelectionBuilder<Prospect> selectionBuilder = SelectionBuilder.create(Prospect.class);
                selectionBuilder.add(Conditions.eq(Prospect.META.id, object.id()));
                Prospect cust = QueryUtil.getFirst(ctx, selectionBuilder.build());
                    new SetProspect().Prospectset(cust, head, ctx);
            }
        }
    }
    @ButtonEventHandler(field="ckcrelang", type = ButtonEventType.AFTER)
    public void ckckcrelangAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        if (constrains == null || constrainslang == null || constrainstate == null) {
        Tlogonresponsebody response = new LogonabasCWS().LogonabasCWS(ctx);
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        PasswordEditor editor=null;
        try {
            editor = operatorCode.createEditor().open(EditorAction.VIEW);
            constrainslang = new GetConstrain().GetServiceConstrain(editor.getYckcrelang(), "search", ctx, response,null);
            constrains = new GetConstrain().GetConstrains(editor.getYckcrelang(), "search", ctx, response,null);
            constrainstate = new GetConstrain().GetState(editor.getYckcrelang(), "search", ctx, response);
        }catch (Exception e){
            FO.box("Fehler","Fehler bei Abruf der Nutzerdaten.");
            LoggerUtil.LOG.error("Fehler beim Abruf der Nutzerdaten.",e);
        }finally {
            if(editor!=null&&editor.active()){
                editor.abort();
                editor=null;
                //FO.box("Meldung","Daten sind vollst\u00e4ndig.");
            }
        }
        }
        new CkcreSearchKeylist().Keylistreturn(head,ctx, event,constrainslang,constrainstate);
        }
	@ButtonEventHandler(field="ckcrelegalformbn", type = ButtonEventType.AFTER)
	public void ckcrelegalformbnAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        if(head.getCkcrecountry()!=null&&head.getCkcrecountry()!="")
        new CkcreSearchKeylist().Keylistreturn(head,ctx, event,constrainslang,constrains);
        else{
            FO.box("Meldung","Bitte w\u00e4hlen sie eine Sprache vorher aus.");
        }
	}
	
	@ButtonEventHandler(field="ckcresuchtyp", type = ButtonEventType.AFTER)
	public void ckcresuchtypAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        if (constrains == null || constrainslang == null || constrainstate == null) {
            Tlogonresponsebody response = new LogonabasCWS().LogonabasCWS(ctx);
            final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
            PasswordEditor editor = null;
            try {
                editor = operatorCode.createEditor();
                editor.open(EditorAction.VIEW);
                          constrainslang = new GetConstrain().GetServiceConstrain(editor.getYckcrelang(), "search", ctx, response,null);
                constrains = new GetConstrain().GetConstrains(editor.getYckcrelang(), "search", ctx, response,null);
                constrainstate = new GetConstrain().GetState(editor.getYckcrelang(), "search", ctx, response);
            } catch (Exception e) {
                    FO.box("Fehler", "Fehler bei Abruf der Nutzerdaten.");
                    LoggerUtil.LOG.error("Fehler beim Abruf der Nutzerdaten.", e);
            } finally {
                    if (editor != null && editor.active()) {
                        editor.abort();
                        editor = null;
                        //FO.box("Meldung", "Daten sind vollst\u00e4ndig.");
                    }
                }
            }
        new CkcreSearchKeylist().Keylistreturn(head, ctx, event, constrainslang, constrains);

	}
	
	@ScreenEventHandler(type = ScreenEventType.ENTER)
	public void screenEnter(ScreenEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        CheckLogondata logincheck = new CheckLogondata();
        if(!logincheck.LoginParamCheck(ctx,head)) {
            AppContext appContext = AppContext.createFor(ctx);
            CommandFactory commandFactory = appContext.getCommandFactory();
            FieldManipulator<CKCRELOGIN> scrParamBuilder = commandFactory.getScrParamBuilder(CKCRELOGIN.class);
            scrParamBuilder.pressButton(CKCRELOGIN.META.start);
            commandFactory.startInfosystem(CKCRELOGIN.class, scrParamBuilder);
            throw new EventException("Bitte f\u00fchren sie erst das Login aus.");
        }if(!logincheck.LoginPasswdCheck(ctx)) {
            AppContext appContext = AppContext.createFor(ctx);
            CommandFactory commandFactory = appContext.getCommandFactory();
            FieldManipulator<CKCRELOGIN> scrParamBuilder = commandFactory.getScrParamBuilder(CKCRELOGIN.class);
            scrParamBuilder.pressButton(CKCRELOGIN.META.start);
            commandFactory.startInfosystem(CKCRELOGIN.class, scrParamBuilder);
            throw new EventException("Bitte \u00e4ndern sie zuerst ihr Pers\u00F6nliches Passwort.");
        }
        AsynchronousKeylist.getAsynchronnousKeylist(ctx);
	}
	@ButtonEventHandler(field="ckcreregtype", type = ButtonEventType.AFTER)
	public void ckcreregtypeAfter(ButtonEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        if(head.getCkcrecountry()!=null&&head.getCkcrecountry()!="")
        new CkcreSearchKeylist().Keylistreturn(head,ctx, event, constrainslang,constrains);
        else{
            FO.box("Meldung","Bitte w\u00e4hlen sie eine Sprache vorher aus.");
        }
	}
	@FieldEventHandler(field="ckcreauswahl", type = FieldEventType.EXIT, table=true)
	public void ckcreauswahlExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head, CKCRESEARCH.Row currentRow) throws EventException {
        for(CKCRESEARCH.Row row: head.getTableRows()){
            if(row.getRowNo()!=currentRow.getRowNo()&&row.getBoolean("ckcreauswahl")){
              row.setBoolean("ckcreauswahl",false);
            }
        }

    }
	@FieldEventHandler(field="ckcrelief", type = FieldEventType.EXIT)
	public void ckcreliefExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        if(head.getCkcrelief()==null||head.getCkcrelief().id().isNullRef()){
            new CkcreSearchUtil().Clear((CKCRESEARCH)head);
            return;
        }
        if(head.getCkcrelief()!=null&&!head.getCkcrelief().toString().matches("")) {
            if (head.getCkcrekundea() != null && !head.getCkcrekundea().getIdno().matches("")) {
                head.setString("Ckcrekundea", "");
            }
            SelectableObject object = head.getCkcrelief().getId();
            SelectionBuilder<Vendor> selectionBuilder = SelectionBuilder.create(Vendor.class);
            selectionBuilder.add(Conditions.eq(Vendor.META.id, object.id()));
            Vendor vendor = QueryUtil.getFirst(ctx, selectionBuilder.build());
            new SetVendor().Vendorset(vendor, head, ctx);
        }

	}
    @FieldEventHandler(field="ckcrecompany", type = FieldEventType.EXIT)
    public void ckcrecompanyExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, CKCRESEARCH head) throws EventException {
        if(head.getCkcrecompany().length()<2){
            FO.box("Fehler","Bitte geben sie mehr als 2 Buchstaben im Feld Firmenname ein.");
        }
    }



}

