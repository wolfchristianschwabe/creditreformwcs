package de.ck.app.ckcre.cws.infosystem.event;
/**
 * @author Christian Schwabe Computerkomplett
 */

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.schema.customer.Customer;

public class SetPrivatPersonCustomer {
    /**
     * @do Import of the Vendor into the IS filling up all Fields
     * @param cust The Customer that needs to be imported
     * @param head The Header of the IS where the Data comes from
     * @param ctx The Context in witch the IS is called up
     */
    public void PrivatPersonsetCustomer(Customer cust, CKCREORDERS head, DbContext ctx) {
        String affix = "";
        String number ="";
        new CkcreordersUtil().clear(head);
        if(head instanceof CKCREORDERS){
            if(cust.getAddr()!=null&&!cust.getAddr().matches("")) {
                if(cust.getStreet()!=null&&!cust.getStreet().matches("")) {
                    if (cust.getStreet().split("[\\d]").length>1)
                        affix = cust.getStreet().split("[\\d]")[1];
                    number = cust.getStreet().replaceAll("[\\D]","");
                }
                ((CKCREORDERS) head).setCkcresurname(cust.getAddr().substring(0,cust.getAddr().indexOf(',')));
                ((CKCREORDERS) head).setCkcrecrefoid(cust.getString("Yckcreident"));
                ((CKCREORDERS) head).setCkcrefirstname(cust.getAddr().substring(cust.getAddr().indexOf(',')).replace(",",""));
                if(cust.getTown()!=null&&!cust.getTown().matches(""))
                    ((CKCREORDERS) head).setCkcrekcity(cust.getTown());
             //   if(cust.getStateOfTaxOffice().getCtryCode2Char()!=null&&!cust.getStateOfTaxOffice().getCtryCode2Char().matches(""))
               //     ((CKCREORDERS) head).setCkcrekcountry(cust.getStateOfTaxOffice().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                    ((CKCREORDERS) head).setCkcrekhousnumber(number);
                if(affix!=null&&!affix.matches(""))
                    ((CKCREORDERS) head).setCkcrehousnumberaff(affix);
                if(cust.getZipCode()!=null&&!cust.getZipCode().matches(""))
                    ((CKCREORDERS) head).setCkcrekpostcode(cust.getZipCode());
                if(cust.getRegion()!=null&&!cust.getRegion().toString().matches(""))
                    ((CKCREORDERS) head).setCkcrekregion(cust.getRegion().getCode());
                if(cust.getStreet()!=null&&!cust.getStreet().matches(""))
                    ((CKCREORDERS) head).setCkcrekstreet(cust.getStreet().split("[\\d]")[0]);
                return;
            }if(cust.getAddr2()!=null&&!cust.getAddr2().matches("")) {
                ((CKCREORDERS) head).setCkcrecrefoid(cust.getString("Yckcreident"));
                if (cust.getStreet2().split("[\\d]").length>1)
                    affix = cust.getStreet2().split("[\\d]")[1];
                number = cust.getStreet2().replaceAll("[\\D]","");
                ((CKCREORDERS) head).setCkcresurname(cust.getAddr2().substring(0,cust.getAddr2().indexOf(',')));
                ((CKCREORDERS) head).setCkcrefirstname(cust.getAddr2().substring(cust.getAddr2().indexOf(',')).replace(",",""));

                if(cust.getTown2()!=null&&!cust.getTown2().matches(""))
                    ((CKCREORDERS) head).setCkcrekcity(cust.getTown2());
              //  if(cust.getStateOfTaxOffice2().getCtryCode2Char()!=null&&!cust.getStateOfTaxOffice2().getCtryCode2Char().matches(""))
               //     ((CKCREORDERS) head).setCkcrekcountry(cust.getStateOfTaxOffice2().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                    ((CKCREORDERS) head).setCkcrekhousnumber(number);
                if(affix!=null&&!affix.matches(""))
                    ((CKCREORDERS) head).setCkcrehousnumberaff(affix);
                if(cust.getZipCode2()!=null&&!cust.getZipCode2().matches(""))
                    ((CKCREORDERS) head).setCkcrekpostcode(cust.getZipCode2());
                if(cust.getRegion2().getCode()!=null&&!cust.getRegion2().getCode().matches(""))
                    ((CKCREORDERS) head).setCkcrekregion(cust.getRegion2().getCode());
                if(cust.getStreet2()!=null&&!cust.getStreet2().matches(""))
                    ((CKCREORDERS) head).setCkcrekstreet(cust.getStreet2().split("[\\d]")[0]);
                return;
            }else {
                FO.box("Fehler","Der Kunde ist nicht korrekt angelegt.");
            }

        }



    }
}
