package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.common.type.AbasDate;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.*;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.app.ckcre.cws.util.LoggerUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author christian.schwabe
 * @do This Class reads the Login Information and Checks if the Data out of the Passworddataset is correct.
 */
public class CheckLogondata {
    public Object LogonGranted(DbContext ctx, CKCRELOGIN head){
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        operatorCode.createEditor();
        List<Boolean> checked = new ArrayList<>();
        PasswordEditor editor = null;
            try {
                editor = operatorCode.createEditor();
                editor.open(EditorAction.VIEW);
                checked.add(editor.getYckcrebrecht());
                checked.add(editor.getYckcreadmin());
            }catch (Exception e) {
                LoggerUtil.LOG.error("Fehler beim Auslesen der Daten", e);
                FO.box("Fehler", "Fehler beim lesen der Daten.");
            }finally
            {
                if (editor != null && editor.active()) {
                    editor.abort();
                    editor = null;
                }
            }
            return checked;
    }
    public boolean LoginParamCheck(DbContext ctx, Object head){
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        operatorCode.createEditor();
        PasswordEditor editor = null;
        try {
            editor = operatorCode.createEditor();
            editor.open(EditorAction.VIEW);
            if(editor.getYckcrebrecht()) {
                if (!editor.getYckcrekeylist().matches("")) {
                    if (!editor.getYckcreuser().matches("")) {
                        if (!editor.getYckcrepw().matches("")) {
                            if (!editor.getYckcregenpwd().matches("")) {
                                if (head instanceof CKCRESEARCH) {
                                   if(editor.getYckcresearch()) {
                                       editor.commit();
                                       return true;
                                   }
                                }
                                if (head instanceof CKCREPRODP) {

                                    if(editor.getYckcreproductav()) {
                                    editor.commit();
                                    return true;
                                }
                                }
                                if (head instanceof CKCREORDERS) {

                                    if(editor.getYckcreorder()) {
                                    editor.commit();
                                    return true;
                                }
                                }
                                if (head instanceof CrefoReport) {

                                    if(editor.getYckcrereport()) {
                                    editor.commit();
                                    return true;
                                }
                                }
                            } else {
                                editor.commit();
                                return false;
                            }
                        } else {
                            editor.commit();
                            return false;
                        }
                    } else {
                        editor.commit();
                        return false;
                    }
                } else {
                    editor.commit();
                    return false;
                }
            }else{
                editor.commit();
                return false;
            }
            }catch (Exception e) {
            LoggerUtil.LOG.error("Fehler beim Auslesen der Daten", e);
            FO.box("Fehler", "Fehler beim Auslesen der Daten.");
        }finally
        {
            if (editor != null && editor.active()) {
                editor.abort();
                editor = null;
            }
        }
        return false;
    }

    public boolean LoginPasswdCheck(DbContext ctx) {
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        PasswordEditor editor = null;
        try {
            editor = operatorCode.createEditor();
            editor.open(EditorAction.VIEW);
            AbasDate ablaufdatum =  AbasDate.valueOf(LocalDateTime.now().minusDays(42).format(DateTimeFormatter.ofPattern("yyyyMMdd")));
            if(editor.getYckcreloginlast()!=null) {
                //ctx.out().println(ablaufdatum.compareTo(editor.getYckcreloginlast()));
                if (ablaufdatum.compareTo(editor.getYckcreloginlast()) > 0){
                    FO.box("Fehler", "Passwort muss ge\u00e4ndert werden!");
                    editor.commit();
                    return false;
                }else{
                    editor.commit();
                    return true;
                }
            }

        } catch (Exception e) {
            LoggerUtil.LOG.error("Fehler beim Auslesen der Daten", e);
            FO.box("Fehler", "Fehler beim lesen der Daten.");
        } finally {
            if (editor != null && editor.active()) {
                editor.abort();
                editor = null;
            }
        }
        return false;
    }
}
