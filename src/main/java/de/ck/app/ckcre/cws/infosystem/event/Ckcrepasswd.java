/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */
package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.common.type.AbasDate;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.ChangepasswordResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tchangepasswordrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

/**
 * @author christian.schwabe
 * @param: ctx is the Database Context witch the IS is loaded in
 * @do This Method Changes the Password on the Webservice
 */
public class Ckcrepasswd {
    public void Passwd(CKCRELOGIN head, DbContext ctx) throws Exception {
        if (!head.getCkcrepasswdwh().equals(head.getCkcrepasswdneu())) {
            FO.box("Felder Passwort und Passwort wiederholen stimme nicht \u00fcberein.");
            return;
        }
        if (head.getCkcrelogin() == null || head.getCkcrelogin() == "") {
            FO.box("Loginname nicht angegeben.");
            return;
        }
        if (head.getCkcrepasswd() == null || head.getCkcrepasswd() == "") {
            FO.box("Passwortfeld darf nicht leer sein.");
            return;
        }
        Trequestheader requestlogin = new Logonrequest().buildupLogin(head.getCkcregenpw(), head.getCkcrepasswd(), head.getCkcrelangchosen(), head.getCkcrelogin(), Integer.parseInt(head.getCkcrekeylist()));
        Tchangepasswordrequestbody change = new Tchangepasswordrequestbody();
        change.setNewpassword(head.getCkcrepasswdneu());
        ChangepasswordResponse response = (ChangepasswordResponse) Services.Changepassword.start(change, requestlogin);
        if (!response.getHeader().getResponseid().equals("")) {
            if (head.getCkcredloginsp()) {
                final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
                PasswordEditor    editor = operatorCode.createEditor();
            editor.open(EditorAction.UPDATE);
                try {
                    editor.setYckcrepw(head.getCkcrepasswd());
                    editor.setString("Yckcreuser", head.getCkcrelogin());
                    editor.setString("Yckcreloginlast", new AbasDate().toString());
                    editor.commit();
                } catch (Exception e) {
                    LoggerUtil.LOG.error("Fehler beim schreiben der Nutzerdaten durch den Editor",e);
                    FO.box("Fehler",e.getMessage());
                } finally {
                    if (editor.active()) {
                        editor.abort();
                        editor = null;
                    }
                }
                FO.box("Passwort ge\u00e4ndert & gespeichert");
            }
        }
    }
}

