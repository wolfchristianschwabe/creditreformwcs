/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */
package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS.Row;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.MailboxdirectoryResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tperiod;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.util.GregorianCalendar;

/**
 * @author christian.schwabe
 * @param: ctx is the Database Context witch the IS is loaded in
 * @do This Method Calls the Mailbox of th Webservice
 */
public class Mailboxcall {
	public void mailboxcallup(DbContext ctx, CKCREORDERS head) {
		Trequestheader logonrequest = null;
		MailboxdirectoryResponse mailbox = null;
		Row row;
		Tperiod period = new Tperiod();
		GregorianCalendar gregor = new GregorianCalendar();
		Tmailboxdirectoryrequestbody probs = new Tmailboxdirectoryrequestbody();
		final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
		PasswordEditor editor=null;
		try {
			editor = operatorCode.createEditor();
			editor.open(EditorAction.VIEW);
		}catch(Exception e){
			FO.box("Fehler","Fehler beim Abruf der Daten f\u00fcr Nutzer: "+operatorCode.getIdno());
			LoggerUtil.LOG.error("Fehler beim Abruf der Nutzerdaten durch den Editor");
		}
		probs.setDeliverytypereport(head.getBoolean("ckcredeliverytyper"));
		probs.setDeliverytypestatusreply(head.getBoolean("ckcredeliverytype"));
		probs.setDeliverytypestockdelivery(head.getBoolean("ckcdeliverytypedel"));
		probs.setDeliverytypesupplement(head.getBoolean("ckcredeliverytypes"));
		probs.setDeliverytypeupdate(head.getBoolean("ckcredeliverytypeu"));
		probs.setOpenorders(head.getBoolean("ckcreopenorders"));
		probs.setEntriesread(head.getBoolean("ckcreentriesread"));
		probs.setEntriesunread(head.getBoolean("ckcreentriesunread"));

		if (head.getCkcnumberofentries() > 0) {
			probs.setNumberofentries(head.getCkcnumberofentries());
		}
		if (head.getCkcreferencenumber() != null && !head.getCkcreferencenumber().equals("")) {
			probs.setReferencenumber(Long.getLong(head.getCkcreferencenumber()));
		}
		if (head.getCkcrecrefoid() != null && !head.getCkcrecrefoid().equals("")) {
			probs.setIdentificationnumber(head.getCkcrecrefoid());
		}
		if (head.getCkcrepagereference() > 0) {
			probs.setPagereference((long) head.getCkcrepagereference());
		}
		if (head.getCkcrecallstart() != null && head.getCkcrecallperiodend() != null
				&& !head.getCkcrecallstart().equals("") && !head.getCkcrecallperiodend().equals("")) try {
			gregor.setTime(head.getCkcrecallstart().toDate());
			period.setDatestart((DatatypeFactory.newInstance().newXMLGregorianCalendar(gregor)));
			gregor.setTime(head.getCkcrecallperiodend().toDate());
			period.setDateend((DatatypeFactory.newInstance().newXMLGregorianCalendar(gregor)));
			probs.setCallperiod(period);
		} catch (DatatypeConfigurationException e) {
			FO.box("Datumskonvertierung Fehlgeschlagen pr\u00fcfen sie das Datum!");
			LoggerUtil.LOG.error("Datumskonvertierung Fehlgeschlagen pr\u00fcfen sie das Datum!",e);
		}
		if (head.getCkcrecreationstart() != null && head.getCkcrecreationend() != null
				&& !head.getCkcrecreationend().equals("") && !head.getCkcrecreationstart().equals("")) {
			try {
				gregor.setTime(head.getCkcrecreationstart().toDate());
				period.setDatestart((DatatypeFactory.newInstance().newXMLGregorianCalendar(gregor)));
				gregor.setTime(head.getCkcrecreationend().toDate());
				period.setDateend((DatatypeFactory.newInstance().newXMLGregorianCalendar(gregor)));
				probs.setCreationperiod(period);
			} catch (DatatypeConfigurationException e) {
				FO.box("Fehler","Datumskonvertierung Fehlgeschlagen pr\u00fcfen sie das Datum!");
				LoggerUtil.LOG.error("Datumskonvertierung Fehlgeschlagen pr\u00fcfen sie das Datum!",e);
			}
		}
		if (head.getCkcreorderperioden() != null && head.getCkcreorderstart() != null
				&& head.getCkcreorderperioden().equals("") && head.getCkcreorderstart().equals("")) {
			try {
				gregor.setTime(head.getCkcreorderstart().toDate());
				period.setDatestart((DatatypeFactory.newInstance().newXMLGregorianCalendar(gregor)));
				gregor.setTime(head.getCkcreorderperioden().toDate());
				period.setDateend((DatatypeFactory.newInstance().newXMLGregorianCalendar(gregor)));
				probs.setOrderperiod(period);
			} catch (DatatypeConfigurationException e) {
				FO.box("Fehler","Datumskonvertierung Fehlgeschlagen pr\u00fcfen sie das Datum!");
				LoggerUtil.LOG.error("Datumskonvertierung Fehlgeschlagen pr\u00fcfen sie das Datum!",e);
			}
		}


		try {
			logonrequest = new Logonrequest().buildupLogin(editor.getYckcregenpwd(), editor.getYckcrepw(), editor.getYckcrelang(), editor.getYckcreuser(), Integer.parseInt(editor.getYckcrekeylist()));
			mailbox = (MailboxdirectoryResponse) Services.Mailboxdirectory.start(probs, logonrequest);
		} catch (Exception e) {
			FO.box("Fehler",e.getMessage());
			LoggerUtil.LOG.error("Fehler im Logonrequest",e);
		}finally {
			if(editor!=null||editor.active()) {
				editor.abort();
				editor=null;
			}
		}


		if (mailbox != null) {
			head.table().clear();
			try {
				for (Entry entry : mailbox.getBody().getEntry()) {
					row = head.table().appendRow();
					if (entry.getMailboxentrynumber() != null)
						row.setCremailentry(entry.getMailboxentrynumber());
					if (entry.getReferencenumber() != null) {
						row.setCrereferencemail(entry.getReferencenumber().toString());
					}
					if (entry.getDeliverytype() != null && entry.getDeliverytype().getDesignation() != null) {
						row.setCkcretdeliverytype(entry.getDeliverytype().getDesignation());
					}
					if (entry.getProducttype() != null && entry.getProducttype().getDesignation() != null) {
						row.setCkcreproducttype(entry.getProducttype().getDesignation());

					}
					if (entry.getStatusoforder() != null && entry.getStatusoforder().getDesignation() != null) {
						row.setCkcrestatusoforder(entry.getStatusoforder().getDesignation());
					}
					if (entry.getTermofsettlement() != null && entry.getTermofsettlement().getDesignation() != null) {
						row.setCkcrettermofsettl(entry.getTermofsettlement().getDesignation());
					}
					if (entry.getEasynumber() != null) {
						row.setCkcreeasynumber(entry.getEasynumber());
					}
					if (entry.getOrdertime() != null) {
						row.setCkcreordertime(entry.getOrdertime().toString());
					}
					if (entry.getCreationtime() != null) {
						row.setCkcrecreationtime(entry.getCreationtime().toString());
					}
					if (entry.getCalltime() != null) {
						row.setCkcrecalltime(entry.getCalltime().toXMLFormat());
					}
					if (entry.getIdentificationnumber() != null) {
						row.setString("Ckcreid", entry.getIdentificationnumber());
					}
					if (entry.getOrderaddresscompany() != null) {
						if (entry.getOrderaddresscompany().getCompanyname() != null) {
							row.setCkcrecompanyname(entry.getOrderaddresscompany().getCompanyname());
						}
						if (entry.getOrderaddresscompany().getCity() != null) {
							row.setCkcrecity(entry.getOrderaddresscompany().getCity());
						}
						if (entry.getOrderaddresscompany().getCommercialname() != null) {
							row.setCkcrecompanyname(entry.getOrderaddresscompany().getCommercialname());
						}
						if (entry.getOrderaddresscompany().getQuarter() != null) {
							row.setCkcrequater(entry.getOrderaddresscompany().getQuarter());
						}
						if (entry.getOrderaddresscompany().getRegion() != null) {
							row.setCkcreregion(entry.getOrderaddresscompany().getRegion());
						}
						if (entry.getOrderaddresscompany().getTradename() != null) {
							row.setCkcretraden(entry.getOrderaddresscompany().getTradename());
						}
						if (entry.getOrderaddresscompany().getCountry() != null) {
							row.setCkrecountry(entry.getOrderaddresscompany().getCountry().getDesignation());
						}
						if (entry.getOrderaddresscompany().getSalutation() != null) {
							row.setCkcresalut(entry.getOrderaddresscompany().getSalutation().getDesignation());
						}
						if (entry.getOrderaddresscompany().getPostcode() != null) {
							row.setCkcrepostcode(entry.getOrderaddresscompany().getPostcode());
						}
						if (entry.getOrderaddresscompany().getHousenumber() != null) {
							row.setCkcrehousenumber(entry.getOrderaddresscompany().getHousenumber().toString());
						}
						if (entry.getOrderaddresscompany().getHousenumberaffix() != null) {
							row.setCkcrehousenumber(row.getCkcrehousenumber() + " "
									+ entry.getOrderaddresscompany().getHousenumberaffix());
						}
						if (entry.getOrderaddresscompany().getStreet() != null) {
							row.setCkcrestreet(entry.getOrderaddresscompany().getStreet());
						}

					}
					if (entry.getOrderaddressprivate() != null) {
						if (entry.getOrderaddressprivate().getBirthname() != null) {
							row.setString("Ckcrebirthname", entry.getOrderaddressprivate().getAlias());
						}
						if (entry.getOrderaddressprivate().getCity() != null) {
							row.setCkcrecity(entry.getOrderaddressprivate().getCity());
						}
						if (entry.getOrderaddressprivate().getCountry() != null) {
							row.setCkrecountry(entry.getOrderaddressprivate().getCountry().getDesignation());
						}
						if (entry.getOrderaddressprivate().getAlias() != null) {
							row.setString("Ckcrealias", entry.getOrderaddressprivate().getAlias());
						}
						if (entry.getOrderaddressprivate().getNameaffix() != null) {
							row.setString("Ckcrenameaffix", entry.getOrderaddressprivate().getNameaffix());
						}
						if (entry.getOrderaddressprivate().getSurnamebeforedivorce() != null) {
							row.setString("Ckcresurnamediv", entry.getOrderaddressprivate().getSurnamebeforedivorce());
						}
						if (entry.getOrderaddressprivate().getSurnamewidow() != null) {
							row.setString("Ckcresurnamewid", entry.getOrderaddressprivate().getSurnamewidow());
						}
						if (entry.getOrderaddressprivate().getTitle() != null) {
							row.setString("Ckcretitle", entry.getOrderaddressprivate().getTitle().getDesignation());
						}
						if (entry.getOrderaddressprivate().getSalutation() != null) {
							row.setCkcresalut(entry.getOrderaddressprivate().getSalutation().getDesignation());
						}
					}
				}
				if(head.getTableRows().size()<1){
					FO.box("Meldung","Es liegen zu den Angaben keine Mails vor.");
				}
			} catch (Exception e) {
				FO.box("Fehler",e.getMessage());
				LoggerUtil.LOG.error("Fehler im Mailboxrequest",e);
			}
		} else {
			FO.box("Meldung","Tabelle kann nicht erstellt werden.\n Pr\u00fcfen sie ihre Angaben und versuchen sie es erneut.");
		}
	}
}
