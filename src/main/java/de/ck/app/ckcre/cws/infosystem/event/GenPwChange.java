package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.gui.MenuBuilder;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody;

/**
 * @author christian.schwabe
 * @do This Programm is for changing the Gen Password in the Passworddataset
 */
public class GenPwChange {
    public void GenPwChanging(CKCRELOGIN head, DbContext ctx, ScreenControl screenControl){
        Tkeylistresponsebody key = null;
        MenuBuilder<String> menue = new MenuBuilder<String>(ctx, "\u00fcbernehmen des ge\u00e4nderten Gen. Passwortes");
        menue.addItem("Y","\u00fcbernehme erfolgt auf eigene Gefahr, es wird ein Login versucht!\nEs erfolgt keine Syncronisation mit dem Server der Creditreform.");
        menue.addItem("N","Nicht \u00fcbernehmen");
        String temp=menue.show();
        if(temp!=null)
            return;
        if(temp.matches("Y")){
            head.setCkcregenpw(head.getCkcregenpwneu());
            Ckcrelogin login = new Ckcrelogin();
            key = login.Login(ctx, head);
        }else{
            menue = new MenuBuilder<String>(ctx,"Zur\u00fccksetzen des Passwortes?");
            menue.addItem("Y","Zur\u00fccksetzen");
            menue.addItem("N","Nicht zur\u00fccksetzen");
            temp =menue.show();
            if(temp!=null)
                return;
            if(temp.matches("Y")){
                LoggerUtil.LOG.info("Logindaten ge\u00e4ndert neues Genpasswort.");
                new SetLoginData().loginData(head,ctx,screenControl);
            }else{
                FO.box("Keine \u00e4nderungen durchgef\u00fchrt!");
            }
        }

    }
}
