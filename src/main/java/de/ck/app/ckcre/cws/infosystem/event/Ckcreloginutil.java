/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */
package de.ck.app.ckcre.cws.infosystem.event;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;

/**
 * @author christian.schwabe
 * @param: ctx is the Database Context witch the IS is loaded in
 * @do This Method is to Reset all Fields in Login to the Defaultvalue
 */
public class Ckcreloginutil {
    public void Clear(CKCRELOGIN head){
        head.setCkcrekeylist("");
//        head.setCkcrekeylistold("");
        head.setCkcrepasswd("");
        head.setCkcrepasswdneu("");
        head.setCkcrepasswdwh("");
        head.setCkcrelogin("");
        head.setCkcregenpw("");
        head.setCkcregenpwneu("");
        head.setCkcreblanceans(false);
        head.setCkcrecollectfs(false);
        head.setCkcrekeylists(false);
        head.setCkcrecollectf(false);
        head.setCkcremails(false);
        head.setCkcremonstatus(false);
        head.setCkcreprocess(false);
        head.setCkcreblanceanm(false);
        head.setCkcrebonimar(false);
        head.setCkcrecancelstmon(false);
        head.setCkcrechangem(false);
        head.setCkcrechangepw(false);
        head.setCkcrecollecta(false);
        head.setCkcrecollectd(false);
        head.setCkcrecollecto(false);
        head.setCkcreconsumerr(false);
        head.setCkcreforwardre(false);
        head.setCkcregeninfo(false);
        head.setCkcreidentify(false);
        head.setCkcreindentifyr(false);
        head.setCkcrecreditorfq(false);
        head.setCkcremailboxd(false);
        head.setCkcremailboxe(false);
        head.setCkcreorder(false);
        head.setCkcrechangemon(false);
        head.setCkcreupgradel(false);
        head.setCkcresearch(false);
        head.setCkcreproductav(false);
        head.setCkcrereport(false);
        head.setCkcrecreditorf(false);
        head.setCkcrepayment(false);
        head.setCkcreparticipatio(false);
        head.setCkcreparticidat(false);
        head.setCkcrerecivable(false);
        head.setCkcresrecivable(false);
        head.setCkcretrecivable(false);
        head.setCkcrelogon(false);
        head.setCkcredloginsp(false);
        head.table().clear();
    }
}
