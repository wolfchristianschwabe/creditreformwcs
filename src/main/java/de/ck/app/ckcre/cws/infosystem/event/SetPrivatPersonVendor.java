package de.ck.app.ckcre.cws.infosystem.event;
/**
 * @author Christian Schwabe Computerkomplett
 */

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;
import de.abas.erp.db.schema.vendor.Vendor;

public class SetPrivatPersonVendor {
    /**
     * @do Import of the Vendor into the IS filling up all Fields
     * @param vendor The Vendor that needs to be imported
     * @param head The Header of the IS where the Data comes from
     * @param ctx The Context in witch the IS is called up
     */


    public void PrivatPersonsetVendor(Vendor vendor, CKCREORDERS head, DbContext ctx) {
        String affix = "";
        String number ="";
        new CkcreordersUtil().clear(head);
        if(head instanceof CKCREORDERS){
            if(vendor.getAddr()!=null&&!vendor.getAddr().matches("")) {
                if(vendor.getStreet()!=null&&!vendor.getStreet().matches("")) {
                    if (vendor.getStreet().split("[\\d]").length>1)
                        affix = vendor.getStreet().split("[\\d]")[1];
                    number = vendor.getStreet().replaceAll("[\\D]","");
                }
                ((CKCREORDERS) head).setCkcresurname(vendor.getAddr().substring(0,vendor.getAddr().indexOf(',')));
                ((CKCREORDERS) head).setCkcrefirstname(vendor.getAddr().substring(vendor.getAddr().indexOf(',')).replace(",",""));
                ((CKCREORDERS) head).setCkcrecrefoid(vendor.getString("Yckcreident"));
                if(vendor.getTown()!=null&&!vendor.getTown().matches(""))
                    ((CKCREORDERS) head).setCkcrekcity(vendor.getTown());
                // if(vendor.getStateOfTaxOffice().getCtryCode2Char()!=null&&!vendor.getStateOfTaxOffice().getCtryCode2Char().matches(""))
                    // ((CKCREORDERS) head).setCkcrekcountry(vendor.getStateOfTaxOffice().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                    ((CKCREORDERS) head).setCkcrekhousnumber(number);
                if(affix!=null&&!affix.matches(""))
                    ((CKCREORDERS) head).setCkcrehousnumberaff(affix);
                if(vendor.getZipCode()!=null&&!vendor.getZipCode().matches(""))
                    ((CKCREORDERS) head).setCkcrekpostcode(vendor.getZipCode());
                if(vendor.getRegion()!=null&&!vendor.getRegion().toString().matches(""))
                    ((CKCREORDERS) head).setCkcrekregion(vendor.getRegion().getCode());
                if(vendor.getStreet()!=null&&!vendor.getStreet().matches(""))
                    ((CKCREORDERS) head).setCkcrekstreet(vendor.getStreet().split("[\\d]")[0]);
                return;
            }if(vendor.getAddr2()!=null&&!vendor.getAddr2().matches("")) {
                if (vendor.getStreet2().split("[\\d]").length>1)
                    affix = vendor.getStreet2().split("[\\d]")[1];
                number = vendor.getStreet2().replaceAll("[\\D]","");
                ((CKCREORDERS) head).setCkcresurname(vendor.getAddr2().substring(0,vendor.getAddr2().indexOf(',')));
                ((CKCREORDERS) head).setCkcrefirstname(vendor.getAddr2().substring(vendor.getAddr2().indexOf(',')).replace(",",""));
                ((CKCREORDERS) head).setCkcrecrefoid(vendor.getString("Yckcreident"));
                if(vendor.getTown2()!=null&&!vendor.getTown2().matches(""))
                    ((CKCREORDERS) head).setCkcrekcity(vendor.getTown2());
                // if(vendor.getStateOfTaxOffice2().getCtryCode2Char()!=null&&!vendor.getStateOfTaxOffice2().getCtryCode2Char().matches(""))
                //      ((CKCREORDERS) head).setCkcrekcountry(vendor.getStateOfTaxOffice2().getCtryCode2Char());
                if(number!=null&&!number.matches(""))
                    ((CKCREORDERS) head).setCkcrekhousnumber(number);
                if(affix!=null&&!affix.matches(""))
                    ((CKCREORDERS) head).setCkcrehousnumberaff(affix);
                if(vendor.getZipCode2()!=null&&!vendor.getZipCode2().matches(""))
                    ((CKCREORDERS) head).setCkcrekpostcode(vendor.getZipCode2());
                if(vendor.getRegion2().getCode()!=null&&!vendor.getRegion2().getCode().matches(""))
                    ((CKCREORDERS) head).setCkcrekregion(vendor.getRegion2().getCode());
                if(vendor.getStreet2()!=null&&!vendor.getStreet2().matches(""))
                    ((CKCREORDERS) head).setCkcrekstreet(vendor.getStreet2().split("[\\d]")[0]);
                return;
            }else {
                FO.box("Fehler","Der Lieferant ist nicht korrekt angelegt.");
            }

        }



    }
}
