package de.ck.app.ckcre.cws.infosystem.event;
/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.common.type.AbasDate;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.LogonResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tkeylistresponsebody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tservice;

import java.util.HashMap;

/**
 * @author christian.schwabe
 * @param: head is from the CKCRELOGIN IS for getting all Fields, ctx is the Context in with the IS is loaded
 * @do The Method is for importing the Settings made in the Login Is to the Passworddatabase if the Login was Correct
 */
public class Ckcrelogin {
    public Tkeylistresponsebody Login(DbContext ctx, CKCRELOGIN head) {
        Logonrequest logon = new Logonrequest();
        LogonResponse response = null;
        Tkeylistresponsebody keylistResponse = null;
        if(head.getCkcrelogin()==null||head.getCkcrelogin().matches("")){
            FO.box("Meldung","Loginname nicht angegeben.");
            return null;
        }
        if(head.getCkcrepasswd()==null||head.getCkcrepasswd().matches("")){
            FO.box("Meldung","Passwortfeld darf nicht leer sein.");
            return null;
        }
        if(head.getCkcregenpw()==null||head.getCkcregenpw().matches("")){
            FO.box("Meldung","Das Generelle Passwort darf nicht leer sein.");
            return null;
        }
        if(head.getCkcrekeylist().contains("[\\D]"))
        if(head.getCkcredefkeylist()){
            FO.box("Meldung","Login mit Standart Keylist wird durchgef\u00fchrt");
        }
        try {
            //AgPw01!
            response =  (LogonResponse) Services.Logon.start(null, logon.buildupLogin(head.getCkcregenpw(), head.getCkcrepasswd(), head.getCkcrelangchosen(), head.getCkcrelogin(), Integer.parseInt(head.getCkcrekeylist())));
            FO.box("OK","Login erfolgreich!");
            //ctx.out().println(((HashMap<String,Object>)new ReadandSetKeyOptions().getConstrains(ctx,response.getBody())).toString());
            HashMap<String,Object>constrainslang = new GetConstrain().GetServiceConstrain("DE","login",ctx,response.getBody(),null);
            HashMap<String,String>constrains = new GetConstrain().GetConstrains("DE","login",ctx,response.getBody(),null);

            for(Tservice service :response.getBody().getService()){
              new SetOperations().SetOpt(head,service.getOperation());

            }
            keylistResponse = (Tkeylistresponsebody) Services.Keylist.start(null, logon.buildupLogin(head.getCkcregenpw(), head.getCkcrepasswd(), head.getCkcrelangchosen(), head.getCkcrelogin(), Integer.parseInt(head.getCkcrekeylist())));
            head.setCkcrekeylist(response.getBody().getCurrentkeylistversion().toString());
            head.setCkcremails(response.getBody().isUnreadmail());
            if(response.getBody().getEmailaddress().size()>0) {
                head.setCkcreemail(response.getBody().getEmailaddress().get(0));
            }
            if(head.getCkcredloginsp()){
                final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
                PasswordEditor editor = operatorCode.createEditor();
                editor.open(EditorAction.UPDATE);
                try {
                    editor.setString("Yckcrepw",head.getCkcrepasswd());
                    editor.setString("Yckcreuser", head.getCkcrelogin());
                    editor.setString("Yckcregenpwd", head.getCkcregenpw());
                    editor.setString("Yckcreemail", head.getCkcreemail());
                    editor.setString("Yckcregeschaeft", head.getString("Ckcregeschaeft"));
                    editor.setString("Yckcrekeylist",head.getCkcrekeylist());
                    editor.setString("Yckcreloginlast", new AbasDate().toString());
                    editor.setString("Yckcrelang",head.getCkcrelangchosen());
                    editor.setBoolean("Yckcremailboxe",head.getCkcremailboxe());
                    editor.setBoolean("Yckcresearch",head.getCkcresearch());
                    editor.setBoolean("Yckcrechangepw",head.getCkcrechangepw());
                    editor.setBoolean("Yckcreproductav",head.getCkcreproductav());
                    editor.setBoolean("Yckcrereport",head.getCkcrereport());
                    editor.setBoolean("Yckcreorder",head.getCkcreorder());
                    editor.setBoolean("Yckcreidentify",head.getCkcreidentify());
                    editor.setBoolean("Yckcremailboxd",head.getCkcremailboxd());
                    editor.setBoolean("Yckcrechangem",head.getCkcrechangem());
                    editor.setBoolean("Yckcreparticipatio",head.getCkcreparticipatio());
                    editor.setBoolean("Yckcreparticidat",head.getCkcreparticidat());
                    editor.setBoolean("Yckcremonstatus",head.getCkcremonstatus());
                    editor.setBoolean("Yckcrelogon",head.getCkcrelogon());
                    editor.setBoolean("Yckcreblanceans",head.getCkcreblanceans());
                    editor.setBoolean("Yckcreblanceanm",head.getCkcreblanceanm());
                    editor.setBoolean("Yckcrecancelstmon",head.getCkcrecancelstmon());
                    editor.setBoolean("Yckcrechangemon",head.getCkcrechangemon());
                    editor.setBoolean("Yckcreupgradel",head.getCkcreupgradel());
                    editor.setBoolean("Yckcreindentifyr",head.getCkcreindentifyr());
                    editor.setBoolean("Yckcrecollecta",head.getCkcrecollecta());
                    editor.setBoolean("Yckcrecollectd",head.getCkcrecollectd());
                    editor.setBoolean("Yckcrecollectf",head.getCkcrecollectf());
                    editor.setBoolean("Yckcrecollecto",head.getCkcrecollecto());
                    editor.setBoolean("Yckcrecreditorf",head.getCkcrecreditorf());
                    editor.setBoolean("Yckcrecreditorfq",head.getCkcrecreditorfq());
                    editor.setBoolean("Yckcreforwardre",head.getCkcreforwardre());
                    editor.setBoolean("Yckcregeninfo",head.getCkcregeninfo());
                    editor.setBoolean("Yckcrepayment",head.getCkcrepayment());
                    editor.setBoolean("Yckcreprocess",head.getCkcreprocess());
                    editor.setBoolean("Yckcrerecivable",head.getCkcrerecivable());
                    editor.setBoolean("Yckcresrecivable",head.getCkcresrecivable());
                    editor.setBoolean("Yckcrecollectfs",head.getCkcrecollectfs());
                    editor.setBoolean("Yckcreconsumerr",head.getCkcreconsumerr());
                    editor.setBoolean("Yckcrebonimar",head.getCkcrebonimar());
                    editor.commit();

                }catch(Exception e){
                    FO.box("Fehler",e.getMessage());
                    LoggerUtil.LOG.error("Fehler Editor kann Daten nicht schreiben",e);
                }finally{
                    if(editor != null&&editor.active()){
                        editor.abort();
                        editor = null;
                    }
                }

            }
    }catch (Exception e){
            FO.box("Fehler",e.toString());
            LoggerUtil.LOG.error("Fehler",e);
        }
        return keylistResponse;
    }
}
