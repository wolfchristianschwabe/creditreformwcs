/**
 * @author Christian Schwabe Computerkomplett
 * @version 0.0.0.1
 */
package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.erp.db.infosystem.custom.owcrefo.CKCREORDERS;

/**
 * @author christian.schwabe
 * @param: ctx is the Database Context witch the IS is loaded in
 * @do This Method is f\u00fcr resetting all Fieldvalues to teh Default Value
 */
public class CkcreordersUtil {
	public void clear(CKCREORDERS head) {
		if(head.getString("Ckcrelief").matches("")) {
			head.setString("Ckcrelief", "");
			head.setCkcrelegalform("");
			head.setCkcrelegitimateint("");
			head.setCkcreordertype("");
			head.setCkcreprodtype("");
			head.setCkcrereportlang("");
		}
		if(head.getString("Ckcrekunde").matches("")) {
			head.setString("Ckcrekunde", "");
			head.setCkcrelegalform("");
			head.setCkcrelegitimateint("");
			head.setCkcreordertype("");
			head.setCkcreprodtype("");
			head.setCkcrereportlang("");
		}
		head.setCkcreopenorders(false);
		head.setCkcredeliverytype(false);
		head.setCkcredeliverytyper(false);
		head.setCkcredeliverytypes(false);
		head.setCkcredeliverytypeu(false);
		head.setCkcreentriesunread(false);
		head.setCkcreentriesread(false);
		head.setCkcdeliverytypedel(false);
		head.setCkcresurname("");
		head.setCkcrecallperiodend(null);
		head.setCkcrecallstart(null);
		head.setCkcrecompany("");
		head.setCkcrecreationend(null);
		head.setCkcrecreationstart(null);
		head.setCkcrecreditamount("");
		head.setCkcrecreditcurren("");
		head.setCkcrecrefoid("");
		head.setCkcredateofbirth("");
		head.setCkcredateofbirth("");
		head.setCkcreextmon(false);
		head.setCkcreextmondate(null);
		head.setCkcreferencenumber("");
		head.setCkcrefirstname("");
		head.setCkcrehousnumberaff("");
		head.setCkcrekcity("");
		head.setCkcrekcountry("");
		head.setCkcrekhousnumber("");
		head.setCkcrekpostcode("");
		head.setCkcrekregion("");
		head.setCkcrekstreet("");
		head.setCkcrekundenref("");
		//head.setCkcrelegalform("");
		//head.setCkcrelegitimateint("");
		head.setCkcremailboxentryn("");
		head.setCkcreorderperioden(null);
		//head.setCkcreordertype("");
		head.setCkcreorderspecific("");
		head.setCkcreorderstart(null);
		head.setCkcrepagereference(0);
		//head.setCkcreprodtype("");
		//head.setCkcrereportlang("");
		head.table().clear();

	}
}
