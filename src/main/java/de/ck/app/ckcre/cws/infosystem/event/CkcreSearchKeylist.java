package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.gui.MenuBuilder;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.axi2.event.ButtonEvent;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRESEARCH;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.abas.connection.Logonrequest;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.LogonResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.util.HashMap;
/**
 * @author christian.schwabe
 * @do This Class reads the Hashmap of informations generated in GetConstrain and give back a Menue for better Use.
 */
public class CkcreSearchKeylist {
    public void Keylistreturn(CKCRESEARCH head, DbContext ctx, ButtonEvent event, HashMap<String,Object> keylist,HashMap<String,String> keylist2) {
        Logonrequest logon = new Logonrequest();
        Trequestheader login = null;
        String temp="";
        int count=0;
        LogonResponse response = null;
        PasswordEditor editor = null;
        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        MenuBuilder<String> menue = new MenuBuilder<String>(ctx, "Ausw\u00e4hlen");
        try {
           editor = operatorCode.createEditor();
           editor.open(EditorAction.VIEW);
        }catch(Exception e){
            FO.box("Fehler","Abruf der Daten gescheitert"+e.getMessage());
            LoggerUtil.LOG.error("Fehler beim Abruf der Nutzerdaten durch den Editor",e);
        }
        if (event.getFieldName().matches("ckcrelang")) {
            try {
                menue = new MenuBuilder<String>(ctx, "Ausw\u00e4hlen");
                menue.addItem("","");
                for (String key : keylist.keySet()) {
                    menue.addItem(key, keylist2.get(key));
                }
                temp = menue.show();
                if (temp == null)
                    return;
                head.setCkcrecountry(temp.toUpperCase());
            } catch (Exception e) {
                FO.box("Fehler","Abruf der Daten gescheitert"+e.getMessage());
                LoggerUtil.LOG.error("Fehler Keylist kann nicht gelesen werden", e);
            } finally {
                if (editor != null || editor.active()) {
                    editor.abort();
                    editor = null;
                }
            }
        }
        if (event.getFieldName().matches("ckcresuchtyp")) {
                    try {
                        menue.addItem("","");
                        for (String key : ((HashMap<String,String>)keylist.get(editor.getYckcrelang())).keySet()) {
                            if (key.contains("SETY"))
                                menue.addItem(key,((HashMap<String,String>)keylist.get(editor.getYckcrelang())).get(key));
                        }
                        temp = menue.show();
                        if(temp==null){
                            return;
                        } else {
                            head.setString("ckcresearchtype",temp);
                        }
                    } catch (Exception e) {
                        FO.box("Fehler",e.getMessage());
                        LoggerUtil.LOG.error("Fehler Keylist kann nicht gelesen werden", e);
                    } finally {
                        if (editor != null || editor.active()) {
                            editor.abort();
                            editor = null;
                        }
                    }
                }
        if (event.getFieldName().matches("ckcrelegalformbn")) {
                try {
                    menue.addItem("","");
                    for (String key : ((HashMap<String, String>) keylist.get(editor.getYckcrelang())).keySet()) {
                        if (key.contains("LEFO"))
                            menue.addItem(key, ((HashMap<String, String>) keylist.get(editor.getYckcrelang())).get(key));
                    }
                    temp = menue.show();
                    if (temp == null) {
                        return;
                    } else {
                        head.setCkcrelegalform(temp);
                    }
                } catch (Exception e) {
                    FO.box("Fehler", "Kann Gesch\u00e4ftsform nicht auslesen");
                    LoggerUtil.LOG.error("Keylist nicht geladen");
                } finally {
                    if (editor != null || editor.active()) {
                        editor.abort();
                        editor = null;
                    }
                }
            }//RETY
        if (event.getFieldName().matches("ckcreregtype")) {
                try {
                    menue.addItem("","");
                    for (String key : ((HashMap<String, String>) keylist.get(editor.getYckcrelang())).keySet()) {
                        if (key.contains("RETY"))
                            menue.addItem(key, ((HashMap<String, String>) keylist.get(editor.getYckcrelang())).get(key));
                    }
                    temp = menue.show();
                    if (temp == null) {
                        return;
                    } else {
                        head.setCkcreregistertype(temp);
                    }

            } catch (Exception e) {
                FO.box("Fehler",e.getMessage());
                LoggerUtil.LOG.error("Editor kann daten nicht laden",e);
            } finally {
                if (editor != null || editor.active()) {
                    editor.abort();
                    editor = null;
                }
            }
        }
            }
}
