package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.ck.abas.connection.Logonrequest;
import de.ck.abas.connection.Services;
import de.ck.app.ckcre.cws.util.LoggerUtil;
import https.onlineservice_creditreform_de.webservice._0600_0021.ChangeemailResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tchangeemailrequestbody;

public class Mailchange {
    public void Mailchanger( DbContext ctx, CKCRELOGIN head){
        if((head.getCkcrelangchosen()!=null||!head.getCkcrelangchosen().matches("")||head.getCkcrelangchosen()!="")&&
          (head.getCkcregenpw()!=null||!head.getCkcregenpw().matches("")||head.getCkcregenpw()!="")&&
          (head.getCkcrepasswd()!=null||!head.getCkcrepasswd().matches("")||head.getCkcrepasswd()!="")&&
          (head.getCkcrelogin()!=null||!head.getCkcrelogin().matches("")||head.getCkcrelogin()!="")&&
          (head.getCkcrekeylist()!=null||!head.getCkcrekeylist().matches("")||head.getCkcrekeylist()!="")&&
          (head.getCkcreemail()!=null||!head.getCkcreemail().matches("")||head.getCkcreemail()!="")){
        ChangeemailResponse response = null ;
        Tchangeemailrequestbody body = new Tchangeemailrequestbody();
        Logonrequest logon = new Logonrequest();
        try {
            body.getEmail().add(head.getCkcreemail());
            response = (ChangeemailResponse) Services.Changeemail.start(body , logon.buildupLogin(head.getCkcregenpw(), head.getCkcrepasswd(), head.getCkcrelangchosen(), head.getCkcrelogin(), Integer.parseInt(head.getCkcrekeylist())));
        }catch(Exception e){
            FO.box("Fehler",e.getMessage());
            LoggerUtil.LOG.error("Fehler bei Changemail anfrage",e);
        }
    }else{
            FO.box("Fehler","Bitte geben sie alle Logindaten korrekt an. Bitte leere Felder ausf\u00fcllen.");
        }
    }
}
