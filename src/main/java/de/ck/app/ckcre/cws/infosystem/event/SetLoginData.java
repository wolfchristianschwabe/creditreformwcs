package de.ck.app.ckcre.cws.infosystem.event;

/**
 * @author christian.schwabe
 * @version 0.0.0.1
 *
 */

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.api.session.OperatorInformation;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.common.type.AbasDate;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRELOGIN;
import de.abas.erp.db.schema.company.Password;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.ck.app.ckcre.cws.util.LoggerUtil;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author christian.schwabe
 * @param: The Parameter that are Used Here are the Head of the IS Login and the DB Context witch the IS is loaded in.
 * @do This is for the Setting of the Informations in Login IS of Crefo
 */
public class SetLoginData {
    public void loginData(CKCRELOGIN head, DbContext ctx, ScreenControl screenControl){

        final Password operatorCode = new OperatorInformation(ctx).getPwdRecord();
        PasswordEditor editor=null;

        try {
            editor = operatorCode.createEditor();
            editor.open(EditorAction.VIEW);
        }catch(Exception e){
            FO.box("Fehler","Fehler beim Abruf der Daten f\u00fcr Nutzer "+operatorCode.getIdno());
            LoggerUtil.LOG.error("Fehler beim Auslesen durch den Editor",e);
        }
        try {
            if (editor != null) {
                if(editor.getYckcrepw()!=null)
                    head.setCkcrepasswd(editor.getYckcrepw());
                if(editor.getYckcreuser()!=null)
                    head.setCkcrelogin(editor.getYckcreuser());
                if(editor.getYckcregenpwd()!=null)
                    head.setCkcregenpw(editor.getYckcregenpwd());
                if(editor.getYckcreemail()!=null)
                    head.setCkcreemail(editor.getYckcreemail());
                 AbasDate ablaufdatum =  AbasDate.valueOf(LocalDateTime.now().minusDays(42).format(DateTimeFormatter.ofPattern("yyyyMMdd")));
                // ctx.out().println(ablaufdatum.compareTo(editor.getYckcreloginlast())+ablaufdatum.toString());
                 if (editor.getYckcreloginlast()!=null&&ablaufdatum.compareTo(editor.getYckcreloginlast())>1) {
                     //.formel yckcreloginlast = "01.01.2019"
                    FO.box("Fehler","Passwort muss ge\u00e4ndert werden!");
                    screenControl.moveCursor(head,"ckcrepasswdneu");
                    return;
                }
                if(editor.getYckcrekeylist()!=null&&!editor.getYckcrekeylist().matches("")){
                    head.setCkcredefkeylist(false);
                    head.setCkcrekeylist(editor.getYckcrekeylist());
                }
                if(editor.getYckcrekeylist()==null||editor.getYckcrekeylist().matches("")){
                    head.setCkcredefkeylist(true);
                    head.setCkcrekeylist("21");
                }
                if (editor.getYckcrelang() != null && !editor.getYckcrelang().matches("")) {
                    head.setString("Ckcrelangchosen", editor.getYckcrelang());
                } else {
                    head.setString("Ckcrelangchosen", editor.getOperLanguageISO().getDisplayString().toUpperCase());
                  }
                head.setCkcremailboxe(editor.getYckcremailboxe());
                head.setCkcresearch(editor.getYckcresearch());
                head.setCkcrechangepw(editor.getYckcrechangepw());
                head.setCkcreproductav(editor.getYckcreproductav());
                head.setCkcrereport(editor.getYckcrereport());
                head.setCkcreorder(editor.getYckcreorder());
                head.setCkcreidentify(editor.getYckcreidentify());
                head.setCkcremailboxd(editor.getYckcremailboxd());
                head.setCkcrechangem(editor.getYckcrechangem());
                head.setCkcreparticipatio(editor.getYckcreparticipatio());
                head.setCkcreparticidat(editor.getYckcreparticidat());
                head.setCkcremonstatus(editor.getYckcremonstatus());
                head.setCkcrelogon(editor.getYckcrelogon());
                head.setCkcreblanceans( editor.getYckcreblanceans());
                head.setCkcreblanceanm(editor.getYckcreblanceanm());
                head.setCkcrecancelstmon( editor.getYckcrecancelstmon());
                head.setCkcrechangemon( editor.getYckcrechangemon());
                head.setCkcreupgradel(editor.getYckcreupgradel());
                head.setCkcreindentifyr( editor.getYckcreindentifyr());
                head.setCkcrecollecta(editor.getYckcrecollecta());
                head.setCkcrecollectd( editor.getYckcrecollectd());
                head.setCkcrecollectf( editor.getYckcrecollectf());
                head.setCkcrecollecto(editor.getYckcrecollecto());
                head.setCkcrecreditorf(editor.getYckcrecreditorf());
                head.setCkcrecreditorfq( editor.getYckcrecreditorfq());
                head.setCkcreforwardre(editor.getYckcreforwardre());
                head.setCkcregeninfo( editor.getYckcregeninfo());
                head.setCkcrepayment(editor.getYckcrepayment());
                head.setCkcreprocess(editor.getYckcreprocess());
                head.setCkcrerecivable(editor.getYckcrerecivable());
                head.setCkcresrecivable(editor.getYckcresrecivable());
                head.setCkcrecollectfs(editor.getYckcrecollectfs());
                head.setCkcreconsumerr(editor.getYckcreconsumerr());
                head.setCkcrebonimar(editor.getYckcrebonimar());
                editor.commit();
            }
        }catch(Exception e){
            FO.box("Fehler",e.getMessage());
            LoggerUtil.LOG.error("Fehler beim schreiben durch den Editor",e);
        }finally {
            if (editor != null&&editor.active() ) {
                editor.abort();
                editor = null;
            }
        }
    }
}
