package de.ck.app.ckcre.cws.infosystem.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.EditorAction;
import de.abas.erp.db.SelectableObject;
import de.abas.erp.db.infosystem.custom.owcrefo.CKCRESEARCH;
import de.abas.erp.db.schema.vendor.Vendor;
import de.abas.erp.db.schema.vendor.VendorEditor;
import de.abas.erp.db.selection.Conditions;
import de.abas.erp.db.selection.SelectionBuilder;
import de.abas.erp.db.util.QueryUtil;
import de.ck.app.ckcre.cws.util.LoggerUtil;

/**
 * @author christian.schwabe
 * @do This Class updates the Vendor Data in the Abas Object Vendor.
 */
public class CkcreSetAbasVendor {
    public void SetVendor(CKCRESEARCH head, CKCRESEARCH.Row row, DbContext ctx) {
        SelectableObject object =  head.getCkcrelief().getId();
        SelectionBuilder<Vendor> selectionBuilder = SelectionBuilder.create(Vendor.class);
        selectionBuilder.add(Conditions.eq(Vendor.META.id, object.id()));
        Vendor vendor = QueryUtil.getFirst(ctx,selectionBuilder.build());
        //ctx.out().println("ID:" + vendor.getId().toString());
        VendorEditor editor = null;
        if( head.getCkcrekundea()!=null &&  !head.getCkcrekundea().toString().matches("")) {
            //editor = null;
        }
        try {
            editor = vendor.createEditor();
            editor.open(EditorAction.UPDATE);
            editor.setYckcrecrefonum(row.getCkcreid().substring(3));
            editor.setYckcreident(row.getCkcreid());
            editor.setStreet(row.getCkcretstreet() + " " + row.getString("Ckcrehousnumber") + " " + row.getString("Ckcrehousnumberaf"));
            if(row.getCkcrename()!=null&&!row.getCkcrename().matches("")) {
                editor.setAddr(row.getCkcrename());
            }
            if(row.getString("Ckcretpostcode")!=null&&!row.getString("Ckcretpostcode").matches(""))
            editor.setZipCode(row.getString("Ckcretpostcode"));
            if(row.getString("Ckcretort")!=null&&!row.getString("Ckcretort").matches(""))
            editor.setTown(row.getString("Ckcretort"));
          //  editor.setString("stateOfTaxOffice",row.getCkcretland());
            editor.commit();
            FO.box("Erfolg","Kundendaten Aktualisiert.");
        }catch (Exception e){
            FO.box("Fehler",e.getMessage());
            LoggerUtil.LOG.error("Editor konnte Daten nicht laden",e);
        }finally{
            if(editor!=null&&editor.active()){
                editor.abort();
                editor = null;
            }else{
                editor =null;
            }
        }

    }
}
