package de.ck.app.ckcre.cws.screen.event;

import de.abas.eks.jfop.remote.FO;
import de.abas.erp.axi.event.EventException;
import de.abas.erp.axi.screen.ScreenControl;
import de.abas.erp.axi2.EventHandlerRunner;
import de.abas.erp.axi2.annotation.EventHandler;
import de.abas.erp.axi2.annotation.FieldEventHandler;
import de.abas.erp.axi2.event.FieldEvent;
import de.abas.erp.axi2.type.FieldEventType;
import de.abas.erp.db.DbContext;
import de.abas.erp.db.schema.company.PasswordEditor;
import de.abas.erp.jfop.rt.api.annotation.RunFopWith;


@EventHandler(head = PasswordEditor.class)

@RunFopWith(EventHandlerRunner.class)

public class CKCREADMIN {
    @FieldEventHandler(field="yckcreadmin", type = FieldEventType.EXIT)
    public void yckcreadminExit(FieldEvent event, ScreenControl screenControl, DbContext ctx, PasswordEditor head) throws EventException {
        if(!head.getYckcrebrecht()){
            FO.fehler("Bitte erst das Loginrecht erteilen!");
            head.setYckcreadmin(!head.getYckcreadmin());
        }else{}
    }
}
