/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.MailboxdirectoryRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.MailboxdirectoryResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category this is for the Service Mailboxdirectory to get this back and Declare the
 *           return as a Hashmap.
 * @version 0.0.0.2
 */
public class Mailboxdirectory extends AbstractService {

	protected MailboxdirectoryResponse initMailboxdirectory( Tmailboxdirectoryrequestbody request, Trequestheader logon) throws Exception {
		return mailboxDirectoryService(request,logon);
	}
	/**
	 * 
	 * @param logon 
	 * @param body 
	 * @return This Method returns an Object of the Type MailbocdirectoryResponse
	 * @throws Exception this Exception is Thrown if there is a Service Fault or the Service is not reachabele 
	 */
	private MailboxdirectoryResponse mailboxDirectoryService(Tmailboxdirectoryrequestbody body, Trequestheader logon) throws Exception {
		MailboxdirectoryRequest mail = new MailboxdirectoryRequest();
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		MailboxdirectoryResponse response = new MailboxdirectoryResponse();
		mail.setHeader(logon);
		mail.setBody(body);
		try {
			new XMLConverterforLogging().XMLConvert(mail,path);
			response = ctoMessagesSoap12.mailboxdirectory(mail);
			new XMLConverterforLogging().XMLConvert(response,path);
			return response;
		} catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
		} 
	}
}
