/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.SearchRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.SearchResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tsearchrequestbody;
import org.apache.log4j.chainsaw.Main;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category Main Service Construktor for the Search Service its builded for Error handling and Datamanagement if any given Type is not in the row for the Parameter List.
 * @see Main Service Construktor for all Services is AbstractService {@link: AbstractService}
 */
public class Search extends AbstractService{
/**
 * 
 * @throws Exception is Thrown if there is any Mandatory Variable Value missing.
 */
	protected SearchResponse initSearch(Trequestheader requestLogon, Tsearchrequestbody search) throws Exception {
		return searchService(requestLogon, search);
	}

	/**
	 * @category Service Connection
	 * @return This Method returns an Object with the Type Searchresponse
	 * @throws Exception this Exception is Thrown if there is a Service Fault or the Service is not reachabele 
	 */
	private SearchResponse searchService(Trequestheader requestLogon, Tsearchrequestbody searchBody) throws Exception {
		SearchRequest request = new SearchRequest();
		String path = "/owcrefo/log/"+ LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		request.setHeader(requestLogon);
		request.setBody(searchBody);
		SearchResponse response = null;
		new XMLConverterforLogging().XMLConvert(request,path);
		/*
		 * Try Catch Block for Connection Establishment
		 */
		try {
			response = ctoMessagesSoap12.search(request);
			new XMLConverterforLogging().XMLConvert(response,path);
		} catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
		}
		return response;
	}
}
