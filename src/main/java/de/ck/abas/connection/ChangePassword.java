/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 *
 */
public class ChangePassword extends AbstractService {
	protected ChangepasswordResponse changePassword(Trequestheader logon, Tchangepasswordrequestbody request) throws Exception {
		return passwordchange(logon,request);
	}
	/**
	 * @param body 
	 * @param logon 
	 * @return the Method retuns the response of the Webservice standard is null or the header as Response with the Responsecode
	 * @throws Exception this is thrown if the Service is not available or the Service is not Reachable and if the Service has an Error
	 */
	private ChangepasswordResponse passwordchange(Trequestheader logon, Tchangepasswordrequestbody body) 
			throws Exception {
		ChangepasswordResponse response = null;
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		ChangepasswordRequest changerequest = new ChangepasswordRequest();
		changerequest.setHeader(logon);
		changerequest.setBody(body);
		try {
			new XMLConverterforLogging().XMLConvert(changerequest,path);
			response = ctoMessagesSoap12.changepassword(changerequest);
			new XMLConverterforLogging().XMLConvert(response,path);
		} catch (Servicefault | Validationfault | ChangepasswordFault e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
		}
		return response;
	}
}
