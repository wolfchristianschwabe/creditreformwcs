/**
 * @author Christian Schwabe ComputerKomplett
 * This Class is the Service Base Class its meant to be an Enum for better Handling.
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.*;

/**
 * @author christian.schwabe
 * @version 0.0.2
 * @category Enum for Service attemps so you can start the Service from this
 *           Enum instead of handling it in a new Method.
 */
public enum Services {
	Search, CancelStandardMonitoring, BalanceAnalyseiesingleCompany, BalanceAnalysesismultiCompany, Bonimareport,
	Consumerreport, UpgradeList, Identificationnumbermap, Migrationdiff, ChangeMonitoringStatus, Mailboxentry,
	MonitoringStatus, Participationdatastart, Participationdata, Changeemail, Mailboxdirectory, ProductAvailability,
	Report, Logon, Order, Changepassword, Keylist, Identificationreport;

	Services() {
	}

	/**
	 * 
	 * @param params is a HashMap that is delivered by the Method getVarMap
	 * @return The Returnvalue is an Object of the Type of the Service it selfe
	 * @throws Exception This will be thrown if the Service is not found or the
	 *                   Service is not correct.
	 */
	public Object start(Object params, Object login) throws Exception{
		Object service = null;
		Trequestheader logon = (Trequestheader) login;
		Object request=null;
			 AbstractService servinit = new  AbstractService ();
			servinit.AbstarctServiceInit();


		switch (this) {
		case Search:
				service = new Search();
				return ((Search) service).initSearch(logon ,(Tsearchrequestbody) params);

		case ProductAvailability:
				service = new ProductAvailabilty();
				return ((ProductAvailabilty)service).productAvailabiltycheck(logon, (Tproductavailabilityrequestbody) params);

		case Order:
				service = new Order();
				return ((Order)service).initOrder(logon, (Torderrequestbody) params);

		case Identificationreport:
			service = new Identificationreport();
			return ((Identificationreport)service).initIdentificationreport(logon,(Tidentificationreportrequestbody) params);

		case Keylist:
			service = new Keylist();
			return ((Keylist)service).Keylistinput(logon);

		case Changepassword:
			service = new ChangePassword();
			return ((ChangePassword)service).changePassword(logon,(Tchangepasswordrequestbody) params);

		case Report:
			service = new Report();
			return ((Report)service).initRepot((Treportrequestbody) params,logon);

		case Logon:
			service =new Login();
			return ((Login)service).logoninput(logon);

		case Mailboxdirectory:
				service = new Mailboxdirectory();
				return ((Mailboxdirectory)service).initMailboxdirectory((Tmailboxdirectoryrequestbody) params,logon);

		case Mailboxentry:
				service = new Mailboxentry();
				return ((Mailboxentry)service).initMailboxEntry((Tmailboxentryrequestbody) params,logon);

		case Changeemail:
				service = new Mailboxchangeemail();
				return ((Mailboxchangeemail)service).initMailboxchangeemail((Tchangeemailrequestbody) params,logon);

		case Participationdatastart:
				service = new Participationdatastart();
				return ((Participationdatastart)service).initParticipationdatastart((Tparticipationdatastartrequestbody) params,logon);

		case Participationdata:
				service = new Participationdata();
				return ((Participationdata)service).initParticipationdata((Tparticipationdatarequestbody) params,logon);

		case MonitoringStatus:
				service = new MonitoringStatus();
				return ((MonitoringStatus)service).initMonitoringstatus((Tmonitoringstatusrequestbody) params,logon);

		case ChangeMonitoringStatus:
				service = new ChangeMonitoringStatus();
				return ((ChangeMonitoringStatus)service).initChangeMonitoringStatus((Tchangeextendedmonitoringrequestbody) params,logon);

		case CancelStandardMonitoring:
				service = new CancelStandardMonitoring();
				return ((CancelStandardMonitoring)service).initcancelStandardMonitoring((Tcancelstandardmonitoringrequestbody) params,logon);

//		case BalanceAnalyseiesingleCompany:
//			Bonimareport balance=new Bonimareport(params);
//			return balance.initBalanceanalysissinglecompany(); 
//		case BalanceAnalysesismultiCompany:
////			ChangeMonitoringStatus chmonstatus=new ChangeMonitoringStatus(params);
////			return chmonstatus.initChangeMonitoringStatus(); 
//		case Bonimareport:
//			Tbonimareportrequestbody request = (Tbonimareportrequestbody) params;
//			return ((Bonimareport) service).initBonimareport(request,logon);
			case BalanceAnalyseiesingleCompany:
				break;
			case BalanceAnalysesismultiCompany:
				break;
			case Bonimareport:
				break;
			case Consumerreport:
//			ChangeMonitoringStatus chmonstatus=new ChangeMonitoringStatus(params);
//			return chmonstatus.initChangeMonitoringStatus(); 
		case UpgradeList:
//			ChangeMonitoringStatus chmonstatus=new ChangeMonitoringStatus(params);
//			return chmonstatus.initChangeMonitoringStatus(); 
		case Identificationnumbermap:
//			ChangeMonitoringStatus chmonstatus=new ChangeMonitoringStatus(params);
//			return chmonstatus.initChangeMonitoringStatus(); 
		case Migrationdiff:
//			ChangeMonitoringStatus chmonstatus=new ChangeMonitoringStatus(params);
//			return chmonstatus.initChangeMonitoringStatus(); 
		default:
			break;

		}
		throw new Exception(
				"Service not found. Please try it again or call ComputerKomplett for further Informations.");
	}

}
