/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.BonimareportRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.BonimareportResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tbonimareportrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category this is for the Service to get the Orderdata back and Declare the
 *           return as a Hashmap.
 * @version 0.0.0.2
 */
public class Bonimareport extends AbstractService{
	protected BonimareportResponse initBonimareport(Tbonimareportrequestbody request, Trequestheader logon) throws Exception {
		return bonimareportService(request,logon);
	}

	/**
	 * 
	 * @param logon
	 * @return This Method returns an Object of the Type MonitoringstatusResponse
	 * @throws Exception this Exception is Thrown if there is a Service Fault or the
	 *                   Service is not reachabele
	 */
	private BonimareportResponse bonimareportService(Tbonimareportrequestbody request, Trequestheader logon) throws Exception {
		BonimareportResponse response = null;
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		BonimareportRequest bonimarequest = new BonimareportRequest();
		bonimarequest.setHeader(logon);
		bonimarequest.setBody(request);
		try {
			new XMLConverterforLogging().XMLConvert(bonimarequest,path);
			response = ctoMessagesSoap12.bonimareport(bonimarequest);
			new XMLConverterforLogging().XMLConvert(response,path);
			return response;
		} catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
		}
	}

}
