/**
 * 
 */
package de.ck.abas.connection;

import de.abas.erp.common.type.AbasDate;
import https.onlineservice_creditreform_de.webservice._0600_0021.ReportRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.ReportResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category this is for the Service to get the Reportdata back and Declare the
 *           return as a Hashmap.
 * @version 0.0.0.2
 */
public class Report extends AbstractService{

	protected ReportResponse initRepot(Treportrequestbody request, Trequestheader logon) throws Exception {
		return reportService(request, logon);
	}

	/**
	 * 
	 * @param logon
	 * @param body
	 * @return This Method returns an Object of the type ReportRespone
	 * @throws Exception this Exception is Thrown if there is a Service Fault or the
	 *                   Service is not reachabele
	 */

	private ReportResponse reportService(Treportrequestbody body, Trequestheader logon) throws Exception {
		ReportRequest reportRequest = new ReportRequest();
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		reportRequest.setHeader(logon);
		reportRequest.setBody(body);
		ReportResponse response =null;
		try {
			new XMLConverterforLogging().XMLConvert(reportRequest,path);
			response = ctoMessagesSoap12.report(reportRequest);
			new XMLConverterforLogging().XMLConvert(response,path);
			// --------------------------------------------------------------------------------------------------------------------------------------
			// Getting the InputStream from the Stream of Data that is represented in
			// Textreport. The DataSource is with MTOM Mechanisum convertabel.
			// --------------------------------------------------------------------------------------------------------------------------------------
			//InputStream inputStream = response.getBody().getReportdata().getTextreport().getInputStream();
			/**
			 * Delete if File exists This Code generate a File from the inputStream. The
			 * Datareader is for the XML Body.
			 */
			//File file = new File("C:/Users/christian.schwabe/Desktop/pdfscrefo/123.pdf");
			//if (file.exists()) {
			//	if (file.delete()) {

			//	}
			//}
			//FileUtils.copyInputStreamToFile(inputStream, file);
			
		} catch (Exception e) {
//			TODO
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			throw e;
//			new ErrorHandler().error(e);

		}
		return response;

	}
}
