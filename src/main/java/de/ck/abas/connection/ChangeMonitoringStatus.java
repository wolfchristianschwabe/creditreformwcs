/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.ChangeextendedmonitoringRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.ChangeextendedmonitoringResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tchangeextendedmonitoringrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category this is for the Service to get the Orderdata back and Declare the
 *           return as a Object of the Type ChangeextendedmonitoringResponse.
 * @version 0.0.0.2
 */
public class ChangeMonitoringStatus extends AbstractService {
	
	protected ChangeextendedmonitoringResponse initChangeMonitoringStatus(Tchangeextendedmonitoringrequestbody request, Trequestheader logon) throws Exception {
		return changeMonitoringStatusService(request,logon);
	}

	private ChangeextendedmonitoringResponse changeMonitoringStatusService(Tchangeextendedmonitoringrequestbody body, Trequestheader logon) throws Exception {
		ChangeextendedmonitoringResponse response = null;
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		ChangeextendedmonitoringRequest request = new ChangeextendedmonitoringRequest();
		request.setHeader(logon);
		request.setBody(body);
		try {
			new XMLConverterforLogging().XMLConvert(request,path);
			response = ctoMessagesSoap12.changeextendedmonitoring(request);
			new XMLConverterforLogging().XMLConvert(response,path);
			return response;
		} catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
		}
	}
}
