package de.ck.abas.connection;

import de.abas.erp.db.DbContext;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DataReader {
	private Method[] declaredMethods;
	private static HashMap<String, Object> datarecord = null;
	private static HashMap<String, Object> datarecord2 = null;
	private static String methodnamebefore;
	public DataReader() {
		if(datarecord==null) {
		datarecord = new HashMap<>();
		datarecord2 = new HashMap<>();
		}
	}

	public HashMap<String, Object> datarecordReader(Object responseobj, DbContext ctx)
			throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		if (responseobj == null) {
			datarecord.put(methodnamebefore, null);
		} else {
			declaredMethods = responseobj.getClass().getDeclaredMethods();
			for (Method method : declaredMethods) {
				if (method.getName().startsWith("get") && !method.getName().equals("getClazz")) {
					ctx.out().println(method.getReturnType().getName());
					switch (method.getReturnType().getName()) {
					case "java.math.BigDecimal":
						ctx.out().println(method.getName());
						if (method.invoke(responseobj, null) != null) {
							if (method.invoke(responseobj, null) instanceof BigDecimal) {
								BigDecimal tempbig = (BigDecimal) method.invoke(responseobj, null);
								ctx.out().println(tempbig.doubleValue());

                                datarecord.put(methodnamebefore + method.getName().replaceFirst("get", ""),
										tempbig.doubleValue());
							}
						}
						break;
					case "java.math.BigInteger":
						ctx.out().println(method.getName());
						if (method.invoke(responseobj, null) != null) {
							if (method.invoke(responseobj, null) instanceof BigInteger) {
								BigInteger tempbig = (BigInteger) method.invoke(responseobj, null);
								ctx.out().println(tempbig.intValue());
                                datarecord.put(methodnamebefore + method.getName(), tempbig.doubleValue());
							}
						}
						break;

					case "java.util.List":
					case "ArrayList":
						List<Object> tempList = (ArrayList<Object>) method.invoke(responseobj, null);
						if (tempList.size() > 0) {
							for (Object tempobj : tempList) {
								if (tempobj instanceof java.lang.String) {
//									System.err.println(method.getName());
//									System.out.println(tempobj.toString());
									continue;
								}
//								System.err.println(method.getName());
								datarecordReader(tempobj, ctx);
								continue;
							}
						}

						break;
					case "javax.xml.datatype.XMLGregorianCalendar":
						ctx.out().println(method.getName());
						if (method.invoke(responseobj, null) != null) {
							ctx.out().println(method.invoke(responseobj, null).toString());
                            datarecord.put(method.getName().replaceFirst("get", ""),
									method.invoke(responseobj, null).toString());

						} else {
                            datarecord.put(methodnamebefore + method.getName().replaceFirst("get", ""),
									null);
						}
						break;
					case "java.lang.String":
						ctx.out().println(method.getName());
						if (method.invoke(responseobj, null) != null) {
							ctx.out().println(method.invoke(responseobj, null));
                            datarecord.put(methodnamebefore + method.getName().replaceFirst("get", ""),
									method.invoke(responseobj, null));
						} else {
                            datarecord.put(methodnamebefore + method.getName().replaceFirst("get", ""),
									null);
						}
						break;
					case "boolean":
					case "java.lang.Boolean":
						ctx.out().println(method.getName());
						if (method.invoke(responseobj, null) != null) {
							ctx.out().println(method.invoke(responseobj, null));
                            datarecord.put(methodnamebefore + method.getName().replaceFirst("get", ""),
									method.invoke(responseobj, null));
						} else {
                            datarecord.put(methodnamebefore + method.getName().replaceFirst("get", ""),
									null);
						}
						break;
					case "integer":
					case "java.lang.Integer":
						ctx.out().println(method.getName());
						if (method.invoke(responseobj, null) != null) {
							ctx.out().println(method.invoke(responseobj, null));
                            datarecord.put(methodnamebefore + method.getName().replaceFirst("get", ""),
									method.invoke(responseobj, null));
						} else {
                            datarecord.put(methodnamebefore + method.getName().replaceFirst("get", ""),
									null);
						}
						break;
					case "long":
					case "java.lang.Long":
						ctx.out().println(method.getName());
						if (method.invoke(responseobj, null) != null) {
							ctx.out().println(method.invoke(responseobj, null));
                            datarecord.put(methodnamebefore + method.getName().replaceFirst("get", ""),
									method.invoke(responseobj, null));
						} else {
                            datarecord.put(methodnamebefore + method.getName().replaceFirst("get", ""),
									null);
						}
						break;
					default:
						ctx.out().println("Oberklasse: " + method.getName());
                        methodnamebefore = method.getName().replaceFirst("get", "");
						datarecord2.put(method.getName(),datarecordReader(method.invoke(responseobj, null), ctx));
						methodnamebefore = "";
						break;
					} 
				}
			}
		}
		return datarecord2;
	}
}