package de.ck.abas.connection;

import de.abas.erp.common.type.AbasDate;
import https.onlineservice_creditreform_de.webservice._0600_0021.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public class Login{
    protected LogonResponse logoninput(Trequestheader logon)throws Exception{
        return Logon(logon);
    }

    private LogonResponse Logon(Trequestheader logon) throws Exception {
        LogonResponse response = null;
        String path = "owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
        LogonRequest logonRequest = new LogonRequest();
        logonRequest.setHeader(logon);
        try {
            new XMLConverterforLogging().XMLConvert(logonRequest,path);
            response = AbstractService.ctoMessagesSoap12.logon(logonRequest);
            new XMLConverterforLogging().XMLConvert(response,path);
        }catch(LogonFault | Servicefault | Validationfault e){
            new XMLConverterforLogging().XMLConvert(e,"Error"+path);
            new ErrorHandler().error(e);
        }
        //jaxbMarshaller.marshal(customer, System.out);
        //JAXB.marshal(logonRequest,"/owcrefo/xml/logonrequest.xml");
        return response;

    }
}
