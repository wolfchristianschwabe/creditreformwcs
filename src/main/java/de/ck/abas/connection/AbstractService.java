package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.CtoMessages;
import https.onlineservice_creditreform_de.webservice._0600_0021.CtoMessagesService;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 * AbastractService is the Class thats basicly needed to get the Webservice Object an everything else Done this is a Basis Class for all common Webservice Methods
 * @author christian.schwabe
 * @version 0.0.0.3
 */
public class AbstractService {
	public static CtoMessages ctoMessagesSoap12;
	public void AbstarctServiceInit() throws Exception {
		ctoMessagesSoap12 = new CtoMessagesService().getCtoMessagesSoap12();
		initWsZugriff(ctoMessagesSoap12,
				"https://ktu.onlineservice.creditreform.de/webservice/0600-0021/soap12/messages.wsdl", true);
	}
	/**
	 * @category this Method is needed to get a Proxy Data-Object for the Connection to the Webservice and all its Methods and Serviceabilities
	 * @param serviceInterface The Interface thats Created by the wsdl compiler of Java
	 * @param webserviceUrl The URL to the Webservice wsdl 
	 * @param enableGzip The enabling of the Gzip compression
	 */
	private void initWsZugriff(Object serviceInterface, String webserviceUrl, boolean enableGzip) {
		if (serviceInterface instanceof BindingProvider) {
			Map<String, Object> requestContext = ((BindingProvider) serviceInterface).getRequestContext();
			// URL setzen:
			requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, webserviceUrl);

			// Header setzen:
			Map<String, List<String>> headers = new HashMap<String, List<String>>();
			if (enableGzip) {
				headers.put("Accept-Encoding", Collections.singletonList("gzip"));
				headers.put("Content-Type", Collections.singletonList("application/soap+xml; charset=UTF-8"));
			}
			requestContext.put(MessageContext.HTTP_REQUEST_HEADERS, headers);
			requestContext.put(MessageContext.HTTP_REQUEST_METHOD, "POST");

		}
	}



}
