/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.Tlanguagerequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigInteger;
import java.util.GregorianCalendar;

/**
 * @author christian.schwabe
 * @version 0.0.0.2
 * @category This is the Object for Loging in the Service and getting the
 *           Credentials in the Header.
 * 
 */
public class Logonrequest {

	public Trequestheader buildupLogin(String generalPassword,String password,String language,String userid,int keylist) throws Exception {
		Integer clientapplicationversion;
		String clientapplicationname, transactionref;
		Trequestheader requestLogon = null;
		try {
		requestLogon = new Trequestheader();
    	clientapplicationversion = 100;
    	clientapplicationname = "CKABASConnector";
    	XMLGregorianCalendar date2 = null;
    	GregorianCalendar c = new GregorianCalendar();
    	date2 = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
    	int year = date2.getYear();
    	year = (year - 2000);
    	transactionref = userid + "-" + year + "." + date2.getDay() + "." + date2.getMonth();
		requestLogon.setClientapplicationname(clientapplicationname);
		requestLogon.setClientapplicationversion(keylist);
		requestLogon.setCommunicationlanguage((Tlanguagerequest.valueOf(language)));
		requestLogon.setGeneralpassword(generalPassword);
		requestLogon.setIndividualpassword(password);
		requestLogon.setKeylistversion(BigInteger.valueOf(keylist));
		requestLogon.setTransactionreference(transactionref);
		requestLogon.setTransmissiontimestamp(date2);
		requestLogon.setUseraccount(userid);
		return requestLogon;
		}catch(Exception e) {
			new ErrorHandler().error(e);
		}
		return requestLogon;
}

}
