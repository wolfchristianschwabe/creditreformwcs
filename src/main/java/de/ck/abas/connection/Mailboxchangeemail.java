/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.ChangeemailRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.ChangeemailResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tchangeemailrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category this is for the Service Mailboxdirectory to get this back and Declare the
 *           return as a Hashmap.
 * @version 0.0.0.2
 */
public class Mailboxchangeemail extends AbstractService {
	protected ChangeemailResponse initMailboxchangeemail(Tchangeemailrequestbody request, Trequestheader logon) throws Exception {
		return mailboxchangeemailService(request,logon);
	}
	/**
	 * 
	 * @param logon
	 * @return This Method returns an Object of the Type ChangemailResponse
	 * @throws Exception this Exception is Thrown if there is a Service Fault or the Service is not reachabele 
	 */
	private ChangeemailResponse mailboxchangeemailService(Tchangeemailrequestbody body, Trequestheader logon) throws Exception {
		ChangeemailRequest mail = new ChangeemailRequest();
		ChangeemailResponse response = new ChangeemailResponse();
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		mail.setHeader(logon);
		mail.setBody(body);
		try {
			new XMLConverterforLogging().XMLConvert(mail,path);
			response = ctoMessagesSoap12.changeemail(mail);
			new XMLConverterforLogging().XMLConvert(response,path);
			return response;
		} catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
		}
	}

}
