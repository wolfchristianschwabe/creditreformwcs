package de.ck.abas.connection;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class ErrorHandler {
	public static Method[] methods = null;
	public static StringBuffer result = new StringBuffer();
	public static Object[] arg =new Object[0];

	public void error(Object e) throws Exception {
		if (e == null) {
			return;
		}
		/*if(e instanceof Validationfault){
			Validationfault val = (Validationfault)e;
			result.append(val.getFaultInfo());
			throw new Exception(result.toString());
		}*/
		if(result!=null&&result.toString().startsWith("Error:")){
			result.delete(0,result.length()-1);
		}
		methods = e.getClass().getDeclaredMethods();
		for (Method method : methods) {
			if (method.getName().startsWith("get") && !method.getName().equals("getClass")) {
				method.getParameterCount();
				switch (method.getReturnType().getName()) {
					case "java.lang.String":
						result.append(method.invoke(e,arg)+"|");
						break;
					case "java.util.List":
					case "ArrayList":
						List<Object> tempList = (ArrayList<Object>) method.invoke(e, arg);
						if (tempList.size() > 0) {
							for (Object tempobj : tempList) {
								error(tempobj);
							}
						}
						break;
					case "java.lang.Boolean":
						result.append("Technischer Fehler: "+method.invoke(e,arg));
						break;
					default:
						error(method.invoke(e, arg));
						break;
				}
			}
		}
		result.insert(0,"Error:|");
	    throw new Exception(result.toString().trim().replaceAll("[|]+","\\\n\\\r"));
	}
}
