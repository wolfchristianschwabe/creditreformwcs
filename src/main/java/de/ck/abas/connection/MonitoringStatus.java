/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.MonitoringstatusRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.MonitoringstatusResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tmonitoringstatusrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category this is for the Service to get the Orderdata back and Declare the
 *           return as a Hashmap.
 * @version 0.0.0.2
 */
public class MonitoringStatus extends AbstractService {
	
	protected MonitoringstatusResponse initMonitoringstatus(Tmonitoringstatusrequestbody request, Trequestheader logon) throws Exception {
		return monitoringstatusService(request,logon);
	}
	/**
	 * @param logon 
	 * @return This Method returns an Object of the Type MonitoringstatusResponse
	 * @throws  Exception this Exception is Thrown if there is a Service Fault or the Service is not reachabele 
	 */
	private MonitoringstatusResponse monitoringstatusService(Tmonitoringstatusrequestbody body, Trequestheader logon) throws Exception {
		MonitoringstatusResponse response = null;
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		MonitoringstatusRequest request = new MonitoringstatusRequest();
		request.setHeader(logon);
		request.setBody(body);
		try {
			new XMLConverterforLogging().XMLConvert(request,path);
			response = ctoMessagesSoap12.monitoringstatus(request);
			new XMLConverterforLogging().XMLConvert(response,path);
			return response;
		} catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;

		}
	}

}
