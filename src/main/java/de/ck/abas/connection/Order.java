/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.OrderRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.OrderResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Torderrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category this is for the Service to get the Orderdata back and Declare the
 *           return as a Hashmap.
 * @version 0.0.0.2
 */
public class Order extends AbstractService  {
	
	protected OrderResponse initOrder(Trequestheader logon, Torderrequestbody body) throws Exception {
		return orderService(logon, body);
	}
	/**
	 * 
	 * @param logon
	 * @return This Method returns an Object of the Type OrderResponse
	 * @throws Exception this Exception is Thrown if there is a Service Fault or the Service is not reachabele 
	 */
	private OrderResponse orderService(Trequestheader logon, Torderrequestbody body) throws Exception {
		OrderResponse response = null;
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		OrderRequest request = new OrderRequest();
		request.setHeader(logon);
		request.setBody(body);
		try {
			new XMLConverterforLogging().XMLConvert(request,path);
			response = ctoMessagesSoap12.order(request);
			new XMLConverterforLogging().XMLConvert(response,path);
			return response;
		} catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
		}
	}

}
