/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.ParticipationdataRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.ParticipationdataResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationdatarequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.chainsaw.Main;

import java.io.File;
import java.io.InputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category Main Service Construktor for the Search Service its builded for Error handling and Datamanagement if any given Type is not in the row for the Parameter List.
 * @see Main Service Construktor for all Services is AbstractService {@link: AbstractService}
 */
public class Participationdata extends AbstractService  {

	protected ParticipationdataResponse initParticipationdata(Tparticipationdatarequestbody request, Trequestheader logon) throws Exception {
		return participationdataService(request,logon);
	}

	/**
	 * @param logon 
	 * @category Service Connection
	 * @return this Method returns an Object of the Type Tparticipationdataresponsebody
	 * @throws Exception this Exception is Thrown if there is a Service Fault or the Service is not reachabele 
	 */
	private ParticipationdataResponse participationdataService(Tparticipationdatarequestbody body, Trequestheader logon) throws Exception {
		ErrorHandler error;
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		ParticipationdataRequest participationdataRequest = new ParticipationdataRequest();
		participationdataRequest.setHeader(logon);
		participationdataRequest.setBody(body);
		ParticipationdataResponse response;
		/*
		 * Try Catch Block for Connection Establishment
		 */
		try {
			new XMLConverterforLogging().XMLConvert(participationdataRequest,path);
			response = ctoMessagesSoap12.participationdata(participationdataRequest);
			new XMLConverterforLogging().XMLConvert(response,path);
			//--------------------------------------------------------------------------------------------------------------------------------------
			//Getting the InputStream from the Stream of Data that is represented in Textreport. The DataSource is with MTOM Mechanisum convertabel.
			//--------------------------------------------------------------------------------------------------------------------------------------
			InputStream inputStream = response.getBody().getTextreport().getInputStream();
			/**
			 * Delete if File exists  
			 * This Code generate a File from the inputStream.
			 * The Datareader is for the XML Body. 
			 */
			File file = new File("C:/Users/christian.schwabe/Desktop/pdfscrefo/1234.pdf");
			if (file.exists()) {
				if (file.delete()) {
				
				}
			}
			FileUtils.copyInputStreamToFile(inputStream, file);

			}catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
				}
		return response;
	}

}
