package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @version 0.0.0.2
 * @category This Class represents the service Productavailability
 */
public class ProductAvailabilty extends AbstractService {
	ProductavailabilityResponse response = null;
/**
 * @return 
 * @category Method for Initialising the Service 
 * @throws Exception, this is thrown if the Service can't be reached in the first try. 
 */
	protected ProductavailabilityResponse productAvailabiltycheck(Trequestheader requestLogon, Tproductavailabilityrequestbody body) throws Exception {
		return productAvailabilityService(requestLogon,  body);	
	}
	/**
	 *
	 * @param requestLogon
	 * @throws Exception this is thrown if the Webservice dont return a good Value or is not reachable in the First try
	 * @return
	 */
	private ProductavailabilityResponse productAvailabilityService(Trequestheader requestLogon, Tproductavailabilityrequestbody body)
			throws Exception {
		response = null;
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		ProductavailabilityRequest productavailable = new ProductavailabilityRequest();
		productavailable.setHeader(requestLogon);
		productavailable.setBody(body);
		try {
			new XMLConverterforLogging().XMLConvert(productavailable,path);
			response = ctoMessagesSoap12.productavailability(productavailable);
			new XMLConverterforLogging().XMLConvert(response,path);
			return response;
		
		} catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
		}

	}
}
