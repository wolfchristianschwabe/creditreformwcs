/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.ParticipationdatastartRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.ParticipationdatastartResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationdatastartrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;
import org.apache.log4j.chainsaw.Main;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category Main Service Construktor for the Search Service its builded for Error handling and Datamanagement if any given Type is not in the row for the Parameter List.
 * @see Main Service Construktor for all Services is AbstractService {@link: AbstractService}
 */
public class Participationdatastart extends AbstractService {

	protected ParticipationdatastartResponse initParticipationdatastart(Tparticipationdatastartrequestbody request, Trequestheader logon) throws Exception {
		return participationdatastartService(request,logon);
	}

	/**
	 * @param logon 
	 * @param request 
	 * @category Service Connection
	 * @return This Method return an Object of Type Tparticipationdatastartresponsebody
	 * @throws Exception this Exception is Thrown if there is a Service Fault or the Service is not reachabele 
	 */
	private ParticipationdatastartResponse participationdatastartService(Tparticipationdatastartrequestbody request, Trequestheader logon) throws Exception {
		ErrorHandler error;
		ParticipationdatastartResponse response=null;
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		ParticipationdatastartRequest participationdataRequest = new ParticipationdatastartRequest();
		participationdataRequest.setHeader(logon);
		participationdataRequest.setBody(request);
		/*
		 * Try Catch Block for Connection Establishment
		 */
		try {
			new XMLConverterforLogging().XMLConvert(participationdataRequest,path);
			response = ctoMessagesSoap12.participationdatastart(participationdataRequest);
			new XMLConverterforLogging().XMLConvert(response,path);
			}catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
				}
		return response;
	}

}
