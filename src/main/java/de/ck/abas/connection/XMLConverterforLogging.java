/**
 * 
 */
package de.ck.abas.connection;

import de.abas.eks.jfop.remote.FO;
import de.ck.app.ckcre.cws.util.LoggerUtil;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;

/**
 * @author Christian Schwabe Computerkomplett 
 * 15.04.2019
 * Webservice
 * This Programm Marshals the Object Given in a Given XML construct you need to have JaxB for this.
 */
public class XMLConverterforLogging {
	public void XMLConvert(Object obj, String path) throws Exception{
				try {
					File filedirectory = new File(path);
					File file = new File(path+"/"+obj.getClass().getSimpleName()+".xml");
					filedirectory.mkdirs();
					if(filedirectory.exists()){
						file.createNewFile();
					}
					JAXBContext ctx = JAXBContext.newInstance(obj.getClass().getPackage().getName());
					Marshaller marshaller = ctx.createMarshaller();
					marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
					marshaller.marshal(obj, file);
				}
				catch (Exception e) {
					LoggerUtil.LOG.error("Fehler",e);
					FO.box("Fehler",e.getMessage());
					// System.out.println(e.getMessage());
				}
			}

		}

		/*try {

		 if(!filedirectory.exists()) {

		 }

	 	 if(filedirectory.exists()){
	        JAXBContext ctx = JAXBContext.newInstance("https.onlineservice_creditreform_de.webservice._0600_0021");
	        Marshaller marshaller = ctx.createMarshaller();
	        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
	        marshaller.marshal(obj, file);
		 }
	    }
	    catch (Exception e) {
	    	throw  new Exception(e.getMessage());
	     // System.out.println(e.getMessage());
	    }
	}
	
}*/
