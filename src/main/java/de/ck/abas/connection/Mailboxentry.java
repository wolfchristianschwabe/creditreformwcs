/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.MailboxentryRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.MailboxentryResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxentryrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category this is for the Service MailboxEntry to get Mails of the Directory
 * @version 0.0.0.2
 */
public class Mailboxentry extends AbstractService {
	
	protected MailboxentryResponse initMailboxEntry(Tmailboxentryrequestbody request, Trequestheader logon) throws Exception {
		return mailboxEntryService(request,logon);
	}
	/**
	 * 
	 * @param logon
	 * @return This Method returns an Object of the Type MailboxentryResponse
	 * @throws Exception this Exception is Thrown if there is a Service Fault or the Service is not reachabele 
	 */
	private MailboxentryResponse mailboxEntryService(Tmailboxentryrequestbody body, Trequestheader logon) throws Exception {
		MailboxentryRequest mail = new MailboxentryRequest();
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		MailboxentryResponse response = new MailboxentryResponse();
		mail.setHeader(logon);
		mail.setBody(body);
		try {
			new XMLConverterforLogging().XMLConvert(mail,path);
			response = ctoMessagesSoap12.mailboxentry(mail);
			new XMLConverterforLogging().XMLConvert(response,path);
			return response;
		} catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
		}
	}

}
