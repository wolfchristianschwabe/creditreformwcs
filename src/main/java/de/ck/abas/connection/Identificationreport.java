/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.IdentificationreportRequest;
import https.onlineservice_creditreform_de.webservice._0600_0021.IdentificationreportResponse;
import https.onlineservice_creditreform_de.webservice._0600_0021.Tidentificationreportrequestbody;
import https.onlineservice_creditreform_de.webservice._0600_0021.Trequestheader;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category this is for the Service to get the Reportdata back and Declare the
 *           return as a Hashmap.
 * @version 0.0.0.2
 */
public class Identificationreport extends AbstractService  {

	protected IdentificationreportResponse initIdentificationreport(Trequestheader logon, Tidentificationreportrequestbody body) throws Exception {
		return identificationreportService(logon, body);
	}
	/**
	 * 
	 * @param logon
	 * @return This Method returns an Object of type identificationreportService
	 * @throws Exception this Exception is Thrown if there is a Service Fault or the Service is not reachabele 
	 */
	private IdentificationreportResponse identificationreportService(Trequestheader logon, Tidentificationreportrequestbody body) throws Exception {
		IdentificationreportRequest identification = new IdentificationreportRequest();
		IdentificationreportResponse response = new IdentificationreportResponse();
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		identification.setHeader(logon);
		identification.setBody(body);
		try {
			new XMLConverterforLogging().XMLConvert(identification,path);
			response = ctoMessagesSoap12.identificationreport(identification);
			new XMLConverterforLogging().XMLConvert(response,path);
			return response;

		} catch (Exception e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
		} finally {

		}
	}

}
