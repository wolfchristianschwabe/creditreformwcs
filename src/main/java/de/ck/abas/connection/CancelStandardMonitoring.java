/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category This Class is the Cancel Service for the Standardmonitoring from Crefo
 * @version 0.0.1
 */
public class CancelStandardMonitoring extends AbstractService {
	protected CancelstandardmonitoringResponse initcancelStandardMonitoring(Tcancelstandardmonitoringrequestbody request, Trequestheader logon) throws Exception {
		return cancelStandardMonitoring(request,logon);
	}		
	/**
	 * @param logon 
	 * @param request 
	 * @return the Method retuns the response of the Webservice standard is CancelstandartmonitoringResponse
	 * @throws Exception this is thrown if the Service is not available or the Service is not Reachable and if the Service has an Error
	 */
	private CancelstandardmonitoringResponse cancelStandardMonitoring(Tcancelstandardmonitoringrequestbody request, Trequestheader logon) 
			throws Exception {
		CancelstandardmonitoringResponse response = null;
		String path = "/owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		CancelstandardmonitoringRequest cancelstanadardmonitoring = new CancelstandardmonitoringRequest();
		cancelstanadardmonitoring.setHeader(logon);
		cancelstanadardmonitoring.setBody(request);
		try {
			new XMLConverterforLogging().XMLConvert(cancelstanadardmonitoring,path);
			response = ctoMessagesSoap12.cancelstandardmonitoring(cancelstanadardmonitoring);
			new XMLConverterforLogging().XMLConvert(response,path);
			if(response == null) {
				System.out.println("gel�scht");
			}
		} catch (Servicefault | Validationfault e) {
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
		}
		return response;
	}
}
