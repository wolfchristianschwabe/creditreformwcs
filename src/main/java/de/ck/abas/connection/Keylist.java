/**
 * 
 */
package de.ck.abas.connection;

import https.onlineservice_creditreform_de.webservice._0600_0021.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * @author christian.schwabe
 * @category This Class is for the Exchange of the Keylist that has to be made
 *           once a Week automaticly
 * @version 0.0.0.1
 */
public class Keylist extends AbstractService {

	protected Tkeylistresponsebody Keylistinput(Trequestheader logon) throws Exception {
		return Keylist(logon);
	}

	/**
	 * @category Main Method for the Service start
	 * @param logon The Connection to the Service created in the AbstractService
	 *              Method
	 * @return The Return of this method is the Response of the Webservice that
	 *         could be a new Keylist for the Webservice mapping
	 * @throws Exception this is thrown if the keylistrequest is false or the
	 *                   returned response is null
	 */
	private Tkeylistresponsebody Keylist(Trequestheader logon) throws Exception {
		Tkeylistresponsebody response = null;
		KeylistResponse responsekeylist = null;
		String path = "owcrefo/log/"+LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));
		KeylistRequest keylistRequest = new KeylistRequest();
		keylistRequest.setHeader(logon);
		try {
			new XMLConverterforLogging().XMLConvert(keylistRequest,path);
			responsekeylist = ctoMessagesSoap12.keylist(keylistRequest);
			new XMLConverterforLogging().XMLConvert(response,path);
			response = responsekeylist.getBody();
			return response;
		} catch (KeylistFault | Servicefault | Validationfault e){
			new XMLConverterforLogging().XMLConvert(e,"Error"+path);
			new ErrorHandler().error(e);
			return null;
		}

	}

}
