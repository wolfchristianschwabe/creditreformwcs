
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tamount complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tamount">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;sequence minOccurs="0">
 *           &lt;element name="value">
 *             &lt;simpleType>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *                 &lt;fractionDigits value="2"/>
 *               &lt;/restriction>
 *             &lt;/simpleType>
 *           &lt;/element>
 *           &lt;element name="currency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *         &lt;/sequence>
 *         &lt;element name="decoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tamount", propOrder = {
    "value",
    "currency",
    "decoration"
})
@XmlSeeAlso({
    https.onlineservice_creditreform_de.webservice._0600_0021.Trealestateamounts.Amount.class,
    Tamountpercent.class
})
public class Tamount {

    protected BigDecimal value;
    protected Tkey currency;
    protected Tkey decoration;

    /**
     * Ruft den Wert der value-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getValue() {
        return value;
    }

    /**
     * Legt den Wert der value-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setValue(BigDecimal value) {
        this.value = value;
    }

    /**
     * Ruft den Wert der currency-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCurrency() {
        return currency;
    }

    /**
     * Legt den Wert der currency-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCurrency(Tkey value) {
        this.currency = value;
    }

    /**
     * Ruft den Wert der decoration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getDecoration() {
        return decoration;
    }

    /**
     * Legt den Wert der decoration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setDecoration(Tkey value) {
        this.decoration = value;
    }

}
