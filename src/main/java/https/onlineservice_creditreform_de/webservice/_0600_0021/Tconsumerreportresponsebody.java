
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tconsumerreportresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tconsumerreportresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element name="creationtime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="useraccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="negativereport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgrequestdata" minOccurs="0"/>
 *         &lt;element name="namesdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgnamesdata" minOccurs="0"/>
 *         &lt;element name="addressdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgaddressdata" minOccurs="0"/>
 *         &lt;element name="addresscheckdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgaddresscheckdata" minOccurs="0"/>
 *         &lt;element name="geodata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcggeodata" minOccurs="0"/>
 *         &lt;element name="ratingdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgratingdata" minOccurs="0"/>
 *         &lt;element name="telecomdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgtelecomdata" minOccurs="0"/>
 *         &lt;element name="negativefacts" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgnegativefacts" minOccurs="0"/>
 *         &lt;element name="paymentbehaviournegative" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgpaymentbehaviour" minOccurs="0"/>
 *         &lt;element name="paymentbehaviourpositive" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgpaymentbehaviour" minOccurs="0"/>
 *         &lt;element name="collectiondata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgcollectiondata" minOccurs="0"/>
 *         &lt;element name="participationdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgparticipationdata" minOccurs="0"/>
 *         &lt;element name="formerrequestsdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgformerrequestsdata" minOccurs="0"/>
 *         &lt;element name="textreport" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tconsumerreportresponsebody", propOrder = {
    "transactionid",
    "customerreference",
    "creationtime",
    "producttype",
    "useraccount",
    "negativereport",
    "requestdata",
    "namesdata",
    "addressdata",
    "addresscheckdata",
    "geodata",
    "ratingdata",
    "telecomdata",
    "negativefacts",
    "paymentbehaviournegative",
    "paymentbehaviourpositive",
    "collectiondata",
    "participationdata",
    "formerrequestsdata",
    "textreport"
})
public class Tconsumerreportresponsebody {

    protected String transactionid;
    protected String customerreference;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationtime;
    protected Tkey producttype;
    protected String useraccount;
    protected String negativereport;
    protected Tcgrequestdata requestdata;
    protected Tcgnamesdata namesdata;
    protected Tcgaddressdata addressdata;
    protected Tcgaddresscheckdata addresscheckdata;
    protected Tcggeodata geodata;
    protected Tcgratingdata ratingdata;
    protected Tcgtelecomdata telecomdata;
    protected Tcgnegativefacts negativefacts;
    protected Tcgpaymentbehaviour paymentbehaviournegative;
    protected Tcgpaymentbehaviour paymentbehaviourpositive;
    protected Tcgcollectiondata collectiondata;
    protected Tcgparticipationdata participationdata;
    protected Tcgformerrequestsdata formerrequestsdata;
    @XmlMimeType("application/octet-stream")
    protected DataHandler textreport;

    /**
     * Ruft den Wert der transactionid-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionid() {
        return transactionid;
    }

    /**
     * Legt den Wert der transactionid-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionid(String value) {
        this.transactionid = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der creationtime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationtime() {
        return creationtime;
    }

    /**
     * Legt den Wert der creationtime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationtime(XMLGregorianCalendar value) {
        this.creationtime = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setProducttype(Tkey value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der useraccount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseraccount() {
        return useraccount;
    }

    /**
     * Legt den Wert der useraccount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseraccount(String value) {
        this.useraccount = value;
    }

    /**
     * Ruft den Wert der negativereport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNegativereport() {
        return negativereport;
    }

    /**
     * Legt den Wert der negativereport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNegativereport(String value) {
        this.negativereport = value;
    }

    /**
     * Ruft den Wert der requestdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgrequestdata }
     *     
     */
    public Tcgrequestdata getRequestdata() {
        return requestdata;
    }

    /**
     * Legt den Wert der requestdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgrequestdata }
     *     
     */
    public void setRequestdata(Tcgrequestdata value) {
        this.requestdata = value;
    }

    /**
     * Ruft den Wert der namesdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgnamesdata }
     *     
     */
    public Tcgnamesdata getNamesdata() {
        return namesdata;
    }

    /**
     * Legt den Wert der namesdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgnamesdata }
     *     
     */
    public void setNamesdata(Tcgnamesdata value) {
        this.namesdata = value;
    }

    /**
     * Ruft den Wert der addressdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgaddressdata }
     *     
     */
    public Tcgaddressdata getAddressdata() {
        return addressdata;
    }

    /**
     * Legt den Wert der addressdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgaddressdata }
     *     
     */
    public void setAddressdata(Tcgaddressdata value) {
        this.addressdata = value;
    }

    /**
     * Ruft den Wert der addresscheckdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgaddresscheckdata }
     *     
     */
    public Tcgaddresscheckdata getAddresscheckdata() {
        return addresscheckdata;
    }

    /**
     * Legt den Wert der addresscheckdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgaddresscheckdata }
     *     
     */
    public void setAddresscheckdata(Tcgaddresscheckdata value) {
        this.addresscheckdata = value;
    }

    /**
     * Ruft den Wert der geodata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcggeodata }
     *     
     */
    public Tcggeodata getGeodata() {
        return geodata;
    }

    /**
     * Legt den Wert der geodata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcggeodata }
     *     
     */
    public void setGeodata(Tcggeodata value) {
        this.geodata = value;
    }

    /**
     * Ruft den Wert der ratingdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgratingdata }
     *     
     */
    public Tcgratingdata getRatingdata() {
        return ratingdata;
    }

    /**
     * Legt den Wert der ratingdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgratingdata }
     *     
     */
    public void setRatingdata(Tcgratingdata value) {
        this.ratingdata = value;
    }

    /**
     * Ruft den Wert der telecomdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgtelecomdata }
     *     
     */
    public Tcgtelecomdata getTelecomdata() {
        return telecomdata;
    }

    /**
     * Legt den Wert der telecomdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgtelecomdata }
     *     
     */
    public void setTelecomdata(Tcgtelecomdata value) {
        this.telecomdata = value;
    }

    /**
     * Ruft den Wert der negativefacts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgnegativefacts }
     *     
     */
    public Tcgnegativefacts getNegativefacts() {
        return negativefacts;
    }

    /**
     * Legt den Wert der negativefacts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgnegativefacts }
     *     
     */
    public void setNegativefacts(Tcgnegativefacts value) {
        this.negativefacts = value;
    }

    /**
     * Ruft den Wert der paymentbehaviournegative-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgpaymentbehaviour }
     *     
     */
    public Tcgpaymentbehaviour getPaymentbehaviournegative() {
        return paymentbehaviournegative;
    }

    /**
     * Legt den Wert der paymentbehaviournegative-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgpaymentbehaviour }
     *     
     */
    public void setPaymentbehaviournegative(Tcgpaymentbehaviour value) {
        this.paymentbehaviournegative = value;
    }

    /**
     * Ruft den Wert der paymentbehaviourpositive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgpaymentbehaviour }
     *     
     */
    public Tcgpaymentbehaviour getPaymentbehaviourpositive() {
        return paymentbehaviourpositive;
    }

    /**
     * Legt den Wert der paymentbehaviourpositive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgpaymentbehaviour }
     *     
     */
    public void setPaymentbehaviourpositive(Tcgpaymentbehaviour value) {
        this.paymentbehaviourpositive = value;
    }

    /**
     * Ruft den Wert der collectiondata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgcollectiondata }
     *     
     */
    public Tcgcollectiondata getCollectiondata() {
        return collectiondata;
    }

    /**
     * Legt den Wert der collectiondata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgcollectiondata }
     *     
     */
    public void setCollectiondata(Tcgcollectiondata value) {
        this.collectiondata = value;
    }

    /**
     * Ruft den Wert der participationdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgparticipationdata }
     *     
     */
    public Tcgparticipationdata getParticipationdata() {
        return participationdata;
    }

    /**
     * Legt den Wert der participationdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgparticipationdata }
     *     
     */
    public void setParticipationdata(Tcgparticipationdata value) {
        this.participationdata = value;
    }

    /**
     * Ruft den Wert der formerrequestsdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgformerrequestsdata }
     *     
     */
    public Tcgformerrequestsdata getFormerrequestsdata() {
        return formerrequestsdata;
    }

    /**
     * Legt den Wert der formerrequestsdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgformerrequestsdata }
     *     
     */
    public void setFormerrequestsdata(Tcgformerrequestsdata value) {
        this.formerrequestsdata = value;
    }

    /**
     * Ruft den Wert der textreport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getTextreport() {
        return textreport;
    }

    /**
     * Legt den Wert der textreport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setTextreport(DataHandler value) {
        this.textreport = value;
    }

}
