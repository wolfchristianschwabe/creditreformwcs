
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Address Data
 * 
 * <p>Java-Klasse f�r Tcollectionrequestaddress complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionrequestaddress">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="addressforservice">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="street" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstreet"/>
 *                   &lt;element name="housenumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Thousenumber" minOccurs="0"/>
 *                   &lt;element name="housenumberaffix" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Thousenumberaffix" minOccurs="0"/>
 *                   &lt;element name="postcode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpostcode"/>
 *                   &lt;element name="city" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcity"/>
 *                   &lt;element name="country" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcountry"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="pobaddress">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="pob">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="postcode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpostcode"/>
 *                   &lt;element name="city" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcity"/>
 *                   &lt;element name="country" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcountry"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="majorcustomeraddress">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="postcode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpostcode" minOccurs="0"/>
 *                   &lt;element name="city" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcity" minOccurs="0"/>
 *                   &lt;element name="country" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcountry"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionrequestaddress", propOrder = {
    "addressforservice",
    "pobaddress",
    "majorcustomeraddress"
})
public class Tcollectionrequestaddress {

    protected Tcollectionrequestaddress.Addressforservice addressforservice;
    protected Tcollectionrequestaddress.Pobaddress pobaddress;
    protected Tcollectionrequestaddress.Majorcustomeraddress majorcustomeraddress;

    /**
     * Ruft den Wert der addressforservice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestaddress.Addressforservice }
     *     
     */
    public Tcollectionrequestaddress.Addressforservice getAddressforservice() {
        return addressforservice;
    }

    /**
     * Legt den Wert der addressforservice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestaddress.Addressforservice }
     *     
     */
    public void setAddressforservice(Tcollectionrequestaddress.Addressforservice value) {
        this.addressforservice = value;
    }

    /**
     * Ruft den Wert der pobaddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestaddress.Pobaddress }
     *     
     */
    public Tcollectionrequestaddress.Pobaddress getPobaddress() {
        return pobaddress;
    }

    /**
     * Legt den Wert der pobaddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestaddress.Pobaddress }
     *     
     */
    public void setPobaddress(Tcollectionrequestaddress.Pobaddress value) {
        this.pobaddress = value;
    }

    /**
     * Ruft den Wert der majorcustomeraddress-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestaddress.Majorcustomeraddress }
     *     
     */
    public Tcollectionrequestaddress.Majorcustomeraddress getMajorcustomeraddress() {
        return majorcustomeraddress;
    }

    /**
     * Legt den Wert der majorcustomeraddress-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestaddress.Majorcustomeraddress }
     *     
     */
    public void setMajorcustomeraddress(Tcollectionrequestaddress.Majorcustomeraddress value) {
        this.majorcustomeraddress = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="street" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstreet"/>
     *         &lt;element name="housenumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Thousenumber" minOccurs="0"/>
     *         &lt;element name="housenumberaffix" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Thousenumberaffix" minOccurs="0"/>
     *         &lt;element name="postcode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpostcode"/>
     *         &lt;element name="city" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcity"/>
     *         &lt;element name="country" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcountry"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "street",
        "housenumber",
        "housenumberaffix",
        "postcode",
        "city",
        "country"
    })
    public static class Addressforservice {

        @XmlElement(required = true)
        protected String street;
        @XmlSchemaType(name = "integer")
        protected Integer housenumber;
        protected String housenumberaffix;
        @XmlElement(required = true)
        protected String postcode;
        @XmlElement(required = true)
        protected String city;
        @XmlElement(required = true)
        protected String country;

        /**
         * Ruft den Wert der street-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreet() {
            return street;
        }

        /**
         * Legt den Wert der street-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreet(String value) {
            this.street = value;
        }

        /**
         * Ruft den Wert der housenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getHousenumber() {
            return housenumber;
        }

        /**
         * Legt den Wert der housenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setHousenumber(Integer value) {
            this.housenumber = value;
        }

        /**
         * Ruft den Wert der housenumberaffix-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHousenumberaffix() {
            return housenumberaffix;
        }

        /**
         * Legt den Wert der housenumberaffix-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHousenumberaffix(String value) {
            this.housenumberaffix = value;
        }

        /**
         * Ruft den Wert der postcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostcode() {
            return postcode;
        }

        /**
         * Legt den Wert der postcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostcode(String value) {
            this.postcode = value;
        }

        /**
         * Ruft den Wert der city-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Legt den Wert der city-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Ruft den Wert der country-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountry() {
            return country;
        }

        /**
         * Legt den Wert der country-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountry(String value) {
            this.country = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="postcode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpostcode" minOccurs="0"/>
     *         &lt;element name="city" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcity" minOccurs="0"/>
     *         &lt;element name="country" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcountry"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "postcode",
        "city",
        "country"
    })
    public static class Majorcustomeraddress {

        protected String postcode;
        protected String city;
        @XmlElement(required = true)
        protected String country;

        /**
         * Ruft den Wert der postcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostcode() {
            return postcode;
        }

        /**
         * Legt den Wert der postcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostcode(String value) {
            this.postcode = value;
        }

        /**
         * Ruft den Wert der city-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Legt den Wert der city-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Ruft den Wert der country-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountry() {
            return country;
        }

        /**
         * Legt den Wert der country-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountry(String value) {
            this.country = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="pob">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="postcode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpostcode"/>
     *         &lt;element name="city" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcity"/>
     *         &lt;element name="country" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcountry"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pob",
        "postcode",
        "city",
        "country"
    })
    public static class Pobaddress {

        @XmlElement(required = true)
        protected String pob;
        @XmlElement(required = true)
        protected String postcode;
        @XmlElement(required = true)
        protected String city;
        @XmlElement(required = true)
        protected String country;

        /**
         * Ruft den Wert der pob-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPob() {
            return pob;
        }

        /**
         * Legt den Wert der pob-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPob(String value) {
            this.pob = value;
        }

        /**
         * Ruft den Wert der postcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostcode() {
            return postcode;
        }

        /**
         * Legt den Wert der postcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostcode(String value) {
            this.postcode = value;
        }

        /**
         * Ruft den Wert der city-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Legt den Wert der city-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Ruft den Wert der country-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountry() {
            return country;
        }

        /**
         * Legt den Wert der country-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountry(String value) {
            this.country = value;
        }

    }

}
