
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipationscompany complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipationscompany">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="furtherparticipations" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="propertyparticipant" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="participationtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="activeparticipations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipations" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="formerparticipations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipations" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipationscompany", propOrder = {
    "furtherparticipations",
    "propertyparticipant",
    "text"
})
public class Tparticipationscompany {

    protected Boolean furtherparticipations;
    protected List<Tparticipationscompany.Propertyparticipant> propertyparticipant;
    protected Ttext text;

    /**
     * Ruft den Wert der furtherparticipations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFurtherparticipations() {
        return furtherparticipations;
    }

    /**
     * Legt den Wert der furtherparticipations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFurtherparticipations(Boolean value) {
        this.furtherparticipations = value;
    }

    /**
     * Gets the value of the propertyparticipant property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the propertyparticipant property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPropertyparticipant().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tparticipationscompany.Propertyparticipant }
     * 
     * 
     */
    public List<Tparticipationscompany.Propertyparticipant> getPropertyparticipant() {
        if (propertyparticipant == null) {
            propertyparticipant = new ArrayList<Tparticipationscompany.Propertyparticipant>();
        }
        return this.propertyparticipant;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="participationtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="activeparticipations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipations" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="formerparticipations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipations" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "participationtype",
        "activeparticipations",
        "formerparticipations"
    })
    public static class Propertyparticipant {

        protected Tkey participationtype;
        protected List<Tparticipations> activeparticipations;
        protected List<Tparticipations> formerparticipations;

        /**
         * Ruft den Wert der participationtype-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getParticipationtype() {
            return participationtype;
        }

        /**
         * Legt den Wert der participationtype-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setParticipationtype(Tkey value) {
            this.participationtype = value;
        }

        /**
         * Gets the value of the activeparticipations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the activeparticipations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getActiveparticipations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tparticipations }
         * 
         * 
         */
        public List<Tparticipations> getActiveparticipations() {
            if (activeparticipations == null) {
                activeparticipations = new ArrayList<Tparticipations>();
            }
            return this.activeparticipations;
        }

        /**
         * Gets the value of the formerparticipations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the formerparticipations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFormerparticipations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tparticipations }
         * 
         * 
         */
        public List<Tparticipations> getFormerparticipations() {
            if (formerparticipations == null) {
                formerparticipations = new ArrayList<Tparticipations>();
            }
            return this.formerparticipations;
        }

    }

}
