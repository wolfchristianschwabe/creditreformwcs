
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcollectionagencyfreetextmessageresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionagencyfreetextmessageresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="message" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="customerreference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="noticeid" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="subject" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionagencyfreetextmessageresponsebody", propOrder = {
    "message"
})
public class Tcollectionagencyfreetextmessageresponsebody {

    protected List<Tcollectionagencyfreetextmessageresponsebody.Message> message;

    /**
     * Gets the value of the message property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the message property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMessage().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcollectionagencyfreetextmessageresponsebody.Message }
     * 
     * 
     */
    public List<Tcollectionagencyfreetextmessageresponsebody.Message> getMessage() {
        if (message == null) {
            message = new ArrayList<Tcollectionagencyfreetextmessageresponsebody.Message>();
        }
        return this.message;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="customerreference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="noticeid" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="subject" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "filenumber",
        "customerreference",
        "noticeid",
        "subject",
        "user"
    })
    public static class Message {

        @XmlElement(required = true)
        protected String filenumber;
        protected String customerreference;
        @XmlElement(required = true)
        protected String noticeid;
        @XmlElement(required = true)
        protected String subject;
        protected String user;

        /**
         * Ruft den Wert der filenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFilenumber() {
            return filenumber;
        }

        /**
         * Legt den Wert der filenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFilenumber(String value) {
            this.filenumber = value;
        }

        /**
         * Ruft den Wert der customerreference-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerreference() {
            return customerreference;
        }

        /**
         * Legt den Wert der customerreference-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerreference(String value) {
            this.customerreference = value;
        }

        /**
         * Ruft den Wert der noticeid-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNoticeid() {
            return noticeid;
        }

        /**
         * Legt den Wert der noticeid-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNoticeid(String value) {
            this.noticeid = value;
        }

        /**
         * Ruft den Wert der subject-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSubject() {
            return subject;
        }

        /**
         * Legt den Wert der subject-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSubject(String value) {
            this.subject = value;
        }

        /**
         * Ruft den Wert der user-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUser() {
            return user;
        }

        /**
         * Legt den Wert der user-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUser(String value) {
            this.user = value;
        }

    }

}
