
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Trequestdata complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Trequestdata">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="ordertime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="ordertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="reportlanguage" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlanguage" minOccurs="0"/>
 *         &lt;element name="legitimateinterest" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *         &lt;choice minOccurs="0">
 *           &lt;element name="company">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
 *                     &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="privateperson">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;sequence minOccurs="0">
 *           &lt;element name="creditenquiryamount">
 *             &lt;simpleType>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                 &lt;minInclusive value="0"/>
 *                 &lt;maxInclusive value="9999999999999999"/>
 *               &lt;/restriction>
 *             &lt;/simpleType>
 *           &lt;/element>
 *           &lt;element name="creditenquirycurrency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *         &lt;/sequence>
 *         &lt;element name="orderspecifyingtext" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Trequestdata", propOrder = {
    "ordertime",
    "producttype",
    "ordertype",
    "reportlanguage",
    "legitimateinterest",
    "identificationnumber",
    "easynumber",
    "company",
    "privateperson",
    "creditenquiryamount",
    "creditenquirycurrency",
    "orderspecifyingtext"
})
public class Trequestdata {

    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar ordertime;
    protected Tkey producttype;
    protected Tkey ordertype;
    protected String reportlanguage;
    protected Tkey legitimateinterest;
    protected String identificationnumber;
    protected String easynumber;
    protected Trequestdata.Company company;
    protected Trequestdata.Privateperson privateperson;
    protected Long creditenquiryamount;
    protected Tkey creditenquirycurrency;
    protected Ttext orderspecifyingtext;

    /**
     * Ruft den Wert der ordertime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOrdertime() {
        return ordertime;
    }

    /**
     * Legt den Wert der ordertime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOrdertime(XMLGregorianCalendar value) {
        this.ordertime = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setProducttype(Tkey value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der ordertype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getOrdertype() {
        return ordertype;
    }

    /**
     * Legt den Wert der ordertype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setOrdertype(Tkey value) {
        this.ordertype = value;
    }

    /**
     * Ruft den Wert der reportlanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportlanguage() {
        return reportlanguage;
    }

    /**
     * Legt den Wert der reportlanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportlanguage(String value) {
        this.reportlanguage = value;
    }

    /**
     * Ruft den Wert der legitimateinterest-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getLegitimateinterest() {
        return legitimateinterest;
    }

    /**
     * Legt den Wert der legitimateinterest-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setLegitimateinterest(Tkey value) {
        this.legitimateinterest = value;
    }

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der easynumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEasynumber() {
        return easynumber;
    }

    /**
     * Legt den Wert der easynumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEasynumber(String value) {
        this.easynumber = value;
    }

    /**
     * Ruft den Wert der company-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Trequestdata.Company }
     *     
     */
    public Trequestdata.Company getCompany() {
        return company;
    }

    /**
     * Legt den Wert der company-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Trequestdata.Company }
     *     
     */
    public void setCompany(Trequestdata.Company value) {
        this.company = value;
    }

    /**
     * Ruft den Wert der privateperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Trequestdata.Privateperson }
     *     
     */
    public Trequestdata.Privateperson getPrivateperson() {
        return privateperson;
    }

    /**
     * Legt den Wert der privateperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Trequestdata.Privateperson }
     *     
     */
    public void setPrivateperson(Trequestdata.Privateperson value) {
        this.privateperson = value;
    }

    /**
     * Ruft den Wert der creditenquiryamount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCreditenquiryamount() {
        return creditenquiryamount;
    }

    /**
     * Legt den Wert der creditenquiryamount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCreditenquiryamount(Long value) {
        this.creditenquiryamount = value;
    }

    /**
     * Ruft den Wert der creditenquirycurrency-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCreditenquirycurrency() {
        return creditenquirycurrency;
    }

    /**
     * Legt den Wert der creditenquirycurrency-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCreditenquirycurrency(Tkey value) {
        this.creditenquirycurrency = value;
    }

    /**
     * Ruft den Wert der orderspecifyingtext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getOrderspecifyingtext() {
        return orderspecifyingtext;
    }

    /**
     * Legt den Wert der orderspecifyingtext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setOrderspecifyingtext(Ttext value) {
        this.orderspecifyingtext = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
     *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "salutation",
        "companyname",
        "tradename",
        "commercialname",
        "alias",
        "street",
        "housenumber",
        "housenumberaffix",
        "postcode",
        "city",
        "quarter",
        "region",
        "country",
        "legalform"
    })
    public static class Company {

        protected Tkey salutation;
        protected String companyname;
        protected String tradename;
        protected String commercialname;
        protected String alias;
        protected String street;
        protected Integer housenumber;
        protected String housenumberaffix;
        protected String postcode;
        protected String city;
        protected String quarter;
        protected String region;
        protected Tkey country;
        protected Tkeywithshortdesignation legalform;

        /**
         * Ruft den Wert der salutation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getSalutation() {
            return salutation;
        }

        /**
         * Legt den Wert der salutation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setSalutation(Tkey value) {
            this.salutation = value;
        }

        /**
         * Ruft den Wert der companyname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyname() {
            return companyname;
        }

        /**
         * Legt den Wert der companyname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyname(String value) {
            this.companyname = value;
        }

        /**
         * Ruft den Wert der tradename-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTradename() {
            return tradename;
        }

        /**
         * Legt den Wert der tradename-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTradename(String value) {
            this.tradename = value;
        }

        /**
         * Ruft den Wert der commercialname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCommercialname() {
            return commercialname;
        }

        /**
         * Legt den Wert der commercialname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCommercialname(String value) {
            this.commercialname = value;
        }

        /**
         * Ruft den Wert der alias-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAlias() {
            return alias;
        }

        /**
         * Legt den Wert der alias-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAlias(String value) {
            this.alias = value;
        }

        /**
         * Ruft den Wert der street-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreet() {
            return street;
        }

        /**
         * Legt den Wert der street-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreet(String value) {
            this.street = value;
        }

        /**
         * Ruft den Wert der housenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getHousenumber() {
            return housenumber;
        }

        /**
         * Legt den Wert der housenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setHousenumber(Integer value) {
            this.housenumber = value;
        }

        /**
         * Ruft den Wert der housenumberaffix-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHousenumberaffix() {
            return housenumberaffix;
        }

        /**
         * Legt den Wert der housenumberaffix-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHousenumberaffix(String value) {
            this.housenumberaffix = value;
        }

        /**
         * Ruft den Wert der postcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostcode() {
            return postcode;
        }

        /**
         * Legt den Wert der postcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostcode(String value) {
            this.postcode = value;
        }

        /**
         * Ruft den Wert der city-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Legt den Wert der city-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Ruft den Wert der quarter-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuarter() {
            return quarter;
        }

        /**
         * Legt den Wert der quarter-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuarter(String value) {
            this.quarter = value;
        }

        /**
         * Ruft den Wert der region-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegion() {
            return region;
        }

        /**
         * Legt den Wert der region-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegion(String value) {
            this.region = value;
        }

        /**
         * Ruft den Wert der country-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getCountry() {
            return country;
        }

        /**
         * Legt den Wert der country-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setCountry(Tkey value) {
            this.country = value;
        }

        /**
         * Ruft den Wert der legalform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public Tkeywithshortdesignation getLegalform() {
            return legalform;
        }

        /**
         * Legt den Wert der legalform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public void setLegalform(Tkeywithshortdesignation value) {
            this.legalform = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "title",
        "salutation",
        "surname",
        "firstname",
        "birthname",
        "nameaffix",
        "surnamewidow",
        "surnamebeforedivorce",
        "alias",
        "street",
        "housenumber",
        "housenumberaffix",
        "postcode",
        "city",
        "quarter",
        "region",
        "country"
    })
    public static class Privateperson {

        protected Tkeywithshortdesignation title;
        protected Tkey salutation;
        protected String surname;
        protected List<String> firstname;
        protected String birthname;
        protected String nameaffix;
        protected String surnamewidow;
        protected String surnamebeforedivorce;
        protected String alias;
        protected String street;
        protected Integer housenumber;
        protected String housenumberaffix;
        protected String postcode;
        protected String city;
        protected String quarter;
        protected String region;
        protected Tkey country;

        /**
         * Ruft den Wert der title-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public Tkeywithshortdesignation getTitle() {
            return title;
        }

        /**
         * Legt den Wert der title-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public void setTitle(Tkeywithshortdesignation value) {
            this.title = value;
        }

        /**
         * Ruft den Wert der salutation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getSalutation() {
            return salutation;
        }

        /**
         * Legt den Wert der salutation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setSalutation(Tkey value) {
            this.salutation = value;
        }

        /**
         * Ruft den Wert der surname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurname() {
            return surname;
        }

        /**
         * Legt den Wert der surname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurname(String value) {
            this.surname = value;
        }

        /**
         * Gets the value of the firstname property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the firstname property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFirstname().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getFirstname() {
            if (firstname == null) {
                firstname = new ArrayList<String>();
            }
            return this.firstname;
        }

        /**
         * Ruft den Wert der birthname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBirthname() {
            return birthname;
        }

        /**
         * Legt den Wert der birthname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBirthname(String value) {
            this.birthname = value;
        }

        /**
         * Ruft den Wert der nameaffix-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNameaffix() {
            return nameaffix;
        }

        /**
         * Legt den Wert der nameaffix-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNameaffix(String value) {
            this.nameaffix = value;
        }

        /**
         * Ruft den Wert der surnamewidow-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurnamewidow() {
            return surnamewidow;
        }

        /**
         * Legt den Wert der surnamewidow-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurnamewidow(String value) {
            this.surnamewidow = value;
        }

        /**
         * Ruft den Wert der surnamebeforedivorce-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurnamebeforedivorce() {
            return surnamebeforedivorce;
        }

        /**
         * Legt den Wert der surnamebeforedivorce-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurnamebeforedivorce(String value) {
            this.surnamebeforedivorce = value;
        }

        /**
         * Ruft den Wert der alias-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAlias() {
            return alias;
        }

        /**
         * Legt den Wert der alias-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAlias(String value) {
            this.alias = value;
        }

        /**
         * Ruft den Wert der street-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreet() {
            return street;
        }

        /**
         * Legt den Wert der street-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreet(String value) {
            this.street = value;
        }

        /**
         * Ruft den Wert der housenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getHousenumber() {
            return housenumber;
        }

        /**
         * Legt den Wert der housenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setHousenumber(Integer value) {
            this.housenumber = value;
        }

        /**
         * Ruft den Wert der housenumberaffix-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHousenumberaffix() {
            return housenumberaffix;
        }

        /**
         * Legt den Wert der housenumberaffix-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHousenumberaffix(String value) {
            this.housenumberaffix = value;
        }

        /**
         * Ruft den Wert der postcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostcode() {
            return postcode;
        }

        /**
         * Legt den Wert der postcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostcode(String value) {
            this.postcode = value;
        }

        /**
         * Ruft den Wert der city-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Legt den Wert der city-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Ruft den Wert der quarter-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuarter() {
            return quarter;
        }

        /**
         * Legt den Wert der quarter-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuarter(String value) {
            this.quarter = value;
        }

        /**
         * Ruft den Wert der region-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegion() {
            return region;
        }

        /**
         * Legt den Wert der region-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegion(String value) {
            this.region = value;
        }

        /**
         * Ruft den Wert der country-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getCountry() {
            return country;
        }

        /**
         * Legt den Wert der country-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setCountry(Tkey value) {
            this.country = value;
        }

    }

}
