
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipationdataactiveformer complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipationdataactiveformer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="active" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationdataparticipations" minOccurs="0"/>
 *         &lt;element name="former" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationdataparticipations" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipationdataactiveformer", propOrder = {
    "active",
    "former"
})
public class Tparticipationdataactiveformer {

    protected Tparticipationdataparticipations active;
    protected Tparticipationdataparticipations former;

    /**
     * Ruft den Wert der active-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tparticipationdataparticipations }
     *     
     */
    public Tparticipationdataparticipations getActive() {
        return active;
    }

    /**
     * Legt den Wert der active-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tparticipationdataparticipations }
     *     
     */
    public void setActive(Tparticipationdataparticipations value) {
        this.active = value;
    }

    /**
     * Ruft den Wert der former-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tparticipationdataparticipations }
     *     
     */
    public Tparticipationdataparticipations getFormer() {
        return former;
    }

    /**
     * Legt den Wert der former-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tparticipationdataparticipations }
     *     
     */
    public void setFormer(Tparticipationdataparticipations value) {
        this.former = value;
    }

}
