
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tallowedkeys complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tallowedkeys">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="keyname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="label" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="keyconstraint" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="keycontent" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation"/>
 *                   &lt;element name="parameters" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparameterlist" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tallowedkeys", propOrder = {
    "keyname",
    "label",
    "keyconstraint"
})
public class Tallowedkeys {

    @XmlElement(required = true)
    protected String keyname;
    protected String label;
    protected List<Tallowedkeys.Keyconstraint> keyconstraint;

    /**
     * Ruft den Wert der keyname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKeyname() {
        return keyname;
    }

    /**
     * Legt den Wert der keyname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKeyname(String value) {
        this.keyname = value;
    }

    /**
     * Ruft den Wert der label-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLabel() {
        return label;
    }

    /**
     * Legt den Wert der label-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLabel(String value) {
        this.label = value;
    }

    /**
     * Gets the value of the keyconstraint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the keyconstraint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getKeyconstraint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tallowedkeys.Keyconstraint }
     * 
     * 
     */
    public List<Tallowedkeys.Keyconstraint> getKeyconstraint() {
        if (keyconstraint == null) {
            keyconstraint = new ArrayList<Tallowedkeys.Keyconstraint>();
        }
        return this.keyconstraint;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="keycontent" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation"/>
     *         &lt;element name="parameters" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparameterlist" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "keycontent",
        "parameters"
    })
    public static class Keyconstraint {

        @XmlElement(required = true)
        protected Tkeywithshortdesignation keycontent;
        protected List<Tparameterlist> parameters;

        /**
         * Ruft den Wert der keycontent-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public Tkeywithshortdesignation getKeycontent() {
            return keycontent;
        }

        /**
         * Legt den Wert der keycontent-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public void setKeycontent(Tkeywithshortdesignation value) {
            this.keycontent = value;
        }

        /**
         * Gets the value of the parameters property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the parameters property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getParameters().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tparameterlist }
         * 
         * 
         */
        public List<Tparameterlist> getParameters() {
            if (parameters == null) {
                parameters = new ArrayList<Tparameterlist>();
            }
            return this.parameters;
        }

    }

}
