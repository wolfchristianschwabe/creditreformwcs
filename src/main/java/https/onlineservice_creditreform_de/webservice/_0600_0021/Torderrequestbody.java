
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Torderrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Torderrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeyproduct"/>
 *         &lt;element name="ordertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeyordertype"/>
 *         &lt;element name="legitimateinterest" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeylegitimateinterest"/>
 *         &lt;element name="reportlanguage" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlanguagerequest"/>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *         &lt;choice>
 *           &lt;element name="company">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="companyname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcompanyname"/>
 *                     &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeylegalform" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="privateperson">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="surname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsurname"/>
 *                     &lt;element name="firstname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfirstname" minOccurs="0"/>
 *                     &lt;element name="dateofbirth" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}requestaddressstructure"/>
 *         &lt;sequence minOccurs="0">
 *           &lt;element name="creditenquiryamount">
 *             &lt;simpleType>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *                 &lt;minInclusive value="0"/>
 *                 &lt;maxInclusive value="9999999999999999"/>
 *                 &lt;fractionDigits value="0"/>
 *               &lt;/restriction>
 *             &lt;/simpleType>
 *           &lt;/element>
 *           &lt;element name="creditenquirycurrency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcurrency"/>
 *         &lt;/sequence>
 *         &lt;element name="orderspecifyingtext" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="364"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}requestmonitoringstructure" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Torderrequestbody", propOrder = {
    "producttype",
    "ordertype",
    "legitimateinterest",
    "reportlanguage",
    "customerreference",
    "identificationnumber",
    "company",
    "privateperson",
    "street",
    "housenumber",
    "housenumberaffix",
    "postcode",
    "city",
    "region",
    "country",
    "creditenquiryamount",
    "creditenquirycurrency",
    "orderspecifyingtext",
    "extendedmonitoring"
})
public class Torderrequestbody {

    @XmlElement(required = true)
    protected String producttype;
    @XmlElement(required = true)
    protected String ordertype;
    @XmlElement(required = true)
    protected String legitimateinterest;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected Tlanguagerequest reportlanguage;
    protected String customerreference;
    protected String identificationnumber;
    protected Torderrequestbody.Company company;
    protected Torderrequestbody.Privateperson privateperson;
    protected String street;
    @XmlSchemaType(name = "integer")
    protected Integer housenumber;
    protected String housenumberaffix;
    protected String postcode;
    protected String city;
    protected String region;
    @XmlElement(required = true)
    protected String country;
    protected BigDecimal creditenquiryamount;
    protected String creditenquirycurrency;
    protected String orderspecifyingtext;
    protected https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody.Extendedmonitoring extendedmonitoring;

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducttype(String value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der ordertype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrdertype() {
        return ordertype;
    }

    /**
     * Legt den Wert der ordertype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrdertype(String value) {
        this.ordertype = value;
    }

    /**
     * Ruft den Wert der legitimateinterest-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegitimateinterest() {
        return legitimateinterest;
    }

    /**
     * Legt den Wert der legitimateinterest-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegitimateinterest(String value) {
        this.legitimateinterest = value;
    }

    /**
     * Ruft den Wert der reportlanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tlanguagerequest }
     *     
     */
    public Tlanguagerequest getReportlanguage() {
        return reportlanguage;
    }

    /**
     * Legt den Wert der reportlanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tlanguagerequest }
     *     
     */
    public void setReportlanguage(Tlanguagerequest value) {
        this.reportlanguage = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der company-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Torderrequestbody.Company }
     *     
     */
    public Torderrequestbody.Company getCompany() {
        return company;
    }

    /**
     * Legt den Wert der company-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Torderrequestbody.Company }
     *     
     */
    public void setCompany(Torderrequestbody.Company value) {
        this.company = value;
    }

    /**
     * Ruft den Wert der privateperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Torderrequestbody.Privateperson }
     *     
     */
    public Torderrequestbody.Privateperson getPrivateperson() {
        return privateperson;
    }

    /**
     * Legt den Wert der privateperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Torderrequestbody.Privateperson }
     *     
     */
    public void setPrivateperson(Torderrequestbody.Privateperson value) {
        this.privateperson = value;
    }

    /**
     * Ruft den Wert der street-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Legt den Wert der street-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Ruft den Wert der housenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHousenumber() {
        return housenumber;
    }

    /**
     * Legt den Wert der housenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHousenumber(Integer value) {
        this.housenumber = value;
    }

    /**
     * Ruft den Wert der housenumberaffix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHousenumberaffix() {
        return housenumberaffix;
    }

    /**
     * Legt den Wert der housenumberaffix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHousenumberaffix(String value) {
        this.housenumberaffix = value;
    }

    /**
     * Ruft den Wert der postcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Legt den Wert der postcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostcode(String value) {
        this.postcode = value;
    }

    /**
     * Ruft den Wert der city-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Legt den Wert der city-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Ruft den Wert der region-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Legt den Wert der region-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Ruft den Wert der country-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Legt den Wert der country-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Ruft den Wert der creditenquiryamount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCreditenquiryamount() {
        return creditenquiryamount;
    }

    /**
     * Legt den Wert der creditenquiryamount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCreditenquiryamount(BigDecimal value) {
        this.creditenquiryamount = value;
    }

    /**
     * Ruft den Wert der creditenquirycurrency-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditenquirycurrency() {
        return creditenquirycurrency;
    }

    /**
     * Legt den Wert der creditenquirycurrency-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditenquirycurrency(String value) {
        this.creditenquirycurrency = value;
    }

    /**
     * Ruft den Wert der orderspecifyingtext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOrderspecifyingtext() {
        return orderspecifyingtext;
    }

    /**
     * Legt den Wert der orderspecifyingtext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOrderspecifyingtext(String value) {
        this.orderspecifyingtext = value;
    }

    /**
     * Ruft den Wert der extendedmonitoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody.Extendedmonitoring }
     *     
     */
    public https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody.Extendedmonitoring getExtendedmonitoring() {
        return extendedmonitoring;
    }

    /**
     * Legt den Wert der extendedmonitoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody.Extendedmonitoring }
     *     
     */
    public void setExtendedmonitoring(https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody.Extendedmonitoring value) {
        this.extendedmonitoring = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="companyname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcompanyname"/>
     *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeylegalform" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "companyname",
        "legalform"
    })
    public static class Company {

        @XmlElement(required = true)
        protected String companyname;
        protected String legalform;

        /**
         * Ruft den Wert der companyname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyname() {
            return companyname;
        }

        /**
         * Legt den Wert der companyname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyname(String value) {
            this.companyname = value;
        }

        /**
         * Ruft den Wert der legalform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLegalform() {
            return legalform;
        }

        /**
         * Legt den Wert der legalform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLegalform(String value) {
            this.legalform = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="surname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsurname"/>
     *         &lt;element name="firstname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfirstname" minOccurs="0"/>
     *         &lt;element name="dateofbirth" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "surname",
        "firstname",
        "dateofbirth"
    })
    public static class Privateperson {

        @XmlElement(required = true)
        protected String surname;
        protected String firstname;
        protected String dateofbirth;

        /**
         * Ruft den Wert der surname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurname() {
            return surname;
        }

        /**
         * Legt den Wert der surname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurname(String value) {
            this.surname = value;
        }

        /**
         * Ruft den Wert der firstname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFirstname() {
            return firstname;
        }

        /**
         * Legt den Wert der firstname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFirstname(String value) {
            this.firstname = value;
        }

        /**
         * Ruft den Wert der dateofbirth-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDateofbirth() {
            return dateofbirth;
        }

        /**
         * Legt den Wert der dateofbirth-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDateofbirth(String value) {
            this.dateofbirth = value;
        }

    }

}
