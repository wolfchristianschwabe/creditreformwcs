
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tparticipationdataresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipationdataresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}participationdatareference"/>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *         &lt;element name="creationtime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="reportlanguage" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlanguage" minOccurs="0"/>
 *         &lt;element name="provider" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="identification" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationdataperson"/>
 *         &lt;element name="participants" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationdata" minOccurs="0"/>
 *         &lt;element name="participations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationdata" minOccurs="0"/>
 *         &lt;element name="legaldisclaimer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="textreport" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipationdataresponsebody", propOrder = {
    "participationdatareference",
    "customerreference",
    "producttype",
    "creationtime",
    "reportlanguage",
    "provider",
    "identification",
    "participants",
    "participations",
    "legaldisclaimer",
    "textreport"
})
public class Tparticipationdataresponsebody {

    protected long participationdatareference;
    protected String customerreference;
    @XmlElement(required = true)
    protected Tkey producttype;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationtime;
    protected String reportlanguage;
    protected String provider;
    @XmlElement(required = true)
    protected Tparticipationdataperson identification;
    protected Tparticipationdata participants;
    protected Tparticipationdata participations;
    protected String legaldisclaimer;
    @XmlMimeType("application/octet-stream")
    protected DataHandler textreport;

    /**
     * Ruft den Wert der participationdatareference-Eigenschaft ab.
     * 
     */
    public long getParticipationdatareference() {
        return participationdatareference;
    }

    /**
     * Legt den Wert der participationdatareference-Eigenschaft fest.
     * 
     */
    public void setParticipationdatareference(long value) {
        this.participationdatareference = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setProducttype(Tkey value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der creationtime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationtime() {
        return creationtime;
    }

    /**
     * Legt den Wert der creationtime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationtime(XMLGregorianCalendar value) {
        this.creationtime = value;
    }

    /**
     * Ruft den Wert der reportlanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportlanguage() {
        return reportlanguage;
    }

    /**
     * Legt den Wert der reportlanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportlanguage(String value) {
        this.reportlanguage = value;
    }

    /**
     * Ruft den Wert der provider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvider() {
        return provider;
    }

    /**
     * Legt den Wert der provider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvider(String value) {
        this.provider = value;
    }

    /**
     * Ruft den Wert der identification-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tparticipationdataperson }
     *     
     */
    public Tparticipationdataperson getIdentification() {
        return identification;
    }

    /**
     * Legt den Wert der identification-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tparticipationdataperson }
     *     
     */
    public void setIdentification(Tparticipationdataperson value) {
        this.identification = value;
    }

    /**
     * Ruft den Wert der participants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tparticipationdata }
     *     
     */
    public Tparticipationdata getParticipants() {
        return participants;
    }

    /**
     * Legt den Wert der participants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tparticipationdata }
     *     
     */
    public void setParticipants(Tparticipationdata value) {
        this.participants = value;
    }

    /**
     * Ruft den Wert der participations-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tparticipationdata }
     *     
     */
    public Tparticipationdata getParticipations() {
        return participations;
    }

    /**
     * Legt den Wert der participations-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tparticipationdata }
     *     
     */
    public void setParticipations(Tparticipationdata value) {
        this.participations = value;
    }

    /**
     * Ruft den Wert der legaldisclaimer-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegaldisclaimer() {
        return legaldisclaimer;
    }

    /**
     * Legt den Wert der legaldisclaimer-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegaldisclaimer(String value) {
        this.legaldisclaimer = value;
    }

    /**
     * Ruft den Wert der textreport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getTextreport() {
        return textreport;
    }

    /**
     * Legt den Wert der textreport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setTextreport(DataHandler value) {
        this.textreport = value;
    }

}
