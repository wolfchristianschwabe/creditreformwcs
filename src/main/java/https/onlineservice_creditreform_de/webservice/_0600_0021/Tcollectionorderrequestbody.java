
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcollectionorderrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionorderrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="collectionordertype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="debtor" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestdebtor"/>
 *         &lt;element name="receivable" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestreceivable"/>
 *         &lt;element name="partreceivable" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestpartreceivable" maxOccurs="unbounded"/>
 *         &lt;element name="costturnover" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestcostturnover" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="paymentturnover" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestpaymentturnover" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionorderrequestbody", propOrder = {
    "collectionordertype",
    "debtor",
    "receivable",
    "partreceivable",
    "costturnover",
    "paymentturnover",
    "user"
})
public class Tcollectionorderrequestbody {

    @XmlElement(required = true)
    protected String collectionordertype;
    @XmlElement(required = true)
    protected Tcollectionrequestdebtor debtor;
    @XmlElement(required = true)
    protected Tcollectionrequestreceivable receivable;
    @XmlElement(required = true)
    protected List<Tcollectionrequestpartreceivable> partreceivable;
    protected List<Tcollectionrequestcostturnover> costturnover;
    protected List<Tcollectionrequestpaymentturnover> paymentturnover;
    protected String user;

    /**
     * Ruft den Wert der collectionordertype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectionordertype() {
        return collectionordertype;
    }

    /**
     * Legt den Wert der collectionordertype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionordertype(String value) {
        this.collectionordertype = value;
    }

    /**
     * Ruft den Wert der debtor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestdebtor }
     *     
     */
    public Tcollectionrequestdebtor getDebtor() {
        return debtor;
    }

    /**
     * Legt den Wert der debtor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestdebtor }
     *     
     */
    public void setDebtor(Tcollectionrequestdebtor value) {
        this.debtor = value;
    }

    /**
     * Ruft den Wert der receivable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestreceivable }
     *     
     */
    public Tcollectionrequestreceivable getReceivable() {
        return receivable;
    }

    /**
     * Legt den Wert der receivable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestreceivable }
     *     
     */
    public void setReceivable(Tcollectionrequestreceivable value) {
        this.receivable = value;
    }

    /**
     * Gets the value of the partreceivable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partreceivable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartreceivable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcollectionrequestpartreceivable }
     * 
     * 
     */
    public List<Tcollectionrequestpartreceivable> getPartreceivable() {
        if (partreceivable == null) {
            partreceivable = new ArrayList<Tcollectionrequestpartreceivable>();
        }
        return this.partreceivable;
    }

    /**
     * Gets the value of the costturnover property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the costturnover property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCostturnover().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcollectionrequestcostturnover }
     * 
     * 
     */
    public List<Tcollectionrequestcostturnover> getCostturnover() {
        if (costturnover == null) {
            costturnover = new ArrayList<Tcollectionrequestcostturnover>();
        }
        return this.costturnover;
    }

    /**
     * Gets the value of the paymentturnover property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentturnover property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentturnover().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcollectionrequestpaymentturnover }
     * 
     * 
     */
    public List<Tcollectionrequestpaymentturnover> getPaymentturnover() {
        if (paymentturnover == null) {
            paymentturnover = new ArrayList<Tcollectionrequestpaymentturnover>();
        }
        return this.paymentturnover;
    }

    /**
     * Ruft den Wert der user-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUser() {
        return user;
    }

    /**
     * Legt den Wert der user-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUser(String value) {
        this.user = value;
    }

}
