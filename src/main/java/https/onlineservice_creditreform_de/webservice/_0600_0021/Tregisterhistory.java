
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tregisterhistory complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tregisterhistory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="historyentry" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="date" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *                   &lt;element name="reason" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;choice minOccurs="0">
 *                     &lt;element name="changeoflegalform">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *                               &lt;element name="registertext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="contract">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="contracttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                               &lt;element name="contractpartner" maxOccurs="unbounded" minOccurs="0">
 *                                 &lt;complexType>
 *                                   &lt;complexContent>
 *                                     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                       &lt;sequence>
 *                                         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *                                         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *                                         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}namestructure"/>
 *                                         &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}status" minOccurs="0"/>
 *                                       &lt;/sequence>
 *                                     &lt;/restriction>
 *                                   &lt;/complexContent>
 *                                 &lt;/complexType>
 *                               &lt;/element>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="changeofcompanybase">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="addressrange" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                               &lt;element name="companybase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="changeofname">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="registration">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="registertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *                               &lt;element name="registerid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                               &lt;element name="register" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcourt" minOccurs="0"/>
 *                               &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="change">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;sequence>
 *                               &lt;element name="explanation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                             &lt;/sequence>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/choice>
 *                   &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tregisterhistory", propOrder = {
    "historyentry",
    "text"
})
public class Tregisterhistory {

    protected List<Tregisterhistory.Historyentry> historyentry;
    protected Ttext text;

    /**
     * Gets the value of the historyentry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the historyentry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHistoryentry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tregisterhistory.Historyentry }
     * 
     * 
     */
    public List<Tregisterhistory.Historyentry> getHistoryentry() {
        if (historyentry == null) {
            historyentry = new ArrayList<Tregisterhistory.Historyentry>();
        }
        return this.historyentry;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="date" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
     *         &lt;element name="reason" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;choice minOccurs="0">
     *           &lt;element name="changeoflegalform">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
     *                     &lt;element name="registertext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="contract">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="contracttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                     &lt;element name="contractpartner" maxOccurs="unbounded" minOccurs="0">
     *                       &lt;complexType>
     *                         &lt;complexContent>
     *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                             &lt;sequence>
     *                               &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
     *                               &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
     *                               &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}namestructure"/>
     *                               &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                               &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}status" minOccurs="0"/>
     *                             &lt;/sequence>
     *                           &lt;/restriction>
     *                         &lt;/complexContent>
     *                       &lt;/complexType>
     *                     &lt;/element>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="changeofcompanybase">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="addressrange" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                     &lt;element name="companybase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="changeofname">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="registration">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="registertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
     *                     &lt;element name="registerid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                     &lt;element name="register" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcourt" minOccurs="0"/>
     *                     &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="change">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;sequence>
     *                     &lt;element name="explanation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *                   &lt;/sequence>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *         &lt;/choice>
     *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "date",
        "reason",
        "changeoflegalform",
        "contract",
        "changeofcompanybase",
        "changeofname",
        "registration",
        "change",
        "text"
    })
    public static class Historyentry {

        protected String date;
        protected Tkey reason;
        protected Tregisterhistory.Historyentry.Changeoflegalform changeoflegalform;
        protected Tregisterhistory.Historyentry.Contract contract;
        protected Tregisterhistory.Historyentry.Changeofcompanybase changeofcompanybase;
        protected Tregisterhistory.Historyentry.Changeofname changeofname;
        protected Tregisterhistory.Historyentry.Registration registration;
        protected Tregisterhistory.Historyentry.Change change;
        protected Ttext text;

        /**
         * Ruft den Wert der date-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDate() {
            return date;
        }

        /**
         * Legt den Wert der date-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDate(String value) {
            this.date = value;
        }

        /**
         * Ruft den Wert der reason-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getReason() {
            return reason;
        }

        /**
         * Legt den Wert der reason-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setReason(Tkey value) {
            this.reason = value;
        }

        /**
         * Ruft den Wert der changeoflegalform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tregisterhistory.Historyentry.Changeoflegalform }
         *     
         */
        public Tregisterhistory.Historyentry.Changeoflegalform getChangeoflegalform() {
            return changeoflegalform;
        }

        /**
         * Legt den Wert der changeoflegalform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tregisterhistory.Historyentry.Changeoflegalform }
         *     
         */
        public void setChangeoflegalform(Tregisterhistory.Historyentry.Changeoflegalform value) {
            this.changeoflegalform = value;
        }

        /**
         * Ruft den Wert der contract-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tregisterhistory.Historyentry.Contract }
         *     
         */
        public Tregisterhistory.Historyentry.Contract getContract() {
            return contract;
        }

        /**
         * Legt den Wert der contract-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tregisterhistory.Historyentry.Contract }
         *     
         */
        public void setContract(Tregisterhistory.Historyentry.Contract value) {
            this.contract = value;
        }

        /**
         * Ruft den Wert der changeofcompanybase-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tregisterhistory.Historyentry.Changeofcompanybase }
         *     
         */
        public Tregisterhistory.Historyentry.Changeofcompanybase getChangeofcompanybase() {
            return changeofcompanybase;
        }

        /**
         * Legt den Wert der changeofcompanybase-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tregisterhistory.Historyentry.Changeofcompanybase }
         *     
         */
        public void setChangeofcompanybase(Tregisterhistory.Historyentry.Changeofcompanybase value) {
            this.changeofcompanybase = value;
        }

        /**
         * Ruft den Wert der changeofname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tregisterhistory.Historyentry.Changeofname }
         *     
         */
        public Tregisterhistory.Historyentry.Changeofname getChangeofname() {
            return changeofname;
        }

        /**
         * Legt den Wert der changeofname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tregisterhistory.Historyentry.Changeofname }
         *     
         */
        public void setChangeofname(Tregisterhistory.Historyentry.Changeofname value) {
            this.changeofname = value;
        }

        /**
         * Ruft den Wert der registration-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tregisterhistory.Historyentry.Registration }
         *     
         */
        public Tregisterhistory.Historyentry.Registration getRegistration() {
            return registration;
        }

        /**
         * Legt den Wert der registration-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tregisterhistory.Historyentry.Registration }
         *     
         */
        public void setRegistration(Tregisterhistory.Historyentry.Registration value) {
            this.registration = value;
        }

        /**
         * Ruft den Wert der change-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tregisterhistory.Historyentry.Change }
         *     
         */
        public Tregisterhistory.Historyentry.Change getChange() {
            return change;
        }

        /**
         * Legt den Wert der change-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tregisterhistory.Historyentry.Change }
         *     
         */
        public void setChange(Tregisterhistory.Historyentry.Change value) {
            this.change = value;
        }

        /**
         * Ruft den Wert der text-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getText() {
            return text;
        }

        /**
         * Legt den Wert der text-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setText(Ttext value) {
            this.text = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="explanation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "explanation"
        })
        public static class Change {

            protected Ttext explanation;

            /**
             * Ruft den Wert der explanation-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Ttext }
             *     
             */
            public Ttext getExplanation() {
                return explanation;
            }

            /**
             * Legt den Wert der explanation-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Ttext }
             *     
             */
            public void setExplanation(Ttext value) {
                this.explanation = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="addressrange" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="companybase" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "addressrange",
            "companybase"
        })
        public static class Changeofcompanybase {

            protected Tkey addressrange;
            protected String companybase;

            /**
             * Ruft den Wert der addressrange-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getAddressrange() {
                return addressrange;
            }

            /**
             * Legt den Wert der addressrange-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setAddressrange(Tkey value) {
                this.addressrange = value;
            }

            /**
             * Ruft den Wert der companybase-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCompanybase() {
                return companybase;
            }

            /**
             * Legt den Wert der companybase-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCompanybase(String value) {
                this.companybase = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
         *         &lt;element name="registertext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "legalform",
            "registertext"
        })
        public static class Changeoflegalform {

            protected Tkeywithshortdesignation legalform;
            protected String registertext;

            /**
             * Ruft den Wert der legalform-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public Tkeywithshortdesignation getLegalform() {
                return legalform;
            }

            /**
             * Legt den Wert der legalform-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public void setLegalform(Tkeywithshortdesignation value) {
                this.legalform = value;
            }

            /**
             * Ruft den Wert der registertext-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegistertext() {
                return registertext;
            }

            /**
             * Legt den Wert der registertext-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegistertext(String value) {
                this.registertext = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "companyname"
        })
        public static class Changeofname {

            protected String companyname;

            /**
             * Ruft den Wert der companyname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCompanyname() {
                return companyname;
            }

            /**
             * Legt den Wert der companyname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCompanyname(String value) {
                this.companyname = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="contracttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="contractpartner" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
         *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
         *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}namestructure"/>
         *                   &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}status" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "contracttype",
            "contractpartner"
        })
        public static class Contract {

            protected Tkey contracttype;
            protected List<Tregisterhistory.Historyentry.Contract.Contractpartner> contractpartner;

            /**
             * Ruft den Wert der contracttype-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getContracttype() {
                return contracttype;
            }

            /**
             * Legt den Wert der contracttype-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setContracttype(Tkey value) {
                this.contracttype = value;
            }

            /**
             * Gets the value of the contractpartner property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the contractpartner property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getContractpartner().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Tregisterhistory.Historyentry.Contract.Contractpartner }
             * 
             * 
             */
            public List<Tregisterhistory.Historyentry.Contract.Contractpartner> getContractpartner() {
                if (contractpartner == null) {
                    contractpartner = new ArrayList<Tregisterhistory.Historyentry.Contract.Contractpartner>();
                }
                return this.contractpartner;
            }


            /**
             * <p>Java-Klasse f�r anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
             *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
             *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}namestructure"/>
             *         &lt;element name="location" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
             *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}status" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "identificationnumber",
                "easynumber",
                "salutation",
                "privateperson",
                "company",
                "location",
                "status"
            })
            public static class Contractpartner {

                protected String identificationnumber;
                protected String easynumber;
                protected Tkey salutation;
                protected https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Privateperson privateperson;
                protected https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Company company;
                protected String location;
                protected Tkeywithdescription status;

                /**
                 * Ruft den Wert der identificationnumber-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getIdentificationnumber() {
                    return identificationnumber;
                }

                /**
                 * Legt den Wert der identificationnumber-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setIdentificationnumber(String value) {
                    this.identificationnumber = value;
                }

                /**
                 * Ruft den Wert der easynumber-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getEasynumber() {
                    return easynumber;
                }

                /**
                 * Legt den Wert der easynumber-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setEasynumber(String value) {
                    this.easynumber = value;
                }

                /**
                 * Ruft den Wert der salutation-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Tkey }
                 *     
                 */
                public Tkey getSalutation() {
                    return salutation;
                }

                /**
                 * Legt den Wert der salutation-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Tkey }
                 *     
                 */
                public void setSalutation(Tkey value) {
                    this.salutation = value;
                }

                /**
                 * Ruft den Wert der privateperson-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Privateperson }
                 *     
                 */
                public https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Privateperson getPrivateperson() {
                    return privateperson;
                }

                /**
                 * Legt den Wert der privateperson-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Privateperson }
                 *     
                 */
                public void setPrivateperson(https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Privateperson value) {
                    this.privateperson = value;
                }

                /**
                 * Ruft den Wert der company-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Company }
                 *     
                 */
                public https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Company getCompany() {
                    return company;
                }

                /**
                 * Legt den Wert der company-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Company }
                 *     
                 */
                public void setCompany(https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Company value) {
                    this.company = value;
                }

                /**
                 * Ruft den Wert der location-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getLocation() {
                    return location;
                }

                /**
                 * Legt den Wert der location-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setLocation(String value) {
                    this.location = value;
                }

                /**
                 * Ruft den Wert der status-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Tkeywithdescription }
                 *     
                 */
                public Tkeywithdescription getStatus() {
                    return status;
                }

                /**
                 * Legt den Wert der status-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Tkeywithdescription }
                 *     
                 */
                public void setStatus(Tkeywithdescription value) {
                    this.status = value;
                }

            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="registertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
         *         &lt;element name="registerid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="register" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcourt" minOccurs="0"/>
         *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "registertype",
            "registerid",
            "register",
            "legalform"
        })
        public static class Registration {

            protected Tkeywithshortdesignation registertype;
            protected String registerid;
            protected Tcourt register;
            protected Tkeywithshortdesignation legalform;

            /**
             * Ruft den Wert der registertype-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public Tkeywithshortdesignation getRegistertype() {
                return registertype;
            }

            /**
             * Legt den Wert der registertype-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public void setRegistertype(Tkeywithshortdesignation value) {
                this.registertype = value;
            }

            /**
             * Ruft den Wert der registerid-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegisterid() {
                return registerid;
            }

            /**
             * Legt den Wert der registerid-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegisterid(String value) {
                this.registerid = value;
            }

            /**
             * Ruft den Wert der register-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tcourt }
             *     
             */
            public Tcourt getRegister() {
                return register;
            }

            /**
             * Legt den Wert der register-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tcourt }
             *     
             */
            public void setRegister(Tcourt value) {
                this.register = value;
            }

            /**
             * Ruft den Wert der legalform-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public Tkeywithshortdesignation getLegalform() {
                return legalform;
            }

            /**
             * Legt den Wert der legalform-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public void setLegalform(Tkeywithshortdesignation value) {
                this.legalform = value;
            }

        }

    }

}
