
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipationsparticipants complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipationsparticipants">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="furtherparticipants" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="participation" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipation">
 *                 &lt;sequence>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}namestructure"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipationsparticipants", propOrder = {
    "furtherparticipants",
    "participation",
    "text"
})
public class Tparticipationsparticipants {

    protected Boolean furtherparticipants;
    protected List<Tparticipationsparticipants.Participation> participation;
    protected Ttext text;

    /**
     * Ruft den Wert der furtherparticipants-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFurtherparticipants() {
        return furtherparticipants;
    }

    /**
     * Legt den Wert der furtherparticipants-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFurtherparticipants(Boolean value) {
        this.furtherparticipants = value;
    }

    /**
     * Gets the value of the participation property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the participation property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParticipation().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tparticipationsparticipants.Participation }
     * 
     * 
     */
    public List<Tparticipationsparticipants.Participation> getParticipation() {
        if (participation == null) {
            participation = new ArrayList<Tparticipationsparticipants.Participation>();
        }
        return this.participation;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipation">
     *       &lt;sequence>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
     *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}namestructure"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificationnumber",
        "easynumber",
        "salutation",
        "privateperson",
        "company"
    })
    public static class Participation
        extends Tparticipation
    {

        protected String identificationnumber;
        protected String easynumber;
        protected Tkey salutation;
        protected Tparticipationsparticipants.Participation.Privateperson privateperson;
        protected Tparticipationsparticipants.Participation.Company company;

        /**
         * Ruft den Wert der identificationnumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdentificationnumber() {
            return identificationnumber;
        }

        /**
         * Legt den Wert der identificationnumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdentificationnumber(String value) {
            this.identificationnumber = value;
        }

        /**
         * Ruft den Wert der easynumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEasynumber() {
            return easynumber;
        }

        /**
         * Legt den Wert der easynumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEasynumber(String value) {
            this.easynumber = value;
        }

        /**
         * Ruft den Wert der salutation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getSalutation() {
            return salutation;
        }

        /**
         * Legt den Wert der salutation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setSalutation(Tkey value) {
            this.salutation = value;
        }

        /**
         * Ruft den Wert der privateperson-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tparticipationsparticipants.Participation.Privateperson }
         *     
         */
        public Tparticipationsparticipants.Participation.Privateperson getPrivateperson() {
            return privateperson;
        }

        /**
         * Legt den Wert der privateperson-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tparticipationsparticipants.Participation.Privateperson }
         *     
         */
        public void setPrivateperson(Tparticipationsparticipants.Participation.Privateperson value) {
            this.privateperson = value;
        }

        /**
         * Ruft den Wert der company-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tparticipationsparticipants.Participation.Company }
         *     
         */
        public Tparticipationsparticipants.Participation.Company getCompany() {
            return company;
        }

        /**
         * Legt den Wert der company-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tparticipationsparticipants.Participation.Company }
         *     
         */
        public void setCompany(Tparticipationsparticipants.Participation.Company value) {
            this.company = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
         *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "companyname",
            "legalform",
            "text"
        })
        public static class Company {

            protected String companyname;
            protected Tkeywithshortdesignation legalform;
            protected Ttext text;

            /**
             * Ruft den Wert der companyname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCompanyname() {
                return companyname;
            }

            /**
             * Legt den Wert der companyname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCompanyname(String value) {
                this.companyname = value;
            }

            /**
             * Ruft den Wert der legalform-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public Tkeywithshortdesignation getLegalform() {
                return legalform;
            }

            /**
             * Legt den Wert der legalform-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public void setLegalform(Tkeywithshortdesignation value) {
                this.legalform = value;
            }

            /**
             * Ruft den Wert der text-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Ttext }
             *     
             */
            public Ttext getText() {
                return text;
            }

            /**
             * Legt den Wert der text-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Ttext }
             *     
             */
            public void setText(Ttext value) {
                this.text = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privatepersonstructure"/>
         *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "maritalstatus",
            "familystatus",
            "numberofchildren",
            "maritalpropertyregime",
            "title",
            "surname",
            "firstname",
            "birthname",
            "dateofbirth",
            "profession",
            "text"
        })
        public static class Privateperson {

            protected Tkey maritalstatus;
            protected Tkey familystatus;
            protected Integer numberofchildren;
            protected Tkey maritalpropertyregime;
            protected Tkeywithshortdesignation title;
            protected String surname;
            protected String firstname;
            protected String birthname;
            protected String dateofbirth;
            protected String profession;
            protected Ttext text;

            /**
             * Ruft den Wert der maritalstatus-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getMaritalstatus() {
                return maritalstatus;
            }

            /**
             * Legt den Wert der maritalstatus-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setMaritalstatus(Tkey value) {
                this.maritalstatus = value;
            }

            /**
             * Ruft den Wert der familystatus-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getFamilystatus() {
                return familystatus;
            }

            /**
             * Legt den Wert der familystatus-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setFamilystatus(Tkey value) {
                this.familystatus = value;
            }

            /**
             * Ruft den Wert der numberofchildren-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getNumberofchildren() {
                return numberofchildren;
            }

            /**
             * Legt den Wert der numberofchildren-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setNumberofchildren(Integer value) {
                this.numberofchildren = value;
            }

            /**
             * Ruft den Wert der maritalpropertyregime-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getMaritalpropertyregime() {
                return maritalpropertyregime;
            }

            /**
             * Legt den Wert der maritalpropertyregime-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setMaritalpropertyregime(Tkey value) {
                this.maritalpropertyregime = value;
            }

            /**
             * Ruft den Wert der title-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public Tkeywithshortdesignation getTitle() {
                return title;
            }

            /**
             * Legt den Wert der title-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public void setTitle(Tkeywithshortdesignation value) {
                this.title = value;
            }

            /**
             * Ruft den Wert der surname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSurname() {
                return surname;
            }

            /**
             * Legt den Wert der surname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSurname(String value) {
                this.surname = value;
            }

            /**
             * Ruft den Wert der firstname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFirstname() {
                return firstname;
            }

            /**
             * Legt den Wert der firstname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFirstname(String value) {
                this.firstname = value;
            }

            /**
             * Ruft den Wert der birthname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBirthname() {
                return birthname;
            }

            /**
             * Legt den Wert der birthname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBirthname(String value) {
                this.birthname = value;
            }

            /**
             * Ruft den Wert der dateofbirth-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDateofbirth() {
                return dateofbirth;
            }

            /**
             * Legt den Wert der dateofbirth-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDateofbirth(String value) {
                this.dateofbirth = value;
            }

            /**
             * Ruft den Wert der profession-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getProfession() {
                return profession;
            }

            /**
             * Legt den Wert der profession-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setProfession(String value) {
                this.profession = value;
            }

            /**
             * Ruft den Wert der text-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Ttext }
             *     
             */
            public Ttext getText() {
                return text;
            }

            /**
             * Legt den Wert der text-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Ttext }
             *     
             */
            public void setText(Ttext value) {
                this.text = value;
            }

        }

    }

}
