
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipationdataparticipation complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipationdataparticipation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="person" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationdataperson"/>
 *         &lt;element name="participationtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *         &lt;element name="participationtill" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;element name="sharepercent" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpercent" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipationdataparticipation", propOrder = {
    "person",
    "participationtype",
    "participationtill",
    "sharepercent"
})
public class Tparticipationdataparticipation {

    @XmlElement(required = true)
    protected Tparticipationdataperson person;
    @XmlElement(required = true)
    protected Tkey participationtype;
    protected String participationtill;
    protected BigDecimal sharepercent;

    /**
     * Ruft den Wert der person-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tparticipationdataperson }
     *     
     */
    public Tparticipationdataperson getPerson() {
        return person;
    }

    /**
     * Legt den Wert der person-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tparticipationdataperson }
     *     
     */
    public void setPerson(Tparticipationdataperson value) {
        this.person = value;
    }

    /**
     * Ruft den Wert der participationtype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getParticipationtype() {
        return participationtype;
    }

    /**
     * Legt den Wert der participationtype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setParticipationtype(Tkey value) {
        this.participationtype = value;
    }

    /**
     * Ruft den Wert der participationtill-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipationtill() {
        return participationtill;
    }

    /**
     * Legt den Wert der participationtill-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipationtill(String value) {
        this.participationtill = value;
    }

    /**
     * Ruft den Wert der sharepercent-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getSharepercent() {
        return sharepercent;
    }

    /**
     * Legt den Wert der sharepercent-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setSharepercent(BigDecimal value) {
        this.sharepercent = value;
    }

}
