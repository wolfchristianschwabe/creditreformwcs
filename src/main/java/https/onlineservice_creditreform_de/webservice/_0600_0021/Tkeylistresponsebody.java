
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tkeylistresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tkeylistresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="keylist">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/>
 *                   &lt;element name="keygroup" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="keygroupname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="keygrouptype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="keyentry" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="sinceversion" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/>
 *                                       &lt;element name="deprecatedsinceversion" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/>
 *                                       &lt;element name="keyattribute" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeyattribute" maxOccurs="unbounded" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tkeylistresponsebody", propOrder = {
    "keylist"
})
public class Tkeylistresponsebody {

    @XmlElement(required = true)
    protected Tkeylistresponsebody.Keylist keylist;

    /**
     * Ruft den Wert der keylist-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeylistresponsebody.Keylist }
     *     
     */
    public Tkeylistresponsebody.Keylist getKeylist() {
        return keylist;
    }

    /**
     * Legt den Wert der keylist-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeylistresponsebody.Keylist }
     *     
     */
    public void setKeylist(Tkeylistresponsebody.Keylist value) {
        this.keylist = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="version" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/>
     *         &lt;element name="keygroup" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="keygroupname" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="keygrouptype" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="keyentry" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="sinceversion" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/>
     *                             &lt;element name="deprecatedsinceversion" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/>
     *                             &lt;element name="keyattribute" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeyattribute" maxOccurs="unbounded" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "version",
        "keygroup"
    })
    public static class Keylist {

        @XmlElement(required = true)
        @XmlSchemaType(name = "positiveInteger")
        protected BigInteger version;
        protected List<Tkeylistresponsebody.Keylist.Keygroup> keygroup;

        /**
         * Ruft den Wert der version-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getVersion() {
            return version;
        }

        /**
         * Legt den Wert der version-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setVersion(BigInteger value) {
            this.version = value;
        }

        /**
         * Gets the value of the keygroup property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the keygroup property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getKeygroup().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tkeylistresponsebody.Keylist.Keygroup }
         * 
         * 
         */
        public List<Tkeylistresponsebody.Keylist.Keygroup> getKeygroup() {
            if (keygroup == null) {
                keygroup = new ArrayList<Tkeylistresponsebody.Keylist.Keygroup>();
            }
            return this.keygroup;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="keygroupname" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="keygrouptype" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="keyentry" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="sinceversion" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/>
         *                   &lt;element name="deprecatedsinceversion" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/>
         *                   &lt;element name="keyattribute" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeyattribute" maxOccurs="unbounded" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "keygroupname",
            "keygrouptype",
            "keyentry"
        })
        public static class Keygroup {

            @XmlElement(required = true)
            protected String keygroupname;
            @XmlElement(required = true)
            protected String keygrouptype;
            protected List<Tkeylistresponsebody.Keylist.Keygroup.Keyentry> keyentry;

            /**
             * Ruft den Wert der keygroupname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getKeygroupname() {
                return keygroupname;
            }

            /**
             * Legt den Wert der keygroupname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setKeygroupname(String value) {
                this.keygroupname = value;
            }

            /**
             * Ruft den Wert der keygrouptype-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getKeygrouptype() {
                return keygrouptype;
            }

            /**
             * Legt den Wert der keygrouptype-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setKeygrouptype(String value) {
                this.keygrouptype = value;
            }

            /**
             * Gets the value of the keyentry property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the keyentry property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getKeyentry().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Tkeylistresponsebody.Keylist.Keygroup.Keyentry }
             * 
             * 
             */
            public List<Tkeylistresponsebody.Keylist.Keygroup.Keyentry> getKeyentry() {
                if (keyentry == null) {
                    keyentry = new ArrayList<Tkeylistresponsebody.Keylist.Keygroup.Keyentry>();
                }
                return this.keyentry;
            }


            /**
             * <p>Java-Klasse f�r anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="sinceversion" type="{http://www.w3.org/2001/XMLSchema}positiveInteger"/>
             *         &lt;element name="deprecatedsinceversion" type="{http://www.w3.org/2001/XMLSchema}positiveInteger" minOccurs="0"/>
             *         &lt;element name="keyattribute" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeyattribute" maxOccurs="unbounded" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "sinceversion",
                "deprecatedsinceversion",
                "keyattribute"
            })
            public static class Keyentry {

                @XmlElement(required = true)
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger sinceversion;
                @XmlSchemaType(name = "positiveInteger")
                protected BigInteger deprecatedsinceversion;
                protected List<Tkeyattribute> keyattribute;

                /**
                 * Ruft den Wert der sinceversion-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getSinceversion() {
                    return sinceversion;
                }

                /**
                 * Legt den Wert der sinceversion-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setSinceversion(BigInteger value) {
                    this.sinceversion = value;
                }

                /**
                 * Ruft den Wert der deprecatedsinceversion-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDeprecatedsinceversion() {
                    return deprecatedsinceversion;
                }

                /**
                 * Legt den Wert der deprecatedsinceversion-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDeprecatedsinceversion(BigInteger value) {
                    this.deprecatedsinceversion = value;
                }

                /**
                 * Gets the value of the keyattribute property.
                 * 
                 * <p>
                 * This accessor method returns a reference to the live list,
                 * not a snapshot. Therefore any modification you make to the
                 * returned list will be present inside the JAXB object.
                 * This is why there is not a <CODE>set</CODE> method for the keyattribute property.
                 * 
                 * <p>
                 * For example, to add a new item, do as follows:
                 * <pre>
                 *    getKeyattribute().add(newItem);
                 * </pre>
                 * 
                 * 
                 * <p>
                 * Objects of the following type(s) are allowed in the list
                 * {@link Tkeyattribute }
                 * 
                 * 
                 */
                public List<Tkeyattribute> getKeyattribute() {
                    if (keyattribute == null) {
                        keyattribute = new ArrayList<Tkeyattribute>();
                    }
                    return this.keyattribute;
                }

            }

        }

    }

}
