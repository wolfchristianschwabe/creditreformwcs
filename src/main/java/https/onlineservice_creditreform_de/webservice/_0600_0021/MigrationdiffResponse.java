
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r anonymous complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="header" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tresponseheader"/>
 *         &lt;element name="body" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tmigrationdiffresponsebody"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "body"
})
@XmlRootElement(name = "migrationdiffResponse")
public class MigrationdiffResponse {

    @XmlElement(required = true)
    protected Tresponseheader header;
    @XmlElement(required = true)
    protected Tmigrationdiffresponsebody body;

    /**
     * Ruft den Wert der header-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tresponseheader }
     *     
     */
    public Tresponseheader getHeader() {
        return header;
    }

    /**
     * Legt den Wert der header-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tresponseheader }
     *     
     */
    public void setHeader(Tresponseheader value) {
        this.header = value;
    }

    /**
     * Ruft den Wert der body-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tmigrationdiffresponsebody }
     *     
     */
    public Tmigrationdiffresponsebody getBody() {
        return body;
    }

    /**
     * Legt den Wert der body-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tmigrationdiffresponsebody }
     *     
     */
    public void setBody(Tmigrationdiffresponsebody value) {
        this.body = value;
    }

}
