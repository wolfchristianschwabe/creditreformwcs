
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tpaymentbehaviour complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tpaymentbehaviour">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="paymentbehaviourindex" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="paymentbehaviourindexmeaning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *         &lt;element name="currentrecorddate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="debtordays" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="debtordaysexplanation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="timeforpayment" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="numberofvouchers" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="numberofsuppliers" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="history" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="recorddate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="paymentbehaviourindex" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *                             &lt;element name="debtordays" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *                             &lt;element name="timeforpayment" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="branch" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="branchcodeedition" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *                   &lt;element name="branchcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="branchtext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="debtordays" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *                   &lt;element name="timeforpayment" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *                   &lt;element name="history" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="recorddate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                                       &lt;element name="debtordays" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *                                       &lt;element name="timeforpayment" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="informationtext" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tpaymentbehaviour", propOrder = {
    "paymentbehaviourindex",
    "paymentbehaviourindexmeaning",
    "trafficlight",
    "currentrecorddate",
    "debtordays",
    "debtordaysexplanation",
    "timeforpayment",
    "numberofvouchers",
    "numberofsuppliers",
    "history",
    "branch",
    "informationtext",
    "text"
})
public class Tpaymentbehaviour {

    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger paymentbehaviourindex;
    protected String paymentbehaviourindexmeaning;
    protected Tkeywithgrade trafficlight;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar currentrecorddate;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger debtordays;
    protected String debtordaysexplanation;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger timeforpayment;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger numberofvouchers;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger numberofsuppliers;
    protected Tpaymentbehaviour.History history;
    protected Tpaymentbehaviour.Branch branch;
    protected Ttext informationtext;
    protected Ttext text;

    /**
     * Ruft den Wert der paymentbehaviourindex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getPaymentbehaviourindex() {
        return paymentbehaviourindex;
    }

    /**
     * Legt den Wert der paymentbehaviourindex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setPaymentbehaviourindex(BigInteger value) {
        this.paymentbehaviourindex = value;
    }

    /**
     * Ruft den Wert der paymentbehaviourindexmeaning-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentbehaviourindexmeaning() {
        return paymentbehaviourindexmeaning;
    }

    /**
     * Legt den Wert der paymentbehaviourindexmeaning-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentbehaviourindexmeaning(String value) {
        this.paymentbehaviourindexmeaning = value;
    }

    /**
     * Ruft den Wert der trafficlight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getTrafficlight() {
        return trafficlight;
    }

    /**
     * Legt den Wert der trafficlight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setTrafficlight(Tkeywithgrade value) {
        this.trafficlight = value;
    }

    /**
     * Ruft den Wert der currentrecorddate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCurrentrecorddate() {
        return currentrecorddate;
    }

    /**
     * Legt den Wert der currentrecorddate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCurrentrecorddate(XMLGregorianCalendar value) {
        this.currentrecorddate = value;
    }

    /**
     * Ruft den Wert der debtordays-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDebtordays() {
        return debtordays;
    }

    /**
     * Legt den Wert der debtordays-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDebtordays(BigInteger value) {
        this.debtordays = value;
    }

    /**
     * Ruft den Wert der debtordaysexplanation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebtordaysexplanation() {
        return debtordaysexplanation;
    }

    /**
     * Legt den Wert der debtordaysexplanation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebtordaysexplanation(String value) {
        this.debtordaysexplanation = value;
    }

    /**
     * Ruft den Wert der timeforpayment-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTimeforpayment() {
        return timeforpayment;
    }

    /**
     * Legt den Wert der timeforpayment-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTimeforpayment(BigInteger value) {
        this.timeforpayment = value;
    }

    /**
     * Ruft den Wert der numberofvouchers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberofvouchers() {
        return numberofvouchers;
    }

    /**
     * Legt den Wert der numberofvouchers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberofvouchers(BigInteger value) {
        this.numberofvouchers = value;
    }

    /**
     * Ruft den Wert der numberofsuppliers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberofsuppliers() {
        return numberofsuppliers;
    }

    /**
     * Legt den Wert der numberofsuppliers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberofsuppliers(BigInteger value) {
        this.numberofsuppliers = value;
    }

    /**
     * Ruft den Wert der history-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tpaymentbehaviour.History }
     *     
     */
    public Tpaymentbehaviour.History getHistory() {
        return history;
    }

    /**
     * Legt den Wert der history-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tpaymentbehaviour.History }
     *     
     */
    public void setHistory(Tpaymentbehaviour.History value) {
        this.history = value;
    }

    /**
     * Ruft den Wert der branch-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tpaymentbehaviour.Branch }
     *     
     */
    public Tpaymentbehaviour.Branch getBranch() {
        return branch;
    }

    /**
     * Legt den Wert der branch-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tpaymentbehaviour.Branch }
     *     
     */
    public void setBranch(Tpaymentbehaviour.Branch value) {
        this.branch = value;
    }

    /**
     * Ruft den Wert der informationtext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getInformationtext() {
        return informationtext;
    }

    /**
     * Legt den Wert der informationtext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setInformationtext(Ttext value) {
        this.informationtext = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="branchcodeedition" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
     *         &lt;element name="branchcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="branchtext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="debtordays" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
     *         &lt;element name="timeforpayment" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
     *         &lt;element name="history" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="recorddate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                             &lt;element name="debtordays" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
     *                             &lt;element name="timeforpayment" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "branchcodeedition",
        "branchcode",
        "branchtext",
        "debtordays",
        "timeforpayment",
        "history"
    })
    public static class Branch {

        protected Tkeywithshortdesignation branchcodeedition;
        protected String branchcode;
        protected String branchtext;
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger debtordays;
        @XmlSchemaType(name = "nonNegativeInteger")
        protected BigInteger timeforpayment;
        protected Tpaymentbehaviour.Branch.History history;

        /**
         * Ruft den Wert der branchcodeedition-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public Tkeywithshortdesignation getBranchcodeedition() {
            return branchcodeedition;
        }

        /**
         * Legt den Wert der branchcodeedition-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public void setBranchcodeedition(Tkeywithshortdesignation value) {
            this.branchcodeedition = value;
        }

        /**
         * Ruft den Wert der branchcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBranchcode() {
            return branchcode;
        }

        /**
         * Legt den Wert der branchcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBranchcode(String value) {
            this.branchcode = value;
        }

        /**
         * Ruft den Wert der branchtext-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBranchtext() {
            return branchtext;
        }

        /**
         * Legt den Wert der branchtext-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBranchtext(String value) {
            this.branchtext = value;
        }

        /**
         * Ruft den Wert der debtordays-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getDebtordays() {
            return debtordays;
        }

        /**
         * Legt den Wert der debtordays-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setDebtordays(BigInteger value) {
            this.debtordays = value;
        }

        /**
         * Ruft den Wert der timeforpayment-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigInteger }
         *     
         */
        public BigInteger getTimeforpayment() {
            return timeforpayment;
        }

        /**
         * Legt den Wert der timeforpayment-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigInteger }
         *     
         */
        public void setTimeforpayment(BigInteger value) {
            this.timeforpayment = value;
        }

        /**
         * Ruft den Wert der history-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tpaymentbehaviour.Branch.History }
         *     
         */
        public Tpaymentbehaviour.Branch.History getHistory() {
            return history;
        }

        /**
         * Legt den Wert der history-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tpaymentbehaviour.Branch.History }
         *     
         */
        public void setHistory(Tpaymentbehaviour.Branch.History value) {
            this.history = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="recorddate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *                   &lt;element name="debtordays" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
         *                   &lt;element name="timeforpayment" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "entry"
        })
        public static class History {

            protected List<Tpaymentbehaviour.Branch.History.Entry> entry;

            /**
             * Gets the value of the entry property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the entry property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getEntry().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Tpaymentbehaviour.Branch.History.Entry }
             * 
             * 
             */
            public List<Tpaymentbehaviour.Branch.History.Entry> getEntry() {
                if (entry == null) {
                    entry = new ArrayList<Tpaymentbehaviour.Branch.History.Entry>();
                }
                return this.entry;
            }


            /**
             * <p>Java-Klasse f�r anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="recorddate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
             *         &lt;element name="debtordays" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
             *         &lt;element name="timeforpayment" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "recorddate",
                "debtordays",
                "timeforpayment"
            })
            public static class Entry {

                @XmlSchemaType(name = "date")
                protected XMLGregorianCalendar recorddate;
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger debtordays;
                @XmlSchemaType(name = "nonNegativeInteger")
                protected BigInteger timeforpayment;

                /**
                 * Ruft den Wert der recorddate-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public XMLGregorianCalendar getRecorddate() {
                    return recorddate;
                }

                /**
                 * Legt den Wert der recorddate-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link XMLGregorianCalendar }
                 *     
                 */
                public void setRecorddate(XMLGregorianCalendar value) {
                    this.recorddate = value;
                }

                /**
                 * Ruft den Wert der debtordays-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getDebtordays() {
                    return debtordays;
                }

                /**
                 * Legt den Wert der debtordays-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setDebtordays(BigInteger value) {
                    this.debtordays = value;
                }

                /**
                 * Ruft den Wert der timeforpayment-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigInteger }
                 *     
                 */
                public BigInteger getTimeforpayment() {
                    return timeforpayment;
                }

                /**
                 * Legt den Wert der timeforpayment-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigInteger }
                 *     
                 */
                public void setTimeforpayment(BigInteger value) {
                    this.timeforpayment = value;
                }

            }

        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="recorddate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="paymentbehaviourindex" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
     *                   &lt;element name="debtordays" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
     *                   &lt;element name="timeforpayment" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "entry"
    })
    public static class History {

        protected List<Tpaymentbehaviour.History.Entry> entry;

        /**
         * Gets the value of the entry property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the entry property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEntry().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tpaymentbehaviour.History.Entry }
         * 
         * 
         */
        public List<Tpaymentbehaviour.History.Entry> getEntry() {
            if (entry == null) {
                entry = new ArrayList<Tpaymentbehaviour.History.Entry>();
            }
            return this.entry;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="recorddate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="paymentbehaviourindex" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
         *         &lt;element name="debtordays" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
         *         &lt;element name="timeforpayment" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "recorddate",
            "paymentbehaviourindex",
            "debtordays",
            "timeforpayment"
        })
        public static class Entry {

            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar recorddate;
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger paymentbehaviourindex;
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger debtordays;
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger timeforpayment;

            /**
             * Ruft den Wert der recorddate-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getRecorddate() {
                return recorddate;
            }

            /**
             * Legt den Wert der recorddate-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setRecorddate(XMLGregorianCalendar value) {
                this.recorddate = value;
            }

            /**
             * Ruft den Wert der paymentbehaviourindex-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getPaymentbehaviourindex() {
                return paymentbehaviourindex;
            }

            /**
             * Legt den Wert der paymentbehaviourindex-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setPaymentbehaviourindex(BigInteger value) {
                this.paymentbehaviourindex = value;
            }

            /**
             * Ruft den Wert der debtordays-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getDebtordays() {
                return debtordays;
            }

            /**
             * Legt den Wert der debtordays-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setDebtordays(BigInteger value) {
                this.debtordays = value;
            }

            /**
             * Ruft den Wert der timeforpayment-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getTimeforpayment() {
                return timeforpayment;
            }

            /**
             * Legt den Wert der timeforpayment-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setTimeforpayment(BigInteger value) {
                this.timeforpayment = value;
            }

        }

    }

}
