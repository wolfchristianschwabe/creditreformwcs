
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcreditopinion complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcreditopinion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="creditlimit" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *         &lt;element name="creditlimitstatement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *         &lt;element name="typeofcreditopinion" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *         &lt;element name="creditenquiryremarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcreditopinion", propOrder = {
    "creditlimit",
    "creditlimitstatement",
    "typeofcreditopinion",
    "creditenquiryremarks",
    "text"
})
public class Tcreditopinion {

    protected Tamount creditlimit;
    protected Tkeywithgrade creditlimitstatement;
    protected Tkeywithgrade typeofcreditopinion;
    protected String creditenquiryremarks;
    protected Ttext text;

    /**
     * Ruft den Wert der creditlimit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamount }
     *     
     */
    public Tamount getCreditlimit() {
        return creditlimit;
    }

    /**
     * Legt den Wert der creditlimit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamount }
     *     
     */
    public void setCreditlimit(Tamount value) {
        this.creditlimit = value;
    }

    /**
     * Ruft den Wert der creditlimitstatement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getCreditlimitstatement() {
        return creditlimitstatement;
    }

    /**
     * Legt den Wert der creditlimitstatement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setCreditlimitstatement(Tkeywithgrade value) {
        this.creditlimitstatement = value;
    }

    /**
     * Ruft den Wert der typeofcreditopinion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getTypeofcreditopinion() {
        return typeofcreditopinion;
    }

    /**
     * Legt den Wert der typeofcreditopinion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setTypeofcreditopinion(Tkeywithgrade value) {
        this.typeofcreditopinion = value;
    }

    /**
     * Ruft den Wert der creditenquiryremarks-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditenquiryremarks() {
        return creditenquiryremarks;
    }

    /**
     * Legt den Wert der creditenquiryremarks-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditenquiryremarks(String value) {
        this.creditenquiryremarks = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

}
