
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tsupplement complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tsupplement">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="supplementdate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="reasonofsupplement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="signalreason" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="terminationreason" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="note" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="miscellaneous" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tsupplement", propOrder = {
    "supplementdate",
    "reasonofsupplement",
    "signalreason",
    "terminationreason",
    "note",
    "miscellaneous"
})
public class Tsupplement {

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar supplementdate;
    protected List<Tkey> reasonofsupplement;
    protected List<Tkey> signalreason;
    protected Tkey terminationreason;
    protected Ttext note;
    protected Ttext miscellaneous;

    /**
     * Ruft den Wert der supplementdate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getSupplementdate() {
        return supplementdate;
    }

    /**
     * Legt den Wert der supplementdate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setSupplementdate(XMLGregorianCalendar value) {
        this.supplementdate = value;
    }

    /**
     * Gets the value of the reasonofsupplement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the reasonofsupplement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getReasonofsupplement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tkey }
     * 
     * 
     */
    public List<Tkey> getReasonofsupplement() {
        if (reasonofsupplement == null) {
            reasonofsupplement = new ArrayList<Tkey>();
        }
        return this.reasonofsupplement;
    }

    /**
     * Gets the value of the signalreason property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the signalreason property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSignalreason().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tkey }
     * 
     * 
     */
    public List<Tkey> getSignalreason() {
        if (signalreason == null) {
            signalreason = new ArrayList<Tkey>();
        }
        return this.signalreason;
    }

    /**
     * Ruft den Wert der terminationreason-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getTerminationreason() {
        return terminationreason;
    }

    /**
     * Legt den Wert der terminationreason-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setTerminationreason(Tkey value) {
        this.terminationreason = value;
    }

    /**
     * Ruft den Wert der note-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getNote() {
        return note;
    }

    /**
     * Legt den Wert der note-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setNote(Ttext value) {
        this.note = value;
    }

    /**
     * Ruft den Wert der miscellaneous-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getMiscellaneous() {
        return miscellaneous;
    }

    /**
     * Legt den Wert der miscellaneous-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setMiscellaneous(Ttext value) {
        this.miscellaneous = value;
    }

}
