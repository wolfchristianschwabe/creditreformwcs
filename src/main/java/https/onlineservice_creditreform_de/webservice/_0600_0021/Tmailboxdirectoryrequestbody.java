
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tmailboxdirectoryrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tmailboxdirectoryrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pagereference" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}referencenumber" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *         &lt;element name="orderperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tperiod" minOccurs="0"/>
 *         &lt;element name="creationperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tperiod" minOccurs="0"/>
 *         &lt;element name="callperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tperiod" minOccurs="0"/>
 *         &lt;element name="openorders" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="entriesread" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="entriesunread" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="deliverytypeupdate" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="deliverytypereport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="deliverytypestatusreply" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="deliverytypesupplement" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="deliverytypestockdelivery" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="numberofentries" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;minInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tmailboxdirectoryrequestbody", propOrder = {
    "pagereference",
    "referencenumber",
    "identificationnumber",
    "orderperiod",
    "creationperiod",
    "callperiod",
    "openorders",
    "entriesread",
    "entriesunread",
    "deliverytypeupdate",
    "deliverytypereport",
    "deliverytypestatusreply",
    "deliverytypesupplement",
    "deliverytypestockdelivery",
    "numberofentries"
})
public class Tmailboxdirectoryrequestbody {

    protected Long pagereference;
    protected Long referencenumber;
    protected String identificationnumber;
    protected Tperiod orderperiod;
    protected Tperiod creationperiod;
    protected Tperiod callperiod;
    protected boolean openorders;
    protected boolean entriesread;
    protected boolean entriesunread;
    protected boolean deliverytypeupdate;
    protected boolean deliverytypereport;
    protected boolean deliverytypestatusreply;
    protected boolean deliverytypesupplement;
    protected boolean deliverytypestockdelivery;
    protected Integer numberofentries;

    /**
     * Ruft den Wert der pagereference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPagereference() {
        return pagereference;
    }

    /**
     * Legt den Wert der pagereference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPagereference(Long value) {
        this.pagereference = value;
    }

    /**
     * Ruft den Wert der referencenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReferencenumber() {
        return referencenumber;
    }

    /**
     * Legt den Wert der referencenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReferencenumber(Long value) {
        this.referencenumber = value;
    }

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der orderperiod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tperiod }
     *     
     */
    public Tperiod getOrderperiod() {
        return orderperiod;
    }

    /**
     * Legt den Wert der orderperiod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tperiod }
     *     
     */
    public void setOrderperiod(Tperiod value) {
        this.orderperiod = value;
    }

    /**
     * Ruft den Wert der creationperiod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tperiod }
     *     
     */
    public Tperiod getCreationperiod() {
        return creationperiod;
    }

    /**
     * Legt den Wert der creationperiod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tperiod }
     *     
     */
    public void setCreationperiod(Tperiod value) {
        this.creationperiod = value;
    }

    /**
     * Ruft den Wert der callperiod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tperiod }
     *     
     */
    public Tperiod getCallperiod() {
        return callperiod;
    }

    /**
     * Legt den Wert der callperiod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tperiod }
     *     
     */
    public void setCallperiod(Tperiod value) {
        this.callperiod = value;
    }

    /**
     * Ruft den Wert der openorders-Eigenschaft ab.
     * 
     */
    public boolean isOpenorders() {
        return openorders;
    }

    /**
     * Legt den Wert der openorders-Eigenschaft fest.
     * 
     */
    public void setOpenorders(boolean value) {
        this.openorders = value;
    }

    /**
     * Ruft den Wert der entriesread-Eigenschaft ab.
     * 
     */
    public boolean isEntriesread() {
        return entriesread;
    }

    /**
     * Legt den Wert der entriesread-Eigenschaft fest.
     * 
     */
    public void setEntriesread(boolean value) {
        this.entriesread = value;
    }

    /**
     * Ruft den Wert der entriesunread-Eigenschaft ab.
     * 
     */
    public boolean isEntriesunread() {
        return entriesunread;
    }

    /**
     * Legt den Wert der entriesunread-Eigenschaft fest.
     * 
     */
    public void setEntriesunread(boolean value) {
        this.entriesunread = value;
    }

    /**
     * Ruft den Wert der deliverytypeupdate-Eigenschaft ab.
     * 
     */
    public boolean isDeliverytypeupdate() {
        return deliverytypeupdate;
    }

    /**
     * Legt den Wert der deliverytypeupdate-Eigenschaft fest.
     * 
     */
    public void setDeliverytypeupdate(boolean value) {
        this.deliverytypeupdate = value;
    }

    /**
     * Ruft den Wert der deliverytypereport-Eigenschaft ab.
     * 
     */
    public boolean isDeliverytypereport() {
        return deliverytypereport;
    }

    /**
     * Legt den Wert der deliverytypereport-Eigenschaft fest.
     * 
     */
    public void setDeliverytypereport(boolean value) {
        this.deliverytypereport = value;
    }

    /**
     * Ruft den Wert der deliverytypestatusreply-Eigenschaft ab.
     * 
     */
    public boolean isDeliverytypestatusreply() {
        return deliverytypestatusreply;
    }

    /**
     * Legt den Wert der deliverytypestatusreply-Eigenschaft fest.
     * 
     */
    public void setDeliverytypestatusreply(boolean value) {
        this.deliverytypestatusreply = value;
    }

    /**
     * Ruft den Wert der deliverytypesupplement-Eigenschaft ab.
     * 
     */
    public boolean isDeliverytypesupplement() {
        return deliverytypesupplement;
    }

    /**
     * Legt den Wert der deliverytypesupplement-Eigenschaft fest.
     * 
     */
    public void setDeliverytypesupplement(boolean value) {
        this.deliverytypesupplement = value;
    }

    /**
     * Ruft den Wert der deliverytypestockdelivery-Eigenschaft ab.
     * 
     */
    public boolean isDeliverytypestockdelivery() {
        return deliverytypestockdelivery;
    }

    /**
     * Legt den Wert der deliverytypestockdelivery-Eigenschaft fest.
     * 
     */
    public void setDeliverytypestockdelivery(boolean value) {
        this.deliverytypestockdelivery = value;
    }

    /**
     * Ruft den Wert der numberofentries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberofentries() {
        return numberofentries;
    }

    /**
     * Legt den Wert der numberofentries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberofentries(Integer value) {
        this.numberofentries = value;
    }

}
