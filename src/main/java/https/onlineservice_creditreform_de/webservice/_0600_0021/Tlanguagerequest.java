
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tlanguagerequest.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * <p>
 * <pre>
 * &lt;simpleType name="Tlanguagerequest">
 *   &lt;restriction base="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlanguage">
 *     &lt;pattern value="[a-z]{2}"/>
 *     &lt;enumeration value="de"/>
 *     &lt;enumeration value="en"/>
 *     &lt;enumeration value="fr"/>
 *     &lt;enumeration value="it"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "Tlanguagerequest")
@XmlEnum
public enum Tlanguagerequest {

    @XmlEnumValue("de")
    DE("de"),
    @XmlEnumValue("en")
    EN("en"),
    @XmlEnumValue("fr")
    FR("fr"),
    @XmlEnumValue("it")
    IT("it");
    private final String value;

    Tlanguagerequest(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static Tlanguagerequest fromValue(String v) {
        for (Tlanguagerequest c: Tlanguagerequest.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
