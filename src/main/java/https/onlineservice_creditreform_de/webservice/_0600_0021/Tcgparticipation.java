
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcgparticipation complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcgparticipation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *         &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="address" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgresponseaddress" minOccurs="0"/>
 *         &lt;element name="participationtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcgparticipation", propOrder = {
    "identificationnumber",
    "companyname",
    "address",
    "participationtype"
})
public class Tcgparticipation {

    protected String identificationnumber;
    protected String companyname;
    protected Tcgresponseaddress address;
    protected Tkey participationtype;

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der companyname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyname() {
        return companyname;
    }

    /**
     * Legt den Wert der companyname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyname(String value) {
        this.companyname = value;
    }

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgresponseaddress }
     *     
     */
    public Tcgresponseaddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgresponseaddress }
     *     
     */
    public void setAddress(Tcgresponseaddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der participationtype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getParticipationtype() {
        return participationtype;
    }

    /**
     * Legt den Wert der participationtype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setParticipationtype(Tkey value) {
        this.participationtype = value;
    }

}
