
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Debtor Information
 * 
 * <p>Java-Klasse f�r Tcollectionrequestdebtor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionrequestdebtor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="company">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;choice>
 *                       &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                       &lt;sequence>
 *                         &lt;element name="namesplit">
 *                           &lt;complexType>
 *                             &lt;complexContent>
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                 &lt;sequence>
 *                                   &lt;element name="name1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                   &lt;element name="name2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                   &lt;element name="name3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                 &lt;/sequence>
 *                               &lt;/restriction>
 *                             &lt;/complexContent>
 *                           &lt;/complexType>
 *                         &lt;/element>
 *                       &lt;/sequence>
 *                     &lt;/choice>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="privateperson">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="salutation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeysalutation" minOccurs="0"/>
 *                     &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="firstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="birthname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="dateofbirth" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;element name="addressdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestaddress"/>
 *         &lt;element name="communicationdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestcommunication" minOccurs="0"/>
 *         &lt;element name="bankdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestbankdata" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionrequestdebtor", propOrder = {
    "company",
    "privateperson",
    "addressdata",
    "communicationdata",
    "bankdata"
})
public class Tcollectionrequestdebtor {

    protected Tcollectionrequestdebtor.Company company;
    protected Tcollectionrequestdebtor.Privateperson privateperson;
    @XmlElement(required = true)
    protected Tcollectionrequestaddress addressdata;
    protected Tcollectionrequestcommunication communicationdata;
    protected Tcollectionrequestbankdata bankdata;

    /**
     * Ruft den Wert der company-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestdebtor.Company }
     *     
     */
    public Tcollectionrequestdebtor.Company getCompany() {
        return company;
    }

    /**
     * Legt den Wert der company-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestdebtor.Company }
     *     
     */
    public void setCompany(Tcollectionrequestdebtor.Company value) {
        this.company = value;
    }

    /**
     * Ruft den Wert der privateperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestdebtor.Privateperson }
     *     
     */
    public Tcollectionrequestdebtor.Privateperson getPrivateperson() {
        return privateperson;
    }

    /**
     * Legt den Wert der privateperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestdebtor.Privateperson }
     *     
     */
    public void setPrivateperson(Tcollectionrequestdebtor.Privateperson value) {
        this.privateperson = value;
    }

    /**
     * Ruft den Wert der addressdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestaddress }
     *     
     */
    public Tcollectionrequestaddress getAddressdata() {
        return addressdata;
    }

    /**
     * Legt den Wert der addressdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestaddress }
     *     
     */
    public void setAddressdata(Tcollectionrequestaddress value) {
        this.addressdata = value;
    }

    /**
     * Ruft den Wert der communicationdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestcommunication }
     *     
     */
    public Tcollectionrequestcommunication getCommunicationdata() {
        return communicationdata;
    }

    /**
     * Legt den Wert der communicationdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestcommunication }
     *     
     */
    public void setCommunicationdata(Tcollectionrequestcommunication value) {
        this.communicationdata = value;
    }

    /**
     * Ruft den Wert der bankdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestbankdata }
     *     
     */
    public Tcollectionrequestbankdata getBankdata() {
        return bankdata;
    }

    /**
     * Legt den Wert der bankdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestbankdata }
     *     
     */
    public void setBankdata(Tcollectionrequestbankdata value) {
        this.bankdata = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;choice>
     *           &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *           &lt;sequence>
     *             &lt;element name="namesplit">
     *               &lt;complexType>
     *                 &lt;complexContent>
     *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                     &lt;sequence>
     *                       &lt;element name="name1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                       &lt;element name="name2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                       &lt;element name="name3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                     &lt;/sequence>
     *                   &lt;/restriction>
     *                 &lt;/complexContent>
     *               &lt;/complexType>
     *             &lt;/element>
     *           &lt;/sequence>
     *         &lt;/choice>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "companyname",
        "namesplit"
    })
    public static class Company {

        protected String companyname;
        protected Tcollectionrequestdebtor.Company.Namesplit namesplit;

        /**
         * Ruft den Wert der companyname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyname() {
            return companyname;
        }

        /**
         * Legt den Wert der companyname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyname(String value) {
            this.companyname = value;
        }

        /**
         * Ruft den Wert der namesplit-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tcollectionrequestdebtor.Company.Namesplit }
         *     
         */
        public Tcollectionrequestdebtor.Company.Namesplit getNamesplit() {
            return namesplit;
        }

        /**
         * Legt den Wert der namesplit-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tcollectionrequestdebtor.Company.Namesplit }
         *     
         */
        public void setNamesplit(Tcollectionrequestdebtor.Company.Namesplit value) {
            this.namesplit = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="name1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="name2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="name3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name1",
            "name2",
            "name3"
        })
        public static class Namesplit {

            @XmlElement(required = true)
            protected String name1;
            protected String name2;
            protected String name3;

            /**
             * Ruft den Wert der name1-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName1() {
                return name1;
            }

            /**
             * Legt den Wert der name1-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName1(String value) {
                this.name1 = value;
            }

            /**
             * Ruft den Wert der name2-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName2() {
                return name2;
            }

            /**
             * Legt den Wert der name2-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName2(String value) {
                this.name2 = value;
            }

            /**
             * Ruft den Wert der name3-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName3() {
                return name3;
            }

            /**
             * Legt den Wert der name3-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName3(String value) {
                this.name3 = value;
            }

        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="salutation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeysalutation" minOccurs="0"/>
     *         &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="firstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="birthname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="dateofbirth" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "salutation",
        "surname",
        "firstname",
        "birthname",
        "dateofbirth"
    })
    public static class Privateperson {

        protected String salutation;
        @XmlElement(required = true)
        protected String surname;
        protected String firstname;
        protected String birthname;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateofbirth;

        /**
         * Ruft den Wert der salutation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSalutation() {
            return salutation;
        }

        /**
         * Legt den Wert der salutation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSalutation(String value) {
            this.salutation = value;
        }

        /**
         * Ruft den Wert der surname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurname() {
            return surname;
        }

        /**
         * Legt den Wert der surname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurname(String value) {
            this.surname = value;
        }

        /**
         * Ruft den Wert der firstname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFirstname() {
            return firstname;
        }

        /**
         * Legt den Wert der firstname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFirstname(String value) {
            this.firstname = value;
        }

        /**
         * Ruft den Wert der birthname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBirthname() {
            return birthname;
        }

        /**
         * Legt den Wert der birthname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBirthname(String value) {
            this.birthname = value;
        }

        /**
         * Ruft den Wert der dateofbirth-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateofbirth() {
            return dateofbirth;
        }

        /**
         * Legt den Wert der dateofbirth-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateofbirth(XMLGregorianCalendar value) {
            this.dateofbirth = value;
        }

    }

}
