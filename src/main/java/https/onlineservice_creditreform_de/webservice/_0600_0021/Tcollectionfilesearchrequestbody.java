
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tcollectionfilesearchrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionfilesearchrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerreference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="debtorname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateinvoicefrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="dateinvoiceuntil" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="invoicenumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fileclosed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="collectionordertype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datepaymentreceiptfrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="datepaymentreceiptuntil" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="datesubmissionfrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="datesubmissionuntil" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="dateclosurefrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="dateclosureuntil" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionfilesearchrequestbody", propOrder = {
    "filenumber",
    "customerreference",
    "debtorname",
    "dateinvoicefrom",
    "dateinvoiceuntil",
    "invoicenumber",
    "fileclosed",
    "collectionordertype",
    "datepaymentreceiptfrom",
    "datepaymentreceiptuntil",
    "datesubmissionfrom",
    "datesubmissionuntil",
    "dateclosurefrom",
    "dateclosureuntil",
    "user"
})
public class Tcollectionfilesearchrequestbody {

    protected String filenumber;
    protected String customerreference;
    protected String debtorname;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateinvoicefrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateinvoiceuntil;
    protected String invoicenumber;
    protected Boolean fileclosed;
    protected String collectionordertype;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datepaymentreceiptfrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datepaymentreceiptuntil;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datesubmissionfrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datesubmissionuntil;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateclosurefrom;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateclosureuntil;
    protected List<String> user;

    /**
     * Ruft den Wert der filenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilenumber() {
        return filenumber;
    }

    /**
     * Legt den Wert der filenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilenumber(String value) {
        this.filenumber = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der debtorname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebtorname() {
        return debtorname;
    }

    /**
     * Legt den Wert der debtorname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebtorname(String value) {
        this.debtorname = value;
    }

    /**
     * Ruft den Wert der dateinvoicefrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateinvoicefrom() {
        return dateinvoicefrom;
    }

    /**
     * Legt den Wert der dateinvoicefrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateinvoicefrom(XMLGregorianCalendar value) {
        this.dateinvoicefrom = value;
    }

    /**
     * Ruft den Wert der dateinvoiceuntil-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateinvoiceuntil() {
        return dateinvoiceuntil;
    }

    /**
     * Legt den Wert der dateinvoiceuntil-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateinvoiceuntil(XMLGregorianCalendar value) {
        this.dateinvoiceuntil = value;
    }

    /**
     * Ruft den Wert der invoicenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicenumber() {
        return invoicenumber;
    }

    /**
     * Legt den Wert der invoicenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicenumber(String value) {
        this.invoicenumber = value;
    }

    /**
     * Ruft den Wert der fileclosed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFileclosed() {
        return fileclosed;
    }

    /**
     * Legt den Wert der fileclosed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFileclosed(Boolean value) {
        this.fileclosed = value;
    }

    /**
     * Ruft den Wert der collectionordertype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectionordertype() {
        return collectionordertype;
    }

    /**
     * Legt den Wert der collectionordertype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionordertype(String value) {
        this.collectionordertype = value;
    }

    /**
     * Ruft den Wert der datepaymentreceiptfrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatepaymentreceiptfrom() {
        return datepaymentreceiptfrom;
    }

    /**
     * Legt den Wert der datepaymentreceiptfrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatepaymentreceiptfrom(XMLGregorianCalendar value) {
        this.datepaymentreceiptfrom = value;
    }

    /**
     * Ruft den Wert der datepaymentreceiptuntil-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatepaymentreceiptuntil() {
        return datepaymentreceiptuntil;
    }

    /**
     * Legt den Wert der datepaymentreceiptuntil-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatepaymentreceiptuntil(XMLGregorianCalendar value) {
        this.datepaymentreceiptuntil = value;
    }

    /**
     * Ruft den Wert der datesubmissionfrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatesubmissionfrom() {
        return datesubmissionfrom;
    }

    /**
     * Legt den Wert der datesubmissionfrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatesubmissionfrom(XMLGregorianCalendar value) {
        this.datesubmissionfrom = value;
    }

    /**
     * Ruft den Wert der datesubmissionuntil-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatesubmissionuntil() {
        return datesubmissionuntil;
    }

    /**
     * Legt den Wert der datesubmissionuntil-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatesubmissionuntil(XMLGregorianCalendar value) {
        this.datesubmissionuntil = value;
    }

    /**
     * Ruft den Wert der dateclosurefrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateclosurefrom() {
        return dateclosurefrom;
    }

    /**
     * Legt den Wert der dateclosurefrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateclosurefrom(XMLGregorianCalendar value) {
        this.dateclosurefrom = value;
    }

    /**
     * Ruft den Wert der dateclosureuntil-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateclosureuntil() {
        return dateclosureuntil;
    }

    /**
     * Legt den Wert der dateclosureuntil-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateclosureuntil(XMLGregorianCalendar value) {
        this.dateclosureuntil = value;
    }

    /**
     * Gets the value of the user property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the user property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUser().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getUser() {
        if (user == null) {
            user = new ArrayList<String>();
        }
        return this.user;
    }

}
