
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tlegalform complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tlegalform">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="presentlegalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="riskrating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="riskindex" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="probabilityofdefault" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="duetolegalform" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="explanation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tlegalform", propOrder = {
    "presentlegalform",
    "text",
    "riskrating",
    "riskindex",
    "probabilityofdefault",
    "trafficlight"
})
public class Tlegalform {

    protected Tkeywithshortdesignation presentlegalform;
    protected Ttext text;
    protected String riskrating;
    protected BigDecimal riskindex;
    protected Tlegalform.Probabilityofdefault probabilityofdefault;
    protected Tkeywithgrade trafficlight;

    /**
     * Ruft den Wert der presentlegalform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithshortdesignation }
     *     
     */
    public Tkeywithshortdesignation getPresentlegalform() {
        return presentlegalform;
    }

    /**
     * Legt den Wert der presentlegalform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithshortdesignation }
     *     
     */
    public void setPresentlegalform(Tkeywithshortdesignation value) {
        this.presentlegalform = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

    /**
     * Ruft den Wert der riskrating-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskrating() {
        return riskrating;
    }

    /**
     * Legt den Wert der riskrating-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskrating(String value) {
        this.riskrating = value;
    }

    /**
     * Ruft den Wert der riskindex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRiskindex() {
        return riskindex;
    }

    /**
     * Legt den Wert der riskindex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRiskindex(BigDecimal value) {
        this.riskindex = value;
    }

    /**
     * Ruft den Wert der probabilityofdefault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tlegalform.Probabilityofdefault }
     *     
     */
    public Tlegalform.Probabilityofdefault getProbabilityofdefault() {
        return probabilityofdefault;
    }

    /**
     * Legt den Wert der probabilityofdefault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tlegalform.Probabilityofdefault }
     *     
     */
    public void setProbabilityofdefault(Tlegalform.Probabilityofdefault value) {
        this.probabilityofdefault = value;
    }

    /**
     * Ruft den Wert der trafficlight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getTrafficlight() {
        return trafficlight;
    }

    /**
     * Legt den Wert der trafficlight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setTrafficlight(Tkeywithgrade value) {
        this.trafficlight = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="duetolegalform" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="explanation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "date",
        "duetolegalform",
        "explanation"
    })
    public static class Probabilityofdefault {

        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar date;
        protected BigDecimal duetolegalform;
        protected String explanation;

        /**
         * Ruft den Wert der date-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDate() {
            return date;
        }

        /**
         * Legt den Wert der date-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDate(XMLGregorianCalendar value) {
            this.date = value;
        }

        /**
         * Ruft den Wert der duetolegalform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getDuetolegalform() {
            return duetolegalform;
        }

        /**
         * Legt den Wert der duetolegalform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setDuetolegalform(BigDecimal value) {
            this.duetolegalform = value;
        }

        /**
         * Ruft den Wert der explanation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExplanation() {
            return explanation;
        }

        /**
         * Legt den Wert der explanation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExplanation(String value) {
            this.explanation = value;
        }

    }

}
