
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tancillaryinformationcapital complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tancillaryinformationcapital">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capital" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *         &lt;element name="capitaltext" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tancillaryinformationcapital", propOrder = {
    "capital",
    "capitaltext"
})
public class Tancillaryinformationcapital {

    protected Tamount capital;
    protected Ttext capitaltext;

    /**
     * Ruft den Wert der capital-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamount }
     *     
     */
    public Tamount getCapital() {
        return capital;
    }

    /**
     * Legt den Wert der capital-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamount }
     *     
     */
    public void setCapital(Tamount value) {
        this.capital = value;
    }

    /**
     * Ruft den Wert der capitaltext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getCapitaltext() {
        return capitaltext;
    }

    /**
     * Legt den Wert der capitaltext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setCapitaltext(Ttext value) {
        this.capitaltext = value;
    }

}
