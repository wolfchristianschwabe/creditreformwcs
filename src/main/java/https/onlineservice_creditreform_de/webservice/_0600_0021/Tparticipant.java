
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipant complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipant">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}status" minOccurs="0"/>
 *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}namestructure"/>
 *         &lt;element name="powerofrepresentation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="typeofpowerofattorney" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="sharedproxy" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="function" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="task" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="limitation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="specialright" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
 *         &lt;element name="participatingsince" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;element name="participatingtill" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}addressstructure"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="warnings" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Twarning" minOccurs="0"/>
 *         &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipant", propOrder = {
    "identificationnumber",
    "easynumber",
    "status",
    "salutation",
    "privateperson",
    "company",
    "powerofrepresentation",
    "typeofpowerofattorney",
    "sharedproxy",
    "function",
    "task",
    "limitation",
    "specialright",
    "participatingsince",
    "participatingtill",
    "street",
    "housenumber",
    "housenumberaffix",
    "postcode",
    "city",
    "quarter",
    "region",
    "country",
    "text",
    "warnings",
    "trafficlight"
})
@XmlSeeAlso({
    Tparticipantextended.class
})
public class Tparticipant {

    protected String identificationnumber;
    protected String easynumber;
    protected Tkeywithdescription status;
    protected Tkey salutation;
    protected https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Privateperson privateperson;
    protected https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Company company;
    protected Tkey powerofrepresentation;
    protected Tkey typeofpowerofattorney;
    protected Tkey sharedproxy;
    protected Tkey function;
    protected Tkey task;
    protected String limitation;
    protected List<String> specialright;
    protected String participatingsince;
    protected String participatingtill;
    protected String street;
    protected Integer housenumber;
    protected String housenumberaffix;
    protected String postcode;
    protected String city;
    protected String quarter;
    protected String region;
    protected Tkey country;
    protected Ttext text;
    protected Twarning warnings;
    protected Tkeywithgrade trafficlight;

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der easynumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEasynumber() {
        return easynumber;
    }

    /**
     * Legt den Wert der easynumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEasynumber(String value) {
        this.easynumber = value;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithdescription }
     *     
     */
    public Tkeywithdescription getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithdescription }
     *     
     */
    public void setStatus(Tkeywithdescription value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der salutation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getSalutation() {
        return salutation;
    }

    /**
     * Legt den Wert der salutation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setSalutation(Tkey value) {
        this.salutation = value;
    }

    /**
     * Ruft den Wert der privateperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Privateperson }
     *     
     */
    public https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Privateperson getPrivateperson() {
        return privateperson;
    }

    /**
     * Legt den Wert der privateperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Privateperson }
     *     
     */
    public void setPrivateperson(https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Privateperson value) {
        this.privateperson = value;
    }

    /**
     * Ruft den Wert der company-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Company }
     *     
     */
    public https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Company getCompany() {
        return company;
    }

    /**
     * Legt den Wert der company-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Company }
     *     
     */
    public void setCompany(https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.Company value) {
        this.company = value;
    }

    /**
     * Ruft den Wert der powerofrepresentation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getPowerofrepresentation() {
        return powerofrepresentation;
    }

    /**
     * Legt den Wert der powerofrepresentation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setPowerofrepresentation(Tkey value) {
        this.powerofrepresentation = value;
    }

    /**
     * Ruft den Wert der typeofpowerofattorney-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getTypeofpowerofattorney() {
        return typeofpowerofattorney;
    }

    /**
     * Legt den Wert der typeofpowerofattorney-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setTypeofpowerofattorney(Tkey value) {
        this.typeofpowerofattorney = value;
    }

    /**
     * Ruft den Wert der sharedproxy-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getSharedproxy() {
        return sharedproxy;
    }

    /**
     * Legt den Wert der sharedproxy-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setSharedproxy(Tkey value) {
        this.sharedproxy = value;
    }

    /**
     * Ruft den Wert der function-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getFunction() {
        return function;
    }

    /**
     * Legt den Wert der function-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setFunction(Tkey value) {
        this.function = value;
    }

    /**
     * Ruft den Wert der task-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getTask() {
        return task;
    }

    /**
     * Legt den Wert der task-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setTask(Tkey value) {
        this.task = value;
    }

    /**
     * Ruft den Wert der limitation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLimitation() {
        return limitation;
    }

    /**
     * Legt den Wert der limitation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLimitation(String value) {
        this.limitation = value;
    }

    /**
     * Gets the value of the specialright property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the specialright property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSpecialright().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getSpecialright() {
        if (specialright == null) {
            specialright = new ArrayList<String>();
        }
        return this.specialright;
    }

    /**
     * Ruft den Wert der participatingsince-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipatingsince() {
        return participatingsince;
    }

    /**
     * Legt den Wert der participatingsince-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipatingsince(String value) {
        this.participatingsince = value;
    }

    /**
     * Ruft den Wert der participatingtill-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipatingtill() {
        return participatingtill;
    }

    /**
     * Legt den Wert der participatingtill-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipatingtill(String value) {
        this.participatingtill = value;
    }

    /**
     * Ruft den Wert der street-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Legt den Wert der street-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Ruft den Wert der housenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHousenumber() {
        return housenumber;
    }

    /**
     * Legt den Wert der housenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHousenumber(Integer value) {
        this.housenumber = value;
    }

    /**
     * Ruft den Wert der housenumberaffix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHousenumberaffix() {
        return housenumberaffix;
    }

    /**
     * Legt den Wert der housenumberaffix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHousenumberaffix(String value) {
        this.housenumberaffix = value;
    }

    /**
     * Ruft den Wert der postcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Legt den Wert der postcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostcode(String value) {
        this.postcode = value;
    }

    /**
     * Ruft den Wert der city-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Legt den Wert der city-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Ruft den Wert der quarter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuarter() {
        return quarter;
    }

    /**
     * Legt den Wert der quarter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuarter(String value) {
        this.quarter = value;
    }

    /**
     * Ruft den Wert der region-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Legt den Wert der region-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Ruft den Wert der country-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCountry() {
        return country;
    }

    /**
     * Legt den Wert der country-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCountry(Tkey value) {
        this.country = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

    /**
     * Ruft den Wert der warnings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Twarning }
     *     
     */
    public Twarning getWarnings() {
        return warnings;
    }

    /**
     * Legt den Wert der warnings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Twarning }
     *     
     */
    public void setWarnings(Twarning value) {
        this.warnings = value;
    }

    /**
     * Ruft den Wert der trafficlight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getTrafficlight() {
        return trafficlight;
    }

    /**
     * Legt den Wert der trafficlight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setTrafficlight(Tkeywithgrade value) {
        this.trafficlight = value;
    }

}
