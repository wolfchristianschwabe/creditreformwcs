
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tbalanceanalysismultiplecompaniesrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tbalanceanalysismultiplecompaniesrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeyproduct"/>
 *         &lt;element name="maincompany" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalanceanalysismultiplecompaniesrequestcompany"/>
 *         &lt;element name="additionalcompany" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalanceanalysismultiplecompaniesrequestcompany" maxOccurs="unbounded"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tbalanceanalysismultiplecompaniesrequestbody", propOrder = {
    "customerreference",
    "producttype",
    "maincompany",
    "additionalcompany"
})
public class Tbalanceanalysismultiplecompaniesrequestbody {

    protected String customerreference;
    @XmlElement(required = true)
    protected String producttype;
    @XmlElement(required = true)
    protected Tbalanceanalysismultiplecompaniesrequestcompany maincompany;
    @XmlElement(required = true)
    protected List<Tbalanceanalysismultiplecompaniesrequestcompany> additionalcompany;

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducttype(String value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der maincompany-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tbalanceanalysismultiplecompaniesrequestcompany }
     *     
     */
    public Tbalanceanalysismultiplecompaniesrequestcompany getMaincompany() {
        return maincompany;
    }

    /**
     * Legt den Wert der maincompany-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tbalanceanalysismultiplecompaniesrequestcompany }
     *     
     */
    public void setMaincompany(Tbalanceanalysismultiplecompaniesrequestcompany value) {
        this.maincompany = value;
    }

    /**
     * Gets the value of the additionalcompany property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalcompany property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalcompany().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tbalanceanalysismultiplecompaniesrequestcompany }
     * 
     * 
     */
    public List<Tbalanceanalysismultiplecompaniesrequestcompany> getAdditionalcompany() {
        if (additionalcompany == null) {
            additionalcompany = new ArrayList<Tbalanceanalysismultiplecompaniesrequestcompany>();
        }
        return this.additionalcompany;
    }

}
