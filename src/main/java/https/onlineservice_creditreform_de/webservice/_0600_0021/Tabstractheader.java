
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tabstractheader complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tabstractheader">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="communicationlanguage" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlanguagerequest"/>
 *         &lt;element name="transmissiontimestamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="keylistversion" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger"/>
 *         &lt;sequence>
 *           &lt;element name="clientapplicationname">
 *             &lt;simpleType>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *                 &lt;minLength value="1"/>
 *                 &lt;maxLength value="25"/>
 *               &lt;/restriction>
 *             &lt;/simpleType>
 *           &lt;/element>
 *           &lt;element name="clientapplicationversion">
 *             &lt;simpleType>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                 &lt;minInclusive value="0"/>
 *                 &lt;maxInclusive value="999999"/>
 *               &lt;/restriction>
 *             &lt;/simpleType>
 *           &lt;/element>
 *         &lt;/sequence>
 *         &lt;element name="transactionreference" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;minLength value="1"/>
 *               &lt;maxLength value="25"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="useraccount" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuseraccount"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tabstractheader", propOrder = {
    "communicationlanguage",
    "transmissiontimestamp",
    "keylistversion",
    "clientapplicationname",
    "clientapplicationversion",
    "transactionreference",
    "useraccount"
})
@XmlSeeAlso({
    Trequestheader.class,
    Tresponseheader.class
})
public abstract class Tabstractheader {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected Tlanguagerequest communicationlanguage;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar transmissiontimestamp;
    @XmlElement(required = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger keylistversion;
    @XmlElement(required = true)
    protected String clientapplicationname;
    protected int clientapplicationversion;
    protected String transactionreference;
    @XmlElement(required = true)
    protected String useraccount;

    /**
     * Ruft den Wert der communicationlanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tlanguagerequest }
     *     
     */
    public Tlanguagerequest getCommunicationlanguage() {
        return communicationlanguage;
    }

    /**
     * Legt den Wert der communicationlanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tlanguagerequest }
     *     
     */
    public void setCommunicationlanguage(Tlanguagerequest value) {
        this.communicationlanguage = value;
    }

    /**
     * Ruft den Wert der transmissiontimestamp-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTransmissiontimestamp() {
        return transmissiontimestamp;
    }

    /**
     * Legt den Wert der transmissiontimestamp-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTransmissiontimestamp(XMLGregorianCalendar value) {
        this.transmissiontimestamp = value;
    }

    /**
     * Ruft den Wert der keylistversion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getKeylistversion() {
        return keylistversion;
    }

    /**
     * Legt den Wert der keylistversion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setKeylistversion(BigInteger value) {
        this.keylistversion = value;
    }

    /**
     * Ruft den Wert der clientapplicationname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientapplicationname() {
        return clientapplicationname;
    }

    /**
     * Legt den Wert der clientapplicationname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientapplicationname(String value) {
        this.clientapplicationname = value;
    }

    /**
     * Ruft den Wert der clientapplicationversion-Eigenschaft ab.
     * 
     */
    public int getClientapplicationversion() {
        return clientapplicationversion;
    }

    /**
     * Legt den Wert der clientapplicationversion-Eigenschaft fest.
     * 
     */
    public void setClientapplicationversion(int value) {
        this.clientapplicationversion = value;
    }

    /**
     * Ruft den Wert der transactionreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionreference() {
        return transactionreference;
    }

    /**
     * Legt den Wert der transactionreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionreference(String value) {
        this.transactionreference = value;
    }

    /**
     * Ruft den Wert der useraccount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseraccount() {
        return useraccount;
    }

    /**
     * Legt den Wert der useraccount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseraccount(String value) {
        this.useraccount = value;
    }

}
