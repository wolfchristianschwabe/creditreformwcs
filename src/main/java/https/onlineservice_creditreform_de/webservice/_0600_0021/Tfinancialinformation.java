
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tfinancialinformation complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tfinancialinformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="creditreformratingclass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="creditreformratingclassmeaning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="creditreformratingexpiry" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="creditreformratingclasstext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="factoringcreditreform" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="factoringother" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="tradecreditinsurance" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="informationtext" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tfinancialinformation", propOrder = {
    "creditreformratingclass",
    "creditreformratingclassmeaning",
    "creditreformratingexpiry",
    "creditreformratingclasstext",
    "factoringcreditreform",
    "factoringother",
    "tradecreditinsurance",
    "informationtext",
    "text"
})
public class Tfinancialinformation {

    protected String creditreformratingclass;
    protected String creditreformratingclassmeaning;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar creditreformratingexpiry;
    protected String creditreformratingclasstext;
    protected Boolean factoringcreditreform;
    protected Boolean factoringother;
    protected Boolean tradecreditinsurance;
    protected Ttext informationtext;
    protected Ttext text;

    /**
     * Ruft den Wert der creditreformratingclass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditreformratingclass() {
        return creditreformratingclass;
    }

    /**
     * Legt den Wert der creditreformratingclass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditreformratingclass(String value) {
        this.creditreformratingclass = value;
    }

    /**
     * Ruft den Wert der creditreformratingclassmeaning-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditreformratingclassmeaning() {
        return creditreformratingclassmeaning;
    }

    /**
     * Legt den Wert der creditreformratingclassmeaning-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditreformratingclassmeaning(String value) {
        this.creditreformratingclassmeaning = value;
    }

    /**
     * Ruft den Wert der creditreformratingexpiry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreditreformratingexpiry() {
        return creditreformratingexpiry;
    }

    /**
     * Legt den Wert der creditreformratingexpiry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreditreformratingexpiry(XMLGregorianCalendar value) {
        this.creditreformratingexpiry = value;
    }

    /**
     * Ruft den Wert der creditreformratingclasstext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreditreformratingclasstext() {
        return creditreformratingclasstext;
    }

    /**
     * Legt den Wert der creditreformratingclasstext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreditreformratingclasstext(String value) {
        this.creditreformratingclasstext = value;
    }

    /**
     * Ruft den Wert der factoringcreditreform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFactoringcreditreform() {
        return factoringcreditreform;
    }

    /**
     * Legt den Wert der factoringcreditreform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFactoringcreditreform(Boolean value) {
        this.factoringcreditreform = value;
    }

    /**
     * Ruft den Wert der factoringother-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFactoringother() {
        return factoringother;
    }

    /**
     * Legt den Wert der factoringother-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFactoringother(Boolean value) {
        this.factoringother = value;
    }

    /**
     * Ruft den Wert der tradecreditinsurance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTradecreditinsurance() {
        return tradecreditinsurance;
    }

    /**
     * Legt den Wert der tradecreditinsurance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTradecreditinsurance(Boolean value) {
        this.tradecreditinsurance = value;
    }

    /**
     * Ruft den Wert der informationtext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getInformationtext() {
        return informationtext;
    }

    /**
     * Legt den Wert der informationtext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setInformationtext(Ttext value) {
        this.informationtext = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

}
