
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tfault complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tfault">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}validationfault" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}servicefault"/>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tfault", propOrder = {
    "validationfault",
    "servicefault"
})
public class Tfault {

    protected List<String> validationfault;
    protected Tservicefault servicefault;

    /**
     * Gets the value of the validationfault property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the validationfault property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValidationfault().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getValidationfault() {
        if (validationfault == null) {
            validationfault = new ArrayList<String>();
        }
        return this.validationfault;
    }

    /**
     * Ruft den Wert der servicefault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tservicefault }
     *     
     */
    public Tservicefault getServicefault() {
        return servicefault;
    }

    /**
     * Legt den Wert der servicefault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tservicefault }
     *     
     */
    public void setServicefault(Tservicefault value) {
        this.servicefault = value;
    }

}
