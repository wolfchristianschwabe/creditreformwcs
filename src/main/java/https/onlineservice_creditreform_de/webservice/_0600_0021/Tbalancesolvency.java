
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tbalancesolvency complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tbalancesolvency">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="date" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;element name="class" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="classmeaning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="classexplanation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *         &lt;element name="pdaveragedate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="pdaverage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="pdaverageexplanation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pdranges" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="note" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tnote" minOccurs="0"/>
 *                   &lt;element name="pdrange" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="class" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="classmeaning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="informationtext" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tbalancesolvency", propOrder = {
    "date",
    "clazz",
    "classmeaning",
    "classexplanation",
    "trafficlight",
    "pdaveragedate",
    "pdaverage",
    "pdaverageexplanation",
    "pdranges",
    "informationtext",
    "text"
})
public class Tbalancesolvency {

    protected String date;
    @XmlElement(name = "class")
    protected String clazz;
    protected String classmeaning;
    protected String classexplanation;
    protected Tkeywithgrade trafficlight;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar pdaveragedate;
    protected BigDecimal pdaverage;
    protected String pdaverageexplanation;
    protected Tbalancesolvency.Pdranges pdranges;
    protected Ttext informationtext;
    protected Ttext text;

    /**
     * Ruft den Wert der date-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDate() {
        return date;
    }

    /**
     * Legt den Wert der date-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDate(String value) {
        this.date = value;
    }

    /**
     * Ruft den Wert der clazz-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClazz() {
        return clazz;
    }

    /**
     * Legt den Wert der clazz-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClazz(String value) {
        this.clazz = value;
    }

    /**
     * Ruft den Wert der classmeaning-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassmeaning() {
        return classmeaning;
    }

    /**
     * Legt den Wert der classmeaning-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassmeaning(String value) {
        this.classmeaning = value;
    }

    /**
     * Ruft den Wert der classexplanation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClassexplanation() {
        return classexplanation;
    }

    /**
     * Legt den Wert der classexplanation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClassexplanation(String value) {
        this.classexplanation = value;
    }

    /**
     * Ruft den Wert der trafficlight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getTrafficlight() {
        return trafficlight;
    }

    /**
     * Legt den Wert der trafficlight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setTrafficlight(Tkeywithgrade value) {
        this.trafficlight = value;
    }

    /**
     * Ruft den Wert der pdaveragedate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getPdaveragedate() {
        return pdaveragedate;
    }

    /**
     * Legt den Wert der pdaveragedate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setPdaveragedate(XMLGregorianCalendar value) {
        this.pdaveragedate = value;
    }

    /**
     * Ruft den Wert der pdaverage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPdaverage() {
        return pdaverage;
    }

    /**
     * Legt den Wert der pdaverage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPdaverage(BigDecimal value) {
        this.pdaverage = value;
    }

    /**
     * Ruft den Wert der pdaverageexplanation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPdaverageexplanation() {
        return pdaverageexplanation;
    }

    /**
     * Legt den Wert der pdaverageexplanation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPdaverageexplanation(String value) {
        this.pdaverageexplanation = value;
    }

    /**
     * Ruft den Wert der pdranges-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tbalancesolvency.Pdranges }
     *     
     */
    public Tbalancesolvency.Pdranges getPdranges() {
        return pdranges;
    }

    /**
     * Legt den Wert der pdranges-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tbalancesolvency.Pdranges }
     *     
     */
    public void setPdranges(Tbalancesolvency.Pdranges value) {
        this.pdranges = value;
    }

    /**
     * Ruft den Wert der informationtext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getInformationtext() {
        return informationtext;
    }

    /**
     * Legt den Wert der informationtext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setInformationtext(Ttext value) {
        this.informationtext = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="note" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tnote" minOccurs="0"/>
     *         &lt;element name="pdrange" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="class" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="classmeaning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "note",
        "pdrange"
    })
    public static class Pdranges {

        protected Tnote note;
        protected List<Tbalancesolvency.Pdranges.Pdrange> pdrange;

        /**
         * Ruft den Wert der note-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tnote }
         *     
         */
        public Tnote getNote() {
            return note;
        }

        /**
         * Legt den Wert der note-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tnote }
         *     
         */
        public void setNote(Tnote value) {
            this.note = value;
        }

        /**
         * Gets the value of the pdrange property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the pdrange property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPdrange().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tbalancesolvency.Pdranges.Pdrange }
         * 
         * 
         */
        public List<Tbalancesolvency.Pdranges.Pdrange> getPdrange() {
            if (pdrange == null) {
                pdrange = new ArrayList<Tbalancesolvency.Pdranges.Pdrange>();
            }
            return this.pdrange;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="class" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="classmeaning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "clazz",
            "classmeaning"
        })
        public static class Pdrange {

            @XmlElement(name = "class")
            protected String clazz;
            protected String classmeaning;

            /**
             * Ruft den Wert der clazz-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClazz() {
                return clazz;
            }

            /**
             * Legt den Wert der clazz-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClazz(String value) {
                this.clazz = value;
            }

            /**
             * Ruft den Wert der classmeaning-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getClassmeaning() {
                return classmeaning;
            }

            /**
             * Legt den Wert der classmeaning-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setClassmeaning(String value) {
                this.classmeaning = value;
            }

        }

    }

}
