
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tancillaryinformationcooperative complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tancillaryinformationcooperative">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capital" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *         &lt;element name="capitaltext" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="members" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="membersdecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="memberstext" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="share" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *         &lt;element name="shares" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="sharesdecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="sharestext" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tancillaryinformationcooperative", propOrder = {
    "capital",
    "capitaltext",
    "members",
    "membersdecoration",
    "memberstext",
    "share",
    "shares",
    "sharesdecoration",
    "sharestext"
})
public class Tancillaryinformationcooperative {

    protected Tamount capital;
    protected Ttext capitaltext;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger members;
    protected Tkey membersdecoration;
    protected Ttext memberstext;
    protected Tamount share;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger shares;
    protected Tkey sharesdecoration;
    protected Ttext sharestext;

    /**
     * Ruft den Wert der capital-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamount }
     *     
     */
    public Tamount getCapital() {
        return capital;
    }

    /**
     * Legt den Wert der capital-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamount }
     *     
     */
    public void setCapital(Tamount value) {
        this.capital = value;
    }

    /**
     * Ruft den Wert der capitaltext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getCapitaltext() {
        return capitaltext;
    }

    /**
     * Legt den Wert der capitaltext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setCapitaltext(Ttext value) {
        this.capitaltext = value;
    }

    /**
     * Ruft den Wert der members-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMembers() {
        return members;
    }

    /**
     * Legt den Wert der members-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMembers(BigInteger value) {
        this.members = value;
    }

    /**
     * Ruft den Wert der membersdecoration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getMembersdecoration() {
        return membersdecoration;
    }

    /**
     * Legt den Wert der membersdecoration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setMembersdecoration(Tkey value) {
        this.membersdecoration = value;
    }

    /**
     * Ruft den Wert der memberstext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getMemberstext() {
        return memberstext;
    }

    /**
     * Legt den Wert der memberstext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setMemberstext(Ttext value) {
        this.memberstext = value;
    }

    /**
     * Ruft den Wert der share-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamount }
     *     
     */
    public Tamount getShare() {
        return share;
    }

    /**
     * Legt den Wert der share-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamount }
     *     
     */
    public void setShare(Tamount value) {
        this.share = value;
    }

    /**
     * Ruft den Wert der shares-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getShares() {
        return shares;
    }

    /**
     * Legt den Wert der shares-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setShares(BigInteger value) {
        this.shares = value;
    }

    /**
     * Ruft den Wert der sharesdecoration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getSharesdecoration() {
        return sharesdecoration;
    }

    /**
     * Legt den Wert der sharesdecoration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setSharesdecoration(Tkey value) {
        this.sharesdecoration = value;
    }

    /**
     * Ruft den Wert der sharestext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getSharestext() {
        return sharestext;
    }

    /**
     * Legt den Wert der sharestext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setSharestext(Ttext value) {
        this.sharestext = value;
    }

}
