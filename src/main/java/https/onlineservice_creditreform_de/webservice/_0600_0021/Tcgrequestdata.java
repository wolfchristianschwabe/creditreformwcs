
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tcgrequestdata complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcgrequestdata">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="firstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="birthname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateofbirth" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="addressone" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgresponseaddress" minOccurs="0"/>
 *         &lt;element name="addresstwo" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgresponseaddress" minOccurs="0"/>
 *         &lt;element name="salutation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="legitimateinterest" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcgrequestdata", propOrder = {
    "surname",
    "firstname",
    "birthname",
    "dateofbirth",
    "addressone",
    "addresstwo",
    "salutation",
    "legitimateinterest"
})
public class Tcgrequestdata {

    protected String surname;
    protected String firstname;
    protected String birthname;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateofbirth;
    protected Tcgresponseaddress addressone;
    protected Tcgresponseaddress addresstwo;
    protected Tkey salutation;
    protected Tkey legitimateinterest;

    /**
     * Ruft den Wert der surname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Legt den Wert der surname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurname(String value) {
        this.surname = value;
    }

    /**
     * Ruft den Wert der firstname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Legt den Wert der firstname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstname(String value) {
        this.firstname = value;
    }

    /**
     * Ruft den Wert der birthname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthname() {
        return birthname;
    }

    /**
     * Legt den Wert der birthname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthname(String value) {
        this.birthname = value;
    }

    /**
     * Ruft den Wert der dateofbirth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateofbirth() {
        return dateofbirth;
    }

    /**
     * Legt den Wert der dateofbirth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateofbirth(XMLGregorianCalendar value) {
        this.dateofbirth = value;
    }

    /**
     * Ruft den Wert der addressone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgresponseaddress }
     *     
     */
    public Tcgresponseaddress getAddressone() {
        return addressone;
    }

    /**
     * Legt den Wert der addressone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgresponseaddress }
     *     
     */
    public void setAddressone(Tcgresponseaddress value) {
        this.addressone = value;
    }

    /**
     * Ruft den Wert der addresstwo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgresponseaddress }
     *     
     */
    public Tcgresponseaddress getAddresstwo() {
        return addresstwo;
    }

    /**
     * Legt den Wert der addresstwo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgresponseaddress }
     *     
     */
    public void setAddresstwo(Tcgresponseaddress value) {
        this.addresstwo = value;
    }

    /**
     * Ruft den Wert der salutation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getSalutation() {
        return salutation;
    }

    /**
     * Legt den Wert der salutation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setSalutation(Tkey value) {
        this.salutation = value;
    }

    /**
     * Ruft den Wert der legitimateinterest-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getLegitimateinterest() {
        return legitimateinterest;
    }

    /**
     * Legt den Wert der legitimateinterest-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setLegitimateinterest(Tkey value) {
        this.legitimateinterest = value;
    }

}
