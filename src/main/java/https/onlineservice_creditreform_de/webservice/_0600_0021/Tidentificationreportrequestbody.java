
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tidentificationreportrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tidentificationreportrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="legitimateinterest" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeylegitimateinterest"/>
 *         &lt;element name="reportlanguage" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlanguagerequest"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeyproduct"/>
 *         &lt;element name="solvencyindexthreshold" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="600"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element name="companyname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcompanyname" minOccurs="0"/>
 *         &lt;element name="street" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstreet" minOccurs="0"/>
 *         &lt;element name="housenumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Thousenumber" minOccurs="0"/>
 *         &lt;element name="housenumberaffix" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Thousenumberaffix" minOccurs="0"/>
 *         &lt;element name="postcode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpostcode" minOccurs="0"/>
 *         &lt;element name="city" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcity" minOccurs="0"/>
 *         &lt;element name="country" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcountry"/>
 *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeylegalform" minOccurs="0"/>
 *         &lt;element name="phone" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tphonerequest" minOccurs="0"/>
 *         &lt;element name="website" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;maxLength value="50"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="registertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeyregistertype" minOccurs="0"/>
 *         &lt;element name="registerid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="vatid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tidentificationreportrequestbody", propOrder = {
    "legitimateinterest",
    "reportlanguage",
    "producttype",
    "solvencyindexthreshold",
    "customerreference",
    "companyname",
    "street",
    "housenumber",
    "housenumberaffix",
    "postcode",
    "city",
    "country",
    "legalform",
    "phone",
    "website",
    "registertype",
    "registerid",
    "vatid"
})
public class Tidentificationreportrequestbody {

    @XmlElement(required = true)
    protected String legitimateinterest;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected Tlanguagerequest reportlanguage;
    @XmlElement(required = true)
    protected String producttype;
    protected Integer solvencyindexthreshold;
    protected String customerreference;
    protected String companyname;
    protected String street;
    @XmlSchemaType(name = "integer")
    protected Integer housenumber;
    protected String housenumberaffix;
    protected String postcode;
    protected String city;
    @XmlElement(required = true)
    protected String country;
    protected String legalform;
    protected Tphonerequest phone;
    protected String website;
    protected String registertype;
    protected String registerid;
    protected String vatid;

    /**
     * Ruft den Wert der legitimateinterest-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegitimateinterest() {
        return legitimateinterest;
    }

    /**
     * Legt den Wert der legitimateinterest-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegitimateinterest(String value) {
        this.legitimateinterest = value;
    }

    /**
     * Ruft den Wert der reportlanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tlanguagerequest }
     *     
     */
    public Tlanguagerequest getReportlanguage() {
        return reportlanguage;
    }

    /**
     * Legt den Wert der reportlanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tlanguagerequest }
     *     
     */
    public void setReportlanguage(Tlanguagerequest value) {
        this.reportlanguage = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducttype(String value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der solvencyindexthreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSolvencyindexthreshold() {
        return solvencyindexthreshold;
    }

    /**
     * Legt den Wert der solvencyindexthreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSolvencyindexthreshold(Integer value) {
        this.solvencyindexthreshold = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der companyname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyname() {
        return companyname;
    }

    /**
     * Legt den Wert der companyname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyname(String value) {
        this.companyname = value;
    }

    /**
     * Ruft den Wert der street-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Legt den Wert der street-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Ruft den Wert der housenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHousenumber() {
        return housenumber;
    }

    /**
     * Legt den Wert der housenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHousenumber(Integer value) {
        this.housenumber = value;
    }

    /**
     * Ruft den Wert der housenumberaffix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHousenumberaffix() {
        return housenumberaffix;
    }

    /**
     * Legt den Wert der housenumberaffix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHousenumberaffix(String value) {
        this.housenumberaffix = value;
    }

    /**
     * Ruft den Wert der postcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Legt den Wert der postcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostcode(String value) {
        this.postcode = value;
    }

    /**
     * Ruft den Wert der city-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Legt den Wert der city-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Ruft den Wert der country-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Legt den Wert der country-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Ruft den Wert der legalform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegalform() {
        return legalform;
    }

    /**
     * Legt den Wert der legalform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegalform(String value) {
        this.legalform = value;
    }

    /**
     * Ruft den Wert der phone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tphonerequest }
     *     
     */
    public Tphonerequest getPhone() {
        return phone;
    }

    /**
     * Legt den Wert der phone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tphonerequest }
     *     
     */
    public void setPhone(Tphonerequest value) {
        this.phone = value;
    }

    /**
     * Ruft den Wert der website-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWebsite() {
        return website;
    }

    /**
     * Legt den Wert der website-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWebsite(String value) {
        this.website = value;
    }

    /**
     * Ruft den Wert der registertype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegistertype() {
        return registertype;
    }

    /**
     * Legt den Wert der registertype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegistertype(String value) {
        this.registertype = value;
    }

    /**
     * Ruft den Wert der registerid-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterid() {
        return registerid;
    }

    /**
     * Legt den Wert der registerid-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterid(String value) {
        this.registerid = value;
    }

    /**
     * Ruft den Wert der vatid-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVatid() {
        return vatid;
    }

    /**
     * Legt den Wert der vatid-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVatid(String value) {
        this.vatid = value;
    }

}
