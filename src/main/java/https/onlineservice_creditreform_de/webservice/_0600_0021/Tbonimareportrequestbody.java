
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tbonimareportrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tbonimareportrequestbody">
 *   &lt;complexContent>
 *     &lt;extension base="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgreportrequestbody">
 *       &lt;sequence>
 *         &lt;element name="firstname2" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfirstname" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tbonimareportrequestbody", propOrder = {
    "firstname2"
})
public class Tbonimareportrequestbody
    extends Tcgreportrequestbody
{

    protected String firstname2;

    /**
     * Ruft den Wert der firstname2-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstname2() {
        return firstname2;
    }

    /**
     * Legt den Wert der firstname2-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstname2(String value) {
        this.firstname2 = value;
    }

}
