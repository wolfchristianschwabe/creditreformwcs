
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Torderresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Torderresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}referencenumber"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Torderresponsebody", propOrder = {
    "referencenumber"
})
public class Torderresponsebody {

    protected long referencenumber;

    /**
     * Ruft den Wert der referencenumber-Eigenschaft ab.
     * 
     */
    public long getReferencenumber() {
        return referencenumber;
    }

    /**
     * Legt den Wert der referencenumber-Eigenschaft fest.
     * 
     */
    public void setReferencenumber(long value) {
        this.referencenumber = value;
    }

}
