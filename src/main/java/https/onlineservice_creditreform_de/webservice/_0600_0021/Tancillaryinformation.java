
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tancillaryinformation complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tancillaryinformation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;choice minOccurs="0">
 *           &lt;element name="ancillaryinformationshares" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tancillaryinformationshares" minOccurs="0"/>
 *           &lt;element name="ancillaryinformationcooperative" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tancillaryinformationcooperative" minOccurs="0"/>
 *           &lt;element name="ancillaryinformationcapital" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tancillaryinformationcapital" minOccurs="0"/>
 *           &lt;element name="ancillaryinformationassociation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tancillaryinformationassociation" minOccurs="0"/>
 *           &lt;element name="ancillaryinformationothers" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tancillaryinformationothers" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tancillaryinformation", propOrder = {
    "ancillaryinformationshares",
    "ancillaryinformationcooperative",
    "ancillaryinformationcapital",
    "ancillaryinformationassociation",
    "ancillaryinformationothers",
    "text"
})
public class Tancillaryinformation {

    protected Tancillaryinformationshares ancillaryinformationshares;
    protected Tancillaryinformationcooperative ancillaryinformationcooperative;
    protected Tancillaryinformationcapital ancillaryinformationcapital;
    protected Tancillaryinformationassociation ancillaryinformationassociation;
    protected Tancillaryinformationothers ancillaryinformationothers;
    protected Ttext text;

    /**
     * Ruft den Wert der ancillaryinformationshares-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tancillaryinformationshares }
     *     
     */
    public Tancillaryinformationshares getAncillaryinformationshares() {
        return ancillaryinformationshares;
    }

    /**
     * Legt den Wert der ancillaryinformationshares-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tancillaryinformationshares }
     *     
     */
    public void setAncillaryinformationshares(Tancillaryinformationshares value) {
        this.ancillaryinformationshares = value;
    }

    /**
     * Ruft den Wert der ancillaryinformationcooperative-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tancillaryinformationcooperative }
     *     
     */
    public Tancillaryinformationcooperative getAncillaryinformationcooperative() {
        return ancillaryinformationcooperative;
    }

    /**
     * Legt den Wert der ancillaryinformationcooperative-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tancillaryinformationcooperative }
     *     
     */
    public void setAncillaryinformationcooperative(Tancillaryinformationcooperative value) {
        this.ancillaryinformationcooperative = value;
    }

    /**
     * Ruft den Wert der ancillaryinformationcapital-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tancillaryinformationcapital }
     *     
     */
    public Tancillaryinformationcapital getAncillaryinformationcapital() {
        return ancillaryinformationcapital;
    }

    /**
     * Legt den Wert der ancillaryinformationcapital-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tancillaryinformationcapital }
     *     
     */
    public void setAncillaryinformationcapital(Tancillaryinformationcapital value) {
        this.ancillaryinformationcapital = value;
    }

    /**
     * Ruft den Wert der ancillaryinformationassociation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tancillaryinformationassociation }
     *     
     */
    public Tancillaryinformationassociation getAncillaryinformationassociation() {
        return ancillaryinformationassociation;
    }

    /**
     * Legt den Wert der ancillaryinformationassociation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tancillaryinformationassociation }
     *     
     */
    public void setAncillaryinformationassociation(Tancillaryinformationassociation value) {
        this.ancillaryinformationassociation = value;
    }

    /**
     * Ruft den Wert der ancillaryinformationothers-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tancillaryinformationothers }
     *     
     */
    public Tancillaryinformationothers getAncillaryinformationothers() {
        return ancillaryinformationothers;
    }

    /**
     * Legt den Wert der ancillaryinformationothers-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tancillaryinformationothers }
     *     
     */
    public void setAncillaryinformationothers(Tancillaryinformationothers value) {
        this.ancillaryinformationothers = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

}
