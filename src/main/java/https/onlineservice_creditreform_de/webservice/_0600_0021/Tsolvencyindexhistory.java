
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tsolvencyindexhistory complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tsolvencyindexhistory">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="solvencyindex" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="600"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="history" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="entry" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyindexhistoryentry" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="best" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyindexhistoryentry" minOccurs="0"/>
 *         &lt;element name="worst" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyindexhistoryentry" minOccurs="0"/>
 *         &lt;element name="average" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="600"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tsolvencyindexhistory", propOrder = {
    "solvencyindex",
    "history",
    "best",
    "worst",
    "average",
    "text"
})
public class Tsolvencyindexhistory {

    protected Integer solvencyindex;
    protected Tsolvencyindexhistory.History history;
    protected Tsolvencyindexhistoryentry best;
    protected Tsolvencyindexhistoryentry worst;
    protected Integer average;
    protected Ttext text;

    /**
     * Ruft den Wert der solvencyindex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSolvencyindex() {
        return solvencyindex;
    }

    /**
     * Legt den Wert der solvencyindex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSolvencyindex(Integer value) {
        this.solvencyindex = value;
    }

    /**
     * Ruft den Wert der history-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tsolvencyindexhistory.History }
     *     
     */
    public Tsolvencyindexhistory.History getHistory() {
        return history;
    }

    /**
     * Legt den Wert der history-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tsolvencyindexhistory.History }
     *     
     */
    public void setHistory(Tsolvencyindexhistory.History value) {
        this.history = value;
    }

    /**
     * Ruft den Wert der best-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tsolvencyindexhistoryentry }
     *     
     */
    public Tsolvencyindexhistoryentry getBest() {
        return best;
    }

    /**
     * Legt den Wert der best-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tsolvencyindexhistoryentry }
     *     
     */
    public void setBest(Tsolvencyindexhistoryentry value) {
        this.best = value;
    }

    /**
     * Ruft den Wert der worst-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tsolvencyindexhistoryentry }
     *     
     */
    public Tsolvencyindexhistoryentry getWorst() {
        return worst;
    }

    /**
     * Legt den Wert der worst-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tsolvencyindexhistoryentry }
     *     
     */
    public void setWorst(Tsolvencyindexhistoryentry value) {
        this.worst = value;
    }

    /**
     * Ruft den Wert der average-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAverage() {
        return average;
    }

    /**
     * Legt den Wert der average-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAverage(Integer value) {
        this.average = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="entry" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyindexhistoryentry" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "entry"
    })
    public static class History {

        protected List<Tsolvencyindexhistoryentry> entry;

        /**
         * Gets the value of the entry property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the entry property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEntry().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tsolvencyindexhistoryentry }
         * 
         * 
         */
        public List<Tsolvencyindexhistoryentry> getEntry() {
            if (entry == null) {
                entry = new ArrayList<Tsolvencyindexhistoryentry>();
            }
            return this.entry;
        }

    }

}
