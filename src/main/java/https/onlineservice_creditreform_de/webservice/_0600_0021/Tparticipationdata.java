
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipationdata complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipationdata">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="shareholdercapital" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationdataactiveformer" minOccurs="0"/>
 *         &lt;element name="deputymanagement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationdataactiveformer" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipationdata", propOrder = {
    "shareholdercapital",
    "deputymanagement"
})
public class Tparticipationdata {

    protected Tparticipationdataactiveformer shareholdercapital;
    protected Tparticipationdataactiveformer deputymanagement;

    /**
     * Ruft den Wert der shareholdercapital-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tparticipationdataactiveformer }
     *     
     */
    public Tparticipationdataactiveformer getShareholdercapital() {
        return shareholdercapital;
    }

    /**
     * Legt den Wert der shareholdercapital-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tparticipationdataactiveformer }
     *     
     */
    public void setShareholdercapital(Tparticipationdataactiveformer value) {
        this.shareholdercapital = value;
    }

    /**
     * Ruft den Wert der deputymanagement-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tparticipationdataactiveformer }
     *     
     */
    public Tparticipationdataactiveformer getDeputymanagement() {
        return deputymanagement;
    }

    /**
     * Legt den Wert der deputymanagement-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tparticipationdataactiveformer }
     *     
     */
    public void setDeputymanagement(Tparticipationdataactiveformer value) {
        this.deputymanagement = value;
    }

}
