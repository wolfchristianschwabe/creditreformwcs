
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the https.onlineservice_creditreform_de.webservice._0600_0021 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CollectionfilesearchFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "collectionfilesearchFault");
    private final static QName _Easynumber_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "easynumber");
    private final static QName _CollectionaccountFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "collectionaccountFault");
    private final static QName _KeylistFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "keylistFault");
    private final static QName _ParticipationdataFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "participationdataFault");
    private final static QName _UpgradelistFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "upgradelistFault");
    private final static QName _BonimareportFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "bonimareportFault");
    private final static QName _ParticipationdatastartFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "participationdatastartFault");
    private final static QName _MonitoringstatusFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "monitoringstatusFault");
    private final static QName _MailboxentryFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "mailboxentryFault");
    private final static QName _CollectionagencydocumentFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "collectionagencydocumentFault");
    private final static QName _Status_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "status");
    private final static QName _Identificationnumber_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "identificationnumber");
    private final static QName _BalanceanalysissinglecompanyFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "balanceanalysissinglecompanyFault");
    private final static QName _IdentificationnumbermapFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "identificationnumbermapFault");
    private final static QName _CollectionagencyfreetextmessageFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "collectionagencyfreetextmessageFault");
    private final static QName _Validationfault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "validationfault");
    private final static QName _MailboxdirectoryFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "mailboxdirectoryFault");
    private final static QName _ReceivableFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "receivableFault");
    private final static QName _ChangepasswordFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "changepasswordFault");
    private final static QName _ReportFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "reportFault");
    private final static QName _StatementreceivablesFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "statementreceivablesFault");
    private final static QName _SearchFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "searchFault");
    private final static QName _ChangeextendedmonitoringFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "changeextendedmonitoringFault");
    private final static QName _TitledreceivableFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "titledreceivableFault");
    private final static QName _Referencenumber_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "referencenumber");
    private final static QName _CancelstandardmonitoringFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "cancelstandardmonitoringFault");
    private final static QName _LogonFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "logonFault");
    private final static QName _PaymentsFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "paymentsFault");
    private final static QName _CreditorfreetextmessagequeryFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "creditorfreetextmessagequeryFault");
    private final static QName _ChangeemailFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "changeemailFault");
    private final static QName _CollectionorderFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "collectionorderFault");
    private final static QName _ProcessFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "processFault");
    private final static QName _MigrationdiffFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "migrationdiffFault");
    private final static QName _OrderFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "orderFault");
    private final static QName _IdentificationreportFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "identificationreportFault");
    private final static QName _Mailboxentrynumber_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "mailboxentrynumber");
    private final static QName _Servicefault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "servicefault");
    private final static QName _GeneralinformationFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "generalinformationFault");
    private final static QName _ProductavailabilityFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "productavailabilityFault");
    private final static QName _Participationdatareference_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "participationdatareference");
    private final static QName _ForwardreceivableFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "forwardreceivableFault");
    private final static QName _ConsumerreportFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "consumerreportFault");
    private final static QName _CreditorfreetextmessageFault_QNAME = new QName("https://onlineservice.creditreform.de/webservice/0600-0021", "creditorfreetextmessageFault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: https.onlineservice_creditreform_de.webservice._0600_0021
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Tcgcollectiondata }
     * 
     */
    public Tcgcollectiondata createTcgcollectiondata() {
        return new Tcgcollectiondata();
    }

    /**
     * Create an instance of {@link Tcgcollectiondata.Longterm }
     * 
     */
    public Tcgcollectiondata.Longterm createTcgcollectiondataLongterm() {
        return new Tcgcollectiondata.Longterm();
    }

    /**
     * Create an instance of {@link Tcgcollectiondata.Regular }
     * 
     */
    public Tcgcollectiondata.Regular createTcgcollectiondataRegular() {
        return new Tcgcollectiondata.Regular();
    }

    /**
     * Create an instance of {@link Tcollectionnegativefacts }
     * 
     */
    public Tcollectionnegativefacts createTcollectionnegativefacts() {
        return new Tcollectionnegativefacts();
    }

    /**
     * Create an instance of {@link Tstaffgroup }
     * 
     */
    public Tstaffgroup createTstaffgroup() {
        return new Tstaffgroup();
    }

    /**
     * Create an instance of {@link Trealestates }
     * 
     */
    public Trealestates createTrealestates() {
        return new Trealestates();
    }

    /**
     * Create an instance of {@link Trealestates.Realestate }
     * 
     */
    public Trealestates.Realestate createTrealestatesRealestate() {
        return new Trealestates.Realestate();
    }

    /**
     * Create an instance of {@link Tsolvencyindexhistory }
     * 
     */
    public Tsolvencyindexhistory createTsolvencyindexhistory() {
        return new Tsolvencyindexhistory();
    }

    /**
     * Create an instance of {@link Tbalancesolvency }
     * 
     */
    public Tbalancesolvency createTbalancesolvency() {
        return new Tbalancesolvency();
    }

    /**
     * Create an instance of {@link Tbalancesolvency.Pdranges }
     * 
     */
    public Tbalancesolvency.Pdranges createTbalancesolvencyPdranges() {
        return new Tbalancesolvency.Pdranges();
    }

    /**
     * Create an instance of {@link Tprofitslossessheet }
     * 
     */
    public Tprofitslossessheet createTprofitslossessheet() {
        return new Tprofitslossessheet();
    }

    /**
     * Create an instance of {@link Tbranch }
     * 
     */
    public Tbranch createTbranch() {
        return new Tbranch();
    }

    /**
     * Create an instance of {@link Tbranch.Typeofbranch }
     * 
     */
    public Tbranch.Typeofbranch createTbranchTypeofbranch() {
        return new Tbranch.Typeofbranch();
    }

    /**
     * Create an instance of {@link Tbranch.Typeofbranch.Branch }
     * 
     */
    public Tbranch.Typeofbranch.Branch createTbranchTypeofbranchBranch() {
        return new Tbranch.Typeofbranch.Branch();
    }

    /**
     * Create an instance of {@link Tparameterlist }
     * 
     */
    public Tparameterlist createTparameterlist() {
        return new Tparameterlist();
    }

    /**
     * Create an instance of {@link Trealestateamounts }
     * 
     */
    public Trealestateamounts createTrealestateamounts() {
        return new Trealestateamounts();
    }

    /**
     * Create an instance of {@link Tbalanceanalysismultiplecompaniesresponsemaincompany }
     * 
     */
    public Tbalanceanalysismultiplecompaniesresponsemaincompany createTbalanceanalysismultiplecompaniesresponsemaincompany() {
        return new Tbalanceanalysismultiplecompaniesresponsemaincompany();
    }

    /**
     * Create an instance of {@link Tbalanceanalysismultiplecompaniesresponsemaincompany.Xbrl }
     * 
     */
    public Tbalanceanalysismultiplecompaniesresponsemaincompany.Xbrl createTbalanceanalysismultiplecompaniesresponsemaincompanyXbrl() {
        return new Tbalanceanalysismultiplecompaniesresponsemaincompany.Xbrl();
    }

    /**
     * Create an instance of {@link Tcgscoredata }
     * 
     */
    public Tcgscoredata createTcgscoredata() {
        return new Tcgscoredata();
    }

    /**
     * Create an instance of {@link Tparticipation }
     * 
     */
    public Tparticipation createTparticipation() {
        return new Tparticipation();
    }

    /**
     * Create an instance of {@link Tcollectionresponsecommunication }
     * 
     */
    public Tcollectionresponsecommunication createTcollectionresponsecommunication() {
        return new Tcollectionresponsecommunication();
    }

    /**
     * Create an instance of {@link Tbalance }
     * 
     */
    public Tbalance createTbalance() {
        return new Tbalance();
    }

    /**
     * Create an instance of {@link Tcgpaymentbehaviour }
     * 
     */
    public Tcgpaymentbehaviour createTcgpaymentbehaviour() {
        return new Tcgpaymentbehaviour();
    }

    /**
     * Create an instance of {@link Tbalanceanalysismultiplecompaniesresponseadditionalcompany }
     * 
     */
    public Tbalanceanalysismultiplecompaniesresponseadditionalcompany createTbalanceanalysismultiplecompaniesresponseadditionalcompany() {
        return new Tbalanceanalysismultiplecompaniesresponseadditionalcompany();
    }

    /**
     * Create an instance of {@link Tbalanceanalysismultiplecompaniesresponseadditionalcompany.Xbrl }
     * 
     */
    public Tbalanceanalysismultiplecompaniesresponseadditionalcompany.Xbrl createTbalanceanalysismultiplecompaniesresponseadditionalcompanyXbrl() {
        return new Tbalanceanalysismultiplecompaniesresponseadditionalcompany.Xbrl();
    }

    /**
     * Create an instance of {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tfinancialstatementfiguresabstract.Figures }
     * 
     */
    public https.onlineservice_creditreform_de.webservice._0600_0021.Tfinancialstatementfiguresabstract.Figures createTfinancialstatementfiguresabstractFigures() {
        return new https.onlineservice_creditreform_de.webservice._0600_0021.Tfinancialstatementfiguresabstract.Figures();
    }

    /**
     * Create an instance of {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tfinancialstatementfiguresabstract.Figures.Chapter }
     * 
     */
    public https.onlineservice_creditreform_de.webservice._0600_0021.Tfinancialstatementfiguresabstract.Figures.Chapter createTfinancialstatementfiguresabstractFiguresChapter() {
        return new https.onlineservice_creditreform_de.webservice._0600_0021.Tfinancialstatementfiguresabstract.Figures.Chapter();
    }

    /**
     * Create an instance of {@link Tshareholder }
     * 
     */
    public Tshareholder createTshareholder() {
        return new Tshareholder();
    }

    /**
     * Create an instance of {@link Tcollectionresponsedebtor }
     * 
     */
    public Tcollectionresponsedebtor createTcollectionresponsedebtor() {
        return new Tcollectionresponsedebtor();
    }

    /**
     * Create an instance of {@link Tcollectionresponsedebtor.Company }
     * 
     */
    public Tcollectionresponsedebtor.Company createTcollectionresponsedebtorCompany() {
        return new Tcollectionresponsedebtor.Company();
    }

    /**
     * Create an instance of {@link Tcollectionrequestinterest }
     * 
     */
    public Tcollectionrequestinterest createTcollectionrequestinterest() {
        return new Tcollectionrequestinterest();
    }

    /**
     * Create an instance of {@link Tcollectionrequestpartreceivable }
     * 
     */
    public Tcollectionrequestpartreceivable createTcollectionrequestpartreceivable() {
        return new Tcollectionrequestpartreceivable();
    }

    /**
     * Create an instance of {@link Tcgaddressdata }
     * 
     */
    public Tcgaddressdata createTcgaddressdata() {
        return new Tcgaddressdata();
    }

    /**
     * Create an instance of {@link Trequestdata }
     * 
     */
    public Trequestdata createTrequestdata() {
        return new Trequestdata();
    }

    /**
     * Create an instance of {@link Tancillaryinformationothers }
     * 
     */
    public Tancillaryinformationothers createTancillaryinformationothers() {
        return new Tancillaryinformationothers();
    }

    /**
     * Create an instance of {@link Tparticipationsparticipants }
     * 
     */
    public Tparticipationsparticipants createTparticipationsparticipants() {
        return new Tparticipationsparticipants();
    }

    /**
     * Create an instance of {@link Tparticipationsparticipants.Participation }
     * 
     */
    public Tparticipationsparticipants.Participation createTparticipationsparticipantsParticipation() {
        return new Tparticipationsparticipants.Participation();
    }

    /**
     * Create an instance of {@link Tlocations }
     * 
     */
    public Tlocations createTlocations() {
        return new Tlocations();
    }

    /**
     * Create an instance of {@link Tparticipationscompany }
     * 
     */
    public Tparticipationscompany createTparticipationscompany() {
        return new Tparticipationscompany();
    }

    /**
     * Create an instance of {@link Tevaluationsfinanceindustry }
     * 
     */
    public Tevaluationsfinanceindustry createTevaluationsfinanceindustry() {
        return new Tevaluationsfinanceindustry();
    }

    /**
     * Create an instance of {@link Tevaluationsfinanceindustry.Evaluator }
     * 
     */
    public Tevaluationsfinanceindustry.Evaluator createTevaluationsfinanceindustryEvaluator() {
        return new Tevaluationsfinanceindustry.Evaluator();
    }

    /**
     * Create an instance of {@link Tstaffcompany }
     * 
     */
    public Tstaffcompany createTstaffcompany() {
        return new Tstaffcompany();
    }

    /**
     * Create an instance of {@link Tstaffcompany.Fiscalyear }
     * 
     */
    public Tstaffcompany.Fiscalyear createTstaffcompanyFiscalyear() {
        return new Tstaffcompany.Fiscalyear();
    }

    /**
     * Create an instance of {@link Tbanks }
     * 
     */
    public Tbanks createTbanks() {
        return new Tbanks();
    }

    /**
     * Create an instance of {@link Tfoundation }
     * 
     */
    public Tfoundation createTfoundation() {
        return new Tfoundation();
    }

    /**
     * Create an instance of {@link Tturnovercompany }
     * 
     */
    public Tturnovercompany createTturnovercompany() {
        return new Tturnovercompany();
    }

    /**
     * Create an instance of {@link Tturnovercompany.Fiscalyear }
     * 
     */
    public Tturnovercompany.Fiscalyear createTturnovercompanyFiscalyear() {
        return new Tturnovercompany.Fiscalyear();
    }

    /**
     * Create an instance of {@link Tnegativefact }
     * 
     */
    public Tnegativefact createTnegativefact() {
        return new Tnegativefact();
    }

    /**
     * Create an instance of {@link Tnegativefact.Enforcements }
     * 
     */
    public Tnegativefact.Enforcements createTnegativefactEnforcements() {
        return new Tnegativefact.Enforcements();
    }

    /**
     * Create an instance of {@link Tnegativefact.Negativefact }
     * 
     */
    public Tnegativefact.Negativefact createTnegativefactNegativefact() {
        return new Tnegativefact.Negativefact();
    }

    /**
     * Create an instance of {@link Tcgtelecomdata }
     * 
     */
    public Tcgtelecomdata createTcgtelecomdata() {
        return new Tcgtelecomdata();
    }

    /**
     * Create an instance of {@link Tsolvencyfinanceindustry }
     * 
     */
    public Tsolvencyfinanceindustry createTsolvencyfinanceindustry() {
        return new Tsolvencyfinanceindustry();
    }

    /**
     * Create an instance of {@link Tsolvencyfinanceindustry.Assignments }
     * 
     */
    public Tsolvencyfinanceindustry.Assignments createTsolvencyfinanceindustryAssignments() {
        return new Tsolvencyfinanceindustry.Assignments();
    }

    /**
     * Create an instance of {@link Tcollectionresponseaddress }
     * 
     */
    public Tcollectionresponseaddress createTcollectionresponseaddress() {
        return new Tcollectionresponseaddress();
    }

    /**
     * Create an instance of {@link Tregisterhistory }
     * 
     */
    public Tregisterhistory createTregisterhistory() {
        return new Tregisterhistory();
    }

    /**
     * Create an instance of {@link Tregisterhistory.Historyentry }
     * 
     */
    public Tregisterhistory.Historyentry createTregisterhistoryHistoryentry() {
        return new Tregisterhistory.Historyentry();
    }

    /**
     * Create an instance of {@link Tregisterhistory.Historyentry.Contract }
     * 
     */
    public Tregisterhistory.Historyentry.Contract createTregisterhistoryHistoryentryContract() {
        return new Tregisterhistory.Historyentry.Contract();
    }

    /**
     * Create an instance of {@link Tcollectionrequestdebtor }
     * 
     */
    public Tcollectionrequestdebtor createTcollectionrequestdebtor() {
        return new Tcollectionrequestdebtor();
    }

    /**
     * Create an instance of {@link Tcollectionrequestdebtor.Company }
     * 
     */
    public Tcollectionrequestdebtor.Company createTcollectionrequestdebtorCompany() {
        return new Tcollectionrequestdebtor.Company();
    }

    /**
     * Create an instance of {@link Tcgnegativefacts }
     * 
     */
    public Tcgnegativefacts createTcgnegativefacts() {
        return new Tcgnegativefacts();
    }

    /**
     * Create an instance of {@link Tcgnegativefacts.Negativefact }
     * 
     */
    public Tcgnegativefacts.Negativefact createTcgnegativefactsNegativefact() {
        return new Tcgnegativefacts.Negativefact();
    }

    /**
     * Create an instance of {@link Tcgnegativefacts.Negativefact.Processsteps }
     * 
     */
    public Tcgnegativefacts.Negativefact.Processsteps createTcgnegativefactsNegativefactProcesssteps() {
        return new Tcgnegativefacts.Negativefact.Processsteps();
    }

    /**
     * Create an instance of {@link Tcapacitiesshareholderbasic }
     * 
     */
    public Tcapacitiesshareholderbasic createTcapacitiesshareholderbasic() {
        return new Tcapacitiesshareholderbasic();
    }

    /**
     * Create an instance of {@link Tpaymentbehaviour }
     * 
     */
    public Tpaymentbehaviour createTpaymentbehaviour() {
        return new Tpaymentbehaviour();
    }

    /**
     * Create an instance of {@link Tpaymentbehaviour.Branch }
     * 
     */
    public Tpaymentbehaviour.Branch createTpaymentbehaviourBranch() {
        return new Tpaymentbehaviour.Branch();
    }

    /**
     * Create an instance of {@link Tpaymentbehaviour.Branch.History }
     * 
     */
    public Tpaymentbehaviour.Branch.History createTpaymentbehaviourBranchHistory() {
        return new Tpaymentbehaviour.Branch.History();
    }

    /**
     * Create an instance of {@link Tpaymentbehaviour.History }
     * 
     */
    public Tpaymentbehaviour.History createTpaymentbehaviourHistory() {
        return new Tpaymentbehaviour.History();
    }

    /**
     * Create an instance of {@link Tcapacitiesshareholder }
     * 
     */
    public Tcapacitiesshareholder createTcapacitiesshareholder() {
        return new Tcapacitiesshareholder();
    }

    /**
     * Create an instance of {@link Tsolvencyindex }
     * 
     */
    public Tsolvencyindex createTsolvencyindex() {
        return new Tsolvencyindex();
    }

    /**
     * Create an instance of {@link Tsolvencyindex.Probabilityofdefault }
     * 
     */
    public Tsolvencyindex.Probabilityofdefault createTsolvencyindexProbabilityofdefault() {
        return new Tsolvencyindex.Probabilityofdefault();
    }

    /**
     * Create an instance of {@link Tsolvencyindex.Probabilityofdefault.Assignments }
     * 
     */
    public Tsolvencyindex.Probabilityofdefault.Assignments createTsolvencyindexProbabilityofdefaultAssignments() {
        return new Tsolvencyindex.Probabilityofdefault.Assignments();
    }

    /**
     * Create an instance of {@link Tcollectionrequestaddress }
     * 
     */
    public Tcollectionrequestaddress createTcollectionrequestaddress() {
        return new Tcollectionrequestaddress();
    }

    /**
     * Create an instance of {@link Tprivateperson }
     * 
     */
    public Tprivateperson createTprivateperson() {
        return new Tprivateperson();
    }

    /**
     * Create an instance of {@link Tallowedkeys }
     * 
     */
    public Tallowedkeys createTallowedkeys() {
        return new Tallowedkeys();
    }

    /**
     * Create an instance of {@link Tparticipationdataperson }
     * 
     */
    public Tparticipationdataperson createTparticipationdataperson() {
        return new Tparticipationdataperson();
    }

    /**
     * Create an instance of {@link Tformerlocations }
     * 
     */
    public Tformerlocations createTformerlocations() {
        return new Tformerlocations();
    }

    /**
     * Create an instance of {@link Tlegalform }
     * 
     */
    public Tlegalform createTlegalform() {
        return new Tlegalform();
    }

    /**
     * Create an instance of {@link Tserviceavailability }
     * 
     */
    public Tserviceavailability createTserviceavailability() {
        return new Tserviceavailability();
    }

    /**
     * Create an instance of {@link Tcollectionrequestcommunication }
     * 
     */
    public Tcollectionrequestcommunication createTcollectionrequestcommunication() {
        return new Tcollectionrequestcommunication();
    }

    /**
     * Create an instance of {@link Tturnovergroup }
     * 
     */
    public Tturnovergroup createTturnovergroup() {
        return new Tturnovergroup();
    }

    /**
     * Create an instance of {@link Tturnovergroup.Fiscalyear }
     * 
     */
    public Tturnovergroup.Fiscalyear createTturnovergroupFiscalyear() {
        return new Tturnovergroup.Fiscalyear();
    }

    /**
     * Create an instance of {@link Tcgformerrequestsdata }
     * 
     */
    public Tcgformerrequestsdata createTcgformerrequestsdata() {
        return new Tcgformerrequestsdata();
    }

    /**
     * Create an instance of {@link Tchangeextendedmonitoringresponsebody }
     * 
     */
    public Tchangeextendedmonitoringresponsebody createTchangeextendedmonitoringresponsebody() {
        return new Tchangeextendedmonitoringresponsebody();
    }

    /**
     * Create an instance of {@link Tmailboxdirectoryresponsebody }
     * 
     */
    public Tmailboxdirectoryresponsebody createTmailboxdirectoryresponsebody() {
        return new Tmailboxdirectoryresponsebody();
    }

    /**
     * Create an instance of {@link Tmailboxdirectoryresponsebody.Entry }
     * 
     */
    public Tmailboxdirectoryresponsebody.Entry createTmailboxdirectoryresponsebodyEntry() {
        return new Tmailboxdirectoryresponsebody.Entry();
    }

    /**
     * Create an instance of {@link Treportrequestbody }
     * 
     */
    public Treportrequestbody createTreportrequestbody() {
        return new Treportrequestbody();
    }

    /**
     * Create an instance of {@link Tcreditorfreetextmessagequeryresponsebody }
     * 
     */
    public Tcreditorfreetextmessagequeryresponsebody createTcreditorfreetextmessagequeryresponsebody() {
        return new Tcreditorfreetextmessagequeryresponsebody();
    }

    /**
     * Create an instance of {@link Tchangeextendedmonitoringrequestbody }
     * 
     */
    public Tchangeextendedmonitoringrequestbody createTchangeextendedmonitoringrequestbody() {
        return new Tchangeextendedmonitoringrequestbody();
    }

    /**
     * Create an instance of {@link Tupgradelistresponsebody }
     * 
     */
    public Tupgradelistresponsebody createTupgradelistresponsebody() {
        return new Tupgradelistresponsebody();
    }

    /**
     * Create an instance of {@link Tupgradelistresponsebody.Entry }
     * 
     */
    public Tupgradelistresponsebody.Entry createTupgradelistresponsebodyEntry() {
        return new Tupgradelistresponsebody.Entry();
    }

    /**
     * Create an instance of {@link Tstatementreceivablesresponsebody }
     * 
     */
    public Tstatementreceivablesresponsebody createTstatementreceivablesresponsebody() {
        return new Tstatementreceivablesresponsebody();
    }

    /**
     * Create an instance of {@link Treceivableresponsebody }
     * 
     */
    public Treceivableresponsebody createTreceivableresponsebody() {
        return new Treceivableresponsebody();
    }

    /**
     * Create an instance of {@link Ttitledreceivableresponsebody }
     * 
     */
    public Ttitledreceivableresponsebody createTtitledreceivableresponsebody() {
        return new Ttitledreceivableresponsebody();
    }

    /**
     * Create an instance of {@link Tidentificationnumbermapresponsebody }
     * 
     */
    public Tidentificationnumbermapresponsebody createTidentificationnumbermapresponsebody() {
        return new Tidentificationnumbermapresponsebody();
    }

    /**
     * Create an instance of {@link Tkeylistresponsebody }
     * 
     */
    public Tkeylistresponsebody createTkeylistresponsebody() {
        return new Tkeylistresponsebody();
    }

    /**
     * Create an instance of {@link Tkeylistresponsebody.Keylist }
     * 
     */
    public Tkeylistresponsebody.Keylist createTkeylistresponsebodyKeylist() {
        return new Tkeylistresponsebody.Keylist();
    }

    /**
     * Create an instance of {@link Tkeylistresponsebody.Keylist.Keygroup }
     * 
     */
    public Tkeylistresponsebody.Keylist.Keygroup createTkeylistresponsebodyKeylistKeygroup() {
        return new Tkeylistresponsebody.Keylist.Keygroup();
    }

    /**
     * Create an instance of {@link Tlogonresponsebody }
     * 
     */
    public Tlogonresponsebody createTlogonresponsebody() {
        return new Tlogonresponsebody();
    }

    /**
     * Create an instance of {@link Tlogonresponsebody.Useraccountinformation }
     * 
     */
    public Tlogonresponsebody.Useraccountinformation createTlogonresponsebodyUseraccountinformation() {
        return new Tlogonresponsebody.Useraccountinformation();
    }

    /**
     * Create an instance of {@link Tcollectionagencyfreetextmessageresponsebody }
     * 
     */
    public Tcollectionagencyfreetextmessageresponsebody createTcollectionagencyfreetextmessageresponsebody() {
        return new Tcollectionagencyfreetextmessageresponsebody();
    }

    /**
     * Create an instance of {@link Tbalanceanalysissinglecompanyresponsebody }
     * 
     */
    public Tbalanceanalysissinglecompanyresponsebody createTbalanceanalysissinglecompanyresponsebody() {
        return new Tbalanceanalysissinglecompanyresponsebody();
    }

    /**
     * Create an instance of {@link Tbalanceanalysissinglecompanyresponsebody.Company }
     * 
     */
    public Tbalanceanalysissinglecompanyresponsebody.Company createTbalanceanalysissinglecompanyresponsebodyCompany() {
        return new Tbalanceanalysissinglecompanyresponsebody.Company();
    }

    /**
     * Create an instance of {@link Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl }
     * 
     */
    public Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl createTbalanceanalysissinglecompanyresponsebodyCompanyXbrl() {
        return new Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl();
    }

    /**
     * Create an instance of {@link Tcollectionaccountresponsebody }
     * 
     */
    public Tcollectionaccountresponsebody createTcollectionaccountresponsebody() {
        return new Tcollectionaccountresponsebody();
    }

    /**
     * Create an instance of {@link Tcollectionaccountresponsebody.Accountinvoice }
     * 
     */
    public Tcollectionaccountresponsebody.Accountinvoice createTcollectionaccountresponsebodyAccountinvoice() {
        return new Tcollectionaccountresponsebody.Accountinvoice();
    }

    /**
     * Create an instance of {@link Tmonitoringstatusresponsebody }
     * 
     */
    public Tmonitoringstatusresponsebody createTmonitoringstatusresponsebody() {
        return new Tmonitoringstatusresponsebody();
    }

    /**
     * Create an instance of {@link Tmonitoringstatusresponsebody.Entry }
     * 
     */
    public Tmonitoringstatusresponsebody.Entry createTmonitoringstatusresponsebodyEntry() {
        return new Tmonitoringstatusresponsebody.Entry();
    }

    /**
     * Create an instance of {@link Tservicefault }
     * 
     */
    public Tservicefault createTservicefault() {
        return new Tservicefault();
    }

    /**
     * Create an instance of {@link Tservicefault.Body }
     * 
     */
    public Tservicefault.Body createTservicefaultBody() {
        return new Tservicefault.Body();
    }

    /**
     * Create an instance of {@link Torderrequestbody }
     * 
     */
    public Torderrequestbody createTorderrequestbody() {
        return new Torderrequestbody();
    }

    /**
     * Create an instance of {@link Tprocessresponsebody }
     * 
     */
    public Tprocessresponsebody createTprocessresponsebody() {
        return new Tprocessresponsebody();
    }

    /**
     * Create an instance of {@link Tcollectionfilesearchresponsebody }
     * 
     */
    public Tcollectionfilesearchresponsebody createTcollectionfilesearchresponsebody() {
        return new Tcollectionfilesearchresponsebody();
    }

    /**
     * Create an instance of {@link Tcollectionfilesearchresponsebody.Filenumberoverview }
     * 
     */
    public Tcollectionfilesearchresponsebody.Filenumberoverview createTcollectionfilesearchresponsebodyFilenumberoverview() {
        return new Tcollectionfilesearchresponsebody.Filenumberoverview();
    }

    /**
     * Create an instance of {@link Tmigrationdiffresponsebody }
     * 
     */
    public Tmigrationdiffresponsebody createTmigrationdiffresponsebody() {
        return new Tmigrationdiffresponsebody();
    }

    /**
     * Create an instance of {@link Tpaymentsresponsebody }
     * 
     */
    public Tpaymentsresponsebody createTpaymentsresponsebody() {
        return new Tpaymentsresponsebody();
    }

    /**
     * Create an instance of {@link Treportresponsebody }
     * 
     */
    public Treportresponsebody createTreportresponsebody() {
        return new Treportresponsebody();
    }

    /**
     * Create an instance of {@link Treportresponsebody.Reportdata }
     * 
     */
    public Treportresponsebody.Reportdata createTreportresponsebodyReportdata() {
        return new Treportresponsebody.Reportdata();
    }

    /**
     * Create an instance of {@link SearchRequest }
     * 
     */
    public SearchRequest createSearchRequest() {
        return new SearchRequest();
    }

    /**
     * Create an instance of {@link Trequestheader }
     * 
     */
    public Trequestheader createTrequestheader() {
        return new Trequestheader();
    }

    /**
     * Create an instance of {@link Tsearchrequestbody }
     * 
     */
    public Tsearchrequestbody createTsearchrequestbody() {
        return new Tsearchrequestbody();
    }

    /**
     * Create an instance of {@link Tfault }
     * 
     */
    public Tfault createTfault() {
        return new Tfault();
    }

    /**
     * Create an instance of {@link ConsumerreportRequest }
     * 
     */
    public ConsumerreportRequest createConsumerreportRequest() {
        return new ConsumerreportRequest();
    }

    /**
     * Create an instance of {@link Tconsumerreportrequestbody }
     * 
     */
    public Tconsumerreportrequestbody createTconsumerreportrequestbody() {
        return new Tconsumerreportrequestbody();
    }

    /**
     * Create an instance of {@link ReportResponse }
     * 
     */
    public ReportResponse createReportResponse() {
        return new ReportResponse();
    }

    /**
     * Create an instance of {@link Tresponseheader }
     * 
     */
    public Tresponseheader createTresponseheader() {
        return new Tresponseheader();
    }

    /**
     * Create an instance of {@link PaymentsResponse }
     * 
     */
    public PaymentsResponse createPaymentsResponse() {
        return new PaymentsResponse();
    }

    /**
     * Create an instance of {@link ProductavailabilityRequest }
     * 
     */
    public ProductavailabilityRequest createProductavailabilityRequest() {
        return new ProductavailabilityRequest();
    }

    /**
     * Create an instance of {@link Tproductavailabilityrequestbody }
     * 
     */
    public Tproductavailabilityrequestbody createTproductavailabilityrequestbody() {
        return new Tproductavailabilityrequestbody();
    }

    /**
     * Create an instance of {@link CancelstandardmonitoringResponse }
     * 
     */
    public CancelstandardmonitoringResponse createCancelstandardmonitoringResponse() {
        return new CancelstandardmonitoringResponse();
    }

    /**
     * Create an instance of {@link Tcancelstandardmonitoringresponsebody }
     * 
     */
    public Tcancelstandardmonitoringresponsebody createTcancelstandardmonitoringresponsebody() {
        return new Tcancelstandardmonitoringresponsebody();
    }

    /**
     * Create an instance of {@link TitledreceivableRequest }
     * 
     */
    public TitledreceivableRequest createTitledreceivableRequest() {
        return new TitledreceivableRequest();
    }

    /**
     * Create an instance of {@link Ttitledreceivablerequestbody }
     * 
     */
    public Ttitledreceivablerequestbody createTtitledreceivablerequestbody() {
        return new Ttitledreceivablerequestbody();
    }

    /**
     * Create an instance of {@link MigrationdiffResponse }
     * 
     */
    public MigrationdiffResponse createMigrationdiffResponse() {
        return new MigrationdiffResponse();
    }

    /**
     * Create an instance of {@link MonitoringstatusRequest }
     * 
     */
    public MonitoringstatusRequest createMonitoringstatusRequest() {
        return new MonitoringstatusRequest();
    }

    /**
     * Create an instance of {@link Tmonitoringstatusrequestbody }
     * 
     */
    public Tmonitoringstatusrequestbody createTmonitoringstatusrequestbody() {
        return new Tmonitoringstatusrequestbody();
    }

    /**
     * Create an instance of {@link ChangepasswordRequest }
     * 
     */
    public ChangepasswordRequest createChangepasswordRequest() {
        return new ChangepasswordRequest();
    }

    /**
     * Create an instance of {@link Tchangepasswordrequestbody }
     * 
     */
    public Tchangepasswordrequestbody createTchangepasswordrequestbody() {
        return new Tchangepasswordrequestbody();
    }

    /**
     * Create an instance of {@link KeylistRequest }
     * 
     */
    public KeylistRequest createKeylistRequest() {
        return new KeylistRequest();
    }

    /**
     * Create an instance of {@link CollectionfilesearchResponse }
     * 
     */
    public CollectionfilesearchResponse createCollectionfilesearchResponse() {
        return new CollectionfilesearchResponse();
    }

    /**
     * Create an instance of {@link ProcessResponse }
     * 
     */
    public ProcessResponse createProcessResponse() {
        return new ProcessResponse();
    }

    /**
     * Create an instance of {@link ProcessRequest }
     * 
     */
    public ProcessRequest createProcessRequest() {
        return new ProcessRequest();
    }

    /**
     * Create an instance of {@link Tprocessrequestbody }
     * 
     */
    public Tprocessrequestbody createTprocessrequestbody() {
        return new Tprocessrequestbody();
    }

    /**
     * Create an instance of {@link ParticipationdatastartResponse }
     * 
     */
    public ParticipationdatastartResponse createParticipationdatastartResponse() {
        return new ParticipationdatastartResponse();
    }

    /**
     * Create an instance of {@link Tparticipationdatastartresponsebody }
     * 
     */
    public Tparticipationdatastartresponsebody createTparticipationdatastartresponsebody() {
        return new Tparticipationdatastartresponsebody();
    }

    /**
     * Create an instance of {@link ParticipationdataRequest }
     * 
     */
    public ParticipationdataRequest createParticipationdataRequest() {
        return new ParticipationdataRequest();
    }

    /**
     * Create an instance of {@link Tparticipationdatarequestbody }
     * 
     */
    public Tparticipationdatarequestbody createTparticipationdatarequestbody() {
        return new Tparticipationdatarequestbody();
    }

    /**
     * Create an instance of {@link StatementreceivablesRequest }
     * 
     */
    public StatementreceivablesRequest createStatementreceivablesRequest() {
        return new StatementreceivablesRequest();
    }

    /**
     * Create an instance of {@link Tstatementreceivablesrequestbody }
     * 
     */
    public Tstatementreceivablesrequestbody createTstatementreceivablesrequestbody() {
        return new Tstatementreceivablesrequestbody();
    }

    /**
     * Create an instance of {@link CollectionagencyfreetextmessageRequest }
     * 
     */
    public CollectionagencyfreetextmessageRequest createCollectionagencyfreetextmessageRequest() {
        return new CollectionagencyfreetextmessageRequest();
    }

    /**
     * Create an instance of {@link Tcollectionagencyfreetextmessagerequestbody }
     * 
     */
    public Tcollectionagencyfreetextmessagerequestbody createTcollectionagencyfreetextmessagerequestbody() {
        return new Tcollectionagencyfreetextmessagerequestbody();
    }

    /**
     * Create an instance of {@link SearchResponse }
     * 
     */
    public SearchResponse createSearchResponse() {
        return new SearchResponse();
    }

    /**
     * Create an instance of {@link Tsearchresponsebody }
     * 
     */
    public Tsearchresponsebody createTsearchresponsebody() {
        return new Tsearchresponsebody();
    }

    /**
     * Create an instance of {@link OrderRequest }
     * 
     */
    public OrderRequest createOrderRequest() {
        return new OrderRequest();
    }

    /**
     * Create an instance of {@link Tkeywithdescription }
     * 
     */
    public Tkeywithdescription createTkeywithdescription() {
        return new Tkeywithdescription();
    }

    /**
     * Create an instance of {@link CancelstandardmonitoringRequest }
     * 
     */
    public CancelstandardmonitoringRequest createCancelstandardmonitoringRequest() {
        return new CancelstandardmonitoringRequest();
    }

    /**
     * Create an instance of {@link Tcancelstandardmonitoringrequestbody }
     * 
     */
    public Tcancelstandardmonitoringrequestbody createTcancelstandardmonitoringrequestbody() {
        return new Tcancelstandardmonitoringrequestbody();
    }

    /**
     * Create an instance of {@link IdentificationreportRequest }
     * 
     */
    public IdentificationreportRequest createIdentificationreportRequest() {
        return new IdentificationreportRequest();
    }

    /**
     * Create an instance of {@link Tidentificationreportrequestbody }
     * 
     */
    public Tidentificationreportrequestbody createTidentificationreportrequestbody() {
        return new Tidentificationreportrequestbody();
    }

    /**
     * Create an instance of {@link ParticipationdatastartRequest }
     * 
     */
    public ParticipationdatastartRequest createParticipationdatastartRequest() {
        return new ParticipationdatastartRequest();
    }

    /**
     * Create an instance of {@link Tparticipationdatastartrequestbody }
     * 
     */
    public Tparticipationdatastartrequestbody createTparticipationdatastartrequestbody() {
        return new Tparticipationdatastartrequestbody();
    }

    /**
     * Create an instance of {@link GeneralinformationResponse }
     * 
     */
    public GeneralinformationResponse createGeneralinformationResponse() {
        return new GeneralinformationResponse();
    }

    /**
     * Create an instance of {@link Tgeneralinformationresponsebody }
     * 
     */
    public Tgeneralinformationresponsebody createTgeneralinformationresponsebody() {
        return new Tgeneralinformationresponsebody();
    }

    /**
     * Create an instance of {@link BalanceanalysismultiplecompaniesRequest }
     * 
     */
    public BalanceanalysismultiplecompaniesRequest createBalanceanalysismultiplecompaniesRequest() {
        return new BalanceanalysismultiplecompaniesRequest();
    }

    /**
     * Create an instance of {@link Tbalanceanalysismultiplecompaniesrequestbody }
     * 
     */
    public Tbalanceanalysismultiplecompaniesrequestbody createTbalanceanalysismultiplecompaniesrequestbody() {
        return new Tbalanceanalysismultiplecompaniesrequestbody();
    }

    /**
     * Create an instance of {@link CreditorfreetextmessageResponse }
     * 
     */
    public CreditorfreetextmessageResponse createCreditorfreetextmessageResponse() {
        return new CreditorfreetextmessageResponse();
    }

    /**
     * Create an instance of {@link Tcreditorfreetextmessageresponsebody }
     * 
     */
    public Tcreditorfreetextmessageresponsebody createTcreditorfreetextmessageresponsebody() {
        return new Tcreditorfreetextmessageresponsebody();
    }

    /**
     * Create an instance of {@link BalanceanalysismultiplecompaniesResponse }
     * 
     */
    public BalanceanalysismultiplecompaniesResponse createBalanceanalysismultiplecompaniesResponse() {
        return new BalanceanalysismultiplecompaniesResponse();
    }

    /**
     * Create an instance of {@link Tbalanceanalysismultiplecompaniesresponsebody }
     * 
     */
    public Tbalanceanalysismultiplecompaniesresponsebody createTbalanceanalysismultiplecompaniesresponsebody() {
        return new Tbalanceanalysismultiplecompaniesresponsebody();
    }

    /**
     * Create an instance of {@link CreditorfreetextmessagequeryRequest }
     * 
     */
    public CreditorfreetextmessagequeryRequest createCreditorfreetextmessagequeryRequest() {
        return new CreditorfreetextmessagequeryRequest();
    }

    /**
     * Create an instance of {@link Tcreditorfreetextmessagequeryrequestbody }
     * 
     */
    public Tcreditorfreetextmessagequeryrequestbody createTcreditorfreetextmessagequeryrequestbody() {
        return new Tcreditorfreetextmessagequeryrequestbody();
    }

    /**
     * Create an instance of {@link CollectionagencydocumentResponse }
     * 
     */
    public CollectionagencydocumentResponse createCollectionagencydocumentResponse() {
        return new CollectionagencydocumentResponse();
    }

    /**
     * Create an instance of {@link Tcollectionagencydocumentresponsebody }
     * 
     */
    public Tcollectionagencydocumentresponsebody createTcollectionagencydocumentresponsebody() {
        return new Tcollectionagencydocumentresponsebody();
    }

    /**
     * Create an instance of {@link IdentificationnumbermapRequest }
     * 
     */
    public IdentificationnumbermapRequest createIdentificationnumbermapRequest() {
        return new IdentificationnumbermapRequest();
    }

    /**
     * Create an instance of {@link Tidentificationnumbermaprequestbody }
     * 
     */
    public Tidentificationnumbermaprequestbody createTidentificationnumbermaprequestbody() {
        return new Tidentificationnumbermaprequestbody();
    }

    /**
     * Create an instance of {@link MonitoringstatusResponse }
     * 
     */
    public MonitoringstatusResponse createMonitoringstatusResponse() {
        return new MonitoringstatusResponse();
    }

    /**
     * Create an instance of {@link CollectionfilesearchRequest }
     * 
     */
    public CollectionfilesearchRequest createCollectionfilesearchRequest() {
        return new CollectionfilesearchRequest();
    }

    /**
     * Create an instance of {@link Tcollectionfilesearchrequestbody }
     * 
     */
    public Tcollectionfilesearchrequestbody createTcollectionfilesearchrequestbody() {
        return new Tcollectionfilesearchrequestbody();
    }

    /**
     * Create an instance of {@link LogonRequest }
     * 
     */
    public LogonRequest createLogonRequest() {
        return new LogonRequest();
    }

    /**
     * Create an instance of {@link CollectionaccountResponse }
     * 
     */
    public CollectionaccountResponse createCollectionaccountResponse() {
        return new CollectionaccountResponse();
    }

    /**
     * Create an instance of {@link BalanceanalysissinglecompanyRequest }
     * 
     */
    public BalanceanalysissinglecompanyRequest createBalanceanalysissinglecompanyRequest() {
        return new BalanceanalysissinglecompanyRequest();
    }

    /**
     * Create an instance of {@link Tbalanceanalysissinglecompanyrequestbody }
     * 
     */
    public Tbalanceanalysissinglecompanyrequestbody createTbalanceanalysissinglecompanyrequestbody() {
        return new Tbalanceanalysissinglecompanyrequestbody();
    }

    /**
     * Create an instance of {@link ForwardreceivableResponse }
     * 
     */
    public ForwardreceivableResponse createForwardreceivableResponse() {
        return new ForwardreceivableResponse();
    }

    /**
     * Create an instance of {@link Tforwardreceivableresponsebody }
     * 
     */
    public Tforwardreceivableresponsebody createTforwardreceivableresponsebody() {
        return new Tforwardreceivableresponsebody();
    }

    /**
     * Create an instance of {@link BalanceanalysissinglecompanyResponse }
     * 
     */
    public BalanceanalysissinglecompanyResponse createBalanceanalysissinglecompanyResponse() {
        return new BalanceanalysissinglecompanyResponse();
    }

    /**
     * Create an instance of {@link CollectionagencydocumentRequest }
     * 
     */
    public CollectionagencydocumentRequest createCollectionagencydocumentRequest() {
        return new CollectionagencydocumentRequest();
    }

    /**
     * Create an instance of {@link Tcollectionagencydocumentrequestbody }
     * 
     */
    public Tcollectionagencydocumentrequestbody createTcollectionagencydocumentrequestbody() {
        return new Tcollectionagencydocumentrequestbody();
    }

    /**
     * Create an instance of {@link IdentificationreportResponse }
     * 
     */
    public IdentificationreportResponse createIdentificationreportResponse() {
        return new IdentificationreportResponse();
    }

    /**
     * Create an instance of {@link ForwardreceivableRequest }
     * 
     */
    public ForwardreceivableRequest createForwardreceivableRequest() {
        return new ForwardreceivableRequest();
    }

    /**
     * Create an instance of {@link Tforwardreceivablerequestbody }
     * 
     */
    public Tforwardreceivablerequestbody createTforwardreceivablerequestbody() {
        return new Tforwardreceivablerequestbody();
    }

    /**
     * Create an instance of {@link CollectionagencyfreetextmessageResponse }
     * 
     */
    public CollectionagencyfreetextmessageResponse createCollectionagencyfreetextmessageResponse() {
        return new CollectionagencyfreetextmessageResponse();
    }

    /**
     * Create an instance of {@link CollectionorderRequest }
     * 
     */
    public CollectionorderRequest createCollectionorderRequest() {
        return new CollectionorderRequest();
    }

    /**
     * Create an instance of {@link Tcollectionorderrequestbody }
     * 
     */
    public Tcollectionorderrequestbody createTcollectionorderrequestbody() {
        return new Tcollectionorderrequestbody();
    }

    /**
     * Create an instance of {@link LogonResponse }
     * 
     */
    public LogonResponse createLogonResponse() {
        return new LogonResponse();
    }

    /**
     * Create an instance of {@link ProductavailabilityResponse }
     * 
     */
    public ProductavailabilityResponse createProductavailabilityResponse() {
        return new ProductavailabilityResponse();
    }

    /**
     * Create an instance of {@link Tproductavailabilityresponsebody }
     * 
     */
    public Tproductavailabilityresponsebody createTproductavailabilityresponsebody() {
        return new Tproductavailabilityresponsebody();
    }

    /**
     * Create an instance of {@link KeylistResponse }
     * 
     */
    public KeylistResponse createKeylistResponse() {
        return new KeylistResponse();
    }

    /**
     * Create an instance of {@link PaymentsRequest }
     * 
     */
    public PaymentsRequest createPaymentsRequest() {
        return new PaymentsRequest();
    }

    /**
     * Create an instance of {@link Tpaymentsrequestbody }
     * 
     */
    public Tpaymentsrequestbody createTpaymentsrequestbody() {
        return new Tpaymentsrequestbody();
    }

    /**
     * Create an instance of {@link MailboxentryResponse }
     * 
     */
    public MailboxentryResponse createMailboxentryResponse() {
        return new MailboxentryResponse();
    }

    /**
     * Create an instance of {@link Tmailboxentryresponsebody }
     * 
     */
    public Tmailboxentryresponsebody createTmailboxentryresponsebody() {
        return new Tmailboxentryresponsebody();
    }

    /**
     * Create an instance of {@link IdentificationnumbermapResponse }
     * 
     */
    public IdentificationnumbermapResponse createIdentificationnumbermapResponse() {
        return new IdentificationnumbermapResponse();
    }

    /**
     * Create an instance of {@link GeneralinformationRequest }
     * 
     */
    public GeneralinformationRequest createGeneralinformationRequest() {
        return new GeneralinformationRequest();
    }

    /**
     * Create an instance of {@link Tgeneralinformationrequestbody }
     * 
     */
    public Tgeneralinformationrequestbody createTgeneralinformationrequestbody() {
        return new Tgeneralinformationrequestbody();
    }

    /**
     * Create an instance of {@link TitledreceivableResponse }
     * 
     */
    public TitledreceivableResponse createTitledreceivableResponse() {
        return new TitledreceivableResponse();
    }

    /**
     * Create an instance of {@link ReceivableResponse }
     * 
     */
    public ReceivableResponse createReceivableResponse() {
        return new ReceivableResponse();
    }

    /**
     * Create an instance of {@link ChangeemailRequest }
     * 
     */
    public ChangeemailRequest createChangeemailRequest() {
        return new ChangeemailRequest();
    }

    /**
     * Create an instance of {@link Tchangeemailrequestbody }
     * 
     */
    public Tchangeemailrequestbody createTchangeemailrequestbody() {
        return new Tchangeemailrequestbody();
    }

    /**
     * Create an instance of {@link ChangeemailResponse }
     * 
     */
    public ChangeemailResponse createChangeemailResponse() {
        return new ChangeemailResponse();
    }

    /**
     * Create an instance of {@link Tchangeemailresponsebody }
     * 
     */
    public Tchangeemailresponsebody createTchangeemailresponsebody() {
        return new Tchangeemailresponsebody();
    }

    /**
     * Create an instance of {@link CollectionorderResponse }
     * 
     */
    public CollectionorderResponse createCollectionorderResponse() {
        return new CollectionorderResponse();
    }

    /**
     * Create an instance of {@link Tcollectionorderresponsebody }
     * 
     */
    public Tcollectionorderresponsebody createTcollectionorderresponsebody() {
        return new Tcollectionorderresponsebody();
    }

    /**
     * Create an instance of {@link StatementreceivablesResponse }
     * 
     */
    public StatementreceivablesResponse createStatementreceivablesResponse() {
        return new StatementreceivablesResponse();
    }

    /**
     * Create an instance of {@link MigrationdiffRequest }
     * 
     */
    public MigrationdiffRequest createMigrationdiffRequest() {
        return new MigrationdiffRequest();
    }

    /**
     * Create an instance of {@link MailboxentryRequest }
     * 
     */
    public MailboxentryRequest createMailboxentryRequest() {
        return new MailboxentryRequest();
    }

    /**
     * Create an instance of {@link Tmailboxentryrequestbody }
     * 
     */
    public Tmailboxentryrequestbody createTmailboxentryrequestbody() {
        return new Tmailboxentryrequestbody();
    }

    /**
     * Create an instance of {@link UpgradelistResponse }
     * 
     */
    public UpgradelistResponse createUpgradelistResponse() {
        return new UpgradelistResponse();
    }

    /**
     * Create an instance of {@link ChangeextendedmonitoringRequest }
     * 
     */
    public ChangeextendedmonitoringRequest createChangeextendedmonitoringRequest() {
        return new ChangeextendedmonitoringRequest();
    }

    /**
     * Create an instance of {@link ParticipationdataResponse }
     * 
     */
    public ParticipationdataResponse createParticipationdataResponse() {
        return new ParticipationdataResponse();
    }

    /**
     * Create an instance of {@link Tparticipationdataresponsebody }
     * 
     */
    public Tparticipationdataresponsebody createTparticipationdataresponsebody() {
        return new Tparticipationdataresponsebody();
    }

    /**
     * Create an instance of {@link CreditorfreetextmessagequeryResponse }
     * 
     */
    public CreditorfreetextmessagequeryResponse createCreditorfreetextmessagequeryResponse() {
        return new CreditorfreetextmessagequeryResponse();
    }

    /**
     * Create an instance of {@link ReportRequest }
     * 
     */
    public ReportRequest createReportRequest() {
        return new ReportRequest();
    }

    /**
     * Create an instance of {@link ChangepasswordResponse }
     * 
     */
    public ChangepasswordResponse createChangepasswordResponse() {
        return new ChangepasswordResponse();
    }

    /**
     * Create an instance of {@link MailboxdirectoryRequest }
     * 
     */
    public MailboxdirectoryRequest createMailboxdirectoryRequest() {
        return new MailboxdirectoryRequest();
    }

    /**
     * Create an instance of {@link Tmailboxdirectoryrequestbody }
     * 
     */
    public Tmailboxdirectoryrequestbody createTmailboxdirectoryrequestbody() {
        return new Tmailboxdirectoryrequestbody();
    }

    /**
     * Create an instance of {@link OrderResponse }
     * 
     */
    public OrderResponse createOrderResponse() {
        return new OrderResponse();
    }

    /**
     * Create an instance of {@link Torderresponsebody }
     * 
     */
    public Torderresponsebody createTorderresponsebody() {
        return new Torderresponsebody();
    }

    /**
     * Create an instance of {@link UpgradelistRequest }
     * 
     */
    public UpgradelistRequest createUpgradelistRequest() {
        return new UpgradelistRequest();
    }

    /**
     * Create an instance of {@link Tupgradelistrequestbody }
     * 
     */
    public Tupgradelistrequestbody createTupgradelistrequestbody() {
        return new Tupgradelistrequestbody();
    }

    /**
     * Create an instance of {@link CreditorfreetextmessageRequest }
     * 
     */
    public CreditorfreetextmessageRequest createCreditorfreetextmessageRequest() {
        return new CreditorfreetextmessageRequest();
    }

    /**
     * Create an instance of {@link Tcreditorfreetextmessagerequestbody }
     * 
     */
    public Tcreditorfreetextmessagerequestbody createTcreditorfreetextmessagerequestbody() {
        return new Tcreditorfreetextmessagerequestbody();
    }

    /**
     * Create an instance of {@link CollectionaccountRequest }
     * 
     */
    public CollectionaccountRequest createCollectionaccountRequest() {
        return new CollectionaccountRequest();
    }

    /**
     * Create an instance of {@link Tcollectionaccountrequestbody }
     * 
     */
    public Tcollectionaccountrequestbody createTcollectionaccountrequestbody() {
        return new Tcollectionaccountrequestbody();
    }

    /**
     * Create an instance of {@link ConsumerreportResponse }
     * 
     */
    public ConsumerreportResponse createConsumerreportResponse() {
        return new ConsumerreportResponse();
    }

    /**
     * Create an instance of {@link Tconsumerreportresponsebody }
     * 
     */
    public Tconsumerreportresponsebody createTconsumerreportresponsebody() {
        return new Tconsumerreportresponsebody();
    }

    /**
     * Create an instance of {@link MailboxdirectoryResponse }
     * 
     */
    public MailboxdirectoryResponse createMailboxdirectoryResponse() {
        return new MailboxdirectoryResponse();
    }

    /**
     * Create an instance of {@link ChangeextendedmonitoringResponse }
     * 
     */
    public ChangeextendedmonitoringResponse createChangeextendedmonitoringResponse() {
        return new ChangeextendedmonitoringResponse();
    }

    /**
     * Create an instance of {@link BonimareportResponse }
     * 
     */
    public BonimareportResponse createBonimareportResponse() {
        return new BonimareportResponse();
    }

    /**
     * Create an instance of {@link Tbonimareportresponsebody }
     * 
     */
    public Tbonimareportresponsebody createTbonimareportresponsebody() {
        return new Tbonimareportresponsebody();
    }

    /**
     * Create an instance of {@link ReceivableRequest }
     * 
     */
    public ReceivableRequest createReceivableRequest() {
        return new ReceivableRequest();
    }

    /**
     * Create an instance of {@link Treceivablerequestbody }
     * 
     */
    public Treceivablerequestbody createTreceivablerequestbody() {
        return new Treceivablerequestbody();
    }

    /**
     * Create an instance of {@link BonimareportRequest }
     * 
     */
    public BonimareportRequest createBonimareportRequest() {
        return new BonimareportRequest();
    }

    /**
     * Create an instance of {@link Tbonimareportrequestbody }
     * 
     */
    public Tbonimareportrequestbody createTbonimareportrequestbody() {
        return new Tbonimareportrequestbody();
    }

    /**
     * Create an instance of {@link Tprofitslossesitem }
     * 
     */
    public Tprofitslossesitem createTprofitslossesitem() {
        return new Tprofitslossesitem();
    }

    /**
     * Create an instance of {@link Tprofitslossesgroup }
     * 
     */
    public Tprofitslossesgroup createTprofitslossesgroup() {
        return new Tprofitslossesgroup();
    }

    /**
     * Create an instance of {@link Tparticipantextended }
     * 
     */
    public Tparticipantextended createTparticipantextended() {
        return new Tparticipantextended();
    }

    /**
     * Create an instance of {@link Tbalanceanalysismultiplecompaniesrequestcompany }
     * 
     */
    public Tbalanceanalysismultiplecompaniesrequestcompany createTbalanceanalysismultiplecompaniesrequestcompany() {
        return new Tbalanceanalysismultiplecompaniesrequestcompany();
    }

    /**
     * Create an instance of {@link Tcollectionrequestcostturnover }
     * 
     */
    public Tcollectionrequestcostturnover createTcollectionrequestcostturnover() {
        return new Tcollectionrequestcostturnover();
    }

    /**
     * Create an instance of {@link Tcgrequestaddress }
     * 
     */
    public Tcgrequestaddress createTcgrequestaddress() {
        return new Tcgrequestaddress();
    }

    /**
     * Create an instance of {@link Tancillaryinformationcooperative }
     * 
     */
    public Tancillaryinformationcooperative createTancillaryinformationcooperative() {
        return new Tancillaryinformationcooperative();
    }

    /**
     * Create an instance of {@link Twarning }
     * 
     */
    public Twarning createTwarning() {
        return new Twarning();
    }

    /**
     * Create an instance of {@link Tfinancialstatementfigures }
     * 
     */
    public Tfinancialstatementfigures createTfinancialstatementfigures() {
        return new Tfinancialstatementfigures();
    }

    /**
     * Create an instance of {@link Timportexport }
     * 
     */
    public Timportexport createTimportexport() {
        return new Timportexport();
    }

    /**
     * Create an instance of {@link Tparticipationdataparticipation }
     * 
     */
    public Tparticipationdataparticipation createTparticipationdataparticipation() {
        return new Tparticipationdataparticipation();
    }

    /**
     * Create an instance of {@link Tphone }
     * 
     */
    public Tphone createTphone() {
        return new Tphone();
    }

    /**
     * Create an instance of {@link Tfinancialstatementfiguresgroup }
     * 
     */
    public Tfinancialstatementfiguresgroup createTfinancialstatementfiguresgroup() {
        return new Tfinancialstatementfiguresgroup();
    }

    /**
     * Create an instance of {@link Tancillaryinformationshares }
     * 
     */
    public Tancillaryinformationshares createTancillaryinformationshares() {
        return new Tancillaryinformationshares();
    }

    /**
     * Create an instance of {@link Tancillaryinformationassociation }
     * 
     */
    public Tancillaryinformationassociation createTancillaryinformationassociation() {
        return new Tancillaryinformationassociation();
    }

    /**
     * Create an instance of {@link Tbusinessarea }
     * 
     */
    public Tbusinessarea createTbusinessarea() {
        return new Tbusinessarea();
    }

    /**
     * Create an instance of {@link Tfinancialinformation }
     * 
     */
    public Tfinancialinformation createTfinancialinformation() {
        return new Tfinancialinformation();
    }

    /**
     * Create an instance of {@link Tturnovercompanyrange }
     * 
     */
    public Tturnovercompanyrange createTturnovercompanyrange() {
        return new Tturnovercompanyrange();
    }

    /**
     * Create an instance of {@link Tdeputymanagementbasic }
     * 
     */
    public Tdeputymanagementbasic createTdeputymanagementbasic() {
        return new Tdeputymanagementbasic();
    }

    /**
     * Create an instance of {@link Tparticipant }
     * 
     */
    public Tparticipant createTparticipant() {
        return new Tparticipant();
    }

    /**
     * Create an instance of {@link Ttrafficlightsolvency }
     * 
     */
    public Ttrafficlightsolvency createTtrafficlightsolvency() {
        return new Ttrafficlightsolvency();
    }

    /**
     * Create an instance of {@link Tparticipationdataactiveformer }
     * 
     */
    public Tparticipationdataactiveformer createTparticipationdataactiveformer() {
        return new Tparticipationdataactiveformer();
    }

    /**
     * Create an instance of {@link Tcgrequestdata }
     * 
     */
    public Tcgrequestdata createTcgrequestdata() {
        return new Tcgrequestdata();
    }

    /**
     * Create an instance of {@link Tparticipationdata }
     * 
     */
    public Tparticipationdata createTparticipationdata() {
        return new Tparticipationdata();
    }

    /**
     * Create an instance of {@link Tkeyattribute }
     * 
     */
    public Tkeyattribute createTkeyattribute() {
        return new Tkeyattribute();
    }

    /**
     * Create an instance of {@link Tparticipations }
     * 
     */
    public Tparticipations createTparticipations() {
        return new Tparticipations();
    }

    /**
     * Create an instance of {@link Tnote }
     * 
     */
    public Tnote createTnote() {
        return new Tnote();
    }

    /**
     * Create an instance of {@link Tsupplement }
     * 
     */
    public Tsupplement createTsupplement() {
        return new Tsupplement();
    }

    /**
     * Create an instance of {@link Tnegativereport }
     * 
     */
    public Tnegativereport createTnegativereport() {
        return new Tnegativereport();
    }

    /**
     * Create an instance of {@link Tdeputymanagement }
     * 
     */
    public Tdeputymanagement createTdeputymanagement() {
        return new Tdeputymanagement();
    }

    /**
     * Create an instance of {@link Timportexportdetails }
     * 
     */
    public Timportexportdetails createTimportexportdetails() {
        return new Timportexportdetails();
    }

    /**
     * Create an instance of {@link Tparticipantcapacities }
     * 
     */
    public Tparticipantcapacities createTparticipantcapacities() {
        return new Tparticipantcapacities();
    }

    /**
     * Create an instance of {@link Trealestate }
     * 
     */
    public Trealestate createTrealestate() {
        return new Trealestate();
    }

    /**
     * Create an instance of {@link Tcggeodata }
     * 
     */
    public Tcggeodata createTcggeodata() {
        return new Tcggeodata();
    }

    /**
     * Create an instance of {@link Tshareholdercapitalbasic }
     * 
     */
    public Tshareholdercapitalbasic createTshareholdercapitalbasic() {
        return new Tshareholdercapitalbasic();
    }

    /**
     * Create an instance of {@link Tkeywithshortdesignation }
     * 
     */
    public Tkeywithshortdesignation createTkeywithshortdesignation() {
        return new Tkeywithshortdesignation();
    }

    /**
     * Create an instance of {@link Tbalancesheetgroup }
     * 
     */
    public Tbalancesheetgroup createTbalancesheetgroup() {
        return new Tbalancesheetgroup();
    }

    /**
     * Create an instance of {@link Tcgaddresscheckdata }
     * 
     */
    public Tcgaddresscheckdata createTcgaddresscheckdata() {
        return new Tcgaddresscheckdata();
    }

    /**
     * Create an instance of {@link Tkeyattributevalue }
     * 
     */
    public Tkeyattributevalue createTkeyattributevalue() {
        return new Tkeyattributevalue();
    }

    /**
     * Create an instance of {@link Tstaffcompanyrange }
     * 
     */
    public Tstaffcompanyrange createTstaffcompanyrange() {
        return new Tstaffcompanyrange();
    }

    /**
     * Create an instance of {@link Tparticipationdataparticipations }
     * 
     */
    public Tparticipationdataparticipations createTparticipationdataparticipations() {
        return new Tparticipationdataparticipations();
    }

    /**
     * Create an instance of {@link Tcgnamesdata }
     * 
     */
    public Tcgnamesdata createTcgnamesdata() {
        return new Tcgnamesdata();
    }

    /**
     * Create an instance of {@link Tamount }
     * 
     */
    public Tamount createTamount() {
        return new Tamount();
    }

    /**
     * Create an instance of {@link Tcreditopinion }
     * 
     */
    public Tcreditopinion createTcreditopinion() {
        return new Tcreditopinion();
    }

    /**
     * Create an instance of {@link Tancillaryinformationcapital }
     * 
     */
    public Tancillaryinformationcapital createTancillaryinformationcapital() {
        return new Tancillaryinformationcapital();
    }

    /**
     * Create an instance of {@link Tenquirycounter }
     * 
     */
    public Tenquirycounter createTenquirycounter() {
        return new Tenquirycounter();
    }

    /**
     * Create an instance of {@link Tsearchhit }
     * 
     */
    public Tsearchhit createTsearchhit() {
        return new Tsearchhit();
    }

    /**
     * Create an instance of {@link Tgeneraldata }
     * 
     */
    public Tgeneraldata createTgeneraldata() {
        return new Tgeneraldata();
    }

    /**
     * Create an instance of {@link Taffiliatedgroup }
     * 
     */
    public Taffiliatedgroup createTaffiliatedgroup() {
        return new Taffiliatedgroup();
    }

    /**
     * Create an instance of {@link Tsolvencyclass }
     * 
     */
    public Tsolvencyclass createTsolvencyclass() {
        return new Tsolvencyclass();
    }

    /**
     * Create an instance of {@link Tcgratingdata }
     * 
     */
    public Tcgratingdata createTcgratingdata() {
        return new Tcgratingdata();
    }

    /**
     * Create an instance of {@link Triskjudgement }
     * 
     */
    public Triskjudgement createTriskjudgement() {
        return new Triskjudgement();
    }

    /**
     * Create an instance of {@link Timpreciseperiod }
     * 
     */
    public Timpreciseperiod createTimpreciseperiod() {
        return new Timpreciseperiod();
    }

    /**
     * Create an instance of {@link Tcgreportrequestbody }
     * 
     */
    public Tcgreportrequestbody createTcgreportrequestbody() {
        return new Tcgreportrequestbody();
    }

    /**
     * Create an instance of {@link Trequestsinperiod }
     * 
     */
    public Trequestsinperiod createTrequestsinperiod() {
        return new Trequestsinperiod();
    }

    /**
     * Create an instance of {@link Tfinancialstatusprivateperson }
     * 
     */
    public Tfinancialstatusprivateperson createTfinancialstatusprivateperson() {
        return new Tfinancialstatusprivateperson();
    }

    /**
     * Create an instance of {@link Tcgresponseaddress }
     * 
     */
    public Tcgresponseaddress createTcgresponseaddress() {
        return new Tcgresponseaddress();
    }

    /**
     * Create an instance of {@link Treceivableposition }
     * 
     */
    public Treceivableposition createTreceivableposition() {
        return new Treceivableposition();
    }

    /**
     * Create an instance of {@link Tsharecapital }
     * 
     */
    public Tsharecapital createTsharecapital() {
        return new Tsharecapital();
    }

    /**
     * Create an instance of {@link Tbalancesheetitem }
     * 
     */
    public Tbalancesheetitem createTbalancesheetitem() {
        return new Tbalancesheetitem();
    }

    /**
     * Create an instance of {@link Tparameter }
     * 
     */
    public Tparameter createTparameter() {
        return new Tparameter();
    }

    /**
     * Create an instance of {@link Tcourt }
     * 
     */
    public Tcourt createTcourt() {
        return new Tcourt();
    }

    /**
     * Create an instance of {@link Tcgidentificationdata }
     * 
     */
    public Tcgidentificationdata createTcgidentificationdata() {
        return new Tcgidentificationdata();
    }

    /**
     * Create an instance of {@link Tpaymentmode }
     * 
     */
    public Tpaymentmode createTpaymentmode() {
        return new Tpaymentmode();
    }

    /**
     * Create an instance of {@link Tstatusreply }
     * 
     */
    public Tstatusreply createTstatusreply() {
        return new Tstatusreply();
    }

    /**
     * Create an instance of {@link Ttext }
     * 
     */
    public Ttext createTtext() {
        return new Ttext();
    }

    /**
     * Create an instance of {@link Tcgparticipationdata }
     * 
     */
    public Tcgparticipationdata createTcgparticipationdata() {
        return new Tcgparticipationdata();
    }

    /**
     * Create an instance of {@link Treporthints }
     * 
     */
    public Treporthints createTreporthints() {
        return new Treporthints();
    }

    /**
     * Create an instance of {@link Tshareholderextended }
     * 
     */
    public Tshareholderextended createTshareholderextended() {
        return new Tshareholderextended();
    }

    /**
     * Create an instance of {@link Tbalancesheetsum }
     * 
     */
    public Tbalancesheetsum createTbalancesheetsum() {
        return new Tbalancesheetsum();
    }

    /**
     * Create an instance of {@link Tsolvencyindexhistoryentry }
     * 
     */
    public Tsolvencyindexhistoryentry createTsolvencyindexhistoryentry() {
        return new Tsolvencyindexhistoryentry();
    }

    /**
     * Create an instance of {@link Tprofitslosses }
     * 
     */
    public Tprofitslosses createTprofitslosses() {
        return new Tprofitslosses();
    }

    /**
     * Create an instance of {@link Tbalancesheet }
     * 
     */
    public Tbalancesheet createTbalancesheet() {
        return new Tbalancesheet();
    }

    /**
     * Create an instance of {@link Tancillaryinformation }
     * 
     */
    public Tancillaryinformation createTancillaryinformation() {
        return new Tancillaryinformation();
    }

    /**
     * Create an instance of {@link Tbusinesspurpose }
     * 
     */
    public Tbusinesspurpose createTbusinesspurpose() {
        return new Tbusinesspurpose();
    }

    /**
     * Create an instance of {@link Tcollectionrequestpaymentturnover }
     * 
     */
    public Tcollectionrequestpaymentturnover createTcollectionrequestpaymentturnover() {
        return new Tcollectionrequestpaymentturnover();
    }

    /**
     * Create an instance of {@link Tperiod }
     * 
     */
    public Tperiod createTperiod() {
        return new Tperiod();
    }

    /**
     * Create an instance of {@link Tcgparticipation }
     * 
     */
    public Tcgparticipation createTcgparticipation() {
        return new Tcgparticipation();
    }

    /**
     * Create an instance of {@link Tcollectionrequestreceivable }
     * 
     */
    public Tcollectionrequestreceivable createTcollectionrequestreceivable() {
        return new Tcollectionrequestreceivable();
    }

    /**
     * Create an instance of {@link Tcountryconstraint }
     * 
     */
    public Tcountryconstraint createTcountryconstraint() {
        return new Tcountryconstraint();
    }

    /**
     * Create an instance of {@link Ttaxdata }
     * 
     */
    public Ttaxdata createTtaxdata() {
        return new Ttaxdata();
    }

    /**
     * Create an instance of {@link Tbusinessdevelopment }
     * 
     */
    public Tbusinessdevelopment createTbusinessdevelopment() {
        return new Tbusinessdevelopment();
    }

    /**
     * Create an instance of {@link Tcompanyidentification }
     * 
     */
    public Tcompanyidentification createTcompanyidentification() {
        return new Tcompanyidentification();
    }

    /**
     * Create an instance of {@link Tshareholdercapital }
     * 
     */
    public Tshareholdercapital createTshareholdercapital() {
        return new Tshareholdercapital();
    }

    /**
     * Create an instance of {@link Tparticipantcapacitiesbasic }
     * 
     */
    public Tparticipantcapacitiesbasic createTparticipantcapacitiesbasic() {
        return new Tparticipantcapacitiesbasic();
    }

    /**
     * Create an instance of {@link Tkeywithgrade }
     * 
     */
    public Tkeywithgrade createTkeywithgrade() {
        return new Tkeywithgrade();
    }

    /**
     * Create an instance of {@link Tregister }
     * 
     */
    public Tregister createTregister() {
        return new Tregister();
    }

    /**
     * Create an instance of {@link Tspecificationofprofession }
     * 
     */
    public Tspecificationofprofession createTspecificationofprofession() {
        return new Tspecificationofprofession();
    }

    /**
     * Create an instance of {@link Tkey }
     * 
     */
    public Tkey createTkey() {
        return new Tkey();
    }

    /**
     * Create an instance of {@link Tbalanceanalysissinglecompany }
     * 
     */
    public Tbalanceanalysissinglecompany createTbalanceanalysissinglecompany() {
        return new Tbalanceanalysissinglecompany();
    }

    /**
     * Create an instance of {@link Tphonerequest }
     * 
     */
    public Tphonerequest createTphonerequest() {
        return new Tphonerequest();
    }

    /**
     * Create an instance of {@link Tcollectionrequestbankdata }
     * 
     */
    public Tcollectionrequestbankdata createTcollectionrequestbankdata() {
        return new Tcollectionrequestbankdata();
    }

    /**
     * Create an instance of {@link Telementconstraint }
     * 
     */
    public Telementconstraint createTelementconstraint() {
        return new Telementconstraint();
    }

    /**
     * Create an instance of {@link Tsolvencyremarks }
     * 
     */
    public Tsolvencyremarks createTsolvencyremarks() {
        return new Tsolvencyremarks();
    }

    /**
     * Create an instance of {@link Tservice }
     * 
     */
    public Tservice createTservice() {
        return new Tservice();
    }

    /**
     * Create an instance of {@link Tshare }
     * 
     */
    public Tshare createTshare() {
        return new Tshare();
    }

    /**
     * Create an instance of {@link Tamountpercent }
     * 
     */
    public Tamountpercent createTamountpercent() {
        return new Tamountpercent();
    }

    /**
     * Create an instance of {@link Ttrafficlightpaymentmode }
     * 
     */
    public Ttrafficlightpaymentmode createTtrafficlightpaymentmode() {
        return new Ttrafficlightpaymentmode();
    }

    /**
     * Create an instance of {@link Tcgcollectiondata.Longterm.Case }
     * 
     */
    public Tcgcollectiondata.Longterm.Case createTcgcollectiondataLongtermCase() {
        return new Tcgcollectiondata.Longterm.Case();
    }

    /**
     * Create an instance of {@link Tcgcollectiondata.Regular.Case }
     * 
     */
    public Tcgcollectiondata.Regular.Case createTcgcollectiondataRegularCase() {
        return new Tcgcollectiondata.Regular.Case();
    }

    /**
     * Create an instance of {@link Tcollectionnegativefacts.Negativefact }
     * 
     */
    public Tcollectionnegativefacts.Negativefact createTcollectionnegativefactsNegativefact() {
        return new Tcollectionnegativefacts.Negativefact();
    }

    /**
     * Create an instance of {@link Tstaffgroup.Fiscalyear }
     * 
     */
    public Tstaffgroup.Fiscalyear createTstaffgroupFiscalyear() {
        return new Tstaffgroup.Fiscalyear();
    }

    /**
     * Create an instance of {@link Trealestates.Realestate.Operationalarea }
     * 
     */
    public Trealestates.Realestate.Operationalarea createTrealestatesRealestateOperationalarea() {
        return new Trealestates.Realestate.Operationalarea();
    }

    /**
     * Create an instance of {@link Tsolvencyindexhistory.History }
     * 
     */
    public Tsolvencyindexhistory.History createTsolvencyindexhistoryHistory() {
        return new Tsolvencyindexhistory.History();
    }

    /**
     * Create an instance of {@link Tbalancesolvency.Pdranges.Pdrange }
     * 
     */
    public Tbalancesolvency.Pdranges.Pdrange createTbalancesolvencyPdrangesPdrange() {
        return new Tbalancesolvency.Pdranges.Pdrange();
    }

    /**
     * Create an instance of {@link Tprofitslossessheet.Chapter }
     * 
     */
    public Tprofitslossessheet.Chapter createTprofitslossessheetChapter() {
        return new Tprofitslossessheet.Chapter();
    }

    /**
     * Create an instance of {@link Tbranch.Cri }
     * 
     */
    public Tbranch.Cri createTbranchCri() {
        return new Tbranch.Cri();
    }

    /**
     * Create an instance of {@link Tbranch.Probabilityofdefault }
     * 
     */
    public Tbranch.Probabilityofdefault createTbranchProbabilityofdefault() {
        return new Tbranch.Probabilityofdefault();
    }

    /**
     * Create an instance of {@link Tbranch.Typeofbranch.Branch.Probabilityofdefault }
     * 
     */
    public Tbranch.Typeofbranch.Branch.Probabilityofdefault createTbranchTypeofbranchBranchProbabilityofdefault() {
        return new Tbranch.Typeofbranch.Branch.Probabilityofdefault();
    }

    /**
     * Create an instance of {@link Tbranch.Typeofbranch.Branch.Extendedbranch }
     * 
     */
    public Tbranch.Typeofbranch.Branch.Extendedbranch createTbranchTypeofbranchBranchExtendedbranch() {
        return new Tbranch.Typeofbranch.Branch.Extendedbranch();
    }

    /**
     * Create an instance of {@link Tparameterlist.Info }
     * 
     */
    public Tparameterlist.Info createTparameterlistInfo() {
        return new Tparameterlist.Info();
    }

    /**
     * Create an instance of {@link Trealestateamounts.Amount }
     * 
     */
    public Trealestateamounts.Amount createTrealestateamountsAmount() {
        return new Trealestateamounts.Amount();
    }

    /**
     * Create an instance of {@link Tbalanceanalysismultiplecompaniesresponsemaincompany.Xbrl.Content }
     * 
     */
    public Tbalanceanalysismultiplecompaniesresponsemaincompany.Xbrl.Content createTbalanceanalysismultiplecompaniesresponsemaincompanyXbrlContent() {
        return new Tbalanceanalysismultiplecompaniesresponsemaincompany.Xbrl.Content();
    }

    /**
     * Create an instance of {@link Tcgscoredata.Scoreentry }
     * 
     */
    public Tcgscoredata.Scoreentry createTcgscoredataScoreentry() {
        return new Tcgscoredata.Scoreentry();
    }

    /**
     * Create an instance of {@link Tparticipation.Mastercrefo }
     * 
     */
    public Tparticipation.Mastercrefo createTparticipationMastercrefo() {
        return new Tparticipation.Mastercrefo();
    }

    /**
     * Create an instance of {@link Tcollectionresponsecommunication.Phone }
     * 
     */
    public Tcollectionresponsecommunication.Phone createTcollectionresponsecommunicationPhone() {
        return new Tcollectionresponsecommunication.Phone();
    }

    /**
     * Create an instance of {@link Tcollectionresponsecommunication.Fax }
     * 
     */
    public Tcollectionresponsecommunication.Fax createTcollectionresponsecommunicationFax() {
        return new Tcollectionresponsecommunication.Fax();
    }

    /**
     * Create an instance of {@link Tbalance.Assets }
     * 
     */
    public Tbalance.Assets createTbalanceAssets() {
        return new Tbalance.Assets();
    }

    /**
     * Create an instance of {@link Tbalance.Liabilities }
     * 
     */
    public Tbalance.Liabilities createTbalanceLiabilities() {
        return new Tbalance.Liabilities();
    }

    /**
     * Create an instance of {@link Tcgpaymentbehaviour.Account }
     * 
     */
    public Tcgpaymentbehaviour.Account createTcgpaymentbehaviourAccount() {
        return new Tcgpaymentbehaviour.Account();
    }

    /**
     * Create an instance of {@link Tbalanceanalysismultiplecompaniesresponseadditionalcompany.Xbrl.Content }
     * 
     */
    public Tbalanceanalysismultiplecompaniesresponseadditionalcompany.Xbrl.Content createTbalanceanalysismultiplecompaniesresponseadditionalcompanyXbrlContent() {
        return new Tbalanceanalysismultiplecompaniesresponseadditionalcompany.Xbrl.Content();
    }

    /**
     * Create an instance of {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tfinancialstatementfiguresabstract.Figures.Chapter.Item }
     * 
     */
    public https.onlineservice_creditreform_de.webservice._0600_0021.Tfinancialstatementfiguresabstract.Figures.Chapter.Item createTfinancialstatementfiguresabstractFiguresChapterItem() {
        return new https.onlineservice_creditreform_de.webservice._0600_0021.Tfinancialstatementfiguresabstract.Figures.Chapter.Item();
    }

    /**
     * Create an instance of {@link Tshareholder.Privateperson }
     * 
     */
    public Tshareholder.Privateperson createTshareholderPrivateperson() {
        return new Tshareholder.Privateperson();
    }

    /**
     * Create an instance of {@link Tshareholder.Company }
     * 
     */
    public Tshareholder.Company createTshareholderCompany() {
        return new Tshareholder.Company();
    }

    /**
     * Create an instance of {@link Tcollectionresponsedebtor.Privateperson }
     * 
     */
    public Tcollectionresponsedebtor.Privateperson createTcollectionresponsedebtorPrivateperson() {
        return new Tcollectionresponsedebtor.Privateperson();
    }

    /**
     * Create an instance of {@link Tcollectionresponsedebtor.Company.Namesplit }
     * 
     */
    public Tcollectionresponsedebtor.Company.Namesplit createTcollectionresponsedebtorCompanyNamesplit() {
        return new Tcollectionresponsedebtor.Company.Namesplit();
    }

    /**
     * Create an instance of {@link Tcollectionrequestinterest.Legal }
     * 
     */
    public Tcollectionrequestinterest.Legal createTcollectionrequestinterestLegal() {
        return new Tcollectionrequestinterest.Legal();
    }

    /**
     * Create an instance of {@link Tcollectionrequestinterest.Variable }
     * 
     */
    public Tcollectionrequestinterest.Variable createTcollectionrequestinterestVariable() {
        return new Tcollectionrequestinterest.Variable();
    }

    /**
     * Create an instance of {@link Tcollectionrequestinterest.Fix }
     * 
     */
    public Tcollectionrequestinterest.Fix createTcollectionrequestinterestFix() {
        return new Tcollectionrequestinterest.Fix();
    }

    /**
     * Create an instance of {@link Tcollectionrequestpartreceivable.Consumptionplace }
     * 
     */
    public Tcollectionrequestpartreceivable.Consumptionplace createTcollectionrequestpartreceivableConsumptionplace() {
        return new Tcollectionrequestpartreceivable.Consumptionplace();
    }

    /**
     * Create an instance of {@link Tcgaddressdata.Addressentry }
     * 
     */
    public Tcgaddressdata.Addressentry createTcgaddressdataAddressentry() {
        return new Tcgaddressdata.Addressentry();
    }

    /**
     * Create an instance of {@link Trequestdata.Company }
     * 
     */
    public Trequestdata.Company createTrequestdataCompany() {
        return new Trequestdata.Company();
    }

    /**
     * Create an instance of {@link Trequestdata.Privateperson }
     * 
     */
    public Trequestdata.Privateperson createTrequestdataPrivateperson() {
        return new Trequestdata.Privateperson();
    }

    /**
     * Create an instance of {@link Tancillaryinformationothers.Share }
     * 
     */
    public Tancillaryinformationothers.Share createTancillaryinformationothersShare() {
        return new Tancillaryinformationothers.Share();
    }

    /**
     * Create an instance of {@link Tparticipationsparticipants.Participation.Privateperson }
     * 
     */
    public Tparticipationsparticipants.Participation.Privateperson createTparticipationsparticipantsParticipationPrivateperson() {
        return new Tparticipationsparticipants.Participation.Privateperson();
    }

    /**
     * Create an instance of {@link Tparticipationsparticipants.Participation.Company }
     * 
     */
    public Tparticipationsparticipants.Participation.Company createTparticipationsparticipantsParticipationCompany() {
        return new Tparticipationsparticipants.Participation.Company();
    }

    /**
     * Create an instance of {@link Tlocations.Location }
     * 
     */
    public Tlocations.Location createTlocationsLocation() {
        return new Tlocations.Location();
    }

    /**
     * Create an instance of {@link Tlocations.Furtherlocations }
     * 
     */
    public Tlocations.Furtherlocations createTlocationsFurtherlocations() {
        return new Tlocations.Furtherlocations();
    }

    /**
     * Create an instance of {@link Tparticipationscompany.Propertyparticipant }
     * 
     */
    public Tparticipationscompany.Propertyparticipant createTparticipationscompanyPropertyparticipant() {
        return new Tparticipationscompany.Propertyparticipant();
    }

    /**
     * Create an instance of {@link Tevaluationsfinanceindustry.Classvalue }
     * 
     */
    public Tevaluationsfinanceindustry.Classvalue createTevaluationsfinanceindustryClassvalue() {
        return new Tevaluationsfinanceindustry.Classvalue();
    }

    /**
     * Create an instance of {@link Tevaluationsfinanceindustry.Evaluator.Assignment }
     * 
     */
    public Tevaluationsfinanceindustry.Evaluator.Assignment createTevaluationsfinanceindustryEvaluatorAssignment() {
        return new Tevaluationsfinanceindustry.Evaluator.Assignment();
    }

    /**
     * Create an instance of {@link Tstaffcompany.Fiscalyear.Stafflist }
     * 
     */
    public Tstaffcompany.Fiscalyear.Stafflist createTstaffcompanyFiscalyearStafflist() {
        return new Tstaffcompany.Fiscalyear.Stafflist();
    }

    /**
     * Create an instance of {@link Tbanks.Bank }
     * 
     */
    public Tbanks.Bank createTbanksBank() {
        return new Tbanks.Bank();
    }

    /**
     * Create an instance of {@link Tfoundation.Probabilityofdefault }
     * 
     */
    public Tfoundation.Probabilityofdefault createTfoundationProbabilityofdefault() {
        return new Tfoundation.Probabilityofdefault();
    }

    /**
     * Create an instance of {@link Tturnovercompany.Fiscalyear.Turnoverlist }
     * 
     */
    public Tturnovercompany.Fiscalyear.Turnoverlist createTturnovercompanyFiscalyearTurnoverlist() {
        return new Tturnovercompany.Fiscalyear.Turnoverlist();
    }

    /**
     * Create an instance of {@link Tnegativefact.Evaluation }
     * 
     */
    public Tnegativefact.Evaluation createTnegativefactEvaluation() {
        return new Tnegativefact.Evaluation();
    }

    /**
     * Create an instance of {@link Tnegativefact.Enforcements.Enforcement }
     * 
     */
    public Tnegativefact.Enforcements.Enforcement createTnegativefactEnforcementsEnforcement() {
        return new Tnegativefact.Enforcements.Enforcement();
    }

    /**
     * Create an instance of {@link Tnegativefact.Negativefact.Termofapplication }
     * 
     */
    public Tnegativefact.Negativefact.Termofapplication createTnegativefactNegativefactTermofapplication() {
        return new Tnegativefact.Negativefact.Termofapplication();
    }

    /**
     * Create an instance of {@link Tnegativefact.Negativefact.Partytotheproceedings }
     * 
     */
    public Tnegativefact.Negativefact.Partytotheproceedings createTnegativefactNegativefactPartytotheproceedings() {
        return new Tnegativefact.Negativefact.Partytotheproceedings();
    }

    /**
     * Create an instance of {@link Tnegativefact.Negativefact.Date }
     * 
     */
    public Tnegativefact.Negativefact.Date createTnegativefactNegativefactDate() {
        return new Tnegativefact.Negativefact.Date();
    }

    /**
     * Create an instance of {@link Tnegativefact.Negativefact.Processstep }
     * 
     */
    public Tnegativefact.Negativefact.Processstep createTnegativefactNegativefactProcessstep() {
        return new Tnegativefact.Negativefact.Processstep();
    }

    /**
     * Create an instance of {@link Tcgtelecomdata.Entry }
     * 
     */
    public Tcgtelecomdata.Entry createTcgtelecomdataEntry() {
        return new Tcgtelecomdata.Entry();
    }

    /**
     * Create an instance of {@link Tsolvencyfinanceindustry.Assignments.Assignment }
     * 
     */
    public Tsolvencyfinanceindustry.Assignments.Assignment createTsolvencyfinanceindustryAssignmentsAssignment() {
        return new Tsolvencyfinanceindustry.Assignments.Assignment();
    }

    /**
     * Create an instance of {@link Tcollectionresponseaddress.Addressforservice }
     * 
     */
    public Tcollectionresponseaddress.Addressforservice createTcollectionresponseaddressAddressforservice() {
        return new Tcollectionresponseaddress.Addressforservice();
    }

    /**
     * Create an instance of {@link Tcollectionresponseaddress.Pobaddress }
     * 
     */
    public Tcollectionresponseaddress.Pobaddress createTcollectionresponseaddressPobaddress() {
        return new Tcollectionresponseaddress.Pobaddress();
    }

    /**
     * Create an instance of {@link Tcollectionresponseaddress.Majorcustomeraddress }
     * 
     */
    public Tcollectionresponseaddress.Majorcustomeraddress createTcollectionresponseaddressMajorcustomeraddress() {
        return new Tcollectionresponseaddress.Majorcustomeraddress();
    }

    /**
     * Create an instance of {@link Tregisterhistory.Historyentry.Changeoflegalform }
     * 
     */
    public Tregisterhistory.Historyentry.Changeoflegalform createTregisterhistoryHistoryentryChangeoflegalform() {
        return new Tregisterhistory.Historyentry.Changeoflegalform();
    }

    /**
     * Create an instance of {@link Tregisterhistory.Historyentry.Changeofcompanybase }
     * 
     */
    public Tregisterhistory.Historyentry.Changeofcompanybase createTregisterhistoryHistoryentryChangeofcompanybase() {
        return new Tregisterhistory.Historyentry.Changeofcompanybase();
    }

    /**
     * Create an instance of {@link Tregisterhistory.Historyentry.Changeofname }
     * 
     */
    public Tregisterhistory.Historyentry.Changeofname createTregisterhistoryHistoryentryChangeofname() {
        return new Tregisterhistory.Historyentry.Changeofname();
    }

    /**
     * Create an instance of {@link Tregisterhistory.Historyentry.Registration }
     * 
     */
    public Tregisterhistory.Historyentry.Registration createTregisterhistoryHistoryentryRegistration() {
        return new Tregisterhistory.Historyentry.Registration();
    }

    /**
     * Create an instance of {@link Tregisterhistory.Historyentry.Change }
     * 
     */
    public Tregisterhistory.Historyentry.Change createTregisterhistoryHistoryentryChange() {
        return new Tregisterhistory.Historyentry.Change();
    }

    /**
     * Create an instance of {@link Tregisterhistory.Historyentry.Contract.Contractpartner }
     * 
     */
    public Tregisterhistory.Historyentry.Contract.Contractpartner createTregisterhistoryHistoryentryContractContractpartner() {
        return new Tregisterhistory.Historyentry.Contract.Contractpartner();
    }

    /**
     * Create an instance of {@link Tcollectionrequestdebtor.Privateperson }
     * 
     */
    public Tcollectionrequestdebtor.Privateperson createTcollectionrequestdebtorPrivateperson() {
        return new Tcollectionrequestdebtor.Privateperson();
    }

    /**
     * Create an instance of {@link Tcollectionrequestdebtor.Company.Namesplit }
     * 
     */
    public Tcollectionrequestdebtor.Company.Namesplit createTcollectionrequestdebtorCompanyNamesplit() {
        return new Tcollectionrequestdebtor.Company.Namesplit();
    }

    /**
     * Create an instance of {@link Tcgnegativefacts.Negativefact.Localcourt }
     * 
     */
    public Tcgnegativefacts.Negativefact.Localcourt createTcgnegativefactsNegativefactLocalcourt() {
        return new Tcgnegativefacts.Negativefact.Localcourt();
    }

    /**
     * Create an instance of {@link Tcgnegativefacts.Negativefact.Processsteps.Processstep }
     * 
     */
    public Tcgnegativefacts.Negativefact.Processsteps.Processstep createTcgnegativefactsNegativefactProcessstepsProcessstep() {
        return new Tcgnegativefacts.Negativefact.Processsteps.Processstep();
    }

    /**
     * Create an instance of {@link Tcapacitiesshareholderbasic.Other }
     * 
     */
    public Tcapacitiesshareholderbasic.Other createTcapacitiesshareholderbasicOther() {
        return new Tcapacitiesshareholderbasic.Other();
    }

    /**
     * Create an instance of {@link Tpaymentbehaviour.Branch.History.Entry }
     * 
     */
    public Tpaymentbehaviour.Branch.History.Entry createTpaymentbehaviourBranchHistoryEntry() {
        return new Tpaymentbehaviour.Branch.History.Entry();
    }

    /**
     * Create an instance of {@link Tpaymentbehaviour.History.Entry }
     * 
     */
    public Tpaymentbehaviour.History.Entry createTpaymentbehaviourHistoryEntry() {
        return new Tpaymentbehaviour.History.Entry();
    }

    /**
     * Create an instance of {@link Tcapacitiesshareholder.Other }
     * 
     */
    public Tcapacitiesshareholder.Other createTcapacitiesshareholderOther() {
        return new Tcapacitiesshareholder.Other();
    }

    /**
     * Create an instance of {@link Tsolvencyindex.Probabilityofdefault.Assignments.Assignment }
     * 
     */
    public Tsolvencyindex.Probabilityofdefault.Assignments.Assignment createTsolvencyindexProbabilityofdefaultAssignmentsAssignment() {
        return new Tsolvencyindex.Probabilityofdefault.Assignments.Assignment();
    }

    /**
     * Create an instance of {@link Tcollectionrequestaddress.Addressforservice }
     * 
     */
    public Tcollectionrequestaddress.Addressforservice createTcollectionrequestaddressAddressforservice() {
        return new Tcollectionrequestaddress.Addressforservice();
    }

    /**
     * Create an instance of {@link Tcollectionrequestaddress.Pobaddress }
     * 
     */
    public Tcollectionrequestaddress.Pobaddress createTcollectionrequestaddressPobaddress() {
        return new Tcollectionrequestaddress.Pobaddress();
    }

    /**
     * Create an instance of {@link Tcollectionrequestaddress.Majorcustomeraddress }
     * 
     */
    public Tcollectionrequestaddress.Majorcustomeraddress createTcollectionrequestaddressMajorcustomeraddress() {
        return new Tcollectionrequestaddress.Majorcustomeraddress();
    }

    /**
     * Create an instance of {@link Tprivateperson.Family }
     * 
     */
    public Tprivateperson.Family createTprivatepersonFamily() {
        return new Tprivateperson.Family();
    }

    /**
     * Create an instance of {@link Tallowedkeys.Keyconstraint }
     * 
     */
    public Tallowedkeys.Keyconstraint createTallowedkeysKeyconstraint() {
        return new Tallowedkeys.Keyconstraint();
    }

    /**
     * Create an instance of {@link Tparticipationdataperson.Company }
     * 
     */
    public Tparticipationdataperson.Company createTparticipationdatapersonCompany() {
        return new Tparticipationdataperson.Company();
    }

    /**
     * Create an instance of {@link Tparticipationdataperson.Privateperson }
     * 
     */
    public Tparticipationdataperson.Privateperson createTparticipationdatapersonPrivateperson() {
        return new Tparticipationdataperson.Privateperson();
    }

    /**
     * Create an instance of {@link Tformerlocations.Location }
     * 
     */
    public Tformerlocations.Location createTformerlocationsLocation() {
        return new Tformerlocations.Location();
    }

    /**
     * Create an instance of {@link Tlegalform.Probabilityofdefault }
     * 
     */
    public Tlegalform.Probabilityofdefault createTlegalformProbabilityofdefault() {
        return new Tlegalform.Probabilityofdefault();
    }

    /**
     * Create an instance of {@link Tserviceavailability.Product }
     * 
     */
    public Tserviceavailability.Product createTserviceavailabilityProduct() {
        return new Tserviceavailability.Product();
    }

    /**
     * Create an instance of {@link Tcollectionrequestcommunication.Phone }
     * 
     */
    public Tcollectionrequestcommunication.Phone createTcollectionrequestcommunicationPhone() {
        return new Tcollectionrequestcommunication.Phone();
    }

    /**
     * Create an instance of {@link Tcollectionrequestcommunication.Fax }
     * 
     */
    public Tcollectionrequestcommunication.Fax createTcollectionrequestcommunicationFax() {
        return new Tcollectionrequestcommunication.Fax();
    }

    /**
     * Create an instance of {@link Tturnovergroup.Fiscalyear.Turnoverlist }
     * 
     */
    public Tturnovergroup.Fiscalyear.Turnoverlist createTturnovergroupFiscalyearTurnoverlist() {
        return new Tturnovergroup.Fiscalyear.Turnoverlist();
    }

    /**
     * Create an instance of {@link Tcgformerrequestsdata.Request }
     * 
     */
    public Tcgformerrequestsdata.Request createTcgformerrequestsdataRequest() {
        return new Tcgformerrequestsdata.Request();
    }

    /**
     * Create an instance of {@link Tchangeextendedmonitoringresponsebody.Extendedmonitoring }
     * 
     */
    public Tchangeextendedmonitoringresponsebody.Extendedmonitoring createTchangeextendedmonitoringresponsebodyExtendedmonitoring() {
        return new Tchangeextendedmonitoringresponsebody.Extendedmonitoring();
    }

    /**
     * Create an instance of {@link Tchangeextendedmonitoringresponsebody.Extendedmonitoringplus }
     * 
     */
    public Tchangeextendedmonitoringresponsebody.Extendedmonitoringplus createTchangeextendedmonitoringresponsebodyExtendedmonitoringplus() {
        return new Tchangeextendedmonitoringresponsebody.Extendedmonitoringplus();
    }

    /**
     * Create an instance of {@link Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring }
     * 
     */
    public Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring createTmailboxdirectoryresponsebodyEntryExtendedmonitoring() {
        return new Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring();
    }

    /**
     * Create an instance of {@link Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus }
     * 
     */
    public Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus createTmailboxdirectoryresponsebodyEntryExtendedmonitoringplus() {
        return new Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus();
    }

    /**
     * Create an instance of {@link Tmailboxdirectoryresponsebody.Entry.Actualaddresscompany }
     * 
     */
    public Tmailboxdirectoryresponsebody.Entry.Actualaddresscompany createTmailboxdirectoryresponsebodyEntryActualaddresscompany() {
        return new Tmailboxdirectoryresponsebody.Entry.Actualaddresscompany();
    }

    /**
     * Create an instance of {@link Tmailboxdirectoryresponsebody.Entry.Actualaddressprivate }
     * 
     */
    public Tmailboxdirectoryresponsebody.Entry.Actualaddressprivate createTmailboxdirectoryresponsebodyEntryActualaddressprivate() {
        return new Tmailboxdirectoryresponsebody.Entry.Actualaddressprivate();
    }

    /**
     * Create an instance of {@link Tmailboxdirectoryresponsebody.Entry.Orderaddresscompany }
     * 
     */
    public Tmailboxdirectoryresponsebody.Entry.Orderaddresscompany createTmailboxdirectoryresponsebodyEntryOrderaddresscompany() {
        return new Tmailboxdirectoryresponsebody.Entry.Orderaddresscompany();
    }

    /**
     * Create an instance of {@link Tmailboxdirectoryresponsebody.Entry.Orderaddressprivate }
     * 
     */
    public Tmailboxdirectoryresponsebody.Entry.Orderaddressprivate createTmailboxdirectoryresponsebodyEntryOrderaddressprivate() {
        return new Tmailboxdirectoryresponsebody.Entry.Orderaddressprivate();
    }

    /**
     * Create an instance of {@link Treportrequestbody.Extendedmonitoring }
     * 
     */
    public Treportrequestbody.Extendedmonitoring createTreportrequestbodyExtendedmonitoring() {
        return new Treportrequestbody.Extendedmonitoring();
    }

    /**
     * Create an instance of {@link Tcreditorfreetextmessagequeryresponsebody.File }
     * 
     */
    public Tcreditorfreetextmessagequeryresponsebody.File createTcreditorfreetextmessagequeryresponsebodyFile() {
        return new Tcreditorfreetextmessagequeryresponsebody.File();
    }

    /**
     * Create an instance of {@link Tchangeextendedmonitoringrequestbody.Cancelextendedmonitoring }
     * 
     */
    public Tchangeextendedmonitoringrequestbody.Cancelextendedmonitoring createTchangeextendedmonitoringrequestbodyCancelextendedmonitoring() {
        return new Tchangeextendedmonitoringrequestbody.Cancelextendedmonitoring();
    }

    /**
     * Create an instance of {@link Tupgradelistresponsebody.Entry.Extendedmonitoring }
     * 
     */
    public Tupgradelistresponsebody.Entry.Extendedmonitoring createTupgradelistresponsebodyEntryExtendedmonitoring() {
        return new Tupgradelistresponsebody.Entry.Extendedmonitoring();
    }

    /**
     * Create an instance of {@link Tupgradelistresponsebody.Entry.Extendedmonitoringplus }
     * 
     */
    public Tupgradelistresponsebody.Entry.Extendedmonitoringplus createTupgradelistresponsebodyEntryExtendedmonitoringplus() {
        return new Tupgradelistresponsebody.Entry.Extendedmonitoringplus();
    }

    /**
     * Create an instance of {@link Tupgradelistresponsebody.Entry.Actualaddresscompany }
     * 
     */
    public Tupgradelistresponsebody.Entry.Actualaddresscompany createTupgradelistresponsebodyEntryActualaddresscompany() {
        return new Tupgradelistresponsebody.Entry.Actualaddresscompany();
    }

    /**
     * Create an instance of {@link Tupgradelistresponsebody.Entry.Actualaddressprivate }
     * 
     */
    public Tupgradelistresponsebody.Entry.Actualaddressprivate createTupgradelistresponsebodyEntryActualaddressprivate() {
        return new Tupgradelistresponsebody.Entry.Actualaddressprivate();
    }

    /**
     * Create an instance of {@link Tstatementreceivablesresponsebody.Tableheader }
     * 
     */
    public Tstatementreceivablesresponsebody.Tableheader createTstatementreceivablesresponsebodyTableheader() {
        return new Tstatementreceivablesresponsebody.Tableheader();
    }

    /**
     * Create an instance of {@link Tstatementreceivablesresponsebody.Row }
     * 
     */
    public Tstatementreceivablesresponsebody.Row createTstatementreceivablesresponsebodyRow() {
        return new Tstatementreceivablesresponsebody.Row();
    }

    /**
     * Create an instance of {@link Treceivableresponsebody.Turnover }
     * 
     */
    public Treceivableresponsebody.Turnover createTreceivableresponsebodyTurnover() {
        return new Treceivableresponsebody.Turnover();
    }

    /**
     * Create an instance of {@link Ttitledreceivableresponsebody.Title }
     * 
     */
    public Ttitledreceivableresponsebody.Title createTtitledreceivableresponsebodyTitle() {
        return new Ttitledreceivableresponsebody.Title();
    }

    /**
     * Create an instance of {@link Tidentificationnumbermapresponsebody.Assignment }
     * 
     */
    public Tidentificationnumbermapresponsebody.Assignment createTidentificationnumbermapresponsebodyAssignment() {
        return new Tidentificationnumbermapresponsebody.Assignment();
    }

    /**
     * Create an instance of {@link Tkeylistresponsebody.Keylist.Keygroup.Keyentry }
     * 
     */
    public Tkeylistresponsebody.Keylist.Keygroup.Keyentry createTkeylistresponsebodyKeylistKeygroupKeyentry() {
        return new Tkeylistresponsebody.Keylist.Keygroup.Keyentry();
    }

    /**
     * Create an instance of {@link Tlogonresponsebody.Interfaceinformation }
     * 
     */
    public Tlogonresponsebody.Interfaceinformation createTlogonresponsebodyInterfaceinformation() {
        return new Tlogonresponsebody.Interfaceinformation();
    }

    /**
     * Create an instance of {@link Tlogonresponsebody.Useraccountinformation.Subuser }
     * 
     */
    public Tlogonresponsebody.Useraccountinformation.Subuser createTlogonresponsebodyUseraccountinformationSubuser() {
        return new Tlogonresponsebody.Useraccountinformation.Subuser();
    }

    /**
     * Create an instance of {@link Tcollectionagencyfreetextmessageresponsebody.Message }
     * 
     */
    public Tcollectionagencyfreetextmessageresponsebody.Message createTcollectionagencyfreetextmessageresponsebodyMessage() {
        return new Tcollectionagencyfreetextmessageresponsebody.Message();
    }

    /**
     * Create an instance of {@link Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl.Content }
     * 
     */
    public Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl.Content createTbalanceanalysissinglecompanyresponsebodyCompanyXbrlContent() {
        return new Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl.Content();
    }

    /**
     * Create an instance of {@link Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitem }
     * 
     */
    public Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitem createTcollectionaccountresponsebodyAccountinvoiceInvoicelineitem() {
        return new Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitem();
    }

    /**
     * Create an instance of {@link Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitemamountpayablereductive }
     * 
     */
    public Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitemamountpayablereductive createTcollectionaccountresponsebodyAccountinvoiceInvoicelineitemamountpayablereductive() {
        return new Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitemamountpayablereductive();
    }

    /**
     * Create an instance of {@link Tmonitoringstatusresponsebody.Entry.Actualaddresscompany }
     * 
     */
    public Tmonitoringstatusresponsebody.Entry.Actualaddresscompany createTmonitoringstatusresponsebodyEntryActualaddresscompany() {
        return new Tmonitoringstatusresponsebody.Entry.Actualaddresscompany();
    }

    /**
     * Create an instance of {@link Tmonitoringstatusresponsebody.Entry.Actualaddressprivate }
     * 
     */
    public Tmonitoringstatusresponsebody.Entry.Actualaddressprivate createTmonitoringstatusresponsebodyEntryActualaddressprivate() {
        return new Tmonitoringstatusresponsebody.Entry.Actualaddressprivate();
    }

    /**
     * Create an instance of {@link Tservicefault.Body.Fault }
     * 
     */
    public Tservicefault.Body.Fault createTservicefaultBodyFault() {
        return new Tservicefault.Body.Fault();
    }

    /**
     * Create an instance of {@link Torderrequestbody.Company }
     * 
     */
    public Torderrequestbody.Company createTorderrequestbodyCompany() {
        return new Torderrequestbody.Company();
    }

    /**
     * Create an instance of {@link Torderrequestbody.Privateperson }
     * 
     */
    public Torderrequestbody.Privateperson createTorderrequestbodyPrivateperson() {
        return new Torderrequestbody.Privateperson();
    }

    /**
     * Create an instance of {@link Tprocessresponsebody.Process }
     * 
     */
    public Tprocessresponsebody.Process createTprocessresponsebodyProcess() {
        return new Tprocessresponsebody.Process();
    }

    /**
     * Create an instance of {@link Tcollectionfilesearchresponsebody.Filenumberoverview.File }
     * 
     */
    public Tcollectionfilesearchresponsebody.Filenumberoverview.File createTcollectionfilesearchresponsebodyFilenumberoverviewFile() {
        return new Tcollectionfilesearchresponsebody.Filenumberoverview.File();
    }

    /**
     * Create an instance of {@link Tmigrationdiffresponsebody.Assignment }
     * 
     */
    public Tmigrationdiffresponsebody.Assignment createTmigrationdiffresponsebodyAssignment() {
        return new Tmigrationdiffresponsebody.Assignment();
    }

    /**
     * Create an instance of {@link Tpaymentsresponsebody.Turnover }
     * 
     */
    public Tpaymentsresponsebody.Turnover createTpaymentsresponsebodyTurnover() {
        return new Tpaymentsresponsebody.Turnover();
    }

    /**
     * Create an instance of {@link Treportresponsebody.Reportdata.Financialdata }
     * 
     */
    public Treportresponsebody.Reportdata.Financialdata createTreportresponsebodyReportdataFinancialdata() {
        return new Treportresponsebody.Reportdata.Financialdata();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "collectionfilesearchFault")
    public JAXBElement<Tfault> createCollectionfilesearchFault(Tfault value) {
        return new JAXBElement<Tfault>(_CollectionfilesearchFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "easynumber")
    public JAXBElement<String> createEasynumber(String value) {
        return new JAXBElement<String>(_Easynumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "collectionaccountFault")
    public JAXBElement<Tfault> createCollectionaccountFault(Tfault value) {
        return new JAXBElement<Tfault>(_CollectionaccountFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "keylistFault")
    public JAXBElement<Tfault> createKeylistFault(Tfault value) {
        return new JAXBElement<Tfault>(_KeylistFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "participationdataFault")
    public JAXBElement<Tfault> createParticipationdataFault(Tfault value) {
        return new JAXBElement<Tfault>(_ParticipationdataFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "upgradelistFault")
    public JAXBElement<Tfault> createUpgradelistFault(Tfault value) {
        return new JAXBElement<Tfault>(_UpgradelistFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "bonimareportFault")
    public JAXBElement<Tfault> createBonimareportFault(Tfault value) {
        return new JAXBElement<Tfault>(_BonimareportFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "participationdatastartFault")
    public JAXBElement<Tfault> createParticipationdatastartFault(Tfault value) {
        return new JAXBElement<Tfault>(_ParticipationdatastartFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "monitoringstatusFault")
    public JAXBElement<Tfault> createMonitoringstatusFault(Tfault value) {
        return new JAXBElement<Tfault>(_MonitoringstatusFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "mailboxentryFault")
    public JAXBElement<Tfault> createMailboxentryFault(Tfault value) {
        return new JAXBElement<Tfault>(_MailboxentryFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "collectionagencydocumentFault")
    public JAXBElement<Tfault> createCollectionagencydocumentFault(Tfault value) {
        return new JAXBElement<Tfault>(_CollectionagencydocumentFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tkeywithdescription }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "status")
    public JAXBElement<Tkeywithdescription> createStatus(Tkeywithdescription value) {
        return new JAXBElement<Tkeywithdescription>(_Status_QNAME, Tkeywithdescription.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "identificationnumber")
    public JAXBElement<String> createIdentificationnumber(String value) {
        return new JAXBElement<String>(_Identificationnumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "balanceanalysissinglecompanyFault")
    public JAXBElement<Tfault> createBalanceanalysissinglecompanyFault(Tfault value) {
        return new JAXBElement<Tfault>(_BalanceanalysissinglecompanyFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "identificationnumbermapFault")
    public JAXBElement<Tfault> createIdentificationnumbermapFault(Tfault value) {
        return new JAXBElement<Tfault>(_IdentificationnumbermapFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "collectionagencyfreetextmessageFault")
    public JAXBElement<Tfault> createCollectionagencyfreetextmessageFault(Tfault value) {
        return new JAXBElement<Tfault>(_CollectionagencyfreetextmessageFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "validationfault")
    public JAXBElement<String> createValidationfault(String value) {
        return new JAXBElement<String>(_Validationfault_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "mailboxdirectoryFault")
    public JAXBElement<Tfault> createMailboxdirectoryFault(Tfault value) {
        return new JAXBElement<Tfault>(_MailboxdirectoryFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "receivableFault")
    public JAXBElement<Tfault> createReceivableFault(Tfault value) {
        return new JAXBElement<Tfault>(_ReceivableFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "changepasswordFault")
    public JAXBElement<Tfault> createChangepasswordFault(Tfault value) {
        return new JAXBElement<Tfault>(_ChangepasswordFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "reportFault")
    public JAXBElement<Tfault> createReportFault(Tfault value) {
        return new JAXBElement<Tfault>(_ReportFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "statementreceivablesFault")
    public JAXBElement<Tfault> createStatementreceivablesFault(Tfault value) {
        return new JAXBElement<Tfault>(_StatementreceivablesFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "searchFault")
    public JAXBElement<Tfault> createSearchFault(Tfault value) {
        return new JAXBElement<Tfault>(_SearchFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "changeextendedmonitoringFault")
    public JAXBElement<Tfault> createChangeextendedmonitoringFault(Tfault value) {
        return new JAXBElement<Tfault>(_ChangeextendedmonitoringFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "titledreceivableFault")
    public JAXBElement<Tfault> createTitledreceivableFault(Tfault value) {
        return new JAXBElement<Tfault>(_TitledreceivableFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "referencenumber")
    public JAXBElement<Long> createReferencenumber(Long value) {
        return new JAXBElement<Long>(_Referencenumber_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "cancelstandardmonitoringFault")
    public JAXBElement<Tfault> createCancelstandardmonitoringFault(Tfault value) {
        return new JAXBElement<Tfault>(_CancelstandardmonitoringFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "logonFault")
    public JAXBElement<Tfault> createLogonFault(Tfault value) {
        return new JAXBElement<Tfault>(_LogonFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "paymentsFault")
    public JAXBElement<Tfault> createPaymentsFault(Tfault value) {
        return new JAXBElement<Tfault>(_PaymentsFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "creditorfreetextmessagequeryFault")
    public JAXBElement<Tfault> createCreditorfreetextmessagequeryFault(Tfault value) {
        return new JAXBElement<Tfault>(_CreditorfreetextmessagequeryFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "changeemailFault")
    public JAXBElement<Tfault> createChangeemailFault(Tfault value) {
        return new JAXBElement<Tfault>(_ChangeemailFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "collectionorderFault")
    public JAXBElement<Tfault> createCollectionorderFault(Tfault value) {
        return new JAXBElement<Tfault>(_CollectionorderFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "processFault")
    public JAXBElement<Tfault> createProcessFault(Tfault value) {
        return new JAXBElement<Tfault>(_ProcessFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "migrationdiffFault")
    public JAXBElement<Tfault> createMigrationdiffFault(Tfault value) {
        return new JAXBElement<Tfault>(_MigrationdiffFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "orderFault")
    public JAXBElement<Tfault> createOrderFault(Tfault value) {
        return new JAXBElement<Tfault>(_OrderFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "identificationreportFault")
    public JAXBElement<Tfault> createIdentificationreportFault(Tfault value) {
        return new JAXBElement<Tfault>(_IdentificationreportFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "mailboxentrynumber")
    public JAXBElement<String> createMailboxentrynumber(String value) {
        return new JAXBElement<String>(_Mailboxentrynumber_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tservicefault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "servicefault")
    public JAXBElement<Tservicefault> createServicefault(Tservicefault value) {
        return new JAXBElement<Tservicefault>(_Servicefault_QNAME, Tservicefault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "generalinformationFault")
    public JAXBElement<Tfault> createGeneralinformationFault(Tfault value) {
        return new JAXBElement<Tfault>(_GeneralinformationFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "productavailabilityFault")
    public JAXBElement<Tfault> createProductavailabilityFault(Tfault value) {
        return new JAXBElement<Tfault>(_ProductavailabilityFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Long }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "participationdatareference")
    public JAXBElement<Long> createParticipationdatareference(Long value) {
        return new JAXBElement<Long>(_Participationdatareference_QNAME, Long.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "forwardreceivableFault")
    public JAXBElement<Tfault> createForwardreceivableFault(Tfault value) {
        return new JAXBElement<Tfault>(_ForwardreceivableFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "consumerreportFault")
    public JAXBElement<Tfault> createConsumerreportFault(Tfault value) {
        return new JAXBElement<Tfault>(_ConsumerreportFault_QNAME, Tfault.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tfault }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "https://onlineservice.creditreform.de/webservice/0600-0021", name = "creditorfreetextmessageFault")
    public JAXBElement<Tfault> createCreditorfreetextmessageFault(Tfault value) {
        return new JAXBElement<Tfault>(_CreditorfreetextmessageFault_QNAME, Tfault.class, null, value);
    }

}
