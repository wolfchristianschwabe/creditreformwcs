
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * Beteiligung/Anteil
 * 
 * <p>Java-Klasse f�r Tshare complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tshare">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capacityparticipant" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="participatingsince" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;element name="participatingtill" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;element name="capitalshare" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamountpercent" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tshare", propOrder = {
    "capacityparticipant",
    "participatingsince",
    "participatingtill",
    "capitalshare"
})
public class Tshare {

    protected Tkey capacityparticipant;
    protected String participatingsince;
    protected String participatingtill;
    protected Tamountpercent capitalshare;

    /**
     * Ruft den Wert der capacityparticipant-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCapacityparticipant() {
        return capacityparticipant;
    }

    /**
     * Legt den Wert der capacityparticipant-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCapacityparticipant(Tkey value) {
        this.capacityparticipant = value;
    }

    /**
     * Ruft den Wert der participatingsince-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipatingsince() {
        return participatingsince;
    }

    /**
     * Legt den Wert der participatingsince-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipatingsince(String value) {
        this.participatingsince = value;
    }

    /**
     * Ruft den Wert der participatingtill-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipatingtill() {
        return participatingtill;
    }

    /**
     * Legt den Wert der participatingtill-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipatingtill(String value) {
        this.participatingtill = value;
    }

    /**
     * Ruft den Wert der capitalshare-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamountpercent }
     *     
     */
    public Tamountpercent getCapitalshare() {
        return capitalshare;
    }

    /**
     * Legt den Wert der capitalshare-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamountpercent }
     *     
     */
    public void setCapitalshare(Tamountpercent value) {
        this.capitalshare = value;
    }

}
