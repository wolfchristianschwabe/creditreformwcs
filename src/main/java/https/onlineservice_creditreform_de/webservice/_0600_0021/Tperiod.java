
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tperiod complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tperiod">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}periodgroup"/>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tperiod", propOrder = {
    "datestart",
    "dateend"
})
public class Tperiod {

    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datestart;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateend;

    /**
     * Ruft den Wert der datestart-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatestart() {
        return datestart;
    }

    /**
     * Legt den Wert der datestart-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatestart(XMLGregorianCalendar value) {
        this.datestart = value;
    }

    /**
     * Ruft den Wert der dateend-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateend() {
        return dateend;
    }

    /**
     * Legt den Wert der dateend-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateend(XMLGregorianCalendar value) {
        this.dateend = value;
    }

}
