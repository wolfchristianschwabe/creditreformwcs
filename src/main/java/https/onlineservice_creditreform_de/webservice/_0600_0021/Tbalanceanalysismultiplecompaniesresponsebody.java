
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tbalanceanalysismultiplecompaniesresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tbalanceanalysismultiplecompaniesresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *         &lt;element name="creationtime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="maincompany" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalanceanalysismultiplecompaniesresponsemaincompany" minOccurs="0"/>
 *         &lt;element name="additionalcompany" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalanceanalysismultiplecompaniesresponseadditionalcompany" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="textreport" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="textreporttwo" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="additionaldata" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tbalanceanalysismultiplecompaniesresponsebody", propOrder = {
    "customerreference",
    "producttype",
    "creationtime",
    "maincompany",
    "additionalcompany",
    "textreport",
    "textreporttwo",
    "additionaldata"
})
public class Tbalanceanalysismultiplecompaniesresponsebody {

    protected String customerreference;
    @XmlElement(required = true)
    protected Tkey producttype;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationtime;
    protected Tbalanceanalysismultiplecompaniesresponsemaincompany maincompany;
    protected List<Tbalanceanalysismultiplecompaniesresponseadditionalcompany> additionalcompany;
    @XmlMimeType("application/octet-stream")
    protected DataHandler textreport;
    @XmlMimeType("application/octet-stream")
    protected DataHandler textreporttwo;
    @XmlMimeType("application/octet-stream")
    protected DataHandler additionaldata;

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setProducttype(Tkey value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der creationtime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationtime() {
        return creationtime;
    }

    /**
     * Legt den Wert der creationtime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationtime(XMLGregorianCalendar value) {
        this.creationtime = value;
    }

    /**
     * Ruft den Wert der maincompany-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tbalanceanalysismultiplecompaniesresponsemaincompany }
     *     
     */
    public Tbalanceanalysismultiplecompaniesresponsemaincompany getMaincompany() {
        return maincompany;
    }

    /**
     * Legt den Wert der maincompany-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tbalanceanalysismultiplecompaniesresponsemaincompany }
     *     
     */
    public void setMaincompany(Tbalanceanalysismultiplecompaniesresponsemaincompany value) {
        this.maincompany = value;
    }

    /**
     * Gets the value of the additionalcompany property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the additionalcompany property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAdditionalcompany().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tbalanceanalysismultiplecompaniesresponseadditionalcompany }
     * 
     * 
     */
    public List<Tbalanceanalysismultiplecompaniesresponseadditionalcompany> getAdditionalcompany() {
        if (additionalcompany == null) {
            additionalcompany = new ArrayList<Tbalanceanalysismultiplecompaniesresponseadditionalcompany>();
        }
        return this.additionalcompany;
    }

    /**
     * Ruft den Wert der textreport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getTextreport() {
        return textreport;
    }

    /**
     * Legt den Wert der textreport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setTextreport(DataHandler value) {
        this.textreport = value;
    }

    /**
     * Ruft den Wert der textreporttwo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getTextreporttwo() {
        return textreporttwo;
    }

    /**
     * Legt den Wert der textreporttwo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setTextreporttwo(DataHandler value) {
        this.textreporttwo = value;
    }

    /**
     * Ruft den Wert der additionaldata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getAdditionaldata() {
        return additionaldata;
    }

    /**
     * Legt den Wert der additionaldata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setAdditionaldata(DataHandler value) {
        this.additionaldata = value;
    }

}
