
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tbalance complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tbalance">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="reportperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timpreciseperiod" minOccurs="0"/>
 *         &lt;element name="jurisdiction" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="balancecategory" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *         &lt;element name="currency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="assets" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="assetheader" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                   &lt;element name="assets" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheetitem" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="sumassets" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheetsum" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="liabilities" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="liabilitiesheader" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                   &lt;element name="liabilities" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheetitem" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="sumliabilities" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheetsum" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tbalance", propOrder = {
    "reportperiod",
    "jurisdiction",
    "balancecategory",
    "currency",
    "assets",
    "liabilities"
})
public class Tbalance {

    protected Timpreciseperiod reportperiod;
    protected Tkey jurisdiction;
    protected Tkeywithshortdesignation balancecategory;
    protected Tkey currency;
    protected Tbalance.Assets assets;
    protected Tbalance.Liabilities liabilities;

    /**
     * Ruft den Wert der reportperiod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Timpreciseperiod }
     *     
     */
    public Timpreciseperiod getReportperiod() {
        return reportperiod;
    }

    /**
     * Legt den Wert der reportperiod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Timpreciseperiod }
     *     
     */
    public void setReportperiod(Timpreciseperiod value) {
        this.reportperiod = value;
    }

    /**
     * Ruft den Wert der jurisdiction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getJurisdiction() {
        return jurisdiction;
    }

    /**
     * Legt den Wert der jurisdiction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setJurisdiction(Tkey value) {
        this.jurisdiction = value;
    }

    /**
     * Ruft den Wert der balancecategory-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithshortdesignation }
     *     
     */
    public Tkeywithshortdesignation getBalancecategory() {
        return balancecategory;
    }

    /**
     * Legt den Wert der balancecategory-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithshortdesignation }
     *     
     */
    public void setBalancecategory(Tkeywithshortdesignation value) {
        this.balancecategory = value;
    }

    /**
     * Ruft den Wert der currency-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCurrency() {
        return currency;
    }

    /**
     * Legt den Wert der currency-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCurrency(Tkey value) {
        this.currency = value;
    }

    /**
     * Ruft den Wert der assets-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tbalance.Assets }
     *     
     */
    public Tbalance.Assets getAssets() {
        return assets;
    }

    /**
     * Legt den Wert der assets-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tbalance.Assets }
     *     
     */
    public void setAssets(Tbalance.Assets value) {
        this.assets = value;
    }

    /**
     * Ruft den Wert der liabilities-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tbalance.Liabilities }
     *     
     */
    public Tbalance.Liabilities getLiabilities() {
        return liabilities;
    }

    /**
     * Legt den Wert der liabilities-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tbalance.Liabilities }
     *     
     */
    public void setLiabilities(Tbalance.Liabilities value) {
        this.liabilities = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="assetheader" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *         &lt;element name="assets" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheetitem" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="sumassets" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheetsum" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "assetheader",
        "assets",
        "sumassets"
    })
    public static class Assets {

        protected Ttext assetheader;
        protected List<Tbalancesheetitem> assets;
        protected Tbalancesheetsum sumassets;

        /**
         * Ruft den Wert der assetheader-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getAssetheader() {
            return assetheader;
        }

        /**
         * Legt den Wert der assetheader-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setAssetheader(Ttext value) {
            this.assetheader = value;
        }

        /**
         * Gets the value of the assets property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the assets property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getAssets().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tbalancesheetitem }
         * 
         * 
         */
        public List<Tbalancesheetitem> getAssets() {
            if (assets == null) {
                assets = new ArrayList<Tbalancesheetitem>();
            }
            return this.assets;
        }

        /**
         * Ruft den Wert der sumassets-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbalancesheetsum }
         *     
         */
        public Tbalancesheetsum getSumassets() {
            return sumassets;
        }

        /**
         * Legt den Wert der sumassets-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbalancesheetsum }
         *     
         */
        public void setSumassets(Tbalancesheetsum value) {
            this.sumassets = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="liabilitiesheader" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *         &lt;element name="liabilities" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheetitem" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="sumliabilities" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheetsum" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "liabilitiesheader",
        "liabilities",
        "sumliabilities"
    })
    public static class Liabilities {

        protected Ttext liabilitiesheader;
        protected List<Tbalancesheetitem> liabilities;
        protected Tbalancesheetsum sumliabilities;

        /**
         * Ruft den Wert der liabilitiesheader-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getLiabilitiesheader() {
            return liabilitiesheader;
        }

        /**
         * Legt den Wert der liabilitiesheader-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setLiabilitiesheader(Ttext value) {
            this.liabilitiesheader = value;
        }

        /**
         * Gets the value of the liabilities property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the liabilities property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getLiabilities().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tbalancesheetitem }
         * 
         * 
         */
        public List<Tbalancesheetitem> getLiabilities() {
            if (liabilities == null) {
                liabilities = new ArrayList<Tbalancesheetitem>();
            }
            return this.liabilities;
        }

        /**
         * Ruft den Wert der sumliabilities-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbalancesheetsum }
         *     
         */
        public Tbalancesheetsum getSumliabilities() {
            return sumliabilities;
        }

        /**
         * Legt den Wert der sumliabilities-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbalancesheetsum }
         *     
         */
        public void setSumliabilities(Tbalancesheetsum value) {
            this.sumliabilities = value;
        }

    }

}
