
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcgscoredata complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcgscoredata">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="scoreentry" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="scoretype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="scorevalue" minOccurs="0">
 *                     &lt;simpleType>
 *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *                         &lt;maxInclusive value="99999"/>
 *                         &lt;minInclusive value="-99999"/>
 *                       &lt;/restriction>
 *                     &lt;/simpleType>
 *                   &lt;/element>
 *                   &lt;element name="trafficlightscore" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcgscoredata", propOrder = {
    "scoreentry"
})
public class Tcgscoredata {

    protected List<Tcgscoredata.Scoreentry> scoreentry;

    /**
     * Gets the value of the scoreentry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scoreentry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScoreentry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcgscoredata.Scoreentry }
     * 
     * 
     */
    public List<Tcgscoredata.Scoreentry> getScoreentry() {
        if (scoreentry == null) {
            scoreentry = new ArrayList<Tcgscoredata.Scoreentry>();
        }
        return this.scoreentry;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="scoretype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="scorevalue" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
     *               &lt;maxInclusive value="99999"/>
     *               &lt;minInclusive value="-99999"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *         &lt;element name="trafficlightscore" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "scoretype",
        "scorevalue",
        "trafficlightscore"
    })
    public static class Scoreentry {

        protected Tkey scoretype;
        protected Integer scorevalue;
        protected Tkeywithgrade trafficlightscore;

        /**
         * Ruft den Wert der scoretype-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getScoretype() {
            return scoretype;
        }

        /**
         * Legt den Wert der scoretype-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setScoretype(Tkey value) {
            this.scoretype = value;
        }

        /**
         * Ruft den Wert der scorevalue-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getScorevalue() {
            return scorevalue;
        }

        /**
         * Legt den Wert der scorevalue-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setScorevalue(Integer value) {
            this.scorevalue = value;
        }

        /**
         * Ruft den Wert der trafficlightscore-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithgrade }
         *     
         */
        public Tkeywithgrade getTrafficlightscore() {
            return trafficlightscore;
        }

        /**
         * Legt den Wert der trafficlightscore-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithgrade }
         *     
         */
        public void setTrafficlightscore(Tkeywithgrade value) {
            this.trafficlightscore = value;
        }

    }

}
