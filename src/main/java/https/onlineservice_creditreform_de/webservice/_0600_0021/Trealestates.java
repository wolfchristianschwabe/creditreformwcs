
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Trealestates complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Trealestates">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="realestate" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;extension base="{https://onlineservice.creditreform.de/webservice/0600-0021}Trealestate">
 *                 &lt;sequence>
 *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}addressstructure"/>
 *                   &lt;element name="operationalarea" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                             &lt;element name="areadecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;element name="rent" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="ownershipinterest" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="estatearea" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *                   &lt;element name="estateareadecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="realestatetype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="insurancevalue" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/extension>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="totallandregisterinspection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="totallandregisterinspectionmeaning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="furtherrealestates" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trealestate" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Trealestates", propOrder = {
    "realestate",
    "totallandregisterinspection",
    "totallandregisterinspectionmeaning",
    "furtherrealestates",
    "text"
})
public class Trealestates {

    protected List<Trealestates.Realestate> realestate;
    protected Boolean totallandregisterinspection;
    protected String totallandregisterinspectionmeaning;
    protected Trealestate furtherrealestates;
    protected Ttext text;

    /**
     * Gets the value of the realestate property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the realestate property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRealestate().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Trealestates.Realestate }
     * 
     * 
     */
    public List<Trealestates.Realestate> getRealestate() {
        if (realestate == null) {
            realestate = new ArrayList<Trealestates.Realestate>();
        }
        return this.realestate;
    }

    /**
     * Ruft den Wert der totallandregisterinspection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isTotallandregisterinspection() {
        return totallandregisterinspection;
    }

    /**
     * Legt den Wert der totallandregisterinspection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setTotallandregisterinspection(Boolean value) {
        this.totallandregisterinspection = value;
    }

    /**
     * Ruft den Wert der totallandregisterinspectionmeaning-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotallandregisterinspectionmeaning() {
        return totallandregisterinspectionmeaning;
    }

    /**
     * Legt den Wert der totallandregisterinspectionmeaning-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotallandregisterinspectionmeaning(String value) {
        this.totallandregisterinspectionmeaning = value;
    }

    /**
     * Ruft den Wert der furtherrealestates-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Trealestate }
     *     
     */
    public Trealestate getFurtherrealestates() {
        return furtherrealestates;
    }

    /**
     * Legt den Wert der furtherrealestates-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Trealestate }
     *     
     */
    public void setFurtherrealestates(Trealestate value) {
        this.furtherrealestates = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;extension base="{https://onlineservice.creditreform.de/webservice/0600-0021}Trealestate">
     *       &lt;sequence>
     *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}addressstructure"/>
     *         &lt;element name="operationalarea" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *                   &lt;element name="areadecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;element name="rent" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="ownershipinterest" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="estatearea" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
     *         &lt;element name="estateareadecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="realestatetype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="insurancevalue" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/extension>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "street",
        "housenumber",
        "housenumberaffix",
        "postcode",
        "city",
        "quarter",
        "region",
        "country",
        "operationalarea",
        "ownershipinterest",
        "estatearea",
        "estateareadecoration",
        "realestatetype",
        "insurancevalue"
    })
    public static class Realestate
        extends Trealestate
    {

        protected String street;
        protected Integer housenumber;
        protected String housenumberaffix;
        protected String postcode;
        protected String city;
        protected String quarter;
        protected String region;
        protected Tkey country;
        protected Trealestates.Realestate.Operationalarea operationalarea;
        protected Integer ownershipinterest;
        protected Long estatearea;
        protected Tkey estateareadecoration;
        protected Tkey realestatetype;
        protected Tamount insurancevalue;

        /**
         * Ruft den Wert der street-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreet() {
            return street;
        }

        /**
         * Legt den Wert der street-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreet(String value) {
            this.street = value;
        }

        /**
         * Ruft den Wert der housenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getHousenumber() {
            return housenumber;
        }

        /**
         * Legt den Wert der housenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setHousenumber(Integer value) {
            this.housenumber = value;
        }

        /**
         * Ruft den Wert der housenumberaffix-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHousenumberaffix() {
            return housenumberaffix;
        }

        /**
         * Legt den Wert der housenumberaffix-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHousenumberaffix(String value) {
            this.housenumberaffix = value;
        }

        /**
         * Ruft den Wert der postcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostcode() {
            return postcode;
        }

        /**
         * Legt den Wert der postcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostcode(String value) {
            this.postcode = value;
        }

        /**
         * Ruft den Wert der city-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Legt den Wert der city-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Ruft den Wert der quarter-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuarter() {
            return quarter;
        }

        /**
         * Legt den Wert der quarter-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuarter(String value) {
            this.quarter = value;
        }

        /**
         * Ruft den Wert der region-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegion() {
            return region;
        }

        /**
         * Legt den Wert der region-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegion(String value) {
            this.region = value;
        }

        /**
         * Ruft den Wert der country-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getCountry() {
            return country;
        }

        /**
         * Legt den Wert der country-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setCountry(Tkey value) {
            this.country = value;
        }

        /**
         * Ruft den Wert der operationalarea-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Trealestates.Realestate.Operationalarea }
         *     
         */
        public Trealestates.Realestate.Operationalarea getOperationalarea() {
            return operationalarea;
        }

        /**
         * Legt den Wert der operationalarea-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Trealestates.Realestate.Operationalarea }
         *     
         */
        public void setOperationalarea(Trealestates.Realestate.Operationalarea value) {
            this.operationalarea = value;
        }

        /**
         * Ruft den Wert der ownershipinterest-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getOwnershipinterest() {
            return ownershipinterest;
        }

        /**
         * Legt den Wert der ownershipinterest-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setOwnershipinterest(Integer value) {
            this.ownershipinterest = value;
        }

        /**
         * Ruft den Wert der estatearea-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getEstatearea() {
            return estatearea;
        }

        /**
         * Legt den Wert der estatearea-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setEstatearea(Long value) {
            this.estatearea = value;
        }

        /**
         * Ruft den Wert der estateareadecoration-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getEstateareadecoration() {
            return estateareadecoration;
        }

        /**
         * Legt den Wert der estateareadecoration-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setEstateareadecoration(Tkey value) {
            this.estateareadecoration = value;
        }

        /**
         * Ruft den Wert der realestatetype-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getRealestatetype() {
            return realestatetype;
        }

        /**
         * Legt den Wert der realestatetype-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setRealestatetype(Tkey value) {
            this.realestatetype = value;
        }

        /**
         * Ruft den Wert der insurancevalue-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tamount }
         *     
         */
        public Tamount getInsurancevalue() {
            return insurancevalue;
        }

        /**
         * Legt den Wert der insurancevalue-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tamount }
         *     
         */
        public void setInsurancevalue(Tamount value) {
            this.insurancevalue = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="area" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
         *         &lt;element name="areadecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="rent" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "area",
            "areadecoration",
            "rent"
        })
        public static class Operationalarea {

            protected Long area;
            protected Tkey areadecoration;
            protected Tamount rent;

            /**
             * Ruft den Wert der area-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Long }
             *     
             */
            public Long getArea() {
                return area;
            }

            /**
             * Legt den Wert der area-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Long }
             *     
             */
            public void setArea(Long value) {
                this.area = value;
            }

            /**
             * Ruft den Wert der areadecoration-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getAreadecoration() {
                return areadecoration;
            }

            /**
             * Legt den Wert der areadecoration-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setAreadecoration(Tkey value) {
                this.areadecoration = value;
            }

            /**
             * Ruft den Wert der rent-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tamount }
             *     
             */
            public Tamount getRent() {
                return rent;
            }

            /**
             * Legt den Wert der rent-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tamount }
             *     
             */
            public void setRent(Tamount value) {
                this.rent = value;
            }

        }

    }

}
