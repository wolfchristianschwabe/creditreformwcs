
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tchangeextendedmonitoringrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tchangeextendedmonitoringrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}referencenumber"/>
 *         &lt;choice>
 *           &lt;element name="cancelextendedmonitoring">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="cancel" minOccurs="0">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}boolean">
 *                           &lt;pattern value="1"/>
 *                           &lt;pattern value="true"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}requestmonitoringstructure"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tchangeextendedmonitoringrequestbody", propOrder = {
    "referencenumber",
    "cancelextendedmonitoring",
    "extendedmonitoring"
})
public class Tchangeextendedmonitoringrequestbody {

    protected long referencenumber;
    protected Tchangeextendedmonitoringrequestbody.Cancelextendedmonitoring cancelextendedmonitoring;
    protected https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody.Extendedmonitoring extendedmonitoring;

    /**
     * Ruft den Wert der referencenumber-Eigenschaft ab.
     * 
     */
    public long getReferencenumber() {
        return referencenumber;
    }

    /**
     * Legt den Wert der referencenumber-Eigenschaft fest.
     * 
     */
    public void setReferencenumber(long value) {
        this.referencenumber = value;
    }

    /**
     * Ruft den Wert der cancelextendedmonitoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tchangeextendedmonitoringrequestbody.Cancelextendedmonitoring }
     *     
     */
    public Tchangeextendedmonitoringrequestbody.Cancelextendedmonitoring getCancelextendedmonitoring() {
        return cancelextendedmonitoring;
    }

    /**
     * Legt den Wert der cancelextendedmonitoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tchangeextendedmonitoringrequestbody.Cancelextendedmonitoring }
     *     
     */
    public void setCancelextendedmonitoring(Tchangeextendedmonitoringrequestbody.Cancelextendedmonitoring value) {
        this.cancelextendedmonitoring = value;
    }

    /**
     * Ruft den Wert der extendedmonitoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody.Extendedmonitoring }
     *     
     */
    public https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody.Extendedmonitoring getExtendedmonitoring() {
        return extendedmonitoring;
    }

    /**
     * Legt den Wert der extendedmonitoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody.Extendedmonitoring }
     *     
     */
    public void setExtendedmonitoring(https.onlineservice_creditreform_de.webservice._0600_0021.Treportrequestbody.Extendedmonitoring value) {
        this.extendedmonitoring = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="cancel" minOccurs="0">
     *           &lt;simpleType>
     *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}boolean">
     *               &lt;pattern value="1"/>
     *               &lt;pattern value="true"/>
     *             &lt;/restriction>
     *           &lt;/simpleType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cancel"
    })
    public static class Cancelextendedmonitoring {

        protected Boolean cancel;

        /**
         * Ruft den Wert der cancel-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isCancel() {
            return cancel;
        }

        /**
         * Legt den Wert der cancel-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setCancel(Boolean value) {
            this.cancel = value;
        }

    }

}
