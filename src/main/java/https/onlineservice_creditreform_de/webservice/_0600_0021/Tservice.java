
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tservice complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tservice">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="operation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="elementconstraint" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Telementconstraint" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="allowedkeys" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tallowedkeys" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="countryconstraint" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcountryconstraint" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tservice", propOrder = {
    "operation",
    "elementconstraint",
    "allowedkeys",
    "countryconstraint"
})
public class Tservice {

    @XmlElement(required = true)
    protected String operation;
    protected List<Telementconstraint> elementconstraint;
    protected List<Tallowedkeys> allowedkeys;
    protected List<Tcountryconstraint> countryconstraint;

    /**
     * Ruft den Wert der operation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperation() {
        return operation;
    }

    /**
     * Legt den Wert der operation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperation(String value) {
        this.operation = value;
    }

    /**
     * Gets the value of the elementconstraint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the elementconstraint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getElementconstraint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Telementconstraint }
     * 
     * 
     */
    public List<Telementconstraint> getElementconstraint() {
        if (elementconstraint == null) {
            elementconstraint = new ArrayList<Telementconstraint>();
        }
        return this.elementconstraint;
    }

    /**
     * Gets the value of the allowedkeys property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the allowedkeys property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAllowedkeys().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tallowedkeys }
     * 
     * 
     */
    public List<Tallowedkeys> getAllowedkeys() {
        if (allowedkeys == null) {
            allowedkeys = new ArrayList<Tallowedkeys>();
        }
        return this.allowedkeys;
    }

    /**
     * Gets the value of the countryconstraint property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the countryconstraint property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCountryconstraint().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcountryconstraint }
     * 
     * 
     */
    public List<Tcountryconstraint> getCountryconstraint() {
        if (countryconstraint == null) {
            countryconstraint = new ArrayList<Tcountryconstraint>();
        }
        return this.countryconstraint;
    }

}
