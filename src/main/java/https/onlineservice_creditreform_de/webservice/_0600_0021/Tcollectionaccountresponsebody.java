
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tcollectionaccountresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionaccountresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="accountinvoice" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="accountinvoicenumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                   &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="currency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *                   &lt;element name="interest" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="dateinvoice" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="invoicelineitem" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="positionnumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="notation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="cancellation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *                             &lt;element name="totalamount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="vatrate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="invoicelineitemamountpayablereductive" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="positionnumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *                             &lt;element name="notation" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="totalamount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                             &lt;element name="vatrate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="nettotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="totalvatrate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="totalvatamount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="totalgrossamountpayable" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="amountpayable" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="collectionagencyexpenses" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionaccountresponsebody", propOrder = {
    "accountinvoice",
    "text"
})
public class Tcollectionaccountresponsebody {

    protected List<Tcollectionaccountresponsebody.Accountinvoice> accountinvoice;
    protected Ttext text;

    /**
     * Gets the value of the accountinvoice property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the accountinvoice property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAccountinvoice().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcollectionaccountresponsebody.Accountinvoice }
     * 
     * 
     */
    public List<Tcollectionaccountresponsebody.Accountinvoice> getAccountinvoice() {
        if (accountinvoice == null) {
            accountinvoice = new ArrayList<Tcollectionaccountresponsebody.Accountinvoice>();
        }
        return this.accountinvoice;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="accountinvoicenumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *         &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="currency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
     *         &lt;element name="interest" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="dateinvoice" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="invoicelineitem" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="positionnumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="notation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="cancellation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *                   &lt;element name="totalamount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="vatrate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="invoicelineitemamountpayablereductive" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="positionnumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
     *                   &lt;element name="notation" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="totalamount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                   &lt;element name="vatrate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="nettotal" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="totalvatrate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="totalvatamount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="totalgrossamountpayable" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="amountpayable" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="collectionagencyexpenses" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "accountinvoicenumber",
        "filenumber",
        "currency",
        "interest",
        "dateinvoice",
        "invoicelineitem",
        "invoicelineitemamountpayablereductive",
        "nettotal",
        "totalvatrate",
        "totalvatamount",
        "totalgrossamountpayable",
        "amountpayable",
        "collectionagencyexpenses",
        "text"
    })
    public static class Accountinvoice {

        protected long accountinvoicenumber;
        @XmlElement(required = true)
        protected String filenumber;
        @XmlElement(required = true)
        protected Tkey currency;
        @XmlElement(required = true)
        protected BigDecimal interest;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateinvoice;
        protected List<Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitem> invoicelineitem;
        protected List<Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitemamountpayablereductive> invoicelineitemamountpayablereductive;
        @XmlElement(required = true)
        protected BigDecimal nettotal;
        @XmlElement(required = true)
        protected BigDecimal totalvatrate;
        @XmlElement(required = true)
        protected BigDecimal totalvatamount;
        @XmlElement(required = true)
        protected BigDecimal totalgrossamountpayable;
        @XmlElement(required = true)
        protected BigDecimal amountpayable;
        protected BigDecimal collectionagencyexpenses;
        protected Ttext text;

        /**
         * Ruft den Wert der accountinvoicenumber-Eigenschaft ab.
         * 
         */
        public long getAccountinvoicenumber() {
            return accountinvoicenumber;
        }

        /**
         * Legt den Wert der accountinvoicenumber-Eigenschaft fest.
         * 
         */
        public void setAccountinvoicenumber(long value) {
            this.accountinvoicenumber = value;
        }

        /**
         * Ruft den Wert der filenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFilenumber() {
            return filenumber;
        }

        /**
         * Legt den Wert der filenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFilenumber(String value) {
            this.filenumber = value;
        }

        /**
         * Ruft den Wert der currency-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getCurrency() {
            return currency;
        }

        /**
         * Legt den Wert der currency-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setCurrency(Tkey value) {
            this.currency = value;
        }

        /**
         * Ruft den Wert der interest-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getInterest() {
            return interest;
        }

        /**
         * Legt den Wert der interest-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setInterest(BigDecimal value) {
            this.interest = value;
        }

        /**
         * Ruft den Wert der dateinvoice-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateinvoice() {
            return dateinvoice;
        }

        /**
         * Legt den Wert der dateinvoice-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateinvoice(XMLGregorianCalendar value) {
            this.dateinvoice = value;
        }

        /**
         * Gets the value of the invoicelineitem property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the invoicelineitem property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInvoicelineitem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitem }
         * 
         * 
         */
        public List<Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitem> getInvoicelineitem() {
            if (invoicelineitem == null) {
                invoicelineitem = new ArrayList<Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitem>();
            }
            return this.invoicelineitem;
        }

        /**
         * Gets the value of the invoicelineitemamountpayablereductive property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the invoicelineitemamountpayablereductive property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getInvoicelineitemamountpayablereductive().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitemamountpayablereductive }
         * 
         * 
         */
        public List<Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitemamountpayablereductive> getInvoicelineitemamountpayablereductive() {
            if (invoicelineitemamountpayablereductive == null) {
                invoicelineitemamountpayablereductive = new ArrayList<Tcollectionaccountresponsebody.Accountinvoice.Invoicelineitemamountpayablereductive>();
            }
            return this.invoicelineitemamountpayablereductive;
        }

        /**
         * Ruft den Wert der nettotal-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getNettotal() {
            return nettotal;
        }

        /**
         * Legt den Wert der nettotal-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setNettotal(BigDecimal value) {
            this.nettotal = value;
        }

        /**
         * Ruft den Wert der totalvatrate-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTotalvatrate() {
            return totalvatrate;
        }

        /**
         * Legt den Wert der totalvatrate-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTotalvatrate(BigDecimal value) {
            this.totalvatrate = value;
        }

        /**
         * Ruft den Wert der totalvatamount-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTotalvatamount() {
            return totalvatamount;
        }

        /**
         * Legt den Wert der totalvatamount-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTotalvatamount(BigDecimal value) {
            this.totalvatamount = value;
        }

        /**
         * Ruft den Wert der totalgrossamountpayable-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getTotalgrossamountpayable() {
            return totalgrossamountpayable;
        }

        /**
         * Legt den Wert der totalgrossamountpayable-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setTotalgrossamountpayable(BigDecimal value) {
            this.totalgrossamountpayable = value;
        }

        /**
         * Ruft den Wert der amountpayable-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmountpayable() {
            return amountpayable;
        }

        /**
         * Legt den Wert der amountpayable-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmountpayable(BigDecimal value) {
            this.amountpayable = value;
        }

        /**
         * Ruft den Wert der collectionagencyexpenses-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getCollectionagencyexpenses() {
            return collectionagencyexpenses;
        }

        /**
         * Legt den Wert der collectionagencyexpenses-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setCollectionagencyexpenses(BigDecimal value) {
            this.collectionagencyexpenses = value;
        }

        /**
         * Ruft den Wert der text-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getText() {
            return text;
        }

        /**
         * Legt den Wert der text-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setText(Ttext value) {
            this.text = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="positionnumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="notation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="cancellation" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
         *         &lt;element name="totalamount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="vatrate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "positionnumber",
            "notation",
            "cancellation",
            "totalamount",
            "vatrate"
        })
        public static class Invoicelineitem {

            protected long positionnumber;
            @XmlElement(required = true)
            protected String notation;
            protected boolean cancellation;
            @XmlElement(required = true)
            protected BigDecimal totalamount;
            @XmlElement(required = true)
            protected BigDecimal vatrate;

            /**
             * Ruft den Wert der positionnumber-Eigenschaft ab.
             * 
             */
            public long getPositionnumber() {
                return positionnumber;
            }

            /**
             * Legt den Wert der positionnumber-Eigenschaft fest.
             * 
             */
            public void setPositionnumber(long value) {
                this.positionnumber = value;
            }

            /**
             * Ruft den Wert der notation-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNotation() {
                return notation;
            }

            /**
             * Legt den Wert der notation-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNotation(String value) {
                this.notation = value;
            }

            /**
             * Ruft den Wert der cancellation-Eigenschaft ab.
             * 
             */
            public boolean isCancellation() {
                return cancellation;
            }

            /**
             * Legt den Wert der cancellation-Eigenschaft fest.
             * 
             */
            public void setCancellation(boolean value) {
                this.cancellation = value;
            }

            /**
             * Ruft den Wert der totalamount-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTotalamount() {
                return totalamount;
            }

            /**
             * Legt den Wert der totalamount-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTotalamount(BigDecimal value) {
                this.totalamount = value;
            }

            /**
             * Ruft den Wert der vatrate-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getVatrate() {
                return vatrate;
            }

            /**
             * Legt den Wert der vatrate-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setVatrate(BigDecimal value) {
                this.vatrate = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="positionnumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
         *         &lt;element name="notation" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="totalamount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *         &lt;element name="vatrate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "positionnumber",
            "notation",
            "totalamount",
            "vatrate"
        })
        public static class Invoicelineitemamountpayablereductive {

            protected long positionnumber;
            @XmlElement(required = true)
            protected String notation;
            @XmlElement(required = true)
            protected BigDecimal totalamount;
            @XmlElement(required = true)
            protected BigDecimal vatrate;

            /**
             * Ruft den Wert der positionnumber-Eigenschaft ab.
             * 
             */
            public long getPositionnumber() {
                return positionnumber;
            }

            /**
             * Legt den Wert der positionnumber-Eigenschaft fest.
             * 
             */
            public void setPositionnumber(long value) {
                this.positionnumber = value;
            }

            /**
             * Ruft den Wert der notation-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNotation() {
                return notation;
            }

            /**
             * Legt den Wert der notation-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNotation(String value) {
                this.notation = value;
            }

            /**
             * Ruft den Wert der totalamount-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getTotalamount() {
                return totalamount;
            }

            /**
             * Legt den Wert der totalamount-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setTotalamount(BigDecimal value) {
                this.totalamount = value;
            }

            /**
             * Ruft den Wert der vatrate-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigDecimal }
             *     
             */
            public BigDecimal getVatrate() {
                return vatrate;
            }

            /**
             * Legt den Wert der vatrate-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigDecimal }
             *     
             */
            public void setVatrate(BigDecimal value) {
                this.vatrate = value;
            }

        }

    }

}
