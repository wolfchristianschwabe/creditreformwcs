
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Timportexport complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Timportexport">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="import" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timportexportdetails" minOccurs="0"/>
 *         &lt;element name="export" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timportexportdetails" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Timportexport", propOrder = {
    "_import",
    "export",
    "text"
})
public class Timportexport {

    @XmlElement(name = "import")
    protected Timportexportdetails _import;
    protected Timportexportdetails export;
    protected Ttext text;

    /**
     * Ruft den Wert der import-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Timportexportdetails }
     *     
     */
    public Timportexportdetails getImport() {
        return _import;
    }

    /**
     * Legt den Wert der import-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Timportexportdetails }
     *     
     */
    public void setImport(Timportexportdetails value) {
        this._import = value;
    }

    /**
     * Ruft den Wert der export-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Timportexportdetails }
     *     
     */
    public Timportexportdetails getExport() {
        return export;
    }

    /**
     * Legt den Wert der export-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Timportexportdetails }
     *     
     */
    public void setExport(Timportexportdetails value) {
        this.export = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

}
