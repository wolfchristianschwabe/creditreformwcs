
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tmailboxdirectoryresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tmailboxdirectoryresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="entry" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}mailboxentrynumber" minOccurs="0"/>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}referencenumber" minOccurs="0"/>
 *                   &lt;element name="deliverytype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="statusoforder" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="termofsettlement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *                   &lt;element name="ordertime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="creationtime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;element name="calltime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}responsemonitoringstructure"/>
 *                   &lt;choice minOccurs="0">
 *                     &lt;element name="actualaddresscompany" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="actualaddressprivate" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/choice>
 *                   &lt;choice minOccurs="0">
 *                     &lt;element name="orderaddresscompany" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                     &lt;element name="orderaddressprivate" minOccurs="0">
 *                       &lt;complexType>
 *                         &lt;complexContent>
 *                           &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                             &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
 *                           &lt;/restriction>
 *                         &lt;/complexContent>
 *                       &lt;/complexType>
 *                     &lt;/element>
 *                   &lt;/choice>
 *                   &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *                   &lt;sequence minOccurs="0">
 *                     &lt;element name="creditamount">
 *                       &lt;simpleType>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *                           &lt;minInclusive value="0"/>
 *                           &lt;maxInclusive value="9999999999999999"/>
 *                           &lt;fractionDigits value="2"/>
 *                         &lt;/restriction>
 *                       &lt;/simpleType>
 *                     &lt;/element>
 *                     &lt;element name="creditamountcurrency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *                   &lt;/sequence>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="morehits" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="nextpagereference" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tmailboxdirectoryresponsebody", propOrder = {
    "entry",
    "morehits",
    "nextpagereference"
})
public class Tmailboxdirectoryresponsebody {

    protected List<Tmailboxdirectoryresponsebody.Entry> entry;
    protected boolean morehits;
    protected Long nextpagereference;

    /**
     * Gets the value of the entry property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the entry property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEntry().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tmailboxdirectoryresponsebody.Entry }
     * 
     * 
     */
    public List<Tmailboxdirectoryresponsebody.Entry> getEntry() {
        if (entry == null) {
            entry = new ArrayList<Tmailboxdirectoryresponsebody.Entry>();
        }
        return this.entry;
    }

    /**
     * Ruft den Wert der morehits-Eigenschaft ab.
     * 
     */
    public boolean isMorehits() {
        return morehits;
    }

    /**
     * Legt den Wert der morehits-Eigenschaft fest.
     * 
     */
    public void setMorehits(boolean value) {
        this.morehits = value;
    }

    /**
     * Ruft den Wert der nextpagereference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getNextpagereference() {
        return nextpagereference;
    }

    /**
     * Legt den Wert der nextpagereference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setNextpagereference(Long value) {
        this.nextpagereference = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}mailboxentrynumber" minOccurs="0"/>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}referencenumber" minOccurs="0"/>
     *         &lt;element name="deliverytype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="statusoforder" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="termofsettlement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
     *         &lt;element name="ordertime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="creationtime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;element name="calltime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}responsemonitoringstructure"/>
     *         &lt;choice minOccurs="0">
     *           &lt;element name="actualaddresscompany" minOccurs="0">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="actualaddressprivate" minOccurs="0">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *         &lt;/choice>
     *         &lt;choice minOccurs="0">
     *           &lt;element name="orderaddresscompany" minOccurs="0">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *           &lt;element name="orderaddressprivate" minOccurs="0">
     *             &lt;complexType>
     *               &lt;complexContent>
     *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
     *                 &lt;/restriction>
     *               &lt;/complexContent>
     *             &lt;/complexType>
     *           &lt;/element>
     *         &lt;/choice>
     *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
     *         &lt;sequence minOccurs="0">
     *           &lt;element name="creditamount">
     *             &lt;simpleType>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
     *                 &lt;minInclusive value="0"/>
     *                 &lt;maxInclusive value="9999999999999999"/>
     *                 &lt;fractionDigits value="2"/>
     *               &lt;/restriction>
     *             &lt;/simpleType>
     *           &lt;/element>
     *           &lt;element name="creditamountcurrency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
     *         &lt;/sequence>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "mailboxentrynumber",
        "referencenumber",
        "deliverytype",
        "statusoforder",
        "producttype",
        "termofsettlement",
        "customerreference",
        "identificationnumber",
        "easynumber",
        "ordertime",
        "creationtime",
        "calltime",
        "endofstandardmonitoring",
        "extendedmonitoring",
        "extendedmonitoringplus",
        "actualaddresscompany",
        "actualaddressprivate",
        "orderaddresscompany",
        "orderaddressprivate",
        "legalform",
        "creditamount",
        "creditamountcurrency"
    })
    public static class Entry {

        protected String mailboxentrynumber;
        protected Long referencenumber;
        protected Tkey deliverytype;
        protected Tkey statusoforder;
        protected Tkey producttype;
        protected Tkey termofsettlement;
        protected String customerreference;
        protected String identificationnumber;
        protected String easynumber;
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar ordertime;
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar creationtime;
        @XmlSchemaType(name = "dateTime")
        protected XMLGregorianCalendar calltime;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar endofstandardmonitoring;
        protected Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring extendedmonitoring;
        protected Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus extendedmonitoringplus;
        protected Tmailboxdirectoryresponsebody.Entry.Actualaddresscompany actualaddresscompany;
        protected Tmailboxdirectoryresponsebody.Entry.Actualaddressprivate actualaddressprivate;
        protected Tmailboxdirectoryresponsebody.Entry.Orderaddresscompany orderaddresscompany;
        protected Tmailboxdirectoryresponsebody.Entry.Orderaddressprivate orderaddressprivate;
        protected Tkeywithshortdesignation legalform;
        protected BigDecimal creditamount;
        protected Tkey creditamountcurrency;

        /**
         * Ruft den Wert der mailboxentrynumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getMailboxentrynumber() {
            return mailboxentrynumber;
        }

        /**
         * Legt den Wert der mailboxentrynumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setMailboxentrynumber(String value) {
            this.mailboxentrynumber = value;
        }

        /**
         * Ruft den Wert der referencenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getReferencenumber() {
            return referencenumber;
        }

        /**
         * Legt den Wert der referencenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setReferencenumber(Long value) {
            this.referencenumber = value;
        }

        /**
         * Ruft den Wert der deliverytype-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getDeliverytype() {
            return deliverytype;
        }

        /**
         * Legt den Wert der deliverytype-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setDeliverytype(Tkey value) {
            this.deliverytype = value;
        }

        /**
         * Ruft den Wert der statusoforder-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getStatusoforder() {
            return statusoforder;
        }

        /**
         * Legt den Wert der statusoforder-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setStatusoforder(Tkey value) {
            this.statusoforder = value;
        }

        /**
         * Ruft den Wert der producttype-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getProducttype() {
            return producttype;
        }

        /**
         * Legt den Wert der producttype-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setProducttype(Tkey value) {
            this.producttype = value;
        }

        /**
         * Ruft den Wert der termofsettlement-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getTermofsettlement() {
            return termofsettlement;
        }

        /**
         * Legt den Wert der termofsettlement-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setTermofsettlement(Tkey value) {
            this.termofsettlement = value;
        }

        /**
         * Ruft den Wert der customerreference-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerreference() {
            return customerreference;
        }

        /**
         * Legt den Wert der customerreference-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerreference(String value) {
            this.customerreference = value;
        }

        /**
         * Ruft den Wert der identificationnumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdentificationnumber() {
            return identificationnumber;
        }

        /**
         * Legt den Wert der identificationnumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdentificationnumber(String value) {
            this.identificationnumber = value;
        }

        /**
         * Ruft den Wert der easynumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEasynumber() {
            return easynumber;
        }

        /**
         * Legt den Wert der easynumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEasynumber(String value) {
            this.easynumber = value;
        }

        /**
         * Ruft den Wert der ordertime-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getOrdertime() {
            return ordertime;
        }

        /**
         * Legt den Wert der ordertime-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setOrdertime(XMLGregorianCalendar value) {
            this.ordertime = value;
        }

        /**
         * Ruft den Wert der creationtime-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getCreationtime() {
            return creationtime;
        }

        /**
         * Legt den Wert der creationtime-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setCreationtime(XMLGregorianCalendar value) {
            this.creationtime = value;
        }

        /**
         * Ruft den Wert der calltime-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getCalltime() {
            return calltime;
        }

        /**
         * Legt den Wert der calltime-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setCalltime(XMLGregorianCalendar value) {
            this.calltime = value;
        }

        /**
         * Ruft den Wert der endofstandardmonitoring-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEndofstandardmonitoring() {
            return endofstandardmonitoring;
        }

        /**
         * Legt den Wert der endofstandardmonitoring-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEndofstandardmonitoring(XMLGregorianCalendar value) {
            this.endofstandardmonitoring = value;
        }

        /**
         * Ruft den Wert der extendedmonitoring-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring }
         *     
         */
        public Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring getExtendedmonitoring() {
            return extendedmonitoring;
        }

        /**
         * Legt den Wert der extendedmonitoring-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring }
         *     
         */
        public void setExtendedmonitoring(Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring value) {
            this.extendedmonitoring = value;
        }

        /**
         * Ruft den Wert der extendedmonitoringplus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus }
         *     
         */
        public Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus getExtendedmonitoringplus() {
            return extendedmonitoringplus;
        }

        /**
         * Legt den Wert der extendedmonitoringplus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus }
         *     
         */
        public void setExtendedmonitoringplus(Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus value) {
            this.extendedmonitoringplus = value;
        }

        /**
         * Ruft den Wert der actualaddresscompany-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Actualaddresscompany }
         *     
         */
        public Tmailboxdirectoryresponsebody.Entry.Actualaddresscompany getActualaddresscompany() {
            return actualaddresscompany;
        }

        /**
         * Legt den Wert der actualaddresscompany-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Actualaddresscompany }
         *     
         */
        public void setActualaddresscompany(Tmailboxdirectoryresponsebody.Entry.Actualaddresscompany value) {
            this.actualaddresscompany = value;
        }

        /**
         * Ruft den Wert der actualaddressprivate-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Actualaddressprivate }
         *     
         */
        public Tmailboxdirectoryresponsebody.Entry.Actualaddressprivate getActualaddressprivate() {
            return actualaddressprivate;
        }

        /**
         * Legt den Wert der actualaddressprivate-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Actualaddressprivate }
         *     
         */
        public void setActualaddressprivate(Tmailboxdirectoryresponsebody.Entry.Actualaddressprivate value) {
            this.actualaddressprivate = value;
        }

        /**
         * Ruft den Wert der orderaddresscompany-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Orderaddresscompany }
         *     
         */
        public Tmailboxdirectoryresponsebody.Entry.Orderaddresscompany getOrderaddresscompany() {
            return orderaddresscompany;
        }

        /**
         * Legt den Wert der orderaddresscompany-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Orderaddresscompany }
         *     
         */
        public void setOrderaddresscompany(Tmailboxdirectoryresponsebody.Entry.Orderaddresscompany value) {
            this.orderaddresscompany = value;
        }

        /**
         * Ruft den Wert der orderaddressprivate-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Orderaddressprivate }
         *     
         */
        public Tmailboxdirectoryresponsebody.Entry.Orderaddressprivate getOrderaddressprivate() {
            return orderaddressprivate;
        }

        /**
         * Legt den Wert der orderaddressprivate-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tmailboxdirectoryresponsebody.Entry.Orderaddressprivate }
         *     
         */
        public void setOrderaddressprivate(Tmailboxdirectoryresponsebody.Entry.Orderaddressprivate value) {
            this.orderaddressprivate = value;
        }

        /**
         * Ruft den Wert der legalform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public Tkeywithshortdesignation getLegalform() {
            return legalform;
        }

        /**
         * Legt den Wert der legalform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public void setLegalform(Tkeywithshortdesignation value) {
            this.legalform = value;
        }

        /**
         * Ruft den Wert der creditamount-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getCreditamount() {
            return creditamount;
        }

        /**
         * Legt den Wert der creditamount-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setCreditamount(BigDecimal value) {
            this.creditamount = value;
        }

        /**
         * Ruft den Wert der creditamountcurrency-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getCreditamountcurrency() {
            return creditamountcurrency;
        }

        /**
         * Legt den Wert der creditamountcurrency-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setCreditamountcurrency(Tkey value) {
            this.creditamountcurrency = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "salutation",
            "companyname",
            "tradename",
            "commercialname",
            "alias",
            "street",
            "housenumber",
            "housenumberaffix",
            "postcode",
            "city",
            "quarter",
            "region",
            "country"
        })
        public static class Actualaddresscompany {

            protected Tkey salutation;
            protected String companyname;
            protected String tradename;
            protected String commercialname;
            protected String alias;
            protected String street;
            protected Integer housenumber;
            protected String housenumberaffix;
            protected String postcode;
            protected String city;
            protected String quarter;
            protected String region;
            protected Tkey country;

            /**
             * Ruft den Wert der salutation-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getSalutation() {
                return salutation;
            }

            /**
             * Legt den Wert der salutation-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setSalutation(Tkey value) {
                this.salutation = value;
            }

            /**
             * Ruft den Wert der companyname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCompanyname() {
                return companyname;
            }

            /**
             * Legt den Wert der companyname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCompanyname(String value) {
                this.companyname = value;
            }

            /**
             * Ruft den Wert der tradename-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTradename() {
                return tradename;
            }

            /**
             * Legt den Wert der tradename-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTradename(String value) {
                this.tradename = value;
            }

            /**
             * Ruft den Wert der commercialname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCommercialname() {
                return commercialname;
            }

            /**
             * Legt den Wert der commercialname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCommercialname(String value) {
                this.commercialname = value;
            }

            /**
             * Ruft den Wert der alias-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAlias() {
                return alias;
            }

            /**
             * Legt den Wert der alias-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAlias(String value) {
                this.alias = value;
            }

            /**
             * Ruft den Wert der street-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStreet() {
                return street;
            }

            /**
             * Legt den Wert der street-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStreet(String value) {
                this.street = value;
            }

            /**
             * Ruft den Wert der housenumber-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getHousenumber() {
                return housenumber;
            }

            /**
             * Legt den Wert der housenumber-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setHousenumber(Integer value) {
                this.housenumber = value;
            }

            /**
             * Ruft den Wert der housenumberaffix-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHousenumberaffix() {
                return housenumberaffix;
            }

            /**
             * Legt den Wert der housenumberaffix-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHousenumberaffix(String value) {
                this.housenumberaffix = value;
            }

            /**
             * Ruft den Wert der postcode-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostcode() {
                return postcode;
            }

            /**
             * Legt den Wert der postcode-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostcode(String value) {
                this.postcode = value;
            }

            /**
             * Ruft den Wert der city-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Legt den Wert der city-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Ruft den Wert der quarter-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuarter() {
                return quarter;
            }

            /**
             * Legt den Wert der quarter-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuarter(String value) {
                this.quarter = value;
            }

            /**
             * Ruft den Wert der region-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegion() {
                return region;
            }

            /**
             * Legt den Wert der region-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegion(String value) {
                this.region = value;
            }

            /**
             * Ruft den Wert der country-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getCountry() {
                return country;
            }

            /**
             * Legt den Wert der country-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setCountry(Tkey value) {
                this.country = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "title",
            "salutation",
            "surname",
            "firstname",
            "birthname",
            "nameaffix",
            "surnamewidow",
            "surnamebeforedivorce",
            "alias",
            "street",
            "housenumber",
            "housenumberaffix",
            "postcode",
            "city",
            "quarter",
            "region",
            "country"
        })
        public static class Actualaddressprivate {

            protected Tkeywithshortdesignation title;
            protected Tkey salutation;
            protected String surname;
            protected List<String> firstname;
            protected String birthname;
            protected String nameaffix;
            protected String surnamewidow;
            protected String surnamebeforedivorce;
            protected String alias;
            protected String street;
            protected Integer housenumber;
            protected String housenumberaffix;
            protected String postcode;
            protected String city;
            protected String quarter;
            protected String region;
            protected Tkey country;

            /**
             * Ruft den Wert der title-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public Tkeywithshortdesignation getTitle() {
                return title;
            }

            /**
             * Legt den Wert der title-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public void setTitle(Tkeywithshortdesignation value) {
                this.title = value;
            }

            /**
             * Ruft den Wert der salutation-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getSalutation() {
                return salutation;
            }

            /**
             * Legt den Wert der salutation-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setSalutation(Tkey value) {
                this.salutation = value;
            }

            /**
             * Ruft den Wert der surname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSurname() {
                return surname;
            }

            /**
             * Legt den Wert der surname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSurname(String value) {
                this.surname = value;
            }

            /**
             * Gets the value of the firstname property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the firstname property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFirstname().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getFirstname() {
                if (firstname == null) {
                    firstname = new ArrayList<String>();
                }
                return this.firstname;
            }

            /**
             * Ruft den Wert der birthname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBirthname() {
                return birthname;
            }

            /**
             * Legt den Wert der birthname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBirthname(String value) {
                this.birthname = value;
            }

            /**
             * Ruft den Wert der nameaffix-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNameaffix() {
                return nameaffix;
            }

            /**
             * Legt den Wert der nameaffix-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNameaffix(String value) {
                this.nameaffix = value;
            }

            /**
             * Ruft den Wert der surnamewidow-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSurnamewidow() {
                return surnamewidow;
            }

            /**
             * Legt den Wert der surnamewidow-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSurnamewidow(String value) {
                this.surnamewidow = value;
            }

            /**
             * Ruft den Wert der surnamebeforedivorce-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSurnamebeforedivorce() {
                return surnamebeforedivorce;
            }

            /**
             * Legt den Wert der surnamebeforedivorce-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSurnamebeforedivorce(String value) {
                this.surnamebeforedivorce = value;
            }

            /**
             * Ruft den Wert der alias-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAlias() {
                return alias;
            }

            /**
             * Legt den Wert der alias-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAlias(String value) {
                this.alias = value;
            }

            /**
             * Ruft den Wert der street-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStreet() {
                return street;
            }

            /**
             * Legt den Wert der street-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStreet(String value) {
                this.street = value;
            }

            /**
             * Ruft den Wert der housenumber-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getHousenumber() {
                return housenumber;
            }

            /**
             * Legt den Wert der housenumber-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setHousenumber(Integer value) {
                this.housenumber = value;
            }

            /**
             * Ruft den Wert der housenumberaffix-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHousenumberaffix() {
                return housenumberaffix;
            }

            /**
             * Legt den Wert der housenumberaffix-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHousenumberaffix(String value) {
                this.housenumberaffix = value;
            }

            /**
             * Ruft den Wert der postcode-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostcode() {
                return postcode;
            }

            /**
             * Legt den Wert der postcode-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostcode(String value) {
                this.postcode = value;
            }

            /**
             * Ruft den Wert der city-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Legt den Wert der city-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Ruft den Wert der quarter-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuarter() {
                return quarter;
            }

            /**
             * Legt den Wert der quarter-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuarter(String value) {
                this.quarter = value;
            }

            /**
             * Ruft den Wert der region-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegion() {
                return region;
            }

            /**
             * Legt den Wert der region-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegion(String value) {
                this.region = value;
            }

            /**
             * Ruft den Wert der country-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getCountry() {
                return country;
            }

            /**
             * Legt den Wert der country-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setCountry(Tkey value) {
                this.country = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="endofextendedmonitoring" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "endofextendedmonitoring"
        })
        public static class Extendedmonitoring {

            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar endofextendedmonitoring;

            /**
             * Ruft den Wert der endofextendedmonitoring-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getEndofextendedmonitoring() {
                return endofextendedmonitoring;
            }

            /**
             * Legt den Wert der endofextendedmonitoring-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setEndofextendedmonitoring(XMLGregorianCalendar value) {
                this.endofextendedmonitoring = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="startofextendedmonitoringplus" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="endofextendedmonitoringplus" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "startofextendedmonitoringplus",
            "endofextendedmonitoringplus"
        })
        public static class Extendedmonitoringplus {

            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar startofextendedmonitoringplus;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar endofextendedmonitoringplus;

            /**
             * Ruft den Wert der startofextendedmonitoringplus-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getStartofextendedmonitoringplus() {
                return startofextendedmonitoringplus;
            }

            /**
             * Legt den Wert der startofextendedmonitoringplus-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setStartofextendedmonitoringplus(XMLGregorianCalendar value) {
                this.startofextendedmonitoringplus = value;
            }

            /**
             * Ruft den Wert der endofextendedmonitoringplus-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getEndofextendedmonitoringplus() {
                return endofextendedmonitoringplus;
            }

            /**
             * Legt den Wert der endofextendedmonitoringplus-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setEndofextendedmonitoringplus(XMLGregorianCalendar value) {
                this.endofextendedmonitoringplus = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "salutation",
            "companyname",
            "tradename",
            "commercialname",
            "alias",
            "street",
            "housenumber",
            "housenumberaffix",
            "postcode",
            "city",
            "quarter",
            "region",
            "country"
        })
        public static class Orderaddresscompany {

            protected Tkey salutation;
            protected String companyname;
            protected String tradename;
            protected String commercialname;
            protected String alias;
            protected String street;
            protected Integer housenumber;
            protected String housenumberaffix;
            protected String postcode;
            protected String city;
            protected String quarter;
            protected String region;
            protected Tkey country;

            /**
             * Ruft den Wert der salutation-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getSalutation() {
                return salutation;
            }

            /**
             * Legt den Wert der salutation-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setSalutation(Tkey value) {
                this.salutation = value;
            }

            /**
             * Ruft den Wert der companyname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCompanyname() {
                return companyname;
            }

            /**
             * Legt den Wert der companyname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCompanyname(String value) {
                this.companyname = value;
            }

            /**
             * Ruft den Wert der tradename-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTradename() {
                return tradename;
            }

            /**
             * Legt den Wert der tradename-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTradename(String value) {
                this.tradename = value;
            }

            /**
             * Ruft den Wert der commercialname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCommercialname() {
                return commercialname;
            }

            /**
             * Legt den Wert der commercialname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCommercialname(String value) {
                this.commercialname = value;
            }

            /**
             * Ruft den Wert der alias-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAlias() {
                return alias;
            }

            /**
             * Legt den Wert der alias-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAlias(String value) {
                this.alias = value;
            }

            /**
             * Ruft den Wert der street-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStreet() {
                return street;
            }

            /**
             * Legt den Wert der street-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStreet(String value) {
                this.street = value;
            }

            /**
             * Ruft den Wert der housenumber-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getHousenumber() {
                return housenumber;
            }

            /**
             * Legt den Wert der housenumber-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setHousenumber(Integer value) {
                this.housenumber = value;
            }

            /**
             * Ruft den Wert der housenumberaffix-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHousenumberaffix() {
                return housenumberaffix;
            }

            /**
             * Legt den Wert der housenumberaffix-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHousenumberaffix(String value) {
                this.housenumberaffix = value;
            }

            /**
             * Ruft den Wert der postcode-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostcode() {
                return postcode;
            }

            /**
             * Legt den Wert der postcode-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostcode(String value) {
                this.postcode = value;
            }

            /**
             * Ruft den Wert der city-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Legt den Wert der city-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Ruft den Wert der quarter-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuarter() {
                return quarter;
            }

            /**
             * Legt den Wert der quarter-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuarter(String value) {
                this.quarter = value;
            }

            /**
             * Ruft den Wert der region-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegion() {
                return region;
            }

            /**
             * Legt den Wert der region-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegion(String value) {
                this.region = value;
            }

            /**
             * Ruft den Wert der country-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getCountry() {
                return country;
            }

            /**
             * Legt den Wert der country-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setCountry(Tkey value) {
                this.country = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "title",
            "salutation",
            "surname",
            "firstname",
            "birthname",
            "nameaffix",
            "surnamewidow",
            "surnamebeforedivorce",
            "alias",
            "street",
            "housenumber",
            "housenumberaffix",
            "postcode",
            "city",
            "quarter",
            "region",
            "country"
        })
        public static class Orderaddressprivate {

            protected Tkeywithshortdesignation title;
            protected Tkey salutation;
            protected String surname;
            protected List<String> firstname;
            protected String birthname;
            protected String nameaffix;
            protected String surnamewidow;
            protected String surnamebeforedivorce;
            protected String alias;
            protected String street;
            protected Integer housenumber;
            protected String housenumberaffix;
            protected String postcode;
            protected String city;
            protected String quarter;
            protected String region;
            protected Tkey country;

            /**
             * Ruft den Wert der title-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public Tkeywithshortdesignation getTitle() {
                return title;
            }

            /**
             * Legt den Wert der title-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public void setTitle(Tkeywithshortdesignation value) {
                this.title = value;
            }

            /**
             * Ruft den Wert der salutation-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getSalutation() {
                return salutation;
            }

            /**
             * Legt den Wert der salutation-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setSalutation(Tkey value) {
                this.salutation = value;
            }

            /**
             * Ruft den Wert der surname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSurname() {
                return surname;
            }

            /**
             * Legt den Wert der surname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSurname(String value) {
                this.surname = value;
            }

            /**
             * Gets the value of the firstname property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the firstname property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFirstname().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getFirstname() {
                if (firstname == null) {
                    firstname = new ArrayList<String>();
                }
                return this.firstname;
            }

            /**
             * Ruft den Wert der birthname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBirthname() {
                return birthname;
            }

            /**
             * Legt den Wert der birthname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBirthname(String value) {
                this.birthname = value;
            }

            /**
             * Ruft den Wert der nameaffix-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNameaffix() {
                return nameaffix;
            }

            /**
             * Legt den Wert der nameaffix-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNameaffix(String value) {
                this.nameaffix = value;
            }

            /**
             * Ruft den Wert der surnamewidow-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSurnamewidow() {
                return surnamewidow;
            }

            /**
             * Legt den Wert der surnamewidow-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSurnamewidow(String value) {
                this.surnamewidow = value;
            }

            /**
             * Ruft den Wert der surnamebeforedivorce-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSurnamebeforedivorce() {
                return surnamebeforedivorce;
            }

            /**
             * Legt den Wert der surnamebeforedivorce-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSurnamebeforedivorce(String value) {
                this.surnamebeforedivorce = value;
            }

            /**
             * Ruft den Wert der alias-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAlias() {
                return alias;
            }

            /**
             * Legt den Wert der alias-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAlias(String value) {
                this.alias = value;
            }

            /**
             * Ruft den Wert der street-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStreet() {
                return street;
            }

            /**
             * Legt den Wert der street-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStreet(String value) {
                this.street = value;
            }

            /**
             * Ruft den Wert der housenumber-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getHousenumber() {
                return housenumber;
            }

            /**
             * Legt den Wert der housenumber-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setHousenumber(Integer value) {
                this.housenumber = value;
            }

            /**
             * Ruft den Wert der housenumberaffix-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHousenumberaffix() {
                return housenumberaffix;
            }

            /**
             * Legt den Wert der housenumberaffix-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHousenumberaffix(String value) {
                this.housenumberaffix = value;
            }

            /**
             * Ruft den Wert der postcode-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostcode() {
                return postcode;
            }

            /**
             * Legt den Wert der postcode-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostcode(String value) {
                this.postcode = value;
            }

            /**
             * Ruft den Wert der city-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Legt den Wert der city-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Ruft den Wert der quarter-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuarter() {
                return quarter;
            }

            /**
             * Legt den Wert der quarter-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuarter(String value) {
                this.quarter = value;
            }

            /**
             * Ruft den Wert der region-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegion() {
                return region;
            }

            /**
             * Legt den Wert der region-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegion(String value) {
                this.region = value;
            }

            /**
             * Ruft den Wert der country-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getCountry() {
                return country;
            }

            /**
             * Legt den Wert der country-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setCountry(Tkey value) {
                this.country = value;
            }

        }

    }

}
