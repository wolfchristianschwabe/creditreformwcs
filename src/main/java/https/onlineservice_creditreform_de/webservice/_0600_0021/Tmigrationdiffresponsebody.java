
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tmigrationdiffresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tmigrationdiffresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="assignment" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="oldreferencenumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Treferencenumber"/>
 *                   &lt;element name="oldidentificationnumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tidentificationnumber"/>
 *                   &lt;element name="newreferencenumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Treferencenumber" minOccurs="0"/>
 *                   &lt;element name="newidentificationnumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tidentificationnumber"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tmigrationdiffresponsebody", propOrder = {
    "assignment"
})
public class Tmigrationdiffresponsebody {

    protected List<Tmigrationdiffresponsebody.Assignment> assignment;

    /**
     * Gets the value of the assignment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assignment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssignment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tmigrationdiffresponsebody.Assignment }
     * 
     * 
     */
    public List<Tmigrationdiffresponsebody.Assignment> getAssignment() {
        if (assignment == null) {
            assignment = new ArrayList<Tmigrationdiffresponsebody.Assignment>();
        }
        return this.assignment;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="oldreferencenumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Treferencenumber"/>
     *         &lt;element name="oldidentificationnumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tidentificationnumber"/>
     *         &lt;element name="newreferencenumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Treferencenumber" minOccurs="0"/>
     *         &lt;element name="newidentificationnumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tidentificationnumber"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "oldreferencenumber",
        "oldidentificationnumber",
        "newreferencenumber",
        "newidentificationnumber"
    })
    public static class Assignment {

        protected long oldreferencenumber;
        @XmlElement(required = true)
        protected String oldidentificationnumber;
        protected Long newreferencenumber;
        @XmlElement(required = true)
        protected String newidentificationnumber;

        /**
         * Ruft den Wert der oldreferencenumber-Eigenschaft ab.
         * 
         */
        public long getOldreferencenumber() {
            return oldreferencenumber;
        }

        /**
         * Legt den Wert der oldreferencenumber-Eigenschaft fest.
         * 
         */
        public void setOldreferencenumber(long value) {
            this.oldreferencenumber = value;
        }

        /**
         * Ruft den Wert der oldidentificationnumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOldidentificationnumber() {
            return oldidentificationnumber;
        }

        /**
         * Legt den Wert der oldidentificationnumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOldidentificationnumber(String value) {
            this.oldidentificationnumber = value;
        }

        /**
         * Ruft den Wert der newreferencenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Long }
         *     
         */
        public Long getNewreferencenumber() {
            return newreferencenumber;
        }

        /**
         * Legt den Wert der newreferencenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Long }
         *     
         */
        public void setNewreferencenumber(Long value) {
            this.newreferencenumber = value;
        }

        /**
         * Ruft den Wert der newidentificationnumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNewidentificationnumber() {
            return newidentificationnumber;
        }

        /**
         * Legt den Wert der newidentificationnumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNewidentificationnumber(String value) {
            this.newidentificationnumber = value;
        }

    }

}
