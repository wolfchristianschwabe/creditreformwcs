
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipantextended complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipantextended">
 *   &lt;complexContent>
 *     &lt;extension base="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipant">
 *       &lt;sequence>
 *         &lt;element name="complementaryparticipantcapacities" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tdeputymanagementbasic" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipantextended", propOrder = {
    "complementaryparticipantcapacities"
})
public class Tparticipantextended
    extends Tparticipant
{

    protected Tdeputymanagementbasic complementaryparticipantcapacities;

    /**
     * Ruft den Wert der complementaryparticipantcapacities-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tdeputymanagementbasic }
     *     
     */
    public Tdeputymanagementbasic getComplementaryparticipantcapacities() {
        return complementaryparticipantcapacities;
    }

    /**
     * Legt den Wert der complementaryparticipantcapacities-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tdeputymanagementbasic }
     *     
     */
    public void setComplementaryparticipantcapacities(Tdeputymanagementbasic value) {
        this.complementaryparticipantcapacities = value;
    }

}
