
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipations complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipations">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}status" minOccurs="0"/>
 *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *         &lt;element name="participatingsince" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;element name="participatingtill" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;element name="capitalshare" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamountpercent" minOccurs="0"/>
 *         &lt;element name="totalcapital" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *         &lt;element name="warnings" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Twarning" minOccurs="0"/>
 *         &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipations", propOrder = {
    "identificationnumber",
    "easynumber",
    "salutation",
    "companyname",
    "tradename",
    "commercialname",
    "alias",
    "street",
    "housenumber",
    "housenumberaffix",
    "postcode",
    "city",
    "quarter",
    "region",
    "country",
    "status",
    "legalform",
    "participatingsince",
    "participatingtill",
    "capitalshare",
    "totalcapital",
    "warnings",
    "trafficlight"
})
public class Tparticipations {

    protected String identificationnumber;
    protected String easynumber;
    protected Tkey salutation;
    protected String companyname;
    protected String tradename;
    protected String commercialname;
    protected String alias;
    protected String street;
    protected Integer housenumber;
    protected String housenumberaffix;
    protected String postcode;
    protected String city;
    protected String quarter;
    protected String region;
    protected Tkey country;
    protected Tkeywithdescription status;
    protected Tkeywithshortdesignation legalform;
    protected String participatingsince;
    protected String participatingtill;
    protected Tamountpercent capitalshare;
    protected Tamount totalcapital;
    protected Twarning warnings;
    protected Tkeywithgrade trafficlight;

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der easynumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEasynumber() {
        return easynumber;
    }

    /**
     * Legt den Wert der easynumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEasynumber(String value) {
        this.easynumber = value;
    }

    /**
     * Ruft den Wert der salutation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getSalutation() {
        return salutation;
    }

    /**
     * Legt den Wert der salutation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setSalutation(Tkey value) {
        this.salutation = value;
    }

    /**
     * Ruft den Wert der companyname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyname() {
        return companyname;
    }

    /**
     * Legt den Wert der companyname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyname(String value) {
        this.companyname = value;
    }

    /**
     * Ruft den Wert der tradename-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTradename() {
        return tradename;
    }

    /**
     * Legt den Wert der tradename-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTradename(String value) {
        this.tradename = value;
    }

    /**
     * Ruft den Wert der commercialname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommercialname() {
        return commercialname;
    }

    /**
     * Legt den Wert der commercialname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommercialname(String value) {
        this.commercialname = value;
    }

    /**
     * Ruft den Wert der alias-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Legt den Wert der alias-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAlias(String value) {
        this.alias = value;
    }

    /**
     * Ruft den Wert der street-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Legt den Wert der street-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Ruft den Wert der housenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHousenumber() {
        return housenumber;
    }

    /**
     * Legt den Wert der housenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHousenumber(Integer value) {
        this.housenumber = value;
    }

    /**
     * Ruft den Wert der housenumberaffix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHousenumberaffix() {
        return housenumberaffix;
    }

    /**
     * Legt den Wert der housenumberaffix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHousenumberaffix(String value) {
        this.housenumberaffix = value;
    }

    /**
     * Ruft den Wert der postcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Legt den Wert der postcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostcode(String value) {
        this.postcode = value;
    }

    /**
     * Ruft den Wert der city-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Legt den Wert der city-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Ruft den Wert der quarter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuarter() {
        return quarter;
    }

    /**
     * Legt den Wert der quarter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuarter(String value) {
        this.quarter = value;
    }

    /**
     * Ruft den Wert der region-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Legt den Wert der region-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Ruft den Wert der country-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCountry() {
        return country;
    }

    /**
     * Legt den Wert der country-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCountry(Tkey value) {
        this.country = value;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithdescription }
     *     
     */
    public Tkeywithdescription getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithdescription }
     *     
     */
    public void setStatus(Tkeywithdescription value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der legalform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithshortdesignation }
     *     
     */
    public Tkeywithshortdesignation getLegalform() {
        return legalform;
    }

    /**
     * Legt den Wert der legalform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithshortdesignation }
     *     
     */
    public void setLegalform(Tkeywithshortdesignation value) {
        this.legalform = value;
    }

    /**
     * Ruft den Wert der participatingsince-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipatingsince() {
        return participatingsince;
    }

    /**
     * Legt den Wert der participatingsince-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipatingsince(String value) {
        this.participatingsince = value;
    }

    /**
     * Ruft den Wert der participatingtill-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipatingtill() {
        return participatingtill;
    }

    /**
     * Legt den Wert der participatingtill-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipatingtill(String value) {
        this.participatingtill = value;
    }

    /**
     * Ruft den Wert der capitalshare-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamountpercent }
     *     
     */
    public Tamountpercent getCapitalshare() {
        return capitalshare;
    }

    /**
     * Legt den Wert der capitalshare-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamountpercent }
     *     
     */
    public void setCapitalshare(Tamountpercent value) {
        this.capitalshare = value;
    }

    /**
     * Ruft den Wert der totalcapital-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamount }
     *     
     */
    public Tamount getTotalcapital() {
        return totalcapital;
    }

    /**
     * Legt den Wert der totalcapital-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamount }
     *     
     */
    public void setTotalcapital(Tamount value) {
        this.totalcapital = value;
    }

    /**
     * Ruft den Wert der warnings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Twarning }
     *     
     */
    public Twarning getWarnings() {
        return warnings;
    }

    /**
     * Legt den Wert der warnings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Twarning }
     *     
     */
    public void setWarnings(Twarning value) {
        this.warnings = value;
    }

    /**
     * Ruft den Wert der trafficlight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getTrafficlight() {
        return trafficlight;
    }

    /**
     * Legt den Wert der trafficlight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setTrafficlight(Tkeywithgrade value) {
        this.trafficlight = value;
    }

}
