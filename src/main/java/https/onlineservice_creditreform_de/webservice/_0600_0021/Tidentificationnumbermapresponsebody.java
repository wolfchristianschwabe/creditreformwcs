
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tidentificationnumbermapresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tidentificationnumbermapresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="assignment" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="oldidentificationnumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="newidentificationnumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tidentificationnumbermapresponsebody", propOrder = {
    "assignment"
})
public class Tidentificationnumbermapresponsebody {

    protected List<Tidentificationnumbermapresponsebody.Assignment> assignment;

    /**
     * Gets the value of the assignment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the assignment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAssignment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tidentificationnumbermapresponsebody.Assignment }
     * 
     * 
     */
    public List<Tidentificationnumbermapresponsebody.Assignment> getAssignment() {
        if (assignment == null) {
            assignment = new ArrayList<Tidentificationnumbermapresponsebody.Assignment>();
        }
        return this.assignment;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="oldidentificationnumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="newidentificationnumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "oldidentificationnumber",
        "newidentificationnumber"
    })
    public static class Assignment {

        @XmlElement(required = true)
        protected String oldidentificationnumber;
        @XmlElement(required = true)
        protected String newidentificationnumber;

        /**
         * Ruft den Wert der oldidentificationnumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getOldidentificationnumber() {
            return oldidentificationnumber;
        }

        /**
         * Legt den Wert der oldidentificationnumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setOldidentificationnumber(String value) {
            this.oldidentificationnumber = value;
        }

        /**
         * Ruft den Wert der newidentificationnumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNewidentificationnumber() {
            return newidentificationnumber;
        }

        /**
         * Legt den Wert der newidentificationnumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNewidentificationnumber(String value) {
            this.newidentificationnumber = value;
        }

    }

}
