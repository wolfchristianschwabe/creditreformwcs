
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Timportexportdetails complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Timportexportdetails">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rate" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;maxInclusive value="999"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="decoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="countries" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Timportexportdetails", propOrder = {
    "rate",
    "decoration",
    "countries"
})
public class Timportexportdetails {

    protected Integer rate;
    protected Tkey decoration;
    protected String countries;

    /**
     * Ruft den Wert der rate-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRate() {
        return rate;
    }

    /**
     * Legt den Wert der rate-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRate(Integer value) {
        this.rate = value;
    }

    /**
     * Ruft den Wert der decoration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getDecoration() {
        return decoration;
    }

    /**
     * Legt den Wert der decoration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setDecoration(Tkey value) {
        this.decoration = value;
    }

    /**
     * Ruft den Wert der countries-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountries() {
        return countries;
    }

    /**
     * Legt den Wert der countries-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountries(String value) {
        this.countries = value;
    }

}
