
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcggeodata complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcggeodata">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="buyingpowerindex" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}decimal">
 *               &lt;fractionDigits value="2"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="streettype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="buildingstructure" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="agestructureunderthirty" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="agestructureoversixty" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="familystructure" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcggeodata", propOrder = {
    "buyingpowerindex",
    "streettype",
    "buildingstructure",
    "agestructureunderthirty",
    "agestructureoversixty",
    "familystructure"
})
public class Tcggeodata {

    protected BigDecimal buyingpowerindex;
    protected Tkey streettype;
    protected Tkey buildingstructure;
    protected Tkey agestructureunderthirty;
    protected Tkey agestructureoversixty;
    protected Tkey familystructure;

    /**
     * Ruft den Wert der buyingpowerindex-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getBuyingpowerindex() {
        return buyingpowerindex;
    }

    /**
     * Legt den Wert der buyingpowerindex-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setBuyingpowerindex(BigDecimal value) {
        this.buyingpowerindex = value;
    }

    /**
     * Ruft den Wert der streettype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getStreettype() {
        return streettype;
    }

    /**
     * Legt den Wert der streettype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setStreettype(Tkey value) {
        this.streettype = value;
    }

    /**
     * Ruft den Wert der buildingstructure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getBuildingstructure() {
        return buildingstructure;
    }

    /**
     * Legt den Wert der buildingstructure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setBuildingstructure(Tkey value) {
        this.buildingstructure = value;
    }

    /**
     * Ruft den Wert der agestructureunderthirty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getAgestructureunderthirty() {
        return agestructureunderthirty;
    }

    /**
     * Legt den Wert der agestructureunderthirty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setAgestructureunderthirty(Tkey value) {
        this.agestructureunderthirty = value;
    }

    /**
     * Ruft den Wert der agestructureoversixty-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getAgestructureoversixty() {
        return agestructureoversixty;
    }

    /**
     * Legt den Wert der agestructureoversixty-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setAgestructureoversixty(Tkey value) {
        this.agestructureoversixty = value;
    }

    /**
     * Ruft den Wert der familystructure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getFamilystructure() {
        return familystructure;
    }

    /**
     * Legt den Wert der familystructure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setFamilystructure(Tkey value) {
        this.familystructure = value;
    }

}
