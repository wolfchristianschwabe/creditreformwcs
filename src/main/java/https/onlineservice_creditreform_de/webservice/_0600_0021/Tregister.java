
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tregister complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tregister">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="chapterheading" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="datelegalform" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="datelastregisterentry" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="registertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *         &lt;element name="registerid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="register" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcourt" minOccurs="0"/>
 *         &lt;element name="reasonofregister" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tregister", propOrder = {
    "chapterheading",
    "text",
    "datelegalform",
    "datelastregisterentry",
    "registertype",
    "registerid",
    "register",
    "reasonofregister"
})
public class Tregister {

    protected String chapterheading;
    protected Ttext text;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datelegalform;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datelastregisterentry;
    protected Tkeywithshortdesignation registertype;
    protected String registerid;
    protected Tcourt register;
    protected Tkey reasonofregister;

    /**
     * Ruft den Wert der chapterheading-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChapterheading() {
        return chapterheading;
    }

    /**
     * Legt den Wert der chapterheading-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChapterheading(String value) {
        this.chapterheading = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

    /**
     * Ruft den Wert der datelegalform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatelegalform() {
        return datelegalform;
    }

    /**
     * Legt den Wert der datelegalform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatelegalform(XMLGregorianCalendar value) {
        this.datelegalform = value;
    }

    /**
     * Ruft den Wert der datelastregisterentry-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatelastregisterentry() {
        return datelastregisterentry;
    }

    /**
     * Legt den Wert der datelastregisterentry-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatelastregisterentry(XMLGregorianCalendar value) {
        this.datelastregisterentry = value;
    }

    /**
     * Ruft den Wert der registertype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithshortdesignation }
     *     
     */
    public Tkeywithshortdesignation getRegistertype() {
        return registertype;
    }

    /**
     * Legt den Wert der registertype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithshortdesignation }
     *     
     */
    public void setRegistertype(Tkeywithshortdesignation value) {
        this.registertype = value;
    }

    /**
     * Ruft den Wert der registerid-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegisterid() {
        return registerid;
    }

    /**
     * Legt den Wert der registerid-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegisterid(String value) {
        this.registerid = value;
    }

    /**
     * Ruft den Wert der register-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcourt }
     *     
     */
    public Tcourt getRegister() {
        return register;
    }

    /**
     * Legt den Wert der register-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcourt }
     *     
     */
    public void setRegister(Tcourt value) {
        this.register = value;
    }

    /**
     * Ruft den Wert der reasonofregister-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getReasonofregister() {
        return reasonofregister;
    }

    /**
     * Legt den Wert der reasonofregister-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setReasonofregister(Tkey value) {
        this.reasonofregister = value;
    }

}
