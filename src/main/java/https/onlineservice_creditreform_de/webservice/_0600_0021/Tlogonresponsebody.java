
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tlogonresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tlogonresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="interfaceinformation" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="expiry" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="currentinterfaceversion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="upgraderecommended" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="currentkeylistversion" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger"/>
 *         &lt;element name="unreadmail" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="emailaddress" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
 *         &lt;element name="service" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tservice" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="reportavailabilityinsearchresponse" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="useraccountinformation" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="subuser" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" minOccurs="0"/>
 *                             &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tlogonresponsebody", propOrder = {
    "interfaceinformation",
    "currentkeylistversion",
    "unreadmail",
    "emailaddress",
    "service",
    "reportavailabilityinsearchresponse",
    "useraccountinformation"
})
public class Tlogonresponsebody {

    protected Tlogonresponsebody.Interfaceinformation interfaceinformation;
    @XmlElement(required = true)
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger currentkeylistversion;
    protected boolean unreadmail;
    protected List<String> emailaddress;
    protected List<Tservice> service;
    protected boolean reportavailabilityinsearchresponse;
    protected Tlogonresponsebody.Useraccountinformation useraccountinformation;

    /**
     * Ruft den Wert der interfaceinformation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tlogonresponsebody.Interfaceinformation }
     *     
     */
    public Tlogonresponsebody.Interfaceinformation getInterfaceinformation() {
        return interfaceinformation;
    }

    /**
     * Legt den Wert der interfaceinformation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tlogonresponsebody.Interfaceinformation }
     *     
     */
    public void setInterfaceinformation(Tlogonresponsebody.Interfaceinformation value) {
        this.interfaceinformation = value;
    }

    /**
     * Ruft den Wert der currentkeylistversion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCurrentkeylistversion() {
        return currentkeylistversion;
    }

    /**
     * Legt den Wert der currentkeylistversion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCurrentkeylistversion(BigInteger value) {
        this.currentkeylistversion = value;
    }

    /**
     * Ruft den Wert der unreadmail-Eigenschaft ab.
     * 
     */
    public boolean isUnreadmail() {
        return unreadmail;
    }

    /**
     * Legt den Wert der unreadmail-Eigenschaft fest.
     * 
     */
    public void setUnreadmail(boolean value) {
        this.unreadmail = value;
    }

    /**
     * Gets the value of the emailaddress property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the emailaddress property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmailaddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getEmailaddress() {
        if (emailaddress == null) {
            emailaddress = new ArrayList<String>();
        }
        return this.emailaddress;
    }

    /**
     * Gets the value of the service property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the service property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getService().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tservice }
     * 
     * 
     */
    public List<Tservice> getService() {
        if (service == null) {
            service = new ArrayList<Tservice>();
        }
        return this.service;
    }

    /**
     * Ruft den Wert der reportavailabilityinsearchresponse-Eigenschaft ab.
     * 
     */
    public boolean isReportavailabilityinsearchresponse() {
        return reportavailabilityinsearchresponse;
    }

    /**
     * Legt den Wert der reportavailabilityinsearchresponse-Eigenschaft fest.
     * 
     */
    public void setReportavailabilityinsearchresponse(boolean value) {
        this.reportavailabilityinsearchresponse = value;
    }

    /**
     * Ruft den Wert der useraccountinformation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tlogonresponsebody.Useraccountinformation }
     *     
     */
    public Tlogonresponsebody.Useraccountinformation getUseraccountinformation() {
        return useraccountinformation;
    }

    /**
     * Legt den Wert der useraccountinformation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tlogonresponsebody.Useraccountinformation }
     *     
     */
    public void setUseraccountinformation(Tlogonresponsebody.Useraccountinformation value) {
        this.useraccountinformation = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="expiry" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="currentinterfaceversion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="upgraderecommended" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "expiry",
        "currentinterfaceversion",
        "upgraderecommended"
    })
    public static class Interfaceinformation {

        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar expiry;
        protected String currentinterfaceversion;
        protected Boolean upgraderecommended;

        /**
         * Ruft den Wert der expiry-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getExpiry() {
            return expiry;
        }

        /**
         * Legt den Wert der expiry-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setExpiry(XMLGregorianCalendar value) {
            this.expiry = value;
        }

        /**
         * Ruft den Wert der currentinterfaceversion-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCurrentinterfaceversion() {
            return currentinterfaceversion;
        }

        /**
         * Legt den Wert der currentinterfaceversion-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCurrentinterfaceversion(String value) {
            this.currentinterfaceversion = value;
        }

        /**
         * Ruft den Wert der upgraderecommended-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Boolean }
         *     
         */
        public Boolean isUpgraderecommended() {
            return upgraderecommended;
        }

        /**
         * Legt den Wert der upgraderecommended-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Boolean }
         *     
         */
        public void setUpgraderecommended(Boolean value) {
            this.upgraderecommended = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="subuser" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" minOccurs="0"/>
     *                   &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "subuser"
    })
    public static class Useraccountinformation {

        protected List<Tlogonresponsebody.Useraccountinformation.Subuser> subuser;

        /**
         * Gets the value of the subuser property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the subuser property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getSubuser().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tlogonresponsebody.Useraccountinformation.Subuser }
         * 
         * 
         */
        public List<Tlogonresponsebody.Useraccountinformation.Subuser> getSubuser() {
            if (subuser == null) {
                subuser = new ArrayList<Tlogonresponsebody.Useraccountinformation.Subuser>();
            }
            return this.subuser;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" minOccurs="0"/>
         *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="address" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "user",
            "name",
            "address"
        })
        public static class Subuser {

            protected String user;
            protected String name;
            protected String address;

            /**
             * Ruft den Wert der user-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUser() {
                return user;
            }

            /**
             * Legt den Wert der user-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUser(String value) {
                this.user = value;
            }

            /**
             * Ruft den Wert der name-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName() {
                return name;
            }

            /**
             * Legt den Wert der name-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName(String value) {
                this.name = value;
            }

            /**
             * Ruft den Wert der address-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAddress() {
                return address;
            }

            /**
             * Legt den Wert der address-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAddress(String value) {
                this.address = value;
            }

        }

    }

}
