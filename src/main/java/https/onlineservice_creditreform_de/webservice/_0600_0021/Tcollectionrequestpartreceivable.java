
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Partreceivable Information
 * 
 * <p>Java-Klasse f�r Tcollectionrequestpartreceivable complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionrequestpartreceivable">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="collectionturnovertype" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="utilitytype" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="businesspartner" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="consumptionaccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="counternumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="counterreadingfrom" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger">
 *               &lt;totalDigits value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="counterreadingupto" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger">
 *               &lt;totalDigits value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="consumption" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger">
 *               &lt;totalDigits value="18"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="measuringunit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datereading" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="consumptionplace" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="street" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstreet" minOccurs="0"/>
 *                   &lt;element name="housenumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Thousenumber" minOccurs="0"/>
 *                   &lt;element name="housenumberaffix" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Thousenumberaffix" minOccurs="0"/>
 *                   &lt;element name="postcode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpostcode" minOccurs="0"/>
 *                   &lt;element name="city" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcity" minOccurs="0"/>
 *                   &lt;element name="country" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcountry"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="dateinvoice" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="invoicenumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="receivablereason" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="datereminder" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="servicedeliveryperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tperiod" minOccurs="0"/>
 *         &lt;element name="datevaluta" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="datedue" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="amount" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestamount"/>
 *         &lt;element name="remarks" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datecontract" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionrequestpartreceivable", propOrder = {
    "collectionturnovertype",
    "utilitytype",
    "businesspartner",
    "consumptionaccount",
    "counternumber",
    "counterreadingfrom",
    "counterreadingupto",
    "consumption",
    "measuringunit",
    "datereading",
    "consumptionplace",
    "dateinvoice",
    "invoicenumber",
    "receivablereason",
    "datereminder",
    "servicedeliveryperiod",
    "datevaluta",
    "datedue",
    "amount",
    "remarks",
    "datecontract"
})
public class Tcollectionrequestpartreceivable {

    @XmlElement(required = true)
    protected String collectionturnovertype;
    protected String utilitytype;
    protected String businesspartner;
    protected String consumptionaccount;
    protected String counternumber;
    protected BigInteger counterreadingfrom;
    protected BigInteger counterreadingupto;
    protected BigInteger consumption;
    protected String measuringunit;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datereading;
    protected Tcollectionrequestpartreceivable.Consumptionplace consumptionplace;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateinvoice;
    protected String invoicenumber;
    @XmlElement(required = true)
    protected String receivablereason;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datereminder;
    protected Tperiod servicedeliveryperiod;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datevaluta;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datedue;
    @XmlElement(required = true)
    protected BigDecimal amount;
    protected String remarks;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datecontract;

    /**
     * Ruft den Wert der collectionturnovertype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectionturnovertype() {
        return collectionturnovertype;
    }

    /**
     * Legt den Wert der collectionturnovertype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionturnovertype(String value) {
        this.collectionturnovertype = value;
    }

    /**
     * Ruft den Wert der utilitytype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUtilitytype() {
        return utilitytype;
    }

    /**
     * Legt den Wert der utilitytype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUtilitytype(String value) {
        this.utilitytype = value;
    }

    /**
     * Ruft den Wert der businesspartner-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinesspartner() {
        return businesspartner;
    }

    /**
     * Legt den Wert der businesspartner-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinesspartner(String value) {
        this.businesspartner = value;
    }

    /**
     * Ruft den Wert der consumptionaccount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumptionaccount() {
        return consumptionaccount;
    }

    /**
     * Legt den Wert der consumptionaccount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumptionaccount(String value) {
        this.consumptionaccount = value;
    }

    /**
     * Ruft den Wert der counternumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCounternumber() {
        return counternumber;
    }

    /**
     * Legt den Wert der counternumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCounternumber(String value) {
        this.counternumber = value;
    }

    /**
     * Ruft den Wert der counterreadingfrom-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCounterreadingfrom() {
        return counterreadingfrom;
    }

    /**
     * Legt den Wert der counterreadingfrom-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCounterreadingfrom(BigInteger value) {
        this.counterreadingfrom = value;
    }

    /**
     * Ruft den Wert der counterreadingupto-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCounterreadingupto() {
        return counterreadingupto;
    }

    /**
     * Legt den Wert der counterreadingupto-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCounterreadingupto(BigInteger value) {
        this.counterreadingupto = value;
    }

    /**
     * Ruft den Wert der consumption-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getConsumption() {
        return consumption;
    }

    /**
     * Legt den Wert der consumption-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setConsumption(BigInteger value) {
        this.consumption = value;
    }

    /**
     * Ruft den Wert der measuringunit-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeasuringunit() {
        return measuringunit;
    }

    /**
     * Legt den Wert der measuringunit-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeasuringunit(String value) {
        this.measuringunit = value;
    }

    /**
     * Ruft den Wert der datereading-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatereading() {
        return datereading;
    }

    /**
     * Legt den Wert der datereading-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatereading(XMLGregorianCalendar value) {
        this.datereading = value;
    }

    /**
     * Ruft den Wert der consumptionplace-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestpartreceivable.Consumptionplace }
     *     
     */
    public Tcollectionrequestpartreceivable.Consumptionplace getConsumptionplace() {
        return consumptionplace;
    }

    /**
     * Legt den Wert der consumptionplace-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestpartreceivable.Consumptionplace }
     *     
     */
    public void setConsumptionplace(Tcollectionrequestpartreceivable.Consumptionplace value) {
        this.consumptionplace = value;
    }

    /**
     * Ruft den Wert der dateinvoice-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateinvoice() {
        return dateinvoice;
    }

    /**
     * Legt den Wert der dateinvoice-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateinvoice(XMLGregorianCalendar value) {
        this.dateinvoice = value;
    }

    /**
     * Ruft den Wert der invoicenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInvoicenumber() {
        return invoicenumber;
    }

    /**
     * Legt den Wert der invoicenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInvoicenumber(String value) {
        this.invoicenumber = value;
    }

    /**
     * Ruft den Wert der receivablereason-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReceivablereason() {
        return receivablereason;
    }

    /**
     * Legt den Wert der receivablereason-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReceivablereason(String value) {
        this.receivablereason = value;
    }

    /**
     * Ruft den Wert der datereminder-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatereminder() {
        return datereminder;
    }

    /**
     * Legt den Wert der datereminder-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatereminder(XMLGregorianCalendar value) {
        this.datereminder = value;
    }

    /**
     * Ruft den Wert der servicedeliveryperiod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tperiod }
     *     
     */
    public Tperiod getServicedeliveryperiod() {
        return servicedeliveryperiod;
    }

    /**
     * Legt den Wert der servicedeliveryperiod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tperiod }
     *     
     */
    public void setServicedeliveryperiod(Tperiod value) {
        this.servicedeliveryperiod = value;
    }

    /**
     * Ruft den Wert der datevaluta-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatevaluta() {
        return datevaluta;
    }

    /**
     * Legt den Wert der datevaluta-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatevaluta(XMLGregorianCalendar value) {
        this.datevaluta = value;
    }

    /**
     * Ruft den Wert der datedue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatedue() {
        return datedue;
    }

    /**
     * Legt den Wert der datedue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatedue(XMLGregorianCalendar value) {
        this.datedue = value;
    }

    /**
     * Ruft den Wert der amount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Legt den Wert der amount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setAmount(BigDecimal value) {
        this.amount = value;
    }

    /**
     * Ruft den Wert der remarks-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRemarks() {
        return remarks;
    }

    /**
     * Legt den Wert der remarks-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRemarks(String value) {
        this.remarks = value;
    }

    /**
     * Ruft den Wert der datecontract-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatecontract() {
        return datecontract;
    }

    /**
     * Legt den Wert der datecontract-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatecontract(XMLGregorianCalendar value) {
        this.datecontract = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="street" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstreet" minOccurs="0"/>
     *         &lt;element name="housenumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Thousenumber" minOccurs="0"/>
     *         &lt;element name="housenumberaffix" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Thousenumberaffix" minOccurs="0"/>
     *         &lt;element name="postcode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpostcode" minOccurs="0"/>
     *         &lt;element name="city" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcity" minOccurs="0"/>
     *         &lt;element name="country" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcountry"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "street",
        "housenumber",
        "housenumberaffix",
        "postcode",
        "city",
        "country"
    })
    public static class Consumptionplace {

        protected String street;
        @XmlSchemaType(name = "integer")
        protected Integer housenumber;
        protected String housenumberaffix;
        protected String postcode;
        protected String city;
        @XmlElement(required = true)
        protected String country;

        /**
         * Ruft den Wert der street-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreet() {
            return street;
        }

        /**
         * Legt den Wert der street-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreet(String value) {
            this.street = value;
        }

        /**
         * Ruft den Wert der housenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getHousenumber() {
            return housenumber;
        }

        /**
         * Legt den Wert der housenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setHousenumber(Integer value) {
            this.housenumber = value;
        }

        /**
         * Ruft den Wert der housenumberaffix-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHousenumberaffix() {
            return housenumberaffix;
        }

        /**
         * Legt den Wert der housenumberaffix-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHousenumberaffix(String value) {
            this.housenumberaffix = value;
        }

        /**
         * Ruft den Wert der postcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostcode() {
            return postcode;
        }

        /**
         * Legt den Wert der postcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostcode(String value) {
            this.postcode = value;
        }

        /**
         * Ruft den Wert der city-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Legt den Wert der city-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Ruft den Wert der country-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountry() {
            return country;
        }

        /**
         * Legt den Wert der country-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountry(String value) {
            this.country = value;
        }

    }

}
