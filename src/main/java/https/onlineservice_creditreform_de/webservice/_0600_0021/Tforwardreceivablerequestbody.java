
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tforwardreceivablerequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tforwardreceivablerequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;choice>
 *           &lt;element name="partreceivable" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestpartreceivable" maxOccurs="unbounded"/>
 *           &lt;element name="costturnover" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestcostturnover" maxOccurs="unbounded"/>
 *           &lt;element name="paymentturnover" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionrequestpaymentturnover" maxOccurs="unbounded"/>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tforwardreceivablerequestbody", propOrder = {
    "filenumber",
    "partreceivable",
    "costturnover",
    "paymentturnover"
})
public class Tforwardreceivablerequestbody {

    @XmlElement(required = true)
    protected String filenumber;
    protected List<Tcollectionrequestpartreceivable> partreceivable;
    protected List<Tcollectionrequestcostturnover> costturnover;
    protected List<Tcollectionrequestpaymentturnover> paymentturnover;

    /**
     * Ruft den Wert der filenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilenumber() {
        return filenumber;
    }

    /**
     * Legt den Wert der filenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilenumber(String value) {
        this.filenumber = value;
    }

    /**
     * Gets the value of the partreceivable property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the partreceivable property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPartreceivable().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcollectionrequestpartreceivable }
     * 
     * 
     */
    public List<Tcollectionrequestpartreceivable> getPartreceivable() {
        if (partreceivable == null) {
            partreceivable = new ArrayList<Tcollectionrequestpartreceivable>();
        }
        return this.partreceivable;
    }

    /**
     * Gets the value of the costturnover property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the costturnover property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCostturnover().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcollectionrequestcostturnover }
     * 
     * 
     */
    public List<Tcollectionrequestcostturnover> getCostturnover() {
        if (costturnover == null) {
            costturnover = new ArrayList<Tcollectionrequestcostturnover>();
        }
        return this.costturnover;
    }

    /**
     * Gets the value of the paymentturnover property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the paymentturnover property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPaymentturnover().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcollectionrequestpaymentturnover }
     * 
     * 
     */
    public List<Tcollectionrequestpaymentturnover> getPaymentturnover() {
        if (paymentturnover == null) {
            paymentturnover = new ArrayList<Tcollectionrequestpaymentturnover>();
        }
        return this.paymentturnover;
    }

}
