
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tfinancialstatementfiguresabstract complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tfinancialstatementfiguresabstract">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="calculationtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="typeofbalance" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="balanceform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="figures" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="reportperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timpreciseperiod" minOccurs="0"/>
 *                   &lt;element name="chapter" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="chaptername" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;element name="item" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="itemname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                                       &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                       &lt;element name="valuebranch" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                       &lt;element name="difference" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                                       &lt;element name="differencesignificant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tfinancialstatementfiguresabstract", propOrder = {
    "calculationtype",
    "typeofbalance",
    "balanceform",
    "figures",
    "text"
})
@XmlSeeAlso({
    Tfinancialstatementfigures.class,
    Tfinancialstatementfiguresgroup.class
})
public abstract class Tfinancialstatementfiguresabstract {

    protected Tkey calculationtype;
    protected Tkey typeofbalance;
    protected Tkey balanceform;
    protected List<Tfinancialstatementfiguresabstract.Figures> figures;
    protected Ttext text;

    /**
     * Ruft den Wert der calculationtype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCalculationtype() {
        return calculationtype;
    }

    /**
     * Legt den Wert der calculationtype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCalculationtype(Tkey value) {
        this.calculationtype = value;
    }

    /**
     * Ruft den Wert der typeofbalance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getTypeofbalance() {
        return typeofbalance;
    }

    /**
     * Legt den Wert der typeofbalance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setTypeofbalance(Tkey value) {
        this.typeofbalance = value;
    }

    /**
     * Ruft den Wert der balanceform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getBalanceform() {
        return balanceform;
    }

    /**
     * Legt den Wert der balanceform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setBalanceform(Tkey value) {
        this.balanceform = value;
    }

    /**
     * Gets the value of the figures property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the figures property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFigures().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tfinancialstatementfiguresabstract.Figures }
     * 
     * 
     */
    public List<Tfinancialstatementfiguresabstract.Figures> getFigures() {
        if (figures == null) {
            figures = new ArrayList<Tfinancialstatementfiguresabstract.Figures>();
        }
        return this.figures;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="reportperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timpreciseperiod" minOccurs="0"/>
     *         &lt;element name="chapter" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="chaptername" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;element name="item" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="itemname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                             &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                             &lt;element name="valuebranch" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                             &lt;element name="difference" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *                             &lt;element name="differencesignificant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "reportperiod",
        "chapter"
    })
    public static class Figures {

        protected Timpreciseperiod reportperiod;
        protected List<Tfinancialstatementfiguresabstract.Figures.Chapter> chapter;

        /**
         * Ruft den Wert der reportperiod-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Timpreciseperiod }
         *     
         */
        public Timpreciseperiod getReportperiod() {
            return reportperiod;
        }

        /**
         * Legt den Wert der reportperiod-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Timpreciseperiod }
         *     
         */
        public void setReportperiod(Timpreciseperiod value) {
            this.reportperiod = value;
        }

        /**
         * Gets the value of the chapter property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the chapter property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getChapter().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tfinancialstatementfiguresabstract.Figures.Chapter }
         * 
         * 
         */
        public List<Tfinancialstatementfiguresabstract.Figures.Chapter> getChapter() {
            if (chapter == null) {
                chapter = new ArrayList<Tfinancialstatementfiguresabstract.Figures.Chapter>();
            }
            return this.chapter;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="chaptername" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="item" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="itemname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *                   &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                   &lt;element name="valuebranch" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                   &lt;element name="difference" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
         *                   &lt;element name="differencesignificant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "chaptername",
            "item"
        })
        public static class Chapter {

            protected Tkey chaptername;
            protected List<Tfinancialstatementfiguresabstract.Figures.Chapter.Item> item;

            /**
             * Ruft den Wert der chaptername-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getChaptername() {
                return chaptername;
            }

            /**
             * Legt den Wert der chaptername-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setChaptername(Tkey value) {
                this.chaptername = value;
            }

            /**
             * Gets the value of the item property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the item property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getItem().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Tfinancialstatementfiguresabstract.Figures.Chapter.Item }
             * 
             * 
             */
            public List<Tfinancialstatementfiguresabstract.Figures.Chapter.Item> getItem() {
                if (item == null) {
                    item = new ArrayList<Tfinancialstatementfiguresabstract.Figures.Chapter.Item>();
                }
                return this.item;
            }


            /**
             * <p>Java-Klasse f�r anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="itemname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
             *         &lt;element name="value" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *         &lt;element name="valuebranch" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *         &lt;element name="difference" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
             *         &lt;element name="differencesignificant" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "itemname",
                "value",
                "valuebranch",
                "difference",
                "differencesignificant"
            })
            public static class Item {

                protected Tkey itemname;
                protected BigDecimal value;
                protected BigDecimal valuebranch;
                protected BigDecimal difference;
                protected Boolean differencesignificant;

                /**
                 * Ruft den Wert der itemname-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Tkey }
                 *     
                 */
                public Tkey getItemname() {
                    return itemname;
                }

                /**
                 * Legt den Wert der itemname-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Tkey }
                 *     
                 */
                public void setItemname(Tkey value) {
                    this.itemname = value;
                }

                /**
                 * Ruft den Wert der value-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getValue() {
                    return value;
                }

                /**
                 * Legt den Wert der value-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setValue(BigDecimal value) {
                    this.value = value;
                }

                /**
                 * Ruft den Wert der valuebranch-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getValuebranch() {
                    return valuebranch;
                }

                /**
                 * Legt den Wert der valuebranch-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setValuebranch(BigDecimal value) {
                    this.valuebranch = value;
                }

                /**
                 * Ruft den Wert der difference-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link BigDecimal }
                 *     
                 */
                public BigDecimal getDifference() {
                    return difference;
                }

                /**
                 * Legt den Wert der difference-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link BigDecimal }
                 *     
                 */
                public void setDifference(BigDecimal value) {
                    this.difference = value;
                }

                /**
                 * Ruft den Wert der differencesignificant-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Boolean }
                 *     
                 */
                public Boolean isDifferencesignificant() {
                    return differencesignificant;
                }

                /**
                 * Legt den Wert der differencesignificant-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Boolean }
                 *     
                 */
                public void setDifferencesignificant(Boolean value) {
                    this.differencesignificant = value;
                }

            }

        }

    }

}
