
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Treportresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Treportresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}referencenumber"/>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *         &lt;element name="creationtime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="reportlanguage" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlanguage"/>
 *         &lt;element name="deliverytype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="useraccount" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuseraccount" minOccurs="0"/>
 *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}responsemonitoringstructure"/>
 *         &lt;element name="negativereport" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="provider" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="reportdata">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="companyidentification" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcompanyidentification" minOccurs="0"/>
 *                   &lt;element name="privatepersonidentification" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tprivateperson" minOccurs="0"/>
 *                   &lt;element name="statusreply" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstatusreply" minOccurs="0"/>
 *                   &lt;element name="negativereport" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tnegativereport" minOccurs="0"/>
 *                   &lt;element name="requestdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestdata" minOccurs="0"/>
 *                   &lt;element name="taxdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttaxdata" minOccurs="0"/>
 *                   &lt;element name="solvencyindex" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyindex" minOccurs="0"/>
 *                   &lt;element name="solvencyclass" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyclass" minOccurs="0"/>
 *                   &lt;element name="trafficlightsolvency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttrafficlightsolvency" minOccurs="0"/>
 *                   &lt;element name="solvencyremarks" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyremarks" minOccurs="0"/>
 *                   &lt;element name="trafficlightpaymentmode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttrafficlightpaymentmode" minOccurs="0"/>
 *                   &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlegalform" minOccurs="0"/>
 *                   &lt;element name="foundation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfoundation" minOccurs="0"/>
 *                   &lt;element name="register" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tregister" minOccurs="0"/>
 *                   &lt;element name="shareholdercapital" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tshareholdercapital" minOccurs="0"/>
 *                   &lt;element name="deputymanagement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tdeputymanagement" minOccurs="0"/>
 *                   &lt;element name="participationscompany" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationscompany" minOccurs="0"/>
 *                   &lt;element name="participationsprivateperson" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipation" minOccurs="0"/>
 *                   &lt;element name="participationsparticipants" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationsparticipants" minOccurs="0"/>
 *                   &lt;element name="businesspurpose" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbusinesspurpose" minOccurs="0"/>
 *                   &lt;element name="branch" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbranch" minOccurs="0"/>
 *                   &lt;element name="staffcompany" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstaffcompany" minOccurs="0"/>
 *                   &lt;element name="staffgroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstaffgroup" minOccurs="0"/>
 *                   &lt;element name="turnovercompany" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tturnovercompany" minOccurs="0"/>
 *                   &lt;element name="turnovergroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tturnovergroup" minOccurs="0"/>
 *                   &lt;element name="certificates" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="balancesheet" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheet" minOccurs="0"/>
 *                   &lt;element name="remarks" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                   &lt;element name="banks" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbanks" minOccurs="0"/>
 *                   &lt;element name="enquirycounter" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tenquirycounter" minOccurs="0"/>
 *                   &lt;element name="businessdevelopment" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbusinessdevelopment" minOccurs="0"/>
 *                   &lt;element name="updatenote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="specificationofprofession" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tspecificationofprofession" minOccurs="0"/>
 *                   &lt;element name="ancillaryinformation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tancillaryinformation" minOccurs="0"/>
 *                   &lt;element name="financialdata" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence minOccurs="0">
 *                             &lt;element name="balance" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalance" maxOccurs="unbounded" minOccurs="0"/>
 *                             &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="businessarea" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbusinessarea" minOccurs="0"/>
 *                   &lt;element name="realestates" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trealestates" minOccurs="0"/>
 *                   &lt;element name="importexport" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timportexport" minOccurs="0"/>
 *                   &lt;element name="paymentmode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpaymentmode" minOccurs="0"/>
 *                   &lt;element name="creditopinion" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcreditopinion" minOccurs="0"/>
 *                   &lt;element name="supplement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsupplement" minOccurs="0"/>
 *                   &lt;element name="negativefacts" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tnegativefact" minOccurs="0"/>
 *                   &lt;element name="registerhistory" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tregisterhistory" minOccurs="0"/>
 *                   &lt;element name="locations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlocations" minOccurs="0"/>
 *                   &lt;element name="profitslosses" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tprofitslosses" minOccurs="0"/>
 *                   &lt;element name="financialstatusprivateperson" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfinancialstatusprivateperson" minOccurs="0"/>
 *                   &lt;element name="formerlocations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tformerlocations" minOccurs="0"/>
 *                   &lt;element name="affiliatedgroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Taffiliatedgroup" minOccurs="0"/>
 *                   &lt;element name="balancesheetgroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheetgroup" minOccurs="0"/>
 *                   &lt;element name="profitslossesgroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tprofitslossesgroup" minOccurs="0"/>
 *                   &lt;element name="solvencyindexhistory" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyindexhistory" minOccurs="0"/>
 *                   &lt;element name="solvencyfinanceindustry" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyfinanceindustry" minOccurs="0"/>
 *                   &lt;element name="evaluationsfinanceindustry" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tevaluationsfinanceindustry" minOccurs="0"/>
 *                   &lt;element name="balancesolvency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesolvency" minOccurs="0"/>
 *                   &lt;element name="financialstatementfigures" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfinancialstatementfigures" minOccurs="0"/>
 *                   &lt;element name="financialstatementfiguresgroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfinancialstatementfiguresgroup" minOccurs="0"/>
 *                   &lt;element name="turnovercompanyrange" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tturnovercompanyrange" minOccurs="0"/>
 *                   &lt;element name="staffcompanyrange" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstaffcompanyrange" minOccurs="0"/>
 *                   &lt;element name="paymentbehaviour" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpaymentbehaviour" minOccurs="0"/>
 *                   &lt;element name="financialinformation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfinancialinformation" minOccurs="0"/>
 *                   &lt;element name="reporthints" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Treporthints" minOccurs="0"/>
 *                   &lt;element name="riskjudgement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Triskjudgement" minOccurs="0"/>
 *                   &lt;element name="legaldisclaimer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="textreport" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Treportresponsebody", propOrder = {
    "referencenumber",
    "customerreference",
    "identificationnumber",
    "easynumber",
    "creationtime",
    "producttype",
    "reportlanguage",
    "deliverytype",
    "useraccount",
    "endofstandardmonitoring",
    "extendedmonitoring",
    "extendedmonitoringplus",
    "negativereport",
    "provider",
    "reportdata"
})
public class Treportresponsebody {

    protected long referencenumber;
    protected String customerreference;
    protected String identificationnumber;
    protected String easynumber;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationtime;
    protected Tkey producttype;
    @XmlElement(required = true)
    protected String reportlanguage;
    protected Tkey deliverytype;
    protected String useraccount;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar endofstandardmonitoring;
    protected https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring extendedmonitoring;
    protected https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus extendedmonitoringplus;
    protected boolean negativereport;
    protected String provider;
    @XmlElement(required = true)
    protected Treportresponsebody.Reportdata reportdata;

    /**
     * Ruft den Wert der referencenumber-Eigenschaft ab.
     * 
     */
    public long getReferencenumber() {
        return referencenumber;
    }

    /**
     * Legt den Wert der referencenumber-Eigenschaft fest.
     * 
     */
    public void setReferencenumber(long value) {
        this.referencenumber = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der easynumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEasynumber() {
        return easynumber;
    }

    /**
     * Legt den Wert der easynumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEasynumber(String value) {
        this.easynumber = value;
    }

    /**
     * Ruft den Wert der creationtime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationtime() {
        return creationtime;
    }

    /**
     * Legt den Wert der creationtime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationtime(XMLGregorianCalendar value) {
        this.creationtime = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setProducttype(Tkey value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der reportlanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportlanguage() {
        return reportlanguage;
    }

    /**
     * Legt den Wert der reportlanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportlanguage(String value) {
        this.reportlanguage = value;
    }

    /**
     * Ruft den Wert der deliverytype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getDeliverytype() {
        return deliverytype;
    }

    /**
     * Legt den Wert der deliverytype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setDeliverytype(Tkey value) {
        this.deliverytype = value;
    }

    /**
     * Ruft den Wert der useraccount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseraccount() {
        return useraccount;
    }

    /**
     * Legt den Wert der useraccount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseraccount(String value) {
        this.useraccount = value;
    }

    /**
     * Ruft den Wert der endofstandardmonitoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEndofstandardmonitoring() {
        return endofstandardmonitoring;
    }

    /**
     * Legt den Wert der endofstandardmonitoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEndofstandardmonitoring(XMLGregorianCalendar value) {
        this.endofstandardmonitoring = value;
    }

    /**
     * Ruft den Wert der extendedmonitoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring }
     *     
     */
    public https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring getExtendedmonitoring() {
        return extendedmonitoring;
    }

    /**
     * Legt den Wert der extendedmonitoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring }
     *     
     */
    public void setExtendedmonitoring(https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry.Extendedmonitoring value) {
        this.extendedmonitoring = value;
    }

    /**
     * Ruft den Wert der extendedmonitoringplus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus }
     *     
     */
    public https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus getExtendedmonitoringplus() {
        return extendedmonitoringplus;
    }

    /**
     * Legt den Wert der extendedmonitoringplus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus }
     *     
     */
    public void setExtendedmonitoringplus(https.onlineservice_creditreform_de.webservice._0600_0021.Tmailboxdirectoryresponsebody.Entry.Extendedmonitoringplus value) {
        this.extendedmonitoringplus = value;
    }

    /**
     * Ruft den Wert der negativereport-Eigenschaft ab.
     * 
     */
    public boolean isNegativereport() {
        return negativereport;
    }

    /**
     * Legt den Wert der negativereport-Eigenschaft fest.
     * 
     */
    public void setNegativereport(boolean value) {
        this.negativereport = value;
    }

    /**
     * Ruft den Wert der provider-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProvider() {
        return provider;
    }

    /**
     * Legt den Wert der provider-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProvider(String value) {
        this.provider = value;
    }

    /**
     * Ruft den Wert der reportdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Treportresponsebody.Reportdata }
     *     
     */
    public Treportresponsebody.Reportdata getReportdata() {
        return reportdata;
    }

    /**
     * Legt den Wert der reportdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Treportresponsebody.Reportdata }
     *     
     */
    public void setReportdata(Treportresponsebody.Reportdata value) {
        this.reportdata = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="companyidentification" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcompanyidentification" minOccurs="0"/>
     *         &lt;element name="privatepersonidentification" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tprivateperson" minOccurs="0"/>
     *         &lt;element name="statusreply" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstatusreply" minOccurs="0"/>
     *         &lt;element name="negativereport" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tnegativereport" minOccurs="0"/>
     *         &lt;element name="requestdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestdata" minOccurs="0"/>
     *         &lt;element name="taxdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttaxdata" minOccurs="0"/>
     *         &lt;element name="solvencyindex" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyindex" minOccurs="0"/>
     *         &lt;element name="solvencyclass" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyclass" minOccurs="0"/>
     *         &lt;element name="trafficlightsolvency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttrafficlightsolvency" minOccurs="0"/>
     *         &lt;element name="solvencyremarks" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyremarks" minOccurs="0"/>
     *         &lt;element name="trafficlightpaymentmode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttrafficlightpaymentmode" minOccurs="0"/>
     *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlegalform" minOccurs="0"/>
     *         &lt;element name="foundation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfoundation" minOccurs="0"/>
     *         &lt;element name="register" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tregister" minOccurs="0"/>
     *         &lt;element name="shareholdercapital" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tshareholdercapital" minOccurs="0"/>
     *         &lt;element name="deputymanagement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tdeputymanagement" minOccurs="0"/>
     *         &lt;element name="participationscompany" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationscompany" minOccurs="0"/>
     *         &lt;element name="participationsprivateperson" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipation" minOccurs="0"/>
     *         &lt;element name="participationsparticipants" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipationsparticipants" minOccurs="0"/>
     *         &lt;element name="businesspurpose" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbusinesspurpose" minOccurs="0"/>
     *         &lt;element name="branch" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbranch" minOccurs="0"/>
     *         &lt;element name="staffcompany" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstaffcompany" minOccurs="0"/>
     *         &lt;element name="staffgroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstaffgroup" minOccurs="0"/>
     *         &lt;element name="turnovercompany" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tturnovercompany" minOccurs="0"/>
     *         &lt;element name="turnovergroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tturnovergroup" minOccurs="0"/>
     *         &lt;element name="certificates" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="balancesheet" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheet" minOccurs="0"/>
     *         &lt;element name="remarks" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *         &lt;element name="banks" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbanks" minOccurs="0"/>
     *         &lt;element name="enquirycounter" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tenquirycounter" minOccurs="0"/>
     *         &lt;element name="businessdevelopment" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbusinessdevelopment" minOccurs="0"/>
     *         &lt;element name="updatenote" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="specificationofprofession" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tspecificationofprofession" minOccurs="0"/>
     *         &lt;element name="ancillaryinformation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tancillaryinformation" minOccurs="0"/>
     *         &lt;element name="financialdata" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence minOccurs="0">
     *                   &lt;element name="balance" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalance" maxOccurs="unbounded" minOccurs="0"/>
     *                   &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="businessarea" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbusinessarea" minOccurs="0"/>
     *         &lt;element name="realestates" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trealestates" minOccurs="0"/>
     *         &lt;element name="importexport" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timportexport" minOccurs="0"/>
     *         &lt;element name="paymentmode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpaymentmode" minOccurs="0"/>
     *         &lt;element name="creditopinion" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcreditopinion" minOccurs="0"/>
     *         &lt;element name="supplement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsupplement" minOccurs="0"/>
     *         &lt;element name="negativefacts" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tnegativefact" minOccurs="0"/>
     *         &lt;element name="registerhistory" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tregisterhistory" minOccurs="0"/>
     *         &lt;element name="locations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlocations" minOccurs="0"/>
     *         &lt;element name="profitslosses" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tprofitslosses" minOccurs="0"/>
     *         &lt;element name="financialstatusprivateperson" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfinancialstatusprivateperson" minOccurs="0"/>
     *         &lt;element name="formerlocations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tformerlocations" minOccurs="0"/>
     *         &lt;element name="affiliatedgroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Taffiliatedgroup" minOccurs="0"/>
     *         &lt;element name="balancesheetgroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesheetgroup" minOccurs="0"/>
     *         &lt;element name="profitslossesgroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tprofitslossesgroup" minOccurs="0"/>
     *         &lt;element name="solvencyindexhistory" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyindexhistory" minOccurs="0"/>
     *         &lt;element name="solvencyfinanceindustry" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsolvencyfinanceindustry" minOccurs="0"/>
     *         &lt;element name="evaluationsfinanceindustry" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tevaluationsfinanceindustry" minOccurs="0"/>
     *         &lt;element name="balancesolvency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalancesolvency" minOccurs="0"/>
     *         &lt;element name="financialstatementfigures" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfinancialstatementfigures" minOccurs="0"/>
     *         &lt;element name="financialstatementfiguresgroup" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfinancialstatementfiguresgroup" minOccurs="0"/>
     *         &lt;element name="turnovercompanyrange" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tturnovercompanyrange" minOccurs="0"/>
     *         &lt;element name="staffcompanyrange" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tstaffcompanyrange" minOccurs="0"/>
     *         &lt;element name="paymentbehaviour" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tpaymentbehaviour" minOccurs="0"/>
     *         &lt;element name="financialinformation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfinancialinformation" minOccurs="0"/>
     *         &lt;element name="reporthints" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Treporthints" minOccurs="0"/>
     *         &lt;element name="riskjudgement" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Triskjudgement" minOccurs="0"/>
     *         &lt;element name="legaldisclaimer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="textreport" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "companyidentification",
        "privatepersonidentification",
        "statusreply",
        "negativereport",
        "requestdata",
        "taxdata",
        "solvencyindex",
        "solvencyclass",
        "trafficlightsolvency",
        "solvencyremarks",
        "trafficlightpaymentmode",
        "legalform",
        "foundation",
        "register",
        "shareholdercapital",
        "deputymanagement",
        "participationscompany",
        "participationsprivateperson",
        "participationsparticipants",
        "businesspurpose",
        "branch",
        "staffcompany",
        "staffgroup",
        "turnovercompany",
        "turnovergroup",
        "certificates",
        "balancesheet",
        "remarks",
        "banks",
        "enquirycounter",
        "businessdevelopment",
        "updatenote",
        "specificationofprofession",
        "ancillaryinformation",
        "financialdata",
        "businessarea",
        "realestates",
        "importexport",
        "paymentmode",
        "creditopinion",
        "supplement",
        "negativefacts",
        "registerhistory",
        "locations",
        "profitslosses",
        "financialstatusprivateperson",
        "formerlocations",
        "affiliatedgroup",
        "balancesheetgroup",
        "profitslossesgroup",
        "solvencyindexhistory",
        "solvencyfinanceindustry",
        "evaluationsfinanceindustry",
        "balancesolvency",
        "financialstatementfigures",
        "financialstatementfiguresgroup",
        "turnovercompanyrange",
        "staffcompanyrange",
        "paymentbehaviour",
        "financialinformation",
        "reporthints",
        "riskjudgement",
        "legaldisclaimer",
        "textreport"
    })
    public static class Reportdata {

        protected Tcompanyidentification companyidentification;
        protected Tprivateperson privatepersonidentification;
        protected Tstatusreply statusreply;
        protected Tnegativereport negativereport;
        protected Trequestdata requestdata;
        protected Ttaxdata taxdata;
        protected Tsolvencyindex solvencyindex;
        protected Tsolvencyclass solvencyclass;
        protected Ttrafficlightsolvency trafficlightsolvency;
        protected Tsolvencyremarks solvencyremarks;
        protected Ttrafficlightpaymentmode trafficlightpaymentmode;
        protected Tlegalform legalform;
        protected Tfoundation foundation;
        protected Tregister register;
        protected Tshareholdercapital shareholdercapital;
        protected Tdeputymanagement deputymanagement;
        protected Tparticipationscompany participationscompany;
        protected Tparticipation participationsprivateperson;
        protected Tparticipationsparticipants participationsparticipants;
        protected Tbusinesspurpose businesspurpose;
        protected Tbranch branch;
        protected Tstaffcompany staffcompany;
        protected Tstaffgroup staffgroup;
        protected Tturnovercompany turnovercompany;
        protected Tturnovergroup turnovergroup;
        protected String certificates;
        protected Tbalancesheet balancesheet;
        protected Ttext remarks;
        protected Tbanks banks;
        protected Tenquirycounter enquirycounter;
        protected Tbusinessdevelopment businessdevelopment;
        protected String updatenote;
        protected Tspecificationofprofession specificationofprofession;
        protected Tancillaryinformation ancillaryinformation;
        protected Treportresponsebody.Reportdata.Financialdata financialdata;
        protected Tbusinessarea businessarea;
        protected Trealestates realestates;
        protected Timportexport importexport;
        protected Tpaymentmode paymentmode;
        protected Tcreditopinion creditopinion;
        protected Tsupplement supplement;
        protected Tnegativefact negativefacts;
        protected Tregisterhistory registerhistory;
        protected Tlocations locations;
        protected Tprofitslosses profitslosses;
        protected Tfinancialstatusprivateperson financialstatusprivateperson;
        protected Tformerlocations formerlocations;
        protected Taffiliatedgroup affiliatedgroup;
        protected Tbalancesheetgroup balancesheetgroup;
        protected Tprofitslossesgroup profitslossesgroup;
        protected Tsolvencyindexhistory solvencyindexhistory;
        protected Tsolvencyfinanceindustry solvencyfinanceindustry;
        protected Tevaluationsfinanceindustry evaluationsfinanceindustry;
        protected Tbalancesolvency balancesolvency;
        protected Tfinancialstatementfigures financialstatementfigures;
        protected Tfinancialstatementfiguresgroup financialstatementfiguresgroup;
        protected Tturnovercompanyrange turnovercompanyrange;
        protected Tstaffcompanyrange staffcompanyrange;
        protected Tpaymentbehaviour paymentbehaviour;
        protected Tfinancialinformation financialinformation;
        protected Treporthints reporthints;
        protected Triskjudgement riskjudgement;
        protected String legaldisclaimer;
        @XmlMimeType("application/octet-stream")
        protected DataHandler textreport;

        /**
         * Ruft den Wert der companyidentification-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tcompanyidentification }
         *     
         */
        public Tcompanyidentification getCompanyidentification() {
            return companyidentification;
        }

        /**
         * Legt den Wert der companyidentification-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tcompanyidentification }
         *     
         */
        public void setCompanyidentification(Tcompanyidentification value) {
            this.companyidentification = value;
        }

        /**
         * Ruft den Wert der privatepersonidentification-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tprivateperson }
         *     
         */
        public Tprivateperson getPrivatepersonidentification() {
            return privatepersonidentification;
        }

        /**
         * Legt den Wert der privatepersonidentification-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tprivateperson }
         *     
         */
        public void setPrivatepersonidentification(Tprivateperson value) {
            this.privatepersonidentification = value;
        }

        /**
         * Ruft den Wert der statusreply-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tstatusreply }
         *     
         */
        public Tstatusreply getStatusreply() {
            return statusreply;
        }

        /**
         * Legt den Wert der statusreply-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tstatusreply }
         *     
         */
        public void setStatusreply(Tstatusreply value) {
            this.statusreply = value;
        }

        /**
         * Ruft den Wert der negativereport-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tnegativereport }
         *     
         */
        public Tnegativereport getNegativereport() {
            return negativereport;
        }

        /**
         * Legt den Wert der negativereport-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tnegativereport }
         *     
         */
        public void setNegativereport(Tnegativereport value) {
            this.negativereport = value;
        }

        /**
         * Ruft den Wert der requestdata-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Trequestdata }
         *     
         */
        public Trequestdata getRequestdata() {
            return requestdata;
        }

        /**
         * Legt den Wert der requestdata-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Trequestdata }
         *     
         */
        public void setRequestdata(Trequestdata value) {
            this.requestdata = value;
        }

        /**
         * Ruft den Wert der taxdata-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttaxdata }
         *     
         */
        public Ttaxdata getTaxdata() {
            return taxdata;
        }

        /**
         * Legt den Wert der taxdata-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttaxdata }
         *     
         */
        public void setTaxdata(Ttaxdata value) {
            this.taxdata = value;
        }

        /**
         * Ruft den Wert der solvencyindex-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tsolvencyindex }
         *     
         */
        public Tsolvencyindex getSolvencyindex() {
            return solvencyindex;
        }

        /**
         * Legt den Wert der solvencyindex-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tsolvencyindex }
         *     
         */
        public void setSolvencyindex(Tsolvencyindex value) {
            this.solvencyindex = value;
        }

        /**
         * Ruft den Wert der solvencyclass-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tsolvencyclass }
         *     
         */
        public Tsolvencyclass getSolvencyclass() {
            return solvencyclass;
        }

        /**
         * Legt den Wert der solvencyclass-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tsolvencyclass }
         *     
         */
        public void setSolvencyclass(Tsolvencyclass value) {
            this.solvencyclass = value;
        }

        /**
         * Ruft den Wert der trafficlightsolvency-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttrafficlightsolvency }
         *     
         */
        public Ttrafficlightsolvency getTrafficlightsolvency() {
            return trafficlightsolvency;
        }

        /**
         * Legt den Wert der trafficlightsolvency-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttrafficlightsolvency }
         *     
         */
        public void setTrafficlightsolvency(Ttrafficlightsolvency value) {
            this.trafficlightsolvency = value;
        }

        /**
         * Ruft den Wert der solvencyremarks-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tsolvencyremarks }
         *     
         */
        public Tsolvencyremarks getSolvencyremarks() {
            return solvencyremarks;
        }

        /**
         * Legt den Wert der solvencyremarks-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tsolvencyremarks }
         *     
         */
        public void setSolvencyremarks(Tsolvencyremarks value) {
            this.solvencyremarks = value;
        }

        /**
         * Ruft den Wert der trafficlightpaymentmode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttrafficlightpaymentmode }
         *     
         */
        public Ttrafficlightpaymentmode getTrafficlightpaymentmode() {
            return trafficlightpaymentmode;
        }

        /**
         * Legt den Wert der trafficlightpaymentmode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttrafficlightpaymentmode }
         *     
         */
        public void setTrafficlightpaymentmode(Ttrafficlightpaymentmode value) {
            this.trafficlightpaymentmode = value;
        }

        /**
         * Ruft den Wert der legalform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tlegalform }
         *     
         */
        public Tlegalform getLegalform() {
            return legalform;
        }

        /**
         * Legt den Wert der legalform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tlegalform }
         *     
         */
        public void setLegalform(Tlegalform value) {
            this.legalform = value;
        }

        /**
         * Ruft den Wert der foundation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tfoundation }
         *     
         */
        public Tfoundation getFoundation() {
            return foundation;
        }

        /**
         * Legt den Wert der foundation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tfoundation }
         *     
         */
        public void setFoundation(Tfoundation value) {
            this.foundation = value;
        }

        /**
         * Ruft den Wert der register-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tregister }
         *     
         */
        public Tregister getRegister() {
            return register;
        }

        /**
         * Legt den Wert der register-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tregister }
         *     
         */
        public void setRegister(Tregister value) {
            this.register = value;
        }

        /**
         * Ruft den Wert der shareholdercapital-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tshareholdercapital }
         *     
         */
        public Tshareholdercapital getShareholdercapital() {
            return shareholdercapital;
        }

        /**
         * Legt den Wert der shareholdercapital-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tshareholdercapital }
         *     
         */
        public void setShareholdercapital(Tshareholdercapital value) {
            this.shareholdercapital = value;
        }

        /**
         * Ruft den Wert der deputymanagement-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tdeputymanagement }
         *     
         */
        public Tdeputymanagement getDeputymanagement() {
            return deputymanagement;
        }

        /**
         * Legt den Wert der deputymanagement-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tdeputymanagement }
         *     
         */
        public void setDeputymanagement(Tdeputymanagement value) {
            this.deputymanagement = value;
        }

        /**
         * Ruft den Wert der participationscompany-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tparticipationscompany }
         *     
         */
        public Tparticipationscompany getParticipationscompany() {
            return participationscompany;
        }

        /**
         * Legt den Wert der participationscompany-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tparticipationscompany }
         *     
         */
        public void setParticipationscompany(Tparticipationscompany value) {
            this.participationscompany = value;
        }

        /**
         * Ruft den Wert der participationsprivateperson-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tparticipation }
         *     
         */
        public Tparticipation getParticipationsprivateperson() {
            return participationsprivateperson;
        }

        /**
         * Legt den Wert der participationsprivateperson-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tparticipation }
         *     
         */
        public void setParticipationsprivateperson(Tparticipation value) {
            this.participationsprivateperson = value;
        }

        /**
         * Ruft den Wert der participationsparticipants-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tparticipationsparticipants }
         *     
         */
        public Tparticipationsparticipants getParticipationsparticipants() {
            return participationsparticipants;
        }

        /**
         * Legt den Wert der participationsparticipants-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tparticipationsparticipants }
         *     
         */
        public void setParticipationsparticipants(Tparticipationsparticipants value) {
            this.participationsparticipants = value;
        }

        /**
         * Ruft den Wert der businesspurpose-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbusinesspurpose }
         *     
         */
        public Tbusinesspurpose getBusinesspurpose() {
            return businesspurpose;
        }

        /**
         * Legt den Wert der businesspurpose-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbusinesspurpose }
         *     
         */
        public void setBusinesspurpose(Tbusinesspurpose value) {
            this.businesspurpose = value;
        }

        /**
         * Ruft den Wert der branch-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbranch }
         *     
         */
        public Tbranch getBranch() {
            return branch;
        }

        /**
         * Legt den Wert der branch-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbranch }
         *     
         */
        public void setBranch(Tbranch value) {
            this.branch = value;
        }

        /**
         * Ruft den Wert der staffcompany-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tstaffcompany }
         *     
         */
        public Tstaffcompany getStaffcompany() {
            return staffcompany;
        }

        /**
         * Legt den Wert der staffcompany-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tstaffcompany }
         *     
         */
        public void setStaffcompany(Tstaffcompany value) {
            this.staffcompany = value;
        }

        /**
         * Ruft den Wert der staffgroup-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tstaffgroup }
         *     
         */
        public Tstaffgroup getStaffgroup() {
            return staffgroup;
        }

        /**
         * Legt den Wert der staffgroup-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tstaffgroup }
         *     
         */
        public void setStaffgroup(Tstaffgroup value) {
            this.staffgroup = value;
        }

        /**
         * Ruft den Wert der turnovercompany-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tturnovercompany }
         *     
         */
        public Tturnovercompany getTurnovercompany() {
            return turnovercompany;
        }

        /**
         * Legt den Wert der turnovercompany-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tturnovercompany }
         *     
         */
        public void setTurnovercompany(Tturnovercompany value) {
            this.turnovercompany = value;
        }

        /**
         * Ruft den Wert der turnovergroup-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tturnovergroup }
         *     
         */
        public Tturnovergroup getTurnovergroup() {
            return turnovergroup;
        }

        /**
         * Legt den Wert der turnovergroup-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tturnovergroup }
         *     
         */
        public void setTurnovergroup(Tturnovergroup value) {
            this.turnovergroup = value;
        }

        /**
         * Ruft den Wert der certificates-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCertificates() {
            return certificates;
        }

        /**
         * Legt den Wert der certificates-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCertificates(String value) {
            this.certificates = value;
        }

        /**
         * Ruft den Wert der balancesheet-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbalancesheet }
         *     
         */
        public Tbalancesheet getBalancesheet() {
            return balancesheet;
        }

        /**
         * Legt den Wert der balancesheet-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbalancesheet }
         *     
         */
        public void setBalancesheet(Tbalancesheet value) {
            this.balancesheet = value;
        }

        /**
         * Ruft den Wert der remarks-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getRemarks() {
            return remarks;
        }

        /**
         * Legt den Wert der remarks-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setRemarks(Ttext value) {
            this.remarks = value;
        }

        /**
         * Ruft den Wert der banks-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbanks }
         *     
         */
        public Tbanks getBanks() {
            return banks;
        }

        /**
         * Legt den Wert der banks-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbanks }
         *     
         */
        public void setBanks(Tbanks value) {
            this.banks = value;
        }

        /**
         * Ruft den Wert der enquirycounter-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tenquirycounter }
         *     
         */
        public Tenquirycounter getEnquirycounter() {
            return enquirycounter;
        }

        /**
         * Legt den Wert der enquirycounter-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tenquirycounter }
         *     
         */
        public void setEnquirycounter(Tenquirycounter value) {
            this.enquirycounter = value;
        }

        /**
         * Ruft den Wert der businessdevelopment-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbusinessdevelopment }
         *     
         */
        public Tbusinessdevelopment getBusinessdevelopment() {
            return businessdevelopment;
        }

        /**
         * Legt den Wert der businessdevelopment-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbusinessdevelopment }
         *     
         */
        public void setBusinessdevelopment(Tbusinessdevelopment value) {
            this.businessdevelopment = value;
        }

        /**
         * Ruft den Wert der updatenote-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getUpdatenote() {
            return updatenote;
        }

        /**
         * Legt den Wert der updatenote-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setUpdatenote(String value) {
            this.updatenote = value;
        }

        /**
         * Ruft den Wert der specificationofprofession-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tspecificationofprofession }
         *     
         */
        public Tspecificationofprofession getSpecificationofprofession() {
            return specificationofprofession;
        }

        /**
         * Legt den Wert der specificationofprofession-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tspecificationofprofession }
         *     
         */
        public void setSpecificationofprofession(Tspecificationofprofession value) {
            this.specificationofprofession = value;
        }

        /**
         * Ruft den Wert der ancillaryinformation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tancillaryinformation }
         *     
         */
        public Tancillaryinformation getAncillaryinformation() {
            return ancillaryinformation;
        }

        /**
         * Legt den Wert der ancillaryinformation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tancillaryinformation }
         *     
         */
        public void setAncillaryinformation(Tancillaryinformation value) {
            this.ancillaryinformation = value;
        }

        /**
         * Ruft den Wert der financialdata-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Treportresponsebody.Reportdata.Financialdata }
         *     
         */
        public Treportresponsebody.Reportdata.Financialdata getFinancialdata() {
            return financialdata;
        }

        /**
         * Legt den Wert der financialdata-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Treportresponsebody.Reportdata.Financialdata }
         *     
         */
        public void setFinancialdata(Treportresponsebody.Reportdata.Financialdata value) {
            this.financialdata = value;
        }

        /**
         * Ruft den Wert der businessarea-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbusinessarea }
         *     
         */
        public Tbusinessarea getBusinessarea() {
            return businessarea;
        }

        /**
         * Legt den Wert der businessarea-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbusinessarea }
         *     
         */
        public void setBusinessarea(Tbusinessarea value) {
            this.businessarea = value;
        }

        /**
         * Ruft den Wert der realestates-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Trealestates }
         *     
         */
        public Trealestates getRealestates() {
            return realestates;
        }

        /**
         * Legt den Wert der realestates-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Trealestates }
         *     
         */
        public void setRealestates(Trealestates value) {
            this.realestates = value;
        }

        /**
         * Ruft den Wert der importexport-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Timportexport }
         *     
         */
        public Timportexport getImportexport() {
            return importexport;
        }

        /**
         * Legt den Wert der importexport-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Timportexport }
         *     
         */
        public void setImportexport(Timportexport value) {
            this.importexport = value;
        }

        /**
         * Ruft den Wert der paymentmode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tpaymentmode }
         *     
         */
        public Tpaymentmode getPaymentmode() {
            return paymentmode;
        }

        /**
         * Legt den Wert der paymentmode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tpaymentmode }
         *     
         */
        public void setPaymentmode(Tpaymentmode value) {
            this.paymentmode = value;
        }

        /**
         * Ruft den Wert der creditopinion-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tcreditopinion }
         *     
         */
        public Tcreditopinion getCreditopinion() {
            return creditopinion;
        }

        /**
         * Legt den Wert der creditopinion-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tcreditopinion }
         *     
         */
        public void setCreditopinion(Tcreditopinion value) {
            this.creditopinion = value;
        }

        /**
         * Ruft den Wert der supplement-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tsupplement }
         *     
         */
        public Tsupplement getSupplement() {
            return supplement;
        }

        /**
         * Legt den Wert der supplement-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tsupplement }
         *     
         */
        public void setSupplement(Tsupplement value) {
            this.supplement = value;
        }

        /**
         * Ruft den Wert der negativefacts-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tnegativefact }
         *     
         */
        public Tnegativefact getNegativefacts() {
            return negativefacts;
        }

        /**
         * Legt den Wert der negativefacts-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tnegativefact }
         *     
         */
        public void setNegativefacts(Tnegativefact value) {
            this.negativefacts = value;
        }

        /**
         * Ruft den Wert der registerhistory-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tregisterhistory }
         *     
         */
        public Tregisterhistory getRegisterhistory() {
            return registerhistory;
        }

        /**
         * Legt den Wert der registerhistory-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tregisterhistory }
         *     
         */
        public void setRegisterhistory(Tregisterhistory value) {
            this.registerhistory = value;
        }

        /**
         * Ruft den Wert der locations-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tlocations }
         *     
         */
        public Tlocations getLocations() {
            return locations;
        }

        /**
         * Legt den Wert der locations-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tlocations }
         *     
         */
        public void setLocations(Tlocations value) {
            this.locations = value;
        }

        /**
         * Ruft den Wert der profitslosses-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tprofitslosses }
         *     
         */
        public Tprofitslosses getProfitslosses() {
            return profitslosses;
        }

        /**
         * Legt den Wert der profitslosses-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tprofitslosses }
         *     
         */
        public void setProfitslosses(Tprofitslosses value) {
            this.profitslosses = value;
        }

        /**
         * Ruft den Wert der financialstatusprivateperson-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tfinancialstatusprivateperson }
         *     
         */
        public Tfinancialstatusprivateperson getFinancialstatusprivateperson() {
            return financialstatusprivateperson;
        }

        /**
         * Legt den Wert der financialstatusprivateperson-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tfinancialstatusprivateperson }
         *     
         */
        public void setFinancialstatusprivateperson(Tfinancialstatusprivateperson value) {
            this.financialstatusprivateperson = value;
        }

        /**
         * Ruft den Wert der formerlocations-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tformerlocations }
         *     
         */
        public Tformerlocations getFormerlocations() {
            return formerlocations;
        }

        /**
         * Legt den Wert der formerlocations-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tformerlocations }
         *     
         */
        public void setFormerlocations(Tformerlocations value) {
            this.formerlocations = value;
        }

        /**
         * Ruft den Wert der affiliatedgroup-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Taffiliatedgroup }
         *     
         */
        public Taffiliatedgroup getAffiliatedgroup() {
            return affiliatedgroup;
        }

        /**
         * Legt den Wert der affiliatedgroup-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Taffiliatedgroup }
         *     
         */
        public void setAffiliatedgroup(Taffiliatedgroup value) {
            this.affiliatedgroup = value;
        }

        /**
         * Ruft den Wert der balancesheetgroup-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbalancesheetgroup }
         *     
         */
        public Tbalancesheetgroup getBalancesheetgroup() {
            return balancesheetgroup;
        }

        /**
         * Legt den Wert der balancesheetgroup-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbalancesheetgroup }
         *     
         */
        public void setBalancesheetgroup(Tbalancesheetgroup value) {
            this.balancesheetgroup = value;
        }

        /**
         * Ruft den Wert der profitslossesgroup-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tprofitslossesgroup }
         *     
         */
        public Tprofitslossesgroup getProfitslossesgroup() {
            return profitslossesgroup;
        }

        /**
         * Legt den Wert der profitslossesgroup-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tprofitslossesgroup }
         *     
         */
        public void setProfitslossesgroup(Tprofitslossesgroup value) {
            this.profitslossesgroup = value;
        }

        /**
         * Ruft den Wert der solvencyindexhistory-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tsolvencyindexhistory }
         *     
         */
        public Tsolvencyindexhistory getSolvencyindexhistory() {
            return solvencyindexhistory;
        }

        /**
         * Legt den Wert der solvencyindexhistory-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tsolvencyindexhistory }
         *     
         */
        public void setSolvencyindexhistory(Tsolvencyindexhistory value) {
            this.solvencyindexhistory = value;
        }

        /**
         * Ruft den Wert der solvencyfinanceindustry-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tsolvencyfinanceindustry }
         *     
         */
        public Tsolvencyfinanceindustry getSolvencyfinanceindustry() {
            return solvencyfinanceindustry;
        }

        /**
         * Legt den Wert der solvencyfinanceindustry-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tsolvencyfinanceindustry }
         *     
         */
        public void setSolvencyfinanceindustry(Tsolvencyfinanceindustry value) {
            this.solvencyfinanceindustry = value;
        }

        /**
         * Ruft den Wert der evaluationsfinanceindustry-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tevaluationsfinanceindustry }
         *     
         */
        public Tevaluationsfinanceindustry getEvaluationsfinanceindustry() {
            return evaluationsfinanceindustry;
        }

        /**
         * Legt den Wert der evaluationsfinanceindustry-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tevaluationsfinanceindustry }
         *     
         */
        public void setEvaluationsfinanceindustry(Tevaluationsfinanceindustry value) {
            this.evaluationsfinanceindustry = value;
        }

        /**
         * Ruft den Wert der balancesolvency-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbalancesolvency }
         *     
         */
        public Tbalancesolvency getBalancesolvency() {
            return balancesolvency;
        }

        /**
         * Legt den Wert der balancesolvency-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbalancesolvency }
         *     
         */
        public void setBalancesolvency(Tbalancesolvency value) {
            this.balancesolvency = value;
        }

        /**
         * Ruft den Wert der financialstatementfigures-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tfinancialstatementfigures }
         *     
         */
        public Tfinancialstatementfigures getFinancialstatementfigures() {
            return financialstatementfigures;
        }

        /**
         * Legt den Wert der financialstatementfigures-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tfinancialstatementfigures }
         *     
         */
        public void setFinancialstatementfigures(Tfinancialstatementfigures value) {
            this.financialstatementfigures = value;
        }

        /**
         * Ruft den Wert der financialstatementfiguresgroup-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tfinancialstatementfiguresgroup }
         *     
         */
        public Tfinancialstatementfiguresgroup getFinancialstatementfiguresgroup() {
            return financialstatementfiguresgroup;
        }

        /**
         * Legt den Wert der financialstatementfiguresgroup-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tfinancialstatementfiguresgroup }
         *     
         */
        public void setFinancialstatementfiguresgroup(Tfinancialstatementfiguresgroup value) {
            this.financialstatementfiguresgroup = value;
        }

        /**
         * Ruft den Wert der turnovercompanyrange-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tturnovercompanyrange }
         *     
         */
        public Tturnovercompanyrange getTurnovercompanyrange() {
            return turnovercompanyrange;
        }

        /**
         * Legt den Wert der turnovercompanyrange-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tturnovercompanyrange }
         *     
         */
        public void setTurnovercompanyrange(Tturnovercompanyrange value) {
            this.turnovercompanyrange = value;
        }

        /**
         * Ruft den Wert der staffcompanyrange-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tstaffcompanyrange }
         *     
         */
        public Tstaffcompanyrange getStaffcompanyrange() {
            return staffcompanyrange;
        }

        /**
         * Legt den Wert der staffcompanyrange-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tstaffcompanyrange }
         *     
         */
        public void setStaffcompanyrange(Tstaffcompanyrange value) {
            this.staffcompanyrange = value;
        }

        /**
         * Ruft den Wert der paymentbehaviour-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tpaymentbehaviour }
         *     
         */
        public Tpaymentbehaviour getPaymentbehaviour() {
            return paymentbehaviour;
        }

        /**
         * Legt den Wert der paymentbehaviour-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tpaymentbehaviour }
         *     
         */
        public void setPaymentbehaviour(Tpaymentbehaviour value) {
            this.paymentbehaviour = value;
        }

        /**
         * Ruft den Wert der financialinformation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tfinancialinformation }
         *     
         */
        public Tfinancialinformation getFinancialinformation() {
            return financialinformation;
        }

        /**
         * Legt den Wert der financialinformation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tfinancialinformation }
         *     
         */
        public void setFinancialinformation(Tfinancialinformation value) {
            this.financialinformation = value;
        }

        /**
         * Ruft den Wert der reporthints-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Treporthints }
         *     
         */
        public Treporthints getReporthints() {
            return reporthints;
        }

        /**
         * Legt den Wert der reporthints-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Treporthints }
         *     
         */
        public void setReporthints(Treporthints value) {
            this.reporthints = value;
        }

        /**
         * Ruft den Wert der riskjudgement-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Triskjudgement }
         *     
         */
        public Triskjudgement getRiskjudgement() {
            return riskjudgement;
        }

        /**
         * Legt den Wert der riskjudgement-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Triskjudgement }
         *     
         */
        public void setRiskjudgement(Triskjudgement value) {
            this.riskjudgement = value;
        }

        /**
         * Ruft den Wert der legaldisclaimer-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLegaldisclaimer() {
            return legaldisclaimer;
        }

        /**
         * Legt den Wert der legaldisclaimer-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLegaldisclaimer(String value) {
            this.legaldisclaimer = value;
        }

        /**
         * Ruft den Wert der textreport-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link DataHandler }
         *     
         */
        public DataHandler getTextreport() {
            return textreport;
        }

        /**
         * Legt den Wert der textreport-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link DataHandler }
         *     
         */
        public void setTextreport(DataHandler value) {
            this.textreport = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence minOccurs="0">
         *         &lt;element name="balance" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tbalance" maxOccurs="unbounded" minOccurs="0"/>
         *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "balance",
            "text"
        })
        public static class Financialdata {

            protected List<Tbalance> balance;
            protected Ttext text;

            /**
             * Gets the value of the balance property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the balance property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getBalance().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link Tbalance }
             * 
             * 
             */
            public List<Tbalance> getBalance() {
                if (balance == null) {
                    balance = new ArrayList<Tbalance>();
                }
                return this.balance;
            }

            /**
             * Ruft den Wert der text-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Ttext }
             *     
             */
            public Ttext getText() {
                return text;
            }

            /**
             * Legt den Wert der text-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Ttext }
             *     
             */
            public void setText(Ttext value) {
                this.text = value;
            }

        }

    }

}
