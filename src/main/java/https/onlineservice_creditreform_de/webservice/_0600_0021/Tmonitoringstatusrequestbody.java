
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tmonitoringstatusrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tmonitoringstatusrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pagereference" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="numberofentries">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *               &lt;minInclusive value="1"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;choice>
 *           &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}referencenumber"/>
 *           &lt;sequence>
 *             &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *             &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeyproduct" minOccurs="0"/>
 *             &lt;element name="orderperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tperiod" minOccurs="0"/>
 *             &lt;element name="standardmonitoringperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tperiod" minOccurs="0"/>
 *             &lt;element name="extendedmonitoringperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tperiod" minOccurs="0"/>
 *             &lt;element name="includestandardmonitoringnoextension" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *             &lt;element name="includeextendedmonitoringordered" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *             &lt;element name="includeextendedmonitoringplusordered" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *             &lt;element name="includeextendedmonitoringactive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *             &lt;element name="includeextendedmonitoringplusactive" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *           &lt;/sequence>
 *         &lt;/choice>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tmonitoringstatusrequestbody", propOrder = {
    "pagereference",
    "numberofentries",
    "referencenumber",
    "identificationnumber",
    "producttype",
    "orderperiod",
    "standardmonitoringperiod",
    "extendedmonitoringperiod",
    "includestandardmonitoringnoextension",
    "includeextendedmonitoringordered",
    "includeextendedmonitoringplusordered",
    "includeextendedmonitoringactive",
    "includeextendedmonitoringplusactive"
})
public class Tmonitoringstatusrequestbody {

    protected Long pagereference;
    protected int numberofentries;
    protected Long referencenumber;
    protected String identificationnumber;
    protected String producttype;
    protected Tperiod orderperiod;
    protected Tperiod standardmonitoringperiod;
    protected Tperiod extendedmonitoringperiod;
    protected Boolean includestandardmonitoringnoextension;
    protected Boolean includeextendedmonitoringordered;
    protected Boolean includeextendedmonitoringplusordered;
    protected Boolean includeextendedmonitoringactive;
    protected Boolean includeextendedmonitoringplusactive;

    /**
     * Ruft den Wert der pagereference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getPagereference() {
        return pagereference;
    }

    /**
     * Legt den Wert der pagereference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setPagereference(Long value) {
        this.pagereference = value;
    }

    /**
     * Ruft den Wert der numberofentries-Eigenschaft ab.
     * 
     */
    public int getNumberofentries() {
        return numberofentries;
    }

    /**
     * Legt den Wert der numberofentries-Eigenschaft fest.
     * 
     */
    public void setNumberofentries(int value) {
        this.numberofentries = value;
    }

    /**
     * Ruft den Wert der referencenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReferencenumber() {
        return referencenumber;
    }

    /**
     * Legt den Wert der referencenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReferencenumber(Long value) {
        this.referencenumber = value;
    }

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducttype(String value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der orderperiod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tperiod }
     *     
     */
    public Tperiod getOrderperiod() {
        return orderperiod;
    }

    /**
     * Legt den Wert der orderperiod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tperiod }
     *     
     */
    public void setOrderperiod(Tperiod value) {
        this.orderperiod = value;
    }

    /**
     * Ruft den Wert der standardmonitoringperiod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tperiod }
     *     
     */
    public Tperiod getStandardmonitoringperiod() {
        return standardmonitoringperiod;
    }

    /**
     * Legt den Wert der standardmonitoringperiod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tperiod }
     *     
     */
    public void setStandardmonitoringperiod(Tperiod value) {
        this.standardmonitoringperiod = value;
    }

    /**
     * Ruft den Wert der extendedmonitoringperiod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tperiod }
     *     
     */
    public Tperiod getExtendedmonitoringperiod() {
        return extendedmonitoringperiod;
    }

    /**
     * Legt den Wert der extendedmonitoringperiod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tperiod }
     *     
     */
    public void setExtendedmonitoringperiod(Tperiod value) {
        this.extendedmonitoringperiod = value;
    }

    /**
     * Ruft den Wert der includestandardmonitoringnoextension-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludestandardmonitoringnoextension() {
        return includestandardmonitoringnoextension;
    }

    /**
     * Legt den Wert der includestandardmonitoringnoextension-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludestandardmonitoringnoextension(Boolean value) {
        this.includestandardmonitoringnoextension = value;
    }

    /**
     * Ruft den Wert der includeextendedmonitoringordered-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeextendedmonitoringordered() {
        return includeextendedmonitoringordered;
    }

    /**
     * Legt den Wert der includeextendedmonitoringordered-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeextendedmonitoringordered(Boolean value) {
        this.includeextendedmonitoringordered = value;
    }

    /**
     * Ruft den Wert der includeextendedmonitoringplusordered-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeextendedmonitoringplusordered() {
        return includeextendedmonitoringplusordered;
    }

    /**
     * Legt den Wert der includeextendedmonitoringplusordered-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeextendedmonitoringplusordered(Boolean value) {
        this.includeextendedmonitoringplusordered = value;
    }

    /**
     * Ruft den Wert der includeextendedmonitoringactive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeextendedmonitoringactive() {
        return includeextendedmonitoringactive;
    }

    /**
     * Legt den Wert der includeextendedmonitoringactive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeextendedmonitoringactive(Boolean value) {
        this.includeextendedmonitoringactive = value;
    }

    /**
     * Ruft den Wert der includeextendedmonitoringplusactive-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIncludeextendedmonitoringplusactive() {
        return includeextendedmonitoringplusactive;
    }

    /**
     * Legt den Wert der includeextendedmonitoringplusactive-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIncludeextendedmonitoringplusactive(Boolean value) {
        this.includeextendedmonitoringplusactive = value;
    }

}
