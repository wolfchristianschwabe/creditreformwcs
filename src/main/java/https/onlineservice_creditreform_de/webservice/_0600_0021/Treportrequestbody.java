
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Treportrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Treportrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber"/>
 *         &lt;element name="legitimateinterest" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeylegitimateinterest"/>
 *         &lt;element name="reportlanguage" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlanguagerequest"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeyproduct"/>
 *         &lt;element name="solvencyindexthreshold" minOccurs="0">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}integer">
 *               &lt;minInclusive value="0"/>
 *               &lt;maxInclusive value="600"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}requestmonitoringstructure" minOccurs="0"/>
 *         &lt;element name="replacereferencenumber" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Treferencenumber" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Treportrequestbody", propOrder = {
    "identificationnumber",
    "legitimateinterest",
    "reportlanguage",
    "producttype",
    "solvencyindexthreshold",
    "customerreference",
    "extendedmonitoring",
    "replacereferencenumber"
})
public class Treportrequestbody {

    @XmlElement(required = true)
    protected String identificationnumber;
    @XmlElement(required = true)
    protected String legitimateinterest;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected Tlanguagerequest reportlanguage;
    @XmlElement(required = true)
    protected String producttype;
    protected Integer solvencyindexthreshold;
    protected String customerreference;
    protected Treportrequestbody.Extendedmonitoring extendedmonitoring;
    protected Long replacereferencenumber;

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der legitimateinterest-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegitimateinterest() {
        return legitimateinterest;
    }

    /**
     * Legt den Wert der legitimateinterest-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegitimateinterest(String value) {
        this.legitimateinterest = value;
    }

    /**
     * Ruft den Wert der reportlanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tlanguagerequest }
     *     
     */
    public Tlanguagerequest getReportlanguage() {
        return reportlanguage;
    }

    /**
     * Legt den Wert der reportlanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tlanguagerequest }
     *     
     */
    public void setReportlanguage(Tlanguagerequest value) {
        this.reportlanguage = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducttype(String value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der solvencyindexthreshold-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSolvencyindexthreshold() {
        return solvencyindexthreshold;
    }

    /**
     * Legt den Wert der solvencyindexthreshold-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSolvencyindexthreshold(Integer value) {
        this.solvencyindexthreshold = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der extendedmonitoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Treportrequestbody.Extendedmonitoring }
     *     
     */
    public Treportrequestbody.Extendedmonitoring getExtendedmonitoring() {
        return extendedmonitoring;
    }

    /**
     * Legt den Wert der extendedmonitoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Treportrequestbody.Extendedmonitoring }
     *     
     */
    public void setExtendedmonitoring(Treportrequestbody.Extendedmonitoring value) {
        this.extendedmonitoring = value;
    }

    /**
     * Ruft den Wert der replacereferencenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getReplacereferencenumber() {
        return replacereferencenumber;
    }

    /**
     * Legt den Wert der replacereferencenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setReplacereferencenumber(Long value) {
        this.replacereferencenumber = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="endofextendedmonitoring" type="{http://www.w3.org/2001/XMLSchema}gYearMonth" minOccurs="0"/>
     *         &lt;element name="extendedmonitoringplus" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "endofextendedmonitoring",
        "extendedmonitoringplus"
    })
    public static class Extendedmonitoring {

        @XmlSchemaType(name = "gYearMonth")
        protected XMLGregorianCalendar endofextendedmonitoring;
        protected boolean extendedmonitoringplus;

        /**
         * Ruft den Wert der endofextendedmonitoring-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEndofextendedmonitoring() {
            return endofextendedmonitoring;
        }

        /**
         * Legt den Wert der endofextendedmonitoring-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEndofextendedmonitoring(XMLGregorianCalendar value) {
            this.endofextendedmonitoring = value;
        }

        /**
         * Ruft den Wert der extendedmonitoringplus-Eigenschaft ab.
         * 
         */
        public boolean isExtendedmonitoringplus() {
            return extendedmonitoringplus;
        }

        /**
         * Legt den Wert der extendedmonitoringplus-Eigenschaft fest.
         * 
         */
        public void setExtendedmonitoringplus(boolean value) {
            this.extendedmonitoringplus = value;
        }

    }

}
