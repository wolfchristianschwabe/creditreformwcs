
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tdeputymanagementbasic complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tdeputymanagementbasic">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="participantcapacities" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipantcapacitiesbasic" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tdeputymanagementbasic", propOrder = {
    "participantcapacities"
})
public class Tdeputymanagementbasic {

    protected List<Tparticipantcapacitiesbasic> participantcapacities;

    /**
     * Gets the value of the participantcapacities property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the participantcapacities property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getParticipantcapacities().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tparticipantcapacitiesbasic }
     * 
     * 
     */
    public List<Tparticipantcapacitiesbasic> getParticipantcapacities() {
        if (participantcapacities == null) {
            participantcapacities = new ArrayList<Tparticipantcapacitiesbasic>();
        }
        return this.participantcapacities;
    }

}
