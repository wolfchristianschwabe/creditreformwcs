
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tcgcollectiondata complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcgcollectiondata">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="totalnumber" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="numberregular" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="numberlongterm" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *         &lt;element name="regular" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="case" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="mainreceivable" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *                             &lt;element name="status" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="longterm" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="case" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="mainreceivable" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *                             &lt;element name="status" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcgcollectiondata", propOrder = {
    "totalnumber",
    "numberregular",
    "numberlongterm",
    "regular",
    "longterm"
})
public class Tcgcollectiondata {

    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger totalnumber;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger numberregular;
    @XmlSchemaType(name = "nonNegativeInteger")
    protected BigInteger numberlongterm;
    protected Tcgcollectiondata.Regular regular;
    protected Tcgcollectiondata.Longterm longterm;

    /**
     * Ruft den Wert der totalnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getTotalnumber() {
        return totalnumber;
    }

    /**
     * Legt den Wert der totalnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setTotalnumber(BigInteger value) {
        this.totalnumber = value;
    }

    /**
     * Ruft den Wert der numberregular-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberregular() {
        return numberregular;
    }

    /**
     * Legt den Wert der numberregular-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberregular(BigInteger value) {
        this.numberregular = value;
    }

    /**
     * Ruft den Wert der numberlongterm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNumberlongterm() {
        return numberlongterm;
    }

    /**
     * Legt den Wert der numberlongterm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNumberlongterm(BigInteger value) {
        this.numberlongterm = value;
    }

    /**
     * Ruft den Wert der regular-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgcollectiondata.Regular }
     *     
     */
    public Tcgcollectiondata.Regular getRegular() {
        return regular;
    }

    /**
     * Legt den Wert der regular-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgcollectiondata.Regular }
     *     
     */
    public void setRegular(Tcgcollectiondata.Regular value) {
        this.regular = value;
    }

    /**
     * Ruft den Wert der longterm-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgcollectiondata.Longterm }
     *     
     */
    public Tcgcollectiondata.Longterm getLongterm() {
        return longterm;
    }

    /**
     * Legt den Wert der longterm-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgcollectiondata.Longterm }
     *     
     */
    public void setLongterm(Tcgcollectiondata.Longterm value) {
        this.longterm = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="case" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="mainreceivable" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
     *                   &lt;element name="status" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "_case"
    })
    public static class Longterm {

        @XmlElement(name = "case")
        protected List<Tcgcollectiondata.Longterm.Case> _case;

        /**
         * Gets the value of the case property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the case property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCase().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tcgcollectiondata.Longterm.Case }
         * 
         * 
         */
        public List<Tcgcollectiondata.Longterm.Case> getCase() {
            if (_case == null) {
                _case = new ArrayList<Tcgcollectiondata.Longterm.Case>();
            }
            return this._case;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="mainreceivable" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
         *         &lt;element name="status" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mainreceivable",
            "status",
            "date"
        })
        public static class Case {

            protected Tamount mainreceivable;
            protected Tkey status;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar date;

            /**
             * Ruft den Wert der mainreceivable-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tamount }
             *     
             */
            public Tamount getMainreceivable() {
                return mainreceivable;
            }

            /**
             * Legt den Wert der mainreceivable-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tamount }
             *     
             */
            public void setMainreceivable(Tamount value) {
                this.mainreceivable = value;
            }

            /**
             * Ruft den Wert der status-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getStatus() {
                return status;
            }

            /**
             * Legt den Wert der status-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setStatus(Tkey value) {
                this.status = value;
            }

            /**
             * Ruft den Wert der date-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDate() {
                return date;
            }

            /**
             * Legt den Wert der date-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDate(XMLGregorianCalendar value) {
                this.date = value;
            }

        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="case" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="mainreceivable" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
     *                   &lt;element name="status" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "_case"
    })
    public static class Regular {

        @XmlElement(name = "case")
        protected List<Tcgcollectiondata.Regular.Case> _case;

        /**
         * Gets the value of the case property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the case property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getCase().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tcgcollectiondata.Regular.Case }
         * 
         * 
         */
        public List<Tcgcollectiondata.Regular.Case> getCase() {
            if (_case == null) {
                _case = new ArrayList<Tcgcollectiondata.Regular.Case>();
            }
            return this._case;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="mainreceivable" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
         *         &lt;element name="status" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "mainreceivable",
            "status",
            "date"
        })
        public static class Case {

            protected Tamount mainreceivable;
            protected Tkey status;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar date;

            /**
             * Ruft den Wert der mainreceivable-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tamount }
             *     
             */
            public Tamount getMainreceivable() {
                return mainreceivable;
            }

            /**
             * Legt den Wert der mainreceivable-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tamount }
             *     
             */
            public void setMainreceivable(Tamount value) {
                this.mainreceivable = value;
            }

            /**
             * Ruft den Wert der status-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getStatus() {
                return status;
            }

            /**
             * Legt den Wert der status-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setStatus(Tkey value) {
                this.status = value;
            }

            /**
             * Ruft den Wert der date-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDate() {
                return date;
            }

            /**
             * Legt den Wert der date-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDate(XMLGregorianCalendar value) {
                this.date = value;
            }

        }

    }

}
