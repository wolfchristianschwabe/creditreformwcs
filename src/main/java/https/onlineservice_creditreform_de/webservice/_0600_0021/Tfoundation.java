
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tfoundation complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tfoundation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="riskofagevalue" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="riskofagetext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="dateoffirstlegalform" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="businessactivitysince" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;element name="businessactivitysincedecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="dateoffoundation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;element name="ageofcompany" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="firstlegalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="probabilityofdefault" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="duetoage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="explanation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tfoundation", propOrder = {
    "riskofagevalue",
    "riskofagetext",
    "dateoffirstlegalform",
    "businessactivitysince",
    "businessactivitysincedecoration",
    "dateoffoundation",
    "ageofcompany",
    "firstlegalform",
    "text",
    "probabilityofdefault",
    "trafficlight"
})
public class Tfoundation {

    protected BigDecimal riskofagevalue;
    protected String riskofagetext;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateoffirstlegalform;
    protected String businessactivitysince;
    protected Tkey businessactivitysincedecoration;
    protected String dateoffoundation;
    protected Integer ageofcompany;
    protected Tkeywithshortdesignation firstlegalform;
    protected Ttext text;
    protected Tfoundation.Probabilityofdefault probabilityofdefault;
    protected Tkeywithgrade trafficlight;

    /**
     * Ruft den Wert der riskofagevalue-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRiskofagevalue() {
        return riskofagevalue;
    }

    /**
     * Legt den Wert der riskofagevalue-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRiskofagevalue(BigDecimal value) {
        this.riskofagevalue = value;
    }

    /**
     * Ruft den Wert der riskofagetext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskofagetext() {
        return riskofagetext;
    }

    /**
     * Legt den Wert der riskofagetext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskofagetext(String value) {
        this.riskofagetext = value;
    }

    /**
     * Ruft den Wert der dateoffirstlegalform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateoffirstlegalform() {
        return dateoffirstlegalform;
    }

    /**
     * Legt den Wert der dateoffirstlegalform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateoffirstlegalform(XMLGregorianCalendar value) {
        this.dateoffirstlegalform = value;
    }

    /**
     * Ruft den Wert der businessactivitysince-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessactivitysince() {
        return businessactivitysince;
    }

    /**
     * Legt den Wert der businessactivitysince-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessactivitysince(String value) {
        this.businessactivitysince = value;
    }

    /**
     * Ruft den Wert der businessactivitysincedecoration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getBusinessactivitysincedecoration() {
        return businessactivitysincedecoration;
    }

    /**
     * Legt den Wert der businessactivitysincedecoration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setBusinessactivitysincedecoration(Tkey value) {
        this.businessactivitysincedecoration = value;
    }

    /**
     * Ruft den Wert der dateoffoundation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateoffoundation() {
        return dateoffoundation;
    }

    /**
     * Legt den Wert der dateoffoundation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateoffoundation(String value) {
        this.dateoffoundation = value;
    }

    /**
     * Ruft den Wert der ageofcompany-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getAgeofcompany() {
        return ageofcompany;
    }

    /**
     * Legt den Wert der ageofcompany-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setAgeofcompany(Integer value) {
        this.ageofcompany = value;
    }

    /**
     * Ruft den Wert der firstlegalform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithshortdesignation }
     *     
     */
    public Tkeywithshortdesignation getFirstlegalform() {
        return firstlegalform;
    }

    /**
     * Legt den Wert der firstlegalform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithshortdesignation }
     *     
     */
    public void setFirstlegalform(Tkeywithshortdesignation value) {
        this.firstlegalform = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

    /**
     * Ruft den Wert der probabilityofdefault-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tfoundation.Probabilityofdefault }
     *     
     */
    public Tfoundation.Probabilityofdefault getProbabilityofdefault() {
        return probabilityofdefault;
    }

    /**
     * Legt den Wert der probabilityofdefault-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tfoundation.Probabilityofdefault }
     *     
     */
    public void setProbabilityofdefault(Tfoundation.Probabilityofdefault value) {
        this.probabilityofdefault = value;
    }

    /**
     * Ruft den Wert der trafficlight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getTrafficlight() {
        return trafficlight;
    }

    /**
     * Legt den Wert der trafficlight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setTrafficlight(Tkeywithgrade value) {
        this.trafficlight = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="duetoage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="explanation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "date",
        "duetoage",
        "explanation"
    })
    public static class Probabilityofdefault {

        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar date;
        protected BigDecimal duetoage;
        protected String explanation;

        /**
         * Ruft den Wert der date-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDate() {
            return date;
        }

        /**
         * Legt den Wert der date-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDate(XMLGregorianCalendar value) {
            this.date = value;
        }

        /**
         * Ruft den Wert der duetoage-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getDuetoage() {
            return duetoage;
        }

        /**
         * Legt den Wert der duetoage-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setDuetoage(BigDecimal value) {
            this.duetoage = value;
        }

        /**
         * Ruft den Wert der explanation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getExplanation() {
            return explanation;
        }

        /**
         * Legt den Wert der explanation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setExplanation(String value) {
            this.explanation = value;
        }

    }

}
