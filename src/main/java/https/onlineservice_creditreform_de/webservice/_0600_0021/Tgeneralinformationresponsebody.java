
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tgeneralinformationresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tgeneralinformationresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="generaldata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tgeneraldata"/>
 *         &lt;element name="debtor" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionresponsedebtor"/>
 *         &lt;element name="receivableposition" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Treceivableposition"/>
 *         &lt;element name="negativefacts" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionnegativefacts"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tgeneralinformationresponsebody", propOrder = {
    "generaldata",
    "debtor",
    "receivableposition",
    "negativefacts",
    "text"
})
public class Tgeneralinformationresponsebody {

    @XmlElement(required = true)
    protected Tgeneraldata generaldata;
    @XmlElement(required = true)
    protected Tcollectionresponsedebtor debtor;
    @XmlElement(required = true)
    protected Treceivableposition receivableposition;
    @XmlElement(required = true)
    protected Tcollectionnegativefacts negativefacts;
    protected Ttext text;

    /**
     * Ruft den Wert der generaldata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tgeneraldata }
     *     
     */
    public Tgeneraldata getGeneraldata() {
        return generaldata;
    }

    /**
     * Legt den Wert der generaldata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tgeneraldata }
     *     
     */
    public void setGeneraldata(Tgeneraldata value) {
        this.generaldata = value;
    }

    /**
     * Ruft den Wert der debtor-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionresponsedebtor }
     *     
     */
    public Tcollectionresponsedebtor getDebtor() {
        return debtor;
    }

    /**
     * Legt den Wert der debtor-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionresponsedebtor }
     *     
     */
    public void setDebtor(Tcollectionresponsedebtor value) {
        this.debtor = value;
    }

    /**
     * Ruft den Wert der receivableposition-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Treceivableposition }
     *     
     */
    public Treceivableposition getReceivableposition() {
        return receivableposition;
    }

    /**
     * Legt den Wert der receivableposition-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Treceivableposition }
     *     
     */
    public void setReceivableposition(Treceivableposition value) {
        this.receivableposition = value;
    }

    /**
     * Ruft den Wert der negativefacts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionnegativefacts }
     *     
     */
    public Tcollectionnegativefacts getNegativefacts() {
        return negativefacts;
    }

    /**
     * Legt den Wert der negativefacts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionnegativefacts }
     *     
     */
    public void setNegativefacts(Tcollectionnegativefacts value) {
        this.negativefacts = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

}
