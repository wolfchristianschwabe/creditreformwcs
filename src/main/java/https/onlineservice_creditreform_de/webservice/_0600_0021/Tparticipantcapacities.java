
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipantcapacities complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipantcapacities">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capacity" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="active" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipantextended" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="former" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tparticipant" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipantcapacities", propOrder = {
    "capacity",
    "active",
    "former",
    "text"
})
public class Tparticipantcapacities {

    protected Tkey capacity;
    protected List<Tparticipantextended> active;
    protected List<Tparticipant> former;
    protected Ttext text;

    /**
     * Ruft den Wert der capacity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCapacity() {
        return capacity;
    }

    /**
     * Legt den Wert der capacity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCapacity(Tkey value) {
        this.capacity = value;
    }

    /**
     * Gets the value of the active property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the active property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActive().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tparticipantextended }
     * 
     * 
     */
    public List<Tparticipantextended> getActive() {
        if (active == null) {
            active = new ArrayList<Tparticipantextended>();
        }
        return this.active;
    }

    /**
     * Gets the value of the former property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the former property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tparticipant }
     * 
     * 
     */
    public List<Tparticipant> getFormer() {
        if (former == null) {
            former = new ArrayList<Tparticipant>();
        }
        return this.former;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

}
