
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipation complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipation">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="furthermastercrefos" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="mastercrefo" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}status" minOccurs="0"/>
 *                   &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
 *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
 *                   &lt;element name="warnings" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Twarning" minOccurs="0"/>
 *                   &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *                   &lt;element name="activeparticipations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tshare" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="formerparticipations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tshare" maxOccurs="unbounded" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipation", propOrder = {
    "furthermastercrefos",
    "mastercrefo",
    "text"
})
@XmlSeeAlso({
    https.onlineservice_creditreform_de.webservice._0600_0021.Tparticipationsparticipants.Participation.class
})
public class Tparticipation {

    protected Boolean furthermastercrefos;
    protected List<Tparticipation.Mastercrefo> mastercrefo;
    protected Ttext text;

    /**
     * Ruft den Wert der furthermastercrefos-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFurthermastercrefos() {
        return furthermastercrefos;
    }

    /**
     * Legt den Wert der furthermastercrefos-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFurthermastercrefos(Boolean value) {
        this.furthermastercrefos = value;
    }

    /**
     * Gets the value of the mastercrefo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mastercrefo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMastercrefo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tparticipation.Mastercrefo }
     * 
     * 
     */
    public List<Tparticipation.Mastercrefo> getMastercrefo() {
        if (mastercrefo == null) {
            mastercrefo = new ArrayList<Tparticipation.Mastercrefo>();
        }
        return this.mastercrefo;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}status" minOccurs="0"/>
     *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
     *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}companyaddressstructure"/>
     *         &lt;element name="warnings" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Twarning" minOccurs="0"/>
     *         &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
     *         &lt;element name="activeparticipations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tshare" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="formerparticipations" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tshare" maxOccurs="unbounded" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificationnumber",
        "easynumber",
        "status",
        "legalform",
        "salutation",
        "companyname",
        "tradename",
        "commercialname",
        "alias",
        "street",
        "housenumber",
        "housenumberaffix",
        "postcode",
        "city",
        "quarter",
        "region",
        "country",
        "warnings",
        "trafficlight",
        "activeparticipations",
        "formerparticipations"
    })
    public static class Mastercrefo {

        protected String identificationnumber;
        protected String easynumber;
        protected Tkeywithdescription status;
        protected Tkeywithshortdesignation legalform;
        protected Tkey salutation;
        protected String companyname;
        protected String tradename;
        protected String commercialname;
        protected String alias;
        protected String street;
        protected Integer housenumber;
        protected String housenumberaffix;
        protected String postcode;
        protected String city;
        protected String quarter;
        protected String region;
        protected Tkey country;
        protected Twarning warnings;
        protected Tkeywithgrade trafficlight;
        protected List<Tshare> activeparticipations;
        protected List<Tshare> formerparticipations;

        /**
         * Ruft den Wert der identificationnumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdentificationnumber() {
            return identificationnumber;
        }

        /**
         * Legt den Wert der identificationnumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdentificationnumber(String value) {
            this.identificationnumber = value;
        }

        /**
         * Ruft den Wert der easynumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEasynumber() {
            return easynumber;
        }

        /**
         * Legt den Wert der easynumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEasynumber(String value) {
            this.easynumber = value;
        }

        /**
         * Ruft den Wert der status-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithdescription }
         *     
         */
        public Tkeywithdescription getStatus() {
            return status;
        }

        /**
         * Legt den Wert der status-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithdescription }
         *     
         */
        public void setStatus(Tkeywithdescription value) {
            this.status = value;
        }

        /**
         * Ruft den Wert der legalform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public Tkeywithshortdesignation getLegalform() {
            return legalform;
        }

        /**
         * Legt den Wert der legalform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public void setLegalform(Tkeywithshortdesignation value) {
            this.legalform = value;
        }

        /**
         * Ruft den Wert der salutation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getSalutation() {
            return salutation;
        }

        /**
         * Legt den Wert der salutation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setSalutation(Tkey value) {
            this.salutation = value;
        }

        /**
         * Ruft den Wert der companyname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyname() {
            return companyname;
        }

        /**
         * Legt den Wert der companyname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyname(String value) {
            this.companyname = value;
        }

        /**
         * Ruft den Wert der tradename-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTradename() {
            return tradename;
        }

        /**
         * Legt den Wert der tradename-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTradename(String value) {
            this.tradename = value;
        }

        /**
         * Ruft den Wert der commercialname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCommercialname() {
            return commercialname;
        }

        /**
         * Legt den Wert der commercialname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCommercialname(String value) {
            this.commercialname = value;
        }

        /**
         * Ruft den Wert der alias-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getAlias() {
            return alias;
        }

        /**
         * Legt den Wert der alias-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setAlias(String value) {
            this.alias = value;
        }

        /**
         * Ruft den Wert der street-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreet() {
            return street;
        }

        /**
         * Legt den Wert der street-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreet(String value) {
            this.street = value;
        }

        /**
         * Ruft den Wert der housenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getHousenumber() {
            return housenumber;
        }

        /**
         * Legt den Wert der housenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setHousenumber(Integer value) {
            this.housenumber = value;
        }

        /**
         * Ruft den Wert der housenumberaffix-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHousenumberaffix() {
            return housenumberaffix;
        }

        /**
         * Legt den Wert der housenumberaffix-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHousenumberaffix(String value) {
            this.housenumberaffix = value;
        }

        /**
         * Ruft den Wert der postcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostcode() {
            return postcode;
        }

        /**
         * Legt den Wert der postcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostcode(String value) {
            this.postcode = value;
        }

        /**
         * Ruft den Wert der city-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Legt den Wert der city-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Ruft den Wert der quarter-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuarter() {
            return quarter;
        }

        /**
         * Legt den Wert der quarter-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuarter(String value) {
            this.quarter = value;
        }

        /**
         * Ruft den Wert der region-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegion() {
            return region;
        }

        /**
         * Legt den Wert der region-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegion(String value) {
            this.region = value;
        }

        /**
         * Ruft den Wert der country-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getCountry() {
            return country;
        }

        /**
         * Legt den Wert der country-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setCountry(Tkey value) {
            this.country = value;
        }

        /**
         * Ruft den Wert der warnings-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Twarning }
         *     
         */
        public Twarning getWarnings() {
            return warnings;
        }

        /**
         * Legt den Wert der warnings-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Twarning }
         *     
         */
        public void setWarnings(Twarning value) {
            this.warnings = value;
        }

        /**
         * Ruft den Wert der trafficlight-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithgrade }
         *     
         */
        public Tkeywithgrade getTrafficlight() {
            return trafficlight;
        }

        /**
         * Legt den Wert der trafficlight-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithgrade }
         *     
         */
        public void setTrafficlight(Tkeywithgrade value) {
            this.trafficlight = value;
        }

        /**
         * Gets the value of the activeparticipations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the activeparticipations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getActiveparticipations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tshare }
         * 
         * 
         */
        public List<Tshare> getActiveparticipations() {
            if (activeparticipations == null) {
                activeparticipations = new ArrayList<Tshare>();
            }
            return this.activeparticipations;
        }

        /**
         * Gets the value of the formerparticipations property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the formerparticipations property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFormerparticipations().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tshare }
         * 
         * 
         */
        public List<Tshare> getFormerparticipations() {
            if (formerparticipations == null) {
                formerparticipations = new ArrayList<Tshare>();
            }
            return this.formerparticipations;
        }

    }

}
