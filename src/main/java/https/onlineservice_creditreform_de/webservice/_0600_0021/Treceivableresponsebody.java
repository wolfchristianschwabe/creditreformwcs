
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Treceivableresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Treceivableresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="currency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="turnover" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="collectionturnovertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="dateinvoice" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="invoicenumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="datedue" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="datevaluta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="receivablereason" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="dateservicedeliveryperiodfrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="dateservicedeliveryperioduntil" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="datecontract" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Treceivableresponsebody", propOrder = {
    "currency",
    "turnover",
    "text"
})
public class Treceivableresponsebody {

    protected Tkey currency;
    protected List<Treceivableresponsebody.Turnover> turnover;
    protected Ttext text;

    /**
     * Ruft den Wert der currency-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCurrency() {
        return currency;
    }

    /**
     * Legt den Wert der currency-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCurrency(Tkey value) {
        this.currency = value;
    }

    /**
     * Gets the value of the turnover property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the turnover property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTurnover().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Treceivableresponsebody.Turnover }
     * 
     * 
     */
    public List<Treceivableresponsebody.Turnover> getTurnover() {
        if (turnover == null) {
            turnover = new ArrayList<Treceivableresponsebody.Turnover>();
        }
        return this.turnover;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="collectionturnovertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="dateinvoice" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="invoicenumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="datedue" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="datevaluta" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="receivablereason" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="dateservicedeliveryperiodfrom" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="dateservicedeliveryperioduntil" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="datecontract" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "collectionturnovertype",
        "dateinvoice",
        "invoicenumber",
        "datedue",
        "datevaluta",
        "receivablereason",
        "amount",
        "dateservicedeliveryperiodfrom",
        "dateservicedeliveryperioduntil",
        "datecontract",
        "text"
    })
    public static class Turnover {

        protected Tkey collectionturnovertype;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateinvoice;
        protected String invoicenumber;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datedue;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datevaluta;
        protected Tkey receivablereason;
        protected BigDecimal amount;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateservicedeliveryperiodfrom;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar dateservicedeliveryperioduntil;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datecontract;
        protected Ttext text;

        /**
         * Ruft den Wert der collectionturnovertype-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getCollectionturnovertype() {
            return collectionturnovertype;
        }

        /**
         * Legt den Wert der collectionturnovertype-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setCollectionturnovertype(Tkey value) {
            this.collectionturnovertype = value;
        }

        /**
         * Ruft den Wert der dateinvoice-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateinvoice() {
            return dateinvoice;
        }

        /**
         * Legt den Wert der dateinvoice-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateinvoice(XMLGregorianCalendar value) {
            this.dateinvoice = value;
        }

        /**
         * Ruft den Wert der invoicenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getInvoicenumber() {
            return invoicenumber;
        }

        /**
         * Legt den Wert der invoicenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setInvoicenumber(String value) {
            this.invoicenumber = value;
        }

        /**
         * Ruft den Wert der datedue-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDatedue() {
            return datedue;
        }

        /**
         * Legt den Wert der datedue-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDatedue(XMLGregorianCalendar value) {
            this.datedue = value;
        }

        /**
         * Ruft den Wert der datevaluta-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDatevaluta() {
            return datevaluta;
        }

        /**
         * Legt den Wert der datevaluta-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDatevaluta(XMLGregorianCalendar value) {
            this.datevaluta = value;
        }

        /**
         * Ruft den Wert der receivablereason-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getReceivablereason() {
            return receivablereason;
        }

        /**
         * Legt den Wert der receivablereason-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setReceivablereason(Tkey value) {
            this.receivablereason = value;
        }

        /**
         * Ruft den Wert der amount-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Legt den Wert der amount-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

        /**
         * Ruft den Wert der dateservicedeliveryperiodfrom-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateservicedeliveryperiodfrom() {
            return dateservicedeliveryperiodfrom;
        }

        /**
         * Legt den Wert der dateservicedeliveryperiodfrom-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateservicedeliveryperiodfrom(XMLGregorianCalendar value) {
            this.dateservicedeliveryperiodfrom = value;
        }

        /**
         * Ruft den Wert der dateservicedeliveryperioduntil-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDateservicedeliveryperioduntil() {
            return dateservicedeliveryperioduntil;
        }

        /**
         * Legt den Wert der dateservicedeliveryperioduntil-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDateservicedeliveryperioduntil(XMLGregorianCalendar value) {
            this.dateservicedeliveryperioduntil = value;
        }

        /**
         * Ruft den Wert der datecontract-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDatecontract() {
            return datecontract;
        }

        /**
         * Legt den Wert der datecontract-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDatecontract(XMLGregorianCalendar value) {
            this.datecontract = value;
        }

        /**
         * Ruft den Wert der text-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getText() {
            return text;
        }

        /**
         * Legt den Wert der text-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setText(Ttext value) {
            this.text = value;
        }

    }

}
