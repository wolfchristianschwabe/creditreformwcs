
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcapacitiesshareholderbasic complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcapacitiesshareholderbasic">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="capacity" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="active" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tshareholder" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="other" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *                   &lt;element name="numberdecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="capitalshare" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamountpercent" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="sharecapital" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsharecapital" minOccurs="0"/>
 *         &lt;element name="former" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tshareholder" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcapacitiesshareholderbasic", propOrder = {
    "capacity",
    "active",
    "other",
    "sharecapital",
    "former",
    "text"
})
public class Tcapacitiesshareholderbasic {

    protected Tkey capacity;
    protected List<Tshareholder> active;
    protected Tcapacitiesshareholderbasic.Other other;
    protected Tsharecapital sharecapital;
    protected List<Tshareholder> former;
    protected Ttext text;

    /**
     * Ruft den Wert der capacity-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCapacity() {
        return capacity;
    }

    /**
     * Legt den Wert der capacity-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCapacity(Tkey value) {
        this.capacity = value;
    }

    /**
     * Gets the value of the active property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the active property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getActive().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tshareholder }
     * 
     * 
     */
    public List<Tshareholder> getActive() {
        if (active == null) {
            active = new ArrayList<Tshareholder>();
        }
        return this.active;
    }

    /**
     * Ruft den Wert der other-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcapacitiesshareholderbasic.Other }
     *     
     */
    public Tcapacitiesshareholderbasic.Other getOther() {
        return other;
    }

    /**
     * Legt den Wert der other-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcapacitiesshareholderbasic.Other }
     *     
     */
    public void setOther(Tcapacitiesshareholderbasic.Other value) {
        this.other = value;
    }

    /**
     * Ruft den Wert der sharecapital-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tsharecapital }
     *     
     */
    public Tsharecapital getSharecapital() {
        return sharecapital;
    }

    /**
     * Legt den Wert der sharecapital-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tsharecapital }
     *     
     */
    public void setSharecapital(Tsharecapital value) {
        this.sharecapital = value;
    }

    /**
     * Gets the value of the former property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the former property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tshareholder }
     * 
     * 
     */
    public List<Tshareholder> getFormer() {
        if (former == null) {
            former = new ArrayList<Tshareholder>();
        }
        return this.former;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
     *         &lt;element name="numberdecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="capitalshare" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamountpercent" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "number",
        "numberdecoration",
        "capitalshare"
    })
    public static class Other {

        protected Integer number;
        protected Tkey numberdecoration;
        protected Tamountpercent capitalshare;

        /**
         * Ruft den Wert der number-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNumber() {
            return number;
        }

        /**
         * Legt den Wert der number-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNumber(Integer value) {
            this.number = value;
        }

        /**
         * Ruft den Wert der numberdecoration-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getNumberdecoration() {
            return numberdecoration;
        }

        /**
         * Legt den Wert der numberdecoration-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setNumberdecoration(Tkey value) {
            this.numberdecoration = value;
        }

        /**
         * Ruft den Wert der capitalshare-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tamountpercent }
         *     
         */
        public Tamountpercent getCapitalshare() {
            return capitalshare;
        }

        /**
         * Legt den Wert der capitalshare-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tamountpercent }
         *     
         */
        public void setCapitalshare(Tamountpercent value) {
            this.capitalshare = value;
        }

    }

}
