
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tstatementreceivablesresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tstatementreceivablesresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tableheader">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="columntitle" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="row" maxOccurs="unbounded">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="typeturnover" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="datevaluta" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                   &lt;element name="columnamount" type="{http://www.w3.org/2001/XMLSchema}decimal" maxOccurs="unbounded"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="currency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tstatementreceivablesresponsebody", propOrder = {
    "tableheader",
    "row",
    "currency"
})
public class Tstatementreceivablesresponsebody {

    @XmlElement(required = true)
    protected Tstatementreceivablesresponsebody.Tableheader tableheader;
    @XmlElement(required = true)
    protected List<Tstatementreceivablesresponsebody.Row> row;
    @XmlElement(required = true)
    protected Tkey currency;

    /**
     * Ruft den Wert der tableheader-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tstatementreceivablesresponsebody.Tableheader }
     *     
     */
    public Tstatementreceivablesresponsebody.Tableheader getTableheader() {
        return tableheader;
    }

    /**
     * Legt den Wert der tableheader-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tstatementreceivablesresponsebody.Tableheader }
     *     
     */
    public void setTableheader(Tstatementreceivablesresponsebody.Tableheader value) {
        this.tableheader = value;
    }

    /**
     * Gets the value of the row property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the row property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRow().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tstatementreceivablesresponsebody.Row }
     * 
     * 
     */
    public List<Tstatementreceivablesresponsebody.Row> getRow() {
        if (row == null) {
            row = new ArrayList<Tstatementreceivablesresponsebody.Row>();
        }
        return this.row;
    }

    /**
     * Ruft den Wert der currency-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCurrency() {
        return currency;
    }

    /**
     * Legt den Wert der currency-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCurrency(Tkey value) {
        this.currency = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="typeturnover" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="datevaluta" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *         &lt;element name="columnamount" type="{http://www.w3.org/2001/XMLSchema}decimal" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "typeturnover",
        "datevaluta",
        "amount",
        "columnamount"
    })
    public static class Row {

        @XmlElement(required = true)
        protected String typeturnover;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datevaluta;
        @XmlElement(required = true)
        protected BigDecimal amount;
        @XmlElement(required = true, nillable = true)
        protected List<BigDecimal> columnamount;

        /**
         * Ruft den Wert der typeturnover-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTypeturnover() {
            return typeturnover;
        }

        /**
         * Legt den Wert der typeturnover-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTypeturnover(String value) {
            this.typeturnover = value;
        }

        /**
         * Ruft den Wert der datevaluta-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDatevaluta() {
            return datevaluta;
        }

        /**
         * Legt den Wert der datevaluta-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDatevaluta(XMLGregorianCalendar value) {
            this.datevaluta = value;
        }

        /**
         * Ruft den Wert der amount-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Legt den Wert der amount-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

        /**
         * Gets the value of the columnamount property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the columnamount property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getColumnamount().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link BigDecimal }
         * 
         * 
         */
        public List<BigDecimal> getColumnamount() {
            if (columnamount == null) {
                columnamount = new ArrayList<BigDecimal>();
            }
            return this.columnamount;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="columntitle" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "columntitle"
    })
    public static class Tableheader {

        @XmlElement(required = true)
        protected List<String> columntitle;

        /**
         * Gets the value of the columntitle property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the columntitle property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getColumntitle().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link String }
         * 
         * 
         */
        public List<String> getColumntitle() {
            if (columntitle == null) {
                columntitle = new ArrayList<String>();
            }
            return this.columntitle;
        }

    }

}
