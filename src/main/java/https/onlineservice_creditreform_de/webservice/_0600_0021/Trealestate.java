
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Trealestate complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Trealestate">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="landregisterinspection" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="values" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trealestateamounts" minOccurs="0"/>
 *         &lt;element name="mortgages" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trealestateamounts" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Trealestate", propOrder = {
    "landregisterinspection",
    "values",
    "mortgages"
})
@XmlSeeAlso({
    https.onlineservice_creditreform_de.webservice._0600_0021.Trealestates.Realestate.class
})
public class Trealestate {

    protected Boolean landregisterinspection;
    protected Trealestateamounts values;
    protected Trealestateamounts mortgages;

    /**
     * Ruft den Wert der landregisterinspection-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isLandregisterinspection() {
        return landregisterinspection;
    }

    /**
     * Legt den Wert der landregisterinspection-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setLandregisterinspection(Boolean value) {
        this.landregisterinspection = value;
    }

    /**
     * Ruft den Wert der values-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Trealestateamounts }
     *     
     */
    public Trealestateamounts getValues() {
        return values;
    }

    /**
     * Legt den Wert der values-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Trealestateamounts }
     *     
     */
    public void setValues(Trealestateamounts value) {
        this.values = value;
    }

    /**
     * Ruft den Wert der mortgages-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Trealestateamounts }
     *     
     */
    public Trealestateamounts getMortgages() {
        return mortgages;
    }

    /**
     * Legt den Wert der mortgages-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Trealestateamounts }
     *     
     */
    public void setMortgages(Trealestateamounts value) {
        this.mortgages = value;
    }

}
