
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tancillaryinformationshares complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tancillaryinformationshares">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="listed" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="isin" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="capitalstock" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *         &lt;element name="capitaltext" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="freereserves" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *         &lt;element name="legalreserves" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *         &lt;element name="reservestext" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="staffshares" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="staffsharesdecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="freefloat" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="freefloatdecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tancillaryinformationshares", propOrder = {
    "listed",
    "isin",
    "capitalstock",
    "capitaltext",
    "freereserves",
    "legalreserves",
    "reservestext",
    "staffshares",
    "staffsharesdecoration",
    "freefloat",
    "freefloatdecoration"
})
public class Tancillaryinformationshares {

    protected Boolean listed;
    protected String isin;
    protected Tamount capitalstock;
    protected Ttext capitaltext;
    protected Tamount freereserves;
    protected Tamount legalreserves;
    protected Ttext reservestext;
    protected BigDecimal staffshares;
    protected Tkey staffsharesdecoration;
    protected BigDecimal freefloat;
    protected Tkey freefloatdecoration;

    /**
     * Ruft den Wert der listed-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isListed() {
        return listed;
    }

    /**
     * Legt den Wert der listed-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setListed(Boolean value) {
        this.listed = value;
    }

    /**
     * Ruft den Wert der isin-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsin() {
        return isin;
    }

    /**
     * Legt den Wert der isin-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsin(String value) {
        this.isin = value;
    }

    /**
     * Ruft den Wert der capitalstock-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamount }
     *     
     */
    public Tamount getCapitalstock() {
        return capitalstock;
    }

    /**
     * Legt den Wert der capitalstock-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamount }
     *     
     */
    public void setCapitalstock(Tamount value) {
        this.capitalstock = value;
    }

    /**
     * Ruft den Wert der capitaltext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getCapitaltext() {
        return capitaltext;
    }

    /**
     * Legt den Wert der capitaltext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setCapitaltext(Ttext value) {
        this.capitaltext = value;
    }

    /**
     * Ruft den Wert der freereserves-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamount }
     *     
     */
    public Tamount getFreereserves() {
        return freereserves;
    }

    /**
     * Legt den Wert der freereserves-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamount }
     *     
     */
    public void setFreereserves(Tamount value) {
        this.freereserves = value;
    }

    /**
     * Ruft den Wert der legalreserves-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamount }
     *     
     */
    public Tamount getLegalreserves() {
        return legalreserves;
    }

    /**
     * Legt den Wert der legalreserves-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamount }
     *     
     */
    public void setLegalreserves(Tamount value) {
        this.legalreserves = value;
    }

    /**
     * Ruft den Wert der reservestext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getReservestext() {
        return reservestext;
    }

    /**
     * Legt den Wert der reservestext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setReservestext(Ttext value) {
        this.reservestext = value;
    }

    /**
     * Ruft den Wert der staffshares-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getStaffshares() {
        return staffshares;
    }

    /**
     * Legt den Wert der staffshares-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setStaffshares(BigDecimal value) {
        this.staffshares = value;
    }

    /**
     * Ruft den Wert der staffsharesdecoration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getStaffsharesdecoration() {
        return staffsharesdecoration;
    }

    /**
     * Legt den Wert der staffsharesdecoration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setStaffsharesdecoration(Tkey value) {
        this.staffsharesdecoration = value;
    }

    /**
     * Ruft den Wert der freefloat-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getFreefloat() {
        return freefloat;
    }

    /**
     * Legt den Wert der freefloat-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setFreefloat(BigDecimal value) {
        this.freefloat = value;
    }

    /**
     * Ruft den Wert der freefloatdecoration-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getFreefloatdecoration() {
        return freefloatdecoration;
    }

    /**
     * Legt den Wert der freefloatdecoration-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setFreefloatdecoration(Tkey value) {
        this.freefloatdecoration = value;
    }

}
