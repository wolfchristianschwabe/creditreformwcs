
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tenquirycounter complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tenquirycounter">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="requests28days" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestsinperiod" minOccurs="0"/>
 *         &lt;element name="requests56days" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestsinperiod" minOccurs="0"/>
 *         &lt;element name="requests365days" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestsinperiod" minOccurs="0"/>
 *         &lt;element name="explanation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tenquirycounter", propOrder = {
    "requests28Days",
    "requests56Days",
    "requests365Days",
    "explanation",
    "text"
})
public class Tenquirycounter {

    @XmlElement(name = "requests28days")
    protected Trequestsinperiod requests28Days;
    @XmlElement(name = "requests56days")
    protected Trequestsinperiod requests56Days;
    @XmlElement(name = "requests365days")
    protected Trequestsinperiod requests365Days;
    protected String explanation;
    protected Ttext text;

    /**
     * Ruft den Wert der requests28Days-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Trequestsinperiod }
     *     
     */
    public Trequestsinperiod getRequests28Days() {
        return requests28Days;
    }

    /**
     * Legt den Wert der requests28Days-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Trequestsinperiod }
     *     
     */
    public void setRequests28Days(Trequestsinperiod value) {
        this.requests28Days = value;
    }

    /**
     * Ruft den Wert der requests56Days-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Trequestsinperiod }
     *     
     */
    public Trequestsinperiod getRequests56Days() {
        return requests56Days;
    }

    /**
     * Legt den Wert der requests56Days-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Trequestsinperiod }
     *     
     */
    public void setRequests56Days(Trequestsinperiod value) {
        this.requests56Days = value;
    }

    /**
     * Ruft den Wert der requests365Days-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Trequestsinperiod }
     *     
     */
    public Trequestsinperiod getRequests365Days() {
        return requests365Days;
    }

    /**
     * Legt den Wert der requests365Days-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Trequestsinperiod }
     *     
     */
    public void setRequests365Days(Trequestsinperiod value) {
        this.requests365Days = value;
    }

    /**
     * Ruft den Wert der explanation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExplanation() {
        return explanation;
    }

    /**
     * Legt den Wert der explanation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExplanation(String value) {
        this.explanation = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

}
