
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcgaddresscheckdata complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcgaddresscheckdata">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="address" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgresponseaddress" minOccurs="0"/>
 *         &lt;element name="addressvalidationresult" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcgaddresscheckdata", propOrder = {
    "address",
    "addressvalidationresult"
})
public class Tcgaddresscheckdata {

    protected Tcgresponseaddress address;
    protected Tkey addressvalidationresult;

    /**
     * Ruft den Wert der address-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgresponseaddress }
     *     
     */
    public Tcgresponseaddress getAddress() {
        return address;
    }

    /**
     * Legt den Wert der address-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgresponseaddress }
     *     
     */
    public void setAddress(Tcgresponseaddress value) {
        this.address = value;
    }

    /**
     * Ruft den Wert der addressvalidationresult-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getAddressvalidationresult() {
        return addressvalidationresult;
    }

    /**
     * Legt den Wert der addressvalidationresult-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setAddressvalidationresult(Tkey value) {
        this.addressvalidationresult = value;
    }

}
