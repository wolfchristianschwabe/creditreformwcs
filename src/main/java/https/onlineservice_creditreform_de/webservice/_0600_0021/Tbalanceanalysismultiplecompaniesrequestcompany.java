
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tbalanceanalysismultiplecompaniesrequestcompany complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tbalanceanalysismultiplecompaniesrequestcompany">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber"/>
 *         &lt;element name="balanceyear" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="jurisdiction" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="balanceform" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tbalanceanalysismultiplecompaniesrequestcompany", propOrder = {
    "identificationnumber",
    "balanceyear",
    "jurisdiction",
    "balanceform"
})
public class Tbalanceanalysismultiplecompaniesrequestcompany {

    @XmlElement(required = true)
    protected String identificationnumber;
    protected int balanceyear;
    @XmlElement(required = true)
    protected String jurisdiction;
    @XmlElement(required = true)
    protected String balanceform;

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der balanceyear-Eigenschaft ab.
     * 
     */
    public int getBalanceyear() {
        return balanceyear;
    }

    /**
     * Legt den Wert der balanceyear-Eigenschaft fest.
     * 
     */
    public void setBalanceyear(int value) {
        this.balanceyear = value;
    }

    /**
     * Ruft den Wert der jurisdiction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getJurisdiction() {
        return jurisdiction;
    }

    /**
     * Legt den Wert der jurisdiction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setJurisdiction(String value) {
        this.jurisdiction = value;
    }

    /**
     * Ruft den Wert der balanceform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceform() {
        return balanceform;
    }

    /**
     * Legt den Wert der balanceform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceform(String value) {
        this.balanceform = value;
    }

}
