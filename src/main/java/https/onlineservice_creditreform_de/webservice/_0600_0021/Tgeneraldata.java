
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * General Information
 * 
 * <p>Java-Klasse f�r Tgeneraldata complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tgeneraldata">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="customerreference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="datesubmission" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="proceedingstatus" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="collectionordertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="dateclosure" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="collectionstatustext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="forwardingpossible" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *         &lt;element name="referencefilenumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tgeneraldata", propOrder = {
    "filenumber",
    "customerreference",
    "datesubmission",
    "proceedingstatus",
    "collectionordertype",
    "dateclosure",
    "collectionstatustext",
    "forwardingpossible",
    "referencefilenumber",
    "user"
})
public class Tgeneraldata {

    @XmlElement(required = true)
    protected String filenumber;
    protected String customerreference;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar datesubmission;
    protected Tkey proceedingstatus;
    protected Tkey collectionordertype;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateclosure;
    protected String collectionstatustext;
    protected boolean forwardingpossible;
    protected String referencefilenumber;
    protected String user;

    /**
     * Ruft den Wert der filenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFilenumber() {
        return filenumber;
    }

    /**
     * Legt den Wert der filenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFilenumber(String value) {
        this.filenumber = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der datesubmission-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDatesubmission() {
        return datesubmission;
    }

    /**
     * Legt den Wert der datesubmission-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDatesubmission(XMLGregorianCalendar value) {
        this.datesubmission = value;
    }

    /**
     * Ruft den Wert der proceedingstatus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getProceedingstatus() {
        return proceedingstatus;
    }

    /**
     * Legt den Wert der proceedingstatus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setProceedingstatus(Tkey value) {
        this.proceedingstatus = value;
    }

    /**
     * Ruft den Wert der collectionordertype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCollectionordertype() {
        return collectionordertype;
    }

    /**
     * Legt den Wert der collectionordertype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCollectionordertype(Tkey value) {
        this.collectionordertype = value;
    }

    /**
     * Ruft den Wert der dateclosure-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateclosure() {
        return dateclosure;
    }

    /**
     * Legt den Wert der dateclosure-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateclosure(XMLGregorianCalendar value) {
        this.dateclosure = value;
    }

    /**
     * Ruft den Wert der collectionstatustext-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectionstatustext() {
        return collectionstatustext;
    }

    /**
     * Legt den Wert der collectionstatustext-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectionstatustext(String value) {
        this.collectionstatustext = value;
    }

    /**
     * Ruft den Wert der forwardingpossible-Eigenschaft ab.
     * 
     */
    public boolean isForwardingpossible() {
        return forwardingpossible;
    }

    /**
     * Legt den Wert der forwardingpossible-Eigenschaft fest.
     * 
     */
    public void setForwardingpossible(boolean value) {
        this.forwardingpossible = value;
    }

    /**
     * Ruft den Wert der referencefilenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReferencefilenumber() {
        return referencefilenumber;
    }

    /**
     * Legt den Wert der referencefilenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReferencefilenumber(String value) {
        this.referencefilenumber = value;
    }

    /**
     * Ruft den Wert der user-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUser() {
        return user;
    }

    /**
     * Legt den Wert der user-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUser(String value) {
        this.user = value;
    }

}
