
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tshareholderextended complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tshareholderextended">
 *   &lt;complexContent>
 *     &lt;extension base="{https://onlineservice.creditreform.de/webservice/0600-0021}Tshareholder">
 *       &lt;sequence>
 *         &lt;element name="complementarycapacitiesshareholder" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tshareholdercapitalbasic" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tshareholderextended", propOrder = {
    "complementarycapacitiesshareholder"
})
public class Tshareholderextended
    extends Tshareholder
{

    protected Tshareholdercapitalbasic complementarycapacitiesshareholder;

    /**
     * Ruft den Wert der complementarycapacitiesshareholder-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tshareholdercapitalbasic }
     *     
     */
    public Tshareholdercapitalbasic getComplementarycapacitiesshareholder() {
        return complementarycapacitiesshareholder;
    }

    /**
     * Legt den Wert der complementarycapacitiesshareholder-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tshareholdercapitalbasic }
     *     
     */
    public void setComplementarycapacitiesshareholder(Tshareholdercapitalbasic value) {
        this.complementarycapacitiesshareholder = value;
    }

}
