
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tnegativefact complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tnegativefact">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="typeofnegativefacts" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *         &lt;element name="negativefact" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="negativefact" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="reportingdate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;element name="localcourt" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcourt" minOccurs="0"/>
 *                   &lt;element name="termofapplication" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="type" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="partytotheproceedings" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="type" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
 *                             &lt;element name="phone" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tphone" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="date" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="datetype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;element name="dateandtime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *                             &lt;element name="datedescriptionline" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="processstep" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="type" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;element name="dateofrecordal" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="evaluation" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *                   &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="enforcements" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="enforcement" maxOccurs="unbounded" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="enforcementtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;element name="year" minOccurs="0">
 *                               &lt;simpleType>
 *                                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
 *                                   &lt;minInclusive value="0"/>
 *                                   &lt;maxInclusive value="9999"/>
 *                                 &lt;/restriction>
 *                               &lt;/simpleType>
 *                             &lt;/element>
 *                             &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
 *                             &lt;element name="amount" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tnegativefact", propOrder = {
    "typeofnegativefacts",
    "negativefact",
    "evaluation",
    "enforcements",
    "text"
})
public class Tnegativefact {

    protected Tkey typeofnegativefacts;
    protected List<Tnegativefact.Negativefact> negativefact;
    protected Tnegativefact.Evaluation evaluation;
    protected Tnegativefact.Enforcements enforcements;
    protected Ttext text;

    /**
     * Ruft den Wert der typeofnegativefacts-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getTypeofnegativefacts() {
        return typeofnegativefacts;
    }

    /**
     * Legt den Wert der typeofnegativefacts-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setTypeofnegativefacts(Tkey value) {
        this.typeofnegativefacts = value;
    }

    /**
     * Gets the value of the negativefact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the negativefact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNegativefact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tnegativefact.Negativefact }
     * 
     * 
     */
    public List<Tnegativefact.Negativefact> getNegativefact() {
        if (negativefact == null) {
            negativefact = new ArrayList<Tnegativefact.Negativefact>();
        }
        return this.negativefact;
    }

    /**
     * Ruft den Wert der evaluation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tnegativefact.Evaluation }
     *     
     */
    public Tnegativefact.Evaluation getEvaluation() {
        return evaluation;
    }

    /**
     * Legt den Wert der evaluation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tnegativefact.Evaluation }
     *     
     */
    public void setEvaluation(Tnegativefact.Evaluation value) {
        this.evaluation = value;
    }

    /**
     * Ruft den Wert der enforcements-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tnegativefact.Enforcements }
     *     
     */
    public Tnegativefact.Enforcements getEnforcements() {
        return enforcements;
    }

    /**
     * Legt den Wert der enforcements-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tnegativefact.Enforcements }
     *     
     */
    public void setEnforcements(Tnegativefact.Enforcements value) {
        this.enforcements = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="enforcement" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="enforcementtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;element name="year" minOccurs="0">
     *                     &lt;simpleType>
     *                       &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
     *                         &lt;minInclusive value="0"/>
     *                         &lt;maxInclusive value="9999"/>
     *                       &lt;/restriction>
     *                     &lt;/simpleType>
     *                   &lt;/element>
     *                   &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
     *                   &lt;element name="amount" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "enforcement",
        "text"
    })
    public static class Enforcements {

        protected List<Tnegativefact.Enforcements.Enforcement> enforcement;
        protected Ttext text;

        /**
         * Gets the value of the enforcement property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the enforcement property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getEnforcement().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tnegativefact.Enforcements.Enforcement }
         * 
         * 
         */
        public List<Tnegativefact.Enforcements.Enforcement> getEnforcement() {
            if (enforcement == null) {
                enforcement = new ArrayList<Tnegativefact.Enforcements.Enforcement>();
            }
            return this.enforcement;
        }

        /**
         * Ruft den Wert der text-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getText() {
            return text;
        }

        /**
         * Legt den Wert der text-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setText(Ttext value) {
            this.text = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="enforcementtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="year" minOccurs="0">
         *           &lt;simpleType>
         *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}int">
         *               &lt;minInclusive value="0"/>
         *               &lt;maxInclusive value="9999"/>
         *             &lt;/restriction>
         *           &lt;/simpleType>
         *         &lt;/element>
         *         &lt;element name="number" type="{http://www.w3.org/2001/XMLSchema}nonNegativeInteger" minOccurs="0"/>
         *         &lt;element name="amount" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamount" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "enforcementtype",
            "year",
            "number",
            "amount"
        })
        public static class Enforcement {

            protected Tkey enforcementtype;
            protected Integer year;
            @XmlSchemaType(name = "nonNegativeInteger")
            protected BigInteger number;
            protected Tamount amount;

            /**
             * Ruft den Wert der enforcementtype-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getEnforcementtype() {
                return enforcementtype;
            }

            /**
             * Legt den Wert der enforcementtype-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setEnforcementtype(Tkey value) {
                this.enforcementtype = value;
            }

            /**
             * Ruft den Wert der year-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getYear() {
                return year;
            }

            /**
             * Legt den Wert der year-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setYear(Integer value) {
                this.year = value;
            }

            /**
             * Ruft den Wert der number-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getNumber() {
                return number;
            }

            /**
             * Legt den Wert der number-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setNumber(BigInteger value) {
                this.number = value;
            }

            /**
             * Ruft den Wert der amount-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tamount }
             *     
             */
            public Tamount getAmount() {
                return amount;
            }

            /**
             * Legt den Wert der amount-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tamount }
             *     
             */
            public void setAmount(Tamount value) {
                this.amount = value;
            }

        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
     *         &lt;element name="note" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "trafficlight",
        "note"
    })
    public static class Evaluation {

        protected Tkeywithgrade trafficlight;
        protected String note;

        /**
         * Ruft den Wert der trafficlight-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithgrade }
         *     
         */
        public Tkeywithgrade getTrafficlight() {
            return trafficlight;
        }

        /**
         * Legt den Wert der trafficlight-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithgrade }
         *     
         */
        public void setTrafficlight(Tkeywithgrade value) {
            this.trafficlight = value;
        }

        /**
         * Ruft den Wert der note-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNote() {
            return note;
        }

        /**
         * Legt den Wert der note-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNote(String value) {
            this.note = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="negativefact" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="reportingdate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="localcourt" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcourt" minOccurs="0"/>
     *         &lt;element name="termofapplication" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="type" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="partytotheproceedings" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="type" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
     *                   &lt;element name="phone" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tphone" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="date" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="datetype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;element name="dateandtime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
     *                   &lt;element name="datedescriptionline" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="processstep" maxOccurs="unbounded" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="type" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;element name="dateofrecordal" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "negativefact",
        "reportingdate",
        "filenumber",
        "localcourt",
        "termofapplication",
        "partytotheproceedings",
        "date",
        "processstep",
        "text"
    })
    public static class Negativefact {

        protected Tkey negativefact;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar reportingdate;
        protected String filenumber;
        protected Tcourt localcourt;
        protected Tnegativefact.Negativefact.Termofapplication termofapplication;
        protected Tnegativefact.Negativefact.Partytotheproceedings partytotheproceedings;
        protected List<Tnegativefact.Negativefact.Date> date;
        protected List<Tnegativefact.Negativefact.Processstep> processstep;
        protected Ttext text;

        /**
         * Ruft den Wert der negativefact-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getNegativefact() {
            return negativefact;
        }

        /**
         * Legt den Wert der negativefact-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setNegativefact(Tkey value) {
            this.negativefact = value;
        }

        /**
         * Ruft den Wert der reportingdate-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getReportingdate() {
            return reportingdate;
        }

        /**
         * Legt den Wert der reportingdate-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setReportingdate(XMLGregorianCalendar value) {
            this.reportingdate = value;
        }

        /**
         * Ruft den Wert der filenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFilenumber() {
            return filenumber;
        }

        /**
         * Legt den Wert der filenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFilenumber(String value) {
            this.filenumber = value;
        }

        /**
         * Ruft den Wert der localcourt-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tcourt }
         *     
         */
        public Tcourt getLocalcourt() {
            return localcourt;
        }

        /**
         * Legt den Wert der localcourt-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tcourt }
         *     
         */
        public void setLocalcourt(Tcourt value) {
            this.localcourt = value;
        }

        /**
         * Ruft den Wert der termofapplication-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tnegativefact.Negativefact.Termofapplication }
         *     
         */
        public Tnegativefact.Negativefact.Termofapplication getTermofapplication() {
            return termofapplication;
        }

        /**
         * Legt den Wert der termofapplication-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tnegativefact.Negativefact.Termofapplication }
         *     
         */
        public void setTermofapplication(Tnegativefact.Negativefact.Termofapplication value) {
            this.termofapplication = value;
        }

        /**
         * Ruft den Wert der partytotheproceedings-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tnegativefact.Negativefact.Partytotheproceedings }
         *     
         */
        public Tnegativefact.Negativefact.Partytotheproceedings getPartytotheproceedings() {
            return partytotheproceedings;
        }

        /**
         * Legt den Wert der partytotheproceedings-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tnegativefact.Negativefact.Partytotheproceedings }
         *     
         */
        public void setPartytotheproceedings(Tnegativefact.Negativefact.Partytotheproceedings value) {
            this.partytotheproceedings = value;
        }

        /**
         * Gets the value of the date property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the date property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getDate().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tnegativefact.Negativefact.Date }
         * 
         * 
         */
        public List<Tnegativefact.Negativefact.Date> getDate() {
            if (date == null) {
                date = new ArrayList<Tnegativefact.Negativefact.Date>();
            }
            return this.date;
        }

        /**
         * Gets the value of the processstep property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the processstep property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getProcessstep().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tnegativefact.Negativefact.Processstep }
         * 
         * 
         */
        public List<Tnegativefact.Negativefact.Processstep> getProcessstep() {
            if (processstep == null) {
                processstep = new ArrayList<Tnegativefact.Negativefact.Processstep>();
            }
            return this.processstep;
        }

        /**
         * Ruft den Wert der text-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getText() {
            return text;
        }

        /**
         * Legt den Wert der text-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setText(Ttext value) {
            this.text = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="datetype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="dateandtime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
         *         &lt;element name="datedescriptionline" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="2" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "datetype",
            "dateandtime",
            "datedescriptionline"
        })
        public static class Date {

            protected Tkey datetype;
            @XmlSchemaType(name = "dateTime")
            protected XMLGregorianCalendar dateandtime;
            protected List<String> datedescriptionline;

            /**
             * Ruft den Wert der datetype-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getDatetype() {
                return datetype;
            }

            /**
             * Legt den Wert der datetype-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setDatetype(Tkey value) {
                this.datetype = value;
            }

            /**
             * Ruft den Wert der dateandtime-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDateandtime() {
                return dateandtime;
            }

            /**
             * Legt den Wert der dateandtime-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDateandtime(XMLGregorianCalendar value) {
                this.dateandtime = value;
            }

            /**
             * Gets the value of the datedescriptionline property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the datedescriptionline property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getDatedescriptionline().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getDatedescriptionline() {
                if (datedescriptionline == null) {
                    datedescriptionline = new ArrayList<String>();
                }
                return this.datedescriptionline;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="type" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privateaddressstructure"/>
         *         &lt;element name="phone" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tphone" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "type",
            "title",
            "salutation",
            "surname",
            "firstname",
            "birthname",
            "nameaffix",
            "surnamewidow",
            "surnamebeforedivorce",
            "alias",
            "street",
            "housenumber",
            "housenumberaffix",
            "postcode",
            "city",
            "quarter",
            "region",
            "country",
            "phone"
        })
        public static class Partytotheproceedings {

            protected Tkey type;
            protected Tkeywithshortdesignation title;
            protected Tkey salutation;
            protected String surname;
            protected List<String> firstname;
            protected String birthname;
            protected String nameaffix;
            protected String surnamewidow;
            protected String surnamebeforedivorce;
            protected String alias;
            protected String street;
            protected Integer housenumber;
            protected String housenumberaffix;
            protected String postcode;
            protected String city;
            protected String quarter;
            protected String region;
            protected Tkey country;
            protected Tphone phone;

            /**
             * Ruft den Wert der type-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getType() {
                return type;
            }

            /**
             * Legt den Wert der type-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setType(Tkey value) {
                this.type = value;
            }

            /**
             * Ruft den Wert der title-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public Tkeywithshortdesignation getTitle() {
                return title;
            }

            /**
             * Legt den Wert der title-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkeywithshortdesignation }
             *     
             */
            public void setTitle(Tkeywithshortdesignation value) {
                this.title = value;
            }

            /**
             * Ruft den Wert der salutation-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getSalutation() {
                return salutation;
            }

            /**
             * Legt den Wert der salutation-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setSalutation(Tkey value) {
                this.salutation = value;
            }

            /**
             * Ruft den Wert der surname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSurname() {
                return surname;
            }

            /**
             * Legt den Wert der surname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSurname(String value) {
                this.surname = value;
            }

            /**
             * Gets the value of the firstname property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the firstname property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getFirstname().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link String }
             * 
             * 
             */
            public List<String> getFirstname() {
                if (firstname == null) {
                    firstname = new ArrayList<String>();
                }
                return this.firstname;
            }

            /**
             * Ruft den Wert der birthname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getBirthname() {
                return birthname;
            }

            /**
             * Legt den Wert der birthname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setBirthname(String value) {
                this.birthname = value;
            }

            /**
             * Ruft den Wert der nameaffix-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNameaffix() {
                return nameaffix;
            }

            /**
             * Legt den Wert der nameaffix-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNameaffix(String value) {
                this.nameaffix = value;
            }

            /**
             * Ruft den Wert der surnamewidow-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSurnamewidow() {
                return surnamewidow;
            }

            /**
             * Legt den Wert der surnamewidow-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSurnamewidow(String value) {
                this.surnamewidow = value;
            }

            /**
             * Ruft den Wert der surnamebeforedivorce-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSurnamebeforedivorce() {
                return surnamebeforedivorce;
            }

            /**
             * Legt den Wert der surnamebeforedivorce-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSurnamebeforedivorce(String value) {
                this.surnamebeforedivorce = value;
            }

            /**
             * Ruft den Wert der alias-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getAlias() {
                return alias;
            }

            /**
             * Legt den Wert der alias-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setAlias(String value) {
                this.alias = value;
            }

            /**
             * Ruft den Wert der street-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStreet() {
                return street;
            }

            /**
             * Legt den Wert der street-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStreet(String value) {
                this.street = value;
            }

            /**
             * Ruft den Wert der housenumber-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getHousenumber() {
                return housenumber;
            }

            /**
             * Legt den Wert der housenumber-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setHousenumber(Integer value) {
                this.housenumber = value;
            }

            /**
             * Ruft den Wert der housenumberaffix-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getHousenumberaffix() {
                return housenumberaffix;
            }

            /**
             * Legt den Wert der housenumberaffix-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setHousenumberaffix(String value) {
                this.housenumberaffix = value;
            }

            /**
             * Ruft den Wert der postcode-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPostcode() {
                return postcode;
            }

            /**
             * Legt den Wert der postcode-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPostcode(String value) {
                this.postcode = value;
            }

            /**
             * Ruft den Wert der city-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCity() {
                return city;
            }

            /**
             * Legt den Wert der city-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCity(String value) {
                this.city = value;
            }

            /**
             * Ruft den Wert der quarter-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getQuarter() {
                return quarter;
            }

            /**
             * Legt den Wert der quarter-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setQuarter(String value) {
                this.quarter = value;
            }

            /**
             * Ruft den Wert der region-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getRegion() {
                return region;
            }

            /**
             * Legt den Wert der region-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setRegion(String value) {
                this.region = value;
            }

            /**
             * Ruft den Wert der country-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getCountry() {
                return country;
            }

            /**
             * Legt den Wert der country-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setCountry(Tkey value) {
                this.country = value;
            }

            /**
             * Ruft den Wert der phone-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tphone }
             *     
             */
            public Tphone getPhone() {
                return phone;
            }

            /**
             * Legt den Wert der phone-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tphone }
             *     
             */
            public void setPhone(Tphone value) {
                this.phone = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="type" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="dateofrecordal" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "type",
            "dateofrecordal"
        })
        public static class Processstep {

            protected Tkey type;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar dateofrecordal;

            /**
             * Ruft den Wert der type-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getType() {
                return type;
            }

            /**
             * Legt den Wert der type-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setType(Tkey value) {
                this.type = value;
            }

            /**
             * Ruft den Wert der dateofrecordal-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDateofrecordal() {
                return dateofrecordal;
            }

            /**
             * Legt den Wert der dateofrecordal-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDateofrecordal(XMLGregorianCalendar value) {
                this.dateofrecordal = value;
            }

        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="type" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="text" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "type",
            "date",
            "text"
        })
        public static class Termofapplication {

            protected Tkey type;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar date;
            protected String text;

            /**
             * Ruft den Wert der type-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getType() {
                return type;
            }

            /**
             * Legt den Wert der type-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setType(Tkey value) {
                this.type = value;
            }

            /**
             * Ruft den Wert der date-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDate() {
                return date;
            }

            /**
             * Legt den Wert der date-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDate(XMLGregorianCalendar value) {
                this.date = value;
            }

            /**
             * Ruft den Wert der text-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getText() {
                return text;
            }

            /**
             * Legt den Wert der text-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setText(String value) {
                this.text = value;
            }

        }

    }

}
