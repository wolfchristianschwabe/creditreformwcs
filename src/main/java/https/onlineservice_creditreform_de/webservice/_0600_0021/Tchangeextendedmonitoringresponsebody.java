
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tchangeextendedmonitoringresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tchangeextendedmonitoringresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="extendedmonitoring" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="endofextendedmonitoring" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="extendedmonitoringplus" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="startofextendedmonitoringplus" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                   &lt;element name="endofextendedmonitoringplus" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tchangeextendedmonitoringresponsebody", propOrder = {
    "extendedmonitoring",
    "extendedmonitoringplus"
})
public class Tchangeextendedmonitoringresponsebody {

    protected Tchangeextendedmonitoringresponsebody.Extendedmonitoring extendedmonitoring;
    protected Tchangeextendedmonitoringresponsebody.Extendedmonitoringplus extendedmonitoringplus;

    /**
     * Ruft den Wert der extendedmonitoring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tchangeextendedmonitoringresponsebody.Extendedmonitoring }
     *     
     */
    public Tchangeextendedmonitoringresponsebody.Extendedmonitoring getExtendedmonitoring() {
        return extendedmonitoring;
    }

    /**
     * Legt den Wert der extendedmonitoring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tchangeextendedmonitoringresponsebody.Extendedmonitoring }
     *     
     */
    public void setExtendedmonitoring(Tchangeextendedmonitoringresponsebody.Extendedmonitoring value) {
        this.extendedmonitoring = value;
    }

    /**
     * Ruft den Wert der extendedmonitoringplus-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tchangeextendedmonitoringresponsebody.Extendedmonitoringplus }
     *     
     */
    public Tchangeextendedmonitoringresponsebody.Extendedmonitoringplus getExtendedmonitoringplus() {
        return extendedmonitoringplus;
    }

    /**
     * Legt den Wert der extendedmonitoringplus-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tchangeextendedmonitoringresponsebody.Extendedmonitoringplus }
     *     
     */
    public void setExtendedmonitoringplus(Tchangeextendedmonitoringresponsebody.Extendedmonitoringplus value) {
        this.extendedmonitoringplus = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="endofextendedmonitoring" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "endofextendedmonitoring"
    })
    public static class Extendedmonitoring {

        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar endofextendedmonitoring;

        /**
         * Ruft den Wert der endofextendedmonitoring-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEndofextendedmonitoring() {
            return endofextendedmonitoring;
        }

        /**
         * Legt den Wert der endofextendedmonitoring-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEndofextendedmonitoring(XMLGregorianCalendar value) {
            this.endofextendedmonitoring = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="startofextendedmonitoringplus" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="endofextendedmonitoringplus" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "startofextendedmonitoringplus",
        "endofextendedmonitoringplus"
    })
    public static class Extendedmonitoringplus {

        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar startofextendedmonitoringplus;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar endofextendedmonitoringplus;

        /**
         * Ruft den Wert der startofextendedmonitoringplus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getStartofextendedmonitoringplus() {
            return startofextendedmonitoringplus;
        }

        /**
         * Legt den Wert der startofextendedmonitoringplus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setStartofextendedmonitoringplus(XMLGregorianCalendar value) {
            this.startofextendedmonitoringplus = value;
        }

        /**
         * Ruft den Wert der endofextendedmonitoringplus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getEndofextendedmonitoringplus() {
            return endofextendedmonitoringplus;
        }

        /**
         * Legt den Wert der endofextendedmonitoringplus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setEndofextendedmonitoringplus(XMLGregorianCalendar value) {
            this.endofextendedmonitoringplus = value;
        }

    }

}
