
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Contact Data
 * 
 * <p>Java-Klasse f�r Tcollectionrequestcommunication complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionrequestcommunication">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="phone" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="countrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="diallingcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="phonenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="phonefullstring" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;choice>
 *           &lt;element name="fax" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="countrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="diallingcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="faxnumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="faxfullstring" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;/choice>
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionrequestcommunication", propOrder = {
    "phone",
    "phonefullstring",
    "fax",
    "faxfullstring",
    "email"
})
public class Tcollectionrequestcommunication {

    protected Tcollectionrequestcommunication.Phone phone;
    protected String phonefullstring;
    protected Tcollectionrequestcommunication.Fax fax;
    protected String faxfullstring;
    protected String email;

    /**
     * Ruft den Wert der phone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestcommunication.Phone }
     *     
     */
    public Tcollectionrequestcommunication.Phone getPhone() {
        return phone;
    }

    /**
     * Legt den Wert der phone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestcommunication.Phone }
     *     
     */
    public void setPhone(Tcollectionrequestcommunication.Phone value) {
        this.phone = value;
    }

    /**
     * Ruft den Wert der phonefullstring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhonefullstring() {
        return phonefullstring;
    }

    /**
     * Legt den Wert der phonefullstring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhonefullstring(String value) {
        this.phonefullstring = value;
    }

    /**
     * Ruft den Wert der fax-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestcommunication.Fax }
     *     
     */
    public Tcollectionrequestcommunication.Fax getFax() {
        return fax;
    }

    /**
     * Legt den Wert der fax-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestcommunication.Fax }
     *     
     */
    public void setFax(Tcollectionrequestcommunication.Fax value) {
        this.fax = value;
    }

    /**
     * Ruft den Wert der faxfullstring-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxfullstring() {
        return faxfullstring;
    }

    /**
     * Legt den Wert der faxfullstring-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxfullstring(String value) {
        this.faxfullstring = value;
    }

    /**
     * Ruft den Wert der email-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Legt den Wert der email-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="countrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="diallingcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="faxnumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "countrycode",
        "diallingcode",
        "faxnumber"
    })
    public static class Fax {

        @XmlElement(required = true)
        protected String countrycode;
        protected String diallingcode;
        @XmlElement(required = true)
        protected String faxnumber;

        /**
         * Ruft den Wert der countrycode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountrycode() {
            return countrycode;
        }

        /**
         * Legt den Wert der countrycode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountrycode(String value) {
            this.countrycode = value;
        }

        /**
         * Ruft den Wert der diallingcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDiallingcode() {
            return diallingcode;
        }

        /**
         * Legt den Wert der diallingcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDiallingcode(String value) {
            this.diallingcode = value;
        }

        /**
         * Ruft den Wert der faxnumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFaxnumber() {
            return faxnumber;
        }

        /**
         * Legt den Wert der faxnumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFaxnumber(String value) {
            this.faxnumber = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="countrycode" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="diallingcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="phonenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "countrycode",
        "diallingcode",
        "phonenumber"
    })
    public static class Phone {

        @XmlElement(required = true)
        protected String countrycode;
        protected String diallingcode;
        @XmlElement(required = true)
        protected String phonenumber;

        /**
         * Ruft den Wert der countrycode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCountrycode() {
            return countrycode;
        }

        /**
         * Legt den Wert der countrycode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCountrycode(String value) {
            this.countrycode = value;
        }

        /**
         * Ruft den Wert der diallingcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDiallingcode() {
            return diallingcode;
        }

        /**
         * Legt den Wert der diallingcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDiallingcode(String value) {
            this.diallingcode = value;
        }

        /**
         * Ruft den Wert der phonenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPhonenumber() {
            return phonenumber;
        }

        /**
         * Legt den Wert der phonenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPhonenumber(String value) {
            this.phonenumber = value;
        }

    }

}
