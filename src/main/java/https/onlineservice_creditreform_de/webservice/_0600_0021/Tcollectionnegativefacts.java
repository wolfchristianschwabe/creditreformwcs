
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * Information on Negative Facts
 * 
 * <p>Java-Klasse f�r Tcollectionnegativefacts complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionnegativefacts">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;sequence>
 *           &lt;element name="negativefact" maxOccurs="unbounded" minOccurs="0">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="negativefact" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *                     &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/sequence>
 *         &lt;element name="creditworthinesstrafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionnegativefacts", propOrder = {
    "negativefact",
    "creditworthinesstrafficlight"
})
public class Tcollectionnegativefacts {

    protected List<Tcollectionnegativefacts.Negativefact> negativefact;
    protected Tkeywithgrade creditworthinesstrafficlight;

    /**
     * Gets the value of the negativefact property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the negativefact property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNegativefact().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tcollectionnegativefacts.Negativefact }
     * 
     * 
     */
    public List<Tcollectionnegativefacts.Negativefact> getNegativefact() {
        if (negativefact == null) {
            negativefact = new ArrayList<Tcollectionnegativefacts.Negativefact>();
        }
        return this.negativefact;
    }

    /**
     * Ruft den Wert der creditworthinesstrafficlight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getCreditworthinesstrafficlight() {
        return creditworthinesstrafficlight;
    }

    /**
     * Legt den Wert der creditworthinesstrafficlight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setCreditworthinesstrafficlight(Tkeywithgrade value) {
        this.creditworthinesstrafficlight = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="negativefact" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
     *         &lt;element name="date" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "negativefact",
        "date"
    })
    public static class Negativefact {

        @XmlElement(required = true)
        protected Tkey negativefact;
        @XmlElement(required = true)
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar date;

        /**
         * Ruft den Wert der negativefact-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getNegativefact() {
            return negativefact;
        }

        /**
         * Legt den Wert der negativefact-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setNegativefact(Tkey value) {
            this.negativefact = value;
        }

        /**
         * Ruft den Wert der date-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDate() {
            return date;
        }

        /**
         * Legt den Wert der date-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDate(XMLGregorianCalendar value) {
            this.date = value;
        }

    }

}
