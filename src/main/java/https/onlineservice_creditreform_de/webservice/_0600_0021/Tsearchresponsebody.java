
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tsearchresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tsearchresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="hit" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsearchhit" maxOccurs="50" minOccurs="0"/>
 *         &lt;element name="morehits" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tsearchresponsebody", propOrder = {
    "hit",
    "morehits"
})
public class Tsearchresponsebody {

    protected List<Tsearchhit> hit;
    protected boolean morehits;

    /**
     * Gets the value of the hit property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the hit property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHit().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tsearchhit }
     * 
     * 
     */
    public List<Tsearchhit> getHit() {
        if (hit == null) {
            hit = new ArrayList<Tsearchhit>();
        }
        return this.hit;
    }

    /**
     * Ruft den Wert der morehits-Eigenschaft ab.
     * 
     */
    public boolean isMorehits() {
        return morehits;
    }

    /**
     * Legt den Wert der morehits-Eigenschaft fest.
     * 
     */
    public void setMorehits(boolean value) {
        this.morehits = value;
    }

}
