
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tmailboxentryresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tmailboxentryresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}mailboxentrynumber" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}referencenumber"/>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *         &lt;element name="creationtime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="calltime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="reportlanguage" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tlanguage"/>
 *         &lt;element name="deliverytype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="report" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Treportresponsebody"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tmailboxentryresponsebody", propOrder = {
    "mailboxentrynumber",
    "referencenumber",
    "customerreference",
    "identificationnumber",
    "easynumber",
    "creationtime",
    "calltime",
    "producttype",
    "reportlanguage",
    "deliverytype",
    "report"
})
public class Tmailboxentryresponsebody {

    protected String mailboxentrynumber;
    protected long referencenumber;
    protected String customerreference;
    protected String identificationnumber;
    protected String easynumber;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationtime;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar calltime;
    protected Tkey producttype;
    @XmlElement(required = true)
    protected String reportlanguage;
    protected Tkey deliverytype;
    @XmlElement(required = true)
    protected Treportresponsebody report;

    /**
     * Ruft den Wert der mailboxentrynumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMailboxentrynumber() {
        return mailboxentrynumber;
    }

    /**
     * Legt den Wert der mailboxentrynumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMailboxentrynumber(String value) {
        this.mailboxentrynumber = value;
    }

    /**
     * Ruft den Wert der referencenumber-Eigenschaft ab.
     * 
     */
    public long getReferencenumber() {
        return referencenumber;
    }

    /**
     * Legt den Wert der referencenumber-Eigenschaft fest.
     * 
     */
    public void setReferencenumber(long value) {
        this.referencenumber = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der easynumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEasynumber() {
        return easynumber;
    }

    /**
     * Legt den Wert der easynumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEasynumber(String value) {
        this.easynumber = value;
    }

    /**
     * Ruft den Wert der creationtime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationtime() {
        return creationtime;
    }

    /**
     * Legt den Wert der creationtime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationtime(XMLGregorianCalendar value) {
        this.creationtime = value;
    }

    /**
     * Ruft den Wert der calltime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCalltime() {
        return calltime;
    }

    /**
     * Legt den Wert der calltime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCalltime(XMLGregorianCalendar value) {
        this.calltime = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setProducttype(Tkey value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der reportlanguage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReportlanguage() {
        return reportlanguage;
    }

    /**
     * Legt den Wert der reportlanguage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReportlanguage(String value) {
        this.reportlanguage = value;
    }

    /**
     * Ruft den Wert der deliverytype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getDeliverytype() {
        return deliverytype;
    }

    /**
     * Legt den Wert der deliverytype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setDeliverytype(Tkey value) {
        this.deliverytype = value;
    }

    /**
     * Ruft den Wert der report-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Treportresponsebody }
     *     
     */
    public Treportresponsebody getReport() {
        return report;
    }

    /**
     * Legt den Wert der report-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Treportresponsebody }
     *     
     */
    public void setReport(Treportresponsebody value) {
        this.report = value;
    }

}
