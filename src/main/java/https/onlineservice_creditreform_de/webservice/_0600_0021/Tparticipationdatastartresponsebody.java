
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tparticipationdatastartresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tparticipationdatastartresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}participationdatareference"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tparticipationdatastartresponsebody", propOrder = {
    "participationdatareference"
})
public class Tparticipationdatastartresponsebody {

    protected long participationdatareference;

    /**
     * Ruft den Wert der participationdatareference-Eigenschaft ab.
     * 
     */
    public long getParticipationdatareference() {
        return participationdatareference;
    }

    /**
     * Legt den Wert der participationdatareference-Eigenschaft fest.
     * 
     */
    public void setParticipationdatareference(long value) {
        this.participationdatareference = value;
    }

}
