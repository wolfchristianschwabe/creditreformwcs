
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcollectionaccountrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionaccountrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="lastknownaccountinvoicenumber" type="{http://www.w3.org/2001/XMLSchema}long"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionaccountrequestbody", propOrder = {
    "lastknownaccountinvoicenumber"
})
public class Tcollectionaccountrequestbody {

    protected long lastknownaccountinvoicenumber;

    /**
     * Ruft den Wert der lastknownaccountinvoicenumber-Eigenschaft ab.
     * 
     */
    public long getLastknownaccountinvoicenumber() {
        return lastknownaccountinvoicenumber;
    }

    /**
     * Legt den Wert der lastknownaccountinvoicenumber-Eigenschaft fest.
     * 
     */
    public void setLastknownaccountinvoicenumber(long value) {
        this.lastknownaccountinvoicenumber = value;
    }

}
