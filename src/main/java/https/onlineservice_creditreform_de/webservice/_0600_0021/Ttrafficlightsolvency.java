
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Ttrafficlightsolvency complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Ttrafficlightsolvency">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence minOccurs="0">
 *         &lt;element name="colour" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *         &lt;element name="solvencyindexrange" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="meaning" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="probabilityofdefaultrange" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="probabilityofdefaultcompaniesaverage" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="probabilityofdefaultrangegreen" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="probabilityofdefaultrangeyellow" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="probabilityofdefaultrangered" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="probabilityofdefaultexplanation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Ttrafficlightsolvency", propOrder = {
    "colour",
    "solvencyindexrange",
    "meaning",
    "probabilityofdefaultrange",
    "probabilityofdefaultcompaniesaverage",
    "probabilityofdefaultrangegreen",
    "probabilityofdefaultrangeyellow",
    "probabilityofdefaultrangered",
    "probabilityofdefaultexplanation",
    "text"
})
public class Ttrafficlightsolvency {

    protected Tkeywithgrade colour;
    protected String solvencyindexrange;
    protected String meaning;
    protected String probabilityofdefaultrange;
    protected BigDecimal probabilityofdefaultcompaniesaverage;
    protected String probabilityofdefaultrangegreen;
    protected String probabilityofdefaultrangeyellow;
    protected String probabilityofdefaultrangered;
    protected String probabilityofdefaultexplanation;
    protected Ttext text;

    /**
     * Ruft den Wert der colour-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getColour() {
        return colour;
    }

    /**
     * Legt den Wert der colour-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setColour(Tkeywithgrade value) {
        this.colour = value;
    }

    /**
     * Ruft den Wert der solvencyindexrange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSolvencyindexrange() {
        return solvencyindexrange;
    }

    /**
     * Legt den Wert der solvencyindexrange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSolvencyindexrange(String value) {
        this.solvencyindexrange = value;
    }

    /**
     * Ruft den Wert der meaning-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMeaning() {
        return meaning;
    }

    /**
     * Legt den Wert der meaning-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMeaning(String value) {
        this.meaning = value;
    }

    /**
     * Ruft den Wert der probabilityofdefaultrange-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProbabilityofdefaultrange() {
        return probabilityofdefaultrange;
    }

    /**
     * Legt den Wert der probabilityofdefaultrange-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProbabilityofdefaultrange(String value) {
        this.probabilityofdefaultrange = value;
    }

    /**
     * Ruft den Wert der probabilityofdefaultcompaniesaverage-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProbabilityofdefaultcompaniesaverage() {
        return probabilityofdefaultcompaniesaverage;
    }

    /**
     * Legt den Wert der probabilityofdefaultcompaniesaverage-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProbabilityofdefaultcompaniesaverage(BigDecimal value) {
        this.probabilityofdefaultcompaniesaverage = value;
    }

    /**
     * Ruft den Wert der probabilityofdefaultrangegreen-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProbabilityofdefaultrangegreen() {
        return probabilityofdefaultrangegreen;
    }

    /**
     * Legt den Wert der probabilityofdefaultrangegreen-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProbabilityofdefaultrangegreen(String value) {
        this.probabilityofdefaultrangegreen = value;
    }

    /**
     * Ruft den Wert der probabilityofdefaultrangeyellow-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProbabilityofdefaultrangeyellow() {
        return probabilityofdefaultrangeyellow;
    }

    /**
     * Legt den Wert der probabilityofdefaultrangeyellow-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProbabilityofdefaultrangeyellow(String value) {
        this.probabilityofdefaultrangeyellow = value;
    }

    /**
     * Ruft den Wert der probabilityofdefaultrangered-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProbabilityofdefaultrangered() {
        return probabilityofdefaultrangered;
    }

    /**
     * Legt den Wert der probabilityofdefaultrangered-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProbabilityofdefaultrangered(String value) {
        this.probabilityofdefaultrangered = value;
    }

    /**
     * Ruft den Wert der probabilityofdefaultexplanation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProbabilityofdefaultexplanation() {
        return probabilityofdefaultexplanation;
    }

    /**
     * Legt den Wert der probabilityofdefaultexplanation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProbabilityofdefaultexplanation(String value) {
        this.probabilityofdefaultexplanation = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

}
