
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Trequestheader complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Trequestheader">
 *   &lt;complexContent>
 *     &lt;extension base="{https://onlineservice.creditreform.de/webservice/0600-0021}Tabstractheader">
 *       &lt;sequence>
 *         &lt;element name="generalpassword" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="individualpassword" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Trequestheader", propOrder = {
    "generalpassword",
    "individualpassword"
})
public class Trequestheader
    extends Tabstractheader
{

    @XmlElement(required = true)
    protected String generalpassword;
    @XmlElement(required = true)
    protected String individualpassword;

    /**
     * Ruft den Wert der generalpassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneralpassword() {
        return generalpassword;
    }

    /**
     * Legt den Wert der generalpassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneralpassword(String value) {
        this.generalpassword = value;
    }

    /**
     * Ruft den Wert der individualpassword-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndividualpassword() {
        return individualpassword;
    }

    /**
     * Legt den Wert der individualpassword-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndividualpassword(String value) {
        this.individualpassword = value;
    }

}
