
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Interest Information
 * 
 * <p>Java-Klasse f�r Tcollectionrequestinterest complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionrequestinterest">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;choice>
 *         &lt;element name="legal">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="variable">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="spread" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="fix">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="interestrate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/choice>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionrequestinterest", propOrder = {
    "legal",
    "variable",
    "fix"
})
public class Tcollectionrequestinterest {

    protected Tcollectionrequestinterest.Legal legal;
    protected Tcollectionrequestinterest.Variable variable;
    protected Tcollectionrequestinterest.Fix fix;

    /**
     * Ruft den Wert der legal-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestinterest.Legal }
     *     
     */
    public Tcollectionrequestinterest.Legal getLegal() {
        return legal;
    }

    /**
     * Legt den Wert der legal-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestinterest.Legal }
     *     
     */
    public void setLegal(Tcollectionrequestinterest.Legal value) {
        this.legal = value;
    }

    /**
     * Ruft den Wert der variable-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestinterest.Variable }
     *     
     */
    public Tcollectionrequestinterest.Variable getVariable() {
        return variable;
    }

    /**
     * Legt den Wert der variable-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestinterest.Variable }
     *     
     */
    public void setVariable(Tcollectionrequestinterest.Variable value) {
        this.variable = value;
    }

    /**
     * Ruft den Wert der fix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionrequestinterest.Fix }
     *     
     */
    public Tcollectionrequestinterest.Fix getFix() {
        return fix;
    }

    /**
     * Legt den Wert der fix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionrequestinterest.Fix }
     *     
     */
    public void setFix(Tcollectionrequestinterest.Fix value) {
        this.fix = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="interestrate" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "interestrate"
    })
    public static class Fix {

        @XmlElement(required = true)
        protected BigDecimal interestrate;

        /**
         * Ruft den Wert der interestrate-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getInterestrate() {
            return interestrate;
        }

        /**
         * Legt den Wert der interestrate-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setInterestrate(BigDecimal value) {
            this.interestrate = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Legal {


    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="spread" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "spread"
    })
    public static class Variable {

        @XmlElement(required = true)
        protected BigDecimal spread;

        /**
         * Ruft den Wert der spread-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getSpread() {
            return spread;
        }

        /**
         * Legt den Wert der spread-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setSpread(BigDecimal value) {
            this.spread = value;
        }

    }

}
