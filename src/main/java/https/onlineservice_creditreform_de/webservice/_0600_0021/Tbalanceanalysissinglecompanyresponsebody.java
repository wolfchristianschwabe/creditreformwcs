
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAnyElement;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import org.w3c.dom.Element;


/**
 * <p>Java-Klasse f�r Tbalanceanalysissinglecompanyresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tbalanceanalysissinglecompanyresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *         &lt;element name="creationtime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="company">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber"/>
 *                   &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *                   &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}balanceanalysismaincompanystructure"/>
 *                   &lt;element name="balanceform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *                   &lt;element name="jurisdiction" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
 *                   &lt;element name="balanceyear" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="balanceyears" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *                   &lt;element name="profitslossescalculationtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="xbrl" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="standardversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="taxonomy" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="taxonomynumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="content">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;any processContents='skip' namespace='##other'/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="textreport" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *         &lt;element name="textreporttwo" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tbalanceanalysissinglecompanyresponsebody", propOrder = {
    "customerreference",
    "producttype",
    "creationtime",
    "company",
    "textreport",
    "textreporttwo"
})
public class Tbalanceanalysissinglecompanyresponsebody {

    protected String customerreference;
    @XmlElement(required = true)
    protected Tkey producttype;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationtime;
    @XmlElement(required = true)
    protected Tbalanceanalysissinglecompanyresponsebody.Company company;
    @XmlMimeType("application/octet-stream")
    protected DataHandler textreport;
    @XmlMimeType("application/octet-stream")
    protected DataHandler textreporttwo;

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setProducttype(Tkey value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der creationtime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationtime() {
        return creationtime;
    }

    /**
     * Legt den Wert der creationtime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationtime(XMLGregorianCalendar value) {
        this.creationtime = value;
    }

    /**
     * Ruft den Wert der company-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tbalanceanalysissinglecompanyresponsebody.Company }
     *     
     */
    public Tbalanceanalysissinglecompanyresponsebody.Company getCompany() {
        return company;
    }

    /**
     * Legt den Wert der company-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tbalanceanalysissinglecompanyresponsebody.Company }
     *     
     */
    public void setCompany(Tbalanceanalysissinglecompanyresponsebody.Company value) {
        this.company = value;
    }

    /**
     * Ruft den Wert der textreport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getTextreport() {
        return textreport;
    }

    /**
     * Legt den Wert der textreport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setTextreport(DataHandler value) {
        this.textreport = value;
    }

    /**
     * Ruft den Wert der textreporttwo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getTextreporttwo() {
        return textreporttwo;
    }

    /**
     * Legt den Wert der textreporttwo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setTextreporttwo(DataHandler value) {
        this.textreporttwo = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber"/>
     *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
     *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}balanceanalysismaincompanystructure"/>
     *         &lt;element name="balanceform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
     *         &lt;element name="jurisdiction" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey"/>
     *         &lt;element name="balanceyear" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="balanceyears" type="{http://www.w3.org/2001/XMLSchema}int"/>
     *         &lt;element name="profitslossescalculationtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="xbrl" minOccurs="0">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="standardversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="taxonomy" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="taxonomynumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="content">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;any processContents='skip' namespace='##other'/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "identificationnumber",
        "easynumber",
        "companyname",
        "tradename",
        "commercialname",
        "street",
        "housenumber",
        "housenumberaffix",
        "postcode",
        "city",
        "quarter",
        "country",
        "legalform",
        "phone",
        "fax",
        "mobile",
        "email",
        "website",
        "balanceform",
        "jurisdiction",
        "balanceyear",
        "balanceyears",
        "profitslossescalculationtype",
        "xbrl"
    })
    public static class Company {

        @XmlElement(required = true)
        protected String identificationnumber;
        protected String easynumber;
        protected String companyname;
        protected String tradename;
        protected String commercialname;
        protected String street;
        protected Integer housenumber;
        protected String housenumberaffix;
        protected String postcode;
        protected String city;
        protected String quarter;
        protected Tkey country;
        protected Tkeywithshortdesignation legalform;
        protected Tphone phone;
        protected Tphone fax;
        protected Tphone mobile;
        protected String email;
        protected String website;
        @XmlElement(required = true)
        protected Tkey balanceform;
        @XmlElement(required = true)
        protected Tkey jurisdiction;
        protected int balanceyear;
        protected int balanceyears;
        protected Tkey profitslossescalculationtype;
        protected Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl xbrl;

        /**
         * Ruft den Wert der identificationnumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getIdentificationnumber() {
            return identificationnumber;
        }

        /**
         * Legt den Wert der identificationnumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setIdentificationnumber(String value) {
            this.identificationnumber = value;
        }

        /**
         * Ruft den Wert der easynumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEasynumber() {
            return easynumber;
        }

        /**
         * Legt den Wert der easynumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEasynumber(String value) {
            this.easynumber = value;
        }

        /**
         * Ruft den Wert der companyname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyname() {
            return companyname;
        }

        /**
         * Legt den Wert der companyname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyname(String value) {
            this.companyname = value;
        }

        /**
         * Ruft den Wert der tradename-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTradename() {
            return tradename;
        }

        /**
         * Legt den Wert der tradename-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTradename(String value) {
            this.tradename = value;
        }

        /**
         * Ruft den Wert der commercialname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCommercialname() {
            return commercialname;
        }

        /**
         * Legt den Wert der commercialname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCommercialname(String value) {
            this.commercialname = value;
        }

        /**
         * Ruft den Wert der street-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getStreet() {
            return street;
        }

        /**
         * Legt den Wert der street-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setStreet(String value) {
            this.street = value;
        }

        /**
         * Ruft den Wert der housenumber-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getHousenumber() {
            return housenumber;
        }

        /**
         * Legt den Wert der housenumber-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setHousenumber(Integer value) {
            this.housenumber = value;
        }

        /**
         * Ruft den Wert der housenumberaffix-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getHousenumberaffix() {
            return housenumberaffix;
        }

        /**
         * Legt den Wert der housenumberaffix-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setHousenumberaffix(String value) {
            this.housenumberaffix = value;
        }

        /**
         * Ruft den Wert der postcode-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPostcode() {
            return postcode;
        }

        /**
         * Legt den Wert der postcode-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPostcode(String value) {
            this.postcode = value;
        }

        /**
         * Ruft den Wert der city-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Legt den Wert der city-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Ruft den Wert der quarter-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getQuarter() {
            return quarter;
        }

        /**
         * Legt den Wert der quarter-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setQuarter(String value) {
            this.quarter = value;
        }

        /**
         * Ruft den Wert der country-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getCountry() {
            return country;
        }

        /**
         * Legt den Wert der country-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setCountry(Tkey value) {
            this.country = value;
        }

        /**
         * Ruft den Wert der legalform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public Tkeywithshortdesignation getLegalform() {
            return legalform;
        }

        /**
         * Legt den Wert der legalform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public void setLegalform(Tkeywithshortdesignation value) {
            this.legalform = value;
        }

        /**
         * Ruft den Wert der phone-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tphone }
         *     
         */
        public Tphone getPhone() {
            return phone;
        }

        /**
         * Legt den Wert der phone-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tphone }
         *     
         */
        public void setPhone(Tphone value) {
            this.phone = value;
        }

        /**
         * Ruft den Wert der fax-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tphone }
         *     
         */
        public Tphone getFax() {
            return fax;
        }

        /**
         * Legt den Wert der fax-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tphone }
         *     
         */
        public void setFax(Tphone value) {
            this.fax = value;
        }

        /**
         * Ruft den Wert der mobile-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tphone }
         *     
         */
        public Tphone getMobile() {
            return mobile;
        }

        /**
         * Legt den Wert der mobile-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tphone }
         *     
         */
        public void setMobile(Tphone value) {
            this.mobile = value;
        }

        /**
         * Ruft den Wert der email-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEmail() {
            return email;
        }

        /**
         * Legt den Wert der email-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEmail(String value) {
            this.email = value;
        }

        /**
         * Ruft den Wert der website-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWebsite() {
            return website;
        }

        /**
         * Legt den Wert der website-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWebsite(String value) {
            this.website = value;
        }

        /**
         * Ruft den Wert der balanceform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getBalanceform() {
            return balanceform;
        }

        /**
         * Legt den Wert der balanceform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setBalanceform(Tkey value) {
            this.balanceform = value;
        }

        /**
         * Ruft den Wert der jurisdiction-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getJurisdiction() {
            return jurisdiction;
        }

        /**
         * Legt den Wert der jurisdiction-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setJurisdiction(Tkey value) {
            this.jurisdiction = value;
        }

        /**
         * Ruft den Wert der balanceyear-Eigenschaft ab.
         * 
         */
        public int getBalanceyear() {
            return balanceyear;
        }

        /**
         * Legt den Wert der balanceyear-Eigenschaft fest.
         * 
         */
        public void setBalanceyear(int value) {
            this.balanceyear = value;
        }

        /**
         * Ruft den Wert der balanceyears-Eigenschaft ab.
         * 
         */
        public int getBalanceyears() {
            return balanceyears;
        }

        /**
         * Legt den Wert der balanceyears-Eigenschaft fest.
         * 
         */
        public void setBalanceyears(int value) {
            this.balanceyears = value;
        }

        /**
         * Ruft den Wert der profitslossescalculationtype-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getProfitslossescalculationtype() {
            return profitslossescalculationtype;
        }

        /**
         * Legt den Wert der profitslossescalculationtype-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setProfitslossescalculationtype(Tkey value) {
            this.profitslossescalculationtype = value;
        }

        /**
         * Ruft den Wert der xbrl-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl }
         *     
         */
        public Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl getXbrl() {
            return xbrl;
        }

        /**
         * Legt den Wert der xbrl-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl }
         *     
         */
        public void setXbrl(Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl value) {
            this.xbrl = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="standardversion" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="taxonomy" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="taxonomynumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="content">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;any processContents='skip' namespace='##other'/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "standardversion",
            "taxonomy",
            "taxonomynumber",
            "content"
        })
        public static class Xbrl {

            @XmlElement(required = true)
            protected String standardversion;
            @XmlElement(required = true)
            protected String taxonomy;
            @XmlElement(required = true)
            protected String taxonomynumber;
            @XmlElement(required = true)
            protected Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl.Content content;

            /**
             * Ruft den Wert der standardversion-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getStandardversion() {
                return standardversion;
            }

            /**
             * Legt den Wert der standardversion-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setStandardversion(String value) {
                this.standardversion = value;
            }

            /**
             * Ruft den Wert der taxonomy-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxonomy() {
                return taxonomy;
            }

            /**
             * Legt den Wert der taxonomy-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxonomy(String value) {
                this.taxonomy = value;
            }

            /**
             * Ruft den Wert der taxonomynumber-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getTaxonomynumber() {
                return taxonomynumber;
            }

            /**
             * Legt den Wert der taxonomynumber-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setTaxonomynumber(String value) {
                this.taxonomynumber = value;
            }

            /**
             * Ruft den Wert der content-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl.Content }
             *     
             */
            public Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl.Content getContent() {
                return content;
            }

            /**
             * Legt den Wert der content-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl.Content }
             *     
             */
            public void setContent(Tbalanceanalysissinglecompanyresponsebody.Company.Xbrl.Content value) {
                this.content = value;
            }


            /**
             * <p>Java-Klasse f�r anonymous complex type.
             * 
             * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;any processContents='skip' namespace='##other'/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "any"
            })
            public static class Content {

                @XmlAnyElement
                protected Element any;

                /**
                 * Ruft den Wert der any-Eigenschaft ab.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Element }
                 *     
                 */
                public Element getAny() {
                    return any;
                }

                /**
                 * Legt den Wert der any-Eigenschaft fest.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Element }
                 *     
                 */
                public void setAny(Element value) {
                    this.any = value;
                }

            }

        }

    }

}
