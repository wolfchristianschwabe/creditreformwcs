
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.activation.DataHandler;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlMimeType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcollectionagencydocumentresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionagencydocumentresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="pdfdocument" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionagencydocumentresponsebody", propOrder = {
    "pdfdocument"
})
public class Tcollectionagencydocumentresponsebody {

    @XmlMimeType("application/octet-stream")
    protected DataHandler pdfdocument;

    /**
     * Ruft den Wert der pdfdocument-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link DataHandler }
     *     
     */
    public DataHandler getPdfdocument() {
        return pdfdocument;
    }

    /**
     * Legt den Wert der pdfdocument-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link DataHandler }
     *     
     */
    public void setPdfdocument(DataHandler value) {
        this.pdfdocument = value;
    }

}
