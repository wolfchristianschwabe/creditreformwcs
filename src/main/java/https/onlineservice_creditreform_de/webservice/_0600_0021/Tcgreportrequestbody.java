
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tcgreportrequestbody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcgreportrequestbody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeyproduct"/>
 *         &lt;element name="legitimateinterest" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeylegitimateinterest"/>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element name="salutation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Trequestkeysalutation"/>
 *         &lt;element name="surname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsurname"/>
 *         &lt;element name="firstname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tfirstname"/>
 *         &lt;element name="birthname" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tsurname" minOccurs="0"/>
 *         &lt;element name="dateofbirth" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *         &lt;element name="addressone" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgrequestaddress"/>
 *         &lt;element name="addresstwo" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgrequestaddress" minOccurs="0"/>
 *         &lt;element name="consentgiven" type="{http://www.w3.org/2001/XMLSchema}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcgreportrequestbody", propOrder = {
    "producttype",
    "legitimateinterest",
    "customerreference",
    "salutation",
    "surname",
    "firstname",
    "birthname",
    "dateofbirth",
    "addressone",
    "addresstwo",
    "consentgiven"
})
@XmlSeeAlso({
    Tconsumerreportrequestbody.class,
    Tbonimareportrequestbody.class
})
public class Tcgreportrequestbody {

    @XmlElement(required = true)
    protected String producttype;
    @XmlElement(required = true)
    protected String legitimateinterest;
    protected String customerreference;
    @XmlElement(required = true)
    protected String salutation;
    @XmlElement(required = true)
    protected String surname;
    @XmlElement(required = true)
    protected String firstname;
    protected String birthname;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateofbirth;
    @XmlElement(required = true)
    protected Tcgrequestaddress addressone;
    protected Tcgrequestaddress addresstwo;
    protected boolean consentgiven;

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProducttype(String value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der legitimateinterest-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLegitimateinterest() {
        return legitimateinterest;
    }

    /**
     * Legt den Wert der legitimateinterest-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLegitimateinterest(String value) {
        this.legitimateinterest = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der salutation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalutation() {
        return salutation;
    }

    /**
     * Legt den Wert der salutation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalutation(String value) {
        this.salutation = value;
    }

    /**
     * Ruft den Wert der surname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Legt den Wert der surname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurname(String value) {
        this.surname = value;
    }

    /**
     * Ruft den Wert der firstname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Legt den Wert der firstname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFirstname(String value) {
        this.firstname = value;
    }

    /**
     * Ruft den Wert der birthname-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthname() {
        return birthname;
    }

    /**
     * Legt den Wert der birthname-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthname(String value) {
        this.birthname = value;
    }

    /**
     * Ruft den Wert der dateofbirth-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateofbirth() {
        return dateofbirth;
    }

    /**
     * Legt den Wert der dateofbirth-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateofbirth(XMLGregorianCalendar value) {
        this.dateofbirth = value;
    }

    /**
     * Ruft den Wert der addressone-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgrequestaddress }
     *     
     */
    public Tcgrequestaddress getAddressone() {
        return addressone;
    }

    /**
     * Legt den Wert der addressone-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgrequestaddress }
     *     
     */
    public void setAddressone(Tcgrequestaddress value) {
        this.addressone = value;
    }

    /**
     * Ruft den Wert der addresstwo-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgrequestaddress }
     *     
     */
    public Tcgrequestaddress getAddresstwo() {
        return addresstwo;
    }

    /**
     * Legt den Wert der addresstwo-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgrequestaddress }
     *     
     */
    public void setAddresstwo(Tcgrequestaddress value) {
        this.addresstwo = value;
    }

    /**
     * Ruft den Wert der consentgiven-Eigenschaft ab.
     * 
     */
    public boolean isConsentgiven() {
        return consentgiven;
    }

    /**
     * Legt den Wert der consentgiven-Eigenschaft fest.
     * 
     */
    public void setConsentgiven(boolean value) {
        this.consentgiven = value;
    }

}
