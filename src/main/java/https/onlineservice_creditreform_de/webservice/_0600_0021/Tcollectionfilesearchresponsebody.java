
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tcollectionfilesearchresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionfilesearchresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="filenumberoverview">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence maxOccurs="unbounded" minOccurs="0">
 *                   &lt;element name="file">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="customerreference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="collectionordertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;element name="debtorname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="datesubmission" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="datepaymentreceipt" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
 *                             &lt;element name="proceedingstatus" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                             &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionfilesearchresponsebody", propOrder = {
    "filenumberoverview"
})
public class Tcollectionfilesearchresponsebody {

    @XmlElement(required = true)
    protected Tcollectionfilesearchresponsebody.Filenumberoverview filenumberoverview;

    /**
     * Ruft den Wert der filenumberoverview-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionfilesearchresponsebody.Filenumberoverview }
     *     
     */
    public Tcollectionfilesearchresponsebody.Filenumberoverview getFilenumberoverview() {
        return filenumberoverview;
    }

    /**
     * Legt den Wert der filenumberoverview-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionfilesearchresponsebody.Filenumberoverview }
     *     
     */
    public void setFilenumberoverview(Tcollectionfilesearchresponsebody.Filenumberoverview value) {
        this.filenumberoverview = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence maxOccurs="unbounded" minOccurs="0">
     *         &lt;element name="file">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="customerreference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="collectionordertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;element name="debtorname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                   &lt;element name="datesubmission" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="datepaymentreceipt" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *                   &lt;element name="proceedingstatus" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *                   &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" minOccurs="0"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "file"
    })
    public static class Filenumberoverview {

        protected List<Tcollectionfilesearchresponsebody.Filenumberoverview.File> file;

        /**
         * Gets the value of the file property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the file property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getFile().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tcollectionfilesearchresponsebody.Filenumberoverview.File }
         * 
         * 
         */
        public List<Tcollectionfilesearchresponsebody.Filenumberoverview.File> getFile() {
            if (file == null) {
                file = new ArrayList<Tcollectionfilesearchresponsebody.Filenumberoverview.File>();
            }
            return this.file;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="filenumber" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="customerreference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="collectionordertype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="debtorname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="datesubmission" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="datepaymentreceipt" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
         *         &lt;element name="proceedingstatus" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
         *         &lt;element name="user" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tuser" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "filenumber",
            "customerreference",
            "collectionordertype",
            "debtorname",
            "datesubmission",
            "datepaymentreceipt",
            "proceedingstatus",
            "user"
        })
        public static class File {

            @XmlElement(required = true)
            protected String filenumber;
            protected String customerreference;
            protected Tkey collectionordertype;
            protected String debtorname;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar datesubmission;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar datepaymentreceipt;
            protected Tkey proceedingstatus;
            protected String user;

            /**
             * Ruft den Wert der filenumber-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getFilenumber() {
                return filenumber;
            }

            /**
             * Legt den Wert der filenumber-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setFilenumber(String value) {
                this.filenumber = value;
            }

            /**
             * Ruft den Wert der customerreference-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCustomerreference() {
                return customerreference;
            }

            /**
             * Legt den Wert der customerreference-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCustomerreference(String value) {
                this.customerreference = value;
            }

            /**
             * Ruft den Wert der collectionordertype-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getCollectionordertype() {
                return collectionordertype;
            }

            /**
             * Legt den Wert der collectionordertype-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setCollectionordertype(Tkey value) {
                this.collectionordertype = value;
            }

            /**
             * Ruft den Wert der debtorname-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getDebtorname() {
                return debtorname;
            }

            /**
             * Legt den Wert der debtorname-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setDebtorname(String value) {
                this.debtorname = value;
            }

            /**
             * Ruft den Wert der datesubmission-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDatesubmission() {
                return datesubmission;
            }

            /**
             * Legt den Wert der datesubmission-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDatesubmission(XMLGregorianCalendar value) {
                this.datesubmission = value;
            }

            /**
             * Ruft den Wert der datepaymentreceipt-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getDatepaymentreceipt() {
                return datepaymentreceipt;
            }

            /**
             * Legt den Wert der datepaymentreceipt-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setDatepaymentreceipt(XMLGregorianCalendar value) {
                this.datepaymentreceipt = value;
            }

            /**
             * Ruft den Wert der proceedingstatus-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link Tkey }
             *     
             */
            public Tkey getProceedingstatus() {
                return proceedingstatus;
            }

            /**
             * Legt den Wert der proceedingstatus-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link Tkey }
             *     
             */
            public void setProceedingstatus(Tkey value) {
                this.proceedingstatus = value;
            }

            /**
             * Ruft den Wert der user-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getUser() {
                return user;
            }

            /**
             * Legt den Wert der user-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setUser(String value) {
                this.user = value;
            }

        }

    }

}
