
package https.onlineservice_creditreform_de.webservice._0600_0021;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tprofitslossessheet complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tprofitslossessheet">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="reportperiod" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timpreciseperiod" minOccurs="0"/>
 *         &lt;element name="balanceform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="typeofbalance" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="jurisdiction" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="calculationtype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="currency" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="chapter" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="chaptername" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="amountdecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                   &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *                   &lt;element name="item" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tprofitslossesitem" maxOccurs="unbounded" minOccurs="0"/>
 *                   &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tprofitslossessheet", propOrder = {
    "reportperiod",
    "balanceform",
    "typeofbalance",
    "jurisdiction",
    "calculationtype",
    "currency",
    "chapter"
})
public class Tprofitslossessheet {

    protected Timpreciseperiod reportperiod;
    protected Tkey balanceform;
    protected Tkey typeofbalance;
    protected Tkey jurisdiction;
    protected Tkey calculationtype;
    protected Tkey currency;
    protected List<Tprofitslossessheet.Chapter> chapter;

    /**
     * Ruft den Wert der reportperiod-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Timpreciseperiod }
     *     
     */
    public Timpreciseperiod getReportperiod() {
        return reportperiod;
    }

    /**
     * Legt den Wert der reportperiod-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Timpreciseperiod }
     *     
     */
    public void setReportperiod(Timpreciseperiod value) {
        this.reportperiod = value;
    }

    /**
     * Ruft den Wert der balanceform-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getBalanceform() {
        return balanceform;
    }

    /**
     * Legt den Wert der balanceform-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setBalanceform(Tkey value) {
        this.balanceform = value;
    }

    /**
     * Ruft den Wert der typeofbalance-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getTypeofbalance() {
        return typeofbalance;
    }

    /**
     * Legt den Wert der typeofbalance-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setTypeofbalance(Tkey value) {
        this.typeofbalance = value;
    }

    /**
     * Ruft den Wert der jurisdiction-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getJurisdiction() {
        return jurisdiction;
    }

    /**
     * Legt den Wert der jurisdiction-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setJurisdiction(Tkey value) {
        this.jurisdiction = value;
    }

    /**
     * Ruft den Wert der calculationtype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCalculationtype() {
        return calculationtype;
    }

    /**
     * Legt den Wert der calculationtype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCalculationtype(Tkey value) {
        this.calculationtype = value;
    }

    /**
     * Ruft den Wert der currency-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCurrency() {
        return currency;
    }

    /**
     * Legt den Wert der currency-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCurrency(Tkey value) {
        this.currency = value;
    }

    /**
     * Gets the value of the chapter property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the chapter property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getChapter().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Tprofitslossessheet.Chapter }
     * 
     * 
     */
    public List<Tprofitslossessheet.Chapter> getChapter() {
        if (chapter == null) {
            chapter = new ArrayList<Tprofitslossessheet.Chapter>();
        }
        return this.chapter;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="chaptername" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="amountdecoration" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="amount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
     *         &lt;element name="item" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tprofitslossesitem" maxOccurs="unbounded" minOccurs="0"/>
     *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "chaptername",
        "amountdecoration",
        "amount",
        "item",
        "text"
    })
    public static class Chapter {

        protected Tkey chaptername;
        protected Tkey amountdecoration;
        protected BigDecimal amount;
        protected List<Tprofitslossesitem> item;
        protected Ttext text;

        /**
         * Ruft den Wert der chaptername-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getChaptername() {
            return chaptername;
        }

        /**
         * Legt den Wert der chaptername-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setChaptername(Tkey value) {
            this.chaptername = value;
        }

        /**
         * Ruft den Wert der amountdecoration-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getAmountdecoration() {
            return amountdecoration;
        }

        /**
         * Legt den Wert der amountdecoration-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setAmountdecoration(Tkey value) {
            this.amountdecoration = value;
        }

        /**
         * Ruft den Wert der amount-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getAmount() {
            return amount;
        }

        /**
         * Legt den Wert der amount-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setAmount(BigDecimal value) {
            this.amount = value;
        }

        /**
         * Gets the value of the item property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the item property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getItem().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link Tprofitslossesitem }
         * 
         * 
         */
        public List<Tprofitslossesitem> getItem() {
            if (item == null) {
                item = new ArrayList<Tprofitslossesitem>();
            }
            return this.item;
        }

        /**
         * Ruft den Wert der text-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getText() {
            return text;
        }

        /**
         * Legt den Wert der text-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setText(Ttext value) {
            this.text = value;
        }

    }

}
