
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tshareholder complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tshareholder">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}identificationnumber" minOccurs="0"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}easynumber" minOccurs="0"/>
 *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}namestructureextended"/>
 *         &lt;element ref="{https://onlineservice.creditreform.de/webservice/0600-0021}status" minOccurs="0"/>
 *         &lt;element name="capitalshare" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tamountpercent" minOccurs="0"/>
 *         &lt;element name="participatingsince" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;element name="participatingtill" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Timprecisedate" minOccurs="0"/>
 *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}addressstructure"/>
 *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
 *         &lt;element name="warnings" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Twarning" minOccurs="0"/>
 *         &lt;element name="trafficlight" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tshareholder", propOrder = {
    "identificationnumber",
    "easynumber",
    "salutation",
    "privateperson",
    "company",
    "status",
    "capitalshare",
    "participatingsince",
    "participatingtill",
    "street",
    "housenumber",
    "housenumberaffix",
    "postcode",
    "city",
    "quarter",
    "region",
    "country",
    "text",
    "warnings",
    "trafficlight"
})
@XmlSeeAlso({
    Tshareholderextended.class
})
public class Tshareholder {

    protected String identificationnumber;
    protected String easynumber;
    protected Tkey salutation;
    protected Tshareholder.Privateperson privateperson;
    protected Tshareholder.Company company;
    protected Tkeywithdescription status;
    protected Tamountpercent capitalshare;
    protected String participatingsince;
    protected String participatingtill;
    protected String street;
    protected Integer housenumber;
    protected String housenumberaffix;
    protected String postcode;
    protected String city;
    protected String quarter;
    protected String region;
    protected Tkey country;
    protected Ttext text;
    protected Twarning warnings;
    protected Tkeywithgrade trafficlight;

    /**
     * Ruft den Wert der identificationnumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdentificationnumber() {
        return identificationnumber;
    }

    /**
     * Legt den Wert der identificationnumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdentificationnumber(String value) {
        this.identificationnumber = value;
    }

    /**
     * Ruft den Wert der easynumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEasynumber() {
        return easynumber;
    }

    /**
     * Legt den Wert der easynumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEasynumber(String value) {
        this.easynumber = value;
    }

    /**
     * Ruft den Wert der salutation-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getSalutation() {
        return salutation;
    }

    /**
     * Legt den Wert der salutation-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setSalutation(Tkey value) {
        this.salutation = value;
    }

    /**
     * Ruft den Wert der privateperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tshareholder.Privateperson }
     *     
     */
    public Tshareholder.Privateperson getPrivateperson() {
        return privateperson;
    }

    /**
     * Legt den Wert der privateperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tshareholder.Privateperson }
     *     
     */
    public void setPrivateperson(Tshareholder.Privateperson value) {
        this.privateperson = value;
    }

    /**
     * Ruft den Wert der company-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tshareholder.Company }
     *     
     */
    public Tshareholder.Company getCompany() {
        return company;
    }

    /**
     * Legt den Wert der company-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tshareholder.Company }
     *     
     */
    public void setCompany(Tshareholder.Company value) {
        this.company = value;
    }

    /**
     * Ruft den Wert der status-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithdescription }
     *     
     */
    public Tkeywithdescription getStatus() {
        return status;
    }

    /**
     * Legt den Wert der status-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithdescription }
     *     
     */
    public void setStatus(Tkeywithdescription value) {
        this.status = value;
    }

    /**
     * Ruft den Wert der capitalshare-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tamountpercent }
     *     
     */
    public Tamountpercent getCapitalshare() {
        return capitalshare;
    }

    /**
     * Legt den Wert der capitalshare-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tamountpercent }
     *     
     */
    public void setCapitalshare(Tamountpercent value) {
        this.capitalshare = value;
    }

    /**
     * Ruft den Wert der participatingsince-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipatingsince() {
        return participatingsince;
    }

    /**
     * Legt den Wert der participatingsince-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipatingsince(String value) {
        this.participatingsince = value;
    }

    /**
     * Ruft den Wert der participatingtill-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getParticipatingtill() {
        return participatingtill;
    }

    /**
     * Legt den Wert der participatingtill-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setParticipatingtill(String value) {
        this.participatingtill = value;
    }

    /**
     * Ruft den Wert der street-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStreet() {
        return street;
    }

    /**
     * Legt den Wert der street-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStreet(String value) {
        this.street = value;
    }

    /**
     * Ruft den Wert der housenumber-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getHousenumber() {
        return housenumber;
    }

    /**
     * Legt den Wert der housenumber-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setHousenumber(Integer value) {
        this.housenumber = value;
    }

    /**
     * Ruft den Wert der housenumberaffix-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHousenumberaffix() {
        return housenumberaffix;
    }

    /**
     * Legt den Wert der housenumberaffix-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHousenumberaffix(String value) {
        this.housenumberaffix = value;
    }

    /**
     * Ruft den Wert der postcode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Legt den Wert der postcode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPostcode(String value) {
        this.postcode = value;
    }

    /**
     * Ruft den Wert der city-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCity() {
        return city;
    }

    /**
     * Legt den Wert der city-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCity(String value) {
        this.city = value;
    }

    /**
     * Ruft den Wert der quarter-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuarter() {
        return quarter;
    }

    /**
     * Legt den Wert der quarter-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuarter(String value) {
        this.quarter = value;
    }

    /**
     * Ruft den Wert der region-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRegion() {
        return region;
    }

    /**
     * Legt den Wert der region-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRegion(String value) {
        this.region = value;
    }

    /**
     * Ruft den Wert der country-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getCountry() {
        return country;
    }

    /**
     * Legt den Wert der country-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setCountry(Tkey value) {
        this.country = value;
    }

    /**
     * Ruft den Wert der text-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Ttext }
     *     
     */
    public Ttext getText() {
        return text;
    }

    /**
     * Legt den Wert der text-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Ttext }
     *     
     */
    public void setText(Ttext value) {
        this.text = value;
    }

    /**
     * Ruft den Wert der warnings-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Twarning }
     *     
     */
    public Twarning getWarnings() {
        return warnings;
    }

    /**
     * Legt den Wert der warnings-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Twarning }
     *     
     */
    public void setWarnings(Twarning value) {
        this.warnings = value;
    }

    /**
     * Ruft den Wert der trafficlight-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getTrafficlight() {
        return trafficlight;
    }

    /**
     * Legt den Wert der trafficlight-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setTrafficlight(Tkeywithgrade value) {
        this.trafficlight = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="legalform" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithshortdesignation" minOccurs="0"/>
     *         &lt;element name="datelastregisterentry" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="datelegalform" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/>
     *         &lt;element name="registertext" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "companyname",
        "legalform",
        "datelastregisterentry",
        "datelegalform",
        "registertext",
        "text"
    })
    public static class Company {

        protected String companyname;
        protected Tkeywithshortdesignation legalform;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datelastregisterentry;
        @XmlSchemaType(name = "date")
        protected XMLGregorianCalendar datelegalform;
        protected String registertext;
        protected Ttext text;

        /**
         * Ruft den Wert der companyname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyname() {
            return companyname;
        }

        /**
         * Legt den Wert der companyname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyname(String value) {
            this.companyname = value;
        }

        /**
         * Ruft den Wert der legalform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public Tkeywithshortdesignation getLegalform() {
            return legalform;
        }

        /**
         * Legt den Wert der legalform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public void setLegalform(Tkeywithshortdesignation value) {
            this.legalform = value;
        }

        /**
         * Ruft den Wert der datelastregisterentry-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDatelastregisterentry() {
            return datelastregisterentry;
        }

        /**
         * Legt den Wert der datelastregisterentry-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDatelastregisterentry(XMLGregorianCalendar value) {
            this.datelastregisterentry = value;
        }

        /**
         * Ruft den Wert der datelegalform-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public XMLGregorianCalendar getDatelegalform() {
            return datelegalform;
        }

        /**
         * Legt den Wert der datelegalform-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link XMLGregorianCalendar }
         *     
         */
        public void setDatelegalform(XMLGregorianCalendar value) {
            this.datelegalform = value;
        }

        /**
         * Ruft den Wert der registertext-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getRegistertext() {
            return registertext;
        }

        /**
         * Legt den Wert der registertext-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setRegistertext(String value) {
            this.registertext = value;
        }

        /**
         * Ruft den Wert der text-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getText() {
            return text;
        }

        /**
         * Legt den Wert der text-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setText(Ttext value) {
            this.text = value;
        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;group ref="{https://onlineservice.creditreform.de/webservice/0600-0021}privatepersonstructure"/>
     *         &lt;element name="text" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Ttext" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "maritalstatus",
        "familystatus",
        "numberofchildren",
        "maritalpropertyregime",
        "title",
        "surname",
        "firstname",
        "birthname",
        "dateofbirth",
        "profession",
        "text"
    })
    public static class Privateperson {

        protected Tkey maritalstatus;
        protected Tkey familystatus;
        protected Integer numberofchildren;
        protected Tkey maritalpropertyregime;
        protected Tkeywithshortdesignation title;
        protected String surname;
        protected String firstname;
        protected String birthname;
        protected String dateofbirth;
        protected String profession;
        protected Ttext text;

        /**
         * Ruft den Wert der maritalstatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getMaritalstatus() {
            return maritalstatus;
        }

        /**
         * Legt den Wert der maritalstatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setMaritalstatus(Tkey value) {
            this.maritalstatus = value;
        }

        /**
         * Ruft den Wert der familystatus-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getFamilystatus() {
            return familystatus;
        }

        /**
         * Legt den Wert der familystatus-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setFamilystatus(Tkey value) {
            this.familystatus = value;
        }

        /**
         * Ruft den Wert der numberofchildren-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNumberofchildren() {
            return numberofchildren;
        }

        /**
         * Legt den Wert der numberofchildren-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNumberofchildren(Integer value) {
            this.numberofchildren = value;
        }

        /**
         * Ruft den Wert der maritalpropertyregime-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getMaritalpropertyregime() {
            return maritalpropertyregime;
        }

        /**
         * Legt den Wert der maritalpropertyregime-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setMaritalpropertyregime(Tkey value) {
            this.maritalpropertyregime = value;
        }

        /**
         * Ruft den Wert der title-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public Tkeywithshortdesignation getTitle() {
            return title;
        }

        /**
         * Legt den Wert der title-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkeywithshortdesignation }
         *     
         */
        public void setTitle(Tkeywithshortdesignation value) {
            this.title = value;
        }

        /**
         * Ruft den Wert der surname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurname() {
            return surname;
        }

        /**
         * Legt den Wert der surname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurname(String value) {
            this.surname = value;
        }

        /**
         * Ruft den Wert der firstname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFirstname() {
            return firstname;
        }

        /**
         * Legt den Wert der firstname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFirstname(String value) {
            this.firstname = value;
        }

        /**
         * Ruft den Wert der birthname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBirthname() {
            return birthname;
        }

        /**
         * Legt den Wert der birthname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBirthname(String value) {
            this.birthname = value;
        }

        /**
         * Ruft den Wert der dateofbirth-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDateofbirth() {
            return dateofbirth;
        }

        /**
         * Legt den Wert der dateofbirth-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDateofbirth(String value) {
            this.dateofbirth = value;
        }

        /**
         * Ruft den Wert der profession-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getProfession() {
            return profession;
        }

        /**
         * Legt den Wert der profession-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setProfession(String value) {
            this.profession = value;
        }

        /**
         * Ruft den Wert der text-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Ttext }
         *     
         */
        public Ttext getText() {
            return text;
        }

        /**
         * Legt den Wert der text-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Ttext }
         *     
         */
        public void setText(Ttext value) {
            this.text = value;
        }

    }

}
