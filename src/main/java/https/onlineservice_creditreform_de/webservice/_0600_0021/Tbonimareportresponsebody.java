
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java-Klasse f�r Tbonimareportresponsebody complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tbonimareportresponsebody">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="transactionid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerreference" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcustomerreference" minOccurs="0"/>
 *         &lt;element name="creationtime" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="producttype" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *         &lt;element name="useraccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="negativereport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="requestdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgrequestdata" minOccurs="0"/>
 *         &lt;element name="addresscheckdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgaddresscheckdata" minOccurs="0"/>
 *         &lt;element name="identificationdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgidentificationdata" minOccurs="0"/>
 *         &lt;element name="scoredata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcgscoredata" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tbonimareportresponsebody", propOrder = {
    "transactionid",
    "customerreference",
    "creationtime",
    "producttype",
    "useraccount",
    "negativereport",
    "requestdata",
    "addresscheckdata",
    "identificationdata",
    "scoredata"
})
public class Tbonimareportresponsebody {

    protected String transactionid;
    protected String customerreference;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar creationtime;
    protected Tkey producttype;
    protected String useraccount;
    protected String negativereport;
    protected Tcgrequestdata requestdata;
    protected Tcgaddresscheckdata addresscheckdata;
    protected Tcgidentificationdata identificationdata;
    protected Tcgscoredata scoredata;

    /**
     * Ruft den Wert der transactionid-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTransactionid() {
        return transactionid;
    }

    /**
     * Legt den Wert der transactionid-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTransactionid(String value) {
        this.transactionid = value;
    }

    /**
     * Ruft den Wert der customerreference-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerreference() {
        return customerreference;
    }

    /**
     * Legt den Wert der customerreference-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerreference(String value) {
        this.customerreference = value;
    }

    /**
     * Ruft den Wert der creationtime-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCreationtime() {
        return creationtime;
    }

    /**
     * Legt den Wert der creationtime-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCreationtime(XMLGregorianCalendar value) {
        this.creationtime = value;
    }

    /**
     * Ruft den Wert der producttype-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkey }
     *     
     */
    public Tkey getProducttype() {
        return producttype;
    }

    /**
     * Legt den Wert der producttype-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkey }
     *     
     */
    public void setProducttype(Tkey value) {
        this.producttype = value;
    }

    /**
     * Ruft den Wert der useraccount-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUseraccount() {
        return useraccount;
    }

    /**
     * Legt den Wert der useraccount-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUseraccount(String value) {
        this.useraccount = value;
    }

    /**
     * Ruft den Wert der negativereport-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNegativereport() {
        return negativereport;
    }

    /**
     * Legt den Wert der negativereport-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNegativereport(String value) {
        this.negativereport = value;
    }

    /**
     * Ruft den Wert der requestdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgrequestdata }
     *     
     */
    public Tcgrequestdata getRequestdata() {
        return requestdata;
    }

    /**
     * Legt den Wert der requestdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgrequestdata }
     *     
     */
    public void setRequestdata(Tcgrequestdata value) {
        this.requestdata = value;
    }

    /**
     * Ruft den Wert der addresscheckdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgaddresscheckdata }
     *     
     */
    public Tcgaddresscheckdata getAddresscheckdata() {
        return addresscheckdata;
    }

    /**
     * Legt den Wert der addresscheckdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgaddresscheckdata }
     *     
     */
    public void setAddresscheckdata(Tcgaddresscheckdata value) {
        this.addresscheckdata = value;
    }

    /**
     * Ruft den Wert der identificationdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgidentificationdata }
     *     
     */
    public Tcgidentificationdata getIdentificationdata() {
        return identificationdata;
    }

    /**
     * Legt den Wert der identificationdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgidentificationdata }
     *     
     */
    public void setIdentificationdata(Tcgidentificationdata value) {
        this.identificationdata = value;
    }

    /**
     * Ruft den Wert der scoredata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcgscoredata }
     *     
     */
    public Tcgscoredata getScoredata() {
        return scoredata;
    }

    /**
     * Legt den Wert der scoredata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcgscoredata }
     *     
     */
    public void setScoredata(Tcgscoredata value) {
        this.scoredata = value;
    }

}
