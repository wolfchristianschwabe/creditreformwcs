
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java-Klasse f�r Tcgratingdata complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcgratingdata">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="riskclass" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *         &lt;element name="trafficlightrating" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *         &lt;element name="paymentmode" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *         &lt;element name="creditopinion" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkeywithgrade" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcgratingdata", propOrder = {
    "riskclass",
    "trafficlightrating",
    "paymentmode",
    "creditopinion"
})
public class Tcgratingdata {

    protected Tkeywithgrade riskclass;
    protected Tkeywithgrade trafficlightrating;
    protected Tkeywithgrade paymentmode;
    protected Tkeywithgrade creditopinion;

    /**
     * Ruft den Wert der riskclass-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getRiskclass() {
        return riskclass;
    }

    /**
     * Legt den Wert der riskclass-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setRiskclass(Tkeywithgrade value) {
        this.riskclass = value;
    }

    /**
     * Ruft den Wert der trafficlightrating-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getTrafficlightrating() {
        return trafficlightrating;
    }

    /**
     * Legt den Wert der trafficlightrating-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setTrafficlightrating(Tkeywithgrade value) {
        this.trafficlightrating = value;
    }

    /**
     * Ruft den Wert der paymentmode-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getPaymentmode() {
        return paymentmode;
    }

    /**
     * Legt den Wert der paymentmode-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setPaymentmode(Tkeywithgrade value) {
        this.paymentmode = value;
    }

    /**
     * Ruft den Wert der creditopinion-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tkeywithgrade }
     *     
     */
    public Tkeywithgrade getCreditopinion() {
        return creditopinion;
    }

    /**
     * Legt den Wert der creditopinion-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tkeywithgrade }
     *     
     */
    public void setCreditopinion(Tkeywithgrade value) {
        this.creditopinion = value;
    }

}
