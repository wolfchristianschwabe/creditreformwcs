
package https.onlineservice_creditreform_de.webservice._0600_0021;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Debtor Information
 * 
 * <p>Java-Klasse f�r Tcollectionresponsedebtor complex type.
 * 
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
 * 
 * <pre>
 * &lt;complexType name="Tcollectionresponsedebtor">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;choice>
 *           &lt;element name="company">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;sequence>
 *                       &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                       &lt;sequence>
 *                         &lt;element name="namesplit">
 *                           &lt;complexType>
 *                             &lt;complexContent>
 *                               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                 &lt;sequence>
 *                                   &lt;element name="name1" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                   &lt;element name="name2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                   &lt;element name="name3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                                 &lt;/sequence>
 *                               &lt;/restriction>
 *                             &lt;/complexContent>
 *                           &lt;/complexType>
 *                         &lt;/element>
 *                       &lt;/sequence>
 *                     &lt;/sequence>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *           &lt;element name="privateperson">
 *             &lt;complexType>
 *               &lt;complexContent>
 *                 &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                   &lt;sequence>
 *                     &lt;element name="salutation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
 *                     &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                     &lt;element name="firstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                     &lt;element name="birthname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                   &lt;/sequence>
 *                 &lt;/restriction>
 *               &lt;/complexContent>
 *             &lt;/complexType>
 *           &lt;/element>
 *         &lt;/choice>
 *         &lt;element name="addressdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionresponseaddress"/>
 *         &lt;element name="communicationdata" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tcollectionresponsecommunication" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Tcollectionresponsedebtor", propOrder = {
    "company",
    "privateperson",
    "addressdata",
    "communicationdata"
})
public class Tcollectionresponsedebtor {

    protected Tcollectionresponsedebtor.Company company;
    protected Tcollectionresponsedebtor.Privateperson privateperson;
    @XmlElement(required = true)
    protected Tcollectionresponseaddress addressdata;
    protected Tcollectionresponsecommunication communicationdata;

    /**
     * Ruft den Wert der company-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionresponsedebtor.Company }
     *     
     */
    public Tcollectionresponsedebtor.Company getCompany() {
        return company;
    }

    /**
     * Legt den Wert der company-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionresponsedebtor.Company }
     *     
     */
    public void setCompany(Tcollectionresponsedebtor.Company value) {
        this.company = value;
    }

    /**
     * Ruft den Wert der privateperson-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionresponsedebtor.Privateperson }
     *     
     */
    public Tcollectionresponsedebtor.Privateperson getPrivateperson() {
        return privateperson;
    }

    /**
     * Legt den Wert der privateperson-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionresponsedebtor.Privateperson }
     *     
     */
    public void setPrivateperson(Tcollectionresponsedebtor.Privateperson value) {
        this.privateperson = value;
    }

    /**
     * Ruft den Wert der addressdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionresponseaddress }
     *     
     */
    public Tcollectionresponseaddress getAddressdata() {
        return addressdata;
    }

    /**
     * Legt den Wert der addressdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionresponseaddress }
     *     
     */
    public void setAddressdata(Tcollectionresponseaddress value) {
        this.addressdata = value;
    }

    /**
     * Ruft den Wert der communicationdata-Eigenschaft ab.
     * 
     * @return
     *     possible object is
     *     {@link Tcollectionresponsecommunication }
     *     
     */
    public Tcollectionresponsecommunication getCommunicationdata() {
        return communicationdata;
    }

    /**
     * Legt den Wert der communicationdata-Eigenschaft fest.
     * 
     * @param value
     *     allowed object is
     *     {@link Tcollectionresponsecommunication }
     *     
     */
    public void setCommunicationdata(Tcollectionresponsecommunication value) {
        this.communicationdata = value;
    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;sequence>
     *           &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *           &lt;sequence>
     *             &lt;element name="namesplit">
     *               &lt;complexType>
     *                 &lt;complexContent>
     *                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                     &lt;sequence>
     *                       &lt;element name="name1" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                       &lt;element name="name2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                       &lt;element name="name3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *                     &lt;/sequence>
     *                   &lt;/restriction>
     *                 &lt;/complexContent>
     *               &lt;/complexType>
     *             &lt;/element>
     *           &lt;/sequence>
     *         &lt;/sequence>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "companyname",
        "namesplit"
    })
    public static class Company {

        @XmlElement(required = true)
        protected String companyname;
        @XmlElement(required = true)
        protected Tcollectionresponsedebtor.Company.Namesplit namesplit;

        /**
         * Ruft den Wert der companyname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCompanyname() {
            return companyname;
        }

        /**
         * Legt den Wert der companyname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCompanyname(String value) {
            this.companyname = value;
        }

        /**
         * Ruft den Wert der namesplit-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tcollectionresponsedebtor.Company.Namesplit }
         *     
         */
        public Tcollectionresponsedebtor.Company.Namesplit getNamesplit() {
            return namesplit;
        }

        /**
         * Legt den Wert der namesplit-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tcollectionresponsedebtor.Company.Namesplit }
         *     
         */
        public void setNamesplit(Tcollectionresponsedebtor.Company.Namesplit value) {
            this.namesplit = value;
        }


        /**
         * <p>Java-Klasse f�r anonymous complex type.
         * 
         * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="name1" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="name2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *         &lt;element name="name3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "name1",
            "name2",
            "name3"
        })
        public static class Namesplit {

            @XmlElement(required = true)
            protected String name1;
            protected String name2;
            protected String name3;

            /**
             * Ruft den Wert der name1-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName1() {
                return name1;
            }

            /**
             * Legt den Wert der name1-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName1(String value) {
                this.name1 = value;
            }

            /**
             * Ruft den Wert der name2-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName2() {
                return name2;
            }

            /**
             * Legt den Wert der name2-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName2(String value) {
                this.name2 = value;
            }

            /**
             * Ruft den Wert der name3-Eigenschaft ab.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getName3() {
                return name3;
            }

            /**
             * Legt den Wert der name3-Eigenschaft fest.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setName3(String value) {
                this.name3 = value;
            }

        }

    }


    /**
     * <p>Java-Klasse f�r anonymous complex type.
     * 
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten ist.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="salutation" type="{https://onlineservice.creditreform.de/webservice/0600-0021}Tkey" minOccurs="0"/>
     *         &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="firstname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="birthname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "salutation",
        "surname",
        "firstname",
        "birthname"
    })
    public static class Privateperson {

        protected Tkey salutation;
        @XmlElement(required = true)
        protected String surname;
        protected String firstname;
        protected String birthname;

        /**
         * Ruft den Wert der salutation-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link Tkey }
         *     
         */
        public Tkey getSalutation() {
            return salutation;
        }

        /**
         * Legt den Wert der salutation-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link Tkey }
         *     
         */
        public void setSalutation(Tkey value) {
            this.salutation = value;
        }

        /**
         * Ruft den Wert der surname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSurname() {
            return surname;
        }

        /**
         * Legt den Wert der surname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSurname(String value) {
            this.surname = value;
        }

        /**
         * Ruft den Wert der firstname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFirstname() {
            return firstname;
        }

        /**
         * Legt den Wert der firstname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFirstname(String value) {
            this.firstname = value;
        }

        /**
         * Ruft den Wert der birthname-Eigenschaft ab.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getBirthname() {
            return birthname;
        }

        /**
         * Legt den Wert der birthname-Eigenschaft fest.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setBirthname(String value) {
            this.birthname = value;
        }

    }

}
