" VERSION 2
"******** screens/screen_4/a/Resources.language ********"
      1 de   |E-Mail
      2 de   |Telefon
      3 de   |Interessent
      4 de   |Anschrift privat
      5 de   |Website
      6 de   |Text 1
      7 de   |Anrede
      8 de   |Suchwort
      9 de   |Website besuchen
     10 de   |Ort
     11 de   |Text 2
     12 de   |Name
     13 de   |Allgemeines
     14 de   |Land
     15 de   |Postleitzahl
     16 de   |Anrufen
     17 de   |Bezeichnung
     18 de   |Identnummer
     19 de   |Anschrift Firma
     20 de   |Fax
     21 de   |Interessentenkontakt
     22 de   |Abteilung
     23 de   |Mobiltelefon
     24 de   |E-Mail senden
     25 de   |Senden
     26 de   |Versand, Warenverkehr
     27 de   |Straße
     28 de   |Ansprechpartner
     29 de   |Kennzeichen
     30 de   |Bemerkungen
     31 de   |Region
     32 de   |Besuchen
     33 de   |EDI
     34 de   |EDI-Nachrichten
     35 de   |Schlüssel Versandart
     36 de   |GLN
     37 de   |Spediteur
     38 de   |Werk
     39 de   |Rechnung
     40 de   |Betreuer
     41 de   |Sprache
     42 de   |Landart
     43 de   |EG-Kennung
     44 de   |Umsatzsteuer-Identifikationsnummer
     45 de   |DUNS
     46 de   |Editieren
     47 de   |Deutsch
     48 de   |Übersetzen
     49 de   |Englisch
     50 de   |Französisch
     51 de   |Spanisch
     52 de   |Italienisch
     53 de   |Polnisch
     54 de   |Russisch
     55 de   |Ungarisch
     56 de   |Tschechisch
     57 de   |Türkisch
     58 de   |Individuelle Sprache 1
     59 de   |Individuelle Sprache 2
     60 de   |Individuelle Sprache 3
     61 de   |Amerikanisches Englisch
     62 de   |Rumänisch
     63 de   |Indonesisch
     64 de   |Niederländisch
     65 de   |Mazedonisch
     66 de   |Bulgarisch
     67 de   |Serbisch
     68 de   |Slowenisch
     69 de   |Slowakisch
     70 de   |Portugiesisch
     71 de   |Chinesisch vereinfacht
     72 de   |Chinesisch traditionell
     73 de   |Griechisch
     74 de   |Thailändisch
     75 de   |Arabisch
     76 de   |Persisch
     77 de   |Vietnamesisch
     78 de   |Urdu
     79 de   |Merkmale
     80 de   |Versand
     81 de   |Abladestelle
     82 de   |Konsignationslagerplatz
     83 de   |Intrastat
     84 de   |Zollkennzeichen
     85 de   |Art des Geschäfts
     86 de   |Japanisch
     87 de   |SLP-Status
