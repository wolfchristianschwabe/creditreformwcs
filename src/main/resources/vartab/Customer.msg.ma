" VERSION 2
"******** screens/screen_0/a/Resources.language ********"
      1 de   |Slowakisch
      2 de   |E-Mail
      3 de   |Telefon
      4 de   |Griechisch
      5 de   |Individuelle Sprache 1
      6 de   |Website
      7 de   |Text 1
      8 de   |Anrede
      9 de   |Mazedonisch
     10 de   |Tschechisch
     11 de   |Bulgarisch
     12 de   |Italienisch
     13 de   |Suchwort
     14 de   |Website besuchen
     15 de   |Übersetzen
     16 de   |Englisch
     17 de   |Individuelle Sprache 3
     18 de   |Slowenisch
     19 de   |Serbisch
     20 de   |Ort
     21 de   |Text 2
     22 de   |Name
     23 de   |EDI
     24 de   |Ungarisch
     25 de   |Allgemeines
     26 de   |Land
     27 de   |Russisch
     28 de   |Anrufen
     29 de   |Bezeichnung
     30 de   |Kunde
     31 de   |Identnummer
     32 de   |Deutsch
     33 de   |Fax
     34 de   |Portugiesisch
     35 de   |Abteilung
     36 de   |Rumänisch
     37 de   |Indonesisch
     38 de   |Anschrift Versand
     39 de   |Französisch
     40 de   |Arabisch
     41 de   |Chinesisch vereinfacht
     42 de   |Mobiltelefon
     43 de   |E-Mail senden
     44 de   |Individuelle Sprache 2
     45 de   |Thailändisch
     46 de   |Senden
     47 de   |Anschrift Rechnung
     48 de   |Editieren
     49 de   |Kundenservice
     50 de   |Straße
     51 de   |Polnisch
     52 de   |Ansprechpartner
     53 de   |Persisch
     54 de   |Kennzeichen
     55 de   |Versand, Warenverkehr
     56 de   |Spanisch
     57 de   |Vietnamesisch
     58 de   |Bemerkungen
     59 de   |Niederländisch
     60 de   |Region
     61 de   |Türkisch
     62 de   |Chinesisch traditionell
     63 de   |Urdu
     64 de   |Amerikanisches Englisch
     65 de   |Besuchen
     66 de   |Merkmale
     67 de   |Postleitzahl
     68 de   |Seite 1
     69 de   |Koordinaten
     70 de   |Längengrad
     71 de   |Breitengrad
     72 de   |DMS
     73 de   |Letztes Dokument
     74 de   |Öffnen
     75 de   |Dokumente archivieren
     76 de   |Belegart
     77 de   |Barcode
     78 de   |Zuordnen
     79 de   |Betreff
     80 de   |Datei
     81 de   |Japanisch
     82 de   |Business App Dokumente neu laden
     83 de   |Business App Kundenakte neu laden
     84 de   |Kundenakte
     85 de   |EDI-Nachrichten
     86 de   |Schlüssel Versandart
     87 de   |Spediteur
     88 de   |Unterlieferant
     89 de   |GLN
     90 de   |Werk
     91 de   |Abladestelle
     92 de   |Rechnung
     93 de   |Rechnungsempfänger
     94 de   |E-Rechnung akzeptiert
     95 D    |Vertreter
     95 E    |Representative
     95 de   |Vertreter
     96 de   |E-Mail Rechnungsempfänger
     97 de   |Betreuer
     98 de   |Sprache
     99 de   |Währung
    100 de   |Bruttopreise
    101 de   |Landart
    102 de   |EG-Kennung
    103 de   |Umsatzsteuer-Identifikationsnummer
    104 de   |Lieferantennummer
    105 de   |Nicht sammelfähig
    106 de   |Preisgruppe
    107 de   |Rabattgruppe
    108 de   |Preisstellung
    109 de   |Rechnungsstellung
    110 de   |Gewährleistung
    111 de   |Lieferbedingung
    112 de   |Code
    113 de   |Drucken
    114 de   |Angebot
    115 de   |Exemplare
    116 de   |Auftragsbestätigung
    117 de   |Lieferschein
    118 de   |DUNS
    119 de   |Zahlung
    120 de   |Zahlungsbedingung
    121 de   |Zahlungsbedingungsschlüssel
    122 de   |Mahnbedingung
    123 de   |Mahnsperre
    124 de   |Zahlungsart
    125 de   |Zahlungssperre
    126 de   |Wechsel
    127 de   |Kreditlimit
    128 de   |SEPA UCI
    129 de   |SEPA-Lastschriftmandat
    130 de   |Bankverbindung
    131 de   |Bearbeiten
    132 de   |Neue erzeugen
    133 de   |Bankname
    134 de   |Internationale Ident
    135 de   |Nationale Ident
    136 de   |IBAN
    137 de   |Bankkontonummer
    138 de   |Kontoinhabername
    139 de   |Liquiditätsplanung
    140 de   |Parameter aktiv
    141 de   |Fälligkeit der offenen Posten
    142 de   |Karenztage zwischen Lieferschein und Rechnung
    143 de   |Statistische Zahlungsdaten ignorieren
    144 de   |Durchschnittliche Zahlungsdauer laut Statistik
    145 de   |Durchschnittlicher Skontosatz laut Statistik
    146 de   |Versand
    147 de   |Tour
    148 de   |Versandart 1
    149 de   |Versandart 2
    150 de   |Versandart 3
    151 de   |Konsignationslagerplatz
    152 de   |Intrastat
    153 de   |Zollkennzeichen
    154 de   |Art des Geschäfts
    155 de   |ATLAS
    156 de   |Teilnehmer-Identifikationsnummer
    157 de   |Angemeldetes Verfahren
    158 de   |Vorangegangenes Verfahren
    159 de   |Weiteres Verfahren
    160 de   |Beförderungsmittel im Inland
    161 de   |Verkehrszweig
    162 de   |Beförderungsmittel an der Grenze
    163 de   |Art
    164 de   |Anreise
    165 de   |Einsatzmittel
    166 de   |Dauer
    167 de   |Std
    168 de   |Betreuung
    169 de   |Team
    170 de   |Techniker
    171 de   |Externer Dienstleister
    172 de   |SLP-Status
