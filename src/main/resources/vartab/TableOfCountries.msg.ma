" VERSION 2
"******** screens/screen_802/a/Resources.language ********"
      1 de   |Slowakisch
      2 de   |Griechisch
      3 de   |Individuelle Sprache 1
      4 de   |ISO3
      5 de   |EG-Kenn
      6 de   |Internationale Telefonvorwahl
      7 de   |Mazedonisch
      8 de   |Tschechisch
      9 de   |Bulgarisch
     10 de   |Italienisch
     11 de   |Suchwort
     12 de   |Übersetzen
     13 de   |Englisch
     14 de   |Individuelle Sprache 3
     15 de   |ISO2-Landescode
     16 de   |Slowenisch
     17 de   |Serbisch
     18 E    |ac = accession
     18 de   |EG-Beitritt
     19 de   |Währung
     20 de   |Landart
     21 de   |Ungarisch
     22 de   |Land
     23 de   |Russisch
     24 de   |Bezeichnung
     25 de   |Inlandswährung
     26 de   |Identnummer
     27 de   |ISO2
     28 de   |Deutsch
     29 de   |Portugiesisch
     30 de   |Rumänisch
     31 de   |Indonesisch
     32 de   |ISO3-Landescode
     33 de   |Vorschlagswerte
     34 de   |Französisch
     35 de   |Arabisch
     36 de   |Chinesisch vereinfacht
     37 de   |Adressformat
     38 de   |Individuelle Sprache 2
     39 de   |Thailändisch
     40 de   |Intra
     41 de   |Editieren
     42 de   |Polnisch
     43 de   |Ländertabelle
     44 de   |Persisch
     45 de   |Kennzeichen
     46 de   |Seite 1
     47 de   |Spanisch
     48 de   |Tel-Vorw
     49 de   |Vietnamesisch
     50 de   |Kennz
     51 de   |Niederländisch
     52 de   |Türkisch
     53 de   |Chinesisch traditionell
     54 de   |Urdu
     55 de   |Amerikanisches Englisch
     56 de   |Sprache
     57 de   |Japanisch
