" VERSION 2
"******** screens/screen_51/a/Resources.language ********"
      1 de   |Druckeinstellungen
      2 de   |Slowakisch
      3 de   |Hilfesprache
      4 de   |E-Mail
      5 de   |Griechisch
      6 de   |Individuelle Sprache 1
      7 de   |Mazedonisch
      8 de   |Tschechisch
      9 de   |Bulgarisch
     10 de   |Italienisch
     11 de   |Suchwort
     12 de   |Einstellungen
     13 de   |Übersetzen
     14 de   |Englisch
     15 de   |Editor
     16 de   |Individuelle Sprache 3
     17 de   |Slowenisch
     18 de   |Serbisch
     19 de   |Name
     20 de   |Ungarisch
     21 de   |Passwortdefinitionen
     22 de   |Russisch
     23 de   |Identnummer
     24 de   |Neues Passwort wiederholen
     25 de   |Deutsch
     26 de   |Portugiesisch
     27 de   |Abteilung
     28 de   |Postsuchwort
     29 de   |Rumänisch
     30 de   |Indonesisch
     31 de   |Erlaubnisse zeigen
     32 de   |Französisch
     33 de   |Arabisch
     34 de   |Chinesisch vereinfacht
     35 de   |Individuelle Sprache 2
     36 de   |Thailändisch
     37 de   |Neues Passwort
     38 de   |Editieren
     39 de   |Externer Username
     40 de   |Polnisch
     41 de   |Persisch
     42 de   |Allgemeines
     43 de   |Seite 1
     44 de   |Spanisch
     45 de   |Vietnamesisch
     46 de   |Bearbeiterzeichen
     47 de   |Niederländisch
     48 de   |Türkisch
     49 de   |Chinesisch traditionell
     50 de   |Matchcode-Erkennung
     51 de   |Urdu
     52 de   |Amerikanisches Englisch
     53 de   |JFOP-Server-Konfiguration
     54 de   |Japanisch
     55 de   |Kommandoübersicht
     56 de   |Lade die URL in die Kommandoübersicht
     57 de   |URL-Aufrufparameter für die Kommandoübersicht beim Anmelden
     58 de   |URL-Aufrufparameter dynamisch laden
     59 de   |URL für die Kommandoübersicht
     60 de   |Business App URL neu laden
