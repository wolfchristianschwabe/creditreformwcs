" VERSION 2
"******** screens/screen_22/a/Resources.language ********"
      1 de   |Mischpreis bei Lagerbewegung
      2 de   |Slowakisch
      3 de   |Konto für Bewertungsdifferenz
      4 de   |Standardkontierungsfähig
      5 de   |Griechisch
      6 de   |Individuelle Sprache 1
      7 de   |Mazedonisch
      8 de   |Tschechisch
      9 de   |Bulgarisch
     10 de   |Italienisch
     11 de   |Suchwort
     12 de   |Bewertung
     13 de   |Warengruppe
     14 de   |Übersetzen
     15 de   |Englisch
     16 de   |Individuelle Sprache 3
     17 de   |Slowenisch
     18 de   |Serbisch
     19 de   |Bestandsführung
     20 de   |Gemeinkosten für Fertigung
     21 de   |Gemeinkosten für Einkauf
     22 de   |Ungarisch
     23 de   |Allgemeines
     24 de   |Lohnveredelung
     25 de   |Inventur
     26 de   |Russisch
     27 de   |Bezeichnung
     28 de   |Konto für Preisabweichung
     29 de   |Bewertungsverfahren
     30 de   |Identnummer
     31 de   |Konto für Mengenabweichung
     32 de   |Deutsch
     33 de   |Kontierung
     34 de   |Portugiesisch
     35 de   |Rumänisch
     36 de   |Indonesisch
     37 de   |Kostenstelle/-träger/-verteiler
     38 de   |Französisch
     39 de   |Konto kostenträgerbezogen
     40 de   |Arabisch
     41 de   |Chinesisch vereinfacht
     42 de   |Individuelle Sprache 2
     43 de   |Thailändisch
     44 de   |Lagerbestandskonto
     45 de   |Editieren
     46 de   |Polnisch
     47 de   |Persisch
     48 de   |Spanisch
     49 de   |Vietnamesisch
     50 de   |Konto kostenstellenbezogen
     51 de   |Niederländisch
     52 de   |Aufwand/Bestandsveränderung
     53 de   |Türkisch
     54 de   |Chinesisch traditionell
     55 de   |Urdu
     56 de   |Amerikanisches Englisch
     57 de   |Bewertungsverfahren für Fertigung
     58 de   |Bewertungsverfahren für Einkauf
     59 de   |Lagerbestand aus Einkauf,\nerhalten, aber noch nicht berechnet
     60 de   |Sperre zum Ändern der Bewertungsart LIFO oder FIFO
     61 de   |Neue Plausibilisierung
     62 de   |beim Speichern zusätzlich nach neuer Plausibilisierung prüfen
     63 de   |Bewertungskontierung
     64 de   |Kontierung Zu- und Abgänge
     65 de   |Einkauf/Sonstige
     66 de   |Zugänge aus Einkauf bzw. sonstigen Ursachen
     67 de   |Lagerbestand aus Einkaufs- oder sonstigen Zugängen
     68 de   |Aufwand Abgänge in Fertigung
     69 de   |Einkauf/Sonstige auch für Beistellzugänge verwenden
     70 de   |Aufwand Abgänge in Service
     71 de   |Lagerbestand aus Einkauf, erhalten, aber noch nicht berechnet
     72 de   |Aufwand/Bestandsveränderung durch sonstige Zugänge
     73 de   |Aufwand Abgänge in Beistellungen
     74 de   |Fertigung
     75 de   |Zugänge aus Fertigung
     76 de   |Lagerbestand aus Fertigungszugängen
     77 de   |Bestandsveränderung Abgänge in Fertigung
     78 de   |Fertigung auch für Beistellzugänge verwenden
     79 de   |Bestandsveränderung Abgänge in Service
     80 de   |Bestandsveränderung durch Fertigungszugänge
     81 de   |Bestandsveränderung Abgänge in Beistellungen
     82 de   |Ohne zugeordnete Zugänge
     83 de   |Bestand
     84 de   |Kontierungsfall
     85 de   |Bestandskonto
     86 de   |Neues Konto
     87 de   |Andere Kontierungen
     88 de   |Aufwand Fremdleistungen im Einkauf
     89 de   |Lohnfertigung/Lohnveredelung
     90 de   |Umlagerung
     91 de   |Aufwand/Bestandsveränderung durch Inventur oder Bestandskorrektur
     92 de   |Zugänge
     93 de   |Abgänge
     94 de   |Aufwand/Bestandsveränderung durch Umlagerung
     95 de   |Japanisch
     96 de   |Aufwand Abgänge in Verkauf
     97 de   |Aufwand Abgänge in Manuelle
     98 de   |Bestandsveränderung Abgänge in Verkauf
     99 de   |Bestandsveränderung Abgänge in Manuelle
    100 de   |Im Verkauf ausgelieferter Lagerbestand,\naber noch nicht berechnet
    101 de   |Bestandsveränderung Gemeinkosten durch Fertigungszugänge
    102 de   |Bewertungsdifferenzen
    103 de   |Konto für Bewertungsdifferenzen
