" VERSION 2
"******** screens/screen_23/a/Resources.language ********"
      1 de   |Slowakisch
      2 de   |Standardkontierungsfähig
      3 de   |Griechisch
      4 de   |Individuelle Sprache 1
      5 de   |Mazedonisch
      6 de   |Tschechisch
      7 de   |Bulgarisch
      8 de   |Italienisch
      9 de   |Suchwort
     10 de   |Bewertung
     11 de   |Übersetzen
     12 de   |Englisch
     13 de   |Individuelle Sprache 3
     14 de   |Produktgruppe
     15 de   |Slowenisch
     16 de   |Serbisch
     17 de   |Gemeinkosten für Fertigung
     18 de   |Gemeinkosten für Einkauf
     19 de   |Ungarisch
     20 de   |Russisch
     21 de   |Bezeichnung
     22 de   |Identnummer
     23 de   |Deutsch
     24 de   |Portugiesisch
     25 de   |Rumänisch
     26 de   |Indonesisch
     27 de   |Kostenstelle/-träger/-verteiler
     28 de   |Französisch
     29 de   |Arabisch
     30 de   |Chinesisch vereinfacht
     31 de   |Individuelle Sprache 2
     32 de   |Thailändisch
     33 de   |Editieren
     34 de   |Polnisch
     35 de   |Persisch
     36 de   |Allgemeines
     37 de   |Spanisch
     38 de   |Vietnamesisch
     39 de   |Niederländisch
     40 de   |Türkisch
     41 de   |Chinesisch traditionell
     42 de   |Urdu
     43 de   |Amerikanisches Englisch
     44 de   |Japanisch
     45 de   |Bewertungskontierung
     46 de   |Einzelkosten fix
     47 de   |Gemeinkosten fix
     48 de   |Gemeinkosten variabel
     49 de   |Einzelkosten variabel
     50 de   |Einzelkosten
     51 de   |Gemeinkosten
     52 de   |fix
     53 de   |variabel
     54 de   |Kontierung
     55 de   |Bestandskonto für unfertige Erzeugnisse
     56 de   |Bestandsveränderungskonto für unfertige Erzeugnisse
     57 de   |Kontierung Verkaufserlöse
     58 de   |Verkaufserlöse
     59 de   |Bestand unfertige Erzeugnisse
     60 de   |Maschinenkosten
     61 de   |Lohnkosten
     62 de   |Sonderkosten
     63 de   |Materialkosten
     64 de   |Erlöse und Bestand
     65 de   |Zugänge - Bestandsveränderung in unfertige Erzeugnisse
     66 de   |Abgänge
     67 de   |Bestandsveränderung aus unfertigen Erzeugnissen
