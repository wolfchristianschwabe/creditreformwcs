" VERSION 2
"******** screens/screen_21/a/Resources.language ********"
      1 de   |Bewertungsart für Lagerzugang
      2 de   |Slowakisch
      3 de   |Griechisch
      4 de   |Individuelle Sprache 1
      5 de   |Mazedonisch
      6 de   |Tschechisch
      7 de   |Bulgarisch
      8 de   |Italienisch
      9 de   |Suchwort
     10 de   |Übersetzen
     11 de   |Englisch
     12 de   |Individuelle Sprache 3
     13 de   |Verwendung in Artikeln zeigen
     14 de   |Slowenisch
     15 de   |Serbisch
     16 de   |Bewertungskonfiguration
     17 de   |Löschen
     18 de   |Ungarisch
     19 de   |Verwendung in Warengruppen zeigen
     20 de   |Russisch
     21 de   |Bezeichnung
     22 de   |Identnummer
     23 de   |Deutsch
     24 de   |Portugiesisch
     25 de   |Bewertungsart für Lagerabgang
     26 de   |Nummer
     27 de   |Rumänisch
     28 de   |Indonesisch
     29 de   |Gesperrt
     30 de   |Französisch
     31 de   |Arabisch
     32 de   |Chinesisch vereinfacht
     33 de   |Individuelle Sprache 2
     34 de   |Thailändisch
     35 KEY  |21
     35 D    |Nummer
     35 E    |Number
     35 de   |Nr
     36 de   |Editieren
     37 de   |Polnisch
     38 de   |Persisch
     39 de   |Seite 1
     40 de   |Spanisch
     41 de   |Vietnamesisch
     42 de   |Niederländisch
     43 de   |Türkisch
     44 de   |Chinesisch traditionell
     45 de   |Verwendung
     46 de   |Urdu
     47 de   |Amerikanisches Englisch
     48 de   |Alle Bewertungsverfahren duplizieren
     49 de   |Alle Einkaufs-Bewertungsverfahren duplizieren
     50 de   |Bewertungsverfahren
     51 de   |Neu
     52 de   |Duplizieren
     53 de   |Sperre zum Ändern der Bewertungsart LIFO oder FIFO
     54 de   |Japanisch
