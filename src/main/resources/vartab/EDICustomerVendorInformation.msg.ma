" VERSION 2
"******** screens/screen_477/a/Resources.language ********"
      1 de   |Allgemein
      2 de   |Ultimo
      3 de   |Suchwort
      4 D    |Mit dem Zyklus definieren Sie, in welchem Intervall ein Turnus ausgefuehrt wird.
      4 E    |The interval of the frequency.
      4 de   |Zyklus
      5 de   |Wählen
      6 de   |Bezeichnung
      7 de   |Parameter für Sendedatum
      8 de   |Identnummer
      9 de   |EDI-Nachrichten Lieferant
     10 de   |Startdatum
     11 de   |Konfigurationsprogramm
     12 de   |Abruf
     13 D    |Mit dem Turnus definieren Sie, mit welchem Rhythmus das Sendedatum berechnet werden soll.
     13 E    |The frequency of the send date.
     13 de   |Turnus
     14 de   |Lieferant
     15 de   |Übertragungsnummer
     16 de   |Vorgangstyp
     17 de   |Aktiv
     18 de   |Programm Plausibilitätsprüfung
     19 de   |EDI-Format
     20 de   |Protokoll
     21 de   |Seite 1
     22 de   |Bemerkungen
     23 de   |Druckformat
     24 de   |Verarbeitung
     25 de   |Schema
     26 de   |Nachricht
     27 de   |Import/Export
