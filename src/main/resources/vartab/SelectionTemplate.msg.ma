" VERSION 2
"******** screens/screen_55/a/Resources.language ********"
      1 de   |Zeichen
      2 de   |Slowakisch
      3 de   |Ordnung
      4 de   |Kennbuchstabenzeile
      5 de   |Variable aus Tabelle
      6 de   |Griechisch
      7 de   |Art der Variablen/Verknüpfung
      8 de   |Individuelle Sprache 1
      9 de   |EFOPs
     10 de   |Titel
     11 de   |Referenz
     12 de   |Mazedonisch
     13 de   |Tschechisch
     14 de   |Bulgarisch
     15 de   |Italienisch
     16 de   |Suchwort
     17 de   |Maskenprüfung
     18 de   |Übersetzen
     19 de   |Englisch
     20 de   |Initialwerte
     21 de   |Individuelle Sprache 3
     22 de   |Slowenisch
     23 E    |Search direction
     23 de   |Suchrichtung
     24 de   |Suchschlüssel
     25 de   |Merkmal
     26 de   |Letzte Selektion merken
     27 de   |Serbisch
     28 de   |Bereich
     29 de   |Ungarisch
     30 de   |Allgemeines
     31 de   |Zusatz
     32 de   |aus Vartab
     33 de   |Maskeneintritt
     34 de   |Einheit
     35 de   |Bearbeiten
     36 de   |Referenzzeile
     37 de   |Datenbank
     38 de   |Eigener Variablenname
     39 de   |Aussehen und Verhalten
     40 de   |Variablensprache
     41 de   |Variable/Verknüpfung
     42 de   |Russisch
     43 de   |Wählen
     44 de   |Selektionsleiste
     45 de   |Bezeichnung
     46 de   |Merkmale
     47 de   |Variablenherkunft
     48 de   |Identnummer
     49 de   |Gruppe(n)
     50 de   |Ablageart
     51 de   |Schreibschutz
     52 de   |Deutsch
     53 de   |Initialwerte normieren
     54 de   |Portugiesisch
     55 de   |Benennung
     56 de   |Kennbuchstabe
     57 de   |Art des Verweises einer Verknüpfung
     58 de   |Vartab
     59 de   |Zeilenselektion
     60 de   |Rumänisch
     61 de   |Spaltenbreite
     62 de   |Indonesisch
     63 de   |Merkmalstyp
     64 de   |Autostart
     65 de   |Französisch
     66 de   |Arabisch
     67 de   |Chinesisch vereinfacht
     68 de   |Verknüpfungart
     69 de   |Individuelle Sprache 2
     70 de   |Thailändisch
     71 de   |Priorität
     72 D    |Spaltenbreite
     72 E    |Column width
     72 de   |Sp-Br
     73 de   |Feld ausgefüllt
     74 de   |Einheitenzeile
     75 de   |Editieren
     76 de   |Trefferprüfung
     77 de   |Polnisch
     78 de   |Persisch
     79 de   |Freigabe abas eB
     80 de   |Merkmal wählen
     81 de   |Spanisch
     82 de   |Platzierung
     83 de   |Vietnamesisch
     84 de   |Operator
     85 de   |Niederländisch
     86 de   |Türkisch
     87 de   |Optimale Schlüsselauswahl
     88 de   |Chinesisch traditionell
     89 de   |Verwendung
     90 de   |Urdu
     91 de   |Amerikanisches Englisch
     92 de   |Selektions-Initialwert
     93 de   |Japanisch
