" VERSION 2
"******** screens/screen_109/a/Resources.language ********"
      1 de   |Slowakisch
      2 de   |Standard-Hilfekapitel
      3 de   |Alte Maskenbeschreibung automatisch erzeugen
      4 de   |Griechisch
      5 de   |Variablen
      6 de   |Individuelle Sprache 1
      7 de   |Variable im Kopfteil anfügen
      8 de   |Mazedonisch
      9 de   |Neue Art
     10 de   |Tschechisch
     11 de   |Bulgarisch
     12 de   |Italienisch
     13 de   |Suchwort
     14 de   |Variablentabelle
     15 de   |Übersetzen
     16 de   |Englisch
     17 de   |Individuelle Sprache 3
     18 de   |Slowenisch
     19 de   |Gruppe
     20 de   |Letzte Selektion merken
     21 de   |Serbisch
     22 de   |Name
     23 de   |Ungarisch
     24 de   |Allgemeines
     25 de   |Hilfe
     26 de   |Tab
     27 de   |Datenbankname
     28 de   |Russisch
     29 de   |Bezeichnung
     30 de   |Identnummer
     31 de   |Standardselektionsleiste
     32 de   |FO gesch
     33 de   |Deutsch
     34 de   |FO bek
     35 de   |Art
     36 de   |Portugiesisch
     37 de   |Maske
     38 de   |Individuelles Hilfekapitel
     39 de   |Rumänisch
     40 de   |Indonesisch
     41 de   |Französisch
     42 de   |Arabisch
     43 de   |Chinesisch vereinfacht
     44 de   |Individuelle Sprache 2
     45 de   |Thailändisch
     46 KEY  |109
     46 D    |Ident-Nummer
     46 E    |ID No
     46 de   |Nr
     47 de   |Editieren
     48 de   |Maske bearbeiten
     49 de   |Polnisch
     50 de   |Persisch
     51 de   |Hilfe anzeigen
     52 de   |Spanisch
     53 de   |Neue Maskenbeschreibung
     54 de   |Vietnamesisch
     55 de   |Niederländisch
     56 de   |Neuer Name
     57 de   |Bedeutung
     58 de   |Türkisch
     59 de   |Chinesisch traditionell
     60 de   |Variable im Tabellenteil anfügen
     61 de   |Urdu
     62 de   |Amerikanisches Englisch
     63 de   |Symbol
     64 de   |Japanisch
