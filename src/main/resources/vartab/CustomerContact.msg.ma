" VERSION 2
"******** screens/screen_25/a/Resources.language ********"
      1 de   |Slowakisch
      2 de   |Griechisch
      3 de   |Individuelle Sprache 1
      4 de   |Mazedonisch
      5 de   |Tschechisch
      6 de   |Bulgarisch
      7 de   |Italienisch
      8 de   |Website besuchen
      9 de   |Übersetzen
     10 de   |Englisch
     11 de   |Individuelle Sprache 3
     12 de   |Slowenisch
     13 de   |Lieferbedingung
     14 de   |Betreuer
     15 de   |Serbisch
     16 de   |Ort
     17 de   |EG-Kennung
     18 de   |Landart
     19 de   |Ungarisch
     20 de   |Allgemeines
     21 de   |Russisch
     22 de   |Bezeichnung
     23 de   |DUNS
     24 de   |Kundenkontakt
     25 de   |Deutsch
     26 de   |Portugiesisch
     27 de   |Steuerbefreiungsnummer
     28 de   |Rumänisch
     29 de   |Indonesisch
     30 de   |Französisch
     31 de   |Arabisch
     32 de   |Chinesisch vereinfacht
     33 de   |E-Mail senden
     34 de   |Individuelle Sprache 2
     35 de   |Thailändisch
     36 de   |Kundenservice
     37 de   |Polnisch
     38 de   |Persisch
     39 de   |Spanisch
     40 de   |Vietnamesisch
     41 de   |Umsatzsteuer-Identifikationsnummer
     42 de   |Niederländisch
     43 de   |Rechnung
     44 de   |Türkisch
     45 de   |Chinesisch traditionell
     46 de   |Urdu
     47 de   |Amerikanisches Englisch
     48 de   |Sprache
     49 de   |Merkmale
     50 de   |Seite 1
     51 de   |Koordinaten
     52 de   |Anschrift Firma
     53 de   |Anschrift privat
     54 de   |Längengrad
     55 de   |Breitengrad
     56 de   |Nicht sammelfähig
     57 de   |Japanisch
     58 de   |Identnummer
     59 de   |Suchwort
     60 de   |Text 2
     61 de   |Text 1
     62 de   |Editieren
     63 de   |Anschrift Rechnung
     64 de   |Anschrift Versand
     65 de   |Name
     66 de   |Straße
     67 de   |Postleitzahl
     68 de   |Region
     69 de   |Land
     70 de   |Telefon
     71 de   |Anrufen
     72 de   |Mobiltelefon
     73 de   |Fax
     74 de   |E-Mail
     75 de   |Senden
     76 de   |Website
     77 de   |Besuchen
     78 de   |Anrede
     79 de   |Ansprechpartner
     80 de   |Abteilung
     81 de   |Kennzeichen
     82 de   |Bemerkungen
     83 de   |EDI
     84 de   |EDI-Nachrichten
     85 de   |Schlüssel Versandart
     86 de   |Spediteur
     87 de   |GLN
     88 de   |Werk
     89 de   |Abladestelle
     90 de   |Versand, Warenverkehr
     91 de   |Versand
     92 de   |Tour
     93 de   |Versandart 1
     94 de   |Versandart 2
     95 de   |Versandart 3
     96 de   |Konsignationslagerplatz
     97 de   |Intrastat
     98 de   |Zollkennzeichen
     99 de   |Art des Geschäfts
    100 de   |ATLAS
    101 de   |Teilnehmer-Identifikationsnummer
    102 de   |Angemeldetes Verfahren
    103 de   |Vorangegangenes Verfahren
    104 de   |Weiteres Verfahren
    105 de   |Beförderungsmittel im Inland
    106 de   |Verkehrszweig
    107 de   |Beförderungsmittel an der Grenze
    108 de   |Art
    109 de   |Anreise
    110 de   |Einsatzmittel
    111 de   |Dauer
    112 de   |Std
    113 de   |Betreuung
    114 de   |Team
    115 de   |Techniker
    116 de   |Kunde
    117 de   |SLP-Status
