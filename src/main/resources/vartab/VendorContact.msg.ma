" VERSION 2
"******** screens/screen_26/a/Resources.language ********"
      1 de   |Slowakisch
      2 de   |E-Mail
      3 de   |Telefon
      4 de   |Anschrift privat
      5 de   |Griechisch
      6 de   |Individuelle Sprache 1
      7 de   |Website
      8 de   |Text 1
      9 de   |Anrede
     10 de   |Mazedonisch
     11 de   |Tschechisch
     12 de   |Bulgarisch
     13 de   |Italienisch
     14 de   |Suchwort
     15 de   |Website besuchen
     16 de   |Übersetzen
     17 de   |Englisch
     18 de   |Individuelle Sprache 3
     19 de   |Slowenisch
     20 de   |Konsignationslagerplatz
     21 de   |Lieferbedingung
     22 de   |Betreuer
     23 de   |Serbisch
     24 de   |Ort
     25 de   |Text 2
     26 de   |EG-Kennung
     27 de   |Name
     28 de   |Landart
     29 de   |Ungarisch
     30 de   |Allgemeines
     31 de   |Land
     32 de   |Russisch
     33 de   |Anrufen
     34 de   |Bezeichnung
     35 de   |DUNS
     36 de   |Identnummer
     37 de   |Anschrift Firma
     38 de   |Deutsch
     39 de   |Fax
     40 de   |Portugiesisch
     41 de   |Mahnsperre
     42 de   |Abteilung
     43 de   |Steuerbefreiungsnummer
     44 de   |Rumänisch
     45 de   |Indonesisch
     46 de   |Französisch
     47 de   |Arabisch
     48 de   |Chinesisch vereinfacht
     49 de   |Mobiltelefon
     50 de   |E-Mail senden
     51 de   |Individuelle Sprache 2
     52 de   |Thailändisch
     53 de   |Abladestelle
     54 de   |Lieferant
     55 de   |Senden
     56 de   |Lieferantenkontakt
     57 de   |Editieren
     58 de   |Straße
     59 de   |Polnisch
     60 de   |Ansprechpartner
     61 de   |Persisch
     62 de   |Kennzeichen
     63 de   |Versand, Warenverkehr
     64 de   |Spanisch
     65 de   |Vietnamesisch
     66 de   |Umsatzsteuer-Identifikationsnummer
     67 de   |Bemerkungen
     68 de   |Niederländisch
     69 de   |Rechnung
     70 de   |Region
     71 de   |Türkisch
     72 de   |Chinesisch traditionell
     73 de   |Urdu
     74 de   |Amerikanisches Englisch
     75 de   |Besuchen
     76 de   |Sprache
     77 de   |EDI
     78 de   |EDI-Nachrichten
     79 de   |Schlüssel Versandart
     80 de   |GLN
     81 de   |Werk
     82 de   |Versand
     83 de   |Merkmale
     84 de   |Seite 1
     85 de   |Koordinaten
     86 de   |Längengrad
     87 de   |Breitengrad
     88 de   |Postleitzahl
     89 de   |Intrastat
     90 de   |Zollkennzeichen
     91 de   |Art des Geschäfts
     92 de   |ATLAS
     93 de   |Teilnehmer-Identifikationsnummer
     94 de   |Angemeldetes Verfahren
     95 de   |Vorangegangenes Verfahren
     96 de   |Weiteres Verfahren
     97 de   |Beförderungsmittel im Inland
     98 de   |Verkehrszweig
     99 de   |Beförderungsmittel an der Grenze
    100 de   |Art
    101 de   |Japanisch
    102 de   |SLP-Status
