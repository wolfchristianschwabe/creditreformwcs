" VERSION 2
"******** screens/screen_1/a/Resources.language ********"
      1 de   |Slowakisch
      2 de   |Griechisch
      3 de   |Individuelle Sprache 1
      4 de   |Mazedonisch
      5 de   |Tschechisch
      6 de   |Bulgarisch
      7 de   |Italienisch
      8 de   |Website besuchen
      9 de   |Übersetzen
     10 de   |Englisch
     11 de   |Individuelle Sprache 3
     12 de   |Slowenisch
     13 de   |Serbisch
     14 de   |EDI
     15 de   |Ungarisch
     16 de   |Allgemeines
     17 de   |Russisch
     18 de   |Bezeichnung
     19 de   |Deutsch
     20 de   |Portugiesisch
     21 de   |Rumänisch
     22 de   |Indonesisch
     23 de   |Französisch
     24 de   |Arabisch
     25 de   |Chinesisch vereinfacht
     26 de   |E-Mail senden
     27 de   |Individuelle Sprache 2
     28 de   |Thailändisch
     29 de   |Lieferant
     30 de   |Polnisch
     31 de   |Persisch
     32 de   |Versand, Warenverkehr
     33 de   |Spanisch
     34 de   |Vietnamesisch
     35 de   |Niederländisch
     36 de   |Türkisch
     37 de   |Chinesisch traditionell
     38 de   |Urdu
     39 de   |Amerikanisches Englisch
     40 de   |Merkmale
     41 de   |Seite 1
     42 de   |Koordinaten
     43 de   |Anschrift Bestellung
     44 de   |Anschrift Versand
     45 de   |Längengrad
     46 de   |Breitengrad
     47 de   |DMS
     48 de   |Letztes Dokument
     49 de   |Öffnen
     50 de   |Dokumente archivieren
     51 de   |Belegart
     52 de   |Barcode
     53 de   |Zuordnen
     54 de   |Betreff
     55 de   |Datei
     56 de   |Japanisch
     57 de   |Lieferantenakte
     58 de   |Business App Lieferantenakte neu laden
     59 de   |Business App Dokumente neu laden
     60 de   |Identnummer
     61 de   |Suchwort
     62 de   |Text 1
     63 de   |Text 2
     64 de   |Editieren
     65 de   |Name
     66 de   |Straße
     67 de   |Postleitzahl
     68 de   |Ort
     69 de   |Region
     70 de   |Land
     71 de   |Telefon
     72 de   |Anrufen
     73 de   |Mobiltelefon
     74 de   |Fax
     75 de   |E-Mail
     76 de   |Senden
     77 de   |Website
     78 de   |Besuchen
     79 de   |Anrede
     80 de   |Ansprechpartner
     81 de   |Abteilung
     82 de   |Kennzeichen
     83 de   |Bemerkungen
     84 de   |EDI-Nachrichten
     85 de   |Kunden-Lieferanten-Beziehung
     86 de   |Spediteur
     87 de   |GLN
     88 de   |Rechnung
     89 de   |Betreuer
     90 de   |Sprache
     91 de   |Vorgangssteuerregel
     92 de   |Währung
     93 de   |Bruttopreise
     94 de   |Landart
     95 de   |Steuernummer
     96 de   |EG-Kennung
     97 de   |Umsatzsteuer-Identifikationsnummer
     98 de   |DUNS
     99 de   |Kundennummer
    100 de   |Preisgruppe
    101 de   |Rabattgruppe
    102 de   |Preisstellung
    103 de   |Rechnungsstellung
    104 de   |Gewährleistung
    105 de   |Lieferbedingung
    106 de   |Code
    107 de   |Zahlung
    108 de   |Zahlungsbedingung
    109 de   |Zahlungsbedingungsschlüssel
    110 de   |Zahlungsart
    111 de   |Zahlungssperre
    112 de   |Skontokarenztage gesperrt
    113 de   |SEPA UCI
    114 de   |SEPA-Lastschriftmandat
    115 de   |Bankverbindung
    116 de   |Bearbeiten
    117 de   |Neue erzeugen
    118 de   |Bankname
    119 de   |Internationale Ident
    120 de   |Nationale Ident
    121 de   |IBAN
    122 de   |Bankkontonummer
    123 de   |Kontoinhabername
    124 de   |Liquiditätsplanung
    125 de   |Parameter aktiv
    126 de   |Fälligkeit der offenen Posten
    127 de   |Karenztage zwischen Lieferschein und Rechnung
    128 de   |Konsignationslagerplatz
    129 de   |Mahnsperre
    130 de   |Intrastat
    131 de   |Zollkennzeichen
    132 de   |Art des Geschäfts
    133 de   |ATLAS
    134 de   |Teilnehmer-Identifikationsnummer
    135 de   |Angemeldetes Verfahren
    136 de   |Vorangegangenes Verfahren
    137 de   |Weiteres Verfahren
    138 de   |Beförderungsmittel im Inland
    139 de   |Verkehrszweig
    140 de   |Beförderungsmittel an der Grenze
    141 de   |Art
    142 de   |SLP-Status
