" VERSION 2
"******** screens/screen_3/a/Resources.language ********"
      1 de   |Bezeichnung
      2 de   |Englisch
      3 de   |Datei
      4 de   |Portugiesisch
      5 de   |E-Mail
      6 de   |Telefon
      7 de   |Interessent
      8 de   |Website
      9 de   |Text 1
     10 de   |Anrede
     11 de   |Suchwort
     12 de   |Website besuchen
     13 de   |Bankverbindung
     14 de   |Interessentenakte
     15 de   |Slowenisch
     16 de   |Chinesisch traditionell
     17 de   |Zahlungsbedingungsschlüssel
     18 de   |Urdu
     19 de   |Persisch
     20 de   |Dokumente archivieren
     21 de   |IBAN
     22 de   |Vietnamesisch
     23 de   |Konsignationslagerplatz
     24 de   |Lieferschein
     25 de   |Lieferbedingung
     26 de   |Betreuer
     27 de   |Versandart 3
     28 de   |EDI-Nachrichten
     29 de   |Slowakisch
     30 D    |Vertreter
     30 E    |Representative
     30 de   |Vertreter
     31 de   |Ort
     32 de   |Währung
     33 de   |Text 2
     34 de   |Rechnungsstellung
     35 de   |EG-Kennung
     36 de   |Name
     37 de   |Landart
     38 de   |Auftragsbestätigung
     39 de   |EDI
     40 de   |Übersetzen
     41 de   |Allgemeines
     42 de   |Land
     43 de   |Chinesisch vereinfacht
     44 de   |Postleitzahl
     45 de   |SLP-Status
     46 de   |Letztes Dokument
     47 de   |Internationale Ident
     48 de   |Rechnungsempfänger
     49 de   |GLN
     50 de   |DMS
     51 de   |Bearbeiten
     52 de   |Rabattgruppe
     53 de   |Unterlieferant
     54 de   |Belegart
     55 de   |Individuelle Sprache 3
     56 de   |Mazedonisch
     57 de   |Anrufen
     58 de   |Preisstellung
     59 de   |Indonesisch
     60 de   |Arabisch
     61 de   |Rechnung
     62 de   |Nationale Ident
     63 de   |Zahlungssperre
     64 de   |DUNS
     65 de   |Individuelle Sprache 2
     66 de   |Japanisch
     67 de   |Identnummer
     68 de   |Betreff
     69 de   |Rumänisch
     70 de   |Kreditlimit
     71 de   |Merkmale
     72 de   |Nicht sammelfähig
     73 de   |Russisch
     74 de   |Drucken
     75 de   |Zollkennzeichen
     76 de   |Editieren
     77 de   |Fax
     78 de   |Barcode
     79 de   |Mahnsperre
     80 de   |Spanisch
     81 de   |Art des Geschäfts
     82 de   |Abteilung
     83 de   |Gewährleistung
     84 de   |Spediteur
     85 de   |Wechsel
     86 de   |Business App Kunden-/Interessentenakte neu laden
     87 de   |Bruttopreise
     88 de   |Anschrift Versand
     89 de   |Bankkontonummer
     90 de   |Angebot
     91 de   |Zahlung
     92 de   |Amerikanisches Englisch
     93 de   |Zuordnen
     94 de   |Italienisch
     95 de   |Mobiltelefon
     96 de   |E-Mail senden
     97 de   |Deutsch
     98 de   |Preisgruppe
     99 de   |Intrastat
    100 de   |Abladestelle
    101 de   |Senden
    102 de   |Bankname
    103 de   |Anschrift Rechnung
    104 de   |Straße
    105 de   |ckcre
    106 de   |STAMMDATEN
    107 de   |BONITÄTSINFORMATIONEN
    108 de   |DEBITORENREGISTER
    109 de   |BRANCHENINFORMATIONEN
    110 de   |Bilanzbonität
    111 de   |Bonitätsindex
    112 de   |Bonitätsklasse
    113 de   |Branchencode
    114 de   |Branchenland
    115 de   |Branchenbezeichnung
    116 de   |Branchenart
    117 de   |Adressprüfung
    118 de   |übernehmen
    119 de   |Crefonummer
    120 de   |Report zeigen
    121 de   |DRD-Index
    122 de   |INSO
    123 de   |Recherchereferenzen/Produkte
    124 de   |Risikobewertung
    125 de   |Öffnen
    126 de   |Ansprechpartner
    127 de   |Schlüssel Versandart
    128 de   |Tschechisch
    129 de   |Kennzeichen
    130 de   |Versand, Warenverkehr
    131 de   |Versandart 2
    132 de   |Ungarisch
    133 de   |Kontoinhabername
    134 de   |Versand
    135 de   |Umsatzsteuer-Identifikationsnummer
    136 de   |Serbisch
    137 de   |Niederländisch
    138 de   |Zahlungsart
    139 de   |Bemerkungen
    140 de   |Versandart 1
    141 de   |Zahlungsbedingung
    142 de   |Bulgarisch
    143 de   |Individuelle Sprache 1
    144 de   |Lieferantennummer
    145 de   |Tour
    146 de   |Französisch
    147 de   |Griechisch
    148 de   |Neue erzeugen
    149 de   |Türkisch
    150 de   |Region
    151 de   |Mahnbedingung
    152 de   |Polnisch
    153 de   |Thailändisch
    154 de   |Exemplare
    155 de   |Besuchen
    156 de   |Sprache
