" VERSION 2
"******** screens/screen_9555554/a/Resources.language ********"
      1 SRC  |screens/screen_9555554/a/Resources.language
      1 de   |Creditreform Login und Passwortänderung
      2 SRC  |screens/screen_9555554/a/Resources.language
      2 de   |Daten ändern
      3 SRC  |screens/screen_9555554/a/Resources.language
      3 de   |Keylist
      4 SRC  |screens/screen_9555554/a/Resources.language
      4 de   |(6-8 Zeichen)
      5 SRC  |screens/screen_9555554/a/Resources.language
      5 de   |Login
      6 SRC  |screens/screen_9555554/a/Resources.language
      6 de   |Verfügbare Services
      7 SRC  |screens/screen_9555554/a/Resources.language
      7 de   |Emailadresse
      8 SRC  |screens/screen_9555554/a/Resources.language
      8 de   |Passwort ändern
      9 SRC  |screens/screen_9555554/a/Resources.language
      9 de   |Verwerfen
     10 SRC  |screens/screen_9555554/a/Resources.language
     10 de   |Bilanzanalyse mehrere Firmen (Deprecated)
     11 SRC  |screens/screen_9555554/a/Resources.language
     11 de   |Bilanzanalyse Einzelfirma (Deprecated)
     12 SRC  |screens/screen_9555554/a/Resources.language
     12 de   |Bonima-Auskunft
     13 SRC  |screens/screen_9555554/a/Resources.language
     13 de   |Kündigen der Standard-Nachtragsfrist
     14 SRC  |screens/screen_9555554/a/Resources.language
     14 de   |Ändern der Email-Adresse
     15 SRC  |screens/screen_9555554/a/Resources.language
     15 de   |Ändern des erweiterten Monitorings
     16 SRC  |screens/screen_9555554/a/Resources.language
     16 de   |Creditreform-Rechnungsdaten
     17 SRC  |screens/screen_9555554/a/Resources.language
     17 de   |Creditreform-Nachrichten
     18 SRC  |screens/screen_9555554/a/Resources.language
     18 de   |Gläubigernachricht senden
     19 SRC  |screens/screen_9555554/a/Resources.language
     19 de   |Akten suchen
     20 SRC  |screens/screen_9555554/a/Resources.language
     20 de   |Inkasso-Auftrag anlegen
     21 SRC  |screens/screen_9555554/a/Resources.language
     21 de   |Consumer-Auskunft
     22 SRC  |screens/screen_9555554/a/Resources.language
     22 de   |Creditreform-Nachrichtenliste
     23 SRC  |screens/screen_9555554/a/Resources.language
     23 de   |Liste der Gläubigernachrichten
     24 SRC  |screens/screen_9555554/a/Resources.language
     24 de   |Mit default Keylist starten?
     25 SRC  |screens/screen_9555554/a/Resources.language
     25 de   |Logindaten speichern
     26 SRC  |screens/screen_9555554/a/Resources.language
     26 de   |Email
     27 SRC  |screens/screen_9555554/a/Resources.language
     27 de   |Email Adresse für Infomails ändern
     28 SRC  |screens/screen_9555554/a/Resources.language
     28 de   |Nachmeldungen vornehmen
     29 SRC  |screens/screen_9555554/a/Resources.language
     29 de   |Allgemeine Akteninformationen
     30 SRC  |screens/screen_9555554/a/Resources.language
     30 de   |Allgemeines Passwort
     31 SRC  |screens/screen_9555554/a/Resources.language
     31 de   |Zuordnung der einheitlichen Identnummer
     32 SRC  |screens/screen_9555554/a/Resources.language
     32 de   |Identifizierung / Produktabruf
     33 SRC  |screens/screen_9555554/a/Resources.language
     33 de   |aktuelle Keylist
     34 SRC  |screens/screen_9555554/a/Resources.language
     34 de   |Keylist anzeigen
     35 SRC  |screens/screen_9555554/a/Resources.language
     35 de   |Schlüsselliste
     36 SRC  |screens/screen_9555554/a/Resources.language
     36 de   |Andere Sprache wählen
     37 SRC  |screens/screen_9555554/a/Resources.language
     37 de   |Gewähle Sprache
     38 SRC  |screens/screen_9555554/a/Resources.language
     38 de   |Loginname
     39 SRC  |screens/screen_9555554/a/Resources.language
     39 de   |Mailbox-Inhaltsverzeichnis
     40 SRC  |screens/screen_9555554/a/Resources.language
     40 de   |Mailboxeinträge abrufen
     41 SRC  |screens/screen_9555554/a/Resources.language
     41 de   |Es gibt Ungelesene Mails
     42 SRC  |screens/screen_9555554/a/Resources.language
     42 de   |Monitoring-Status
     43 SRC  |screens/screen_9555554/a/Resources.language
     43 de   |Rechercheauftrag
     44 SRC  |screens/screen_9555554/a/Resources.language
     44 de   |Verflechtungsdaten anfordern
     45 SRC  |screens/screen_9555554/a/Resources.language
     45 de   |Verflechtungsdaten-Session-ID anfordern
     46 SRC  |screens/screen_9555554/a/Resources.language
     46 de   |Persönliches Passwort
     47 SRC  |screens/screen_9555554/a/Resources.language
     47 de   |Persönliches Passwort ändern
     48 SRC  |screens/screen_9555554/a/Resources.language
     48 de   |Persönliches Passwort neu
     49 SRC  |screens/screen_9555554/a/Resources.language
     49 de   |Persönliches Passwort neu wiederholen
     50 SRC  |screens/screen_9555554/a/Resources.language
     50 de   |Zahlungen
     51 SRC  |screens/screen_9555554/a/Resources.language
     51 de   |Vorgangshistorie
     52 SRC  |screens/screen_9555554/a/Resources.language
     52 de   |Produktverfügbarkeit
     53 SRC  |screens/screen_9555554/a/Resources.language
     53 de   |Forderungspositionen
     54 SRC  |screens/screen_9555554/a/Resources.language
     54 de   |Auskunft
     55 SRC  |screens/screen_9555554/a/Resources.language
     55 de   |Suche
     56 SRC  |screens/screen_9555554/a/Resources.language
     56 de   |Forderungsaufstellung
     57 SRC  |screens/screen_9555554/a/Resources.language
     57 de   |Titulierte Forderungspositionen
     58 SRC  |screens/screen_9555554/a/Resources.language
     58 de   |Liste der upgrade-fähigen Produktaufträge
     59 SRC  |screens/screen_9555554/a/Resources.language
     59 de   |keyattributevalue
     60 SRC  |screens/screen_9555554/a/Resources.language
     60 de   |keyattributename
     61 SRC  |screens/screen_9555554/a/Resources.language
     61 de   |keygroupname
     62 SRC  |screens/screen_9555554/a/Resources.language
     62 de   |keygrouptype
     63 SRC  |screens/screen_9555554/a/Resources.language
     63 de   |language
     64 SRC  |screens/screen_9555554/a/Resources.language
     64 de   |sinceversion
     65 SRC  |screens/screen_9555554/a/Resources.language
     65 de   |Zeilennummer
