" VERSION 2
"******** screens/screen_9555555/a/Resources.language ********"
      1 SRC  |screens/screen_9555555/a/Resources.language
      1 de   |Mailbox abrufen
      2 SRC  |screens/screen_9555555/a/Resources.language
      2 de   |Abbruch
      3 SRC  |screens/screen_9555555/a/Resources.language
      3 de   |Eintragsreferenznummer
      4 SRC  |screens/screen_9555555/a/Resources.language
      4 de   |Postleitzahl
      5 SRC  |screens/screen_9555555/a/Resources.language
      5 de   |Land
      6 SRC  |screens/screen_9555555/a/Resources.language
      6 de   |Quater
      7 SRC  |screens/screen_9555555/a/Resources.language
      7 de   |Firmenname
      8 SRC  |screens/screen_9555555/a/Resources.language
      8 de   |Rechercheaufträge & Mailbox
      9 SRC  |screens/screen_9555555/a/Resources.language
      9 de   |Tradename
     10 SRC  |screens/screen_9555555/a/Resources.language
     10 de   |Hausnummer
     11 SRC  |screens/screen_9555555/a/Resources.language
     11 de   |Produktype
     12 SRC  |screens/screen_9555555/a/Resources.language
     12 de   |Eintragsnummer
     13 SRC  |screens/screen_9555555/a/Resources.language
     13 de   |Straße
     14 SRC  |screens/screen_9555555/a/Resources.language
     14 de   |Easynummer
     15 SRC  |screens/screen_9555555/a/Resources.language
     15 de   |Rechercheauftrag Absenden
     16 SRC  |screens/screen_9555555/a/Resources.language
     16 de   |Stadt
     17 SRC  |screens/screen_9555555/a/Resources.language
     17 de   |Abwicklungs Bedingung
     18 SRC  |screens/screen_9555555/a/Resources.language
     18 de   |Salutation
     19 SRC  |screens/screen_9555555/a/Resources.language
     19 de   |Angefragte Währung
     20 SRC  |screens/screen_9555555/a/Resources.language
     20 de   |Öffnen
     21 SRC  |screens/screen_9555555/a/Resources.language
     21 de   |Privatperson prüfen
     22 SRC  |screens/screen_9555555/a/Resources.language
     22 de   |Land wählen
     23 SRC  |screens/screen_9555555/a/Resources.language
     23 de   |wählen
     24 SRC  |screens/screen_9555555/a/Resources.language
     24 de   |Lieferant
     25 SRC  |screens/screen_9555555/a/Resources.language
     25 de   |Vorbelegung Speichern
     26 SRC  |screens/screen_9555555/a/Resources.language
     26 de   |Mailbox
     27 SRC  |screens/screen_9555555/a/Resources.language
     27 de   |Optional
     28 SRC  |screens/screen_9555555/a/Resources.language
     28 de   |Pflicht
     29 SRC  |screens/screen_9555555/a/Resources.language
     29 de   |Privat Person Pflichtfelder
     30 SRC  |screens/screen_9555555/a/Resources.language
     30 de   |Firma Pflichtfelder
     31 SRC  |screens/screen_9555555/a/Resources.language
     31 de   |PDF Anhang
     32 SRC  |screens/screen_9555555/a/Resources.language
     32 de   |Emailtyp Bestands Mails abrufen
     33 SRC  |screens/screen_9555555/a/Resources.language
     33 de   |Anzahl der abzurufenden Einträge
     34 SRC  |screens/screen_9555555/a/Resources.language
     34 de   |Verwerfen
     35 SRC  |screens/screen_9555555/a/Resources.language
     35 de   |Abrufdatum Ende
     36 SRC  |screens/screen_9555555/a/Resources.language
     36 de   |Abrufdatum Start
     37 SRC  |screens/screen_9555555/a/Resources.language
     37 de   |Letztes Erstellungsdatum
     38 SRC  |screens/screen_9555555/a/Resources.language
     38 de   |Erstellungsdatum
     39 SRC  |screens/screen_9555555/a/Resources.language
     39 de   |Angefragter Betrag
     40 SRC  |screens/screen_9555555/a/Resources.language
     40 de   |Identnummer
     41 SRC  |screens/screen_9555555/a/Resources.language
     41 de   |Geburtsdatum
     42 SRC  |screens/screen_9555555/a/Resources.language
     42 de   |Emailtyp Status
     43 SRC  |screens/screen_9555555/a/Resources.language
     43 de   |Emailtyp Report
     44 SRC  |screens/screen_9555555/a/Resources.language
     44 de   |Emailtyp Ergänzung
     45 SRC  |screens/screen_9555555/a/Resources.language
     45 de   |Emailtyp Update
     46 SRC  |screens/screen_9555555/a/Resources.language
     46 de   |Bereits gelesene
     47 SRC  |screens/screen_9555555/a/Resources.language
     47 de   |Ungelesene Anzeigen
     48 SRC  |screens/screen_9555555/a/Resources.language
     48 de   |Referrenz Nummer
     49 SRC  |screens/screen_9555555/a/Resources.language
     49 de   |Vorname
     50 SRC  |screens/screen_9555555/a/Resources.language
     50 de   |Hausnummerzusatz
     51 SRC  |screens/screen_9555555/a/Resources.language
     51 de   |Länderkürzel
     52 SRC  |screens/screen_9555555/a/Resources.language
     52 de   |Region
     53 SRC  |screens/screen_9555555/a/Resources.language
     53 de   |Strasse
     54 SRC  |screens/screen_9555555/a/Resources.language
     54 de   |Kunde
     55 SRC  |screens/screen_9555555/a/Resources.language
     55 de   |Geschäftszeichen
     56 SRC  |screens/screen_9555555/a/Resources.language
     56 de   |Sprache wählen
     57 SRC  |screens/screen_9555555/a/Resources.language
     57 de   |Geschäftsform
     58 SRC  |screens/screen_9555555/a/Resources.language
     58 de   |Grund
     59 SRC  |screens/screen_9555555/a/Resources.language
     59 de   |Berechtigtes Interesse
     60 SRC  |screens/screen_9555555/a/Resources.language
     60 de   |Bereits geöffnet
     61 SRC  |screens/screen_9555555/a/Resources.language
     61 de   |Ende des Rechercheauftrages aus der Mail
     62 SRC  |screens/screen_9555555/a/Resources.language
     62 de   |Auftrags spezifikationen
     63 SRC  |screens/screen_9555555/a/Resources.language
     63 de   |Start Datum des Auftrags
     64 SRC  |screens/screen_9555555/a/Resources.language
     64 de   |Ordertyp: 1 = Normal, 2 = Eil
     65 SRC  |screens/screen_9555555/a/Resources.language
     65 de   |Auftragsart wählen
     66 SRC  |screens/screen_9555555/a/Resources.language
     66 de   |Produkttype
     67 SRC  |screens/screen_9555555/a/Resources.language
     67 de   |Producttyp wählen
     68 SRC  |screens/screen_9555555/a/Resources.language
     68 de   |Sprache
     69 SRC  |screens/screen_9555555/a/Resources.language
     69 de   |PDF Vorschau
     70 SRC  |screens/screen_9555555/a/Resources.language
     70 de   |Nachname
     71 SRC  |screens/screen_9555555/a/Resources.language
     71 de   |Alias
     72 SRC  |screens/screen_9555555/a/Resources.language
     72 de   |Email öffnen
     73 SRC  |screens/screen_9555555/a/Resources.language
     73 de   |Birthname
     74 SRC  |screens/screen_9555555/a/Resources.language
     74 de   |Abruf Zeitpunkt
     75 SRC  |screens/screen_9555555/a/Resources.language
     75 de   |Kommerzieller Name
     76 SRC  |screens/screen_9555555/a/Resources.language
     76 de   |Erstellungs Zeitpunkt
     77 SRC  |screens/screen_9555555/a/Resources.language
     77 de   |Creditreformnummer
     78 SRC  |screens/screen_9555555/a/Resources.language
     78 de   |Nameszusatz
     79 SRC  |screens/screen_9555555/a/Resources.language
     79 de   |Recherche Zeitpunkt
     80 SRC  |screens/screen_9555555/a/Resources.language
     80 de   |Anrede
     81 SRC  |screens/screen_9555555/a/Resources.language
     81 de   |Status des Rechercheauftrages
     82 SRC  |screens/screen_9555555/a/Resources.language
     82 de   |Name nach Scheidung
     83 SRC  |screens/screen_9555555/a/Resources.language
     83 de   |Witwen Name
     84 SRC  |screens/screen_9555555/a/Resources.language
     84 de   |Titel
     85 SRC  |screens/screen_9555555/a/Resources.language
     85 de   |Handelsname
     86 SRC  |screens/screen_9555555/a/Resources.language
     86 de   |Anfrage stellen
     87 SRC  |screens/screen_9555555/a/Resources.language
     87 de   |Seite 1
     88 SRC  |screens/screen_9555555/a/Resources.language
     88 de   |Erstllungs Zeitpunkt
