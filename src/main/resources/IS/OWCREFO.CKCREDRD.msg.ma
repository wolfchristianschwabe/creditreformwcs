" VERSION 2
"******** screens/screen_9555559/a/Resources.language ********"
      1 SRC  |screens/screen_9555559/a/Resources.language
      1 de   |Cron
      2 SRC  |screens/screen_9555559/a/Resources.language
      2 de   |Kunde
      3 SRC  |screens/screen_9555559/a/Resources.language
      3 de   |Infosystem Debitorenregister (Kreditreform)
      4 SRC  |screens/screen_9555559/a/Resources.language
      4 de   |Auswertung
      5 SRC  |screens/screen_9555559/a/Resources.language
      5 de   |Seite 1
      6 SRC  |screens/screen_9555559/a/Resources.language
      6 de   |Seite 2
      7 SRC  |screens/screen_9555559/a/Resources.language
      7 de   |Optionen
      8 SRC  |screens/screen_9555559/a/Resources.language
      8 de   |Zeitrahmen (Monate)
      9 SRC  |screens/screen_9555559/a/Resources.language
      9 de   |Informationen
     10 SRC  |screens/screen_9555559/a/Resources.language
     10 de   |Request verarbeiten
     11 SRC  |screens/screen_9555559/a/Resources.language
     11 de   |Response verarbeiten
     12 SRC  |screens/screen_9555559/a/Resources.language
     12 de   |bis
     13 SRC  |screens/screen_9555559/a/Resources.language
     13 de   |Datum von
     14 SRC  |screens/screen_9555559/a/Resources.language
     14 de   |Brancheninformationen
     15 SRC  |screens/screen_9555559/a/Resources.language
     15 de   |Lieferanteninformationen
     16 SRC  |screens/screen_9555559/a/Resources.language
     16 de   |2 Monate
     17 SRC  |screens/screen_9555559/a/Resources.language
     17 de   |4 Monate
     18 SRC  |screens/screen_9555559/a/Resources.language
     18 de   |8 Monate
     19 SRC  |screens/screen_9555559/a/Resources.language
     19 de   |12 Monate
     20 SRC  |screens/screen_9555559/a/Resources.language
     20 de   |Poolinformationen
     21 SRC  |screens/screen_9555559/a/Resources.language
     21 de   |Zusatzinformationen
     22 SRC  |screens/screen_9555559/a/Resources.language
     22 de   |Anzahl Belege der Branche aus den letzten 12 Monaten
     23 SRC  |screens/screen_9555559/a/Resources.language
     23 de   |Anzahl der Crefo-Nummern, zu denen Belege vorliegen
     24 SRC  |screens/screen_9555559/a/Resources.language
     24 de   |Durchschnittlicher Belegbetrag in Euro der Branche aus den letzten 12 Monaten
     25 SRC  |screens/screen_9555559/a/Resources.language
     25 de   |Betragsvolumen offener Posten
     26 SRC  |screens/screen_9555559/a/Resources.language
     26 de   |Buchungskreis
     27 SRC  |screens/screen_9555559/a/Resources.language
     27 de   |Crefonummer
     28 SRC  |screens/screen_9555559/a/Resources.language
     28 de   |Stand des Datensatzes
     29 SRC  |screens/screen_9555559/a/Resources.language
     29 de   |Debitornummer
     30 SRC  |screens/screen_9555559/a/Resources.language
     30 de   |Debitor
     31 SRC  |screens/screen_9555559/a/Resources.language
     31 de   |Anzahl Belege letzte 12 Monate
     32 SRC  |screens/screen_9555559/a/Resources.language
     32 de   |Belege 12 Monate Pool exkl. Debitorennummer
     33 SRC  |screens/screen_9555559/a/Resources.language
     33 de   |Durchschnittl. Belegbetrag letzte 12 Monate
     34 SRC  |screens/screen_9555559/a/Resources.language
     34 de   |Durchschnittl. Belegbetrag letzte 12 Monate Pool exkl. Debitornummer
     35 SRC  |screens/screen_9555559/a/Resources.language
     35 de   |Betragsgewichtete durchschnittliche Tage ´┐¢ber (+) oder unter (-) Ziel
     36 SRC  |screens/screen_9555559/a/Resources.language
     36 de   |Betragsgewichtete durchschnittliche Zahlungsweise
     37 SRC  |screens/screen_9555559/a/Resources.language
     37 de   |Betragsgewichtete durchschnittliche Zahlungsweise aus dem Pool
     38 SRC  |screens/screen_9555559/a/Resources.language
     38 de   |Betragsgewichtetes durchschnittliches Zahlungsziel aus den Belegen
     39 SRC  |screens/screen_9555559/a/Resources.language
     39 de   |Betragsgewichtetes durchschnittliches Zahlungsziel aus dem Pool
     40 SRC  |screens/screen_9555559/a/Resources.language
     40 de   |Anzahl Belege letzte 2 Monate
     41 SRC  |screens/screen_9555559/a/Resources.language
     41 de   |Belege 2 Monate Pool exkl. Debitorennummer
     42 SRC  |screens/screen_9555559/a/Resources.language
     42 de   |Durchschnittl. Belegbetrag letzte 2 Monate
     43 SRC  |screens/screen_9555559/a/Resources.language
     43 de   |Durchschnittl. Belegbetrag letzte 2 Monate Pool exkl. Debitornummer
     44 SRC  |screens/screen_9555559/a/Resources.language
     44 de   |Anzahl Belege letzte 4 Monate
     45 SRC  |screens/screen_9555559/a/Resources.language
     45 de   |Belege 4 Monate Pool exkl. Debitorennummer
     46 SRC  |screens/screen_9555559/a/Resources.language
     46 de   |Durchschnittl. Belegbetrag letzte 4 Monate
     47 SRC  |screens/screen_9555559/a/Resources.language
     47 de   |Durchschnittl. Belegbetrag letzte 4 Monate Pool exkl. Debitornummer
     48 SRC  |screens/screen_9555559/a/Resources.language
     48 de   |Anzahl Belege letzte 8 Monate
     49 SRC  |screens/screen_9555559/a/Resources.language
     49 de   |Belege 8 Monate Pool exkl. Debitorennummer
     50 SRC  |screens/screen_9555559/a/Resources.language
     50 de   |Durchschnittl. Belegbetrag letzte 8 Monate
     51 SRC  |screens/screen_9555559/a/Resources.language
     51 de   |Durchschnittl. Belegbetrag letzte 8 Monate Pool exkl. Debitornummer
     52 SRC  |screens/screen_9555559/a/Resources.language
     52 de   |Mandant
     53 SRC  |screens/screen_9555559/a/Resources.language
     53 de   |Mitgliedsnummer
     54 SRC  |screens/screen_9555559/a/Resources.language
     54 de   |Betragsgewichtete durchschnittliche Zahlungsweise der Branche
     55 SRC  |screens/screen_9555559/a/Resources.language
     55 de   |Betragsgewichtetes durchschnittliches Zahlungsziel der Baranche
