package de.ck.app.ckcre.parser;

import de.ck.app.ckcre.TestConstants;
import de.ck.app.ckcre.drd.response.ResponseParser;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class TestResponseParser {

    private static File xsd = TestConstants.getXSD_RESPONSE_TEST();
    private static File xml = TestConstants.getXML_RESPONSE_TEST();

    @Test
    public void testFileExistXSD(){
        assertTrue(xsd.exists());
        assertTrue(xsd.canRead());
    }

    @Test
    public void testFileExistXML(){
        assertTrue(xml.exists());
        assertTrue(xml.canRead());
    }

    @Test
    public void testResponseParsing(){
        try{
            assertNotNull(ResponseParser.parse(xsd, xml));
        }catch(Exception e){
            e.printStackTrace();
            fail();
        }
    }

}
