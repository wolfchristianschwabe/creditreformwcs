package de.ck.app.ckcre.translation;

import de.ck.app.ckcre.drd.util.translation.TranslatedException;
import org.junit.Test;

import static org.junit.Assert.fail;

public class TestTranslatedException {
    @Test
    public void testByErrorcode() {
        int count = 0;
        for (int i = 0; i < 500; i++)
            if (!TranslatedException.Companion.byErrorcode(i).name().equals(TranslatedException.NOT_TRANSLATED.name()))
                count++;

        if (count != TranslatedException.values().length - 1)
            fail("Anzahl Konversionen ungleich Anzahl valider Errorcodes!");
    }
}