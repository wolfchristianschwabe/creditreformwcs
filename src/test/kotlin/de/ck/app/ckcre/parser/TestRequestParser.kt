package de.ck.app.ckcre.parser

import de.ck.app.ckcre.TestConstants
import de.ck.app.ckcre.drd.request.RequestParser
import de.ck.app.ckcre.drd.template.req.*
import de.ck.app.ckcre.drd.util.dsl.*
import de.ck.app.ckcre.drd.util.translation.MarshalFileIOException
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Test
import java.math.BigDecimal

class TestRequestParser {

    @Test
    fun testFileExistXSD() {
        assertTrue(TestConstants.XSD_REQUEST_TEST.exists())
        assertTrue(TestConstants.XSD_REQUEST_TEST.canRead())
    }

    @Test
    fun testFileExistXML() {
        assertTrue(TestConstants.XML_REQUEST_TEST.exists())
        assertTrue(TestConstants.XML_REQUEST_TEST.canRead())
    }

    @Test
    fun testRequestParsing() {
        try {
            val daten = requestdaten {
                extraktion = extraktiontype {
                    buchhaltungssystem = type(BuchhaltungssystemType::class.java) {
                        version = "v2017r4n14"
                        value = "abas-ERP"
                    }
                    buchungskreis = "200"
                    extraktionssoftware = type(ExtraktionssoftwareType::class.java) {
                        value = "abas-ERP"
                        version = "v2017r4n14"
                    }
                    extraktionszeitpunkt = calendar {
                        day = 25; month = 9; year = 2018
                        hour = 17; minute = 30; second = 15
                    }
                    mandant = "entw"
                    mitgliedsnummer = BigDecimal(999999999995.0)
                    sendezeitpunkt = calendar {
                        day = 25; month = 9; year = 2018
                        hour = 17; minute = 30; second = 15
                    }
                    stichtagOffenePosten = calendar {
                        day = 25; month = 9; year = 2018
                        hour = 17; minute = 30; second = 15
                    }
                }

                debitoren = debitoren {
                    debitor.add(
                        debitortype {
                            debitornummer = "0815-001"
                            amtsgerichtPlz = "0815"
                            crefonummer = BigDecimal(24536789665)
                            email = "test@test.test"
                            fax = type(KommunikationsnummerType::class.java){
                                vorwahl = "02058"
                                rufnummer = "08150"
                            }
                            adresse = adressetype{
                                zustelladresse = type(ZustelladresseType::class.java){
                                    hausnummer = "25"
                                    land = "Germany"
                                    ort = "Wülfrath"
                                    plz = "42489"
                                    strasse = "Liegnitzer Strasse 18"
                                }
                                grosskundenadresse = type(GrosskundenadresseType::class.java){
                                    ort = "Wülfrath"
                                    land = "Germany"
                                    plz = "42489"
                                }
                                postfachadresse = type(PostfachadresseType::class.java){
                                    land = "Germany"
                                    plz = "42489"
                                    ort = "Wülfrath"
                                    postfach = "1337"
                                }
                            }
                            name = type(NameType::class.java){
                                name1 = "Name 1"
                                name2 = "Name 2"
                                name3 = "Name 3"
                                name4 = "Name 4"
                            }
                            handelsregisternummer = "0815"
                            mobil = "24356768"
                            rechtsform = "GmbH"
                            registerart = "REGISTERART"
                            telefon = type(KommunikationsnummerType::class.java){
                                rufnummer = "24536"
                                vorwahl = "34214"
                            }
                            saldoOffenePosten = type(BetragType::class.java){
                                value = BigDecimal(5000)
                                waehrung = BetragWaehrungType.EUR
                            }
                            umsatzsteuerID = "UStID"
                            web = "http://www.google.de/"
                        })
                }
                konvertierungsstatistik = konvertierungsstatistik{
                    info.add(type(KonvertierungsInfoType::class.java){
                                id = "ID-001"
                                beschreibung = "Beschreibung!"
                                anzahl = BigDecimal(15)
                            })
                }
                belege = de.ck.app.ckcre.drd.util.dsl.belege {
                    beleg.add(type(BelegType::class.java) {

                        debitornummer = "0815-001"
                        belegnummer = "0815-001-01"
                        buchungsvorgang = "Buchungsvorgang"
                        mahnfreitext = "Mahnfreitext"
                        mahnschluessel = "Mahnschlüssel"
                        mahnsperre = "Mahnsperre"
                        mahnstufe = "Mahnstufe"
                        zahlungskondition = "Zahlungskondition"
                        zahlungsart = "Zahlungsart"

                        ausgleich = type(AusgleichType::class.java){
                            zahlungsbetrag = type(BetragType::class.java){
                                value = BigDecimal(5000)
                                waehrung = BetragWaehrungType.EUR
                            }
                            belegnummer = "Belegnummer"
                            datum = calendar {
                                day = 25; month = 9; year = 2018
                                hour = 17; minute = 30; second = 15
                            }
                        }
                        rechnungsrestbetrag = type(BetragType::class.java){
                            waehrung = BetragWaehrungType.EUR
                            value = BigDecimal(500)
                        }
                        buchungsdatum = calendar {
                            day = 25; month = 9; year = 2018
                            hour = 17; minute = 30; second = 15
                        }
                        zahlungsfrist = type(ZahlungsfristType::class.java){
                            basisdatum = calendar {
                                day = 25; month = 9; year = 2018
                                hour = 17; minute = 30; second = 15
                            }
                            nettofaelligkeitsdatum = calendar {
                                day = 25; month = 9; year = 2018
                                hour = 17; minute = 30; second = 15
                            }
                            //Bei Skonto entweder Schlüsel oder Tage *und* Prozent!
                            skonto.add(type(SkontoType::class.java){
                                tage = BigDecimal(5)
                                prozent = BigDecimal(2.65)
                            })
                            skonto.add(type(SkontoType::class.java){
                                schluessel = "0815"
                            })
                        }
                        rechnungsbetrag = type(BetragType::class.java){
                            value = BigDecimal(5000)
                            waehrung = BetragWaehrungType.EUR
                        }
                    })
                }
            }
            RequestParser(daten).parse(TestConstants.XSD_REQUEST_TEST, TestConstants.XML_REQUEST_TEST)
        } catch (e: MarshalFileIOException) {
            e.printStackTrace()
            fail()
        }
    }
}
