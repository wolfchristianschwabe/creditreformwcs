package de.ck.app.ckcre

import java.io.File

object TestConstants{
    // Tests PARSING/MARSHALLING etc
    @JvmStatic
    val XSD_REQUEST_TEST = File("src/main/resources/base/owcrefo/DRDSchema-v1.0.xsd")
    @JvmStatic
    val XSD_RESPONSE_TEST = File("src/main/resources/base/owcrefo/DRDRuecklieferungSchemaErw-v1.1.xsd")
    @JvmStatic
    val XML_REQUEST_TEST = File("tmp/RESULT.TEST.REQUEST.XML")
    @JvmStatic
    val XML_RESPONSE_TEST = File("src/test/resources/xml/123456789_100_1100_150929.xml")

    //SFTP-Client
    @JvmStatic
    val SFTP_REQUEST_PATH_TEST = "drd-einlieferung/test"
    @JvmStatic
    val SFTP_RESPONSE_PATH_TEST = "drd-ruecklieferung/test"
}