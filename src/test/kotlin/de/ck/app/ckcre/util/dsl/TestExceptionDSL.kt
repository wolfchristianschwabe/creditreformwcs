package de.ck.app.ckcre.util.dsl

import de.ck.app.ckcre.drd.util.dsl.withErrorCode
import de.ck.app.ckcre.drd.util.translation.TestException
import de.ck.app.ckcre.drd.util.translation.TranslatedException
import org.junit.Assert.assertTrue
import org.junit.Test

class TestExceptionDSL{

    @Test
    fun testException(){
        var x = Exception() withErrorCode -1
        assertTrue(x::class == Exception::class)
        x = Exception::class withErrorCode -1
        assertTrue(x::class == Exception::class)
        assertTrue(x.localizedMessage.contains(TranslatedException.NOT_TRANSLATED.TEXT_EN))
    }

    @Test
    fun testInvalidException(){
        val x = TestException("Test!") withErrorCode -1354676
        assertTrue(x::class == TestException::class)
    }

    @Test
    fun testExceptionAtRandom(){
        val extramsg = "Test\nException! ;)"
        val errorcode = TranslatedException.values().let { it.get((Math.random() * it.size).toInt()).ERRORCODE }
        val x = TestException(extramsg) withErrorCode errorcode

        assertTrue(x::class == TestException::class)
        assertTrue(x::class.java == TestException::class.java)
        assertTrue(x.localizedMessage.contains(extramsg))
        TranslatedException.byErrorcode(errorcode).let {
            assertTrue(x.localizedMessage.contains(it.name))
            assertTrue(x.localizedMessage.contains(it.TEXT_DE!!) ||x.localizedMessage.contains(it.TEXT_EN))
        }
    }
}