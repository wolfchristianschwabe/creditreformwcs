package de.ck.app.ckcre

import de.abas.erp.db.DbContext
import de.abas.erp.db.util.ContextHelper
import java.io.File
import java.util.*

object IntegTestConstants{
    // Tests PARSING/MARSHALLING etc
    @JvmStatic
    val XSD_REQUEST_TEST = File("src/main/resources/base/owcrefo/DRDSchema-v1.0.xsd")
    @JvmStatic
    val XSD_RESPONSE_TEST = File("src/main/resources/base/owcrefo/DRDRuecklieferungSchemaErw-v1.1.xsd")
    @JvmStatic
    val XML_REQUEST_TEST = File("tmp/RESULT.TEST.REQUEST.XML")
    @JvmStatic
    val XML_RESPONSE_TEST = File("src/test/resources/xml/123456789_100_1100_150929.xml")

    //SFTP-Client
    @JvmStatic
    val SFTP_REQUEST_PATH_TEST = "drd-einlieferung/test"
    @JvmStatic
    val SFTP_RESPONSE_PATH_TEST = "drd-ruecklieferung/test"
    @JvmStatic
    val credentials = SFTPCredentials(host="drd2-ftp.creditreform.de", user="prz_405000010001", password="ZJlPh7CUcesv", port=22)

    @JvmStatic
    val ctx: DbContext by lazy{
        val map = HashMap<String, String>()
        Scanner(File("gradle.properties")).use {
            while(it.hasNextLine())
                it.nextLine().split(Regex("""[ ]*=[ ]*""")).let { x ->
                    try{map.put(x[0], x[1])} catch (e: Exception){}
                }
        }
        ContextHelper.createClientContext(map.get("EDP_HOST"), map.get("EDP_PORT")?.toInt()?:6560, map.get("ABAS_CLIENTID"), map.get("EDP_PASSWORD"), "owcrefo-integTest") //String host, int port, String mandant, String password, String appName
//        ContextHelper.createClientContext("localhost", 6560, "erp", "sy", "owcrefo-integTest") //String host, int port, String mandant, String password, String appName
    }
}