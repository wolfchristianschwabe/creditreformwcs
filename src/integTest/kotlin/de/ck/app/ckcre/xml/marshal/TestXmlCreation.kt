package de.ck.app.ckcre.xml.marshal

import com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table
import de.abas.erp.db.schema.customer.Customer
import de.abas.erp.db.schema.part.Product
import de.abas.erp.db.schema.sales.Invoice
import de.abas.erp.db.schema.sales.InvoiceEditor
import de.abas.erp.db.selection.SelectionBuilder
import de.ck.app.ckcre.IntegTestConstants
import org.junit.BeforeClass
import org.junit.Test
import java.lang.Math.random
import java.math.BigDecimal

class TestXmlCreation {

    companion object {
        @BeforeClass
        @JvmStatic
        fun createTestData(){
            // 50 Buchungen erstellen!
            repeat(50){
                try{
                    edit(clazz = InvoiceEditor::class.java, ctx = IntegTestConstants.ctx) {
                        table().clear()
                        //Zufälligen Kunden einfügen
                        IntegTestConstants.ctx.createQuery(SelectionBuilder.create(Customer::class.java).build()).execute().let{ customers ->
                            customer = customers.get( (Math.random() * customers.size ).toInt() )
                        }

                        comments = "IntegTests owcrefo"

                        Constants.ctx = IntegTestConstants.ctx
                        setString(Invoice.META.dateFrom, "-${random() * (Integer.parseInt(Constants.CONFIG.get("INTERVAL")) * 5).toInt()}")
                        setString(Invoice.META.entDate, "-${random() * (Integer.parseInt(Constants.CONFIG.get("INTERVAL")) * 5).toInt()}")
//                            dateFrom = randomDate
//                            entDate = randomDate
                        postPackSlipInvoice = true

                        //Tabelle füllen
                        repeat((random() * 10 + 2).toInt()){
                            table().appendRow().let { row ->
                                IntegTestConstants.ctx.createQuery(SelectionBuilder.create(Product::class.java).build()).execute().let { p ->
                                    row.product = p.get( (random() * p.size).toInt() )
                                }
                                row.unitQty = BigDecimal( (random() * 100000).toInt() )
                                row.price = if(row.price.compareTo(BigDecimal.ZERO) == 0) BigDecimal.TEN else row.price
                            }
                        }
                    }
                }catch(e: Exception){
                    e.printStackTrace()
                }
            }
        }
    }

    @Test
    fun test(){
        System.out.println("Dies ist ein IntegTest!")
    }


}