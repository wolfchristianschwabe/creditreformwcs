package de.ck.app.ckcre.ftp.client

import com.jcraft.jsch.ChannelSftp
import de.ck.app.ckcre.IntegTestConstants
import org.junit.Assert.assertTrue
import org.junit.Assert.fail
import org.junit.Test
import java.io.File

class TestSftpClient {

//    data class FTPClient(val user: String? = "prz_405000010001", val password: String? = "ZJlPh7CUcesv", val root: URI? = URI("ftp://drd2-ftp.creditreform.de/"), val port: Int? = 22, var reqPath: String? = "drd-einlieferung", var respPath: String? = "drd-ruecklieferung") : Closeable {
    @Test
    fun testConnect() {
        sftp(IntegTestConstants.credentials, {
            if(!isConnected) fail("Not Connected!")
        })
    }

    @Test
    fun testDownload() {
        sftp(IntegTestConstants.credentials){
            ls(IntegTestConstants.SFTP_RESPONSE_PATH_TEST).filter { !(it as ChannelSftp.LsEntry).attrs.isDir }.forEach {
                val sf = it as ChannelSftp.LsEntry
                File("tmp/${sf.filename}").let{
                    get("${IntegTestConstants.SFTP_RESPONSE_PATH_TEST}/${sf.filename}", it.outputStream())
                    assertTrue(it.exists() && it.canRead())
                }
            }
        }
    }

    @Test
    fun testUpload(){
        sftp(IntegTestConstants.credentials){
            put(IntegTestConstants.XML_REQUEST_TEST.inputStream(), "drd-einlieferung/test/${IntegTestConstants.XML_REQUEST_TEST.name}")
            assertTrue(ls(IntegTestConstants.SFTP_REQUEST_PATH_TEST).any { (it as ChannelSftp.LsEntry).filename.matches(Regex(IntegTestConstants.XML_REQUEST_TEST.name))})
        }
    }

    @Test
    fun testLs(){
        sftp(IntegTestConstants.credentials){
            println("pwd(): ${pwd()}")
            println("""Einlieferung (Testverzeichnis):
                |${ls(IntegTestConstants.SFTP_REQUEST_PATH_TEST).joinToString("\n")}
            """.trimMargin())
            println("""Rücklieferung (Testverzeichnis):
                |${ls(IntegTestConstants.SFTP_RESPONSE_PATH_TEST).joinToString("\n")}
            """.trimMargin())
        }
    }
}